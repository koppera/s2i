import MaterialTable from 'material-table';
import { useMutation, useQuery, useManualQuery } from 'graphql-hooks';
import { forwardRef, useEffect } from 'react';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import ReactDOM from 'react-dom';
import MTableToolbar from 'material-table';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import CloseIcon from '@material-ui/icons/Close';
import { withStyles } from '@material-ui/core/styles';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import { Grid, Typography } from '@material-ui/core';
import MuiDialogActions from '@material-ui/core/DialogActions';
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import Timestamp from '../timestamp';
import Theme from '../css/theme';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import Error from '../components/emptyPage';
import Loader from '../components/loader';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import MenuItem from '@material-ui/core/MenuItem';
import SendIcon from '@material-ui/icons/Send';
import InterafaceExpension from '../CertificatesDetails/certificatesDetailsPage';
import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles,
} from '@material-ui/core/styles';
const REMOVE_PROTOCOL_CERTIFICATE = `mutation removeProtocolCertificate($protocolCertificateUUID:String){
  removeProtocolCertificate(protocolCertificateUUID:$protocolCertificateUUID) {
    partnerProfileUUID
    idProtocolCertificate
    protocolCertificateUUID
    protocolCertificateName
    publicCertificate {
      Name
      imageData
      Length
      Type
    } 
    privateCertificate
    usageValue
    protocolCertificateComments
    APIStatus
    createdOn
    lastUpdatedOn
  } 
} 
  `;
const ADD_PROTOCOL_CERTIFICATE = `mutation createProtocolCertificate($ProtocolCertificateInput:ProtocolCertificateInput){
  createProtocolCertificate(input:$ProtocolCertificateInput) {
    idProtocolCertificate
    protocolCertificateUUID
    protocolCertificateName        
    privateCertificate
    usageValue
    protocolCertificateComments
    partnerProfileUUID
    APIStatus
    createdOn
    lastUpdatedOn 
  } 
}
`;
/*const ADD_PROTOCOL_CERTIFICATE = `mutation createProtocolCertificate($file: Upload!){
  createProtocolCertificate(file: $file) {
    
    publicCertificate
    
   
  } 
}
`;*/
const FETCH_PROTOCOL_CERTIFICATE_BY_PARTNERPROFILEUUID = `query fetchProtocolCertificateByPartnerProfile($partnerProfileUUID:String){
  fetchProtocolCertificateByPartnerProfile(partnerProfileUUID:$partnerProfileUUID) {
    partnerProfileUUID
    idProtocolCertificate
    protocolCertificateUUID
    protocolCertificateName
    privateCertificate
    usageValue
    protocolCertificateComments
   publicCertificate {
     Name
     imageData
     Length
     Type
   } 
   APIStatus
    createdOn
    lastUpdatedOn
  } 
}`;
const API_CAll_DATA = `query ProtocolCertificateAPI($protocolCertificateUUID:String,$partnerProfileUUId: String){ 
  ProtocolCertificateAPI(protocolCertificateUUID:$protocolCertificateUUID,partnerProfileUUId:$partnerProfileUUId){
   statusAPI
   msg
  }
}`;
const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  input: {
    display: 'none',
    //display: 'inline-block'
  },
}));
export default function ProtocolCertificateTable(props) {
  const classes = useStyles();
  const [addProtocolCertificate, addloading, adddata] = useMutation(
    ADD_PROTOCOL_CERTIFICATE
  );
  //remove address
  const [removeProtocolCertificate, removeloading, removedata] = useMutation(
    REMOVE_PROTOCOL_CERTIFICATE
  );
  const { loading, error, data, refetch } = useQuery(
    FETCH_PROTOCOL_CERTIFICATE_BY_PARTNERPROFILEUUID,
    {
      variables: {
        partnerProfileUUID: props.uuid,
      },
    }
  );
  const [
    fetchProtocolCetificateAPI,
    { loading: pTaloading, data: pTadata, error: pTaerrors },
  ] = useManualQuery(API_CAll_DATA);
  const [protocolCertificatedata, setData] = React.useState([]);
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [message, setSnackbarMessage] = React.useState();
  const [variant, setSnackbarVariant] = React.useState('error');
  const [serverError, setServerError] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const [nameError, setNameError] = React.useState({
    helperText: '',
    validateInput: false,
  });
  const columns = [
    {
      title: 'UUID',
      field: 'protocolCertificateUUID',
      editable: 'never',
      hidden: true,
    },
    {
      title: 'Name',
      field: 'protocolCertificateName',
      editable: 'never',
      /*    editComponent: (props) => {
        console.log('editComponent', props, props.value);

        return (
          <div>
            <TextField
              
              placeholder="Name"
              margin="normal"
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            />

            <div style={{ color: 'red' }}>
              {props.rowData.Name === '' ||
              !/^[a-zA-Z.\s]+$/.test(props.rowData.protocolCertificateName)
                ? 'Enter the Alphabets only'
                : ''}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.protocolCertificateName}</div>;
      },*/
    },
    {
      title: 'Public',
      field: 'publicCertificate',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <input
              // accept="image/*"
              className={classes.input}
              id="contained-button-file"
              //multiple
              type="file"
              onChange={(v) => props.onChange(v.target.files)}
            />
            <label htmlFor="contained-button-file">
              <CloudUploadIcon style={{ color: '#0b153e' }} />
            </label>

            <div style={{ color: 'red' }}>
              {!props.value &&
              nameError.validateInput &&
              props.rowData.submitted
                ? nameError.helperText
                : ''}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.publicCertificate.Name}</div>;
      },
    },
    {
      title: 'Private',
      field: 'privateCertificate',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <input
              // accept="image/*"
              className={classes.input}
              id="contained-button-file"
              //multiple
              type="file"
              onChange={(v) => props.onChange(v.target.files)}
            />
            <label htmlFor="contained-button-file">
              <CloudUploadIcon style={{ color: '#0b153e' }} />
            </label>
            <div style={{ color: 'red' }}>
              {!props.value &&
              nameError.validateInput &&
              props.rowData.submitted &&
              !props.rowData.publicCertificate
                ? nameError.helperText
                : ''}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.privateCertificate}</div>;
      },
    },
    {
      title: 'Comments',
      field: 'protocolCertificateComments',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);

        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder="Comments"
              margin="normal"
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            />

            <div style={{ color: 'red' }}>
              {props.rowData.protocolCertificateComments === '' ||
              !/^[a-zA-Z0-9\s]+$/.test(
                props.rowData.protocolCertificateComments
              )
                ? ' Special charecters are Not allowed'
                : ''}
            </div>
          </div>
        );
      },

      render: (rowData) => {
        return <div>{rowData.protocolCertificateComments}</div>;
      },
    },
    {
      title: 'Usage',
      field: 'usageValue',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);

        return (
          <div>
            <TextField
              select
              placeholder="Usage"
              margin="normal"
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            >
              <MenuItem value={'ssl'}>ssl</MenuItem>
              <MenuItem value={'decrypt'}>decrypt</MenuItem>
              <MenuItem value={'sign'}>sign</MenuItem>
            </TextField>
            <div style={{ color: 'red' }}>
              {!props.value &&
              nameError.validateInput &&
              props.rowData.submitted
                ? nameError.helperText
                : ''}
            </div>
          </div>
        );
      },

      render: (rowData) => {
        return <div>{rowData.usageValue}</div>;
      },
    },
    {
      title: 'API Status',

      // field: 'Status'
      render: (rowData) => {
        //console.log(rowData, rowData.APIStatus);
        if (rowData === undefined) {
          return '';
        } else if (
          rowData.APIStatus == null ||
          rowData.APIStatus === 'Inactive'
        ) {
          return <div style={{ color: 'red' }}>Failure</div>;
        } else {
          return <div style={{ color: 'green' }}>Success</div>;
        }
      },
    },
    {
      title: 'Created',
      field: 'createdOn',
      editable: 'never',
      render: (rowData) => {
        console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.createdOn);
        } else {
          return;
        }
      },
    },
    {
      title: 'Last Modified',
      field: 'lastUpdatedOn',
      editable: 'never',
      render: (rowData) => {
        console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.lastUpdatedOn);
        } else {
          return;
        }
      },
    },
  ];
  useEffect(() => {
    /* if (pTadata !== undefined) {
      console.log(pTadata.ProtocolCertificateAPI);
      handleAPISuccess(
        pTadata.ProtocolCertificateAPI.statusAPI,
        pTadata.ProtocolCertificateAPI.msg
      );
    }*/
    if (data !== undefined) {
      console.log(data.fetchProtocolCertificateByPartnerProfile);
      setData(data.fetchProtocolCertificateByPartnerProfile);
      setServerError(false);
    } else {
      console.log('server error');
      setServerError(true);
    }
  }, [data]);
  const handleRefetch = (value, message) => {
    refetch({
      updateData(_, data) {
        return data;
      },
    });
    setOpenSnackbar(true);
    setSnackbarMessage(message);
    if (value) {
      setSnackbarVariant('error');
    } else {
      setSnackbarVariant('success');
    }
  };
  const handleAPISuccess = async (Status, aPIMsg) => {
    //console.log('MsgAPI', msgAPI);
    //setOpenSnackbar(true);
    //setSnackbarMessage(aPIMsg);

    if (Status === 'failure' || Status === null) {
      // setSnackbarVariant('error');
      handleRefetch(true, aPIMsg);
    } else {
      // setSnackbarVariant('success');
      handleRefetch(false, aPIMsg);
    }
  };
  // handleRefetch();
  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const tableIcons = {
    Add: forwardRef((props, ref) => (
      <AddBox color="primary" {...props} ref={ref} />
    )),
    Check: forwardRef((props, ref) => (
      <Check color="primary" {...props} ref={ref} />
    )),
    Clear: forwardRef((props, ref) => (
      <Clear color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline color="error" {...props} ref={ref} />
    )),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => (
      <Edit
        {...props}
        ref={ref}
        color="primary"
        onClick={console.log('hello world')}
      />
    )),
    Export: forwardRef((props, ref) => (
      <SaveAlt color="primary" {...props} ref={ref} />
    )),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => (
      <FirstPage color="primary" {...props} ref={ref} />
    )),
    LastPage: forwardRef((props, ref) => (
      <LastPage color="primary" {...props} ref={ref} />
    )),
    NextPage: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft color="primary" {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => (
      <Clear color="primary" {...props} ref={ref} />
    )),
    Search: forwardRef((props, ref) => (
      <Search color="primary" {...props} ref={ref} />
    )),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSelect: {
          icon: {
            color: '#0b153e',
          },
        },
        MuiTypography: {
          h6: {
            fontSize: 14,
            fontFamily: 'Arial !important',
          },
        },
        MuiDialogTitle: {
          root: {
            backgroundColor: '#0b153e',
            color: '#FFFFFF',
          },
        },
        MuiGrid: {
          container: {
            flexWrap: 'nowrap',
          },
        },
      },
    });
  const readStream = async (stream, encoding = 'utf8') => {
    stream.setEncoding('base64');

    return new Promise((resolve, reject) => {
      let data = '';

      // eslint-disable-next-line no-return-assign
      stream.on('data', (chunk) => (data += chunk));
      stream.on('end', () => resolve(data));
      stream.on('error', (error) => reject(error));
    });
  };
  var fileByteArray = [];
  function processFile(theFile) {
    return function (e) {
      var theBytes = e.target.result; //.split('base64,')[1]; // use with uploadFile2
      fileByteArray.push(theBytes);
      /*  document.getElementById('file').innerText = '';
      for (var i = 0; i < fileByteArray.length; i++) {
        document.getElementById('file').innerText += fileByteArray[i];
      }*/
    };
  }
  function uploadFile3(files) {
    // var files = myInput.files[0];
    var reader = new FileReader();
    reader.onload = processFile(files[0]);
    reader.readAsArrayBuffer(files[0]);
  }
  async function handleAddProtocolCertificate(newData) {
    console.log(newData, newData.publicCertificate);

    console.log(fileByteArray);
    // const reader = new FileReader();
    // console.log(readFileDataAsBase64(newData.publicCertificate));
    return await new Promise(async (resolve) => {
      resolve();
      //await uploadFile3(newData.publicCertificate);
      //   if (fileByteArray.length > 0) {
      addProtocolCertificate({
        variables: {
          ProtocolCertificateInput: {
            protocolCertificateName: newData.protocolCertificateName,
            publicCertificate: newData.publicCertificate[0],
            privateCertificate: newData.privateCertificate,
            protocolCertificateComments: newData.protocolCertificateComments,
            usageValue: newData.usageValue,
            partnerProfileUUID: props.uuid,
          },
        },
      })
        .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
          console.log(data, error, graphQLErrors, networkError, cacheHit);
          if (data !== null && error == false) {
            handleRefetch(error, 'Saved successfully');
          } else if (error && graphQLErrors.length > 0) {
            let duperror = graphQLErrors[0].message;
            console.log(duperror);
            if (duperror !== null && duperror.indexOf('ER_DUP_ENTRY') !== -1) {
              handleRefetch(error, 'Value already existed');
            }
          }
        })
        .catch((e) => {
          // you can do something with the error here
          console.log(e);
        });
      //  }
    });
  }
  async function handleRemoveProtocolCertificate(oldData) {
    return await new Promise(async (resolve) => {
      resolve();
      await removeProtocolCertificate({
        variables: {
          protocolCertificateUUID: oldData.protocolCertificateUUID,
        },
      });
      console.log(removeloading, removedata);
      if (removeloading.loading) {
        console.log('protocolCertificateUUID  not deleted');
        handleRefetch(removeloading.loading, 'Graphql hooks error');
      } else {
        console.log('protocolCertificateUUID deleted successfully');
        handleRefetch(removeloading.loading, 'Deleted successfully');
      }
    });
  }
  const handleCallAPI = async (event, rowData) => {
    console.log('rowData', rowData);
    await fetchProtocolCetificateAPI({
      variables: {
        protocolCertificateUUID: rowData.protocolCertificateUUID,
        partnerProfileUUId: rowData.partnerProfileUUID,
      },
    }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
      console.log(data, error, graphQLErrors, networkError, cacheHit);
      if (data !== null && error == false) {
        handleAPISuccess(
          data.ProtocolCertificateAPI.statusAPI,
          data.ProtocolCertificateAPI.msg
        );
      } else {
        handleRefetch(error, 'graphql hooks error');
      }
    });
  };
  if (loading) return <Loader />;
  if (serverError)
    return <Error type={'Sever connection lost.Please try again'} />;
  return (
    <MuiThemeProvider theme={getMuiTheme()} encType={'multipart/form-data'}>
      <MaterialTable
        icons={tableIcons}
        title="Protocol Certificate"
        columns={columns}
        data={protocolCertificatedata}
        editable={{
          onRowAdd: (newData) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                newData.submitted = true;
                if (
                  (!newData.publicCertificate && !newData.privateCertificate) ||
                  !newData.usageValue
                ) {
                  setNameError({
                    helperText: 'Required.',
                    validateInput: true,
                  });
                  reject();
                  return;
                }
                resolve();
                handleAddProtocolCertificate(newData);
              }, 600);
            }),
          onRowDelete: (oldData) => handleRemoveProtocolCertificate(oldData),
        }}
        detailPanel={[
          {
            render: (rowData) => {
              return <InterafaceExpension rowData={rowData} />;
            },
          },
        ]}
        actions={[
          (rowData) => {
            console.log(rowData, rowData.APIStatus);
            return rowData.APIStatus == null || rowData.APIStatus == 'Inactive'
              ? {
                  icon: SendIcon,
                  disabled: false,
                  onClick: (event, rowData) => handleCallAPI(event, rowData),
                }
              : {
                  icon: SendIcon,
                  disabled: true,
                  //hidden: true
                };
          },
        ]}
        /* actions={[
          {
            icon: SendIcon,
            tooltip: 'API',
            onClick: (event, rowData) => handleCallAPI(event, rowData),
          },
        ]}*/
        options={{
          headerStyle: {
            textAlign: 'center',
            fontSize: 12,
            fontWeight: 'bold',
            fontFamily: 'Arial !important',
            backgroundColor: '#0b153e',
            color: '#ffffff',
            padding: '4px',
          },

          searchFieldStyle: {
            color: '#0b153e',
          },

          actionsColumnIndex: -1,
          pageSize: 5,
          pageSizeOptions: [5, 10, 20],
          toolbar: true,
          paging: true,
        }}
      />
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        autoHideDuration={1500}
        open={openSnackbar}
        onClose={handleCloseSnackbar}
      >
        <MySnackbarContentWrapper
          onClose={handleCloseSnackbar}
          variant={variant}
          message={message}
        />
      </Snackbar>
    </MuiThemeProvider>
  );
}
