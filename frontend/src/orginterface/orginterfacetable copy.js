import MaterialTable from 'material-table';
import { forwardRef } from 'react';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import ReactDOM from 'react-dom';
import MTableToolbar from 'material-table';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import CloseIcon from '@material-ui/icons/Close';
import { withStyles } from '@material-ui/core/styles';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import { Grid, Typography } from '@material-ui/core';
import MuiDialogActions from '@material-ui/core/DialogActions';
import React, { Component, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import Timestamp from '../timestamp';
import { useMutation, useQuery } from 'graphql-hooks';
import Theme from '../css/theme';
import Loader from '../components/loader';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import Error from '../components/emptyPage';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import ProjectForm from './addProjectForm';
import ModalFab from '../components/modalfabtms';
import OrganisationModal from '../orgProjInterfacePanel/organizationForm';
import OrgInterfaceModelForm from './orgInteraceModelForm';
import ProjectModelForm from '../components/tmsmodal';
import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles,
} from '@material-ui/core/styles';

function iconStyles() {
  return {};
}
const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(2),
    },
  },

  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));
const styles = (theme) => ({
  root: {
    Width: 1200,
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: '#FFFFFF',
  },
});
const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const ADD_ORG_INTERFACE = `mutation createOrgInterface($InputCreate:OrgInterfaceInput){
          createOrgInterface(input:$InputCreate){
            interfaceUUID 
              projectUUID 
              sourceProfileUUID
              targetProfileUUID
              docTypeUUID
              serviceUUID
              organizationUUID
          }
        }`;

const DISPLAY_PARTNERS = `{
          allPartners1{
            PartnerProfileUUID
            CorporationName            
          }
        }`;

const DISPLAY_All_ORGANIZATION = `query fetchOrganization1($searchInput:String){
          fetchOrganization1(searchInput:$searchInput) {    
            organizationUUID
            OrganizationName         
          } 
        }
        `;
const DISPLAY_All_INTERFACE = `query fetchInterface1($searchInput:String){
          fetchInterface1(searchInput:$searchInput) {           
            interfaceUUID
            name           
          }
        }
        `;

const DISPLAY_All_PROJECT = `query fetchProject1($searchInput:String){
          fetchProject1(searchInput:$searchInput) {
            projectUUID
            ProjectName           
          }
        }
        `;

const DISPLAY_All_DOCUMENTTYPE = `query fetchDocumentType1($searchInput:String){
          fetchDocumentType1(searchInput:$searchInput) {          
            documentTypeUUID
            name           
          }
        }
        `;
const DISPLAY_All_SERVICE = `query fetchSERVICE1($searchInput:String){
          fetchService1(searchInput:$searchInput) {
            serviceUUID
            name      
          } 
        }
        `;
const DISPLAY_All_ORGINTERFACE = `query organisationInterface($searchInput:String){
          organisationInterface(searchInput:$searchInput) {
            interfaceUUID 
            projectUUID 
            sourceProfileUUID
            targetProfileUUID
            docTypeUUID
            serviceUUID
            organizationUUID
              orgInterfaceUUID
              createdOn
              lastUpdatedOn     
          } 
        }
        `;

export default function OrgInterface() {
  const [searchInput, setSearchValue] = React.useState('');
  const [orgData, setOrgData] = React.useState([]);
  const [orgInterface, setOrgInterface] = React.useState([]);
  const [fromopen, setProjectForm] = React.useState(false);
  const [partnerData, setPartnerData] = React.useState([]);
  const [interfaceData, setInterfaceData] = React.useState([]);
  const [projectData, setProjectData] = React.useState([]);
  const [documentTypeData, setDocumentTypeData] = React.useState([]);
  const [serviceData, setServiceData] = React.useState([]);
  const {
    loading: oloading,
    error: oerror,
    data: OrgData,
    refetch: orefetch,
  } = useQuery(DISPLAY_All_ORGANIZATION, {
    variables: {
      searchInput: searchInput,
    },
  });
  const {
    loading: ploading,
    error: perror,
    data: PartnerData,
    refetch: prefetch,
  } = useQuery(DISPLAY_PARTNERS);
  const {
    loading: iloading,
    error: ierror,
    data: InterfaceData,
    refetch: irefetch,
  } = useQuery(DISPLAY_All_INTERFACE, {
    variables: {
      searchInput: searchInput,
    },
  });

  const {
    loading: pjloading,
    error: pjerror,
    data: ProjectData,
    refetch: pjrefetch,
  } = useQuery(DISPLAY_All_PROJECT, {
    variables: {
      searchInput: searchInput,
    },
  });

  const {
    loading: dtloading,
    error: dterror,
    data: DocumentTypeData,
    refetch: dtrefetch,
  } = useQuery(DISPLAY_All_DOCUMENTTYPE, {
    variables: {
      searchInput: searchInput,
    },
  });
  const {
    loading: sloading,
    error: serror,
    data: ServiceData,
    refetch: srefetch,
  } = useQuery(DISPLAY_All_SERVICE, {
    variables: {
      searchInput: searchInput,
    },
  });

  const { loading: iodloading, error: ioderror, data, refetch } = useQuery(
    DISPLAY_All_ORGINTERFACE,
    {
      variables: {
        searchInput: searchInput,
      },
    }
  );

  const [addOrgInterface, { loading: ioloading, error: ioerror }] = useMutation(
    ADD_ORG_INTERFACE
  );

  const columns = [
    { title: 'UUID', field: 'UUID', editable: 'never', hidden: true },
    {
      title: 'Organization ',
      field: 'organizationUUID',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <Select
              margin="normal"
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            >
              <MenuItem value={10}>Cloudgen</MenuItem>
            </Select>

            <div style={{ color: 'red' }}>
              {props.rowData.Organization === '' ? 'Select a value' : ''}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.Organization}</div>;
      },
    },
    {
      title: 'Project',
      field: 'Project',
      render: (rowData) => {
        console.log(rowData);
      },
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <Select
              id=""
              margin="normal"
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            >
              <MenuItem value={10}>EDI</MenuItem>
            </Select>

            <div style={{ color: 'red' }}>
              {props.rowData.Project === '' ? 'Select a value' : ''}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.Project}</div>;
      },
    },
    {
      title: 'Interface ',
      field: 'Interface',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <Select
              margin="normal"
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            >
              <MenuItem value={10}>Amazon</MenuItem>
            </Select>

            <div style={{ color: 'red' }}>
              {props.rowData.Interface === '' ? 'Select a value' : ''}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.Interface}</div>;
      },
    },

    {
      title: 'Source Profile ',
      field: 'SourceProfile',
      lookup: {
        0: 'Amazon',
        1: 'Canfor',
        2: 'Cloudgen',
        3: 'Walmart',
        4: 'Google',
        5: 'Yahoo',
      },
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <Select
              margin="normal"
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            >
              <MenuItem value={10}>Amazon</MenuItem>
              <MenuItem value={20}>Google</MenuItem>
              <MenuItem value={30}>Yahoo</MenuItem>
            </Select>

            <div style={{ color: 'red' }}>
              {props.rowData.SourceProfile === '' ? 'Select a value' : ''}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.SourceProfile}</div>;
      },
    },

    {
      title: 'Target Profile ',
      field: 'TargetProfile',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <Select
              margin="normal"
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            >
              <MenuItem value={10}>Amazon</MenuItem>
            </Select>

            <div style={{ color: 'red' }}>
              {props.rowData.TargetProfile === '' ? 'Select a value' : ''}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.TargetProfile}</div>;
      },
    },

    {
      title: 'Document ',
      field: 'DocType',

      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <Select
              margin="normal"
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            >
              <MenuItem value={10}>Amazon</MenuItem>
            </Select>

            <div style={{ color: 'red' }}>
              {props.rowData.DocType === '' ? 'Select a value' : ''}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.DocType}</div>;
      },
    },

    {
      title: 'Service ',
      field: 'Service',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <Select
              margin="normal"
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            >
              <MenuItem value={10}>Amazon</MenuItem>
            </Select>
            <div style={{ color: 'red' }}>
              {props.rowData.Service === '' ? 'Select a value' : ''}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.Service}</div>;
      },
    },

    {
      title: 'Created',
      field: 'CreatedOn',
      editable: 'never',
      render: (rowData) => {
        console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.CreatedOn);
        } else {
          return;
        }
      },
    },
    {
      title: 'Last Updated',
      field: 'lastUpdatedOn',
      editable: 'never',
      render: (rowData) => {
        console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.lastUpdatedOn);
        } else {
          return;
        }
      },
    },
  ];

  useEffect(() => {
    if (data !== undefined) {
      console.log(data.organisationInterface[0]);
      setOrgInterface(data.organisationInterface);

      /* console.log("data",data.organisationInterface[0].OrganizationUUID,
      orgData[data.organisationInterface[0].OrganizationUUID],partnerData[data.organisationInterface[0].sourceProfileUUID],
      interfaceData[data.organisationInterface[0].interfaceUUID],projectData[data.organisationInterface[0].projectUUID],
      documentTypeData[data.organisationInterface[0].docTypeUUID],partnerData[data.organisationInterface[0].targetProfileUUID],
      interfaceData[data.organisationInterface[0].interfaceUUID],projectData[data.organisationInterface[0].projectUUID],
    )*/
    }
    if (OrgData !== undefined) {
      var OrgdropDown = OrgData.fetchOrganization1.reduce(
        (obj, item) => (
          (obj[item.organizationUUID] = item.OrganizationName), obj
        ),
        {}
      );
      console.log(OrgdropDown);
      setOrgData(OrgdropDown);
    }
    /*  if(OrgData!==undefined){
     var obj = OrgData.fetchOrganization1.reduce(function(acc, cur, i) {
        acc[cur.OrganizationUUID] = cur.OrganizationName;
        return acc;
        }, {});
        setOrgData(obj)
    }*/

    if (PartnerData !== undefined) {
      var PartnerDataDD = PartnerData.allPartners1.reduce(
        (obj, item) => (
          (obj[item.PartnerProfileUUID] = item.CorporationName), obj
        ),
        {}
      );
      console.log(PartnerDataDD);
      setPartnerData(PartnerDataDD);
    }
    if (InterfaceData !== undefined) {
      var InterfaceDataDD = InterfaceData.fetchInterface1.reduce(
        (obj, item) => ((obj[item.interfaceUUID] = item.name), obj),
        {}
      );
      console.log(InterfaceDataDD);
      setInterfaceData(InterfaceDataDD);
    }
    if (ProjectData !== undefined) {
      var ProjectDataDD = ProjectData.fetchProject1.reduce(
        (obj, item) => ((obj[item.projectUUID] = item.ProjectName), obj),
        {}
      );
      console.log(ProjectDataDD);
      setProjectData(ProjectDataDD);
    }
    if (DocumentTypeData !== undefined) {
      var DocumentTypeDataDD = DocumentTypeData.fetchDocumentType1.reduce(
        (obj, item) => ((obj[item.documentTypeUUID] = item.name), obj),
        {}
      );
      console.log(DocumentTypeDataDD);
      setDocumentTypeData(DocumentTypeDataDD);
    }
    if (ServiceData !== undefined) {
      var ServiceDataDD = ServiceData.fetchService1.reduce(
        (obj, item) => ((obj[item.serviceUUID] = item.name), obj),
        {}
      );
      console.log(ServiceDataDD);
      setServiceData(ServiceDataDD);
    }
  }, [
    OrgData,
    PartnerData,
    InterfaceData,
    ProjectData,
    DocumentTypeData,
    ServiceData,
    data,
  ]);
  const [state, setState] = React.useState({
    data: [],
  });
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const classes = makeStyles(iconStyles)();
  const tableIcons = {
    Add: forwardRef((props, ref) => (
      <AddBox color="primary" {...props} ref={ref} />
    )),
    Check: forwardRef((props, ref) => (
      <Check color="primary" {...props} ref={ref} />
    )),
    Clear: forwardRef((props, ref) => (
      <Clear color="primary" color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline color="primary" color="error" {...props} ref={ref} />
    )),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => (
      <Edit
        color="primary"
        {...props}
        ref={ref}
        onClick={console.log('hello world')}
      />
    )),
    Export: forwardRef((props, ref) => (
      <SaveAlt color="primary" {...props} ref={ref} />
    )),
    Filter: forwardRef((props, ref) => (
      <FilterList color="primary" {...props} ref={ref} />
    )),
    FirstPage: forwardRef((props, ref) => (
      <FirstPage color="primary" {...props} ref={ref} />
    )),
    LastPage: forwardRef((props, ref) => (
      <LastPage color="primary" {...props} ref={ref} />
    )),
    NextPage: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => (
      <Search color="primary" {...props} ref={ref} />
    )),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiTypography: {
          h6: {
            fontSize: 14,
            fontFamily: 'Arial !important',
          },
        },

        MuiSelect: {
          select: {
            minWidth: 10,
          },
        },

        MuiDialogTitle: {
          root: {
            backgroundColor: '#0b153e',
            color: '#FFFFFF',
          },
        },
        MuiGrid: {
          container: {
            flexWrap: 'nowrap',
          },
        },
      },
    });
  const classes1 = useStyles();
  function handleAddOrgInterface(newData) {
    return new Promise((resolve) => {
      console.log(newData);
      addOrgInterface({
        variables: {
          input: {
            projectUUID: newData.projectUUID,
            sourceProfileUUID: newData.sourceProfileUUID,
            targetProfileUUID: newData.targetProfileUUID,
            docTypeUUID: newData.docTypeUUID,
            serviceUUID: newData.serviceUUID,
            interfaceUUID: newData.interfaceUUID,
            organizationUUID: newData.organizationUUID,
          },
        },
      });

      if (ioloading) {
        console.log('OrgInterface cannot created');
      } else {
        console.log('OrgInterface created successfully');
        handleRefetch();
      }
    });
  }
  const handleRefetch = () => {
    refetch({
      updateData(_, data) {
        return data;
      },
    });
  };
  const handleOpen = (e) => {
    setProjectForm(true);
  };
  const CloseProject = () => {
    setProjectForm(false);
  };
  const handleUpdateOrgInterface = (newData) => {};
  const handleDeleteOrgInterface = (newData) => {};

  if (ioloading || iodloading) return <Loader />;
  if (ioerror || ioderror) return <Error />;
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <MaterialTable
        icons={tableIcons}
        title="Organization Interface"
        columns={columns}
        data={orgInterface}
        editable={{
          onRowAdd: (newData) => handleAddOrgInterface(newData),
          onRowUpdate: (newData, oldData) => handleUpdateOrgInterface(newData),
          onRowDelete: (oldData) => handleDeleteOrgInterface(oldData),
        }}
        actions={[
          {
            icon: AddCircleIcon,
            tooltip: 'Add Project',
            onClick: (event) => handleOpen(event),
          },
        ]}
        options={{
          headerStyle: {
            textAlign: 'center',
            fontSize: 12,
            fontWeight: 'bold',
            fontFamily: 'Arial !important',
            backgroundColor: '#0b153e',
            color: '#ffffff',
            padding: '4px',
          },

          searchFieldStyle: {
            color: '#0b153e',
          },
          actionsColumnIndex: -1,
        }}
      />
      {/* <ModalFab title="Organisation">
        <OrganisationModal />
      </ModalFab>*/}
      <ModalFab title="OrganizationInterface">
        <OrgInterfaceModelForm />
      </ModalFab>
      <ProjectModelForm
        handleCloseModal={CloseProject}
        isModalOpen={fromopen}
        title={'Add Project Form'}
      >
        <ProjectForm handleCloseModal={CloseProject} isModalOpen={fromopen} />
      </ProjectModelForm>
    </MuiThemeProvider>
  );
}
