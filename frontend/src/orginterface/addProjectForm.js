import React, { useEffect, useContext, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { Grid, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { withStyles } from '@material-ui/core/styles';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import Theme from '../css/theme';
import Modal from '@material-ui/core/Modal';
import {
  createMuiTheme,
  MuiThemeProvider
} from '@material-ui/core/styles';
function rand() {
  return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles(theme => ({  
     
}));
export default function ProjectFrom(props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);
  const [values, setValues] = React.useState({
    ProjectName: '',
    Description: '',
  });
  const [ErrMsg, setErrMsg] = React.useState(false);
  const [errorInfo, setErrorInfo] = React.useState({
    ProjectName: false,
    Description: false,
  });
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [value, setValue] = React.useState('IS');
  const [variant, setSnackbarVariant] = React.useState('error');
  const [message, setSnackbarMessage] = React.useState();
  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }
  const handleChange = (name) => (event) => {
    setErrMsg(false);
    setValues({ ...values, [name]: event.target.value });
    switch (name) {
      case 'ProjectName':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match('^([a-zA-Z])[a-zA-Z]*$') &&
          event.target.value.length <= 25
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'Description':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match('^([a-zA-Z])[a-zA-Z]*$') &&
          event.target.value.length <= 25
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      default:
        return false;
    }
  };
  const handleSubmit = async () => {
    console.log(values);
     setErrMsg(false);
    if (
      values.ProjectName === '' ||
      values.Description === '' 
    ) {
      setErrMsg(true);
    } else {
      setErrMsg(false);
      setValues({
        ...values,
        ProjectName: '',
        Description:'',
      });
    }
  };

  const handlereset = () => {
    setErrMsg(false);
    setErrorInfo({
      ProjectName: false,
      Description: false,
    });
    setValues({
      ...values,
      ProjectName: '',
      Description: '',
    });
  };
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSvgIcon:{
          root:{
         color:"#FFFFFF"
        }},
        MuiDialogTitle: {
            root: {
              backgroundColor: '#0b153e',
              color: '#FFFFFF'
            }
          },}});
          const handleModalClose = () => {
            props.handleCloseModal(false);
          };
return (
        
    <div>
        <MuiThemeProvider theme={getMuiTheme()}>
        <Grid container className="header">
          <Grid item xs={10} sm={10}>
            <h2 className="h2">Add Project Form</h2>
          </Grid>
          <Grid item xs={2} sm={2} className="close">
            <CloseIcon onClick={handleModalClose} />
          </Grid>
        </Grid>
      <div>
    {ErrMsg === true ? (
  <div style={{ textAlign: 'center' }} className="addfieldserror">
    Please fill the fields
  </div>
  ) : (
  ''
     )}
  <form className="content" noValidate autoComplete="off">
  <Grid className="configGrid">
   <Grid container spacing={2}>  
   <Grid
                item
                xs={6}
                sm={6}
                gutterBottom
                variant="body2"
                color="textSecondary"
                className="text"
              >
        <TextField
          id="outlined-dense"
          label="ProjectName "
          className="textField"
          margin="dense"
          onChange={handleChange('ProjectName')}
          value={values.ProjectName}
          variant="outlined"
          name="ProjectName"
        />
        {errorInfo.ProjectName === true ? (
          <div id="nameid" className="addfieldserror">
            "Allowed alphabets,no special characters and Numerics are
            allowed.Maximum length is 25."
          </div>
        ) : (
          ''
        )}
        </Grid>
         <Grid
                item
                xs={6}
                sm={6}
                gutterBottom
                variant="body2"
                color="textSecondary"
                className="text"
              >
        <TextField
          id="outlined-dense"
          label="Description "
          className="textField"
          onChange={handleChange('Description')}
          value={values.Description}
          margin="dense"
          variant="outlined"
          name="Description"
        />
         {errorInfo.Description === true ? (
          <div id="nameid" className="addfieldserror">
            "Allowed alphabets,no special characters and Numerics are
            allowed.Maximum length is 25."
          </div>
        ) : (
          ''
        )}
      </Grid>
              <Grid
                item
                xs={6}
                sm={6}
                gutterBottom
                variant="body2"
                color="textSecondary"
                className="text"
              >
                  <Button
                    style={{
                        color: '#000006',
                        backgroundColor:'#E5CCFF' 
                     }}
                    variant="contained"
                    //size="Large"
                    className="createpartnerbutton"
                    onClick={handlereset}
              >
                  RESET
                </Button>
            </Grid>
            <Grid
                item
                xs={6}
                sm={6}
                gutterBottom
                variant="body2"
                color="textSecondary"
                className="text"
              >
                <Button
                    variant="contained"
                  // size="Large"
                    className="createpartnerbutton"
                    onClick={handleSubmit}
                  >
                    SAVE
                  </Button>
                </Grid>
                </Grid>
                </Grid>

      </form>
      </div>   
      </MuiThemeProvider>
     </div>
  )
}