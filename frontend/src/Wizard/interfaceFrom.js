import React, { useEffect, useContext, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid';
const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing(2)
  }
}));
export default function Switches() {
  const [values, setValues] = React.useState({
    Name: '',
   Description: '',
 });
 const [ErrMsg, setErrMsg] = React.useState(false);
 const [errorInfo, setErrorInfo] = React.useState({
  Name: false,
   Description: false,
 });
 const [openSnackbar, setOpenSnackbar] = React.useState(false);
 const [value, setValue] = React.useState('IS');
 const [variant, setSnackbarVariant] = React.useState('error');
 const [message, setSnackbarMessage] = React.useState();

 function handleCloseSnackbar(event, reason) {
   if (reason === 'clickaway') {
     return;
   }
   setOpenSnackbar(false);
 }
const handleChange = name => event => {
   setErrMsg(false);
   setValues({ ...values, [name]: event.target.value });
   switch (name) {
     case 'Name':
       if (event.target.value.length === 0) {
         setValues({ ...values, [name]: '' });
         setErrorInfo({ ...errorInfo, [name]: false });
       } else if (
         event.target.value.match('^([a-zA-Z])[a-zA-Z]*$') &&
         event.target.value.length <= 25
       ) {
         setErrorInfo({ ...errorInfo, [name]: false });
         setValues({ ...values, [name]: event.target.value });
       } else {
         setErrorInfo({ ...errorInfo, [name]: true });
       }
       break;
     case 'Description':
       if (event.target.value.length === 0) {
         setValues({ ...values, [name]: '' });
         setErrorInfo({ ...errorInfo, [name]: false });
       } else if (
         event.target.value.match('^([a-zA-Z])[a-zA-Z]*$') &&
         event.target.value.length <= 25
       ) {
         setErrorInfo({ ...errorInfo, [name]: false });
         setValues({ ...values, [name]: event.target.value });
       } else {
         setErrorInfo({ ...errorInfo, [name]: true });
       }
       break;
     default:
       return false;
   }
 };
 const handleSubmit = async () => {
   console.log(values);
   setErrMsg(false);
   if (
     values.Name === '' ||
     values.Description === '' 
    
   ) {
     setErrMsg(true);
   } else {
     setErrMsg(false);
     setValues({
       ...values,
       Name: '',
       Description: ''
     });
   }
 };
 return (
  
  <form className="content" noValidate autoComplete="off">
  <Grid container spacing={2}>  
    <Grid
            item
            xs={6}
            sm={6}
            gutterBottom
            variant="body2"
            color="textSecondary"
            className="text"
          >
    <TextField
      id="outlined-dense"
      label="Name "
      className="textField"
      margin="dense"
      onChange={handleChange('Name')}
      value={values.Name}
      variant="outlined"
      name="Name"
    />
    {errorInfo.Name === true ? (
      <div id="nameid" className="addfieldserror">
        "Allowed alphabets,no special characters and Numerics are
        allowed.Maximum length is 25."
      </div>
    ) : (
      ''
    )}
    </Grid>
     <Grid
            item
            xs={6}
            sm={6}
            gutterBottom
            variant="body2"
            color="textSecondary"
            className="text"
          >
    <TextField
      id="outlined-dense"
      label="Description "
      className="textField"
      onChange={handleChange('Description')}
      value={values.Description}
      margin="dense"
      variant="outlined"
      name="Description"
    />
     {errorInfo.Description === true ? (
      <div id="nameid" className="addfieldserror">
        "Allowed alphabets,no special characters and Numerics are
        allowed.Maximum length is 25."
      </div>
    ) : (
      ''
    )}
</Grid>
</Grid>     

  </form>
 
);
}
