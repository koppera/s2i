import React, { useReducer, createContext } from "react";
;

export const myContext = createContext();

export default function Store(props) {
  const counter = 0;
  const [state, dispatch] = useReducer((state, action) => {
    return state + action;
  }, counter);
  return (
    <myContext.Provider value={{ state, dispatch }}>
      {props.children}
    </myContext.Provider>
  );
}