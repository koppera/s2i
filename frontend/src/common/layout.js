import React, { useEffect, useContext } from 'react';
import PropTypes from 'prop-types';
import { ClientContext } from 'graphql-hooks';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import DescriptionIcon from '@material-ui/icons/Description';
import MenuIcon from '@material-ui/icons/Menu';
import GetApp from '@material-ui/icons/GetApp';
import GamepadIcon from '@material-ui/icons/Gamepad';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import StorageIcon from '@material-ui/icons/Storage';
import PersonPinCircleIcon from '@material-ui/icons/PersonPinCircle';
import CompareArrowsIcon from '@material-ui/icons/CompareArrows';
import BusinessIcon from '@material-ui/icons/Business';
import AssignmentIcon from '@material-ui/icons/Assignment';
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import RecentActorsIcon from '@material-ui/icons/RecentActors';
import ImportContactsIcon from '@material-ui/icons/ImportContacts';
import DevicesOtherIcon from '@material-ui/icons/DevicesOther';
import BugReportIcon from '@material-ui/icons/BugReport';
import PresentToAllIcon from '@material-ui/icons/PresentToAll';
import VerticalSplitIcon from '@material-ui/icons/VerticalSplit';
import StreetviewIcon from '@material-ui/icons/Streetview';
import SupervisedUserCircleIcon from '@material-ui/icons/SupervisedUserCircle';
import FaceIcon from '@material-ui/icons/Face';
import WebIcon from '@material-ui/icons/Web';
import SpeakerGroupIcon from '@material-ui/icons/SpeakerGroup';
import DesktopMacRoundedIcon from '@material-ui/icons/DesktopMacRounded';
import GroupAddIcon from '@material-ui/icons/GroupAdd';
import ContactsIcon from '@material-ui/icons/Contacts';
import PermContactCalendarIcon from '@material-ui/icons/PermContactCalendar';
import PermIdentityIcon from '@material-ui/icons/PermIdentity';
import HomeWorkIcon from '@material-ui/icons/HomeWork';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
import DeviceHubIcon from '@material-ui/icons/DeviceHub';
import PhoneIcon from '@material-ui/icons/Phone';
import LanguageIcon from '@material-ui/icons/Language';
import TrackChangesIcon from '@material-ui/icons/TrackChanges';
import DashboardIcon from '@material-ui/icons/Dashboard';
import SettingsInputAntennaRoundedIcon from '@material-ui/icons/SettingsInputAntennaRounded';
import ContactPhoneRoundedIcon from '@material-ui/icons/ContactPhoneRounded';
import FormatListNumberedRtlSharpIcon from '@material-ui/icons/FormatListNumberedRtlSharp';
import MySnackbarContentWrapper from '../components/Snackbar';
import SettingsEthernetIcon from '@material-ui/icons/SettingsEthernet';
import FetchUserOrganization from '../loginPage/fetchUserOrganization';
import PeopleIcon from '@material-ui/icons/People';
import CardMembershipIcon from '@material-ui/icons/CardMembership';
import {
  makeStyles,
  useTheme,
  MuiThemeProvider,
  createMuiTheme, 
} from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import Badge from '@material-ui/core/Badge';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import DesktopMac from '@material-ui/icons/DesktopMac';
import NotificationsIcon from '@material-ui/icons/Notifications';
import MoreIcon from '@material-ui/icons/MoreVert';
import PersonIcon from '@material-ui/icons/Person';
import DnsIcon from '@material-ui/icons/Dns';
import PortableWifiOffOutlinedIcon from '@material-ui/icons/PortableWifiOffOutlined';
import { withRouter } from 'react-router-dom';
import SettingsApplications from '@material-ui/icons/SettingsApplications';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import Snackbar from '@material-ui/core/Snackbar';
import Theme from '../css/theme';
import Tooltip from '@material-ui/core/Tooltip';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import Grid from '@material-ui/core/Grid';
import { BottomNavigationAction } from '@material-ui/core';
import PeopleOutlineIcon from '@material-ui/icons/PeopleOutline';
import '../css/commonStyle.css';
import Collapse from '@material-ui/core/Collapse';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import SendIcon from '@material-ui/icons/Send';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import StarBorder from '@material-ui/icons/StarBorder';
import ListSubheader from '@material-ui/core/ListSubheader';
import AccountTreeIcon from '@material-ui/icons/AccountTree';
import { render } from 'react-dom';
import LoginForm from '../loginPage/loginPage';
import { Redirect } from 'react-router';
import UserReducer from '../reducers/userReducer';
import UserOrgReducer from '../reducers/userOrgReducer';
import AuthContext from '../authUtil/Auth';
import WorkOutlineOutlinedIcon from '@material-ui/icons/WorkOutlineOutlined';
import VideocamIcon from '@material-ui/icons/Videocam'
import TurnedInNotIcon from '@material-ui/icons/TurnedInNot';
import HelpIcon from '@material-ui/icons/Help';
import AccountBoxRoundedIcon from '@material-ui/icons/AccountBoxRounded';
import ControlPointDuplicateOutlinedIcon from '@material-ui/icons/ControlPointDuplicateOutlined';
import ListAltIcon from '@material-ui/icons/ListAlt';
import LinearScaleIcon from '@material-ui/icons/LinearScale';
import PoolIcon from '@material-ui/icons/Pool';
import CastConnectedIcon from '@material-ui/icons/CastConnected';
import TimelineIcon from '@material-ui/icons/Timeline';
import ReactDOM from 'react-dom';
//import Draggable from 'react-draggable';



//export const AuthContext = React.createContext(); // added this

const drawerWidth = 190;
const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  appBar: {
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
    },
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  // necessary for content to be below app bar
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: 0,
  },

  links: {
    textDecoration: 'none',
  },

  drawerPaper: {
    width: drawerWidth,
    color: '#fff',
    overflowX: 'hidden',
  },

  nested: {
    paddingLeft: theme.spacing(0.7),
  },
}));

const getMuiTheme = () =>
  createMuiTheme(Theme, {
    overrides: {
      MuiListItem: {
        root: {
          display: 'flex',
          position: 'relative',
          boxSizing: 'borderBox',
          textAlign: 'left',

          /* padding-top: 2px; */
          /* padding-bottom: 5px; */

          textDecoration: 'none',
          paddingTop: 0,
          paddingBottom: 0,
        },
      },
      MuiTypography: {
        body1: {
          fontSize: '.8rem',
          fontFamily: 'Roboto Helvetica Arial sans-serif',
          fontWeight: 400,
          /* lineHeight: 1.5; */
          letterSpacing: '0.00938em',
        },
        displayBlock: {
          color: '#FFFFFF',
        },
      },
      MuiSvgIcon: {
        root: {
          marginRight: '10%',
        },
      },

      MuiListItemIcon: {
        root: {
          minWidth: '10px',
        },
      },
    },
  });

const ITEM_HEIGHT = 48;

function ResponsiveDrawer(props) {
  const [loginState, loginDispatch] = React.useReducer(UserReducer, {
    loginUserdata: [],
  });
  const [userOrgState, userOrgDispatch] = React.useReducer(UserOrgReducer, {
    userOrgdata: [],
  });
  console.log(
    'proceed',
    userOrgState.proceed,
    "localStorage.getItem('loginType')==='User'||loginState.loginType",
    loginState.loginType
  );

  /*const [otherAppState, otherAppDispatch] = useReducer(OtherAppReducer, {
    anotherThing: []
  });*/
  const client = useContext(ClientContext);
  //const { loginDispatch } = React.useContext(AuthContext);

  console.log('clientIndxLy', client);
  console.log(props);
  const { container } = props;
  const classes = useStyles();
  const theme = useTheme();
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);
  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);
  let [isProfileView, setProfileView] = React.useState(false);
  const [sideMenuState, setSideMenuState] = React.useState(true);
  let [selectedIdx, setSelectedIdx] = React.useState(true);
  let [selectAdmin, setSelectAdmin] = React.useState(false);
  let [selectedList, setSelectedList] = React.useState(true);
  const [open, setOpen] = React.useState(false);
  const [openPartners, setOpenPartners] = React.useState(true);
  const [openProcessingrole, setOpenProcesingrole] = React.useState(true);
  const [openUser, setOpenUser] = React.useState(true);
  const [openType, setOpenType] = React.useState(true);
  const [openProtocols, setOpenProtocols] = React.useState(true);
  const [openArcesb, setArcesb] = React.useState(false);
  const [openFlows, setFlows] = React.useState(true);
  const [checkLoginType, setCheckLoginType] = React.useState(true);
  const [openDocumentation,setOpenDocumentation]=React.useState(true);
  const [openProfile,setOpenProfile]=React.useState(true);
  const [openFlowsMenu,setOpenFlowsMenu] = React.useState(false);
  const [openConnnector,setOpenConnnector] = React.useState(false);
  
  const currentLocation = props.location.pathname;
  //localStorage.removeItem('token');
  // let checkToken= localStorage.getItem('token');
  //console.log("checkToken",checkToken)

  useEffect(() => {
    console.log(
      'localStorage.getItem(loginType)',
      localStorage.getItem('loginType')
    );
    if (localStorage.getItem('loginType') === 'Arcesb') {
      console.log(
        'localStorage.getItem(loginType)',
        localStorage.getItem('loginType')
      );
      props.history.push('/workspace');
    }
  });

  const loginSuccess = (val) => {
    console.log('val', val);
    // setAuthenticated(val)
    // checkToken= localStorage.getItem('token');
    //  console.log("checkToken",checkToken)
    props.history.push('/');
  };
  const handleLogout = () => {
    console.log('Logout', 'props', props);
    // event.preventDefault();
    localStorage.removeItem('token');
    localStorage.clear();
    userOrgDispatch({
      type: 'FETCH_USER_ORG_LOGOUT',
      payload: '',
    });
    loginDispatch({
      type: 'LOGOUT',
      payload: '',
    });
    console.log('Logouttoken', localStorage.getItem('token'));
    {
      /*<Redirect to='/' />;*/
    }
    //props.history.push('/')
  };
  const handleClick = () => {
    setOpen(!open);
  };

  const handleFlowsMenu = () => {
    setOpenFlowsMenu(!openFlowsMenu);
  };
 
  function handleDrawerToggle() {
    setMobileOpen(!mobileOpen);
  }
  function handleProfileMenuOpen(event) {
    setAnchorEl(event.currentTarget);
  }

  function handleMobileMenuClose() {
    setMobileMoreAnchorEl(null);
  }

  function handleMenuClose() {
    setAnchorEl(null);
    handleMobileMenuClose();
  }

  function handleProfileView() {
    setProfileView(true);
  }
  const handleMenu = (item, menuText) => {
    console.log(item, 'item value');
    if (item == 'Partners') {
      setOpenPartners(true);
      setSideMenuState((prevState) => ({ [item]: !prevState[item] }));
      setOpenUser(false);
      setOpenProcesingrole(false);
      setOpenProtocols(false);
      setOpenType(false);
      setArcesb(false);
      setOpenDocumentation(false);
      setOpenProfile(false);
      setOpenFlowsMenu(false);
      setOpenConnnector(false);
    } else if (item == 'Processing Rules') {
      setOpenProcesingrole(true);
      setSideMenuState((prevState) => ({ [item]: !prevState[item] }));
      setOpenPartners(false);
      setOpenUser(false);
      setOpenProtocols(false);
      setOpenType(false);
      setArcesb(false);
      setOpenDocumentation(false);
      setOpenProfile(false);
      setOpenFlowsMenu(false);
      setOpenConnnector(false);
    } else if (item == 'User') {
      setOpenUser(true);
      setSideMenuState((prevState) => ({ [item]: !prevState[item] }));
      setOpenPartners(false);
      setOpenProtocols(false);
      setOpenType(false);
      setOpenProcesingrole(false);
      setArcesb(false);
      setOpenDocumentation(false);
      setOpenProfile(false);
      setOpenFlowsMenu(false);
      setOpenConnnector(false);
    } else if (item == 'Type') {
      setOpenType(true);
      setSideMenuState((prevState) => ({ [item]: !prevState[item] }));
      setOpenPartners(false);
      setOpenProtocols(false);
      setOpenProcesingrole(false);
      setOpenUser(false);
      setArcesb(false);
      setOpenDocumentation(false);
      setOpenProfile(false);
      setOpenFlowsMenu(false);
      setOpenConnnector(false);
    } else if (item == 'Protocols') {
      setOpenProtocols(true);
      setSideMenuState((prevState) => ({ [item]: !prevState[item] }));
      setOpenPartners(false);
      setOpenUser(false);
      setOpenType(false);
      setOpenProcesingrole(false);
      setArcesb(false);
      setOpenDocumentation(false);
      setOpenProfile(false);
      setOpenFlowsMenu(false);
      setOpenConnnector(false);
    } else if (item == 'Arcesb') {
      setArcesb(!openArcesb);
      setSideMenuState((prevState) => ({ [item]: !prevState[item] }));
      setOpenPartners(false);
      setOpenUser(false);
      setOpenType(false);
      setOpenProtocols(false);
      setOpenProcesingrole(false);
      setOpenFlowsMenu(false);
      setOpenConnnector(false);
    } else if (item == 'Documentation') {
      setOpenDocumentation(true);
      setFlows(false);
      setArcesb(true);
      setSideMenuState((prevState) => ({ [item]: !prevState[item] }));
      setOpenPartners(false);
      setOpenUser(false);
      setOpenType(false);
      setOpenProtocols(false);
      setOpenFlowsMenu(false);
      setOpenConnnector(false);
      setOpenProfile(false);
      setOpenProcesingrole(false);}
      else if (item == 'Profile') {
        setOpenProfile(true);
        setFlows(false);
        setArcesb(true);
        setSideMenuState((prevState) => ({ [item]: !prevState[item] }));
        setOpenPartners(false);
        setOpenUser(false);
        setOpenType(false);
        setOpenProtocols(false);
        setOpenProcesingrole(false);
        setOpenFlowsMenu(false);
        setOpenConnnector(false);
        setOpenDocumentation(false);
    } else if (item == 'Flows') {
      setFlows(true);
      setArcesb(true);
      setOpenFlowsMenu(!openFlowsMenu);
      setSideMenuState((prevState) => ({ [item]: !prevState[item] }));
      setOpenPartners(false);
      setOpenUser(false);
      setOpenType(false);
      setOpenProtocols(false);
      setOpenProcesingrole(false);
      setOpenDocumentation(false);
      setOpenProfile(false);
      setOpenConnnector(false);
    } else if (item == 'Connector') {
      setOpenConnnector(true);
      setOpenFlowsMenu(true);
      setArcesb(false);
      setSideMenuState((prevState) => ({ [item]: !prevState[item] }));
      setOpenPartners(false);
      setOpenUser(false);
      setOpenType(false);
      setOpenProtocols(false);
      setOpenProcesingrole(false);
      setOpenDocumentation(false);
      setOpenProfile(false);
    } else if (item == 0) {
      setSideMenuState((prevState) => ({ [item]: !prevState[item] }));
      setOpenPartners(false);
      setOpenUser(false);
      setOpenType(false);
      setOpenProtocols(false);
      setOpenProcesingrole(false);
      setArcesb(false);
      setOpenFlowsMenu(false);
      setOpenConnnector(false);
    } 
    else if (item == 2) {
      setSideMenuState((prevState) => ({ [item]: !prevState[item] }));
      setOpenPartners(false);
      setOpenUser(false);
      setOpenType(false);
      setOpenProtocols(false);
      setOpenProcesingrole(false);
      setArcesb(true);
      setOpenFlowsMenu(false);
      setOpenConnnector(false);
    }
    if (menuText == 'Logout') {
      handleLogout();
    }
  };

  const menuId = 'primary-search-account-menu';
  const mobileMenuId = 'primary-search-account-menu-mobile';
  const MenuLists = [
    {
      link: '/organisation',
      icon: <BusinessIcon style={{ fontSize: 20 }} />,
      text: 'Organization',
      mainMenu: 0,
    },
    {
      text: 'User',
      children: [
        {
          link: '/applicationusers',
          icon: (
            <DevicesOtherIcon style={{ paddingLeft: '8px', fontSize: 20 }} />
          ),
          text: 'Application User',
          mainMenu: 1,
        },

        {
          link: '/userroleassignment',
          icon: (
            <AssignmentIndIcon
              style={{ paddingLeft: '8px', fontSize: 20, marginBottom: '25%' }}
            />
          ),
          text: 'User Role Assignment',
          mainMenu: 1,
        },
        {
          link: '/userroletype',
          icon: (
            <RecentActorsIcon style={{ paddingLeft: '8px', fontSize: 20 }} />
          ),
          text: 'User Role Type',
          mainMenu: 1,
        },

        {
          link: '/usertypes',
          icon: (
            <SupervisedUserCircleIcon
              style={{ paddingLeft: '8px', fontSize: 20 }}
            />
          ),
          text: 'User Type',
          mainMenu: 1,
        },
        {
          link: '/user',
          icon: <GroupAddIcon style={{ paddingLeft: '8px', fontSize: 20 }} />,
          text: 'Users',
          mainMenu: 1,
        },
      ],
    },

    /* {
   link: '/certificatesDetailsPage',
   icon: <AddCircleOutlineIcon style={{fontSize:20}}/>,
   text: 'Serial Number',
},*/
  ];

  

 
  const flowsmenu = [
    {
      text: 'Connector',
    
      icon: <TimelineIcon />,
      children:[
        
        { 
          link: '/x12ConnectorTable',
          icon: <SettingsEthernetIcon style={{paddingLeft:'30px',fontSize:20}}/>,
          text: 'X12',
          mainMenu:1
        }
      ]
    }, 
  ];

  const arcesb = [
    /*
        {
          link: '/profiles',
          icon: <PersonIcon style={{fontSize:20}}/>,
          text: 'Profiles',
          mainMenu:2
          
        },
        
        {
          text: 'Flows',

          icon: <PresentToAllIcon style={{fontSize:20}}/>,
          children:[
            {
              link: '/connectors',
              icon: <SettingsEthernetIcon style={{paddingLeft:'8px',fontSize:20}}/>,
              text: 'Connectors',
              mainMenu:1
            }

          ]
        },  
        */

    {
      link: '/',
      icon: <WorkOutlineOutlinedIcon style={{ fontSize: 20 }} />,
      text: 'Partner',
      mainMenu: 2,
    },
    {
      link: '/arcESBMonitoring',
      icon: <DesktopMacRoundedIcon style={{ fontSize: 20 }} />,
      text: 'Monitoring',
      mainMenu: 2,
    },
    {
      link: '/documentTypeTablearcesb',
      icon: <DescriptionIcon style={{ fontSize: 20 }} />,
      text: 'Document Types',
      mainMenu: 2,
    },
    {
      text: 'Documentation',
 
      icon: < AssignmentIcon  style={{fontSize:20}}/>,
      children:[
        {
          link: '/workflowDocumentation',
          icon: <AccountTreeIcon style={{paddingLeft:'8px',fontSize:20}}/>,
          text: 'WorkFlow Tutorial',
          mainMenu:1
        },
        {
          link: '/video',
          icon: <VideocamIcon style={{paddingLeft:'8px',fontSize:20}}/>,
          text: 'Videos',
          mainMenu:1
        },
        {
          link: '/support',
          icon: <HelpIcon style={{paddingLeft:'8px',fontSize:20}}/>,
          text: 'Support',
          mainMenu:1
        },
 
      ]
    },
    {
      text: 'Profile',
      icon: < AccountBoxRoundedIcon  style={{fontSize:20}}/>,
      children:[
        {
          link: '/aSTwoProfile',
          icon: <PeopleIcon style={{paddingLeft:'8px',fontSize:20}}/>,
          text: 'AS2Profile',
          mainMenu:1
        },
       
        {
          link: 'qualifiersTable',
          icon: <TurnedInNotIcon style={{paddingLeft:'8px',fontSize:20}}/>,
          text: 'Qualifiers',
          mainMenu:1
        },
        {
          link: '/controlVersion',
          icon: <ControlPointDuplicateOutlinedIcon style={{paddingLeft:'8px', fontSize: 20 }} />,
          text: 'ControlVersion',
          mainMenu: 1,
        },
        {
          link: '/certificateTable',
          icon: <CardMembershipIcon style={{ paddingLeft:'8px',fontSize: 20 }} />,
          text: 'Certificates',
          mainMenu: 1,
        },
 
      ]
    },

  
    {
      link: '/',
      icon: <ExitToAppIcon style={{ fontSize: 20 }} />,
      text: 'Logout',
      mainMenu: 2,
    },
  ];

  const logoutMenu = [
    /*{
    text:'Interface',
    children:[]
  },*/
    {
      link: '/projecttable',
      icon: <StorageIcon style={{ fontSize: 20 }} />,
      text: 'Project',
      mainMenu: 0,
    },
    {
      text: 'Partners',
      children: [
        {
          link: '/',
          icon: <PersonIcon style={{ paddingLeft: '8px', fontSize: 20 }} />,
          text: 'Partner Profile',
          mainMenu: 1,
        },
        {
          link: '/addresstype',
          icon: (
            <ImportContactsIcon style={{ paddingLeft: '8px', fontSize: 20 }} />
          ),
          text: 'Address Type',
          mainMenu: 1,
        },
        {
          link: '/ContactType',
          icon: (
            <PermContactCalendarIcon
              style={{ paddingLeft: '8px', fontSize: 18 }}
            />
          ),
          text: 'Contact Type',
          mainMenu: 1,
        },
        {
          link: '/profilidtypestable',
          icon: (
            <SpeakerGroupIcon style={{ paddingLeft: '8px', fontSize: 18 }} />
          ),
          text: 'Profile Id Type',
          mainMenu: 1,
        },
      ],
    },
    {
      text: 'Processing Rules',
      children: [
        {
          link: '/interface',
          icon: <DeviceHubIcon style={{ paddingLeft: '8px', fontSize: 20 }} />,
          text: 'Interface',
          mainMenu: 1,
        },
        {
          link: '/orginterface',
          icon: <DashboardIcon style={{ paddingLeft: '8px', fontSize: 20 }} />,
          text: 'Organization Interface',
          mainMenu: 1,
        },
        {
          link: '/servicetable',
          icon: (
            <PortableWifiOffOutlinedIcon
              style={{ paddingLeft: '8px', fontSize: 18 }}
            />
          ),
          text: 'Service',
          mainMenu: 1,
        },
        {
          link: '/documenttypetable',
          icon: (
            <DescriptionIcon style={{ paddingLeft: '8px', fontSize: 18 }} />
          ),
          text: 'Document',
          mainMenu: 1,
        },
      ],
    },
    {
      text: 'Type',
      children: [
        {
          link: '/moduletypetable',
          icon: (
            <VerticalSplitIcon style={{ paddingLeft: '8px', fontSize: 20 }} />
          ),
          text: 'Module',
          mainMenu: 1,
        },
      ],
    },

    {
      text: 'Protocols',
      children: [
        {
          link: '/protocolType',
          icon: <BugReportIcon style={{ paddingLeft: '8px', fontSize: 18 }} />,
          text: 'Protocol Type',
          mainMenu: 1,
        },

        /*
        {
          link: '/webtransferprotocoltype',
          icon: <SettingsInputAntennaRoundedIcon />,
          text: 'Web Transfer Protocol Type',
        },
        
        {
        
          link: '/webtransferprotocol',
          icon: <LanguageIcon style={{fontSize:20}}/>,
          text: 'Web Transfer Protocol',
        },
        {
          link: '/fileprotocoltype',
          icon: <BugReportIcon />,
          text: 'File Protocol Type',
  
        },
        {
          link: '/filetransferprotocol',
          icon: <PresentToAllIcon style={{fontSize:20}}/>,
          text: 'File Transfer Protocol'
        },
      
        {
          link: '/protocolcertificate',
          icon: <AssignmentIcon style={{paddingLeft:'8px',fontSize:18}}/>,
          text: ' Protocol Certificate',
          mainMenu:1
        }, */
      ],
    },

    {
      link: '/monitoring',
      icon: <DesktopMacRoundedIcon style={{ fontSize: 20 }} />,
      text: 'Monitoring',
      mainMenu: 0,
    },
    {
      link: '/resourcesTable',
      icon: <AccountTreeIcon style={{ fontSize: 20 }} />,
      text: 'Resources',
      mainMenu: 0,
    },

    {
      link: '/Accounts',
      icon: <AccountCircleIcon style={{ fontSize: 20 }} />,
      text: 'Accounts',
      mainMenu: 0,
    },
   
    {
      link: '/',
      icon: <ExitToAppIcon style={{ fontSize: 20 }} />,
      text: 'Logout',
      mainMenu: 0,
    },
  ];
  const openMenu = (subOption) => {
    if (
      subOption.text == 'Partners' &&
      (currentLocation == '/' ||
        currentLocation == '/addresstype' ||
        currentLocation == '/ContactType' ||
        currentLocation == '/profilidtypestable')
    ) {
      return (
        <Collapse in={openPartners} timeout="auto" unmountOnExit>
          {handler(subOption.children)}
        </Collapse>
      );
    } else if (
      subOption.text == 'Processing Rules' &&
      (currentLocation == '/interface' ||
        currentLocation == '/orginterface' ||
        currentLocation == '/servicetable' ||
        currentLocation == '/documenttypetable')
    ) {
      return (
        <Collapse in={openProcessingrole} timeout="auto" unmountOnExit>
          {handler(subOption.children)}
        </Collapse>
      );
    } else if (
      subOption.text == 'User' &&
      (currentLocation == '/applicationusers' ||
        currentLocation == '/userroleassignment' ||
        currentLocation == '/userroletype' ||
        currentLocation == '/usertypes' ||
        currentLocation == '/user')
    ) {
      return (
        <Collapse in={openUser} timeout="auto" unmountOnExit>
          {handler(subOption.children)}
        </Collapse>
      );
    } else if (
      subOption.text == 'Type' &&
      currentLocation == '/moduletypetable'
    ) {
      return (
        <Collapse in={openType} timeout="auto" unmountOnExit>
          {handler(subOption.children)}
        </Collapse>
      );
    } else if (
      subOption.text == 'Protocols' &&
      currentLocation == '/protocolType'
      // ||currentLocation =='/protocolcertificate'
    ) {
      return (
        <Collapse in={openProtocols} timeout="auto" unmountOnExit>
          {handler(subOption.children)}
        </Collapse>
      );
    }else if (
      subOption.text == 'Documentation' &&
      (currentLocation =='/workflowDocumentation'|| 
      currentLocation == '/video'||
      currentLocation=='/support'
      )
    ) {
      return (
        <Collapse
         in={openDocumentation} 
         timeout="auto"
          unmountOnExit>
          {handler(subOption.children)}
        </Collapse>
      );
      
    } 
    else if (
      subOption.text == 'Profile' &&
      (currentLocation =='/aSTwoProfile'||
      currentLocation == '/qualifiersTable'||
      currentLocation == '/controlVersion'||
      currentLocation == '/certificateTable'
      
      )
    ) {
      return (
        <Collapse
         in={openProfile} 
         timeout="auto"
          unmountOnExit>
          {handler(subOption.children)}
        </Collapse>
      );
      
    }
    else if (
      subOption.text == 'FlowsMenu' &&
      (currentLocation == '/Connector'||
      currentLocation == '/x12ConnectorTable'
      )
    ) {
      return (
        <Collapse
         in={openFlowsMenu} 
         timeout="auto"
          unmountOnExit>
          {handler(subOption.children)}
        </Collapse>
      );
      
    } 
    else if (
      subOption.text == 'Connector' &&
      (currentLocation == '/x12ConnectorTable'
      )
    ) {
      return (
        <Collapse
         in={openConnnector} 
         timeout="auto"
          unmountOnExit>
          {handler(subOption.children)}
        </Collapse>
      );
      
    } 
    else if (
      subOption.text == 'Flows' &&
      (currentLocation == '/connectors' || currentLocation == '/logout')
    ) {
      return (
        <Collapse in={openFlows} timeout="auto" unmountOnExit>
          {handler(subOption.children)}
        </Collapse>
      );
    } else {
      return (
        <Collapse
          in={sideMenuState[subOption.text]}
          timeout="auto"
          unmountOnExit
        >
          {handler(subOption.children)}
        </Collapse>
      );
    }
  };
  const handler = (children) => {
    return children.map((subOption) => {
      if (!subOption.children) {
        console.log('one', subOption.text);
        console.log(
          '676',
          localStorage.getItem('loginType') === 'User' || loginState.loginType
        );
        return (
          <div key={subOption.text}>
            <Link to={subOption.link} className={classes.links}>
              <ListItem
                button
                selected={selectedList === subOption.text}
                key={subOption.text}
                onClick={() => {
                  setSelectedList(subOption.text);
                  handleMenu(subOption.mainMenu, subOption.text);
                }}
              >
                <ListItemIcon className="itemText">
                  {subOption.icon}
                </ListItemIcon>

                <ListItemText primary={subOption.text} className="itemText" />
              </ListItem>
            </Link>
          </div>
        );
      }
      return (
        <div key={subOption.text}>
          {console.log(
            '700',
            localStorage.getItem('loginType') === 'User' || loginState.loginType
          )}
          <ListItem
            button
            selected={selectedIdx === subOption.text}
            onClick={() => {
              console.log(subOption.text);
              handleMenu(subOption.text);
              setSelectedIdx(subOption.text);
            }}
          >
            {subOption.text == 'Documentation' ? (
              <ListItemIcon className="itemText">{subOption.icon}</ListItemIcon>
            ) : (
              ''
            )}
            {subOption.text == 'Profile' ? (
              <ListItemIcon className="itemText">{subOption.icon}</ListItemIcon>
            ) : (
              ''
            )}
            {subOption.text == 'Connector' ? (
              <ListItemIcon className="itemText">{subOption.icon}</ListItemIcon>
            ) : (
              ''
            )}

            <ListItemText className="subOption" primary={subOption.text} />
            {sideMenuState[subOption.text] ? (
              <ExpandLess style={{ marginRight: '10%' }} />
            ) : (
              <ExpandMore style={{ marginRight: '10%' }} />
            )}
          </ListItem>
          {openMenu(subOption)}
        </div>
      );
    });
  };
  console.log(currentLocation);
  const drawer = (
    <div>
      {localStorage.getItem('loginType') === 'Arcesb' ? (
        <div>
          {' '}
          <List
            component="nav"
            aria-labelledby="nested-list-subheader"
            className={classes.Drawer}
          >
            <ListItem
              button
              selected={selectAdmin}
              onClick={() => {
                handleClick();
                setSelectAdmin(false);
              }}
            >
              {localStorage.getItem('loginType') === 'User' ? (
                ''
              ) : (
                <ListItemText style={{ marginLeft: '11px' }} primary="Admin" />
              )}
              {localStorage.getItem('loginType') === 'User' ? (
                ''
              ) : open ? (
                <ExpandLess style={{ marginRight: '10%' }} />
              ) : (
                <ExpandMore style={{ marginRight: '10%' }} />
              )}
            </ListItem>
            <Collapse in={open} unmountOnExit>
              {localStorage.getItem('loginType') === 'User'
                ? ''
                : handler(MenuLists)}
            </Collapse>
            
          </List>
          {handler(logoutMenu)}{' '}
        </div>
      ) : (
        <List
          component="nav"
          aria-labelledby="nested-list-subheader"
          className={classes.Drawer}
        >
        {/*  <ListItem
            button
            onClick={() => {
              handleMenu('Arcesb');
            }}
          >
            <ListItemText style={{ marginLeft: '11px' }} primary="Arcesb" />
            {openArcesb ? (
              <ExpandLess style={{ marginRight: '10%' }} />
            ) : (
              <ExpandMore style={{ marginRight: '10%' }} />
            )}
          </ListItem>
          <Collapse in={openArcesb} unmountOnExit>
            {handler(arcesb)}
            </Collapse>*/}
            <ListItem
            button
            onClick={() => {
              handleMenu('Flows');
            }}
          >
            
            <div style={{paddingLeft:'10px'}} />
            <ListItemText style={{ marginRight: '10px' }} primary="Flows" />
            {openFlowsMenu ? (
              <ExpandLess style={{ marginRight: '10%' }} />
            ) : (
              <ExpandMore style={{ marginRight: '10%' }} />
            )}
          </ListItem>
          <Collapse in={openFlowsMenu} unmountOnExit>
            {handler(flowsmenu)}
            </Collapse>
            {handler(arcesb)}
        </List>
      )}
    </div>
  );
  console.log(
    'statestate',
    loginState,
    'userOrgState',
    userOrgState,
    'client.headers',
    client.headers
  );
  const handleProceed = () => {
    setCheckLoginType(false);
  };
  return (
    <AuthContext.Provider
      value={{
        state: {
          ...loginState,
          ...userOrgState,
        },
        dispatch: { userOrgDispatch, loginDispatch },
      }}
    >
      {/*localStorage.getItem('token')/loginState.token*/}
      {console.log(
        '---------------loginState.token',
        loginState.token,
        'checkLoginType',
        checkLoginType,
        'loginState.loginType',
        loginState.loginType
      )}
      {localStorage.getItem('token') ? (
        localStorage.getItem('loginType') === 'User' && checkLoginType ? (
          <FetchUserOrganization
            handleProceed={handleProceed}
            userUUIDdata={loginState.user}
            handleLogout={handleLogout}
          />
        ) : (
          <MuiThemeProvider theme={(Theme, getMuiTheme())}>
            {/* console.log(isAuthenticated , checkToken,"isAuthenticated && checkToken")*/}
            <div className={classes.root}>
              <nav className={classes.drawer} aria-label="mailbox folders">
                <IconButton
                  color="inherit"
                  aria-label="Open drawer"
                  edge="start"
                  onClick={handleDrawerToggle}
                  className="menubutton"
                >
                  <MenuIcon />
                </IconButton>
                <Hidden smUp implementation="css">
                  <Drawer
                    container={container}
                    variant="temporary"
                    anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                    open={mobileOpen}
                    onClose={handleDrawerToggle}
                    ModalProps={{
                      keepMounted: true, // Better open performance on mobile.
                    }}
                    classes={{ paper: classes.drawerPaper }}
                  >
                    <div>{drawer}</div>
                  </Drawer>
                </Hidden>
                <Hidden xsDown implementation="css">
                  <Drawer
                    variant="persistent"
                    anchor="left"
                    open
                    classes={{ paper: classes.drawerPaper }}
                  >
                    <div>{drawer}</div>
                  </Drawer>
                </Hidden>
              </nav>

              <main className={classes.content}>{props.children}</main>
            </div>
          </MuiThemeProvider>
        )
      ) : (
        <LoginForm loginSuccess={loginSuccess} />
      )}
    </AuthContext.Provider>
  );
}
ResponsiveDrawer.propTypes = {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  container: PropTypes.any,
};
export default withRouter(ResponsiveDrawer);