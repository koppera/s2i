import React, { useEffect, useContext, useCallback } from 'react';
import { useMutation, useQuery, useManualQuery } from 'graphql-hooks';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import '../css/commonStyle.css';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import Theme from '../css/theme';
import Grid from '@material-ui/core/Grid';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
const LOGIN_USER = `mutation loginUser($userID:String,$userPassword:String){
    loginUser(userID:$userID,userPassword:$userPassword) {
      idUser
      userUUID
      userID
      userPassword
      loginType
      createdOn
      lastUpdatedOn
    }
    }`;

const LOGIN_USERS=`mutation loginUser($userID:String,$userPassword:String)
 {
  loginUser(userID:$userID,userPassword:$userPassword){
     tokenVal
  }
 }`;
const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

const getMuiTheme = () =>
  createMuiTheme(Theme, {
    overrides: {
      MuiSelect: {
        select: {
          minWidth: 180,
        },
      },
    },
  });

export default function LoginForm(props) {
  //LOGIN
  const [
    loginUser,
    { loading: mloading, error: merror, data: mdata },
  ] = useMutation(LOGIN_USER);
  const classes = useStyles();
  const [dropdown, setDropdown] = React.useState('');
  const [state, setState] = React.useState({
    checkedA: true,
    checkedB: true,
  });

  const handleChangeDropdown = (name) => (event) => {
    setErrMsg(false);
    setErrMsgDropdown(false);
    setErrMsgNameInfo(false);
    setValues({
      ...values,
      [event.target.name]: event.target.value,
    });
  };

  const [values, setValues] = React.useState({ username: '', password: '' });
  const [ErrMsgDropdown, setErrMsgDropdown] = React.useState(false);
  const [ErrMsgNameInfo, setErrMsgNameInfo] = React.useState(false);
  const [ErrMsg, setErrMsg] = React.useState(false);
  const [errorInfo, setErrorInfo] = React.useState({
    username: false,
    password: false,
  });
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [value, setValue] = React.useState('IS');
  const [variant, setSnackbarVariant] = React.useState('error');
  const [message, setSnackbarMessage] = React.useState();

  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }

  const handleChange = (name) => (event) => {
    setErrMsg(false);

    setValues({ ...values, [name]: event.target.value });
    switch (name) {
      case 'username':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else {
          if (
            event.target.value.match('^([a-zA-Z0-9])[a-zA-Z0-9]*$') &&
            event.target.value.length <= 25
          ) {
            setErrorInfo({ ...errorInfo, [name]: false });
            setValues({ ...values, [name]: event.target.value });
          } else {
            setErrorInfo({ ...errorInfo, [name]: true });
          }
        }
        break;

      case 'password':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else {
          if (
            event.target.value.match(
              '^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,15}$'
            )
          ) {
            setErrorInfo({ ...errorInfo, [name]: false });
            setValues({ ...values, [name]: event.target.value });
          } else {
            setErrorInfo({ ...errorInfo, [name]: true });
          }
        }
        break;

      default:
        return false;
    }
  };

  const handleSubmit = async () => {
    setErrMsg(false);

    if (values.username === '' || values.username === '') {
      setErrMsg(true);
    } else {
      loginUser({
        variables: {
          userID: values.username,
          userPassword: values.password,
        },
      });
      console.log(mloading, merror, mdata);
      if (mdata !== undefined && mdata.loginUser !== undefined) {
        console.log(mdata.loginUser[0]);
        if (mdata.loginUser[0] !== undefined) {
          props.history.push({ pathname: '/' });
          setErrMsg(false);
          setValues({ ...values, username: '', password: '' });
        } else {
          console.log('user not found');
        }
      }
    }
  };

  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <div>
        {ErrMsg === true ? (
          <div style={{ textAlign: 'center' }} className="addfieldserror">
            Please fill the fields
          </div>
        ) : (
          ''
        )}
        <form className="content" noValidate autoComplete="off">
          <TextField
            id="outlined-dense"
            label="Username "
            className="textField"
            margin="dense"
            variant="outlined"
            onChange={handleChange('username')}
            value={values.username}
            name="username"
          />

          {errorInfo.username === true ? (
            <div className="addfieldserror">
              "Allowed alphanumeric,no special characters are allowed.Maximum
              length is 25."
            </div>
          ) : (
            ''
          )}

          <TextField
            id="outlined-dense"
            label="Password "
            className="textField"
            margin="dense"
            variant="outlined"
            type="password"
            //placeholder=""
            onChange={handleChange('password')}
            value={values.password}
            name="password"
          />

          {errorInfo.password === true ? (
            <div id="nameid" className="addfieldserror">
              Password length must be between 8 and 15 characters must have
              atleast one[uppercase,lowercase,special] character and atleast one
              number
            </div>
          ) : (
            ''
          )}

          <Button
            onClick={handleSubmit}
            variant="contained"
            fullWidth="true"
            className="createpartnerbutton"
          >
            Create
          </Button>
        </form>
        <Snackbar
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
          autoHideDuration={5000}
          open={openSnackbar}
          onClose={handleCloseSnackbar}
        >
          <MySnackbarContentWrapper
            onClose={handleCloseSnackbar}
            variant={variant}
            message={message}
          />
        </Snackbar>
      </div>
    </MuiThemeProvider>
  );
}
