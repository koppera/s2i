import MaterialTable from 'material-table';
import { forwardRef, useEffect } from 'react';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import ReactDOM from 'react-dom';
import MTableToolbar  from 'material-table';
import React, { Component } from "react";
import { useManualQuery } from 'graphql-hooks'
import Error from '../components/emptyPage';
import Timestamp from '../timestamp';
import Button from "@material-ui/core/Button";
import Loader from '../components/loader';
import Theme from '../css/theme';
import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles
} from '@material-ui/core/styles';

function iconStyles() {
  return {
   
  };
}


const ELASTIC_SEARCH=`mutation elasticSearch($SearchInput:String){
  elasticSearch(search:$SearchInput){
    PostName
    PostType
    PostBody
  }
}`;




export default function Monitoring(props) {
  const [searchValue,setSearchValue]=React.useState();
  const [serverError, setServerError] = React.useState(false);
  const [elasticData,setElasticData]=React.useState([]);
  const [fetchUser, { loading, error, data }] = useManualQuery(ELASTIC_SEARCH);
  const columns=[
    { title: 'Post Name', field: 'PostName' },
    { title: 'Post Type', field: 'PostType' },
    { title: 'Post Body', field: 'PostBody' }     
  ];

/*
  const [state, setState] = React.useState({
    columns: [
      { title: 'UUID', field: 'UUID' ,editable: 'never',hidden:true },
      { title: 'Name', field: 'Name' },
      { title: 'Description', field: 'Description' },
      { title: 'Created', field: 'createdon',editable: 'never' ,
      render : (rowData) => {
        console.log(rowData)
        if(rowData!= undefined){
          return Timestamp(rowData.createdon)
        }else{
          return 
        }
      }  
    },
      { title: 'Updated', field: 'lastupdatedon',editable: 'never' ,
      render : (rowData) => {
        console.log(rowData)
        if(rowData!= undefined){
          return Timestamp(rowData.lastupdatedon)
        }else{
          return 
        }
      }  
    },
     
    ],
    data:[]
    
  });
  */

  const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear color="error"{...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline  color="error" {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => (<ChevronRight  {...props} ref={ref} />)),
    Edit: forwardRef((props, ref) => (
      <Edit
       
        {...props}
        ref={ref}
        onClick={console.log('hello world')}
      />
    )),
    
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search   {...props} ref={ref}  />),
    SortArrow: forwardRef((props, ref) => (
      
      <ArrowDownward {...props} ref={ref} />
      
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
  };


  useEffect(()=>{
    if(data!==undefined){
      setElasticData(data.elasticSearch)
      setServerError(false);
    }
    else {
      console.log('server error');
      setServerError(true);
    }
  },[data])
  
 
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSvgIcon:{
          root:{
         color:"#0b153e"
        }
        },
        MuiTypography:{h6:{
          fontSize:14,   
          fontFamily:"Arial !important"   
          }},
      }
      
    });
    
    const handleSearchValue = async (e)=>{
      console.log("e---",e)
      if(e.length>3){
      await  fetchUser({
          variables:{
            SearchInput:e
          }
        })  
      }
      setSearchValue(e)
    }
    if (loading) return <Loader />;
    if (serverError)
    return <Error type={'Sever connection lost.Please try again'} />;
  return (
  
    <MuiThemeProvider theme={getMuiTheme()}>
    <MaterialTable
      //onSearchChange={e => handleSearchValue(e)}
     icons={tableIcons}
         title="Monitoring"
      columns={columns}
      data={elasticData}
      
      editable={{
        onRowAdd: newData =>
          new Promise(resolve => {
           
          }),
              
        onRowUpdate: (newData, oldData) =>
          new Promise(resolve => {
           
          }),
        onRowDelete: oldData =>
          new Promise(resolve => {
          
          }),
      }}  options={{
        headerStyle: {
        
        textAlign: 'center',
        fontSize:12,
        fontWeight:'bold',
        fontFamily:'Arial !important',
        backgroundColor:"#0b153e",
        color:"#ffffff",
        padding:'4px',
       
        },
        
          
          searchFieldStyle: {
            color: '#0b153e'
          
         
        },
        actionsColumnIndex: -1
       
        }}
       
        
      />
  
       
        </MuiThemeProvider>
      
  )
    }
