import MaterialTable from 'material-table';
import { useMutation, useQuery } from 'graphql-hooks';
import { forwardRef, useEffect } from 'react';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import ReactDOM from 'react-dom';
import MTableToolbar from 'material-table';
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Timestamp from '../timestamp';
import TextField from '@material-ui/core/TextField';
import Theme from '../css/theme';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import Error from '../components/emptyPage';
import Loader from '../components/loader';
import CircularIndeterminateLoader from '../components/loader';

import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles,
} from '@material-ui/core/styles';

const DISPLAY_All_USER_ROLETYPES = `{
  allUserRoleTypes{
    idUserRoleType
    userRoleTypeUUID
    userRoleTypeName
    userRoleTypeDescription
    userRoleTypeComment
    createdOn
    lastUpdatedOn
  }
}
`;
const ADD_USER_ROLETYPES = `mutation createUserRoleTypes($inputCreate:UserRoleTypesInput){
  createUserRoleTypes(input:$inputCreate)
  {
    userRoleTypeName
    userRoleTypeDescription
    userRoleTypeComment
   
  }
}`;
const UPDATE_USER_ROLETYPES = `mutation updateUserRoleTypes($input:UserRoleTypesInput){
  updateUserRoleTypes(input:$input) {
    idUserRoleType
    userRoleTypeUUID
    userRoleTypeName
    userRoleTypeDescription
    userRoleTypeComment
    createdOn
    lastUpdatedOn
  }
}`;
const REMOVE_USER_ROLETYPES = `mutation removeUserRoleTypes($input:UserRoleTypesInput){
  removeUserRoleTypes(input:$input) {
    idUserRoleType
    userRoleTypeUUID
    userRoleTypeName
    userRoleTypeDescription
    userRoleTypeComment
    createdOn
    lastUpdatedOn
  }
}
`;
function iconStyles() {
  return {};
}
export default function MaterialTableDemo() {
  const [userRoleTypedata, setData] = React.useState([]);
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [message, setSnackbarMessage] = React.useState();
  const [variant, setSnackbarVariant] = React.useState('error');
  const [serverError, setServerError] = React.useState(false);
  const [nameError, setNameError] = React.useState({
    error: false,
    label: "",
    helperText: "",
    validateInput: false,
});
const [state, setState] = React.useState({
 
});

  const [valid, setValid] = React.useState(false);
  const columns= [
    { title: 'UUID', field: 'userRoleTypeUUID' ,editable: 'never',hidden:true},
    { title: 'Name', field: 'userRoleTypeName' ,
    editComponent: (props) => {
      console.log('editComponent', props, props.value);
      return (
        <div>
          <TextField
            placeholder="Name"
            margin="normal"
            error={
              !props.value &&
              nameError.validateInput &&
              props.rowData.submitted
                  ? nameError.error
                  : false
          }
          helperText={
            !props.value &&
            nameError.validateInput &&
            props.rowData.submitted
                ? nameError.helperText
                : ""
        }
            fullWidth
            getOptionLabel={(option) => option.name}
            value={props.value}
            onChange={(v) => props.onChange(v.target.value)}
          />

            <div style={{ color: 'red' }}>
            {!props.rowData.userRoleTypeName||
            /^[a-zA-Z ]+$/.test(props.rowData.userRoleTypeName)
              ? ''
              : 'Enter only alphabets'}
          </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.userRoleTypeName}</div>;
      },
    },
    {
      title: 'Description',
      field: 'userRoleTypeDescription',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder="Description"
              margin="normal"
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            />
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.userRoleTypeDescription}</div>;
      },
    },
    {
      title: 'Comment',
      field: 'userRoleTypeComment',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder="Comment"
              margin="normal"
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            />

            <div style={{ color: 'red' }}>
              {!props.rowData.userRoleTypeComment||
              /^[a-zA-Z0-9 ]+$/.test(props.rowData.userRoleTypeComment)
                ? ''
                : 'Special characters are not allowed'}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.userRoleTypeComment}</div>;
      },
    },
    {
      title: 'Created',
      field: 'createdOn',
      editable: 'never',
      render: (rowData) => {
        console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.createdOn);
        } else {
          return;
        }
      },
    },
    {
      title: 'Last Modified',
      field: 'lastUpdatedOn',
      editable: 'never',
      render: (rowData) => {
        console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.lastUpdatedOn);
        } else {
          return;
        }
      },
    },
  ];

  //ADD USER ROLE TYPES
  const [addUserRoleTypes, addloading, adddata] = useMutation(
    ADD_USER_ROLETYPES
  );
  //FETCH ALL USER ROLE TYPES
  const { loading, error, data, refetch } = useQuery(
    DISPLAY_All_USER_ROLETYPES
  );
  //EDIT USER ROLE TYPES
  const [updateUserRoleTypes, updateloading, updatedata] = useMutation(
    UPDATE_USER_ROLETYPES
  );
  //remove USER ROLE TYPES
  const [removeUserRoleTypes, removeloading, removedata] = useMutation(
    REMOVE_USER_ROLETYPES
  );
  const handleRefetch = (value, message) => {
    refetch({
      updateData(_, data) {
        return data;
      },
    });
    setOpenSnackbar(true);
    setSnackbarMessage(message);
    if (value) {
      setSnackbarVariant('error');
    } else {
      setSnackbarVariant('success');
    }
  };
  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }
  const classes = makeStyles(iconStyles)();
  const tableIcons = {
    Add: forwardRef((props, ref) => (
      <AddBox color="primary" {...props} ref={ref} />
    )),
    Check: forwardRef((props, ref) => (
      <Check color="primary" {...props} ref={ref} />
    )),
    Clear: forwardRef((props, ref) => (
      <Clear color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline color="error" {...props} ref={ref} />
    )),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => (
      <Edit
        {...props}
        ref={ref}
        color="primary"
        onClick={console.log('hello world')}
      />
    )),
    Export: forwardRef((props, ref) => (
      <SaveAlt color="primary" {...props} ref={ref} />
    )),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => (
      <FirstPage color="primary" {...props} ref={ref} />
    )),
    LastPage: forwardRef((props, ref) => (
      <LastPage color="primary" {...props} ref={ref} />
    )),
    NextPage: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft color="primary" {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => (
      <Clear color="primary" {...props} ref={ref} />
    )),
    Search: forwardRef((props, ref) => (
      <Search color="primary" {...props} ref={ref} />
    )),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSelect: {
          icon: {
            color: '#0b153e',
          },
        },

        MuiTypography: {
          h6: {
            fontSize: 14,
            fontFamily: 'Arial !important',
          },
        },
      },
    });
  useEffect(() => {
    if (data !== undefined) {
      console.log(data.allUserRoleTypes);
      setData(data.allUserRoleTypes);
      setServerError(false);
    } else {
      console.log('server error');
      setServerError(true);
    }
  }, [data]);

  async function handleAddUserType(newData) {
    return await new Promise(async (resolve) => {
      resolve();
      await addUserRoleTypes({
        variables: {
          inputCreate: {
            userRoleTypeName: newData.userRoleTypeName,
            userRoleTypeDescription: newData.userRoleTypeDescription,
            userRoleTypeComment: newData.userRoleTypeComment,
          },
        },
      }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
        console.log(data, error, graphQLErrors, networkError, cacheHit);
       if (data !== null && error == false) {
         handleRefetch(error, 'Saved successfully');
       } 
       else if (error && graphQLErrors.length > 0)
         {
          let duperror = graphQLErrors[0].message;
          console.log(duperror);
          if (
            duperror !== null &&
            duperror.indexOf('ER_DUP_ENTRY') !== -1
          ) {
            handleRefetch(error, 'Name already existed');
          }
        }
     })
     .catch((e) => {
       // you can do something with the error here
       console.log(e);
     });
    });
  }
  async function handleUpdateUserType(newData, oldData) {
    return await new Promise(async (resolve) => {
      resolve();
      await updateUserRoleTypes({
        variables: {
          input: {
            userRoleTypeName: newData.userRoleTypeName,
            userRoleTypeDescription: newData.userRoleTypeDescription,
            userRoleTypeComment: newData.userRoleTypeComment,
            userRoleTypeUUID: newData.userRoleTypeUUID,
          },
        },
      }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
        console.log(data, error, graphQLErrors, networkError, cacheHit);
       if (data !== null && error == false) {
         handleRefetch(error, 'Updated successfully');
       } 
       else if (error && graphQLErrors.length > 0)
         {
          let duperror = graphQLErrors[0].message;
          console.log(duperror);
          if (
            duperror !== null &&
            duperror.indexOf('ER_DUP_ENTRY') !== -1
          ) {
            handleRefetch(error, 'Name already existed');
          }
        }
     })
     .catch((e) => {
       // you can do something with the error here
       console.log(e);
     });
    });
  }
  async function handleRemoveUserType(oldData) {
    return await new Promise(async (resolve) => {
      resolve();
      await removeUserRoleTypes({
        variables: {
          input: {
            userRoleTypeUUID: oldData.userRoleTypeUUID,
          },
        },
      }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
        console.log(data, error, graphQLErrors, networkError, cacheHit);
        if (data !== null && error == false) {
          handleRefetch(error, 'Deleted successfully');
        } else if (error && graphQLErrors.length > 0) {
          let duperror = graphQLErrors[0].message;
          console.log(duperror);
          if (
            duperror !== null &&
            duperror.indexOf('ER_ROW_IS_REFERENCED') !== -1
          ) {
            setOpenSnackbar(true);
            setSnackbarVariant('error');
            setSnackbarMessage('Selected row is referenced in User Role Assignment table');
          }
        }
      })
      .catch((e) => {
        // you can do something with the error here
        console.log(e);
      });
    });
  }
  if (loading) return <Loader />;
  if (serverError)
  return <Error type={'Server connection lost.Please try again'} />;
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
    <MaterialTable
     
     icons={tableIcons}
         title="User Role Type"
      columns={columns}
      data={userRoleTypedata}
      editable={{
        onRowAdd: newData =>
        new Promise((resolve, reject) => {
          setTimeout(() => {
              newData.submitted = true;
              if (!newData.userRoleTypeName||
               !/^[a-zA-Z\s]+$/.test(newData.userRoleTypeName)||
               !/^[a-zA-Z0-9 ]*$/.test(newData.userRoleTypeComment)
               ) {
                  setNameError({
                      error: true,
                      label: "required",
                      helperText: "Required.",
                      validateInput: true,
                  });
                  reject();
                  return;
              }
              resolve();
            
              handleAddUserType(newData)
             
             
          }, 600);
      }),
      
          onRowUpdate: (newData, oldData) =>
        
        new Promise((resolve, reject) => {
          setTimeout(() => {
              newData.submitted = true;
              if (!newData.userRoleTypeName||
               !/^[a-zA-Z\s]+$/.test(newData.userRoleTypeName)||
               !/^[a-zA-Z0-9 ]*$/.test(newData.userRoleTypeComment)
               ) {
                  setNameError({
                      error: true,
                      label: "required",
                      helperText: "Required.",
                      validateInput: true,
                  });
                  reject();
                  return;
              }
              resolve();
            
              handleUpdateUserType(newData)
             
             
          }, 600);
      }),
          
          
        onRowDelete: oldData =>handleRemoveUserType(oldData)
          
      }} 
      options={{
        headerStyle: {
        
        textAlign: 'center',
        fontSize:12,
        fontWeight:'bold',
        fontFamily:'Arial !important',
        backgroundColor:"#0b153e",
        color:"#ffffff",
        padding:'4px',
       
        },
        
          searchFieldStyle: {
            color: '#0b153e',
          },
          actionsColumnIndex: -1,
          pageSize:10,
          pageSizeOptions: [10, 25 ,50,100 ],
          toolbar: true,
          paging: true
        }}
      />
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        autoHideDuration={1500}
        open={openSnackbar}
        onClose={handleCloseSnackbar}
      >
        <MySnackbarContentWrapper
          onClose={handleCloseSnackbar}
          variant={variant}
          message={message}
        />
      </Snackbar>
    </MuiThemeProvider>
  );
}
