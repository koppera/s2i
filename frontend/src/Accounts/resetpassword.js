import React from 'react';
import { useMutation } from 'graphql-hooks';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import Logout from '@material-ui/icons/ExitToApp';
import Theme from '../css/theme';
import {Typography, Paper } from '@material-ui/core';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';

export default function ResetPassword(props) {
  const [values, setValues] = React.useState({
   currentpassword:'',
    newpassword: '',
    confirmpassword: '',
  });
  const [nameError, setNameError] = React.useState(false);

  const [error, setError] = React.useState({
    currentpassword:false,
    newpassword: false,
    confirmpassword: false
  });
  const [ErrMsg, setErrMsg] = React.useState(false);
  const [pwderror, setpwderror] = React.useState(false);
  const handlereset = () => {
    setErrMsg(false);
    setNameError(false);
    setError({
      currentpassword: false,
      newpassword: false,
      confirmpassword:false,
    });
    setValues({
      ...values,
      currentpassword: '',
      newpassword: '',
      confirmpassword: '',
    });
  };
  
const passwordexp = (   /^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,15}$/);


  const handleChange = (name) => (event) => {
    setValues({ ...values, [name]: event.target.value });
    setErrMsg(false);
    switch (name) {
        case 'currentpassword':
        if (event.target.value.length === 0) {
          setError({ ...error, [name]: false });
        } else if (
          event.target.value.match(
            passwordexp
          )
        ) {
         // console.log(values);
          setError({ ...error, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setError({ ...error, [name]: true });
        }
        break;
      case 'newpassword':
        if (event.target.value.length === 0) {
          setError({ ...error, [name]: false });
        } else if (
          event.target.value.match(
            passwordexp
          )
        ) {
         // console.log(values);
          setError({ ...error, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setError({ ...error, [name]: true });
        }

        break;
      case 'confirmpassword':

        if (event.target.value.length === 0) {
            setValues({ ...values, [name]: '' });
            setError({ ...error, [name]: false });
          } else if(event.target.value == values.newpassword)
          {
            setError({ ...error, [name]: false });
          }
          else
          {
           setError({ ...error, [name]: true });
          }
          break;
        default:return true
    }
  };

  const handleSubmit = () => {
    setErrMsg(false);
    setError(false);
    if (
        !values.currentpassword &&
        !values.newpassword &&
        !values.confirmpassword 
       
      ) {
        setErrMsg(true);
      } else if(
        !values.currentpassword ||
        !values.newpassword ||
        !values.confirmpassword 
      ){
        setErrMsg(false);
        setNameError(true);
      }else if(
        !values.currentpassword &&
        !values.newpassword &&
        !values.confirmpassword &&
        error.userID !== true &&
        error.userPassword!== true &&
        error.confirmPassword!== true &&
        error.loginType !== true 
      ){
        setErrMsg(false);
        setValues({
          ...values,
          currentpassword: values.currentpassword,
          newpassword: values.newpassword,
          confirmPassword: values.confirmPassword,
        });
      }
 
  };
 
  return (
    <MuiThemeProvider theme={Theme}>
      <div>
      <Grid container spacing={3} className="content">   
      <Grid  items xs={12} sm={6} >
        <form className="content" noValidate autoComplete="off">
            <Paper className="paperpage1" style={{padding:"18px"}}>
            {ErrMsg === true ? (
            <div style={{ textAlign: 'center' }} className="addfieldserror">
                Please fill the fields
            </div>
            ) : (
            ''
                )}
            <Grid item xs={10} sm={10}>
          <h2 className="h2" style={{ color: '#0b153e' }}>Reset Password</h2>
          </Grid>
                <Grid
                  item
                  xs={12} 
                  sm={12}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                    <TextField
                        id="standard-basic"
                        className="partnertextField"
                        label="Current Password"
                        onChange={handleChange('currentpassword')}
                        value={values.currentpassword}
                        name="currentpassword"
                        variant="outlined"
                        type="password"
                        margin="dense"
                    />
                    {error.currentpassword === true ? (
                        <div id="nameid" className="addfieldserror">
                       8 to 15 characters must have atleast one[uppercase,lowercase,special character and Number]
                        </div>
                    ) : (
                        ''
                    )}
                        {nameError&&!values.currentpassword? (
                            <div id="nameid" className="addfieldserror">
                              Please enter Current Password.
                            </div>
                          )
                           : ""}
             </Grid>
                <Grid
                  item
                  xs={12}
                  sm={12}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                    <TextField
                        id="standard-basic"
                        className="partnertextField"
                        label="New Password"
                        onChange={handleChange('newpassword')}
                        value={values.newpassword}
                        name="newpassword"
                        variant="outlined"
                        type="password"
                        margin="dense"
                    />
                    {error.newpassword === true ? (
                        <div id="nameid" className="addfieldserror">
                       8 to 15 characters must have atleast one[uppercase,lowercase,special character and Number]
                        </div>
                    ) : (
                        ''
                    )}
                      {nameError&&!values.newpassword? (
                            <div id="nameid" className="addfieldserror">
                              Please enter New Password.
                            </div>
                          )
                           : ""}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={12}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                 >
                    <TextField
                        id="standard-basic"
                        className="partnertextField"
                        label="Confirm Password"
                        onChange={handleChange('confirmpassword')}
                        value={values.confirmpassword}
                        name="confirmpassword"
                        variant="outlined"
                        type="password"
                        margin="dense"
                    />
                    {error.confirmpassword === true ? (
                        <div id="nameid" className="addfieldserror">
                        Password Mismatch!
                        </div>
                    ) : (
                        ''
                    )}
                         {nameError&&!values.confirmpassword? (
                            <div id="nameid" className="addfieldserror">
                              Please enter Confirm Password.
                            </div>
                          )
                           : ""}

             </Grid>
            <Grid container spacing={3}>
                <Grid
                item
                xs={6}
                sm={6}
                gutterBottom
                variant="body2"
                color="textSecondary"
                className="text"
              >
                  <Button
                    style={{
                        color: '#000006',
                        backgroundColor:'#E5CCFF' 
                     }}
                    variant="contained"
                    //size="Large"
                    className="createpartnerbutton"
                    onClick={handlereset}
              >
                  RESET
                </Button>
                
            </Grid>
            <Grid
                item
                xs={6}
                sm={6}
                gutterBottom
                variant="body2"
                color="textSecondary"
                className="text"
              >
                <Button
                    variant="contained"
                    className="createpartnerbutton"
                    onClick={handleSubmit}
                  >
                    SAVE
                  </Button>
                </Grid>
                </Grid>
                </Paper>
                </form>
            </Grid>
            <Grid  items xs={12} sm={6} >
            <Paper className="paperpage1" style={{marginTop:'10px',paddingBottom:"54px",paddingTop:'25px',paddingLeft:'20px',paddingRight:'20px'}}>
            <Grid item xs={12} sm={12}>
             <h2 className="h2" style={{ color: '#0b153e'}}>Profile</h2>
              </Grid>
              <Grid container spacing={3}>
              <Grid
                  item
                  xs={6} 
                  sm={6}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                     <Typography className="typographysubheader" style={{lineHeight:3}}>First Name</Typography>
                   
             </Grid>
             <Grid
                  item
                  xs={6} 
                  sm={6}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                <Typography className="typographysubheader" style={{lineHeight:3}}>Last Name</Typography>
              </Grid>
             <Grid
                  item
                  xs={6} 
                  sm={6}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                <Typography className="typographysubheader" style={{lineHeight:3}}>Email</Typography>
             </Grid>
             <Grid
                  item
                  xs={6} 
                  sm={6}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                <Typography className="typographysubheader" style={{lineHeight:2.5}}>Phone</Typography>
             </Grid>
             </Grid>
             </Paper>
             </Grid>
             </Grid>
      </div>
    </MuiThemeProvider>
  );
}

