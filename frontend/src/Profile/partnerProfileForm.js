import React, { useEffect, useContext, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import { useMutation, useQuery } from 'graphql-hooks';
import Snackbar from '@material-ui/core/Snackbar';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Switch from '@material-ui/core/Switch';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import Modal from '@material-ui/core/Modal';
import Theme from '../css/theme';
import AuthContext from '../authUtil/Auth';

import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
const DISPLAY_All_ORGANIZATION = `query fetchOrganization($searchInput:String){
  fetchOrganization(searchInput:$searchInput) {
    idOrganization
    organizationUUID
    organizatonName
    organizationDescription
    createdOn
    lastUpdatedOn
  } 
}
`;
const DISPLAY_PROTOCOL_TYPE = `{
  allProtocolType{
    protocolTypeUUID
    protocolTypeName
  }
}`;
const DISPLAY_All_PROFILEID_TYPES = `{
  allProfileIDTypes {
    idProfileIDTypes
    profileIDTypesUUID
    profileIDTypeName
    profileIDTypeDescription
    createdOn
    lastUpdatedOn
  }
}

`;
const ADD_PARTNERS = `mutation createPartner($inputCreate:PartnerInput){
  createPartner(input:$inputCreate){
    corporationName
  organizationUUID
    partnerProfileDescription
    organizationUnit
    status
    prefferedProtocolUUID
    partnerProfileUUID
  }
}`;
const ADD_PROFILEIDVALUES = `mutation createProfileIDValue($input:ProfileIDValueInput){
  createProfileIDValue(input:$input) {
    idProfileIDValues
    profileIDValuesUUID
    profileIDTypesUUID
    profileIDValue
    partnerProfileUUID
    createdOn
    lastUpdatedOn
  }
}

`;
const UPDATE_PARTNERS = `mutation updatePartner($inputCreate:PartnerInput){
  updatePartner(input:$inputCreate){
    corporationName
    organizationUUID
    partnerProfileDescription
    organizationUnit
    status
    prefferedProtocolUUID
  }
}`;

const useStyles = makeStyles((theme) => ({

  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
}));
export default function PartnerProfileForm(props) {
  console.log(props,"OrgUUID",props.OrgUUID);
  const { loading: orgloading, error: orgerror, data: orgData } = useQuery(
    DISPLAY_All_ORGANIZATION,
    {
      variables: {
        searchInput: '',
      },
    }
  );
  const {
    loading: pTloading,
    data: pTdata,
    errors: pTerrors,
    networkError: pTnetworkError,
  } = useQuery(DISPLAY_PROTOCOL_TYPE);
  const {
    loading: pIloading,
    data: pIdata,
    errors: pIerrors,
    networkError: pInetworkError,
  } = useQuery(DISPLAY_All_PROFILEID_TYPES);
  //UPDATE CONTACT
  const [values, setValues] = React.useState({
    organizationUUID: '',
    organizationUnit: '',
    corporationName: '',
    partnerProfileDescription: '',
    prefferedProtocolUUID: null,
    status: 'Active',
    profileIDTypesUUID: '',
    profileIDValue: '',
  });
  const [
    addPartner,
    {
      loading: ploading,
      data: pdata,
      errors: perrors,
      networkError: anetworkError,
    },
  ] = useMutation(ADD_PARTNERS);
  const [addProfileIDValues, addloading, adddata] = useMutation(
    ADD_PROFILEIDVALUES
  );
  const [
    updatePartner,
    {
      loading: uploading,
      data: updata,
      errors: uperrors,
      networkError: unetworkError,
    },
  ] = useMutation(UPDATE_PARTNERS);
  const [readValue, setReadOnly] = React.useState(false);
  const [ErrMsg, setErrMsg] = React.useState(false);
  const [errorInfo, setErrorInfo] = React.useState({
    organizationUUID: false,
    organizationUnit: false,
    corporationName: false,
    partnerProfileDescription: false,
    prefferedProtocolUUID: false,
    profileIDTypesUUID: false,
    profileIDValue: false,
  });
  const [submitErr, setSubmitErr] = React.useState(false);
  const [orgnazationData, setOrgData] = React.useState([]);
  const [protocolData, setProtocolData] = React.useState([]);
  const [profileIDTypeData, setProfileIdTypeData] = React.useState([]);

  useEffect(() => {
    console.log(props.rowData);
    if (props.rowData != undefined && props.title != 'Add') {
      console.log(props.rowData, props.rowData.organizationUnit);
      setValues({
        organizationUUID: props.rowData.organizationUUID,
        organizationUnit: props.rowData.organizationUnit,
        corporationName: props.rowData.corporationName,
        partnerProfileDescription: props.rowData.partnerProfileDescription,
        prefferedProtocolUUID: props.rowData.prefferedProtocolUUID,
        status: props.rowData.status,
      });
    }
    if (orgData !== undefined) {
      console.log(orgData.fetchOrganization);
      setOrgData(orgData.fetchOrganization);
    }
    if (pTdata !== undefined) {
      console.log(pTdata.allProtocolType);
      setProtocolData(pTdata.allProtocolType);
    }
    if (pIdata !== undefined) {
      console.log(pIdata.allProfileIDTypes);
      setProfileIdTypeData(pIdata.allProfileIDTypes);
    }
  }, [props, orgData, pTdata, pIdata]);
  const handleChange = (name) => (event) => {
    console.log(event.target.checked, name);
    setErrMsg(false);
    setValues({ ...values, [name]: event.target.value });
    switch (name) {
      case 'organizationUnit':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[a-zA-Z ]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
          
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'corporationName':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[a-zA-Z ]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'partnerProfileDescription':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'prefferedProtocolUUID':
        setValues({
          ...values,
          [event.target.name]: event.target.value,
        });
        break;
      case 'profileIDTypesUUID':
        setValues({
          ...values,
          [event.target.name]: event.target.value,
        });
        break;
      case 'organizationUUID':
        setValues({
          ...values,
          [event.target.name]: event.target.value,
        });
        break;
      case 'status':
        if (event.target.checked) {
          setValues({
            ...values,
            [event.target.name]: 'Active',
          });
        } else {
          setValues({
            ...values,
            [event.target.name]: 'Inactive',
          });
        }

        break;
      case 'profileIDValue':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[a-zA-Z0-9 ]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      default:
        return false;
    }
  };
  const handleSubmit = async () => {
    console.log(values);
    setErrMsg(false);
    if (props.title == 'Add') {
      if (
        values.organizationUUID!==""&&
        values.organizationUnit!=="" &&
        values.corporationName!=="" &&
        values.profileIDTypesUUID!==""&&
        values.profileIDValue!=="" &&
        errorInfo.organizationUnit !== true &&
        errorInfo.organizationUUID !== true &&
        errorInfo.corporationName !== true 
      ){
        setErrMsg(false);
        setValues({
          ...values,
          organizationUUID: values.organizationUUID,
          organizationUnit: values.organizationUnit,
          corporationName: values.corporationName,
          partnerProfileDescription: values.partnerProfileDescription,
          prefferedProtocolUUID: values.prefferedProtocolUUID,
          Status: values.Status,
          profileIDTypesUUID: values.profileIDTypesUUID,
          profileIDValue: values.profileIDValue,
        });
        await addPartner({
          variables: {
            inputCreate: {
              corporationName: values.corporationName,
              organizationUUID: values.organizationUUID,
              partnerProfileDescription: values.partnerProfileDescription,
              organizationUnit: values.organizationUnit,
              status: values.status,
              prefferedProtocolUUID:
                values.prefferedProtocolUUID == 'null'
                  ? null
                  : values.prefferedProtocolUUID,
            },
          },
        })
          .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
            console.log(data, error, graphQLErrors, networkError, cacheHit);
            if (data !== null && error == false) {
              console.log(data.createPartner[0]);
              addProfileIDValues({
                variables: {
                  input: {
                    profileIDTypesUUID: values.profileIDTypesUUID,
                    profileIDValue: values.profileIDValue,
                    partnerProfileUUID:
                      data.createPartner[0].partnerProfileUUID,
                  },
                },
              })
                .then(
                  ({
                    dataProfile,
                    error,
                    graphQLErrors,
                    networkError,
                    cacheHit,
                  }) => {
                    console.log(
                      dataProfile,
                      error,
                      graphQLErrors,
                      networkError,
                      cacheHit
                    );
                    if (dataProfile !== null && error == false) {
                      props.handleClickRoute(
                        true,
                        data.createPartner[0],
                        false
                      );
                    } else if (error && graphQLErrors.length > 0) {
                      let duperror = graphQLErrors[0].message;
                      console.log(duperror);
                      if (
                        duperror !== null &&
                        duperror.indexOf('ER_DUP_ENTRY') !== -1
                      ) {
                        props.onSuccess(error, 'Value already existed', false);
                      }
                      if (
                        duperror !== null &&
                        duperror.indexOf('Unexpected error value') !== -1
                      ) {
                        props.handleClickRoute(
                          true,
                          data.createPartner[0],
                          true
                        );
                        //handleRefetch(error, 'Profileid value already taken');
                      }
                    }
                  }
                )
                .catch((e) => {
                  // you can do something with the error here
                  console.log(e);
                });
              // props.handleClickRoute(true, data.createPartner[0]);
            } else if (error && graphQLErrors.length > 0) {
              let duperror = graphQLErrors[0].message;
              console.log(duperror);
              if (
                duperror !== null &&
                duperror.indexOf('ER_DUP_ENTRY') !== -1
              ) {
                props.onSuccess(error, 'Name already existed', false);
              }
              if (
                duperror !== null &&
                duperror.indexOf('Unexpected error value') !== -1
              ) {
                props.onSuccess(error, 'Combination already exist', true);
              }
            }
          })
          .catch((e) => {
            // you can do something with the error here
            console.log(e);
          });

        props.handleCloseModal(false);
      }
      else if( values.organizationUUID==""&&
      values.organizationUnit=="" &&
      values.corporationName=="" &&
      values.profileIDTypesUUID==""&&
      values.profileIDValue==""&&
      values.partnerProfileDescription==""&&
      values.prefferedProtocolUUID==null
       )
      {
        setErrMsg(true);
       
      }else if(values.organizationUUID==""||
      values.organizationUnit=="" ||
      values.corporationName=="" ||
      values.profileIDTypesUUID==""||
      values.profileIDValue=="")
      {
        setErrMsg(false);
        setSubmitErr(true);
      }
    } else {
      console.log('update');
      if (
        values.organizationUUID!=="" &&
        values.organizationUnit!=="" &&
        values.corporationName!=="" &&
        errorInfo.organizationUnit !== true &&
        errorInfo.organizationUUID !== true &&
        errorInfo.corporationName !== true 
      ) {
        setErrMsg(false);
        setValues({
          ...values,
          organizationUUID: values.organizationUUID,
          organizationUnit: values.organizationUnit,
          corporationName: values.corporationName,
          partnerProfileDescription: values.partnerProfileDescription,
          prefferedProtocolUUID: values.prefferedProtocolUUID,
          Status: values.Status,
        });
        await updatePartner({
          variables: {
            inputCreate: {
              corporationName: values.corporationName,
              partnerProfileDescription: values.partnerProfileDescription,
              organizationUnit: values.organizationUnit,
              status: values.status,
              prefferedProtocolUUID:
                values.prefferedProtocolUUID == 'null'
                  ? null
                  : values.prefferedProtocolUUID,
              partnerProfileUUID: props.rowData.partnerProfileUUID,
            },
          },
        })
          .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
            console.log(data, error, graphQLErrors, networkError, cacheHit);
            if (data !== null && error == false) {
              // handleRefetch(error, 'Updated successfully');
              props.onSuccess(error, 'Updated successfully', true);
            } else if (error && graphQLErrors.length > 0) {
              let duperror = graphQLErrors[0].message;
              console.log(duperror);
              if (
                duperror !== null &&
                duperror.indexOf('ER_DUP_ENTRY') !== -1
              ) {
                props.onSuccess(error, 'Name already existed', false);
              }
            }
          })
          .catch((e) => {
            // you can do something with the error here
            console.log(e);
          });
        // props.handleCloseModal(false);
      }
    }
  };
  const handleCloseModal = () => {
    props.handleCloseModal(false);
  };
  const handleReset = async () => {
    if(props.title == 'Add'){
    setErrMsg(false);
    setErrorInfo({
      organizationUUID: false,
      organizationUnit: false,
      corporationName: false,
      partnerProfileDescription: false,
      prefferedProtocolUUID: false,
      Status: false,
      profileIDTypesUUID: false,
      profileIDValue: false,
    });
    setValues({
      ...values,
      organizationUUID: '',
      organizationUnit: '',
      corporationName: '',
      partnerProfileDescription: '',
      prefferedProtocolUUID: null,
      Status: 'Inactive',
      profileIDTypesUUID: '',
      profileIDValue: '',
    });
    setSubmitErr(false);
  }
  else{
    setValues({
      ...values,
      partnerProfileDescription: '',
    });
  }
  };
  useEffect(() => {
    if (props.title != 'Add') {
      setReadOnly(true);
      console.log('read only' + readValue);
    }
  });
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiInputLabel: {
          formControl: {
            fontSize: '12px',
          },
        },
       
      },
    });
  return (
    <AuthContext.Consumer>
    { context=>
    <div>{console.log("contex",context)}
      <MuiThemeProvider theme={getMuiTheme()}>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={props.isModalOpen}
          onClose={handleCloseModal}
          className="fabmodal"
        >
          <div className="modalpaper">
            <Grid container className="header">
              <Grid item xs={10} sm={10}>
                <h2 id="modal-title" className="h2">
                  {props.title == 'Add'
                    ? 'Add Partner Profile'
                    : 'Update Partner Profile'}
                </h2>
              </Grid>
              <Grid item xs={2} sm={2} className="close">
                <CloseIcon onClick={handleCloseModal} />
              </Grid>
            </Grid>
            <div>
              {ErrMsg === true ? (
                <div style={{ textAlign: 'center' }} className="addfieldserror">
                  Please fill the fields
                </div>
              ) : (
                ''
              )}
              <form noValidate autoComplete="off">
                <Grid container spacing={3} className="addressformcontent">
                  <Grid item xs={12} sm={3}  >
                    <FormControl className="formcontrol" disabled={readValue}>
                      <InputLabel
                        style={{
                          align: 'center',
                          color: '#0b153e',
                          fontSize: 12,
                        }}
                        id="demo-simple-select-label"
                      >
                        Organization
                      </InputLabel>
                    
                      <Select
                     className="groupdropdown"
                   
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        // value={searchPartner.prefferedProtocolUUID}
                        value={values.organizationUUID}
                        onChange={handleChange('organizationUUID')}
                        name="organizationUUID"
                      >
                        {orgnazationData != undefined &&
                        orgnazationData.length > 0
                          ? orgnazationData.map((v, k) => {
                              return (
                                <MenuItem value={v.organizationUUID}>
                                  {v.organizatonName}
                                </MenuItem>
                              );
                            })
                          : null}
                        {/*{address != undefined && address.length > 0
                              ? address.map((v, k) => {
                                  return (
                                    <MenuItem value={v.organizationUUID}>
                                      {v.organizationUUID}
                                    </MenuItem>
                                  );
                                })
                              : null}
                              */}
                               
                      </Select>
                    </FormControl>
                     {submitErr&&!values.organizationUUID? (
                        <div id="nameid" className="addfieldserror">
                          Please select organization
                        </div>
                      )
                       : ""}
                  </Grid>
                  <Grid item xs={12} sm={3}>
                    <TextField
                      id="outlined-dense"
                      label="Organization Unit"
                      className="partnertextField"
                      margin="dense"
                      variant="outlined"
                      //placeholder=""
                      onChange={handleChange('organizationUnit')}
                      value={values.organizationUnit}
                      name="organizationUnit"
                      InputProps={{
                        readOnly: readValue,
                      }}
                    />
                    {errorInfo.organizationUnit === true ? (
                      <div id="nameid" className="addfieldserror">
                        "Enter only alphabets"
                      </div>
                    ) : (
                      ''
                    )}
                     {submitErr&&!values.organizationUnit? (
                        <div id="nameid" className="addfieldserror">
                          Please enter organization unit
                        </div>
                      )
                       : ""}
                  </Grid>
                  <Grid item xs={12} sm={3}>
                    <TextField
                      id="outlined-dense"
                      label="Name"
                      className="partnertextField"
                      margin="dense"
                      variant="outlined"
                      onChange={handleChange('corporationName')}
                      value={values.corporationName}
                      name="corporationName"
                      InputProps={{
                        readOnly: readValue,
                      }}
                    />
                    {errorInfo.corporationName === true ? (
                      <div id="nameid" className="addfieldserror">
                        "Enter only alphabets"
                      </div>
                    ) : (
                      ''
                    )}
              {submitErr&&!values.corporationName? (
                        <div id="nameid" className="addfieldserror">
                          Please enter name
                        </div>
                      )
                       : ""}
                  </Grid>
                  <Grid item xs={12} sm={3}>
                    <TextField
                      id="outlined-dense"
                      label="Description "
                      className="partnertextField"
                      margin="dense"
                      variant="outlined"
                      onChange={handleChange('partnerProfileDescription')}
                      value={values.partnerProfileDescription}
                      name="partnerProfileDescription"
                    />
                  </Grid>
                  <Grid item xs={12} sm={3}>
                    <FormControl>
                      <InputLabel
                        style={{
                          align: 'center',
                          color: '#0b153e',
                          fontSize: 12,
                        }}
                        id="demo-simple-select-label"
                      >
                        Preferred Protocol
                      </InputLabel>
                      <Select
                        className="groupdropdown"
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        // value={searchPartner.prefferedProtocolUUID}
                        value={values.prefferedProtocolUUID}
                        onChange={handleChange('prefferedProtocolUUID')}
                        name="prefferedProtocolUUID"
                      >
                        <MenuItem value={null}>{'select'}</MenuItem>

                        {protocolData != undefined && protocolData.length > 0
                          ? protocolData.map((v, k) => {
                              return (
                                <MenuItem value={v.protocolTypeUUID}>
                                  {v.protocolTypeName}
                                </MenuItem>
                              );
                            })
                          : null}
                        {/*{address != undefined && address.length > 0
                              ? address.map((v, k) => {
                                  return (
                                    <MenuItem value={v.prefferedProtocolUUID}>
                                      {v.prefferedProtocolUUID}
                                    </MenuItem>
                                  );
                                })
                              : null}
                              */}
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item xs={12} sm={3}>
                    <FormControlLabel
                      label="Enable"
                      control={
                        <Switch
                          checked={values.status === 'Active' ? true : false}
                          onChange={handleChange('status')}
                          name="status"
                        />
                      }
                    />
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={3}
                    style={{
                      display: props.title !== 'Add' ? 'none' : 'block',
                    }}
                  >
                    <FormControl disabled={readValue}>
                      <InputLabel
                        style={{
                          align: 'center',
                          color: '#0b153e',
                          fontSize: 12,
                        }}
                        id="demo-simple-select-label"
                      >
                        Type
                      </InputLabel>
                      <Select
                        className="groupdropdown"
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        // value={searchPartner.prefferedProtocolUUID}
                        value={values.profileIDTypesUUID}
                        onChange={handleChange('profileIDTypesUUID')}
                        name="profileIDTypesUUID"
                      >
                        {profileIDTypeData != undefined &&
                        profileIDTypeData.length > 0
                          ? profileIDTypeData.map((v, k) => {
                              return (
                                <MenuItem value={v.profileIDTypesUUID}>
                                  {v.profileIDTypeName}
                                </MenuItem>
                              );
                            })
                          : null}
                        {/*{address != undefined && address.length > 0
                              ? address.map((v, k) => {
                                  return (
                                    <MenuItem value={v.prefferedProtocolUUID}>
                                      {v.prefferedProtocolUUID}
                                    </MenuItem>
                                  );
                                })
                              : null}
                              */}
                      </Select>
                    </FormControl>
                    {submitErr&&!values.profileIDTypesUUID? (
                        <div id="nameid" className="addfieldserror">
                          Please select type
                        </div>
                      )
                       : ""}
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={3}
                    style={{
                      display: props.title !== 'Add' ? 'none' : 'block',
                    }}
                  >
                    <TextField
                      id="outlined-dense"
                      label="Value "
                      className="partnertextField"
                      margin="dense"
                      variant="outlined"
                      //placeholder=""
                      onChange={handleChange('profileIDValue')}
                      value={values.profileIDValue}
                      name="profileIDValue"
                      InputProps={{
                        readOnly: readValue,
                      }}
                    />
                    {errorInfo.profileIDValue === true ? (
                      <div id="nameid" className="addfieldserror">
                        "Special characters are not allowed"
                      </div>
                    ) : (
                      ''
                    )}
                    {submitErr&&!values.profileIDValue? (
                        <div id="nameid" className="addfieldserror">
                        Please enter value
                        </div>
                      )
                       : ""}
                  </Grid>
                </Grid>
                <Grid container spacing={3} className="userformcontent">
                  <Grid item xs={6} sm={6}>
                    <Button
                      style={{
                        color: '#000006',
                        backgroundColor: '#E5CCFF',
                        display: props.title !== 'Add' ? 'none' : 'block',
                      }}
                      onClick={handleReset}
                      variant="contained"
                      fullWidth="true"
                      className="createpartnerbutton"
                    >
                      RESET
                    </Button>
                  </Grid>
                  <Grid item xs={6} sm={6}>
                    <Button
                      onClick={handleSubmit}
                      vvariant="contained"
                      fullWidth="true"
                      className="createpartnerbutton"
                    >
                      {props.title !== 'Add' ? 'UPDATE' : 'SAVE'}
                    </Button>
                  </Grid>
                </Grid>
              </form>
            </div>
          </div>
        </Modal>
      </MuiThemeProvider>
    </div>}
</AuthContext.Consumer>
  );

}
