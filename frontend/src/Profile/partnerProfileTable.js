import MaterialTable from 'material-table';
import { forwardRef, useEffect } from 'react';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import ReactDOM from 'react-dom';
import MTableToolbar from 'material-table';
import React, { Component } from 'react';
import ModalFab from '../components/modalfabtms';
import AddPartnerProfile from './partnerprofileconfig';
import Loader from '../components/loader';
import Error from '../components/emptyPage';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Button from '@material-ui/core/Button';
import Timestamp from '../timestamp';
import Theme from '../css/theme';
import FourKIcon from '@material-ui/icons/FourK';
import Checkbox from '@material-ui/core/Checkbox';
import Select from '@material-ui/core/Select';
//import Autocomplete from '@material-ui/lab/Autocomplete';
import MenuItem from '@material-ui/core/MenuItem';
import { TextField } from '@material-ui/core';
import SendIcon from '@material-ui/icons/Send';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import APIstatusView from '../components/tmsmodal';
import APIstatus from './apiStatusView';
import VisibilityIcon from '@material-ui/icons/Visibility';
import LaunchIcon from '@material-ui/icons/Launch';
import { withStyles } from '@material-ui/core/styles';
import Menu from '@material-ui/core/Menu';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import AddIcon from '@material-ui/icons/Add';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import '../css/commonStyle.css';
import ProfileForm from './partnerProfileForm';
import PartnerProfileTableForm from '../components/tmsmodal';
import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles,
} from '@material-ui/core/styles';
import { useQuery, useMutation, useManualQuery } from 'graphql-hooks';
import AuthContext from '../authUtil/Auth';

/*const API_CAll_DATA = `query allPartnerDetailsAPI($SearchInput:String){ 
  allPartnerDetailsAPI(search:$SearchInput){
    success
  }
}`;*/
const PARTNER_BY_ORGUUID = `query fetchPartnerProfileByOrganization($organizationUUID:String){
  fetchPartnerProfileByOrganization(organizationUUID:$organizationUUID) {
    partnerProfileUUID    
    organizationUUID
    corporationName
    partnerProfileDescription
    organizationUnit
    status
    prefferedProtocolUUID
    createdOn
    lastUpdatedOn
    APIStatus
    APIStatusMsg
    APIPartnerID
  }
}
`;
const API_CAll_DATA = `query allPartnerDetailsAPI($SearchInput:String){ 
  allPartnerDetailsAPI(search:$SearchInput){
    statusAPI
    msg
    partnerID
  }
}`;
const DISPLAY_PROTOCOL_TYPE = `{
  allProtocolType{
    protocolTypeUUID
    protocolTypeName
  }
}`;

const DISPLAY_PARTNERS = `{
  allPartners{
    partnerProfileUUID    
    organizationUUID
    corporationName
    partnerProfileDescription
    organizationUnit
    status
    prefferedProtocolUUID
    createdOn
    lastUpdatedOn
    APIStatus
    APIStatusMsg
    APIPartnerID
  }
}`;
const DISPLAY_PARTNERS_WITH_SEARCH_go = `mutation allSearchPartners($SearchInput:PartnerInput){
  allSearchPartners(search:$SearchInput){
     partnerProfileUUID
    organizationUUID
    corporationName
    partnerProfileDescription
    organizationUnit
    status
    prefferedProtocolUUID
    createdOn
    lastUpdatedOn
  }
}`;
const DISPLAY_PARTNERS_WITH_SEARCH = `query allPartners($SearchInput:String){
  allPartners(search:$SearchInput){
     partnerProfileUUID
    organizationUUID
    corporationName
    partnerProfileDescription
    organizationUnit
    status
    prefferedProtocolUUID
    createdOn
    lastUpdatedOn
  }
}`;

const ADD_PARTNERS = `mutation createPartner($inputCreate:PartnerInput){
    createPartner(input:$inputCreate){
      corporationName
    organizationUUID
      partnerProfileDescription
      organizationUnit
      status
      prefferedProtocolUUID
      partnerProfileUUID
    }
}`;

const UPDATE_PARTNERS = `mutation updatePartner($inputCreate:PartnerInput){
  updatePartner(input:$inputCreate){
    corporationName
    organizationUUID
    partnerProfileDescription
    organizationUnit
    status
    prefferedProtocolUUID
  }
}`;
const DELETE_PARTNERS = `mutation deletePartner($partnerProfileUUID:String){
  deletePartner(input:$partnerProfileUUID){
    corporationName
  }
}`;

const DISPLAY_All_ORGANIZATION = `query fetchOrganization($searchInput:String){
  fetchOrganization(searchInput:$searchInput) {
    idOrganization
    organizationUUID
    organizatonName
    organizationDescription
    createdOn
    lastUpdatedOn
  } 
}
`;
const StyledMenu = withStyles({
  paper: {
    border: '1px solid #d3d4d5',
  },
})((props) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'center',
    }}
    {...props}
  />
));

const StyledMenuItem = withStyles((theme) => ({
  root: {
    '&:focus': {
      //backgroundColor: theme.palette.primary.main,
      '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
        color: theme.palette.common.white,
      },
    },
  },
}))(MenuItem);
function iconStyles() {
  return {};
}

export default function PartnerProfileTable(props) {
  console.log("props",props,"props.OrgUUID",props.OrgUUID)
  const [status, setStatus] = React.useState({
    Active: true,
    Inactive: false,
  });
  const [organizationList, setOrganizationList] = React.useState([]);
  const [partnerProfiles, setPartnerProfiles] = React.useState([]);
  const [partnerForm, setPartnerForm] = React.useState('update');
  const [protocolType, setProtocolType] = React.useState([]);
  const [openProfileModal, setOpenProfileModal] = React.useState(false);
  const [aPIMsg, setAPIMsg] = React.useState('');
  const { loading: orgloading, error: orgerror, data: orgData } = useQuery(
    DISPLAY_All_ORGANIZATION,
    {
      variables: {
        searchInput: '',
      },
    }
  );
 /* const { loading, data, errors, networkError, refetch } = useQuery(
    DISPLAY_PARTNERS
  );*/
  const { loading, data, errors, networkError, refetch } = useQuery(
    PARTNER_BY_ORGUUID,
    {
      variables:{
        organizationUUID:props.OrgUUID!==undefined?props.OrgUUID:''
      }
    }
  );
 
  const [
    fetchProfile,
    { loading: pTaloading, data: pTadata, errors: pTaerrors },
  ] = useManualQuery(API_CAll_DATA);
  const {
    loading: sloading,
    data: sdata,
    errors: sTerrors,
    networkError: sTnetworkError,
    refetch: srefetch,
  } = useQuery(DISPLAY_PARTNERS_WITH_SEARCH, {
    variable: {
      SearchInput: '',
    },
  });
  const {
    loading: pTloading,
    data: pTdata,
    errors: pTerrors,
    networkError: pTnetworkError,
  } = useQuery(DISPLAY_PROTOCOL_TYPE);
  const [
    addPartner,
    {
      loading: ploading,
      data: pdata,
      errors: perrors,
      networkError: anetworkError,
    },
  ] = useMutation(ADD_PARTNERS);
  const [
    updatePartner,
    {
      loading: uploading,
      data: updata,
      errors: uperrors,
      networkError: unetworkError,
    },
  ] = useMutation(UPDATE_PARTNERS);
  const [
    deletePartner,
    {
      loading: dploading,
      data: dpdata,
      errors: dperrors,
      networkError: dnetworkError,
    },
  ] = useMutation(DELETE_PARTNERS);
  //var LastUpdatedOn = Format.asString('yyyy-MM-dd hh:mm:ss', new Date());
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [message, setSnackbarMessage] = React.useState();
  const [viewopen, setViewForm] = React.useState(false);
  const [APImessage, setAPImessage] = React.useState();
  const [variant, setSnackbarVariant] = React.useState('error');
  const [serverError, setServerError] = React.useState(false);
  const [rowInfo, setRowDetails] = React.useState();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [rowData, setrowData] = React.useState();

  const handleClick = (event, rowData) => {
    setAnchorEl(event.currentTarget);
    setrowData(rowData);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const [nameError, setNameError] = React.useState({
    error: false,
    label: '',
    helperText: '',
    validateInput: false,
  });
  const columns = [
    {
      title: 'Organization',
      field: 'organizationUUID',
      lookup: organizationList,
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              //placeholder="Description"
              margin="normal"
              select
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            >
              {orgData != undefined && orgData.fetchOrganization.length > 0
                ? orgData.fetchOrganization.map((v, k) => {
                    return (
                      <MenuItem value={v.organizationUUID}>
                        {v.organizatonName}
                      </MenuItem>
                    );
                  })
                : null}
            </TextField>
          </div>
        );
      },
    },
    {
      title: 'Organization Unit',
      field: 'organizationUnit',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder="Organization Unit"
              margin="normal"
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            />

            <div style={{ color: 'red' }}>
              {!props.rowData.organizationUnit ||
              /^[a-zA-Z ]+$/.test(props.rowData.organizationUnit)
                ? ''
                : 'Enter only alphabets'}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.organizationUnit}</div>;
      },
    },
    {
      title: 'Name',
      field: 'corporationName',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder="Name"
              margin="normal"
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            />

            <div style={{ color: 'red' }}>
              {!props.rowData.corporationName ||
              /^[a-zA-Z ]+$/.test(props.rowData.corporationName)
                ? ''
                : 'Enter only alphabets'}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.corporationName}</div>;
      },
    },
    {
      title: 'Description',
      field: 'partnerProfileDescription',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder="Description"
              margin="normal"
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            />
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.partnerProfileDescription}</div>;
      },
    },
    {
      title: 'Preferred Protocol',
      field: 'prefferedProtocolUUID',
      lookup: protocolType,
    },
    {
      title: 'UUID',
      field: 'partnerProfileUUID',
      editable: 'never',
      hidden: true,
    },
    /*{
      title: 'Disable',
      // field: 'Status',
      render: (rowData) => {
        // console.log(props);

        return (
          <FormControlLabel
            style={{ backgroundColor: 'transparent' }}
            control={
              <Switch
                color="primary"
                onClick={() => handleShowComponent1(rowData)}
                checked={
                  rowData === undefined
                    ? false
                    : rowData.status === 'Inactive'
                    ? false
                    : true
                }
              />
            }
          />
        );
      },
    },*/
    {
      title: 'Enable',
      field: 'status',
      editComponent: (props) => {
        //  console.log(props);
        var statusVal;
        if (props.value == 'Active' || props.value == true) {
          statusVal = true;
        } else {
          statusVal = false;
        }
        return (
          <FormControlLabel
            style={{ backgroundColor: 'transparent' }}
            control={
              <Switch
                color="primary"
                onChange={(v) => props.onChange(v.target.checked)}
                checked={statusVal}
              />
            }
          />
        );
      },
      render: (rowData) => {
        // console.log(rowData);
        return (
          <FormControlLabel
            style={{ backgroundColor: 'transparent' }}
            control={
              <Switch
                color="primary"
                onClick={() => handleShowComponent1(rowData)}
                checked={
                  rowData === undefined
                    ? false
                    : rowData.status === 'Inactive'
                    ? false
                    : true
                }
              />
            }
          />
        );
      },
    },
    {
      title: 'API Status',

      // field: 'Status'
      render: (rowData) => {
        //console.log(rowData, rowData.APIStatus);
        if (rowData === undefined) {
          return '';
        } else if (
          rowData.APIStatus == null ||
          rowData.APIStatus === 'Inactive'
        ) {
          return <div style={{ color: 'red' }}>Failure</div>;
        } else {
          return <div style={{ color: 'green' }}>Success</div>;
        }
      },
    },
    {
      title: 'Created',
      field: 'createdOn',
      editable: 'never',
      // hidden: true,
      render: (rowData) => {
        //  console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.createdOn);
        } else {
          return;
        }
      },
    },

    {
      title: 'Last Modified',
      field: 'lastUpdatedOn',
      editable: 'never',
      //hidden: true,
      render: (rowData) => {
        // console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.lastUpdatedOn);
        } else {
          return;
        }
      },
    },
  ];
  const [state, setState] = React.useState({
    data: partnerProfiles,
  });

  const [state1, setState1] = React.useState({
    enable: true,
  });
  const handleChange = (event) => {
    setState1({ ...state1, [event.target.name]: event.target.checked });
  };
  const handleRefetch = (value, message, modelClose) => {
    refetch({
      updateData(_, data) {
        return data;
      },
    });
    console.log(value, message);
    setOpenSnackbar(true);
    setSnackbarMessage(message);
    if (value) {
      setSnackbarVariant('error');
    } else {
      setSnackbarVariant('success');
    }
    if (modelClose) {
      handleClosePartner();
    }
  };
  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }
  var obj = {};
  useEffect(() => {
    console.log('pTadata', pTadata);
    /* if (pTadata !== undefined) {
      console.log(pTadata.allPartnerDetailsAPI);
      handleAPISuccess(
        pTadata.allPartnerDetailsAPI.statusAPI,
        pTadata.allPartnerDetailsAPI.msg
      );
      //setAPIMsg(pTadata.allPartnerDetailsAPI.success);
      //setTimeout( handleAPISuccess(pTadata.allPartnerDetailsAPI.success),500);
    }*/
    if (data !== undefined) {
      /*console.log(data.allPartners);
      setPartnerProfiles(data.allPartners);*/
      console.log(data.fetchPartnerProfileByOrganization);
      setPartnerProfiles(data.fetchPartnerProfileByOrganization);
      setServerError(false);
    } else {
      console.log('server error');
      setServerError(true);
    }
    if (orgData !== undefined) {
      console.log(orgData.fetchOrganization);
      var orgDD = orgData.fetchOrganization.reduce(
        (obj, item) => (
          (obj[item.organizationUUID] = [item.organizatonName]), obj
        ),
        {}
      );
      console.log('orgDD', orgDD);
      setOrganizationList(orgDD);
    }
    if (pTdata !== undefined) {
      obj = pTdata.allProtocolType.reduce(function (acc, cur, i) {
        acc[null] = 'Select';
        acc[cur.protocolTypeUUID] = cur.protocolTypeName;
        return acc;
      }, {});
      setProtocolType(obj);
      console.log(obj, 'potocol');
    }
  }, [data, pTdata, orgData, serverError]);

  const classes = makeStyles(iconStyles)();
  const tableIcons = {
    Add: forwardRef((props, ref) => (
      <AddIcon color="primary" {...props} ref={ref} />
    )),

    Check: forwardRef((props, ref) => (
      <Check color="primary" {...props} ref={ref} />
    )),
    Clear: forwardRef((props, ref) => (
      <Clear color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline color="error" {...props} ref={ref} />
    )),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => (
      <Edit color="primary" {...props} ref={ref} />
    )),
    Export: forwardRef((props, ref) => (
      <SaveAlt color="primary" {...props} ref={ref} />
    )),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => (
      <FirstPage color="primary" {...props} ref={ref} />
    )),
    LastPage: forwardRef((props, ref) => (
      <LastPage color="primary" {...props} ref={ref} />
    )),
    NextPage: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft color="primary" {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => (
      <Clear color="primary" {...props} ref={ref} />
    )),
    Search: forwardRef((props, ref) => (
      <Search color="primary" {...props} ref={ref} />
    )),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };

  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSvgIcon: {
          fontSizeSmall: {
            color: '#0b153e',
            fontSize: '15px',
          },
        },

        MuiListItemIcon: {
          root: {
            minWidth: '30px',
          },
        },

        MuiTypography: {
          h6: {
            fontSize: 14,
            fontFamily: 'Arial !important',
          },
          body1: {
            fontSize: '10px',
          },
        },
        MuiSelect: {
          select: {
            minWidth: 100,
          },
          icon: {
            color: '#0b153e',
          },
        },
        MuiTab: {
          root: {
            padding: '10px',
          },
        },
      },
    });
  function handleShowComponent1(rowData) {
    console.log(rowData.status);
    if (rowData !== undefined) {
      if (rowData.status === 'Active') {
        rowData.status = false;
        console.log(rowData);
        handleUpdatePartner(rowData);
      } else {
        rowData.status = true;
        handleUpdatePartner(rowData);
      }
    }
  }
  async function handleCreatePartner(newData) {
    console.log(newData);
    return await new Promise(async (resolve) => {
      console.log(newData.prefferedProtocolUUID);
      await addPartner({
        variables: {
          inputCreate: {
            corporationName: newData.corporationName,
            organizationUUID: newData.organizationUUID,
            partnerProfileDescription: newData.partnerProfileDescription,
            organizationUnit: newData.organizationUnit,
            status: newData.status == true ? 'Active' : 'Inactive',
            prefferedProtocolUUID:
              newData.prefferedProtocolUUID == 'null'
                ? null
                : newData.prefferedProtocolUUID,
          },
        },
      })
        .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
          console.log(data, error, graphQLErrors, networkError, cacheHit);
          if (data !== null && error == false) {
            //handleRefetch(error, 'Saved successfully');
            console.log(data.createPartner[0]);
            handleClickRoute(true, data.createPartner[0]);
          } else if (error && graphQLErrors.length > 0) {
            let duperror = graphQLErrors[0].message;
            console.log(duperror);
            if (duperror !== null && duperror.indexOf('ER_DUP_ENTRY') !== -1) {
              handleRefetch(error, 'Name already existed');
            }
          }
        })
        .catch((e) => {
          // you can do something with the error here
          console.log(e);
        });
    });
  }

  async function handleUpdatePartner(newData) {
    console.log(newData);
    return await new Promise(async (resolve) => {
      await updatePartner({
        variables: {
          inputCreate: {
            corporationName: newData.corporationName,
            partnerProfileDescription: newData.partnerProfileDescription,
            organizationUnit: newData.organizationUnit,
            status: newData.status == true ? 'Active' : 'Inactive',
            prefferedProtocolUUID:
              newData.prefferedProtocolUUID == 'null'
                ? null
                : newData.prefferedProtocolUUID,
            partnerProfileUUID: newData.partnerProfileUUID,
          },
        },
      })
        .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
          console.log(data, error, graphQLErrors, networkError, cacheHit);
          if (data !== null && error == false) {
            handleRefetch(error, 'Updated successfully');
          } else if (error && graphQLErrors.length > 0) {
            let duperror = graphQLErrors[0].message;
            console.log(duperror);
            if (duperror !== null && duperror.indexOf('ER_DUP_ENTRY') !== -1) {
              handleRefetch(error, 'Name already existed');
            }
          }
        })
        .catch((e) => {
          // you can do something with the error here
          console.log(e);
        });
    });
  }

  async function handleDeletePartner(oldData) {
    console.log('oldData', oldData);
    return await new Promise(async (resolve) => {
      await deletePartner({
        variables: {
          partnerProfileUUID: oldData.partnerProfileUUID,
        },
      })
        /*if (dploading) {
        console.log('Partner not deleted');
        handleRefetch(dploading, 'Graphql hooks error');
      } else {
        console.log('Partner deleted successfully');
        handleRefetch(dploading, 'Deleted successfully');
      }*/
        .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
          console.log(data, error, graphQLErrors, networkError, cacheHit);
          if (data !== null && error == false) {
            handleRefetch(error, 'Deleted successfully');
          } else if (error && graphQLErrors.length > 0) {
            let duperror = graphQLErrors[0].message;
            console.log(duperror);
            if (
              duperror !== null &&
              duperror.indexOf('ER_ROW_IS_REFERENCED') !== -1
            ) {
              handleRefetch(
                error,
                'Selected row is referenced in Organization interface table'
              );
            }
          }
        })
        .catch((e) => {
          // you can do something with the error here
          console.log(e);
        });
    });
  }
  const handleAPISuccess = (Status, aPIMsg) => {
    console.log('MsgAPI', aPIMsg);
    //setOpenSnackbar(true);
    /// setSnackbarMessage(aPIMsg);

    if (Status === 'failure') {
      handleRefetch(true, aPIMsg);
      // setSnackbarVariant('error');
    } else {
      //setSnackbarVariant('success');
      handleRefetch(false, aPIMsg);
    }
    // handleRefetchAPI();
  };

  const handleCallAPI = async (event, rowData) => {
    console.log('rowData', rowData);
    console.log('rowData', rowData);
    setAnchorEl(null);
    await fetchProfile({
      variables: {
        SearchInput: rowData.partnerProfileUUID,
      },
    }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
      console.log(data, error, graphQLErrors, networkError, cacheHit);
      if (data !== null && error == false) {
        handleAPISuccess(
          data.allPartnerDetailsAPI.statusAPI,
          data.allPartnerDetailsAPI.msg
        );
      } else {
        handleRefetch(error, 'graphql hooks error');
      }
    });
  };

  const handleViewAPI = async (event, rowData) => {
    console.log('APIVIEWFUNCTION', rowData);
    setAnchorEl(null);
    setRowDetails(rowData);
    setViewForm(true);
  };

  const handleClickRoute = (event, rowData, results) => {
    console.log(event, rowData, results);
    setAnchorEl(null);
    props.handleShowComponent(true, rowData.partnerProfileUUID, results);
  };
  const handleCloseAPI = () => {
    setViewForm(false);
  };

  const handleClickUpdateOpen = (e, rowData) => {
    console.log(e, rowData);
    setPartnerForm('Update');
    setrowData(rowData);
    setOpenProfileModal(true);
  };
  const handleClickAddOpen = (e) => {
    setPartnerForm('Add');
    setOpenProfileModal(true);
  };
  const handleClosePartner = (e) => {
    setOpenProfileModal(false);
  };

  if (loading) return <Loader />;
  if (errors || perrors || uperrors) return <Error />;
  if (serverError)
    return <Error type={'Server connection lost.Please try again'} />;
  return (
    <AuthContext.Consumer>
    { context=>
    <MuiThemeProvider theme={getMuiTheme()}>{console.log("contex",context)}
      <MaterialTable
        icons={tableIcons}
        title="Partner Profile"
        columns={columns}
        data={partnerProfiles}
        // onRowClick={(event, rowData) => handleClickRoute(event, rowData)}
        editable={{
          // onRowAdd: (newData) => handleCreatePartner(newData),
          /* onRowAdd: (newData) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                newData.submitted = true;
                if (
                  !newData.corporationName ||
                  !newData.organizationUnit ||
                  !newData.organizationUUID ||
                  !/^[a-zA-Z\s]+$/.test(newData.corporationName) ||
                  !/^[a-zA-Z\s]+$/.test(newData.organizationUnit)
                ) {
                  setNameError({
                    error: true,
                    label: 'required',
                    helperText: 'Required.',
                    validateInput: true,
                  });
                  reject();
                  return;
                }
                resolve();
                if(newData.partnerProfileUUID==null)
              
                handleCreatePartner(newData);
              }, 600);
            }),
     
          onRowUpdate: (newData) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                newData.submitted = true;
                if (
                  !newData.corporationName ||
                  !newData.organizationUnit ||
                  !newData.organizationUUID ||
                  !/^[a-zA-Z\s]+$/.test(newData.corporationName) ||
                  !/^[a-zA-Z\s]+$/.test(newData.organizationUnit)
                ) {
                  setNameError({
                    error: true,
                    label: 'required',
                    helperText: 'Required.',
                    validateInput: true,
                  });
                  reject();
                  return;
                }
                resolve();
                handleUpdatePartner(newData);
              }, 600);
            }),*/
          onRowDelete: (oldData) => handleDeletePartner(oldData),
        }}
        actions={[
          {
            icon: MoreVertIcon,
            tooltip: 'More',
            onClick: (event, rowData) => handleClick(event, rowData),
          },
          {
            icon: AddIcon,
            tooltip: 'Add',
            className: 'addIconColor',
            isFreeAction: true,
            onClick: (event) => handleClickAddOpen(event),
          },
          {
            icon: Edit,
            tooltip: 'Edit',
            className: 'addIconColor',
            onClick: (event, rowData) => handleClickUpdateOpen(event, rowData),
          },
          /*
          {
            icon: SendIcon,
            tooltip: 'Send API',
            onClick: (event, rowData) => handleCallAPI(event, rowData),
            
          },
          {
            icon: VisibilityIcon,
            tooltip: 'Quick View',
            onClick: (event, rowData) => handleViewAPI(event, rowData),
            
          },
          {
            icon: LaunchIcon,
            tooltip: 'Detailed Infromation',
            onClick: (event, rowData) => handleClickRoute(event, rowData),
          },*/
        ]}
        options={{
          headerStyle: {
            textAlign: 'center',
            fontSize: 12,
            fontWeight: 'bold',
            fontFamily: 'Arial !important',
            backgroundColor: '#0b153e',
            color: '#ffffff',
            padding: '4px',
          },
          searchFieldStyle: {
            color: '#0b153e',
          },
          actionsColumnIndex: -1,
          pageSize: 10,
          pageSizeOptions: [10, 25, 50, 100],
          toolbar: true,
          paging: true,
        }}
      />
      <div>
        <StyledMenu
          id="customized-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
        >
          <StyledMenuItem
            rowData={rowData}
            onClick={() => handleCallAPI(true, rowData)}
          >
            <ListItemIcon>
              <SendIcon fontSize="small" />
            </ListItemIcon>
            <ListItemText primary="Send API" />
          </StyledMenuItem>
          <StyledMenuItem
            rowData={rowData}
            onClick={() => handleViewAPI(true, rowData)}
          >
            <ListItemIcon>
              <VisibilityIcon fontSize="small" />
            </ListItemIcon>
            <ListItemText primary="API Response" />
          </StyledMenuItem>
          <StyledMenuItem
            rowData={rowData}
            onClick={() => handleClickRoute(true, rowData)}
          >
            <ListItemIcon>
              <LaunchIcon fontSize="small" />
            </ListItemIcon>
            <ListItemText primary="Details" />
          </StyledMenuItem>
        </StyledMenu>
      </div>
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        autoHideDuration={1500}
        open={openSnackbar}
        onClose={handleCloseSnackbar}
      >
        <MySnackbarContentWrapper
          onClose={handleCloseSnackbar}
          variant={variant}
          message={message}
        />
      </Snackbar>
      <APIstatusView
        handleCloseModal={handleCloseAPI}
        isModalOpen={viewopen}
        title={'Status'}
      >
        <APIstatus
          handleCloseModal={handleCloseAPI}
          isModalOpen={viewopen}
          rowData={rowInfo}
        />
      </APIstatusView>
      {/* <ModalFab title="Partner Profile Config">
        <AddPartnerProfile />
      </ModalFab>*/}
      <PartnerProfileTableForm
        handleCloseModal={handleClosePartner}
        isModalOpen={openProfileModal}
      >
        <ProfileForm
          handleCloseModal={handleClosePartner}
          onSuccess={handleRefetch}
          handleClickRoute={handleClickRoute}
          isModalOpen={openProfileModal}
          rowData={rowData}
          uuid={props.uuid}
          OrgUUID={props.OrgUUID}
          title={partnerForm}
        />
      </PartnerProfileTableForm>
    </MuiThemeProvider>}
</AuthContext.Consumer>
  );
}
