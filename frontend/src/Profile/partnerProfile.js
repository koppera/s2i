import React, { useEffect } from 'react';
import { makeStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { useMutation, useQuery } from 'graphql-hooks';
import Button from '@material-ui/core/Button';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import Grid from '@material-ui/core/Grid';
import '../css/commonStyle.css';
import Typography from '@material-ui/core/Typography';
import PartnerProfileTable from './partnerProfileTable';
import PartnerProfileDetails from '../partnerprofiledetails/partnerprofiledetails';
import AuthContext from '../authUtil/Auth';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
}));
export default function PartnerProfile() {
  const classes = useStyles();
  const [showDetails, setShowDetails] = React.useState(false);
  const [ppUUID, setPPUUID] = React.useState(false);
  const [resultVal, setResults] = React.useState();
  useEffect(() => {}, [ppUUID, resultVal]);
  const handleShowComponent = (val, uuid, results) => {
    console.log(results);
    setShowDetails(val);
    setPPUUID(uuid);
    setResults(results);
  };
  const setProfileUUID = (uuid) => {
    console.log(uuid);
    setPPUUID(uuid);
  };
 
  return (
    <AuthContext.Consumer>
    { context=>
   <div className={classes.root}>{console.log("contex",context)}
      <Grid container spacing={3}>
        {showDetails === false ? (
          <Grid item xs={12}>
            <PartnerProfileTable handleShowComponent={handleShowComponent} OrgUUID={context.state.Organization}/>
          </Grid>
        ) : (
          <Grid item xs={12}>
            <PartnerProfileDetails
              handleShowComponent={handleShowComponent}
              uuid={ppUUID}
              profilUUID={setProfileUUID}
              resultVal={resultVal}
            />
          </Grid>
        )}
      </Grid>
    </div>}
     </AuthContext.Consumer>
  );
 
}
