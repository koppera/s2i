import React, { useEffect } from 'react';
import { makeStyles, MuiThemeProvider,createMuiTheme, } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Grid from "@material-ui/core/Grid";
import '../css/commonStyle.css';
import TextField from '@material-ui/core/TextField';
import Theme from '../css/theme';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';

const useStyles = makeStyles(theme => ({
  }));

  const getMuiTheme = () =>
  createMuiTheme(Theme, {
    overrides: {
        MuiSelect:{
            select:{
                minWidth:180,
                marginTop:'8%'
            }
        },
        MuiTypography:{
            body1:{
                fontSize:12,
                fontWeight:200

        }
    }
    }
  });
export default function Switches() {
    const classes = useStyles();
    const [TypeUUID, setTypeUUID] = React.useState('');
    const [ContactTypeUUID, setContactTypeUUID] = React.useState('');
    const [ContactTypeUUID1, setContactTypeUUID1] = React.useState('');
    const [age, setAge] = React.useState('');
    const [state, setState] = React.useState({
      checkedA: true,
      checkedB: true,
    });
    const [state1, setState1] = React.useState({
        checkedA: true,
        checkedB: true,
      });

    const handleChange1 = (event) => {
        setTypeUUID(event.target.value);
      };
   

      const handleChange = (event) => {
        setContactTypeUUID1(event.target.value);
      };
    
      const handleChange2 = (event) => {
        setAge(event.target.value);
      };
     
      const handleChange3 = (event) => {
        setContactTypeUUID(event.target.value);
      };
      const handleChange4 = (event) => {
        setState({ ...state, [event.target.name]: event.target.checked });
      };
    
      const handleChange5 = (event) => {
        setState1({ ...state1, [event.target.name]: event.target.checked });
      };
    
  
                
            
    return (  
        <MuiThemeProvider theme={getMuiTheme()}>
      <div>
              
          <form className="configcontainer" noValidate autoComplete="off">
            <Grid className="configGrid">
                <Grid container spacing={3}>
                    <Grid sm={3} xs={3}>
                         <TextField
                            id="standard-name"
                            label="OrganisationUnit "
                            className="configtextfield"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            margin="normal"
                            
                          />
                      </Grid>
                   <Grid sm={3} xs={3}>
                       <TextField                      
                            id="standard-name"
                            label="CorporationName"
                            className="configtextfield"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            margin="normal"
                         />  
                   </Grid>
                   <Grid sm={3} xs={3}>
                      <TextField                      
                            id="standard-name"
                            label="Partner Profile Description"
                            className="configtextfield"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            margin="normal"
                        /> 
                    </Grid>
                  <Grid sm={3} xs={3}>
                     <FormControl className={classes.formControl}>
                       <InputLabel style={{fontSize:'14px'}} id= "demo-simple-select-label">PrefferedProtocolUUID</InputLabel>
                         <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={age}
                            onChange={handleChange2}                   >
                                <MenuItem value={10}>TMS</MenuItem>
                                <MenuItem value={20}>Changemaker</MenuItem>
                                <MenuItem value={30}>Cloudgen</MenuItem>
                          </Select>   
                     </FormControl>
                   </Grid>
                <Grid sm={3} xs={3}>
                     <FormControlLabel
                        label="Partner Profile Status"
                        control={<Switch checked={state1.checkedA} onChange={handleChange5} name="checkedA" />}
                    />
              </Grid>
      
        <Grid sm={3} xs={3}>
          <FormControl className={classes.formControl}>
            <InputLabel style={{fontSize:'14px'}} id="demo-simple-select-label">ContactTypeUUID</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={ContactTypeUUID}
                onChange={handleChange3}
                    >
            <MenuItem value={10}>CNP</MenuItem>
            <MenuItem value={20}>TMS</MenuItem>
            <MenuItem value={30}>Cloudgen</MenuItem>
            </Select>
         </FormControl>
         </Grid>

          <Grid sm={3} xs={3}>             
            <TextField 
                    id="standard-name"
                    label="ProfileIDVaue Name"
                    className="configtextfield"
                    InputLabelProps={{
                    shrink: true,
                    }}
                    margin="normal"
                />                      
                              
             </Grid>
            <Grid sm={3} xs={3}>
                 <TextField
                    id="standard-name"
                    label="ProfileIDVaue Description"
                    className="configtextfield"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    margin="normal"
                  />                       
                            
              </Grid>
               
            <Grid sm={3} xs={3}>
                <TextField                      
                      id="standard-name"
                      label="AddressName"
                      className="configtextfield"
                      InputLabelProps={{
                        shrink: true,
                      }}
                      margin="normal"
                 /> 
             </Grid>
             <Grid sm={3} xs={3}>
               <TextField
                      id="standard-name"
                      label="Line1"    
                      className="configtextfield"
                      InputLabelProps={{
                        shrink: true,
                      }}
                      margin="normal"
                    /> 
            </Grid>
             <Grid sm={3} xs={3}>
              <TextField
                      id="standard-name"
                      label="Line2"
                      className="configtextfield"
                      InputLabelProps={{
                        shrink: true,
                      }}
                      margin="normal"
                /> 
             </Grid>
                    <Grid sm={3} xs={3}>
                        <TextField
                        id="standard-name"
                        label="Line3"    
                        className="configtextfield"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        margin="normal"
                        /> 
                    </Grid>
                    <Grid sm={3} xs={3}>
                        <TextField
                            id="standard-name"
                            label="City"   
                            className="configtextfield"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            margin="normal"
                        /> 
                    </Grid>
                    <Grid sm={3} xs={3}>
                       <TextField
                            id="standard-name"
                            label="State" 
                            className="configtextfield"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            margin="normal"
                       /> 
                    </Grid>
                 <Grid sm={3} xs={3}>
                    <TextField
                        id="standard-name"
                        label="Country"    
                        className="configtextfield"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        margin="normal"
                    /> 
                </Grid>
                    <Grid sm={3} xs={3}>
                      <TextField
                        id="standard-name"
                        label="Zip"           
                        className="configtextfield"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        margin="normal"
                    /> 
                     </Grid>
                     <Grid sm={3} xs={3}>
                        <FormControlLabel
                            label="Address Status"
                            control={<Switch checked={state.checkedA} onChange={handleChange4} name="checkedA" />}
                        />
                     </Grid>
                     <Grid sm={3} xs={3}>
                       <FormControl className={classes.formControl}>
                         <InputLabel style={{fontSize:'14px'}} id="demo-simple-select-label">AddressTypeUUID</InputLabel>
                           <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={TypeUUID}
                            onChange={handleChange1}
                                                >
                            <MenuItem value={10}>CNP</MenuItem>
                            <MenuItem value={20}>TMS</MenuItem>
                            <MenuItem value={30}>Cloudgen</MenuItem>
                       </Select>
                  </FormControl>
                   </Grid>
               
                <Grid sm={3} xs={3}>
                    <TextField
                        id="standard-name"
                        label="Role"
                        className="configtextfield"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        margin="normal"
                    /> 
                 </Grid>
                    <Grid sm={3} xs={3}>
                    <TextField
                        id="standard-name"
                        label="FirstName"      
                        className="configtextfield"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        margin="normal"
                    /> 
                    </Grid>
                 <Grid sm={3} xs={3}>
                    <TextField
                        id="standard-name"
                        label="LastName"          
                        className="configtextfield"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        margin="normal"
                    /> 
                    </Grid>
                      <Grid sm={3} xs={3}>
                        <TextField
                            id="standard-name"
                            label="Email"       
                            className="configtextfield"
                            InputLabelProps={{
                                shrink: true,
                            }}
                          margin="normal"
                        /> 
                     </Grid>
                 <Grid sm={3} xs={3}>
                    <TextField
                        id="standard-name"
                        label="Phone"      
                        className="configtextfield"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        margin="normal"
                    /> 
                    </Grid>
                 <Grid sm={3} xs={3}>
                    <TextField
                        id="standard-name"
                        label="Fax"         
                        className="configtextfield"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        margin="normal"
                    /> 
                 </Grid>
               <Grid sm={3} xs={3}>
                 <TextField                      
                      id="standard-name"
                      label="Country"    
                      className="configtextfield"
                      InputLabelProps={{
                        shrink: true,
                      }}
                      margin="normal"
                    /> 
                </Grid>
                 <Grid sm={3} xs={3}>
                    <TextField                      
                        id="standard-name"
                        label="AddressUUID"
                        className="configtextfield"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        margin="normal"
                    /> 
                 </Grid>
                    <Grid sm={3} xs={3}>
                      <FormControl className={classes.formControl}>
                        <InputLabel style={{fontSize:'14px'}} id="demo-simple-select-label">UUID</InputLabel>
                          <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={ContactTypeUUID1}
                            onChange={handleChange}
                                        >
                            <MenuItem value={10}>CNP</MenuItem>
                            <MenuItem value={20}>TMS</MenuItem>
                            <MenuItem value={30}>Cloudgen</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
            </Grid>   
               <Button variant="contained"   className="createpartnerbutton" color="secondary" >    
               Create
              </Button>   
              </Grid>                      

        </form>
        </div>
        </MuiThemeProvider>
  );
    }
