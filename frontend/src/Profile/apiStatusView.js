import React, { useEffect, useContext, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { Grid, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { withStyles } from '@material-ui/core/styles';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import '../css/commonStyle.css';
import Theme from '../css/theme';
import Timestamp from '../timestamp';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { useQuery, useMutation, useManualQuery } from 'graphql-hooks';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(2),
    },
  },

  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));
const styles = (theme) => ({
  root: {
    Width: 1200,
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: '#FFFFFF',
  },
});
export default function StatusAPIView(props) {
  console.log(props);
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSvgIcon: {
          root: {
            color: '#FFFFFF',
          },
        },
        MuiGrid: {
          container: {},
          item: {
            fontSize: '10px',
          },
        },
        MuiDialogTitle: {
          root: {
            backgroundColor: '#0b153e',
            color: '#FFFFFF',
          },
        },
      },
    });

  useEffect(() => {}, []);
  const handleModalClose = () => {
    props.handleCloseModal(false);
  };
console.log(props.rowData,"props.rowData")
  return (
    <div>
      <MuiThemeProvider theme={getMuiTheme()}>
        <Grid container className="header">
          <Grid item xs={10} sm={10}>
            <h2 className="h2">API Response</h2>
          </Grid>
          <Grid item xs={2} sm={2} className="close">
            <CloseIcon onClick={handleModalClose} />
          </Grid>
        </Grid>
        <Grid container className="pdashboardroot">
          <Grid container spacing={3} className="content">
            <Grid
              item
              xs={12}
              sm={4}
              variant="body2"
              color="textSecondary"
              className="text"
            >
              <Typography className="typographysubheader">
                API Partner ID
              </Typography>
              {props.rowData.APIPartnerID}
            </Grid>
            <Grid
              item
              xs={12}
              sm={4}
              variant="body2"
              color="textSecondary"
              className="text"
            >
              <Typography className="typographysubheader">
                API Message
              </Typography>
              {props.rowData.APIStatusMsg}
            </Grid>
            <Grid
              item
              xs={12}
              sm={4}
              variant="body2"
              color="textSecondary"
              className="text"
            >
              <Typography className="typographysubheader">
                API Status
              </Typography>
              {props.rowData.APIStatus === 'Inactive' || props.rowData.APIStatus === null ? (
                <Typography
                  style={{
                    color: 'red',
                    fontSize: 10,
                  }}
                >
                  {'Failure'}
                </Typography>
              ) : (
                <Typography
                  style={{
                    color: 'green',
                    fontSize: 10,
                  }}
                >
                  {'Success'}
                </Typography>
              )}
            </Grid>
          </Grid>
        </Grid>
      </MuiThemeProvider>
    </div>
  );
}
