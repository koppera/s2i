import React, { useEffect } from 'react';
import MUIDataTable from 'mui-datatables';
import { makeStyles } from '@material-ui/core/styles';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import Theme from '../css/theme';
import ModalFab from '../components/ModalFab';
import Adduser from './Users';
import IconButton from '@material-ui/core/IconButton';
import GroupAddIcon from '@material-ui/icons/GroupAdd';
import Button from '@material-ui/core/Button';
import Modal from '../container/modal';
import TextField from '@material-ui/core/TextField';
import AddGrouptoUser from './addgrouptouser';
import AddRoleToUser from './addRoletoUser';
import AddIcon from '@material-ui/icons/Add';
import Tooltip from '@material-ui/core/Tooltip';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import { useQuery, useMutation } from 'graphql-hooks';
import EmptyPage from '../container/emptyPage';
import Paper from '@material-ui/core/Paper';
import CircularIndeterminateLoader from '../components/loader';
import Checkbox from '@material-ui/core/Checkbox';
import ConfirmationModal from '../container/modal';
import Confirmationbox from '../components/confirmationbox';

const FETCH_ALL_USERS = `
{
  allUsers{
    _id
    Mail
    CommonName
    Surname
    Uid
    Mobile
    Role
    UserRoles{
      rid
      RoleName
    }
  }
}

`;
const UPDATE_ROLE = `mutation addRole($roleInput:UserInput){
  addRole(input:$roleInput) {
    _id
    Mail
    CommonName
    Surname
    Uid
    Mobile
    Role
  }
}
`;
const DELETE_USER = `
mutation removeUser($input:[UserInput]){
  removeUsers(input:$input) {
    _id
    Mail
    CommonName
    Surname
    Uid
    Mobile
    Role
  }
}`;

const useStyles = makeStyles((theme) => ({
  table: {
    color: 'red',
  },
  MUIDataTableToolbarSelect: {
    title: {
      paddingLeft: 26,
    },
  },
}));

export default function Groupstable(props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState();
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [variant, setSnackbarVariant] = React.useState('error');
  const [message, setSnackbarMessage] = React.useState();
  const { loading, error, data, refetch } = useQuery(FETCH_ALL_USERS);
  const [updateRole, roleloading, roledata] = useMutation(UPDATE_ROLE);
  const [deleteUser, delloading, deldata] = useMutation(DELETE_USER);
  const fetchAllUsers = [];
  console.log('data48', data);
  const [isaddgroup, setAddgroup] = React.useState();
  const [userData, setUserData] = React.useState();
  const [columns, setColumns] = React.useState([]);
  const [userID, setUserID] = React.useState();
  const [CommonName, setCommonName] = React.useState();
  const [allusers, setAllUsers] = React.useState([]);
  const [openConfirm, setConfirmBox] = React.useState(false);
  const [deleteValues, setDeleteValues] = React.useState([]);
  const [confirmMessage, setConfirmMesage] = React.useState('');
  const [adminRolevalues, setAdminRoleValues] = React.useState();

  /* const handleRedirectGroup = uid => {
    console.log(uid);
    if (uid !== undefined) {
      props.history.push({
        pathname: '/Groups/' + uid,
        state: { configData: props.configData, uid: uid }
      });
    }
  };**/
  const handleRedirectRole = (uid) => {
    console.log(uid);
    if (uid !== undefined) {
      props.history.push({
        pathname: '/Roles/' + uid,
        state: { configData: props.configData, uid: uid },
      });
    }
  };

  console.log(loading, error);
  const handleAddRole = (userid, commonname) => {
    setCommonName(commonname);
    setUserID(userid);
    setAddgroup(true);
  };
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiIconButton: {
          root: {
            color: '#ffffff',
            '&:hover': {
              backgroundColor: '#ffffff',
              color: '#4d0000 ',
            },
          },
        },

        MuiTablePagination: {
          input: {
            color: '#0b153e',
          },
        },
        MuiSvgIcon: {
          root: {
            color: '#0b153e',
          },
        },
        MuiTypography: {
          h6: {
            fontSize: 14,
            fontFamily: 'Arial !important',
          },
        },
        MuiInput: {
          underline: {
            color: '#FFFFFF !important',
            '&:before': {
              //underline color when textfield is inactive
              borderBottomColor: 'white !important',
            },
            '&:hover:not($disabled):before': {
              //underline color when hovered
              borderBottomColor: 'white !important',
            },
            '&:after': {
              //underline color when textfield is inactive
              borderBottomColor: 'white !important',
            },
          },
        },
      },
    });
  function handleChange(userInfo) {
    setAdminRoleValues(userInfo);
    setConfirmMesage(' Are you sure you want to change the permissions?');
    setConfirmBox(true);
    console.log(userInfo);
  }
  useEffect(() => {
    if (data !== undefined) {
      if (data.allUsers !== null && data.allUsers.length > 0) {
        console.log(data.allUsers);
        setAllUsers(data.allUsers);
        let suggestions = data.allUsers.map((suggestion) => [
          suggestion._id,
          suggestion.CommonName + '  ' + suggestion.Surname,

          suggestion.Mail,
          suggestion.Uid,
          suggestion.Mobile,
          // suggestion.EmployeeNumber,
          <Checkbox
            checked={suggestion.Role}
            onChange={() => handleChange(suggestion)}
            value="admin"
          />,

          // suggestion.UserRoles[0].RoleName
          /* <Tooltip title="View Role">
            <IconButton
              className="usertableicon"
              onClick={() => handleRedirectRole(suggestion.Uid)}
            >
              <GroupAddIcon className="usertableicon" />
            </IconButton>
          </Tooltip>,

          <Tooltip title="Assign Role">
            <IconButton
              className="iconbutton"
              onClick={() =>
                handleAddRole(suggestion._id, suggestion.CommonName)
              }
            >
              <AddIcon className="usertableicon" />
            </IconButton>
          </Tooltip>*/
        ]);
        setColumns([
          {
            name: 'ID',
            options: {
              display: false,
              filter: false,
              viewColumns: false,
            },
          },
          'FullName',
          'Email',
          'UID/EmpNumber',
          'Mobile',
          'Assign Admin',
          // 'View Roles',
          // 'Assign Roles'
        ]);
        setUserData(suggestions);
      }
    }
  }, [data]);
  const options = {
    filterType: 'checkbox',
    rowsPerPage: 2,
    responsive: 'scroll',
    rowsPerPageOptions: [2, 4, 6],
    onRowsDelete: (rowsDeleted) => {
      console.log(rowsDeleted, userData);
      const idsToDelete = rowsDeleted.data.map((d) => allusers[d.dataIndex]); // array of all ids to to be deleted
      console.log(idsToDelete);
      setDeleteValues(idsToDelete);
      setConfirmMesage(' Are you sure you want to delete the users?');
      setConfirmBox(true);
    },
  };

  const handleOpen = () => {
    setOpen(true);
  };

  let handleClose = () => {
    setOpen(false);
  };

  function handleCloseModal() {
    setAddgroup(false);
  }

  const handleRefetch = (value, message) => {
    console.log(value);
    refetch({
      updateData(_, data) {
        return data;
      },
    });
    setOpenSnackbar(true);
    setSnackbarMessage(message);
    if (value) {
      setSnackbarVariant('error');
    } else {
      setSnackbarVariant('success');
    }
  };
  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }
  const [state, setState] = React.useState({ value: 'coconut' });

  const handleConfirmationBox = () => {
    setConfirmBox(false);
    console.log(deleteValues, deleteValues.length);
    setDeleteValues([]);
    if (deleteValues.length > 0) {
      refetch({
        updateData(_, data) {
          return data;
        },
      });
    }
  };
  const handleClickYes = () => {
    setConfirmBox(false);
    console.log(deleteValues, deleteValues.length, adminRolevalues);
    // delete users
    if (deleteValues.length > 0) {
      deleteUser({
        variables: {
          input: deleteValues,
        },
      });
      console.log(deldata, delloading);
      if (delloading.loading == false) {
        setDeleteValues([]);
        setTimeout(() => {
          handleRefetch(false, 'Users deleted successfully');
        }, 1000);
      }
    }
    // role changes
    console.log(adminRolevalues);
    if (adminRolevalues !== undefined && adminRolevalues !== '') {
      let role;
      if (adminRolevalues.Role == 1) {
        role = 0;
      } else {
        role = 1;
      }
      updateRole({
        variables: {
          roleInput: {
            CommonName: adminRolevalues.CommonName,
            Role: role,
            _id: adminRolevalues._id,
          },
        },
      });
      console.log(roledata);
      if (roleloading.loading === false) {
        setAdminRoleValues('');
        setTimeout(() => {
          handleRefetch(false, 'admin permissions changed');
        }, 1000);
      }
    }
  };
  console.log("loading,error,data",loading,error,data)
  if (loading) return <CircularIndeterminateLoader />;
  if (error) return <EmptyPage type={'Server Error! Please try again later'} />;
  if (data !== undefined)
    return (
      <MuiThemeProvider theme={getMuiTheme()}>
        {data.allUsers.length > 0 ? (
          <MUIDataTable
            title={'Users'}
            data={userData}
            columns={columns}
            options={options}
            className={classes.table}
          />
        ) : (
          <EmptyPage type={'No Users.Please Add the Users'} />
        )}
        <ModalFab title="Add Users">
          <Adduser
            onSuccess={handleRefetch}
            configData={props.ConfigSettingData}
            location={props.location}
            {...props}
          />
        </ModalFab>
        <Modal
          title="Add group"
          isModalOpen={isaddgroup}
          handleCloseModal={handleCloseModal}
        >
          <AddRoleToUser
            onSuccess={handleRefetch}
            handleCloseModal={handleCloseModal}
            id={userID}
            name={CommonName}
          />
        </Modal>
        <Snackbar
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
          autoHideDuration={5000}
          open={openSnackbar}
          onClose={handleCloseSnackbar}
        >
          <MySnackbarContentWrapper
            onClose={handleCloseSnackbar}
            variant={variant}
            message={message}
          />
        </Snackbar>
        <ConfirmationModal
          isModalOpen={openConfirm}
          handleCloseModal={handleConfirmationBox}
        >
          <Confirmationbox
            handleClickYes={handleClickYes}
            handleCloseModal={handleConfirmationBox}
            isModalOpen={openConfirm}
            message={confirmMessage}
            // configData={props.history.location.state.configData}
          />
        </ConfirmationModal>
      </MuiThemeProvider>
    );
  // }
}
