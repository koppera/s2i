import React, { useEffect } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import Typography from '@material-ui/core/Typography';
import '../css/commonStyle.css';
import NoSsr from '@material-ui/core/NoSsr';
import Select from 'react-select';
import { useMutation } from 'graphql-hooks';
import ReactSelect from '../container/reactSelect';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
const ADD_User = `mutation addUservalue($create: UserInput){
  addUser(input:$create) {
    _id
    Mail
    CommonName
    Surname
    Uid
   Mobile 
   Role
  } 
}`;

const suggestions = [
  { name: 'Bhavya' },
  { name: 'Madhuri' },
  { name: 'Tejaswini' }
].map(suggestion => ({
  value: suggestion.name,
  label: suggestion.name
}));
export default function ADD_users(props) {
  console.log(props);
  const [
    addUserInformation,
    { loading, error, graphQLErrors, networkError, data, cacheHit }
  ] = useMutation(ADD_User);
  const [values, setValues] = React.useState({
    commonname: '',
    password:'',
    surname: '',
    email: '',
    groups: [
      {
        gid: '',
        CommonName: ''
      }
    ],
    uid: '',
    mobile: ''
    //  empnum: ''
  });
  const [ErrMsg, setErrMsg] = React.useState(false);
  const [errorInfo, setErrorInfo] = React.useState({
    commonname: false,
    surname: false,
    email: false,
    password: false,
    uid: false,
    mobile: false
    // empnum: false
  });

  const [state, setState] = React.useState(false);
  const [snackbarMessage, setSnackbarMessage] = React.useState('');
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [variant, setSnackbarVariant] = React.useState('error');
  function handleClose() {
    setOpenSnackbar(false);
  }

  useEffect(() => {
    console.log('values', values);
  }, [values]);
  /*const handleChangeCheckbox = name => event => {
    console.log(event.target.checked);
    setState(event.target.checked);
  };*/
  const handleChange = name => event => {
    setErrMsg(false);
    setValues({ ...values, [name]: event.target.value });

    switch (name) {
      case 'commonname':
      case 'surname':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match('^[A-Z]{1}[a-z]*[0-9]*$') &&
          event.target.value.length <= 25
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }

        break;
        {
          /*case 'empnum':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match('^[a-zA-Z0-9]*$') &&
          event.target.value.length <= 25
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }

      break;*/
        }
      case 'uid':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match('^([a-zA-Z])[a-zA-Z0-9]*$') &&
          event.target.value.length <= 25
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }

        break;
      case 'email':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match(
            '^[a-zA-Z0-9.]+(@[a-zA-Z0-9]*)+([a-zA-Z]{3,})+(.[a-zA-Z]{2,3})$'
          )
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
        } else setErrorInfo({ ...errorInfo, [name]: true });
        break;
      case 'password':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match(
            '^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,15}$'
          )
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;

      case 'mobile':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[7-9][0-9]{9}$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
          console.log(event.target.value);
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      default:
        return false;
    }
  };

  function handleWebMethodsImages(val) {
    setErrMsg(false);
    setValues({ ...values, groups: val });
  }

  async function handleSubmit() {
    if (
      Object.values(values).includes('') ||
      Object.values(errorInfo).includes(true)
    ) {
      setErrMsg(true);
    } else {
      console.log('add user values.....');
      console.log(values);
      let groupData = [],
        groupInfo;
      /* if (values.groups[0].gid != '') {
        groupData = values.groups;
      } else {
        groupData = [];
      }*/
      console.log(groupData);
      await addUserInformation({
        variables: {
          create: {
            CommonName: values.commonname,
            Surname: values.surname,
            Mail: values.email.toLowerCase(),
            UserPassword: values.password,
            Uid: values.uid.toUpperCase(),
            Mobile: values.mobile
            //   Manager: values.manager,
            // UserRoles: groupData,
            // EmployeeNumber: values.empnum
          }
        }
      })
        .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
          // you can do something with the response here
          console.log(data);
          console.log(error, graphQLErrors, networkError, data, cacheHit);
          if (data !== null && error == false) {
            setValues({
              ...values,
              commonname: '',
              password:'',
              email: '',
              surname: '',
              uid: '',
              mobile: ''
              //  empnum: ''
            });
            setState(false);
            props.onSuccess(loading, 'User saved successfully');
          } else if (error && graphQLErrors.length > 0) {
            //duplicate project name or not inserting to db
            let duperror = graphQLErrors[0].message;
            console.log(duperror);
            if (
              duperror !== null &&
              duperror.indexOf(11000) !== -1 &&
              duperror.indexOf('CommonName') !== -1
            ) {
              setOpenSnackbar(true); 
              setSnackbarVariant('error');
              setSnackbarMessage(
                'commonname ' + values.commonname + ' is already taken'
              );
            } else if (
              duperror !== null &&
              duperror.indexOf(11000) !== -1 &&
              duperror.indexOf('Uid') !== -1
            ) {
              setOpenSnackbar(true);
              setSnackbarVariant('error');
              setSnackbarMessage('UID ' + values.uid + ' is already taken');
            } else if (
              duperror !== null &&
              duperror.indexOf(11000) !== -1 &&
              duperror.indexOf('Mail') !== -1
            ) {
              setOpenSnackbar(true);
              setSnackbarVariant('error');
              setSnackbarMessage('EMail ' + values.email + ' is already taken');
            } else {
              setOpenSnackbar(true);
              setSnackbarVariant('error');
              setSnackbarMessage('Please check user details');
            }
          }
        })
        .catch(e => {
          // you can do something with the error here
          console.log(e);
        });

      //  setOpenSnackbar(true);
      //setSnackbarMessage('Successfully Saved');

      /* if (loading.loading === true) {
        props.onSuccess(loading.loading, 'Graphql hooks error');
      } else {
        setErrMsg(false);

        setTimeout(() => {
          props.onSuccess(loading.loading, 'User saved successfully');
        }, 1000);
      }*/
    }
  }

  const handlereset = () => {
    setErrMsg(false);
    setErrorInfo({
      ...errorInfo,
      commonname: false,
      surname: false,
      email: false,
      password: false,
      uid: false,
      mobile: false
      // empnum: false
    });
    setValues({
      ...values,
      commonname: '',
      password:'',
      email: '',
      surname: '',

      uid: '',
      mobile: ''
      //  manager: '',

      //  empnum: ''
    });
  };

  return (
    <div>
      <div style={{ textAlign: 'center' }} className="addfieldserror1">
        {ErrMsg === true ? 'Please fill the fields.' : ''}
      </div>
      <form className="usercontent" noValidate autoComplete="off">
        <Grid container spacing={3} className="usercontainer">
          {/* <Grid item xs={4} sm={4}>
          <TextField
            id="standard-basic"
            label="DC"
            className="textField"
            onChange={handleChange('domaincomponent')}
            defaultValue="infra"
            value={values.Domaincomponent}
            name="DComponent"
            variant="outlined"
            InputProps={{
              readOnly: true
            }}
            margin="dense"
          />
        </Grid>
        <Grid item xs={4} sm={4}>
          <TextField
            id="standard-basic"
            label="DC"
            className="textField"
            onChange={handleChange('domaincomponent')}
            defaultValue="cgdemos"
            value={values.Domaincomponent}
            name="DComponent"
            variant="outlined"
            InputProps={{
              readOnly: true
            }}
            margin="dense"
          />
        </Grid>
        <Grid item xs={4} sm={4}>
          <TextField
            id="standard-basic"
            label="DC"
            className="textField"
            onChange={handleChange('domaincomponent')}
            defaultValue="com"
            value={values.Domaincomponent}
            name="DComponent"
            variant="outlined"
            InputProps={{
              readOnly: true
            }}
            margin="dense"
          />
          </Grid> 
        <Grid item xs={4} sm={4}>
          <TextField
            id="standard-basic"
            label="OU"
            className="textField"
            onChange={handleChange('organisationunit')}
            defaultValue="Users"
            value={values.organisationunit}
            name="organisationunit"
            variant="outlined"
            InputProps={{
              readOnly: true
            }}
            margin="dense"
          />
          </Grid> */}
          <Grid item xs={4} sm={4} className="usergrid">
            <TextField
              id="standard-basic"
              label="CommonName"
              className="textField"
              helperText={
                errorInfo.commonname === true
                  ? 'Starts with UpperCase followed by all lowercase. Maximum length is 25.'
                  : false
              }
              error={errorInfo.commonname}
              onChange={handleChange('commonname')}
              value={values.commonname}
              name="commonname"
              variant="outlined"
              margin="dense"
            />
          </Grid>
          <Grid item xs={4} sm={4} className="usergrid">
            <TextField
              id="standard-basic"
              label="SurName"
              className="textField"
              helperText={
                errorInfo.surname === true
                  ? 'Starts with UpperCase followed by all lowercase. Maximum length is 25.'
                  : false
              }
              error={errorInfo.surname}
              onChange={handleChange('surname')}
              value={values.surname}
              name="surname"
              variant="outlined"
              margin="dense"
            />
          </Grid>
          <Grid item xs={4} sm={4} className="usergrid">
            <TextField
              id="standard-basic"
              label="Email"
              variant="outlined"
              className="textField"
              helperText={
                errorInfo.email === true ? 'Please give valid Email' : false
              }
              error={errorInfo.email}
              onChange={handleChange('email')}
              value={values.email}
              name="email"
              margin="dense"
            />
          </Grid>
          <Grid item xs={4} sm={4} className="usergrid">
            <TextField
              id="standard-basic"
              className="textField"
              label="Password"
              helperText={
                errorInfo.password === true
                  ? 'Password length must be between 8 and 15 characters must have atleast one[uppercase,lowercase,special] character and atleast one number'
                  : false
              }
              error={errorInfo.password}
              onChange={handleChange('password')}
              value={values.password}
              name="password"
              variant="outlined"
              type="password"
              margin="dense"
            />
          </Grid>
          <Grid item xs={4} sm={4} className="usergrid">
            <TextField
              id="standard-basic"
              className="textField"
              label="UID"
              helperText={
                errorInfo.uid === true
                  ? 'It starts with alphabets followed by alphanumeric.Maximum length is 25.'
                  : false
              }
              error={errorInfo.uid}
              onChange={handleChange('uid')}
              value={values.uid}
              name="uid"
              variant="outlined"
              margin="dense"
            />
          </Grid>
          <Grid item xs={4} sm={4} className="usergrid">
            <TextField
              id="standard-basic"
              className="textField"
              label="Mobile"
              helperText={
                errorInfo.mobile === true
                  ? 'Please give valid mobile number.'
                  : false
              }
              error={errorInfo.mobile}
              onChange={handleChange('mobile')}
              value={values.mobile}
              name="mobile"
              variant="outlined"
              margin="dense"
            />
          </Grid>
          {/*  <Grid item xs={4} sm={4} className="usergrid">
            <TextField
              id="standard-basic"
              className="textField"
              label="Employee No."
              helperText={
                errorInfo.empnum === true
                  ? 'Allows alphanumeric .Maximum length is 25.'
                  : false
              }
              error={errorInfo.empnum}
              onChange={handleChange('empnum')}
              value={values.empnum}
              name="empnum"
              variant="outlined"
              margin="dense"
            />
          </Grid>
          {/*} <Grid item xs={4} sm={4} className="checkboxgrid">
            <FormGroup row>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={state}
                    onChange={handleChangeCheckbox('admin')}
                    value="admin"
                  />
                }
                label="Admin"
              />
            </FormGroup>
          </Grid>*/}
        </Grid>
        {/*  <Grid item sm={4} xs={4} className="autoselectroot">
          <ReactSelect handleWebMethodsImages={handleWebMethodsImages} />
        </Grid>*/}
        <Grid container spacing={3}>
          <Grid item sm={6} xs={6}>
            <Button
              variant="contained"
              size="Large"
              className="createresetbutton"
              onClick={handlereset}
            >
              RESET
            </Button>
          </Grid>
          <Grid item sm={6} xs={6}>
            <Button
              variant="contained"
              size="Large"
              className="createbutton"
              onClick={handleSubmit}
            >
              SAVE
            </Button>
          </Grid>
        </Grid>
      </form>
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center'
        }}
        autoHideDuration={5000}
        open={openSnackbar}
        onClose={handleClose}
      >
        <MySnackbarContentWrapper
          onClose={handleClose}
          variant={variant}
          message={snackbarMessage}
        />
      </Snackbar>
    </div>
  );
}
