import React, { useEffect, useContext, useCallback } from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Tooltip from "@material-ui/core/Tooltip";
import MySnackbarContentWrapper from "../components/Snackbar";
import Snackbar from "@material-ui/core/Snackbar";
import   "../css/commonStyle.css";
import Modalfab from "./orgcards";
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles(theme => ({
}));

export default function AddOrganisation(props) {
  const classes = useStyles();
  const [values, setValues] = React.useState({ OrganisationID: "", OrganisationName: "",OrganisationDescription: "",OrganisationAddress: "",OrganisationStartDate:"",OrganisationEndDate:"" });
  const [ErrMsg, setErrMsg] = React.useState(false);
  const [errorInfo, setErrorInfo] = React.useState(
    {
      OrganisationDescription:false,
      OrganisationAddress:false,
      OrganisationID:false,
      OrganisationName:false,
      OrganisationStartDate:false,
      OrganisationEndDate:false,
    });
    const [snackbarMessage, setSnackbarMessage] = React.useState('');
    const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const [open1, setOpen1] = React.useState(false);
  const [open2, setOpen2] = React.useState(false);
  const [open3, setOpen3] = React.useState(false);
  const [open4, setOpen4] = React.useState(false);
  const [open5, setOpen5] = React.useState(false);
  

  function handleTooltipClose() {
    setOpen(false);
  }

  function handleTooltipOpen() {
    if (values.OrganisationID === "") setOpen(true);
  }

  function handleTooltipClose1() {
    setOpen1(false);
  }

  function handleTooltipOpen1() {
    if (values.OrganisationName === "") setOpen1(true);
  }

  function handleTooltipClose2() {
    setOpen2(false);
  }

  function handleTooltipOpen2() {
    if (values.OrganisationDescription.length === 0) setOpen2(true);
  }

  function handleTooltipClose3() {
    setOpen3(false);
  }

  function handleTooltipOpen3() {
    if (values.OrganisationAddress.length === 0) setOpen3(true);
  }
  
  
  function handleTooltipClose4() {
    setOpen4(false);
  }

  function handleTooltipOpen4() {
    if (values.OrganisationAddress.length === 0) setOpen4(true);
  }
  
   
  function handleTooltipClose5() {
    setOpen5(false);
  }

  function handleTooltipOpen5() {
    if (values.OrganisationAddress.length === 0) setOpen5(true);
  }
  
  function handleClose() {
    setOpenSnackbar(false);
  }
 

  const handleChange = name => event => {
    setErrMsg(false);
    setValues({...values,[name]:event.target.value}); 
    switch(name){
      case "OrganisationDescription": setOpen2(false)
                                      if(event.target.value.length === 0){
                                       setValues({ ...values, [name]: ""});
                                       setErrorInfo({...errorInfo,[name]:false});
                                       }
                                else   {
                                  if (event.target.value.match("^([a-zA-Z0-9])[a-zA-Z0-9._-]*$") && event.target.value.length <= 25) {
                                  setErrorInfo({...errorInfo,[name]:false});
                                  setValues({ ...values, [name]: event.target.value });
                                 }
                                else {
                                    setErrorInfo({...errorInfo,[name]:true});
                                    }
                              }                                 
                           break;
      case "OrganisationAddress": setOpen3(false)
                                  if(event.target.value.length === 0){
                                  setValues({ ...values, [name]: ""});
                                  setErrorInfo({...errorInfo,[name]:false});
                                  }
                              else  {
                                  if(event.target.value.match("^([a-zA-Z0-9])[a-zA-Z0-9._-]*$") && event.target.value.length <= 25) {
                                   setErrorInfo({...errorInfo,[name]:false});
                                   setValues({ ...values, [name]: event.target.value });
                                   }
                                else {
                                    setErrorInfo({...errorInfo,[name]:true});
                                     }
                                  }                                 
                           break;
      case "OrganisationID":setOpen(false)
                            if (event.target.value.length === 0) {
                           setValues({ ...values, [name]: "" });
                            setErrorInfo({...errorInfo,[name]:false});
                            } 
                          else  {
                           if (event.target.value.match("^([a-zA-Z])[a-zA-Z0-9_]*$") && event.target.value.length <= 25) {
                           setErrorInfo({...errorInfo,[name]:false});
                           setValues({ ...values, [name]: event.target.value });
                        } else {
                        setErrorInfo({...errorInfo,[name]:true});
                        }
                     }
                    break;
      case "OrganisationName": setOpen1(false)
                            if (event.target.value.length === 0) {
                             setValues({ ...values, [name]: "" });
                             setErrorInfo({...errorInfo,[name]:false});
                            } 
                           else {
                               if (event.target.value.match("^([a-zA-Z0-9])[a-zA-Z0-9 ]*$") && event.target.value.length <= 25) {
                               setErrorInfo({...errorInfo,[name]:false});
                                setValues({ ...values, [name]: event.target.value });
                              } else {
                              setErrorInfo({...errorInfo,[name]:true});
                              }
                            }
                      break;
      case "OrganisationStartDate":  setOpen4(false)
                                    if(event.target.value.length === 0){
                                    setValues({ ...values, [name]: ""});
                                    setErrorInfo({...errorInfo,[name]:false});
                                  }
                                  else{
                                    if (event.target.value.match("^[a-zA-Z0-9][a-zA-Z0-9._-]*$") && event.target.value.length <= 25) {
                                        setErrorInfo({...errorInfo,[name]:false});
                                        setValues({ ...values, [name]: event.target.value });
                                    }
                                    else {
                                      setErrorInfo({...errorInfo,[name]:true});
                                    }
                                  }                                 
                            break;
      case "OrganisationEndDate":  setOpen5(false)
                              if(event.target.value.length === 0){
                              setValues({ ...values, [name]: ""});
                              setErrorInfo({...errorInfo,[name]:false});
                            }
                            else{
                              if (event.target.value.match("^[a-zA-Z0-9][a-zA-Z0-9._-]*$") && event.target.value.length <= 25) {
                                  setErrorInfo({...errorInfo,[name]:false});
                                  setValues({ ...values, [name]: event.target.value });
                              }
                              else {
                                setErrorInfo({...errorInfo,[name]:true});
                              }
                            }                                 
                      break;
      default:  return false                      
                          }
 
    }

  
    const handleSubmit =async () => {
      console.log(values)
      setErrMsg(false);
      if (values.OrganisationID === "" || values.OrganisationName === "" || values.OrganisationDescription.length === 0 || values.OrganisationAddress.length === 0 || values.OrganisationStartDate.length === 0 || values.OrganisationEndDate.length === 0 || Object.values(errorInfo).includes(true)){
        setErrMsg(true);
      } 
      else {
        setErrMsg(false);
        setSnackbarMessage('Succesfully Saved');
        setOpenSnackbar(true);
        setValues({ ...values, OrganisationID: "", OrganisationName: "",OrganisationDescription:"",OrganisationAddress:"" ,OrganisationStartDate:"",OrganisationEndDate:""});
      }
    };
    
  
  return (
    <div>
    {ErrMsg === true ? (
      <div style={{ textAlign: "center" }} className="addfieldserror">
        Please fill the fields
      </div>
    ) : (
      ""
    )}
     
      <form className="content" noValidate autoComplete="off">
           <Grid container spacing={3} className="mainreggrid">
                      <Grid item xs={6} sm={6} className="reggrid">
                      <Tooltip
          title="Allowed alphanumeric,no special characters allowed except[.,-,_]. Maximum length is 25."
          onOpen={handleTooltipOpen}
          onClose={handleTooltipClose}
          open={open}
        >
          <TextField
            id="outlined-dense"
            label="OrganisationID"
            className="textField"
            margin="dense"
            variant="outlined"
            //placeholder=""
            onChange={handleChange("OrganisationID")}
            value={values.OrganisationID}
            name="OrganisationID"
          />
            </Tooltip>
            {errorInfo.OrganisationID === true ? (
          <div id="nameid" className="addfieldserror">
            Starts with alphabets followed by alphanumeric. No special
            characters allowed except underscore"_". Maximum length is 25.
          </div>
        ) : (
          ""
        )}
          </Grid>

       
        <Grid item xs={6} sm={6} className="reggrid">
        <Tooltip
          title="Allowed alphanumeric,no special characters allowed except[.,-,_]. Maximum length is 25."
          onOpen={handleTooltipOpen1}
          onClose={handleTooltipClose1}
          open={open1}
        >
          <TextField
            id="outlined-dense"
            label="OrganisationName "
            className="textField"
            margin="dense"
            variant="outlined"
            onChange={handleChange("OrganisationName")}
            value={values.OrganisationName}
            name="OrganisationName"
          />
          </Tooltip>
          {errorInfo.OrganisationName === true ? (
          <div className="addfieldserror">
            Allowed alphanumeric,no special characters allowed except spaces.Maximum length is 25.
          </div>
        ) : (
          ""
        )}
        </Grid>
      
         <Grid item xs={6} sm={6} className="reggrid">
        <Tooltip
          title="Allowed alphanumeric,no special characters allowed except[.,-,_]. Maximum length is 25."
          onOpen={handleTooltipOpen2}
          onClose={handleTooltipClose2}
          open={open2}
        >
          <TextField
            id="outlined-dense"
            label="OrganisationDescription "
            className="textField"
            margin="dense"
            variant="outlined"
            onChange={handleChange("OrganisationDescription")}
            value={values.OrganisationDescription}
            name="OrganisationDescription"
          />
        </Tooltip>
        {errorInfo.OrganisationDescription=== true ? (
          <div className="addfieldserror">
            Allowed alphanumeric,no special characters allowed except[.,_,-] .Maximum length is 25.
          </div>
        ) : (
          ""
        )}
        </Grid>
        
         <Grid item xs={6} sm={6} className="reggrid">
        <Tooltip
          title="Allowed alphanumeric,no special characters allowed except[.,-,_]. Maximum length is 25. "
          onOpen={handleTooltipOpen3}
          onClose={handleTooltipClose3}
          open={open3}
        >
          <TextField
            id="outlined-dense"
            label="OrganisationAddress "
            className="textField"
            margin="dense"
            variant="outlined"
            onChange={handleChange("OrganisationAddress")}
            value={values.OrganisationAddress}
            name="OrganisationAddress"
          />
        </Tooltip>
        {errorInfo.OrganisationAddress === true ? (
          <div className="addfieldserror">
            Allowed alphanumeric,no special characters allowed except[.,_,-] .Maximum length is 25.
          </div>
        ) : (
          ""
        )}
        </Grid>
       
        <Grid item xs={6} sm={6} className="reggrid">
           <Tooltip
          title="Allowed alphanumeric,no special characters allowed except[.,-,_]. Maximum length is 25. "
          onOpen={handleTooltipOpen4}
          onClose={handleTooltipClose4}
          open={open4}
        >
         <TextField
            id="outlined-dense"
            label="OrganisationStartDate "
            className="textField"
            margin="dense"
            variant="outlined"
            onChange={handleChange("OrganisationStartDate")}
            value={values.OrganisationStartDate}
            name="OrganisationStartDate"
          />
          
          </Tooltip>
          {errorInfo.OrganisationStartDate === true ? (
          <div id="nameid" className="addfieldserror">
            Starts with alphabets followed by alphanumeric. No special
            characters allowed except underscore"_". Maximum length is 25.
          </div>
        ) : (
          ""
        )}
          </Grid>
         
         <Grid item xs={6} sm={6} className="reggrid">
          <Tooltip
          title="Allowed alphanumeric,no special characters allowed except[.,-,_]. Maximum length is 25. "
          onOpen={handleTooltipOpen5}
          onClose={handleTooltipClose5}
          open={open5}
        >
            <TextField
            id="outlined-dense"
            label="OrganisationEndDate "
            className="textField"
            margin="dense"
            variant="outlined"
            onChange={handleChange("OrganisationEndDate")}
            value={values.OrganisationEndDate}
            name="OrganisationEndDate"
          />
          </Tooltip>
          {errorInfo.OrganisationEndDate === true ? (
          <div className="addfieldserror">
            Allowed alphanumeric,no special characters allowed except[.,_,-] .Maximum length is 25.
          </div>
        ) : (
          ""
        )}
          </Grid>
         
        <Button
         onClick={handleSubmit}
          variant="contained"
          fullWidth="true"
          className="createbutton" color="secondary"
        >
          Create
        </Button>
        </Grid>
      </form>
   
      <Snackbar
              anchorOrigin={{
              vertical: 'top',
              horizontal: 'center'
            }}
            autoHideDuration={5000}
            open={openSnackbar}
            onClose={handleClose}
          >
            <MySnackbarContentWrapper
              onClose={handleClose}
              variant="success"
              message={snackbarMessage}
            />
          </Snackbar>
    </div>
  );
}
