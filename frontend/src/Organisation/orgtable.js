import MaterialTable from 'material-table';
import { useMutation, useQuery } from 'graphql-hooks';
import { forwardRef, useEffect } from 'react';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import ReactDOM from 'react-dom';
import MTableToolbar from 'material-table';
import React, { Component } from 'react';
import ModalFab from '../components/modalfabtms';
import Button from '@material-ui/core/Button';
import OrganisationForm from './organisationModel';
import Timestamp from '../timestamp';
import Theme from '../css/theme';
import { TextField } from '@material-ui/core';
import Loader from '../components/loader';
import Error from '../components/emptyPage';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';

import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles,
} from '@material-ui/core/styles';

function iconStyles() {
  return {};
}
const DISPLAY_All_ORGANIZATION = `query fetchOrganization($searchInput:String){
  fetchOrganization(searchInput:$searchInput) {
    idOrganization
    organizationUUID
    organizatonName
    organizationDescription
    createdOn
    lastUpdatedOn
  } 
}
`;
const ADD_Organization = `mutation createAddress($input:OrganizationInput){
  createOrganization(input:$input) {
    idOrganization
    organizationUUID
    organizatonName
    organizationDescription
    createdOn
    lastUpdatedOn
  }
}`;
const UPDATE_Organization = `mutation updateOrganization($input:OrganizationInput){
  updateOrganization(input:$input) {
    idOrganization
    organizationUUID
    organizatonName
    organizationDescription
    createdOn
    lastUpdatedOn
  } 
}`;
const REMOVE_Organization = `mutation removeOrganization($input:OrganizationInput){
  removeOrganization(input:$input) {
    idOrganization
    organizationUUID
    organizatonName
    organizationDescription
    createdOn
    lastUpdatedOn
  } 
}
`;

export default function Organization(props) {
  const [searchInput, setSearchValue] = React.useState('');
  //Fetch ALL ADDRESS
  const { loading, error, data, refetch } = useQuery(DISPLAY_All_ORGANIZATION, {
    variables: {
      searchInput: searchInput,
    },
  }); 
  /*
  const [state, setState] = React.useState({
    columns: [
      {
        title: 'UUID',
        field: 'organizationUUID',
        editable: 'never',
        hidden: true
      },
      { title: 'Name', field: 'organizatonName',editable: 'never'

      } ,
      {
        title: 'Description',
        field: 'organizationDescription',
        lookup: { 0: 'Cloudgen', 1: 'CNP', 2: 'CMC', 3: 'TMC' }
      },
      { title: 'Created', field: 'createdOn', editable: 'never',
      render : (rowData) => {
        console.log(rowData)
        if(rowData!= undefined){
          return Timestamp(rowData.createdOn)
        }else{
          return 
        }
      }
     },
      { title: 'Updated', field: 'lastUpdatedOn', editable: 'never',
      render : (rowData) => {
        console.log(rowData)
        if(rowData!= undefined){
          return Timestamp(rowData.lastUpdatedOn)
        }else{
          return 
        }
      }
      },         
    ],

    data: []
  });*/
  //ADD organizaton
  const [addOrganization, addloading, adddata] = useMutation(ADD_Organization);
  const [Organizationdata, setData] = React.useState([]);
  //EDIT organizaton
  const [updateorganizaton, updateloading, updatedata] = useMutation(
    UPDATE_Organization
  );
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [message, setSnackbarMessage] = React.useState();
  const [variant, setSnackbarVariant] = React.useState('error');
  //remove organizaton
  const [nameError, setNameError] = React.useState({
    error: false,
    label: "",
    helperText: "",
    validateInput: false,
});
const nameexp =( /^[a-zA-Z,-.\s]+$/);
  const [serverError, setServerError] = React.useState(false);
  const [removeorganizaton, removeloading, removedata] = useMutation(
    REMOVE_Organization
  );
  const columns = [
    {
      title: 'UUID',
      field: 'organizationUUID',
      editable: 'never',
      hidden: true,
    },
    {
      title: 'Name',
      field: 'organizatonName',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
         <TextField
             placeholder="Name"
             margin="normal"
             fullWidth
             getOptionLabel={(option) => option.name}
             value={props.value}
           
            error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                    ? nameError.error
                    : false
            }
            helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                    ? nameError.helperText
                    : ""
            }
              onChange={(v) => props.onChange(v.target.value)}  
        />
            <div style={{ color: 'red' }}>
              {!props.rowData.organizatonName||
              nameexp.test(props.rowData.organizatonName)
                ? ''
                : 'Enter only alphabets'}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.organizatonName}</div>;
      },
    },
    {
      title: 'Description',
      field: 'organizationDescription',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        function handlechange(v) {
          props.onChange(v);
        }
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder="Description"
              margin="normal"
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => handlechange(v.target.value)}
            />
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.organizationDescription}</div>;
      },
    },
    {
      title: 'Created',
      field: 'createdOn',
      editable: 'never',
      render: (rowData) => {
        console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.createdOn);
        } else {
          return;
        }
      },
    },
    {
      title: 'Last Modified',
      field: 'lastUpdatedOn',
      editable: 'never',
      render: (rowData) => {
        console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.lastUpdatedOn);
        } else {
          return;
        }
      },
    },
  ];

  useEffect(() => {  
    console.log();
    if (data !== undefined) {
      console.log(data.fetchOrganization);
      setData(data.fetchOrganization);
      setServerError(false);
    }
    else {
      console.log('server error');
      setServerError(true);
    }
  }, [data]);

  const handleRefetch = (value, message) => {
    refetch({
      updateData(_, data) {
        return data;
      },
    });
    setOpenSnackbar(true);
    setSnackbarMessage(message);
    if (value) {
      setSnackbarVariant('error');
    } else {
      setSnackbarVariant('success');
    }
  };
  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }
  const classes = makeStyles(iconStyles)();
  const tableIcons = {
    Add: forwardRef((props, ref) => (
      <AddBox color="primary" {...props} ref={ref} />
    )),
    Check: forwardRef((props, ref) => (
      <Check color="primary" {...props} ref={ref} />
    )),
    Clear: forwardRef((props, ref) => (
      <Clear color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline style={{marginLeft:"1px"}}   color="error" {...props} ref={ref} />
    )),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => (
      <Edit
        {...props}
        style={{marginLeft:"30px"}}
        ref={ref}
        color="primary"
        onClick={console.log('hello world')}
      />
    )),
    Export: forwardRef((props, ref) => (
      <SaveAlt color="primary" {...props} ref={ref} />
    )),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => (
      <FirstPage color="primary" {...props} ref={ref} />
    )),
    LastPage: forwardRef((props, ref) => (
      <LastPage color="primary" {...props} ref={ref} />
    )),
    NextPage: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft color="primary" {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => (
      <Clear color="primary" {...props} ref={ref} />
    )),
    Search: forwardRef((props, ref) => (
      <Search color="primary" {...props} ref={ref} />
    )),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };

  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSelect: {
          select: {
            minWidth: 100,
          },
          icon:{
            color:'#0b153e'
              }
        },

        MuiTypography: {
          h6: {
            fontSize: 14,
            fontFamily: 'Arial !important',
          },
        },
      },
    });
  const handleSearchValue = (val) => {
    console.log(val);
    setSearchValue(val);
  };
  async function handleAddOrganization(newData) {
    return await new Promise(async (resolve) => {
      resolve();
      console.log(newData);
      await addOrganization({
        variables: {
          input: {
            organizatonName: newData.organizatonName,
            organizationDescription: newData.organizationDescription,
          },
        },
      }) .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
        console.log(data, error, graphQLErrors, networkError, cacheHit);
       if (data !== null && error == false) {
         handleRefetch(error, 'Saved successfully');
       } 
       else if (error && graphQLErrors.length > 0)
         {
          let duperror = graphQLErrors[0].message;
          console.log(duperror);
          if (
            duperror !== null &&
            duperror.indexOf('ER_DUP_ENTRY') !== -1
          ) {
            handleRefetch(error, 'Name already existed');
          }
        }
     })
     .catch((e) => {
       // you can do something with the error here
       console.log(e);
     });
    });
  }
  async function handleRemoveOrganization(oldData) {
    return await new Promise(async (resolve) => {
      resolve();
      await removeorganizaton({
        variables: {
          input: {
            organizationUUID: oldData.organizationUUID,
          },
        },
      }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
        console.log(data, error, graphQLErrors, networkError, cacheHit);
        if (data !== null && error == false) {
          handleRefetch(error, 'Deleted successfully');
        } else if (error && graphQLErrors.length > 0) {
          let duperror = graphQLErrors[0].message;
          console.log(duperror);
          if (
            duperror !== null &&
            duperror.indexOf('ER_ROW_IS_REFERENCED') !== -1
          ) {
            setOpenSnackbar(true);
            setSnackbarVariant('error');
            setSnackbarMessage('Delete operation cannot be performed as that is been referenced');
          }
        }
      })
      .catch((e) => {
        // you can do something with the error here
        console.log(e);
      });
    });
  }
  async function handleUpdateOrganization(newData, oldData) {
    return await new Promise(async (resolve) => {
      resolve();
      console.log(newData);
      await updateorganizaton({
        variables: {
          input: {
            organizatonName: newData.organizatonName,
            organizationDescription: newData.organizationDescription,
            organizationUUID: newData.organizationUUID,
          },
        },
      }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
        console.log(data, error, graphQLErrors, networkError, cacheHit);
       if (data !== null && error == false) {
         handleRefetch(error, 'Updated successfully');
       } 
       else if (error && graphQLErrors.length > 0)
         {
          let duperror = graphQLErrors[0].message;
          console.log(duperror);
          if (
            duperror !== null &&
            duperror.indexOf('ER_DUP_ENTRY') !== -1
          ) {
            handleRefetch(error, 'Name already existed');
          }
        }
     })
     .catch((e) => {
       // you can do something with the error here
       console.log(e);
     });
    });
  }
  if (loading) return <Loader />;
  if (serverError)
    return <Error type={'Server connection lost.Please try again'} />;

  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <MaterialTable
        icons={tableIcons}
        title="Organization"
        columns={columns}
        data={Organizationdata}
        //onSearchChange={(e) => handleSearchValue(e)}
        editable={{
          onRowAdd: (newData) =>
          new Promise((resolve, reject) => {
         setTimeout(() => {
             newData.submitted = true;
             if (!newData.organizatonName||
              !nameexp.test(newData.organizatonName)
              ) {
                 setNameError({
                     error: true,
                     label: "required",
                     helperText: "Required.",
                     validateInput: true,
                 });
                 reject();
                 return;
             }
             resolve();
           
             handleAddOrganization(newData)
          
         }, 600);
     }),
        
        //  onRowAdd: (newData) => handleAddOrganization(newData),
         /* onRowUpdate: (newData, oldData) =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
                newData.submitted = true;
                if (!newData.organizatonName||
                 !nameexp.test(newData.organizatonName)
                 ) {
                    setNameError({
                        error: true,
                        label: "required",
                        helperText: "Required.",
                        validateInput: true,
                    });
                    reject();
                    return;
                }
                resolve();
              
                handleUpdateOrganization(newData, oldData);
             
            }, 600);
        }),*/
          onRowDelete: (oldData) => handleRemoveOrganization(oldData),
        }}
        options={{
          headerStyle: {
            textAlign: 'center',
            fontSize: 12,
            fontWeight: 'bold',
            fontFamily: 'Arial !important',
            backgroundColor: '#0b153e',
            color: '#ffffff',
            padding: '4px',
          },

          searchFieldStyle: {
            color: '#0b153e',
          },
          actionsColumnIndex: -1,
          pageSize:10,
          pageSizeOptions: [10, 25 ,50,100 ],
          toolbar: true,
          paging: true
        }}
      />
     {/*  <ModalFab title="Organisation">
        <OrganisationForm />
      </ModalFab>*/}
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        autoHideDuration={1500}
        open={openSnackbar}
        onClose={handleCloseSnackbar}
      >
        <MySnackbarContentWrapper
          onClose={handleCloseSnackbar}
          variant={variant}
          message={message}
        />
      </Snackbar>
    </MuiThemeProvider>
  );
}
