import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  button: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));



export default function HorizontalLinearStepper() {
  const classes = useStyles();
  const [values, setValues] = React.useState({
    Name: '',
    OrganizationDescription: '',
    OrganizationName:'',
    ProjectName:'',
    ProjectDescription:'',
    interfaceDescription:'',
 });
 const [ErrMsg, setErrMsg] = React.useState(false);
 const [errorInfo, setErrorInfo] = React.useState({
  Name: false,
  OrganizationDescription: false,
    OrganizationName:false,
    ProjectName:false,
    ProjectDescription:false,
    interfaceDescription:false,
 });
 const [openSnackbar, setOpenSnackbar] = React.useState(false);
 const [value, setValue] = React.useState('IS');
 const [variant, setSnackbarVariant] = React.useState('error');
 const [message, setSnackbarMessage] = React.useState();

 function handleCloseSnackbar(event, reason) {
   if (reason === 'clickaway') {
     return;
   }
   setOpenSnackbar(false);
 }
  const [activeStep, setActiveStep] = React.useState(0);
  const [skipped, setSkipped] = React.useState(new Set());
  const steps = getSteps();
  const[errorvalue,setErrorValue]=React.useState(true);
  function getSteps() {
    return ['Organisation', 'Project', 'Interface'];
  }
 
  const handleChange = name => event => {
    setErrMsg(false);
    setValues({ ...values, [name]: event.target.value });
    switch (name) {
      case 'OrganisationName':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match('^[a-zA-Z0-9,-.:/ ]*$') &&
          event.target.value.length <= 25
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'OrganisationDescription':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match('^[a-zA-Z0-9,-.:+@()/ ]*$') 
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
        case 'interfaceDescription':
          if (event.target.value.length === 0) {
            setValues({ ...values, [name]: '' });
            setErrorInfo({ ...errorInfo, [name]: false });
          } else if (
            event.target.value.match('^[a-zA-Z0-9,-.:+@()/ ]*$')
          ) {
            setErrorInfo({ ...errorInfo, [name]: false });
            setValues({ ...values, [name]: event.target.value });
          } else {
            setErrorInfo({ ...errorInfo, [name]: true });
          }
          break;
          case 'ProjectDescription':
            if (event.target.value.length === 0) {
              setValues({ ...values, [name]: '' });
              setErrorInfo({ ...errorInfo, [name]: false });
            } else if (
              event.target.value.match('^[a-zA-Z0-9,-.:+@()/ ]*$')
            ) {
              setErrorInfo({ ...errorInfo, [name]: false });
              setValues({ ...values, [name]: event.target.value });
            } else {
              setErrorInfo({ ...errorInfo, [name]: true });
            }
            break;
        case 'ProjectName':
          if (event.target.value.length === 0) {
            setValues({ ...values, [name]: '' });
            setErrorInfo({ ...errorInfo, [name]: false });
          } else if (
            event.target.value.match('^[a-zA-Z0-9,-.:/ ]*$') &&
            event.target.value.length <= 25
          ) {
            setErrorInfo({ ...errorInfo, [name]: false });
            setValues({ ...values, [name]: event.target.value });
          } else {
            setErrorInfo({ ...errorInfo, [name]: true });
          }
          break;
          case 'Name':
            if (event.target.value.length === 0) {
              setValues({ ...values, [name]: '' });
              setErrorInfo({ ...errorInfo, [name]: false });
            } else if (
              event.target.value.match('^[a-zA-Z0-9,-.:/ ]*$') &&
              event.target.value.length <= 25
            ) {
              setErrorInfo({ ...errorInfo, [name]: false });
              setValues({ ...values, [name]: event.target.value });
            } else {
              setErrorInfo({ ...errorInfo, [name]: true });
            }
            break;
      default:
        return false;
    }
  };
 
  function getStepContent(step) {
    switch (step) {
      case 0:
        return ( <div>
          {ErrMsg === true ? (
            <div style={{ textAlign: 'center' }} className="addfieldserror">
              Please fill the fields
            </div>
          ) : (
            ''
          )}
    
          <form className="content" noValidate autoComplete="off">
          <Grid container spacing={2}>  
            <Grid
                    item
                    xs={6}
                    sm={6}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
            <TextField
              id="outlined-dense"
              label="Organisation Name "
              className="textField"
              margin="dense"
              onChange={handleChange('OrganisationName')}
              value={values.OrganisationName}
              variant="outlined"
              name="OrganisationName"
            />
            {errorInfo.OrganisationName === true ? (
              <div id="nameid" className="addfieldserror">
                "Allowed alphabets and Numerics ,no special characters are
                allowed.Maximum length is 25."
              </div>
            ) : (
              ''
            )}
            </Grid>
             <Grid
                    item
                    xs={6}
                    sm={6}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
            <TextField
              id="outlined-dense"
              label="Organisation Description "
              className="textField"
              onChange={handleChange('OrganisationDescription')}
              value={values.OrganisationDescription}
              margin="dense"
              variant="outlined"
              name="OrganisationDescription"
            />
             {errorInfo.OrganisationDescription === true ? (
              <div id="nameid" className="addfieldserror">
                "Allowed alphabets and Numerics,no special characters are
                allowed."
              </div>
            ) : (
              ''
            )}
    </Grid>
    </Grid>     
    
          </form>
          </div>);
        break;
      case 1:
        return  (  <div>
          {ErrMsg === true ? (
            <div style={{ textAlign: 'center' }} className="addfieldserror">
              Please fill the fields
            </div>
          ) : (
            ''
          )} 
          <form className="content" noValidate autoComplete="off">
          <Grid container spacing={2}>  
            <Grid
                    item
                    xs={6}
                    sm={6}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
            <TextField
              id="outlined-dense"
              label="Project Name "
              className="textField"
              margin="dense"
              onChange={handleChange('ProjectName')}
              value={values.ProjectName}
              variant="outlined"
              name="ProjectName"
            />
            {errorInfo.ProjectName === true ? (
              <div id="nameid" className="addfieldserror">
                "Allowed alphabets and Numerics ,no special characters are
                allowed.Maximum length is 25."
              </div>
            ) : (
              ''
            )}
            </Grid>
             <Grid
                    item
                    xs={6}
                    sm={6}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
            <TextField
              id="outlined-dense"
              label="Project Description "
              className="textField"
              onChange={handleChange('ProjectDescription')}
              value={values.ProjectDescription}
              margin="dense"
              variant="outlined"
              name="ProjectDescription"
            />
             {errorInfo.ProjectDescription === true ? (
              <div id="nameid" className="addfieldserror">
                 "Allowed alphabets and Numerics,no special characters are
                allowed."
              </div>
            ) : (
              ''
            )}
      </Grid>
      </Grid>     
      
          </form></div>);
        break;
      case 2:
        return ( <div>
          {ErrMsg === true ? (
            <div style={{ textAlign: 'center' }} className="addfieldserror">
              Please fill the fields
            </div>
          ) : (
            ''
          )}<form className="content" noValidate autoComplete="off">
        <Grid container spacing={2}>  
          <Grid
                  item
                  xs={6}
                  sm={6}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
          <TextField
            id="outlined-dense"
            label="Name "
            className="textField"
            margin="dense"
            onChange={handleChange('Name')}
            value={values.Name}
            variant="outlined"
            name="Name"
          />
          {errorInfo.Name === true ? (
            <div id="nameid" className="addfieldserror">
              "Allowed alphabets and Numerics ,no special characters are
                allowed.Maximum length is 25."
            </div>
          ) : (
            ''
          )}
          </Grid>
           <Grid
                  item
                  xs={6}
                  sm={6}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
          <TextField
            id="outlined-dense"
            label="Interface Description "
            className="textField"
            onChange={handleChange('interfaceDescription')}
            value={values.interfaceDescription}
            margin="dense"
            variant="outlined"
            name="interfaceDescription"
          />
           {errorInfo.interfaceDescription === true ? (
            <div id="nameid" className="addfieldserror">
               "Allowed alphabets and Numerics,no special characters are
                allowed."
            </div>
          ) : (
            ''
          )}
      </Grid>
      </Grid>     
      
        </form></div>);
        break;
      default:
        return 'Unknown step';
    }
  }
  

  const isStepOptional = (step) => {
    return step === 1;
  };

  const isStepSkipped = (step) => {
    return skipped.has(step);
  };

  const handleNext = () => {
    let newSkipped = skipped;
    if (isStepSkipped(activeStep)) {
      newSkipped = new Set(newSkipped.values());
      newSkipped.delete(activeStep);
    }

    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    setSkipped(newSkipped);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleSkip = () => {
    if (!isStepOptional(activeStep)) {
      throw new Error("You can't skip a step that isn't optional.");
    }

    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    setSkipped((prevSkipped) => {
      const newSkipped = new Set(prevSkipped.values());
      newSkipped.add(activeStep);
      return newSkipped;
    });
  };

  const handleReset = () => {
    setActiveStep(0);
  };
  

  return (
    <div className={classes.root}>
      <Stepper activeStep={activeStep}>
        {steps.map((label, index) => {
          const stepProps = {};
          const labelProps = {};

          if (isStepSkipped(index)) {
            stepProps.completed = false;
          }
          return (
            <Step key={label} {...stepProps}>
              <StepLabel {...labelProps}>{label}</StepLabel>
              
            </Step>
          );
        })}
      </Stepper>
      <div>
        {activeStep === steps.length ? (
          <div style={{
                margin:'30px'
              }} >
            <Typography className={classes.instructions}>
              Submited successfully
            </Typography>
            <Button style={{
                margin:'30px'
              }} onClick={handleReset} className={classes.button}>
              Reset
            </Button>
          </div>
        ) : (
          <div>
          
            <Typography className={classes.instructions}>{getStepContent(activeStep)}</Typography>
            <div>


              <Button   style={{
                margin:'30px'
              }}
              disabled={activeStep === 0} onClick={handleBack} className={classes.button}>
                Back
              </Button>
              
             

              <Button
                variant="contained"
                color="primary"
                onClick={handleNext} 
                className={classes.button}
              > 
                {activeStep === steps.length - 1 ? 'Submit' : 'Next'}
              </Button>
            </div>
          
          </div>
        )}
        
      </div>
    </div>
  );
}