
const initialStateuserOrg = {
   Organization:null,
   proceed:true
  };
  
const UserOrgReducer = (state=initialStateuserOrg, action) => {
    switch (action.type) {
      case "FETCH_USER_ORG_RESPONSE":console.log("actionUseOrg",action)
       
        
        return {
          ...state,
          Organization:action.payload,
          proceed:false
          
        };
      case "FETCH_USER_ORG_FAILURE":
      case "FETCH_USER_ORG_LOADING":
      case "FETCH_USER_ORG_LOGOUT":
        
        return {
          ...state,
          proceed:true,
          Organization:null
        };
      default:
        return state;
    }
  };

  export default UserOrgReducer ;