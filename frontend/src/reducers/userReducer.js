
const initialState = {
    isAuthenticated: false,
    user: null,
    token: null,
    loginType:null
  };
  
 const UserReducer = (state=initialState, action) => {
    switch (action.type) {
      case "LOGIN":console.log("action",action)
        localStorage.setItem("user", JSON.stringify(action.payload.loginUser.userUUID));
        localStorage.setItem("token", JSON.stringify(action.payload.loginUser.tokenVal));
        localStorage.setItem("loginType", JSON.stringify(action.payload.loginUser.loginType));
        
        return {
          ...state,
          isAuthenticated: true,
          user: action.payload.loginUser.userUUID,
          token: action.payload.loginUser.tokenVal,
          loginType:action.payload.loginUser.loginType
        };
      case "LOGOUT":console.log("action",action)
        localStorage.clear();
        console.log("action",action,"localStorage",localStorage)
        return {
          ...state,
          isAuthenticated: false,
          user: null,
          loginType:null,
          token:null
        };
      default:
        return state;
    }
  };

  export default UserReducer;