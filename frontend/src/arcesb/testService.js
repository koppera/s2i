import React, { useEffect, useContext, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useMutation, useQuery, useManualQuery } from 'graphql-hooks';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import '../css/commonStyle.css';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import Theme from '../css/theme';
import Modal from '@material-ui/core/Modal';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import LinearLoader from '../components/linearLoader';
const TRANSACTION_testService = `
query TestService($transactionInput:transactionInput){
  TestService(input:$transactionInput) {
    statusAPI
  msg
  }
}
`;
const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

const getMuiTheme = () =>
  createMuiTheme(Theme, {
    overrides: {},
  });

export default function WorkFlowExpansionModal(props) {
  console.log(props);
  const [
    testService,
    {
      loading: testServiceloading,
      error: testServiceerror,
      data: testServicedata,
    },
  ] = useManualQuery(TRANSACTION_testService);
  const classes = useStyles();
  const [values, setValues] = React.useState({ name: '' });
  const [ErrMsgNameInfo, setErrMsgNameInfo] = React.useState(false);
  const [ErrMsg, setErrMsg] = React.useState(false);
  const [errorInfo, setErrorInfo] = React.useState({
    name: false,
  });
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [value, setValue] = React.useState('IS');
  const [variant, setSnackbarVariant] = React.useState('error');
  const [message, setSnackbarMessage] = React.useState();

  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }

  const handleChange = (name) => (event) => {
    setErrMsg(false);

    setValues({ ...values, [name]: event.target.value });
    switch (name) {
      case 'name':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else {
          if (event.target.value.match() && event.target.value.length <= 25) {
            setErrorInfo({ ...errorInfo, [name]: false });
            setValues({ ...values, [name]: event.target.value });
          } else {
            setErrorInfo({ ...errorInfo, [name]: true });
          }
        }
        break;

      default:
        return false;
    }
  };

  const handleCloseModal = () => {
    props.handleCloseModal(false);
  };

  const handleSubmit = async () => {
    console.log(values);
    setErrMsg(false);

    if (values.name === '') {
      setErrMsg(true);
    } else {
      setErrMsg(false);
      //setValues({ ...values, name: '' });

      console.log('rowData', props);
      var transaction = {
        workFlowName: props.rowData.workFlowName,
        workSpaceName: props.workspaceName,
        content: values.name,
      };
      props.handleTestService(transaction);
    }
  };

  const handleReset = async () => {
    setErrMsg(false);
    setErrorInfo({
      name: false,
    });
    setValues({
      ...values,
      name: '',
    });
    setErrMsg(false);
  };
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={props.isModalOpen}
        onClose={handleCloseModal}
        className="fabmodal"
      >
        <div className="modalpaper">
          <Grid container className="header">
            <Grid item xs={10} sm={10}>
              <h2 id="modal-title" className="h2">
                Create Transaction
              </h2>
            </Grid>
            <Grid item xs={2} sm={2} className="close">
              <CloseIcon onClick={handleCloseModal} />
            </Grid>
          </Grid>
          <div>
            {ErrMsg === true ? (
              <div style={{ textAlign: 'center' }} className="addfieldserror">
                Please fill the field
              </div>
            ) : (
              ''
            )}
            <form className="content" noValidate autoComplete="off">
              <Grid container spacing={3}>
                <Grid item xs={12} sm={12}>
                  <TextareaAutosize
                    onChange={handleChange('name')}
                    value={values.name}
                    name="name"
                    aria-label="minimum height"
                    rowsMin={3}
                    style={{
                      width: '100%',
                      height: 100,
                    }}
                  />
                </Grid>
                <Grid container spacing={3} className="userformcontent">
                  <Grid item xs={12} sm={6}>
                    <Button
                      onClick={handleReset}
                      variant="contained"
                      fullWidth="true"
                      className="createpartnerbutton"
                      style={{
                        color: '#000006',
                        backgroundColor: '#E5CCFF',
                      }}
                    >
                      Reset
                    </Button>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <Button
                      onClick={handleSubmit}
                      variant="contained"
                      fullWidth="true"
                      className="createpartnerbutton"
                      disabled={values.name === '' ? true : false}
                    >
                      Process Transaction
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
            </form>

            <Snackbar
              anchorOrigin={{
                vertical: 'top',
                horizontal: 'center',
              }}
              autoHideDuration={5000}
              open={openSnackbar}
              onClose={handleCloseSnackbar}
            >
              <MySnackbarContentWrapper
                onClose={handleCloseSnackbar}
                variant={variant}
                message={message}
              />
            </Snackbar>
          </div>
        </div>
      </Modal>
    </MuiThemeProvider>
  );
}
