import MaterialTable from 'material-table';
import { useMutation, useQuery, useManualQuery } from 'graphql-hooks';
import { forwardRef, useEffect } from 'react';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import ReactDOM from 'react-dom';
import MTableToolbar from 'material-table';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import CloseIcon from '@material-ui/icons/Close';
import { withStyles } from '@material-ui/core/styles';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import { Grid, Typography } from '@material-ui/core';
import MuiDialogActions from '@material-ui/core/DialogActions';
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import Timestamp from '../timestamp';
import Error from '../components/emptyPage';
import Loader from '../components/loader';
import Theme from '../css/theme';
import Snackbar from '@material-ui/core/Snackbar';
import MySnackbarContentWrapper from '../components/Snackbar';
import WorkflowExpansion from '../arcesb/workflowExpansion';
import SendIcon from '@material-ui/icons/Send';
import TrackChangesIcon from '@material-ui/icons/TrackChanges';
import SwapVerticalCircleIcon from '@material-ui/icons/SwapVerticalCircle';
import WorkFlowModel from '../components/tmsmodal';
import WorkFlowForm from './workFlowModel';
import MenuItem from '@material-ui/core/MenuItem';
import LinearLoader from '../components/linearLoader';
import WorkFlowProgressLoader from '../components/workFlowProgressBar';
import SelectAllIcon from '@material-ui/icons/SelectAll';
import PartnerAddModal from '../components/tmsmodal';
import PartnerForm from "../arcesb/partnersAddModal";

import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles,
} from '@material-ui/core/styles';

const TRANSACTION_FLOW_850 = `
query flowAPI850($flowData:transactionInput){
  flowAPI850(input:$flowData) {
    Status
  }
}
`;

const TRANSACTION_FLOW_825 = `
query flowAPI825{
  flowAPI825 {
    Status
  }
}
`;

const ADD_Workspace = `
mutation createWorkSpaceAPI($workspaceInput:workspaceInput){
  createWorkSpaceAPI(input:$workspaceInput) {
    workspaceUUID
  }
}
`;
const UPDATE_Workspace = `
mutation updateWorkSpace($workspaceInput:workspaceInput){
  updateWorkSpace(input:$workspaceInput) {
    workspaceUUID
  }
}
`;
const Remove_Workspace = `
mutation removeWorkSpace($workspaceInput:workspaceInput){
  removeWorkSpace(input:$workspaceInput) {
    workspaceUUID
  }
}
`;
const Post_WorkSpace_API = `query postWorkSpaceAPI($workspaceInput:workspaceInput){
  postWorkSpaceAPI(input:$workspaceInput) {
    statusAPI
    msg
  
  }

}

`;
const ALL_WorkSpace = `{
  fetchWorkSpace {
   
    workspaceUUID
    workspaceName
    senderQualifier
    APIStatus
    createdOn
    updatedOn
  }
}
`;

const TRANSACTION_testService = `
query TestService{
  TestService {
    Status
  }
}
`;
const FlowAPI850_X12 = `query flowAPI850_X12($transactionInput:transactionInput){
  flowAPI850_X12 (input:$transactionInput) {
  statusAPI
  msg
  }
}`;
const FlowAPI850_XMLMap = `query flowAPI850_XMLMap($transactionInput:transactionInput){
  flowAPI850_XMLMap (input:$transactionInput) {
  statusAPI
  msg
  }
}`;
const FlowAPI850_JSON = `query flowAPI850_JSON($transactionInput:transactionInput){
  flowAPI850_JSON (input:$transactionInput) {
    statusAPI
    msg
  }
}`;
const FlowAPI850_REST = `query flowAPI850_REST($transactionInput:transactionInput){
  flowAPI850_REST (input:$transactionInput) {
    statusAPI
  msg
  }
}`;
const FlowAPI850_File = `query flowAPI850_File($transactionInput:transactionInput){
  flowAPI850_File (input:$transactionInput) {
    statusAPI
  msg
  }
}`;
const FlowAPI850_Send = `query flowAPI850_Send($transactionInput:transactionInput){
  flowAPI850_Send (input:$transactionInput) {
    statusAPI
  msg
  }
}`;
function iconStyles() {
  return {};
}
const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(2),
    },
  },

  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));
const styles = (theme) => ({
  root: {
    Width: 1200,
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: '#FFFFFF',
  },
});
export default function MaterialTableDemo() {
  const [
    flow850,
    { loading: flow850loading, error: flow850error, data: flow850data },
  ] = useManualQuery(TRANSACTION_FLOW_850);
  const [
    flow825,
    { loading: flow825loading, error: flow825error, data: flow825data },
  ] = useManualQuery(TRANSACTION_FLOW_825);
  const [
    testService,
    {
      loading: testServiceloading,
      error: testServiceerror,
      data: testServicedata,
    },
  ] = useManualQuery(TRANSACTION_testService);
  const [
    flowAPI850_X12,
    { loading: X12loading, error: X12error, data: X12data },
  ] = useManualQuery(FlowAPI850_X12);
  const [
    flowAPI850_XMLMap,
    { loading: XMLMaploading, error: XMLMaperror, data: XMLMapdata },
  ] = useManualQuery(FlowAPI850_XMLMap);
  const [
    flowAPI850_JSON,
    { loading: JSONloading, error: JSONerror, data: JSONdata },
  ] = useManualQuery(FlowAPI850_JSON);
  const [
    flowAPI850_REST,
    { loading: RESTloading, error: RESTerror, data: RESTdata },
  ] = useManualQuery(FlowAPI850_REST);
  const [
    flowAPI850_File,
    { loading: Fileloading, error: Fileerror, data: Filedata },
  ] = useManualQuery(FlowAPI850_File);
  const [
    flowAPI850_Send,
    { loading: Sendloading, error: Senderror, data: Senddata },
  ] = useManualQuery(FlowAPI850_Send);

  /* const [fetchUser, { loading, error, data }] = useManualQuery(GET_USER_QUERY, {
    variables: { id: props.userId }
  })*/
  //ALL_WorkSpace
  const { loading, error, data, refetch } = useQuery(ALL_WorkSpace);
  //ADD Workspace
  const [addWorkSpace, addloading, adddata] = useMutation(ADD_Workspace);
  //Update Workspace
  const [UpdateWorkSpace, updateloading, updatedata] = useMutation(
    UPDATE_Workspace
  );
  //REMOVE Workspace
  const [removeWorkSpace, removeloading, removedata] = useMutation(
    Remove_Workspace
  );

  //fetch api
  const [
    PostAPIWorkspace,
    { loading: wloading, data: wdata, error: werrors },
  ] = useManualQuery(Post_WorkSpace_API);
  const [startTime, setStartTime] = React.useState();
  const [endTime, setEndTime] = React.useState();
  const [msg, setMessage] = React.useState(null);
  const [responseMsg, setResponseMessage] = React.useState();
  const [progressLoaderVal, setProgressLoader] = React.useState(false);
  const [linearLoaderVal, setLinearLoader] = React.useState(false);
  const columns = [
    {
      title: 'UUID',
      field: 'workspaceUUID',
      editable: 'never',
      hidden: true,
    },
    {
      title: 'Partner',
      field: 'workspaceName',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder="Partner"
              margin="normal"
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
              value={props.value}
            />

            <div style={{ color: 'red' }}>
              {!props.rowData.workspaceName ||
              /^[A-Z]{1}[A-Za-z]*$/.test(props.rowData.workspaceName)
                ? ''
                : 'Starts with Uppercase letter followed by lowercase and uppercase letters'}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.workspaceName}</div>;
      },
    },

    {
      title: 'Sender Identifier',
      field: 'senderQualifier',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder="Sender Identifier"
              margin="normal"
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
              value={props.value}
            />

            <div style={{ color: 'red' }}>
              {!props.rowData.senderQualifier ||
              /^[A-Z\s]+$/.test(props.rowData.senderQualifier)
                ? ''
                : 'Enter only Uppercase alphabets'}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.senderQualifier}</div>;
      },
    },
    {
      title: 'API Status',

      // field: 'Status'
      render: (rowData) => {
        //console.log(rowData, rowData.APIStatus);
        if (rowData === undefined) {
          return '';
        } else if (
          rowData.APIStatus == null ||
          rowData.APIStatus === 'Inactive'
        ) {
          return <div style={{ color: 'red' }}>Not Sync </div>;
        } else {
          return <div style={{ color: 'green' }}>Sync </div>;
        }
      },
    },
    /*{
      title: 'transactionType',
      field: 'transactionType',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
    /* label="addresstypeName"*/
    //placeholder="Description"
    /*margin="normal"
              select
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            >
              <MenuItem value={'X12 4010 850'}> X12 4010 850</MenuItem>
              <MenuItem value={'X12 4010 825'}> X12 4010 825 </MenuItem>
            </TextField>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.transactionType}</div>;
      },
    },*/
  ];

  const [open, setOpen] = React.useState(false);
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [serverError, setServerError] = React.useState(false);
  const [message, setSnackbarMessage] = React.useState();
  const [variant, setSnackbarVariant] = React.useState('error');
  const [workspaceData, setData] = React.useState([]);
  const [openWorkFlow, setOpenWorkFlow] = React.useState(false);
  const [rowData, setrowData] = React.useState();
  const [timeloader, setTimeLoader] = React.useState(100);
  const [WSpaceWFlow, setWSpaceWFlow] = React.useState([]);
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const [nameError, setNameError] = React.useState({
    error: false,
    label: '',
    helperText: '',
    validateInput: false,
  });
  const [openPartnerModal,setOpenPartnerModal] = React.useState(false);
  const [partnerTitle,setPartnerTitle] = React.useState('');

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }
  const tableIcons = {
    Add: forwardRef((props, ref) => (
      <AddBox color="primary" {...props} ref={ref} />
    )),
    Check: forwardRef((props, ref) => (
      <Check color="primary" {...props} ref={ref} />
    )),
    Clear: forwardRef((props, ref) => (
      <Clear color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline color="error" {...props} ref={ref} />
    )),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => (
      <Edit color="primary" {...props} ref={ref} />
    )),
    Export: forwardRef((props, ref) => (
      <SaveAlt color="primary" {...props} ref={ref} />
    )),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => (
      <FirstPage color="primary" {...props} ref={ref} />
    )),
    LastPage: forwardRef((props, ref) => (
      <LastPage color="primary" {...props} ref={ref} />
    )),
    NextPage: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft color="primary" {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => (
      <Clear color="primary" {...props} ref={ref} />
    )),
    Search: forwardRef((props, ref) => (
      <Search color="primary" {...props} ref={ref} />
    )),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiDialogTitle: {
          root: {
            backgroundColor: '#0b153e',
            color: '#FFFFFF',
          },
        },
        MuiGrid: {
          container: {
            flexWrap: 'nowrap',
          },
        },
        MuiSelect: {
          icon: {
            color: '#0b153e',
          },
        },
        MuiTableCell: {
          root: {
            paddingRight: '50px',
          },
        },
        MuiTypography: {
          h6: {
            fontSize: 14,
            fontFamily: 'Arial !important',
          },
        },
      },
    });
  const classes1 = useStyles();
  useEffect(() => {
    if (data !== undefined) {
      console.log(data.fetchWorkSpace);
      setData(data.fetchWorkSpace);
      setServerError(false);
    } else {
      console.log('server error');
      setServerError(true);
    }
  }, [data, responseMsg]);
  const handleRefetch = (value, message, modelval) => {
    setMessage(null);
    setEndTime();
    setStartTime();
    setResponseMessage();
    setProgressLoader(false);
    refetch({
      updateData(_, data) {
        return data;
      },
    });
    setOpenSnackbar(true);
    setSnackbarMessage(message);
    if (value) {
      setTimeLoader(90);
      setSnackbarVariant('error');
    } else {
      setTimeLoader(100);
      setSnackbarVariant('success');
    }
    if (modelval) {
      handleCloseForm();
      handleClosePartnerForm();
    }
  };
  const handleWorkFlow = async (workflowData, message, modelval) => {
    if (modelval) {
      handleCloseForm();
    }
    setProgressLoader(true);

    //setEndTime(15);
    // setMessage('SUCCESS');
    //  setEndTime(15);
    //setResponseMessage('Done');

    setStartTime(1);

    flowAPI850_X12({
      variables: {
        transactionInput: workflowData,
      },
    }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
      console.log(data, error, graphQLErrors, networkError, cacheHit);
      console.log(X12loading, X12error, X12data);
      if (data !== null && error == false) {
        //props.onSuccess(X12error, data, true);
        //API XMLMAP

        //callback
        console.log(responseMsg); // myname
        setResponseMessage('X12 Connector created');
        setEndTime(15);
        flowAPI850_XMLMap({
          variables: {
            transactionInput: workflowData,
          },
        }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
          console.log(data, error, graphQLErrors, networkError, cacheHit);
          //console.log(MLMaploading, XMLMaperror, XMLMapdata);
          if (data !== null && error == false) {
            //add JSON
            setStartTime(16);
            setEndTime(30);
            setResponseMessage('XMLMap Connector created');
            flowAPI850_JSON({
              variables: {
                transactionInput: workflowData,
              },
            }).then(
              ({ data, error, graphQLErrors, networkError, cacheHit }) => {
                console.log(data, error, graphQLErrors, networkError, cacheHit);
                console.log(JSONloading, JSONerror, JSONdata);
                if (data !== null && error == false) {
                  setStartTime(31);
                  setEndTime(45);
                  setResponseMessage('JSON Connector created');
                  flowAPI850_REST({
                    variables: {
                      transactionInput: workflowData,
                    },
                  }).then(
                    ({
                      data,
                      error,
                      graphQLErrors,
                      networkError,
                      cacheHit,
                    }) => {
                      console.log(
                        data,
                        error,
                        graphQLErrors,
                        networkError,
                        cacheHit
                      );
                      console.log(RESTloading, RESTerror, RESTdata);
                      if (data !== null && error == false) {
                        setStartTime(46);
                        setEndTime(60);
                        setResponseMessage('Elastic Search Connector created');
                        flowAPI850_File({
                          variables: {
                            transactionInput: workflowData,
                          },
                        }).then(
                          ({
                            data,
                            error,
                            graphQLErrors,
                            networkError,
                            cacheHit,
                          }) => {
                            console.log(
                              data,
                              error,
                              graphQLErrors,
                              networkError,
                              cacheHit
                            );

                            if (data !== null && error == false) {
                              setStartTime(61);
                              setEndTime(75);
                              setResponseMessage('File Connector created');
                              flowAPI850_Send({
                                variables: {
                                  transactionInput: workflowData,
                                },
                              }).then(
                                async ({
                                  data,
                                  error,
                                  graphQLErrors,
                                  networkError,
                                  cacheHit,
                                }) => {
                                  console.log(
                                    data,
                                    error,
                                    graphQLErrors,
                                    networkError,
                                    cacheHit
                                  );
                                  console.log(JSONloading, JSONerror, JSONdata);
                                  if (data !== null && error == false) {
                                    setStartTime(76);
                                    setEndTime(100);
                                    setResponseMessage('Map Json created');
                                    await setTimeout(async () => {
                                      setMessage('success');

                                      console.log(data.flowAPI850_Send.msg);
                                      // setProgressLoader(false);
                                      await handleRefetch(
                                        error,
                                        'The Work Flow has been created successfully!',
                                        true
                                      );
                                    }, 1200);
                                  } else {
                                    setResponseMessage(
                                      'Connector creation failed'
                                    );
                                    setEndTime(100);
                                    setTimeout(() => {
                                      setMessage('failure');
                                      setProgressLoader(false);
                                      handleRefetch(
                                        error,
                                        'Could not create work flow. Please try again!',
                                        false
                                      );
                                    }, 1200);
                                  }
                                }
                              );
                            } else {
                              setResponseMessage('Connector creation failed');
                              setEndTime(75);
                              setTimeout(() => {
                                setMessage('failure');
                                setProgressLoader(false);
                                handleRefetch(
                                  error,
                                  ' Could not create work flow. Please try again!',
                                  false
                                );
                              }, 1200);
                            }
                          }
                        );
                      } else {
                        setResponseMessage('Connector creation failed');
                        setEndTime(60);
                        setTimeout(() => {
                          setMessage('failure');
                          setProgressLoader(false);
                          handleRefetch(
                            error,
                            'Could not create work flow. Please try again!',
                            false
                          );
                        }, 1200);
                      }
                    }
                  );
                } else {
                  setResponseMessage('Connector creation failed');
                  setEndTime(45);
                  setTimeout(() => {
                    setMessage('failure');
                    setProgressLoader(false);
                    handleRefetch(
                      error,
                      'Could not create work flow. Please try again!',
                      false
                    );
                  }, 1200);
                }
              }
            );
          } else {
            //xmlmap false
            setResponseMessage('Connector creation failed');
            setEndTime(30);
            setTimeout(() => {
              setMessage('failure');
              setProgressLoader(false);
              handleRefetch(
                error,
                'Could not create work flow. Please try again!',
                false
              );
            }, 1200);
          }
        });
      } else {
        //x12 false
        console.log('x12hhhhhh');

        setResponseMessage('Connector creation failed');
        setEndTime(15);
        setTimeout(() => {
          setMessage('failure');
          handleRefetch(
            error,
            'Could not create work flow. Please try again!',
            false
          );
        }, 1200);
      }
    });
  };
  async function handleAddWorkSpace(newData) {
    return await new Promise(async (resolve) => {
      console.log(newData);
      resolve();
      await addWorkSpace({
        variables: {
          workspaceInput: {
            workspaceName: newData.workspaceName,
            senderQualifier: newData.senderQualifier,
            //transactionType:newData.transactionType
          },
        },
      })
        .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
          console.log(data, error, graphQLErrors, networkError, cacheHit);
          if (data !== null && error == false) {
            handleRefetch(error, 'Partner created successfully');
          } else if (error && graphQLErrors.length > 0) {
            let duperror = graphQLErrors[0].message;
            console.log(duperror);
            if (duperror !== null && duperror.indexOf('ER_DUP_ENTRY') !== -1) {
              handleRefetch(error, 'Name already existed');
            }
          }
        })
        .catch((e) => {
          // you can do something with the error here
          console.log(e);
        });
    });
  }
  const handleAPISuccess = async (Status, aPIMsg) => {
    if (Status === 'failure' || Status === null) {
      handleRefetch(true, aPIMsg);
    } else {
      setLinearLoader(false);
      handleRefetch(false, aPIMsg);
    }
  };
  const handleCallAPI = async (event, rowData) => {
    console.log('rowData', rowData);
    setLinearLoader(true);
    //setLoad(true);
    //setStartTime(1);
    await PostAPIWorkspace({
      variables: {
        workspaceInput: {
          workspaceName: rowData.workspaceName,
          workspaceUUID: rowData.workspaceUUID,
        },
      },
    }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
      console.log(data, error, graphQLErrors, networkError, cacheHit);
      if (data !== null && error == false) {
        handleAPISuccess(
          data.postWorkSpaceAPI.statusAPI,
          data.postWorkSpaceAPI.msg
        );
        // setEndTime(100);
        setMessage('success');
        setLinearLoader(false);
      } else if (error && graphQLErrors.length > 0) {
        let duperror = graphQLErrors[0].message;
        console.log(duperror);
        //setEndTime(85);
        setMessage('failure');
        setLinearLoader(false);
        if (duperror !== null) {
          handleRefetch(error, 'Partner did not sync. Please try again!');
        }
      } else {
        handleRefetch(error, 'graphql hooks error');
      }
    });
  };
  const handelProgressLoadeClose = () => {
    setProgressLoader(false);
  };
  const handleCreateConnectorFlow = async (event, rowData) => {
    console.log('rowData', rowData);
    await flow850({
      variables: {
        flowData: {
          workSpaceName: rowData.workspaceName,
          // transactionType:rowData.transactionType,
          senderIdentifier: rowData.senderQualifier,
        },
      },
    });
    /*if(rowData.transactionType==='850'){
    console.log('rowData', rowData.transactionType);
    await flow850({
      variables:{
        flow850:{
       
          workSpaceName:rowData.workspaceName,
          transactionType:rowData.transactionType,
          senderIdentifier:rowData.senderQualifier
        }
      }
    });
  }
  if(rowData.transactionType==='825'){
    console.log('rowData', rowData.transactionType);
    await flow825({});
  }
*/
  };

  const handleClickOpenModel = (e, rowdata) => {
    setOpenWorkFlow(true);
    setWSpaceWFlow(rowdata);
  };
  const handleCloseForm = (e) => {
    setOpenWorkFlow(false);
  };
  const handleTestService = async (event, rowdata) => {
    console.log('rowData', rowdata);
    await testService({});
  };
  //testService
  /* if (linearLoaderVal == true)
    return (
      <PercentageLoader
        maxTimeLoader={handleCloseLoader}
        value={startTime}
        time={endTime}
        msg={msg}
      />
    );

  
  if (progressLoaderVal == true)
    return (
      <ProgressLoader
        maxTimeLoader={handleCloseLoader}
        value={startTime}
        time={endTime}
        msg={msg}
      />
    );*/

  async function handleUpdateWorkSpace(newData, oldData) {
    return await new Promise(async (resolve) => {
      resolve();
      console.log(newData);
      await UpdateWorkSpace({
        variables: {
          workspaceInput: {
            workspaceName: newData.workspaceName,
            senderQualifier: newData.senderQualifier,
            workspaceUUID: newData.workspaceUUID,
          },
        },
      })
        .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
          console.log(data, error, graphQLErrors, networkError, cacheHit);
          if (data !== null && error == false) {
            handleRefetch(error, 'Updated successfully');
          } else if (error && graphQLErrors.length > 0) {
            let duperror = graphQLErrors[0].message;
            console.log(duperror);
            if (duperror !== null && duperror.indexOf('ER_DUP_ENTRY') !== -1) {
              handleRefetch(error, 'Partner already existed');
            }
          }
        })
        .catch((e) => {
          // you can do something with the error here
          console.log(e);
        });
    });
  }

  async function handleRemoveWorkSpace(oldData) {
    return await new Promise(async (resolve) => {
      resolve();
      await removeWorkSpace({
        variables: {
          workspaceInput: {
            workspaceUUID: oldData.workspaceUUID,
          },
        },
      })
        .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
          console.log(data, error, graphQLErrors, networkError, cacheHit);
          if (data !== null && error == false) {
            handleRefetch(error, 'Deleted successfully');
          } else if (error && graphQLErrors.length > 0) {
            let duperror = graphQLErrors[0].message;
            console.log(duperror);
            if (
              duperror !== null &&
              duperror.indexOf('ER_ROW_IS_REFERENCED') !== -1
            ) {
              setOpenSnackbar(true);
              setSnackbarVariant('error');
              setSnackbarMessage(
                'Selected row is referenced in workflow table'
              );
            }
          }
        })
        .catch((e) => {
          // you can do something with the error here
          console.log(e);
        });
    });
  }
  const handleClickAddOpen = () =>
   {
    setPartnerTitle('Add');
    setOpenPartnerModal(true);
  }
  const handleClosePartnerForm =() =>{
    setOpenPartnerModal(false);
  }
  const handleUpdatePartner = (event,rowData) => {
    setrowData(rowData);
    setPartnerTitle('Update');
    setOpenPartnerModal(true);
  }

  if (loading && progressLoaderVal == false && linearLoaderVal == false)
    return <Loader />;
  if (serverError)
    return <Error type={'Sever connection lost.Please try again'} />;
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <MaterialTable
        icons={tableIcons}
        title="Partner"
        columns={columns}
        data={workspaceData}
        editable={{
         
          onRowDelete: (oldData) => handleRemoveWorkSpace(oldData),
        }}
        actions={[
          (rowData) => {
            //  console.log(rowData, rowData.APIStatus);
            return rowData.APIStatus == null || rowData.APIStatus == 'Inactive'
              ? {
                  icon: SendIcon,
                  tooltip: 'Sync Partner',
                  disabled: false,
                  onClick: (event, rowData) => handleCallAPI(event, rowData),
                }
              : {
                  icon: SendIcon,
                  tooltip: 'Sync Partner',
                  disabled: true,
                  //hidden: true
                };    
          },
          {
            icon: AddIcon,
            tooltip: 'Add',
            className: 'addIconColor',
            isFreeAction: true,
            onClick: (event) => handleClickAddOpen(event),
          },
          (rowData) => {
            //  console.log(rowData, rowData.APIStatus);
            return rowData.APIStatus == null || rowData.APIStatus == 'Inactive'
              ? {
                  icon: Edit,
                  tooltip: 'Sync Partner',
                  disabled: false,
                  onClick: (event, rowData) => handleUpdatePartner(event, rowData),
                }
              : {
                  icon: Edit,
                  tooltip: 'Sync Partner',
                  disabled: true,
                  //hidden: true
                };    
          },
          (rowData) => {
            console.log(rowData, rowData.APIStatus);
            return rowData.APIStatus == null || rowData.APIStatus == 'Inactive'
              ? {
                  icon: SelectAllIcon,
                  tooltip: 'Select Work Flow',
                  disabled: true,
                }
              : {
                  icon: SelectAllIcon,
                  tooltip: 'Select Work Flow',
                  disabled: false,
                  onClick: (event, rowData) =>
                    handleClickOpenModel(event, rowData),
                  //hidden: true
                };
          },
        ]}
        detailPanel={[
          {
            render: (rowData) => {
              return <WorkflowExpansion rowData={rowData} />;
            },
          },
        ]}
        options={{
          headerStyle: {
            textAlign: 'center',
            fontSize: 12,
            fontWeight: 'bold',
            fontFamily: 'Arial !important',
            backgroundColor: '#0b153e',
            color: '#ffffff',
            padding: '4px',
          },
          searchFieldStyle: {
            color: '#0b153e',
          },
          actionsColumnIndex: -1,
          pageSize: 10,
          pageSizeOptions: [10, 25, 50, 100],
          toolbar: true,
          paging: true,
        }}
        // onRowClick={(handleClickOpen)}
      />
      <WorkFlowModel
        handleCloseModal={handleCloseForm}
        isModalOpen={openWorkFlow}
      >
        <WorkFlowForm
          handleCloseModal={handleCloseForm}
          isModalOpen={openWorkFlow}
          rowData={WSpaceWFlow}
          onSuccess={handleRefetch}
          workFlowData={handleWorkFlow}
        />
      </WorkFlowModel>

      <PartnerAddModal
        handleCloseModal={handleClosePartnerForm}
        isModalOpen={openPartnerModal}
      >
        <PartnerForm
          handleCloseModal={handleClosePartnerForm}
          onSuccess={handleRefetch}
          isModalOpen={openPartnerModal}
          rowData={rowData}
          title={partnerTitle}
        />
      </PartnerAddModal>

      {/* <PercentageLoader
          maxTimeLoader={handleCloseLoader}
          value={startTime}
          time={endTime}
          msg={msg}
       /> */}

      <WorkFlowProgressLoader
        isModalOpen={progressLoaderVal}
        loaderClose={handleRefetch}
        value={startTime}
        time={endTime}
        msg={msg}
        labelText={responseMsg}
      />
      <LinearLoader isModalOpen={linearLoaderVal} />
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        autoHideDuration={1500}
        open={openSnackbar}
        onClose={handleCloseSnackbar}
      >
        <MySnackbarContentWrapper
          onClose={handleCloseSnackbar}
          variant={variant}
          message={message}
        />
      </Snackbar>
    </MuiThemeProvider>
  );
}