import MaterialTable from 'material-table';
import { useMutation, useQuery, useManualQuery } from 'graphql-hooks';
import { forwardRef, useEffect } from 'react';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import ReactDOM from 'react-dom';
import MTableToolbar from 'material-table';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import CloseIcon from '@material-ui/icons/Close';
import { withStyles } from '@material-ui/core/styles';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import { Grid, Typography } from '@material-ui/core';
import MuiDialogActions from '@material-ui/core/DialogActions';
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import Timestamp from '../timestamp';
import Error from '../components/emptyPage';
import Loader from '../components/loader';
import Theme from '../css/theme';
import Snackbar from '@material-ui/core/Snackbar';
import MySnackbarContentWrapper from '../components/Snackbar';
import Action from '../components/tmsmodal';
import ActionForm from '../arcesbold/actionForm';
import TrackChangesIcon from '@material-ui/icons/TrackChanges';
import WorkFlowExapnsionForm from '../components/tmsmodal';
import WorkFlowExapansionModal from './testService';
import SendIcon from '@material-ui/icons/Send';
import SwapVerticalCircleIcon from '@material-ui/icons/SwapVerticalCircle';
import UpdateOutlinedIcon from '@material-ui/icons/UpdateOutlined';
import LinearLoader from '../components/linearLoader';
import EditIcon from '@material-ui/icons/Edit';
import ProgressLoader from '../components/progressBar';
import WorkFlowProgressLoader from '../components/workFlowProgressBar';
import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles,
} from '@material-ui/core/styles';

const fetchWorkFlowByWorkspace = `query fetchWorkFlowByWorkspace($workspaceUUID:String){
  fetchWorkFlowByWorkspace (workspaceUUID:$workspaceUUID) {
    workFlowName
    workspaceUUID
    workFlowUUID
    APIStatus
    ResendWorkflow
    createdOn
    updatedOn
  }
    
}
`;
const TRANSACTION_testService = `
query TestService($transactionInput:transactionInput){
  TestService(input:$transactionInput) {
    statusAPI
  msg
  }
}
`;
const Remove_WorkFlow = `
mutation removeWorkFlow($transactionInput:transactionInput){
  removeWorkFlow(input:$transactionInput) {
    workspaceUUID
  }
}
`;
const FlowAPI850_X12 = `query flowAPI850_X12($transactionInput:transactionInput){
  flowAPI850_X12 (input:$transactionInput) {
  statusAPI
  msg
  }
}`;
const FlowAPI850_XMLMap = `query flowAPI850_XMLMap($transactionInput:transactionInput){
  flowAPI850_XMLMap (input:$transactionInput) {
  statusAPI
  msg
  }
}`;
const FlowAPI850_JSON = `query flowAPI850_JSON($transactionInput:transactionInput){
  flowAPI850_JSON (input:$transactionInput) {
    statusAPI
    msg
  }
}`;
const FlowAPI850_REST = `query flowAPI850_REST($transactionInput:transactionInput){
  flowAPI850_REST (input:$transactionInput) {
    statusAPI
  msg
  }
}`;
const FlowAPI850_File = `query flowAPI850_File($transactionInput:transactionInput){
  flowAPI850_File (input:$transactionInput) {
    statusAPI
  msg
  }
}`;
const FlowAPI850_Send = `query flowAPI850_Send($transactionInput:transactionInput){
  flowAPI850_Send (input:$transactionInput) {
    statusAPI
  msg
  }
}`;
function iconStyles() {
  return {};
}
const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(2),
    },
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));
const styles = (theme) => ({
  root: {
    Width: 1200,
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: '#FFFFFF',
  },
});
export default function MaterialTableDemo(props) {
  //FETCH ALL AS2PROFILE
  //console.log(props);
  const { loading, error, data, refetch } = useQuery(fetchWorkFlowByWorkspace, {
    variables: {
      workspaceUUID: props.rowData.workspaceUUID,
    },
  });
  const [
    flowAPI850_X12,
    { loading: X12loading, error: X12error, data: X12data },
  ] = useManualQuery(FlowAPI850_X12);
  const [
    flowAPI850_XMLMap,
    { loading: XMLMaploading, error: XMLMaperror, data: XMLMapdata },
  ] = useManualQuery(FlowAPI850_XMLMap);
  const [
    flowAPI850_JSON,
    { loading: JSONloading, error: JSONerror, data: JSONdata },
  ] = useManualQuery(FlowAPI850_JSON);
  const [
    flowAPI850_REST,
    { loading: RESTloading, error: RESTerror, data: RESTdata },
  ] = useManualQuery(FlowAPI850_REST);
  const [
    flowAPI850_File,
    { loading: Fileloading, error: Fileerror, data: Filedata },
  ] = useManualQuery(FlowAPI850_File);
  const [
    flowAPI850_Send,
    { loading: Sendloading, error: Senderror, data: Senddata },
  ] = useManualQuery(FlowAPI850_Send);

  const [
    testService,
    {
      loading: testServiceloading,
      error: testServiceerror,
      data: testServicedata,
    },
  ] = useManualQuery(TRANSACTION_testService);
  //REMOVE WorkFlow
  const [removeWorkFlow, removeloading, removedata] = useMutation(
    Remove_WorkFlow
  );
  // console.log(loading, error, data, refetch);
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [serverError, setServerError] = React.useState(false);
  const [message, setSnackbarMessage] = React.useState();
  const [variant, setSnackbarVariant] = React.useState('error');
  const [OpenAction, setOpenAction] = React.useState(false);
  const [actionForm, setOpenActionForm] = React.useState('update');
  const [openWorkFlow, setOpenWorkFlow] = React.useState(false);
  const [WSpaceWFlow, setWSpaceWFlow] = React.useState([]);
  const [rowData, setrowData] = React.useState();
  const [workflowData, setData] = React.useState([]);
  const [linearLoaderVal, setLinearLoader] = React.useState(false);
  const [progressLoaderVal, setProgressLoader] = React.useState(false);
  const [load, setLoad] = React.useState(false);
  const [startTime, setStartTime] = React.useState();
  const [endTime, setEndTime] = React.useState();
  const [msg, setMessage] = React.useState(null);
  const [responseMsg, setResponseMessage] = React.useState();
  //work flow loader
  const [workFlowLoaderVal, setWorkFlowLoaderVal] = React.useState(false);
  const [workFlowLoad, setWorkFlowLoad] = React.useState(false);
  const [workFlowStartTime, setWorkFlowStartTime] = React.useState();
  const [workFlowEndTime, setWorkFlowEndTime] = React.useState();
  const [workFlowMsg, setWorkFlowMessage] = React.useState(null);
  const [workFlowResponseMsg, setWorkFlowResponseMessage] = React.useState();
  const columns = [
    {
      title: 'UUID',
      field: 'workFlowUUID',
      editable: 'never',
      hidden: true,
    },
    {
      title: 'WorkFlow',
      field: 'workFlowName',
    },
    {
      title: 'ResendWorkflow',
      field: 'ResendWorkflow',
      editable: 'never',
      hidden: true,
    },
    {
      title: 'API Status',
      render: (rowData) => {
        //  console.log(rowData, rowData.APIStatus);
        if (rowData === undefined) {
          return '';
        } else if (
          rowData.APIStatus == null ||
          rowData.APIStatus === 'Inactive'
        ) {
          if (rowData.ResendWorkflow == 1) {
            return (
              <div style={{ color: 'red' }}>
                Creating X12 connector failed. To create work flow please click
                onto resend icon!
              </div>
            );
          } else if (rowData.ResendWorkflow == 2) {
            return (
              <div style={{ color: 'red' }}>
                Creating XMLMAP connector failed. To create work flow please
                click onto resend icon!
              </div>
            );
          } else if (rowData.ResendWorkflow == 3) {
            return (
              <div style={{ color: 'red' }}>
                Creating JSON connector failed. To create work flow please click
                onto resend icon!
              </div>
            );
          } else if (rowData.ResendWorkflow == 4) {
            return (
              <div style={{ color: 'red' }}>
                Creating Elastic Search connector failed. To create work flow
                please click onto resend icon!
              </div>
            );
          } else if (rowData.ResendWorkflow == 5) {
            return (
              <div style={{ color: 'red' }}>
                Creating File connector failed. To create work flow please click
                onto resend icon!
              </div>
            );
          } else if (rowData.ResendWorkflow == 6) {
            return (
              <div style={{ color: 'red' }}>
                Creating Map JSON connector failed. To create work flow please
                click onto resend icon!
              </div>
            );
          } else {
            console.log(rowData.ResendWorkflow);
          }
        } else {
          return <div style={{ color: 'green' }}>Work Flow Created</div>;
        }
      },
    },
  ];
  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }
  const tableIcons = {
    Add: forwardRef((props, ref) => (
      <AddBox color="primary" {...props} ref={ref} />
    )),
    Check: forwardRef((props, ref) => (
      <Check color="primary" {...props} ref={ref} />
    )),
    Clear: forwardRef((props, ref) => (
      <Clear color="error" {...props} ref={ref} />
    )),

    Delete: forwardRef((props, ref) => (
      <DeleteOutline
        /*style={{marginLeft:5}}*/ color="error"
        {...props}
        ref={ref}
      />
    )),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => (
      <Edit color="primary" {...props} ref={ref} />
    )),
    Export: forwardRef((props, ref) => (
      <SaveAlt color="primary" {...props} ref={ref} />
    )),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => (
      <FirstPage color="primary" {...props} ref={ref} />
    )),
    LastPage: forwardRef((props, ref) => (
      <LastPage color="primary" {...props} ref={ref} />
    )),
    NextPage: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft color="primary" {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => (
      <Clear color="primary" {...props} ref={ref} />
    )),
    Search: forwardRef((props, ref) => (
      <Search color="primary" {...props} ref={ref} />
    )),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };
  const handleOpenExpansionModel = (e, rowdata) => {
    setOpenWorkFlow(true);
    setWSpaceWFlow(rowdata);
  };
  const handleCloseForm = (e) => {
    setOpenWorkFlow(false);
  };
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiDialogTitle: {
          root: {
            backgroundColor: '#0b153e',
            color: '#FFFFFF',
          },
        },
        MuiSelect: {
          icon: {
            color: '#0b153e',
          },
        },
        MuiTypography: {
          h6: {
            fontSize: 14,
            fontFamily: 'Arial !important',
          },
        },
      },
    });
  const classes1 = useStyles();
  const handleRefetch = (value, message, modelVal) => {
    setWorkFlowMessage(null);
    setWorkFlowEndTime();
    setWorkFlowStartTime();
    setWorkFlowResponseMessage();
    setWorkFlowLoaderVal(false);
    refetch({
      updateData(_, data) {
        return data;
      },
    });
    setOpenSnackbar(true);
    setSnackbarMessage(message);
    if (value) {
      setSnackbarVariant('error');
    } else {
      setSnackbarVariant('success');
    }
    if (modelVal) {
      handleCloseForm();
    }
  };

  useEffect(() => {
    if (data !== undefined) {
      console.log(data.fetchWorkFlowByWorkspace);
      setData(data.fetchWorkFlowByWorkspace);
      setServerError(false);
    } else {
      console.log('server error');
      setServerError(true);
    }
  }, [data, props]);
  const handleTestService = (transactionData) => {
    handleCloseForm();
    //   setLinearLoader(true);
    setProgressLoader(true);
    setTimeout(() => {
      setStartTime(1);
      // setEndTime(2);
    }, 500);
    testService({
      variables: {
        transactionInput: transactionData,
      },
    }).then(async ({ data, error, graphQLErrors, networkError, cacheHit }) => {
      console.log(data, error, graphQLErrors, networkError, cacheHit);
      if (data !== null && error == false) {
        console.log('292 success test service');
        if (
          data.TestService.statusAPI === 'failure' ||
          data.TestService.statusAPI === null
        ) {
          //setLinearLoader(false);
          await handleRefetch(true, data.TestService.msg, false);
        } else {
          console.log('299 success test service');
          setLoad(true);
          setResponseMessage('X12 Connector created');
          console.log('X12 Connector created');
          setTimeout(() => {
            //  setStartTime(25);
            setEndTime(50);
          }, 500);
          setResponseMessage('XMLMap Connector created');
          console.log('XMLMap Connector created');
          setTimeout(() => {
            // setStartTime(50);
            setEndTime(75);
          }, 500);
          setResponseMessage('JSON Connector created');
          console.log('JSON Connector created');
          setTimeout(() => {
            // setStartTime(75);
            setEndTime(95);
          }, 500);
          setResponseMessage('Elastic Search Connector created');
          console.log('Elastic Search Connector created');
          setTimeout(() => {
            // setStartTime(95);
            setEndTime(100);
          }, 500);
          /*  setResponseMessage('File Connector created');
          console.log('File Connector created');
          setTimeout(() => {
            setStartTime(76);
            setEndTime(85);
          }, 500);
          setResponseMessage('Map Json created');
          console.log('Map Json created');
          await setTimeout(async () => {
            await setStartTime(86);
            await setEndTime(95);
          }, 500);
          await setTimeout(async () => {
            await setStartTime(96);
            await setEndTime(100);

            
          }, 500);*/
          // setLinearLoader(false);
          //  handleRefetch(false, data.TestService.msg, true);
        }
      } else if (error && graphQLErrors != null && graphQLErrors.length > 0) {
        let duperror = graphQLErrors[0].message;
        console.log(duperror);
        if (duperror !== null) {
          setLinearLoader(false);
          handleRefetch(
            error,
            'Could not process transaction.Please try again!',
            false
          );
        }
      } else {
        setLinearLoader(false);
        handleRefetch(
          error,
          'Could not process transaction.Please try again!',
          false
        );
      }
    });
  };

  async function handleRemoveWorkFlow(oldData) {
    return await new Promise(async (resolve) => {
      resolve();
      await removeWorkFlow({
        variables: {
          transactionInput: {
            workFlowUUID: oldData.workFlowUUID,
          },
        },
      })
        .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
          console.log(data, error, graphQLErrors, networkError, cacheHit);
          if (data !== null && error == false) {
            handleRefetch(error, 'Deleted successfully');
          } else if (error && graphQLErrors.length > 0) {
            let duperror = graphQLErrors[0].message;
            console.log(duperror);
            if (
              duperror !== null &&
              duperror.indexOf('ER_ROW_IS_REFERENCED') !== -1
            ) {
              setOpenSnackbar(true);
              setSnackbarVariant('error');
              setSnackbarMessage(
                'Selected row is referenced in workflow table'
              );
            }
          }
        })
        .catch((e) => {
          // you can do something with the error here
          console.log(e);
        });
    });
  }
  function ToolTipFunction(name) {
    var str = name;
    var ret = name.replace(/_Inbound/g, '');
    str = ret.split('_').pop();
    return str;
  }
  const handleCloseLoader = () => {
    setLoad(false);
  };
  const handleProgressLoader = (val) => {
    console.log(val);
    if (val == 100) {
      setProgressLoader(false);
      setStartTime(0);
      handleRefetch(false, 'Transaction Processed Successfully');
    }
  };

  function X12(workflowData) {
    setWorkFlowLoaderVal(true);
    setWorkFlowStartTime(1);
    flowAPI850_X12({
      variables: {
        transactionInput: workflowData,
      },
    }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
      console.log(data, error, graphQLErrors, networkError, cacheHit);
      console.log(X12loading, X12error, X12data);
      if (data !== null && error == false) {
        //props.onSuccess(X12error, data, true);
        //API XMLMAP

        //callback
        console.log(responseMsg); // myname

        setWorkFlowLoad(true);
        setWorkFlowResponseMessage('X12 Connector created');

        setWorkFlowEndTime(15);
        //calling 2nd api call
        XMLMap(workflowData);
      } else {
        //x12 false
        console.log('x12hhhhhh');

        setWorkFlowResponseMessage('Connector creation failed');
        setWorkFlowEndTime(15);
        setTimeout(() => {
          setWorkFlowMessage('failure');
          setWorkFlowLoaderVal(false);
          handleRefetch(
            error,
            'Could not create work flow. Please try again!',
            false
          );
        }, 1200);
      }
    });
  }
  function XMLMap(workflowData) {
    flowAPI850_XMLMap({
      variables: {
        transactionInput: workflowData,
      },
    }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
      console.log(data, error, graphQLErrors, networkError, cacheHit);
      //console.log(MLMaploading, XMLMaperror, XMLMapdata);
      if (data !== null && error == false) {
        //add JSON
        setWorkFlowResponseMessage('XMLMap Connector created');

        setWorkFlowStartTime(16);
        setWorkFlowEndTime(30);

        //calling 3rd api call
        JSON(workflowData);
      } else {
        //xmlmap false
        setWorkFlowResponseMessage('Connector creation failed');
        setWorkFlowEndTime(30);
        setTimeout(() => {
          setWorkFlowMessage('failure');
          setWorkFlowLoaderVal(false);
          handleRefetch(
            error,
            'Could not create work flow. Please try again!',
            false
          );
        }, 1200);
      }
    });
  }
  function JSON(workflowData) {
    flowAPI850_JSON({
      variables: {
        transactionInput: workflowData,
      },
    }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
      console.log(data, error, graphQLErrors, networkError, cacheHit);
      console.log(JSONloading, JSONerror, JSONdata);
      if (data !== null && error == false) {
        setWorkFlowResponseMessage('JSON Connector created');

        setWorkFlowStartTime(31);
        setWorkFlowEndTime(45);

        //calling 4TH api call
        REST(workflowData);
      } else {
        setWorkFlowResponseMessage('Connector creation failed');
        setWorkFlowEndTime(45);
        setTimeout(() => {
          setWorkFlowMessage('failure');
          setWorkFlowLoaderVal(false);
          handleRefetch(
            error,
            'Could not create work flow. Please try again!',
            false
          );
        }, 1200);
      }
    });
  }
  function REST(workflowData) {
    flowAPI850_REST({
      variables: {
        transactionInput: workflowData,
      },
    }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
      console.log(data, error, graphQLErrors, networkError, cacheHit);
      console.log(RESTloading, RESTerror, RESTdata);
      if (data !== null && error == false) {
        setWorkFlowResponseMessage('Elastic Search Connector created');

        setWorkFlowStartTime(46);
        setWorkFlowEndTime(60);

        //calling 5TH api call
        FILE(workflowData);
      } else {
        setWorkFlowResponseMessage('Connector creation failed');
        setWorkFlowEndTime(60);
        setTimeout(() => {
          setWorkFlowMessage('failure');
          setWorkFlowLoaderVal(false);
          handleRefetch(
            error,
            'Could not create work flow. Please try again!',
            false
          );
        }, 1200);
      }
    });
  }
  function FILE(workflowData) {
    flowAPI850_File({
      variables: {
        transactionInput: workflowData,
      },
    }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
      console.log(data, error, graphQLErrors, networkError, cacheHit);

      if (data !== null && error == false) {
        setWorkFlowResponseMessage('File Connector created');

        setWorkFlowStartTime(61);
        setWorkFlowEndTime(75);

        //calling 6th API CALL
        SEND(workflowData);
      } else {
        setWorkFlowResponseMessage('Connector creation failed');
        setWorkFlowEndTime(75);
        setTimeout(() => {
          setWorkFlowMessage('failure');
          setWorkFlowLoaderVal(false);
          handleRefetch(
            error,
            ' Could not create work flow. Please try again!',
            false
          );
        }, 1200);
      }
    });
  }
  function SEND(workflowData) {
    flowAPI850_Send({
      variables: {
        transactionInput: workflowData,
      },
    }).then(async ({ data, error, graphQLErrors, networkError, cacheHit }) => {
      console.log(data, error, graphQLErrors, networkError, cacheHit);
      console.log(JSONloading, JSONerror, JSONdata);
      if (data !== null && error == false) {
        setWorkFlowStartTime(76);
        setWorkFlowEndTime(100);
        setWorkFlowResponseMessage('Map Json created');
        await setTimeout(async () => {
          setWorkFlowMessage('success');

          //console.log(data.flowAPI850_Send.msg);
          // setProgressLoader(false);
          await handleRefetch(
            error,
            'The Work Flow has been created successfully!',
            true
          );
        }, 1200);
      } else {
        setResponseMessage('Connector creation failed');
        setWorkFlowEndTime(100);
        setTimeout(() => {
          setMessage('failure');
          setWorkFlowLoaderVal(false);
          handleRefetch(
            error,
            'Could not create work flow. Please try again!',
            false
          );
        }, 1200);
      }
    });
  }
  const handleResendWorkflow = (e, rowdata) => {
    console.log(rowdata, rowdata.ResendWorkflow);
    var workflowData = {
      workSpaceName: props.rowData.workspaceName,
      senderIdentifier: props.rowData.senderQualifier,
      workFlowName: rowdata.workFlowName,
      transactionType: ToolTipFunction(rowdata.workFlowName),
    };
    console.log(workflowData);
    if (rowdata.ResendWorkflow == 1) {
      X12(workflowData);
    } else if (rowdata.ResendWorkflow == 2) {
      setWorkFlowLoaderVal(true);
      setWorkFlowStartTime(16);
      XMLMap(workflowData);
    } else if (rowdata.ResendWorkflow == 3) {
      setWorkFlowLoaderVal(true);
      setWorkFlowStartTime(31);
      JSON(workflowData);
    } else if (rowdata.ResendWorkflow == 4) {
      setWorkFlowLoaderVal(true);
      setWorkFlowStartTime(46);
      REST(workflowData);
    } else if (rowdata.ResendWorkflow == 5) {
      setWorkFlowLoaderVal(true);
      setWorkFlowStartTime(61);
      FILE(workflowData);
    } else if (rowdata.ResendWorkflow == 6) {
      setWorkFlowLoaderVal(true);
      setWorkFlowStartTime(76);
      SEND(workflowData);
    } else {
      console.log('Done');
    }
  };
  if (loading) return <Loader />;
  if (serverError)
    return <Error type={'Sever connection lost.Please try again'} />;
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <MaterialTable
        icons={tableIcons}
        title="WorkFlow"
        data={workflowData}
        columns={columns}
        editable={{
          onRowDelete: (oldData) => handleRemoveWorkFlow(oldData),
        }}
        actions={[
          (rowData) => {
            // console.log(rowData, rowData.workFlowName);

            return rowData.APIStatus == null || rowData.APIStatus == 'Inactive'
              ? {
                  icon: TrackChangesIcon,
                  tooltip: 'Test ' + ToolTipFunction(rowData.workFlowName),
                  disabled: true,
                  onClick: (event, rowData) =>
                    handleOpenExpansionModel(event, rowData),
                }
              : {
                  icon: TrackChangesIcon,
                  tooltip: 'Test ' + ToolTipFunction(rowData.workFlowName),
                  disabled: false,
                  onClick: (event, rowData) =>
                    handleOpenExpansionModel(event, rowData),
                };
          },
          (rowData) => {
            // console.log(rowData, rowData.workFlowName);

            return rowData.ResendWorkflow > 0
              ? {
                  icon: UpdateOutlinedIcon,
                  tooltip: 'Resend Work Flow',
                  // hidden: false,
                  disabled: false,
                  onClick: (event, rowData) =>
                    handleResendWorkflow(event, rowData),
                }
              : {
                  icon: UpdateOutlinedIcon,
                  tooltip: 'Resend Work Flow',
                  disabled: true,
                  //hidden: true,
                  //onClick: (event, rowData) =>
                  //handleResendWorkflow(event, rowData),
                };
          },

          {
            icon: EditIcon,
            tooltip: 'Edit',
            disabled: true,
          },
        ]}
        options={{
          headerStyle: {
            textAlign: 'center',
            fontSize: 12,
            fontWeight: 'bold',
            fontFamily: 'Arial !important',
            backgroundColor: '#0b153e',
            color: '#ffffff',
            paddingRight: '4px',
          },
          searchFieldStyle: {
            color: '#0b153e',
          },
          actionsColumnIndex: -1,
          // pageSize: 10,
          //pageSizeOptions: [10, 25, 50, 100],
          // toolbar: true,
          // paging: true,
        }}
      />
      <WorkFlowExapnsionForm
        handleCloseModal={handleCloseForm}
        isModalOpen={openWorkFlow}
      >
        <WorkFlowExapansionModal
          handleCloseModal={handleCloseForm}
          isModalOpen={openWorkFlow}
          rowData={WSpaceWFlow}
          workspaceName={props.rowData.workspaceName}
          onSuccess={handleRefetch}
          handleTestService={handleTestService}
        />
      </WorkFlowExapnsionForm>
      <LinearLoader isModalOpen={linearLoaderVal} />
      <ProgressLoader
        isModalOpen={progressLoaderVal}
        maxTimeLoader={handleCloseLoader}
        value={startTime}
        time={endTime}
        msg={msg}
        labelText={responseMsg}
        successProgressLoader={handleProgressLoader}
      />

      <WorkFlowProgressLoader
        isModalOpen={workFlowLoaderVal}
        maxTimeLoader={handleCloseLoader}
        value={workFlowStartTime}
        time={workFlowEndTime}
        msg={workFlowMsg}
        labelText={workFlowResponseMsg}
      />
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        autoHideDuration={1500}
        open={openSnackbar}
        onClose={handleCloseSnackbar}
      >
        <MySnackbarContentWrapper
          onClose={handleCloseSnackbar}
          variant={variant}
          message={message}
        />
      </Snackbar>
    </MuiThemeProvider>
  );
}
