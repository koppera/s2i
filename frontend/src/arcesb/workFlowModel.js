import React from 'react';
import { useMutation, useQuery, useManualQuery } from 'graphql-hooks';
import { forwardRef, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import { Grid, Typography } from '@material-ui/core';
import Theme from '../css/theme';
import '../css/commonStyle.css';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import Loader from '../components/loader';
import Error from '../components/emptyPage';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import AuthContext from '../authUtil/Auth';
import Loader1 from '../components/progressBar';
import CloseIcon from '@material-ui/icons/Close';
import Modal from '@material-ui/core/Modal';
import Snackbar from '@material-ui/core/Snackbar';
import MySnackbarContentWrapper from '../components/Snackbar';
const ALL_DocumentType = `query fetchDocumentTypeArcesb($searchInput:String){
  fetchDocumentTypeArcesb(searchInput:$searchInput) {
    idDocumentType
    documentTypeUUID
    name
    description
    docType
    APIStatus
    createdOn
    lastUpdatedOn
  }
}
`;
const Create_WorkFLow = `query flowAPI850($transactionInput:transactionInput){
  flowAPI850 (input:$transactionInput) {
    workFlowName
    workspaceUUID
    workFlowUUID
    createdOn
    updatedOn
  }
}`;

const fetchWorkFlowByWorkspace = `query fetchWorkFlowByWorkspace($workspaceUUID:String){
  fetchWorkFlowByWorkspace (workspaceUUID:$workspaceUUID) {
    workFlowName
    workspaceUUID
    workFlowUUID
    APIStatus
    ResendWorkflow
    createdOn
    updatedOn
  }
    
}
`;
export default function FetchUserOrganization(props) {
  console.log('props', props);
  const [searchInput, setSearchValue] = React.useState('');
  const [load, setLoad] = React.useState(false);
  const [startTime, setStartTime] = React.useState(1);
  const [endTime, setEndTime] = React.useState(100);
  const [msg, setMessage] = React.useState(null);
  //Fetch ALL DocumentType
  const { loading, errors, data, refetch } = useQuery(ALL_DocumentType, {
    variables: {
      searchInput: searchInput,
    },
  });
  /* const { workflowloading, workflowerror, workflowData } = useQuery(
    fetchWorkFlowByWorkspace,
    {
      variables: {
        workspaceUUID: props.rowData.workspaceUUID,
      },
    }
  );
  console.log(workflowloading, workflowerror, workflowData);*/
  const [
    fetchWorkFlow,
    { loading: flow850loading, error: flow850error, data: flow850data },
  ] = useManualQuery(fetchWorkFlowByWorkspace);
  const [createWorkFlow] = useManualQuery(Create_WorkFLow);
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [message, setSnackbarMessage] = React.useState();
  const [variant, setSnackbarVariant] = React.useState('error');
  const [value, setValue] = React.useState();
  const [ErrMsg, setErrMsg] = React.useState(false);
  const [serverError, setServerError] = React.useState(false);
  const [Documentdata, setData] = React.useState([]);
  const handleChange = (event) => {
    setValue(event.target.value);
  };
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSelect: {
          select: {
            minWidth: 180,
          },
        },
      },
    });
  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }
  useEffect(() => {
    console.log();
    if (data !== undefined) {
      console.log(data.fetchDocumentTypeArcesb);
      setData(data.fetchDocumentTypeArcesb);
      setServerError(false);
    } else {
      console.log('server error');
      setServerError(true);
    }
  }, [data]);
  function search(nameKey, myArray) {
    for (var i = 0; i < myArray.length; i++) {
      if (myArray[i].workFlowName.indexOf(nameKey) !== -1) {
        return myArray[i].workFlowName;
      }
    }
  }
  const handleSubmit = async (event) => {
    console.log(event.detail);
    if (!event.detail || event.detail == 1) {
      if (value) {
        setErrMsg(false);
        //Add Procced functionality
        console.log(value);
        //duplicatation resolving
        var transactionTypeval = value.replace(/ /g, '');
        console.log(transactionTypeval);
        await fetchWorkFlow({
          variables: {
            workspaceUUID: props.rowData.workspaceUUID,
          },
        }).then(
          async ({ data, error, graphQLErrors, networkError, cacheHit }) => {
            console.log(data, error, graphQLErrors, networkError, cacheHit);
            if (data !== null && error == false) {
              var workflowValues = data.fetchWorkFlowByWorkspace;
              var resultObject = search(transactionTypeval, workflowValues);
              console.log(resultObject);
              if (resultObject == null || resultObject == undefined) {
                let workFlowName =
                  props.rowData.workspaceName +
                  '_' +
                  transactionTypeval +
                  '_Inbound';
                console.log(workFlowName);
                await createWorkFlow({
                  variables: {
                    transactionInput: {
                      workSpaceName: props.rowData.workspaceName,
                      senderIdentifier: props.rowData.senderQualifier,
                      workFlowName: workFlowName,
                      transactionType: transactionTypeval,
                      workspaceUUID: props.rowData.workspaceUUID,
                    },
                  },
                }).then(
                  ({ data, error, graphQLErrors, networkError, cacheHit }) => {
                    console.log(
                      data,
                      error,
                      graphQLErrors,
                      networkError,
                      cacheHit
                    );
                    if (data !== null && error == false) {
                      //props.onSuccess(error, 'saved successfully', true);
                      //API X12
                      var workflowData = {
                        workSpaceName: props.rowData.workspaceName,
                        senderIdentifier: props.rowData.senderQualifier,
                        workFlowName: workFlowName,
                        transactionType: transactionTypeval,
                        workspaceUUID: props.rowData.workspaceUUID,
                      };
                      props.workFlowData(workflowData, 'success', true);
                    } else {
                      props.onSuccess(error, 'Graphql HOOKS Error', false);
                    }
                  }
                );
              } else {
                //document already taken
                console.log('document already taken');
                setOpenSnackbar(true);
                setSnackbarMessage('Work Flow already created');
                setSnackbarVariant('error');
              }
            }
          }
        );
      } else {
        setErrMsg(true);
      }
    }
  };

  const handleCloseModal = () => {
    props.handleCloseModal(false);
  };

  if (loading) return <Loader />;
  return (
    <div>
      <MuiThemeProvider theme={getMuiTheme()}>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={props.isModalOpen}
          onClose={handleCloseModal}
          className="fabmodal"
        >
          <div className="modalpaper">
            <Grid container className="header">
              <Grid item xs={10} sm={10}>
                <h2 id="modal-title" className="h2">
                  Create Work Flow
                </h2>
              </Grid>
              <Grid item xs={2} sm={2} className="close">
                <CloseIcon onClick={handleCloseModal} />
              </Grid>
            </Grid>
            <div>
              {ErrMsg === true ? (
                <div style={{ textAlign: 'center' }} className="addfieldserror">
                  Please select one among the list
                </div>
              ) : (
                ''
              )}
              <Typography
                style={{
                  color: '#06153e',
                  textAlign: 'center',
                  marginTop: '5px',
                }}
              >
                Please Select WorkFlow
              </Typography>
              <Grid container spacing={3} className="userformcontent">
                <Grid item xs={12} sm={12}>
                  <RadioGroup
                    row
                    aria-label="WorkFlow"
                    name="WorkFlow"
                    value={value}
                    onChange={handleChange}
                  >
                    {Documentdata !== null || Documentdata.length > 0
                      ? Documentdata.map((v, k) => {
                          return (
                            <Grid item xs={12} sm={3}>
                              <FormControlLabel
                                value={v.name}
                                control={<Radio />}
                                label={v.name}
                              />
                            </Grid>
                          );
                        })
                      : ''}
                  </RadioGroup>
                </Grid>
                <Grid container spacing={3} className="userformcontent">
                  <Grid
                    item
                    xs={12}
                    sm={4}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  ></Grid>
                  <Grid
                    item
                    xs={12}
                    sm={4}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
                    <Button
                      onClick={handleSubmit}
                      variant="contained"
                      fullWidth="true"
                      className="createpartnerbutton"
                      disabled={value === undefined ? true : false}
                    >
                      Create Work Flow
                    </Button>
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={4}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  ></Grid>
                </Grid>
              </Grid>
            </div>
          </div>
        </Modal>
        <Snackbar
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
          autoHideDuration={5000}
          open={openSnackbar}
          onClose={handleCloseSnackbar}
        >
          <MySnackbarContentWrapper
            onClose={handleCloseSnackbar}
            variant={variant}
            message={message}
          />
        </Snackbar>
      </MuiThemeProvider>
    </div>
  );
}
