import MaterialTable from 'material-table';
import { useMutation, useQuery, useManualQuery } from 'graphql-hooks';
import { forwardRef, useEffect } from 'react';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import ReactDOM from 'react-dom';
import MTableToolbar from 'material-table';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import CloseIcon from '@material-ui/icons/Close';
import { withStyles } from '@material-ui/core/styles';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import { Grid, Typography } from '@material-ui/core';
import MuiDialogActions from '@material-ui/core/DialogActions';
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import Timestamp from '../timestamp';
import Error from '../components/emptyPage';
import Loader from '../components/loader';
import Theme from '../css/theme';
import Snackbar from '@material-ui/core/Snackbar';
import MySnackbarContentWrapper from '../components/Snackbar';
//import ConnectorExpansion from './connectorExpansion';
import SendIcon from '@material-ui/icons/Send';
import TrackChangesIcon from '@material-ui/icons/TrackChanges';
import SwapVerticalCircleIcon from '@material-ui/icons/SwapVerticalCircle';

import MenuItem from '@material-ui/core/MenuItem';
import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles,
} from '@material-ui/core/styles';

const TRANSACTION_FLOW_8501 = `
query flowAPI850($flowData:transactionInput){
  flowAPI850(input:$flowData) {
    Status
  }
}
`;
const TRANSACTION_FLOW_850 = `
query flowAPI850{
  flowAPI850 {
    Status
  }
}
`;

const TRANSACTION_FLOW_825 = `
query flowAPI825{
  flowAPI825 {
    Status
  }
}
`;

const ADD_Workspace = `
mutation createWorkSpaceAPI($workspaceInput:workspaceInput){
  createWorkSpaceAPI(input:$workspaceInput) {
    workspaceUUID
  }
}
`;
const Post_WorkSpace_API = `query postWorkSpaceAPI($workspaceInput:workspaceInput){
  postWorkSpaceAPI(input:$workspaceInput) {
  
    workspaceUUID
    workspaceName
    createdOn
    updatedOn
  }

}

`;
const ALL_WorkSpace = `{
  fetchWorkSpace {
   
    workspaceUUID
    workspaceName
    senderQualifier
    transactionType
    createdOn
    updatedOn
  }
}
`;

const TRANSACTION_testService=`
query TestService{
  TestService {
    Status
  }
}
`;

function iconStyles() {
  return {};
}
const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(2),
    },
  },

  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));
const styles = (theme) => ({
  root: {
    Width: 1200,
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: '#FFFFFF',
  },
});
export default function MaterialTableDemo() {
  const [flow850, { loading:flow850loading, error:flow850error, data:flow850data }] = useManualQuery(TRANSACTION_FLOW_850);
  const [flow825, { loading:flow825loading, error:flow825error, data:flow825data }] = useManualQuery(TRANSACTION_FLOW_825);
  const [testService, { loading:testServiceloading, error:testServiceerror, data:testServicedata }] = useManualQuery(TRANSACTION_testService);
  /* const [fetchUser, { loading, error, data }] = useManualQuery(GET_USER_QUERY, {
    variables: { id: props.userId }
  })*/
  //ALL_WorkSpace
  const { loading, error, data, refetch } = useQuery(ALL_WorkSpace);
  //ADD Workspace
  const [addWorkSpace, addloading, adddata] = useMutation(ADD_Workspace);
  //fetch api
  const [
    PostAPIWorkspace,
    { loading: wloading, data: wdata, error: werrors },
  ] = useManualQuery(Post_WorkSpace_API);

  const columns = [
    { title: 'WorkSpace', field: 'workspaceName' },

    { title: 'Sender Indentifier', field: 'senderQualifier' },
    {
      title: 'transactionType',
      field: 'transactionType',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              //placeholder="Description"
              margin="normal"
              select
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            >
              <MenuItem value={'X12 4010 850'}> X12 4010 850</MenuItem>
              <MenuItem value={'X12 4010 825'}> X12 4010 825 </MenuItem>
            </TextField>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.transactionType}</div>;
      },
    },
  ];

  const [open, setOpen] = React.useState(false);
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [serverError, setServerError] = React.useState(false);
  const [message, setSnackbarMessage] = React.useState();
  const [variant, setSnackbarVariant] = React.useState('error');
  const [workspaceData, setData] = React.useState([]);

  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }
  const classes = makeStyles(iconStyles)();
  const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => (
      <Clear color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline color="error" {...props} ref={ref} />
    )),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => (
      <Edit {...props} ref={ref} onClick={console.log('hello world')} />
    )),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiDialogTitle: {
          root: {
            backgroundColor: '#0b153e',
            color: '#FFFFFF',
          },
        },
        MuiGrid: {
          container: {
            flexWrap: 'nowrap',
          },
        },
        MuiSelect: {
          icon: {
            color: '#0b153e',
          },
        },

        MuiTypography: {
          h6: {
            fontSize: 14,
            fontFamily: 'Arial !important',
          },
        },
      },
    });
  const classes1 = useStyles();
  useEffect(() => {
    if (data !== undefined) {
      console.log(data.fetchWorkSpace);
      setData(data.fetchWorkSpace);
      setServerError(false);
    } else {
      console.log('server error');
      setServerError(true);
    }
  }, [data]);
  const handleRefetch = (value, message) => {
    refetch({
      updateData(_, data) {
        return data;
      },
    });
    setOpenSnackbar(true);
    setSnackbarMessage(message);
    if (value) {
      setSnackbarVariant('error');
    } else {
      setSnackbarVariant('success');
    }
  };

  async function handleAddWorkSpace(newData) {
    return await new Promise(async (resolve) => {
      console.log(newData);
      resolve();
      await addWorkSpace({
        variables: {
          workspaceInput: {
            workspaceName: newData.workspaceName,
            senderQualifier:newData.senderQualifier,
            transactionType:newData.transactionType
          },
        },
      })
        .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
          console.log(data, error, graphQLErrors, networkError, cacheHit);
          if (data !== null && error == false) {
            handleRefetch(error, 'Saved successfully');
          } else if (error && graphQLErrors.length > 0) {
            let duperror = graphQLErrors[0].message;
            console.log(duperror);
            if (duperror !== null && duperror.indexOf('ER_DUP_ENTRY') !== -1) {
              handleRefetch(error, 'Name already existed');
            }
          }
        })
        .catch((e) => {
          // you can do something with the error here
          console.log(e);
        });
    });
  }
  const handleAPISuccess = async (Status, aPIMsg) => {
    if (Status === 'failure' || Status === null) {
      handleRefetch(true, aPIMsg);
    } else {
      handleRefetch(false, aPIMsg);
    }
  };
  const handleCallAPI = async (event, rowData) => {
    console.log('rowData', rowData);
    await PostAPIWorkspace({
      variables: {
        workspaceInput: {
          workspaceName: rowData.workspaceName,
        },
      },
    }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
      console.log(data, error, graphQLErrors, networkError, cacheHit);
      if (data !== null && error == false) {
        handleAPISuccess(
          data.Post_WorkSpace_API.statusAPI,
          data.Post_WorkSpace_API.msg
        );
      } else {
        handleRefetch(error, 'graphql hooks error');
      }
    });
  };

const handleCreateConnectorFlow=async(event, rowData)=>{
  console.log('rowData', rowData);
  await flow850({});
  if(rowData.transactionType==='850'){
    console.log('rowData', rowData.transactionType);
    await flow850({
      variables:{
        flow850:{
       
          workSpaceName:rowData.workspaceName,
          transactionType:rowData.transactionType,
          senderIdentifier:rowData.senderQualifier
        }
      }
    });
  }
  if(rowData.transactionType==='825'){
    console.log('rowData', rowData.transactionType);
    await flow825({});
  }

}

const handleTestService=async(event,rowdata)=>{
  console.log('rowData', rowdata);
  await testService({});
}
//testService

  if (loading) return <Loader />;
  if (serverError)
    return <Error type={'Sever connection lost.Please try again'} />;
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <MaterialTable
        icons={tableIcons}
        title="WorkSpace"
        columns={columns}
        data={workspaceData}
        editable={{
          onRowAdd: (newData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                resolve();
                handleAddWorkSpace(newData);
              }, 600);
            }),
          onRowUpdate: (newData, oldData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                resolve();
                if (oldData) {
                }
              }, 600);
            }),

          onRowDelete: (oldData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                resolve();
              }, 600);
            }),
        }}
        actions={[
          {
            icon: SendIcon,
            tooltip: 'Send API',
            onClick: (event, rowData) => handleCallAPI(event, rowData),
          },
          {
            icon: TrackChangesIcon,
            tooltip: 'Test Service',
            onClick: (event, rowData) => handleTestService(event, rowData),
          },
          {
            icon: SwapVerticalCircleIcon,
            tooltip: 'Create Connector Flow',
            onClick: (event, rowData) =>
              handleCreateConnectorFlow(event, rowData),
          },
        ]}
        /* detailPanel={[
          {
            render: (rowData) => {
              return <ConnectorExpansion rowData={rowData} />;
            },
          },
        ]} */
        options={{
          headerStyle: {
            textAlign: 'center',
            fontSize: 12,
            fontWeight: 'bold',
            fontFamily: 'Arial !important',
            backgroundColor: '#0b153e',
            color: '#ffffff',
            padding: '4px',
          },
          searchFieldStyle: {
            color: '#0b153e',
          },
          actionsColumnIndex: -1,
          pageSize: 10,
          pageSizeOptions: [10, 25, 50, 100],
          toolbar: true,
          paging: true,
        }}
        // onRowClick={(handleClickOpen)}
      />
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        autoHideDuration={1500}
        open={openSnackbar}
        onClose={handleCloseSnackbar}
      >
        <MySnackbarContentWrapper
          onClose={handleCloseSnackbar}
          variant={variant}
          message={message}
        />
      </Snackbar>
    </MuiThemeProvider>
  );
}
