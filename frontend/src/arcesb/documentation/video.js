import React from "react"
import ReactPlayer from "react-player"
import { Grid, Typography } from '@material-ui/core';


function App() {
  return (
    <div>
      <center>
      <Typography style={{fontSize:'32px',fontWeight:700,color:'#0b153e',marginTop:'4%'}}>
        ArcESB WorkFlow Video
      </Typography>

      <ReactPlayer  style={{marginTop:"7%"}} show controls
        url="https://cloudgen.sharepoint.com/sites/CGNextGenPlatformImplementers/Shared%20Documents/SmartEDI-ArcESB/ArcESB%20App%20Walk%20Through.mp4"
      />
      </center>
    </div>
  )
}

export default App