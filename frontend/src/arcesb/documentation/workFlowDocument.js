import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import LoginArcesb from '../images/LoginArcesb.png';
import Partner from '../images/Partnerscreen.png';
import Partnercreation from "../images/Addpartner.png";
import Syncpartner from "../images/Syncpartner.png";
import PartnerLoader from "../images/PartnerLoader.png";
import Partnerworkspace from "../images/Partnerworkspace.png";
import Workflowcreation from "../images/Workflowcreation.png";
import Workflowscreen from "../images/Workflowscreen.png";
import WorkFlowLoader1 from "../images/WorkFlowLoader1.png";
import WorkFlowLoader2 from "../images/WorkFlowLoader2.png";
import WorkFlowLoader3 from "../images/WorkFlowLoader3.png";
import Transcationmodal from "../images/Transcationmodal.png";
import Transcationcreation from "../images/Transcationcreation.png";
import Connectorsflowchart from '../images/Connectorsflowchart.png';
import Arcesbmonitoring from "../images/Arcesbmonitoring.png";
import Successtranscation from "../images/Successtranscation.png";
import X12Form from "../images/X12Form.png";
import CreationofX12 from "../images/CreationofX12Connector.png";
import X12View from "../images/X12View.png";
import SuccessofX12 from "../images/SuccessofX12.png";
import X12SyncLoader from "../images/X12SyncLoader.png";
import X12Sync from "../images/X12Sync.png";
import X1Transaction from "../images/X12Transaction.png";
import TestX12Loader1 from "../images/TestX12Loader1.png";
import TestX12Loader2 from "../images/TestX12Loader2.png";
import X1TransactionSuccess from "../images/X12TransactionSuccess.png";
import AS2Form from "../images/AS2Form.png";
import AS2Creation from "../images/AS2Creation.png";
import AS2CreationSuccess from "../images/AS2CreationSuccess.png";
import AS2View from "../images/AS2View.png";
import AS2SyncLoader from "../images/AS2SyncLoader.png";
import AS2Sync from "../images/AS2Sync.png";
import '../../css/commonStyle.css';



const useStyles = makeStyles((theme) => ({}));

export default function CircularIndeterminateLoader() {
  const classes = useStyles();

  return (
    <Grid container spacing={3} className="tutorialcontainer">
      <Grid item xs={12}>
        <Typography className="tutorialheading" >
          Tutorial on navigation to ArcESB Application
        </Typography>
      </Grid>

      <Grid item xs={12}>
        <Typography className="tutorialtext">1.User Login to Application</Typography>
      </Grid>
      <Grid item xs={12}>
        <img alt="noimage" style={{ width: '90%' }} src={LoginArcesb} />
      </Grid>
      <Grid item xs={12}>
        <Typography className="tutorialtext">2.Create workspace for partners</Typography>
      </Grid>
      <Grid item xs={12}>
        <img alt="noimage" style={{ width: '90%' }}src={Partner} />
      </Grid>
      <Grid item xs={12}>
        <Typography className="tutorialtext">Click add icon on top right à provide Partner & Sender Identifier </Typography>
      </Grid>
      <Grid item xs={12}>
        <img alt="noimage" style={{ width: '90%' }}src={Partnercreation}  />
      </Grid>
      <Grid item xs={12}>
        <Typography className="tutorialtext"> Click Sync Partner Icon under Actions</Typography>
      </Grid>
      <Grid item xs={12}>
        <img alt="noimage" style={{ width: '90%' }} src={Syncpartner} />
      </Grid>
      <Grid item xs={12}>
        <img alt="noimage" style={{ width: '90%' }} src={PartnerLoader} />
      </Grid>
      <Grid item xs={12}>
        <Typography className="tutorialtext">Now the partner workspace will be created in the ArcESB </Typography>
      </Grid>
      <Grid item xs={12}>
        <img alt="noimage" style={{ width: '90%' }} src={Partnerworkspace} />
      </Grid>
      <Grid item xs={12}>
        <Typography className="tutorialtext"> 3.Create workflow with connectors 

          <p style={{fontSize:"15px"}}> 3.1: Accept EDI file </p> 

           <p style={{fontSize:"15px"}}> 3.2: Convert X12 to XML </p> 

           <p style={{fontSize:"15px"}}> 3.3: Mapping X124010850 to Transaction Logs </p> 

           <p style={{fontSize:"15px"}}> 3.4: Convert to Json </p> 

           <p style={{fontSize:"15px"}}> 3.5: Store data into Elastic Search </p> 

 

Click Select Work Flow Icon under Actions </Typography>
      </Grid>
      <Grid item xs={12}>
        <img alt="noimage" style={{ width: '90%' }} src={Syncpartner} />
      </Grid>
      <Grid item xs={12}>
        <Typography className="tutorialtext">Select the Work Flow X124010850 </Typography>
      </Grid>
      <Grid item xs={12}>
        <img alt="noimage" style={{ width: '90%' }}  src={Workflowcreation}/>
      </Grid>
  
      <Grid item xs={12}>
        <img alt="noimage" style={{ width: '90%' }}  src={WorkFlowLoader3}/>
      </Grid>
      
      <Grid item xs={12}>
        <Typography className="tutorialtext">We can see the connectors created in the ArcESB workspace </Typography>
      </Grid>
      <Grid item xs={12}>
        <img alt="noimage" style={{ width: '90%' }} src={Connectorsflowchart} />
      </Grid>
      <Grid item xs={12}>
        <Typography className="tutorialtext">4.Test the WorkFlow  </Typography>
        <Typography className="tutorialtext"> Select the partner and click on Test Icon under Actions </Typography>
      </Grid>
      <Grid item xs={12}>
        <img alt="noimage" style={{ width: '90%' }}src={Workflowscreen} />
      </Grid>
      <Grid item xs={12}>
        <Typography className="tutorialtext">Provide Base64 encoded EDI Data àclick on PROCESS TRANSACTION </Typography>
      </Grid>
      <Grid item xs={12}>
        <img alt="noimage" style={{ width: '90%' }}  src={Transcationmodal}/>
      </Grid>
     
      <Grid item xs={12}>
        <img alt="noimage" style={{ width: '90%' }}  src={Transcationcreation}/>
      </Grid>
      <Grid item xs={12}>
        <img alt="noimage" style={{ width: '90%' }}  src={WorkFlowLoader1}/>
      </Grid>
      <Grid item xs={12}>
        <img alt="noimage" style={{ width: '90%' }}  src={WorkFlowLoader2}/>
      </Grid>
      <Grid item xs={12}>
        <Typography className="tutorialtext">Transaction successful </Typography>
      </Grid>
      <Grid item xs={12}>
        <img alt="noimage" style={{ width: '90%' }}  src={Successtranscation}/>
      </Grid>
      <Grid item xs={12}>
        <Typography className="tutorialtext">View transaction data in Kibana 

 

Click on Arcesb and select Monitoring to view Transactions </Typography>
      </Grid>
      <Grid item xs={12}>
        <img alt="noimage" style={{ width: '90%' }}  src={Arcesbmonitoring}/>
      </Grid>

      <Grid item xs={12}>
        <Typography className="tutorialtext">5.X12 Connector Form </Typography>
      </Grid>
      <Grid item xs={12}>
        <img alt="noimage" style={{ width: '90%' }}  src={X12Form}/>
      </Grid>
      <Grid item xs={12}>
        <Typography className="tutorialtext">Creation of X12 Connector </Typography>
      </Grid>
      <Grid item xs={12}>
        <img alt="noimage" style={{ width: '90%' }}  src={CreationofX12}/>
      </Grid>
      <Grid item xs={12}>
        <img alt="noimage" style={{ width: '90%' }}  src={SuccessofX12}/>
      </Grid>
    
      <Grid item xs={12}>
        <Typography className="tutorialtext">X12 Connector View </Typography>
      </Grid>
      <Grid item xs={12}>
        <img alt="noimage" style={{ width: '90%' }}  src={X12View}/>
      </Grid>
      <Grid item xs={12}>
        <Typography className="tutorialtext">Sync of X12 Connector </Typography>
      </Grid>
      <Grid item xs={12}>
        <img alt="noimage" style={{ width: '90%' }}  src={X12SyncLoader}/>
      </Grid>
      <Grid item xs={12}>
        <Typography className="tutorialtext">Successful Sync of X12 Connector </Typography>
      </Grid>
      <Grid item xs={12}>
        <img alt="noimage" style={{ width: '90%' }}  src={X12Sync}/>
      </Grid>
      <Grid item xs={12}>
        <img alt="noimage" style={{ width: '90%' }}  src={X1Transaction}/>
      </Grid>
      <Grid item xs={12}>
        <img alt="noimage" style={{ width: '90%' }}  src={TestX12Loader1}/>
      </Grid>
      <Grid item xs={12}>
        <img alt="noimage" style={{ width: '90%' }}  src={TestX12Loader2}/>
      </Grid>
      <Grid item xs={12}>
        <img alt="noimage" style={{ width: '90%' }}  src={X1TransactionSuccess}/>
      </Grid>
      <Grid item xs={12}>
        <Typography className="tutorialtext">6.AS2 Profie Form </Typography>
      </Grid>
      <Grid item xs={12}>
        <img alt="noimage" style={{ width: '90%' }}  src={AS2Form}/>
      </Grid>
      <Grid item xs={12}>
        <Typography className="tutorialtext">Creation of AS2 Profile Form </Typography>
      </Grid>
      <Grid item xs={12}>
        <img alt="noimage" style={{ width: '90%' }}  src={AS2Creation}/>
      </Grid>
      <Grid item xs={12}>
        <img alt="noimage" style={{ width: '90%' }}  src={AS2CreationSuccess}/>
      </Grid>
      <Grid item xs={12}>
        <Typography className="tutorialtext">AS2 Profile View </Typography>
      </Grid>
      <Grid item xs={12}>
        <img alt="noimage" style={{ width: '90%' }}  src={AS2View}/>
      </Grid>
      <Grid item xs={12}>
        <Typography className="tutorialtext">Sync of AS2 Profile </Typography>
      </Grid>
      <Grid item xs={12}>
        <img alt="noimage" style={{ width: '90%' }}  src={AS2SyncLoader}/>
      </Grid>
      <Grid item xs={12}>
        <Typography className="tutorialtext">Successful Sync of AS2 Profile </Typography>
      </Grid>
      <Grid item xs={12}>
        <img alt="noimage" style={{ width: '90%' }}  src={AS2Sync}/>
      </Grid>
    </Grid>
  );
}
