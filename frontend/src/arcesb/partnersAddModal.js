import React, { useEffect, useContext, useCallback } from 'react';
import { useMutation, useQuery, useManualQuery } from 'graphql-hooks';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import Logout from '@material-ui/icons/ExitToApp';
import Theme from '../css/theme';
import {Typography, Paper } from '@material-ui/core';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import Modal from '@material-ui/core/Modal';
const ADD_Workspace = `
mutation createWorkSpaceAPI($workspaceInput:workspaceInput){
  createWorkSpaceAPI(input:$workspaceInput) {
    workspaceUUID
  }
}
`;
const UPDATE_Workspace = `
mutation updateWorkSpace($workspaceInput:workspaceInput){
  updateWorkSpace(input:$workspaceInput) {
    workspaceUUID
  }
}
`;
const ALL_WorkSpace = `{
  fetchWorkSpace {
    workspaceUUID
    workspaceName
    senderQualifier
    APIStatus
    createdOn
    updatedOn
  }
}
`;
export default function AddPartner(props) {
  const [values, setValues] = React.useState({
    workspaceName:'',
    senderQualifier: '',
  });
  const [nameError, setNameError] = React.useState(false);
  const [errorInfo,setErrorInfo] = React.useState(
    {
      workspaceName:false,
      senderQualifier:false
    }
  )
  const [ErrMsg, setErrMsg] = React.useState(false);
  const handlereset = () => {
    setErrMsg(false);
    setNameError(false);
    setErrorInfo({
      workspaceName:false,
      senderQualifier:false
    });
    setValues({
      ...values,
      workspaceName:'',
     senderQualifier: '',
    });
  };
  const { loading, error, data, refetch } = useQuery(ALL_WorkSpace);
  //ADD Workspace
  const [addWorkSpace, addloading, adddata] = useMutation(ADD_Workspace);
  //Update Workspace
  const [UpdateWorkSpace, updateloading, updatedata] = useMutation(
    UPDATE_Workspace
  );
  useEffect(() => {
    console.log(props.rowData);
    if (props.rowData != undefined && props.title != 'Add') {
      setValues({
        workspaceName:props.rowData.workspaceName,
        senderQualifier:props.rowData.senderQualifier,
      });
    }
  }, [props]);
  const handleChange = (name) => (event) => {
    setValues({ ...values, [name]: event.target.value });
    setErrMsg(false);
    switch (name) {
        case 'workspaceName':
        if (event.target.value.length === 0) {
          setErrorInfo({ ...error, [name]: false });
        } else if (
          event.target.value.match(/^[A-Z]{1}[A-Za-z]*$/)
        ) {
         // console.log(values);
         setErrorInfo({ ...error, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...error, [name]: true });
        }
        break;
      case 'senderQualifier':
        if (event.target.value.length === 0) {
          setErrorInfo({ ...error, [name]: false });
        } else if (
          event.target.value.match(
            /^[A-Z\s]+$/
          )
        ) {
         // console.log(values);
         setErrorInfo({ ...error, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...error, [name]: true });
        }

        break;
        default:return true
    }
  };

  const handleSubmit = () => {
    setErrMsg(false);
    setErrorInfo(false);
    if (
        values.workspaceName!=='' &&
        values.senderQualifier!=='' &&
        errorInfo.workspaceName !== true &&
        errorInfo.senderQualifier!== true 
       
      ) {
        setErrMsg(false);
        setValues({
          ...values,
          workspaceName:values.workspaceName,
          senderQualifier:values.senderQualifier
        });
        if(props.title=='Add')
        {
          return new Promise(async (resolve) => {
            console.log(values);
            resolve();
            await addWorkSpace({
              variables: {
                workspaceInput: {
                  workspaceName: values.workspaceName,
                  senderQualifier: values.senderQualifier,
                  //transactionType:newData.transactionType
                },
              },
            }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
                console.log(data, error, graphQLErrors, networkError, cacheHit);
                if (data !== null && error == false) {
                  props.onSuccess(error, 'Partner created successfully',true);
                } else if (error && graphQLErrors.length > 0) {
                  let duperror = graphQLErrors[0].message;
                  console.log(duperror);
                  if (duperror !== null && duperror.indexOf('ER_DUP_ENTRY') !== -1) {
                    props.onSuccess(error, 'Name already existed',false);
                  }
                }
              })
              .catch((e) => {
                // you can do something with the error here
                console.log(e);
              });
          });
        }
        else
        {
          return new Promise(async (resolve) => {
            resolve();
            console.log(values);
            await UpdateWorkSpace({
              variables: {
                workspaceInput: {
                  workspaceName: values.workspaceName,
                  senderQualifier: values.senderQualifier,
                  workspaceUUID: props.rowData.workspaceUUID,
                },
              },
            })
              .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
                console.log(data, error, graphQLErrors, networkError, cacheHit);
                if (data !== null && error == false) {
                  props.onSuccess(error, 'Updated successfully',true);
                } else if (error && graphQLErrors.length > 0) {
                  let duperror = graphQLErrors[0].message;
                  console.log(duperror);
                  if (duperror !== null && duperror.indexOf('ER_DUP_ENTRY') !== -1) {
                    props.onSuccess(error, 'Partner already existed',false);
                  }
                }
              })
              .catch((e) => {
                // you can do something with the error here
                console.log(e);
              });
          });
        }


     }else if(
         values.workspaceName=='' ||
          values.senderQualifier==''
      ){
        setErrMsg(true);
      }
       else {
        setErrMsg(false);
        setNameError(true);
      }
 
  };
  const handleCloseModal = () => {
    props.handleCloseModal(false);
  };
 
  return (
    <MuiThemeProvider theme={Theme}>
         <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={props.isModalOpen}
          onClose={handleCloseModal}
          className="fabmodal"
        >
          <div className="modalpaper">
            <Grid container className="header">
              <Grid item xs={10} sm={10}>
                <h2 id="modal-title" className="h2">
                {props.title === 'Add'?'Add Partner':'Update Partner'}
                </h2>
              </Grid>
              <Grid item xs={2} sm={2} className="close">
                <CloseIcon onClick={handleCloseModal} />
              </Grid>
            </Grid>
      <div>
      <Grid container spacing={3} className="content">   

      <Grid  items xs={12} sm={12} >
            {ErrMsg === true ? (
            <div style={{ textAlign: 'center' }} className="addfieldserror">
                Please fill the fields
            </div>
            ) : (
            ''
                )}

          <Grid container spacing={3} className="content">   
                <Grid
                  item
                  xs={12} 
                  sm={6}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                    <TextField
                        id="standard-basic"
                        className="partnertextField"
                        label="Partner"
                        onChange={handleChange('workspaceName')}
                        value={values.workspaceName}
                        name="workspaceName"
                        variant="outlined"
                        margin="dense"
                    />
                    {errorInfo.workspaceName === true ? (
                        <div id="nameid" className="addfieldserror">
                            Starts with Uppercase letter followed by lowercase and uppercase letters
                        </div>
                    ) : (
                        ''
                    )}
                        {nameError&&!values.workspaceName? (
                            <div id="nameid" className="addfieldserror">
                              Please enter Partner
                            </div>
                          )
                           : ""}
             </Grid>
                <Grid
                  item
                  xs={12}
                  sm={6}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                    <TextField
                        id="standard-basic"
                        className="partnertextField"
                        label="Sender Identifier"
                        onChange={handleChange('senderQualifier')}
                        value={values.senderQualifier}
                        name="senderQualifier"
                        variant="outlined"
                        margin="dense"
                    />
                    {errorInfo.senderQualifier === true ? (
                        <div id="nameid" className="addfieldserror">
                        Enter only Uppercase alphabets     
                       </div>
                    ) : (
                        ''
                    )}
                      {nameError&&!values.senderQualifier? (
                            <div id="nameid" className="addfieldserror">
                              Please enter Sender Identifier
                            </div>
                          )
                           : ""}
                </Grid>
             </Grid>
            <Grid container spacing={3}>
                <Grid
                item
                xs={6}
                sm={6}
                gutterBottom
                variant="body2"
                color="textSecondary"
                className="text"
              >
                  <Button
                    style={{
                        color: '#000006',
                        backgroundColor:'#E5CCFF' 
                     }}
                    variant="contained"
                    //size="Large"
                    className="createpartnerbutton"
                    onClick={handlereset}
              >
                  RESET
                </Button>
                
            </Grid>
            <Grid
                item
                xs={6}
                sm={6}
                gutterBottom
                variant="body2"
                color="textSecondary"
                className="text"
              >
                <Button
                    variant="contained"
                    className="createpartnerbutton"
                    onClick={handleSubmit}
                  >
                      {props.title === 'Add'?'SAVE':'Update'}
                  </Button>
                </Grid>
                </Grid>
            </Grid>
             </Grid>
      </div>
      </div>
      </Modal>
    </MuiThemeProvider>
  );
}

