import React, { useEffect, useContext, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MySnackbarContentWrapper from '../../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import '../../css/commonStyle.css';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import Theme from '../../css/theme';
import Grid from '@material-ui/core/Grid';
import Checkbox from '@material-ui/core/Checkbox';
import FormGroup from '@material-ui/core/FormGroup';
import {Typography, Paper } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import Modal from '@material-ui/core/Modal';
import {
  createMuiTheme,
  MuiThemeProvider,
} from '@material-ui/core/styles';
const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing(2)
  }
}));
const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSelect:{
          select:{
            minWidth:180
          }
          }
      }
    });
 export default function AutomationSettings(props) {
      const classes = useStyles();
      const [selectedValue, setSelectedValue] = React.useState('a');
      const [values, setValues] = React.useState({ 
          retryinterval: '',
          retrymaximum: '', 
          resendinterval:'',
          resendmaximum:'',
          send: true,
         });
      const [ErrMsgDropdown, setErrMsgDropdown] = React.useState(false);
      const [ErrMsgNameInfo, setErrMsgNameInfo] = React.useState(false);
      const [ErrMsg, setErrMsg] = React.useState(false);
      const [errorInfo, setErrorInfo] = React.useState({
        retryinterval: false,
        retrymaximum:false,
        resendinterval: false,
        resendmaximum: false
      });
      const [nameError, setNameError] = React.useState(false);
      const handleChange = name => event => {
        setErrMsg(false);
        setErrMsgDropdown(false);
        setValues({ ...values, [name]: event.target.value });
          switch (name) {
            case 'retryinterval':
              if (event.target.value.length ===0) {
                setValues({ ...values, [name]: '' });
                setErrorInfo({ ...errorInfo, [name]: false });
              } else {
                if (
                  event.target.value.match('^[0-9]*$') &&
                  event.target.value.length <= 25
                ) {
                  setErrorInfo({ ...errorInfo, [name]: false });
                  setValues({ ...values, [name]: event.target.value });
                } else {
                  setErrorInfo({ ...errorInfo, [name]: true });
                }
              }
              break;
                case 'retrymaximum':
                  if (event.target.value.length ===0) {
                    setValues({ ...values, [name]: '' });
                    setErrorInfo({ ...errorInfo, [name]: false });
                  } else {
                    if (
                      event.target.value.match('^[0-9]*$') &&
                      event.target.value.length <= 25
                    ) {
                      setErrorInfo({ ...errorInfo, [name]: false });
                      setValues({ ...values, [name]: event.target.value });
                    } else {
                      setErrorInfo({ ...errorInfo, [name]: true });
                    }
                  }
                  break;
                    case 'resendinterval':
                      if (event.target.value.length ===0) {
                        setValues({ ...values, [name]: '' });
                        setErrorInfo({ ...errorInfo, [name]: false });
                      } else {
                        if (
                          event.target.value.match('^[0-9]*$') &&
                          event.target.value.length <= 25
                        ) {
                          setErrorInfo({ ...errorInfo, [name]: false });
                          setValues({ ...values, [name]: event.target.value });
                        } else {
                          setErrorInfo({ ...errorInfo, [name]: true });
                        }
                      }
                      break;
                        case 'resendmaximum':
                          if (event.target.value.length ===0) {
                            setValues({ ...values, [name]: '' });
                            setErrorInfo({ ...errorInfo, [name]: false });
                          } else {
                            if (
                              event.target.value.match('^[0-9]*$') &&
                              event.target.value.length <= 25
                            ) {
                              setErrorInfo({ ...errorInfo, [name]: false });
                              setValues({ ...values, [name]: event.target.value });
                            } else {
                              setErrorInfo({ ...errorInfo, [name]: true });
                            }
                          }
                          break;
                         case 'send':
                       setValues({ ...values, [event.target.name]: event.target.checked });
                      break;
                     default:
                    return false;
                     } 
                    };
        const handleSubmit = async () => {
          console.log(values);
         if(values.retryinterval!== ''&&
         values.retrymaximum!== ''&& 
         values.resendinterval!==''&&
         values.resendmaximum!==''&&
         errorInfo.retryinterval!==true&&
         errorInfo.retrymaximum!==true&
         errorInfo.resendinterval!==true&&
         errorInfo.resendmaximum!==true
         )
         {  setErrMsg(false);
            setNameError(false);
            setValues({
             ...values,
             retryinterval:values.retryinterval,
             retrymaximum:values.retrymaximum, 
             resendinterval:values.resendinterval,
             resendmaximum:values.resendmaximum,
             send:values.send,
            });}
            else if(values.retryinterval== ''&&
            values.retrymaximum== ''&&
            values.resendinterval=='' &&
            values.resendmaximum=='')
       {
       setErrMsg(true);
       }
      else
      {
     setNameError(true);  
       }   
       }
        const handleReset = async () => {
              setErrMsg(false);
              setErrorInfo({
                  retryinterval: false,
                  retrymaximum:false,
                  resendinterval: false,
                  resendmaximum: false,
              });
              setValues({
                ...values,
                retryinterval: '',
                retrymaximum: '', 
                resendinterval:'',
                resendmaximum:'',
                send: true,
                });
              setNameError(false);
            };
      return (
    <MuiThemeProvider theme={getMuiTheme()}>
     <div>
      {ErrMsg === true ? (
        <div style={{ textAlign: 'center' }} className="addfieldserror">
          Please fill the fields
        </div>
      ) : (
        ''
      )}
      <form className="content" noValidate autoComplete="off">
      <Typography>
       <div style={{ color: '#0b153e', fontsize: '14px',
    fontfamily: 'Arial !important',
    fontweight: 500,
    }}>Automation Settings</div>
      </Typography>
       <Grid container spacing={3} className="userformcontent" >
            <Grid 
                  item
                  xs={12} 
                  sm={4}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                <TextField
                id="outlined-dense"
                label="Retry Interval (minutes) "
                className="textField"
                margin="dense"
                variant="outlined"
                onChange={handleChange('retryinterval')}
                value={values.retryinterval}
                name="retryinterval"
                />
                {errorInfo.retryinterval === true ? (
                <div id="nameid"  className="addfieldserror">
                    "Allowed only numbers"
                </div>
                ) : (
                ''
                )}
                {nameError && !values.retryinterval ? (
               <div id="nameid" className="addfieldserror">
                Please enter Retry interval (minutes)
                </div>
                 ) : (
                 ''
                )}
               </Grid>
               <Grid
                  item
                  xs={12} 
                  sm={4}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                <TextField
                id="outlined-dense"
                label="Retry Maximum Attempts"
                className="textField"
                margin="dense"
                variant="outlined"
                onChange={handleChange('retrymaximum')}
                value={values.retrymaximum}
                name="retrymaximum"
                />
                {errorInfo.retrymaximum === true ? (
                <div className="addfieldserror">
                    "Allowed only numbers."
                </div>
                ) : (
                ''
                )}
                 {nameError && !values.retrymaximum ? (
               <div id="nameid" className="addfieldserror">
                Please enter Retry Maximum Attempts
                </div>
                 ) : (
                 ''
                )}
               </Grid>
                <Grid
                  item
                  xs={12} 
                  sm={4}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                <TextField
                id="outlined-dense"
                label="Resend Interval (minutes) "
                className="textField"
                margin="dense"
                variant="outlined"
                onChange={handleChange('resendinterval')}
                value={values.resendinterval}
                name="resendinterval"
                />
                {errorInfo.resendinterval === true ? (
                <div className="addfieldserror">
                    "Allowed only numbers."
                </div>
                ) : (
                ''
                )}
                 {nameError && !values.resendinterval ? (
               <div id="nameid" className="addfieldserror">
                Please enter Resend Interval (minutes)
                </div>
                 ) : (
                 ''
                )}
                </Grid>
                <Grid
                  item
                  xs={12} 
                  sm={4}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                <TextField
                id="outlined-dense"
                label="Resend Maximum Attempts"
                className="textField"
                margin="dense"
                variant="outlined"
                onChange={handleChange('resendmaximum')}
                value={values.resendmaximum}
                name="resendmaximum"
                />
                {errorInfo.resendmaximum === true ? (
                <div className="addfieldserror">
                    "Allowed only numbers."
                </div>
                ) : (
                ''
                )}
                 {nameError && !values.resendmaximum ? (
               <div id="nameid" className="addfieldserror">
                Please enter Resend Maximum Attempts
                </div>
                 ) : (
                 ''
                )}
               </Grid>
              <Grid item xs={12} sm={6}>
              <Typography>
               <Checkbox
                     inputProps={{
                  'aria-label': 'uncontrolled-checkbox',
                   }}
                   value={values.send}
                   checked={values.send}
                  name='send'
                  onChange={handleChange('send')}
                  color="primary"
                  />
                  send 
                </Typography>
              </Grid>
            <Grid container spacing={3} >
                      <Grid
                        item
                        xs={12}
                        sm={6}
                        gutterBottom
                        variant="body2"
                        color="textSecondary"
                        className="text"
                      >
                        <Button
                          style={{
                            color: '#000006',
                            backgroundColor: '#E5CCFF',
                          }}
                          onClick={handleReset}
                          variant="contained"
                          fullWidth="true"
                          className="createpartnerbutton"
                        >
                          RESET
                        </Button>
                       </Grid>
                        <Grid item xs={12} sm={6}>
                        <Button
                        onClick={handleSubmit}
                        variant="contained"
                        fullWidth="true"
                        className="createpartnerbutton"
                        >
                       Create
                      </Button>
                     </Grid>
                     </Grid>
                    </Grid>
                    </form>
                    </div>
                   </MuiThemeProvider>
                   );
                  }