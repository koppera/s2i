import MaterialTable from 'material-table';
import { forwardRef, useEffect } from 'react';
import { useMutation, useQuery, useManualQuery } from 'graphql-hooks';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import Snackbar from '@material-ui/core/Snackbar';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import ReactDOM from 'react-dom';
import MTableToolbar from 'material-table';
import React, { Component } from 'react';
import Timestamp from '../../timestamp';
import AddIcon from '@material-ui/icons/Add';
import Button from '@material-ui/core/Button';
import Error from '../../components/emptyPage';
import Loader from '../../components/loader';
import Theme from '../../css/theme';
import X12ViewModel from '../../components/tmsmodal';
import X12View from '../tradinginfo/x12ConnectorView';
import SendIcon from '@material-ui/icons/Send';
import X12FormModel from '../../components/tmsmodal';
import X12Form from './x12ConnectorForm';
import { TextField } from '@material-ui/core';
import SelectAllIcon from '@material-ui/icons/SelectAll';
import WorkFlowForm from '../../components/tmsmodal';
import WorkFlowModel from './x12workFlowModel';
import ProgressLoader from '../../components/x12Loader';
import X12TestServiceModal from '../../components/tmsmodal';
import X12TestServiceModalForm from './x12TestService';
import TrackChangesIcon from '@material-ui/icons/TrackChanges';
import X12TestServiceLoader from '../../components/x12TestServiceLoader';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import UploadCertificateModel from '../../components/tmsmodal';
import UploadCertificateForm from './x12Upload';

import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles,
} from '@material-ui/core/styles';
import MySnackbarContentWrapper from '../../components/Snackbar';

function iconStyles() {
  return {};
}
const TRANSACTION_testService = `
query TestService($transactionInput:transactionInput){
  TestService(input:$transactionInput) {
    statusAPI
  msg
  }
}
`;
const TRANSACTION_Fileupload_testService = `
query fileuploadtestService($transactionFileInput:transactionFileInput){
  fileuploadtestService(input:$transactionFileInput) {
    statusAPI
  msg
  }
}
`;
const DISPLAY_Workspace = `{
  allWorkspaceByStatus {
    workspaceUUID
    workspaceName
    senderQualifier
    APIStatus
    createdOn
    updatedOn
  }
}`;

const ALL_X12CONNECTOR = `{
    allX12Connector {
      workspaceUUID
      idX12Connector
      X12ConnectorUUID
      connectorId
      connectorDescription
      transactionType
      profilesenderIdentifier
      senderIdQualifier
      receiverIdQualifier
      profilereceiverIdentifier
      controlVersionNumber
      usageIndicator
      functionalGroup
      ta1Acknowledgement
      batchTransactions
      functionalACK
      functionalACKType
      send
      resendInterval
      resendIntervalMaximumAttachments
      dataElementSeparator
      componentElementSeparator
      segmentTerminator
      suffix
      authorizationQualifier
      authorizationId
      securityQualifier
      securityPassword
      controlStandardsIdentifier
      senderIdentifier
      receiverIdentifier
      dateFormat
      timeFormat
      responsibleAgencyCode
      identifierCode
      encoding
      generateDescriptionAs
      hippaSchemas
      hippa278
      nestLoops
      parentConnector
      processingDelay
      snipValidation
      strictSchemaValidation
      localFileScheme
      logMessages
      sendFilter
      useReferenceId
      expandQualifierValues
      saveToSentFolder
      othersettingsFuntionalACKS
      APIStatus
      workspaceName
      createdOn
      updatedOn
    }
  }

`;

const Remove_X12CONNECTOR = `
mutation removeX12Connector($X12ConnectorInput:X12ConnectorInput){
  removeX12Connector(input:$X12ConnectorInput) {
    idX12Connector
    X12ConnectorUUID
 
  }
}`;

const Post_X12Connector_API = `query postX12ConnectorAPI($X12ConnectorInput:X12ConnectorInput){
  postX12ConnectorAPI(input:$X12ConnectorInput) {
    statusAPI
    msg
  } 
 }

`;

export default function X12Table(props) {
  console.log(props);
  const {
    loading: wloading,
    data: wdata,
    errors: werrors,
    networkError: wnetworkError,
  } = useQuery(DISPLAY_Workspace);
  const [
    testService,
    {
      loading: testServiceloading,
      error: testServiceerror,
      data: testServicedata,
    },
  ] = useManualQuery(TRANSACTION_testService);
  const [
    fileUploadTestService,
    {
      loading: testFileServiceloading,
      error: testFileServiceerror,
      data: testFileServicedata,
    },
  ] = useManualQuery(TRANSACTION_Fileupload_testService);

  //fetch api
  const [
    PostAPIX12Connector,
    { loading: wploading, data: wpdata, error: wperrors },
  ] = useManualQuery(Post_X12Connector_API);
  const [openX12TestService, setOpenX12TestService] = React.useState(false);
  const [openX12Modal, setOpenX12Modal] = React.useState(false);

  const [workspaceData, setWorksapceData] = React.useState([]);
  const [x12ConnectorForm, setX12ConnectorForm] = React.useState('update');
  const [rowData, setRowData] = React.useState();
  const [linearLoaderVal, setLinearLoader] = React.useState(false);
  const [responseMsg, setResponseMessage] = React.useState();
  const [startTime, setStartTime] = React.useState();
  const [msg, setMessage] = React.useState(null);
  const [endTime, setEndTime] = React.useState();
  const [load, setLoad] = React.useState(false);
  const [progressLoaderVal, setProgressLoader] = React.useState(false);
  const [testResponseMsg, setTestResponseMessage] = React.useState();
  const [testStartTime, setTestStartTime] = React.useState();
  const [testMsg, setTestMessage] = React.useState(null);
  const [testEndTime, setTestEndTime] = React.useState();
  const [openUploadModal, setOpenUploadModal] = React.useState(false);

  const handleCloseUpload = () => {
    setOpenUploadModal(false);
  };

  const handleOpenUpload = (e, rowData) => {
    console.log('upppppp');
    setOpenUploadModal(true);
    setWSpaceWFlow(rowData);
  };

  const columns = [
    {
      title: 'Work Space',
      field: 'workspaceUUID',
      lookup: workspaceData,
      editable: 'never',
    },
    {
      title: 'Connector Id',
      field: 'connectorId',
    },
    {
      title: 'Transaction Type',
      field: 'transactionType',
    },
    {
      title: 'Sender Id Qualifier',
      field: 'senderIdQualifier',
    },
    {
      title: 'Sender Identifier',
      field: 'profilesenderIdentifier',
    },
    {
      title: 'Receiver Id Qualifier',
      field: 'receiverIdQualifier',
    },
    {
      title: 'Receiver Identifier',
      field: 'profilereceiverIdentifier',
    },

    {
      title: 'API Status',
      render: (rowData) => {
        if (rowData === undefined) {
          return '';
        } else if (
          rowData.APIStatus == null ||
          rowData.APIStatus === 'Inactive'
        ) {
          return <div style={{ color: 'red' }}>Not Sync </div>;
        } else {
          return <div style={{ color: 'green' }}>Sync </div>;
        }
      },
    },
  ];

  //ALL_X12CONNECTOR
  const { loading, error, data, refetch } = useQuery(ALL_X12CONNECTOR);
  const [openWorkFlow, setOpenWorkFlow] = React.useState(false);
  const [x12ConnectorData, setX12ConnectorData] = React.useState([]);
  const [serverError, setServerError] = React.useState(false);
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [variant, setSnackbarVariant] = React.useState('error');
  const [message, setSnackbarMessage] = React.useState();

  const [WSpaceWFlow, setWSpaceWFlow] = React.useState([]);
  const [timeloader, setTimeLoader] = React.useState(100);

  //REMOVE Workspace
  const [removeX12Connector, removeloading, removedata] = useMutation(
    Remove_X12CONNECTOR
  );

  const classes = makeStyles(iconStyles)();
  const tableIcons = {
    Add: forwardRef((props, ref) => (
      <AddBox color="primary" {...props} ref={ref} />
    )),
    Check: forwardRef((props, ref) => (
      <Check color="primary" {...props} ref={ref} />
    )),
    Clear: forwardRef((props, ref) => (
      <Clear color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline color="error" {...props} ref={ref} />
    )),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => (
      <Edit color="primary" {...props} ref={ref} />
    )),
    Export: forwardRef((props, ref) => (
      <SaveAlt color="primary" {...props} ref={ref} />
    )),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => (
      <FirstPage color="primary" {...props} ref={ref} />
    )),
    LastPage: forwardRef((props, ref) => (
      <LastPage color="primary" {...props} ref={ref} />
    )),
    NextPage: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft color="primary" {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => (
      <Clear color="primary" {...props} ref={ref} />
    )),
    Search: forwardRef((props, ref) => (
      <Search color="primary" {...props} ref={ref} />
    )),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };

  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSelect: {
          icon: {
            color: '#0b153e',
          },
        },

        MuiTypography: {
          h6: {
            fontSize: 14,
            fontFamily: 'Arial !important',
          },
        },
      },
    });

  const [openViewModal, setOpenViewModal] = React.useState(false);

  const handleCloseView = (e) => {
    setOpenViewModal(false);
  };
  const handleOpenView = (e, rowData) => {
    setOpenViewModal(true);
    setRowData(rowData);
  };

  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }
  const handleClickUpdateOpen = (e, rowData) => {
    console.log(e, rowData);
    setX12ConnectorForm('Update');
    setRowData(rowData);
    setOpenX12Modal(true);
  };
  const handleClickAddOpen = (e) => {
    setX12ConnectorForm('Add');
    setOpenX12Modal(true);
  };
  const handleCloseForm = (e) => {
    setOpenX12Modal(false);
    setOpenX12TestService(false);
    setOpenUploadModal(false);
  };

  useEffect(() => {

    if (data !== undefined) {
      setX12ConnectorData(data.allX12Connector);
      setServerError(false);
    } else {
      console.log('server error');
      setServerError(true);
    }
    if (wdata !== undefined) {
      var worksapceDD = wdata.allWorkspaceByStatus.reduce(
        (obj, item) => ((obj[item.workspaceUUID] = [item.workspaceName]), obj),
        {}
      );
      setWorksapceData(worksapceDD);
    }
  }, [data, responseMsg, wdata]);

  const handleRefetch = (value, message, modelClose) => {
    console.log(value);
    console.log(message);
    setStartTime();
    setEndTime();
    setTestStartTime();
    setTestEndTime();
    refetch({
      updateData(_, data) {
        return data;
      },
    });
    setProgressLoader(false);
    setLinearLoader(false);
    setOpenSnackbar(true);
    setSnackbarMessage(message);
    if (value) {
      setSnackbarVariant('error');
    } else {
      setSnackbarVariant('success');
    }
    if (modelClose) {
      handleCloseForm();
    }
  };

  const handleAPISuccess = async (Status, aPIMsg) => {
    if (Status === 'failure' || Status === null) {
      handleRefetch(true, aPIMsg);
    } else {
      handleRefetch(false, aPIMsg);
    }
  };

  const handleCallAPI = async (event, rowData) => {
    //  console.log('rowData', rowData);
    console.log('rowData', rowData);
    setProgressLoader(true);
    setStartTime(1);

    await PostAPIX12Connector({
      variables: {
        X12ConnectorInput: {
          workspaceUUID: rowData.workspaceUUID,
          connectorId: rowData.connectorId,
          X12ConnectorUUID: rowData.X12ConnectorUUID,
          receiverIdQualifier: rowData.receiverIdQualifier,
          senderIdQualifier: rowData.senderIdQualifier,
          receiverIdentifier: rowData.profilereceiverIdentifier,
          senderIdentifier: rowData.profilesenderIdentifier,
          transactionType: rowData.transactionType,
          securityQualifier: rowData.securityQualifier,
          resendInterval: rowData.resendInterval,
          authorizationQualifier: rowData.authorizationQualifier,
          responsibleAgencyCode: rowData.responsibleAgencyCode,
          send: rowData.send,
          resendIntervalMaximumAttachments:
            rowData.resendIntervalMaximumAttachments,
          usageIndicator: rowData.usageIndicator,
          dateFormat: rowData.dateFormat,
          suffix: rowData.suffix,
          controlStandardsIdentifier: rowData.controlStandardsIdentifier,
          strictSchemaValidation: rowData.strictSchemaValidation,
          encoding: rowData.encoding,
          dataElementSeparator: rowData.dataElementSeparator,
          segmentTerminator: rowData.segmentTerminator,
          saveToSentFolder: rowData.saveToSentFolder,
          ta1Acknowledgement: rowData.ta1Acknowledgement,
          timeFormat: rowData.timeFormat,
          functionalACK: rowData.functionalACK,
          componentElementSeparator: rowData.componentElementSeparator,
          logMessages: rowData.logMessages,
          controlVersionNumber: rowData.controlVersionNumber,
          functionalACKType: rowData.functionalACKType,
        },
      },
    }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
      console.log(data, error, graphQLErrors, networkError, cacheHit);

      if (data !== null && error == false) {
        setEndTime(100);
        setTimeout(() => {
          handleAPISuccess(
            data.postX12ConnectorAPI.statusAPI,
            data.postX12ConnectorAPI.msg
          );
        }, 6000);
      } else if (error && graphQLErrors.length > 0) {
        let duperror = graphQLErrors[0].message;
        console.log(duperror);
        if (duperror !== null) {
          setEndTime(90);
          setTimeout(() => {
            handleRefetch(
              error,
              'x12 connector profile did not sync. Please try again!'
            );
          }, 5000);
        }
      } else {
        setEndTime(90);
        setTimeout(() => {
          handleRefetch(error, 'graphql hooks error');
        }, 5000);
      }
    });
  };

  async function handleRemoveX12Connector(oldData) {
    console.log('delete X12Connector');
    return await new Promise(async (resolve) => {
      resolve();

      await removeX12Connector({
        variables: {
          X12ConnectorInput: {
            X12ConnectorUUID: oldData.X12ConnectorUUID,
          },
        },
      })
        .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
          console.log(data, error, graphQLErrors, networkError, cacheHit);
          if (data !== null && error == false) {
            handleRefetch(error, 'Deleted successfully');
          } else if (error && graphQLErrors.length > 0) {
            let duperror = graphQLErrors[0].message;
            console.log(duperror);
            if (
              duperror !== null &&
              duperror.indexOf('ER_ROW_IS_REFERENCED') !== -1
            ) {
              setOpenSnackbar(true);
              setSnackbarVariant('error');
              setSnackbarMessage(
                'Selected row is referenced in workflow table'
              );
            }
          }
        })
        .catch((e) => {
          // you can do something with the error here
          console.log(e);
        });
    });
  }
  const handleClickOpenModel = (e, rowdata) => {
    setOpenWorkFlow(true);
    setWSpaceWFlow(rowdata);
  };

  const handleWorkFlow = async (workflowData, message, modelval) => {
    if (modelval) {
      handleCloseForm();
    }
  };
  function ToolTipFunction(name) {
    var str = name;
    var ret = name.replace(/_Outbound|_Inbound/g, '');

    str = ret.split('_').pop();
    return str;
  }

  const handleOpenExpansionModel = (e, rowData) => {
    setOpenX12TestService(true);
    setWSpaceWFlow(rowData);
  };

  const handleCloseTestService = () => {
    setOpenX12TestService(false);
  };
  const handleTestService = (transactionData) => {
    handleCloseForm();
    setLinearLoader(true);
    //setProgressLoader(true);
    setTimeout(() => {
      setTestStartTime(1);
      // setEndTime(2);
    }, 500);
    testService({
      variables: {
        transactionInput: transactionData,
      },
    }).then(async ({ data, error, graphQLErrors, networkError, cacheHit }) => {
      console.log(data, error, graphQLErrors, networkError, cacheHit);
      if (data !== null && error == false) {
        console.log('292 success test service');
        console.log(data.TestService.statusAPI);
        if (
          data.TestService.statusAPI === 'failure' ||
          data.TestService.statusAPI === null
        ) {
          //setLinearLoader(false);
          await handleRefetch(true, data.TestService.msg, false);
        } else {
          setTimeout(() => {
            //  setStartTime(25);
            setTestEndTime(50);
          }, 500);

          setTimeout(() => {
            setStartTime(51);
            setTestEndTime(100);
          }, 500);
          setTimeout(() => {
            handleRefetch(false, data.TestService.msg, false);
          }, 8000);
        }
      } else if (error && graphQLErrors != null && graphQLErrors.length > 0) {
        let duperror = graphQLErrors[0].message;
        console.log(duperror);
        if (duperror !== null) {
          setLinearLoader(false);
          handleRefetch(
            error,
            'Could not process transaction.Please try again!',
            false
          );
        }
      } else {
        setLinearLoader(false);
        handleRefetch(
          error,
          'Could not process transaction.Please try again!',
          false
        );
      }
    });
  };
  //file upload test service
  const handleFileTestService = (transactionData) => {
    handleCloseForm();
    setLinearLoader(true);
    //setProgressLoader(true);
    setTimeout(() => {
      setTestStartTime(1);
      // setEndTime(2);
    }, 500);
    fileUploadTestService({
      variables: {
        transactionFileInput: transactionData,
      },
    }).then(async ({ data, error, graphQLErrors, networkError, cacheHit }) => {
      console.log(data, error, graphQLErrors, networkError, cacheHit);
      if (data !== null && error == false) {
        console.log('292 success test service');
        console.log(data.fileuploadtestService.statusAPI);
        if (
          data.fileuploadtestService.statusAPI === 'failure' ||
          data.fileuploadtestService.statusAPI === null
        ) {
          //setLinearLoader(false);
          await handleRefetch(true, data.fileuploadtestService.msg, false);
        } else {
          setTimeout(() => {
            //  setStartTime(25);
            setTestEndTime(50);
          }, 500);

          setTimeout(() => {
            setStartTime(51);
            setTestEndTime(100);
          }, 500);
          setTimeout(() => {
            handleRefetch(false, data.fileuploadtestService.msg, false);
          }, 8000);
        }
      } else if (error && graphQLErrors != null && graphQLErrors.length > 0) {
        let duperror = graphQLErrors[0].message;
        console.log(duperror);
        if (duperror !== null) {
          setLinearLoader(false);
          handleRefetch(
            error,
            'Could not process transaction.Please try again!',
            false
          );
        }
      } else {
        setLinearLoader(false);
        handleRefetch(
          error,
          'Could not process transaction.Please try again!',
          false
        );
      }
    });
  };
  const handleCloseLoader = () => {
    setLoad(false);
    setLinearLoader(false);
  };
  const handleProgressLoader = (val) => {
    console.log(val);
    if (val == 100) {
      setProgressLoader(false);
      setLinearLoader(false);
      setStartTime(0);
      handleRefetch(false, 'Transaction Processed Successfully');
    }
  };
  if (loading && progressLoaderVal == false) return <Loader />;
  if (serverError)
    return <Error type={'Sever connection lost.Please try again'} />;
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <MaterialTable
        icons={tableIcons}
        title="X12 Connector"
        columns={columns}
        data={x12ConnectorData}
        editable={{
          onRowDelete: (oldData) => handleRemoveX12Connector(oldData),
        }}
        options={{
          headerStyle: {
            textAlign: 'center',
            fontSize: 12,
            fontWeight: 'bold',
            fontFamily: 'Arial !important',
            backgroundColor: '#0b153e',
            color: '#ffffff',
            padding: '4px',
          },
          searchFieldStyle: {
            color: '#0b153e',
          },
          actionsColumnIndex: -1,
          pageSize: 10,
          pageSizeOptions: [10, 25, 50, 100],
          toolbar: true,
          paging: true,
        }}
        actions={[
          (rowData) => {
            return rowData.APIStatus == null || rowData.APIStatus == 'Inactive'
              ? {
                  icon: SendIcon,
                  tooltip: 'Sync X12Connector',
                  disabled: false,
                  onClick: (event, rowData) => handleCallAPI(event, rowData),
                }
              : {
                  icon: SendIcon,
                  tooltip: 'Sync X12Connector',
                  disabled: true,
                  //hidden: true
                };
          },
          (rowData) => {
            // console.log(rowData, rowData.workFlowName);

            return rowData.APIStatus == null || rowData.APIStatus == 'Inactive'
              ? {
                  icon: TrackChangesIcon,
                  tooltip: 'Test ' + ToolTipFunction(rowData.connectorId),
                  disabled: true,
                  onClick: (event, rowData) =>
                    handleOpenExpansionModel(event, rowData),
                }
              : {
                  icon: TrackChangesIcon,
                  tooltip: 'Test ' + ToolTipFunction(rowData.connectorId),
                  disabled: false,
                  onClick: (event, rowData) =>
                    handleOpenExpansionModel(event, rowData),
                };
          },

          (rowData) => {
            return rowData.APIStatus == null || rowData.APIStatus == 'Inactive'
              ? {
                  icon: CloudUploadIcon,
                  tooltip: 'Test ' + ToolTipFunction(rowData.connectorId),
                  disabled: true,
                  onClick: (event, rowData) => handleOpenUpload(event, rowData),
                }
              : {
                  icon: CloudUploadIcon,
                  tooltip: 'Test ' + ToolTipFunction(rowData.connectorId),
                  disabled: false,
                  //hidden: true
                  onClick: (event, rowData) => handleOpenUpload(event, rowData),
                };
          },

          {
            icon: AddIcon,
            tooltip: 'Add',
            className: 'addIconColor',
            isFreeAction: true,
            onClick: (event) => handleClickAddOpen(event),
          },
          {
            icon: Edit,
            tooltip: 'Edit',
            className: 'addIconColor',
            onClick: (event, rowData) => handleClickUpdateOpen(event, rowData),
          },

          /*  {
            icon: SelectAllIcon,
            tooltip: 'Select Work Flow',
            disabled: false,
            onClick: (event, rowData) => handleClickOpenModel(event, rowData),
            //hidden: true
          },*/
        ]}
        onRowClick={(event, rowData) => handleOpenView(event, rowData)}
      />
      <X12FormModel
        handleCloseModal={handleCloseForm}
        isModalOpen={openX12Modal}
      >
        <X12Form
          handleCloseModal={handleCloseForm}
          onSuccess={handleRefetch}
          isModalOpen={openX12Modal}
          rowData={rowData}
          title={x12ConnectorForm}
        />
      </X12FormModel>
      <X12ViewModel
        handleCloseModal={handleCloseView}
        isModalOpen={openViewModal}
      >
        <X12View
          handleCloseModal={handleCloseView}
          //onSuccess={handleRefetch}
          isModalOpen={openViewModal}
          rowData={rowData}
          title={x12ConnectorForm}
        />
      </X12ViewModel>
      <WorkFlowForm
        handleCloseModal={handleCloseForm}
        isModalOpen={openWorkFlow}
      >
        <WorkFlowModel
          handleCloseModal={handleCloseForm}
          isModalOpen={openWorkFlow}
          rowData={WSpaceWFlow}
          onSuccess={handleRefetch}
          workFlowData={handleWorkFlow}
        />
      </WorkFlowForm>

      <UploadCertificateModel
        handleCloseModal={handleCloseUpload}
        isModalOpen={openUploadModal}
      >
        <UploadCertificateForm
          handleCloseModal={handleCloseUpload}
          // onSuccess={handleRefetch}
          rowData={WSpaceWFlow}
          isModalOpen={openUploadModal}
          handleTestService={handleFileTestService}
        />
      </UploadCertificateModel>

      <ProgressLoader
        isModalOpen={progressLoaderVal}
        // maxTimeLoader={handleCloseLoader}
        value={startTime}
        time={endTime}
        msg={msg}
        labelText={responseMsg}
        // successProgressLoader={handleProgressLoader}
      />
      <X12TestServiceModal
        handleCloseModal={handleCloseTestService}
        isModalOpen={openX12TestService}
      >
        <X12TestServiceModalForm
          handleCloseModal={handleCloseTestService}
          isModalOpen={openX12TestService}
          rowData={WSpaceWFlow}
          // workspaceName={rowData.workspaceName}
          // onSuccess={handleRefetch}
          handleTestService={handleTestService}
        />
      </X12TestServiceModal>
      <X12TestServiceLoader
        isModalOpen={linearLoaderVal}
        maxTimeLoader={handleCloseLoader}
        value={testStartTime}
        time={testEndTime}
        msg={testMsg}
        labelText={testResponseMsg}
        successProgressLoader={handleProgressLoader}
      />
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        autoHideDuration={1500}
        open={openSnackbar}
        onClose={handleCloseSnackbar}
      >
        <MySnackbarContentWrapper
          onClose={handleCloseSnackbar}
          variant={variant}
          message={message}
        />
      </Snackbar>
    </MuiThemeProvider>
  );
}
