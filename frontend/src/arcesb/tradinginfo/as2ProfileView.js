import React, { useEffect, useContext, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { Grid, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { withStyles } from '@material-ui/core/styles';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import Modal from '@material-ui/core/Modal';
import '../../css/commonStyle.css';
import Theme from '../../css/theme';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
const DISPLAY_All_AS2profile = `{
  allAS2Profile {
  idas2Profile
  connectorID
  connectorDescription
  as2Identifier
  partnerURL
  sendMessageSecurity
  receiveMessageSecurity
  requestMDNReceipt
  signed
  unSigned
  delivery
  asynchronous
  streaming
  as2Restart
  enableAs2Reliability
  inputFolder
  outputFolder
  processedFolder
  localAs2Identifier
  profilePrivateCertificate
  profilecertificatePassword
  userProfileCertificate
  TLSprivateCertificate
  TLScertificatePassword
  HTTPAuthenticationType
  user
  password
  characterset
  headersName
  headersValue
  maxWorkers
  maxFiles
  asyncMDNTimeout
  duplicateFileAction
  duplicateFileInterval
  encryptionAlgorithm
  extensionMap
  hTTPSubject
  logLevel
  logRequests
  messageId
  parentConnector
  parseFDAExtensions
  partnerSigningCertificate
  processingDelay
  signatureAlgorithm
  TLSenabledProtocols
  tempReceiveDirectory
  logMessages
  sendFilter
  savetoSentFolder
  updatedOn
}}

`;
export default function As2View(props) {
  console.log(props.rowData);
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSvgIcon: {
          root: {
            color: '#FFFFFF',
          },
        },
        MuiGrid: {
          container: {
            paddingLeft: '1.5%',
            paddingBottom: '1%',
            overflowX: 'hidden',
          },
          item: {
            fontSize: '10px',
          },
        },
        MuiDialogTitle: {
          root: {
            backgroundColor: '#0b153e',
            color: '#FFFFFF',
          },
        },
      },
    });
  const handleModalClose = () => {
    props.handleCloseModal(false);
  };

  return (
    <div>
      <MuiThemeProvider theme={getMuiTheme()}>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={props.isModalOpen}
          onClose={handleModalClose}
          className="fabmodal"
        >
          <div className="modalpaper">
            <Grid container className="header">
              <Grid item xs={10} sm={10}>
                <h2 id="modal-title" className="h2">
                  AS2 Profile View
                </h2>
              </Grid>
              <Grid item xs={2} sm={2} className="close">
                <CloseIcon onClick={handleModalClose} />
              </Grid>
            </Grid>
            <Grid container className="pdashboardroot">
              <Grid container spacing={3}>
                {/* <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    AS2 Identifier
                  </Typography>
                  {props.rowData.as2Identifier}
              </Grid> 
               <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Private Certificate
                  </Typography>
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Certificate Password
                  </Typography>
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Rollover Private Certificate
                  </Typography>
                </Grid>{' '}
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Rollover Certificate Password
                  </Typography>
                </Grid>{' '}
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Public Domains
                  </Typography>
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Asynchronous MDN URL
                  </Typography>
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Receiving URL
                  </Typography>
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Public URL
                  </Typography>
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Public Certificate
                  </Typography>
            </Grid>*/}
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Connector Id
                  </Typography>
                  {props.rowData.connectorID}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Connector Description
                  </Typography>
                  {props.rowData.connectorDescription}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Partner URL
                  </Typography>
                  {props.rowData.partnerURL}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Send Message Security
                  </Typography>
                  {props.rowData.sendMessageSecurity}
                </Grid>
                {/*   <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                <Typography className="typographysubheader">
                    Encrypt Send Data
                  </Typography>
                  {props.rowData.sendMessageSecurity}
               </Grid>*/}
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Receive Message Security
                  </Typography>
                  {props.rowData.receiveMessageSecurity}
                </Grid>
                {/*   <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Require Sign
                  </Typography>
                  {props.rowData.receiveMessageSecurity}
             </Grid>*/}
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Request MDN Receipt
                  </Typography>
                  {props.rowData.requestMDNReceipt}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Security
                  </Typography>
                  {props.rowData.security}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Delivery
                  </Typography>
                  {props.rowData.delivery}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Encryption Certificate
                  </Typography>
                  {props.rowData.encryptionCertificate}
                </Grid>
                {/*     <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Input Folder
                  </Typography>
                  {props.rowData.inputFolder}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Output Folder
                  </Typography>
                  {props.rowData.outputFolder}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Processed Folder
                  </Typography>
                  {props.rowData.processedFolder}
           </Grid>*/}
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    AS2 Restart
                  </Typography>
                  {props.rowData.as2Restart}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Enable AS2 Relability
                  </Typography>
                  {props.rowData.as2Reliability}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Streaming
                  </Typography>
                  {props.rowData.streaming}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    UserProfile
                  </Typography>
                  {props.rowData.userProfileSettings}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    TLS Private Certificate
                  </Typography>
                  {props.rowData.TLSprivateCertificate}
                </Grid>
                {/* <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Local AS2 Identifier
                  </Typography>
                </Grid>*/}
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Certificate Password
                  </Typography>
                  {props.rowData.TLScertificatePassword}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    HTTP Authentication Type
                  </Typography>
                  {props.rowData.HTTPAuthenticationType}
                </Grid>
                {/*     <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">User</Typography>
                  {props.rowData.user}
           </Grid>*/}
                {/*   <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Password
                  </Typography>
                  {props.rowData.password}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Character Set
                  </Typography>
                </Grid>
                 <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                <Typography className="typographysubheader">
                    Headers Name
                  </Typography>
                  {props.rowData.headersName}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Headers Value
                  </Typography>
                  {props.rowData.headersValue}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Max Workers
                  </Typography>
                  {props.rowData.maxWorkers}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Max Files
                  </Typography>
                  {props.rowData.maxFiles}
          </Grid>*/}
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Async MDN Timeout
                  </Typography>
                  {props.rowData.asyncMDNTimeout}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Duplicate File Action
                  </Typography>
                  {props.rowData.duplicateFileAction}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Duplicate File Interval
                  </Typography>
                  {props.rowData.duplicateFileInterval}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Encryption Algorithm
                  </Typography>
                  {props.rowData.encryptionAlgorithm}
                </Grid>
                {/*    <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Extension Map
                  </Typography>
                  {props.rowData.extensionMap}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    HTTP Subject
                  </Typography>
                  {props.rowData.hTTPSubject}
            </Grid>*/}
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Log Level
                  </Typography>
                  {props.rowData.logLevel}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Log Requests
                  </Typography>
                  {props.rowData.logRequests}
                </Grid>
                {/*   <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Message Id
                  </Typography>
                  {props.rowData.messageId}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Parent Connector
                  </Typography>
                  {props.rowData.parentConnector}
             </Grid>*/}
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Parse FDA Extensions
                  </Typography>
                  {props.rowData.parseFDAExtensions}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Partner Signing Certificate
                  </Typography>
                  {props.rowData.partnerSigningCertificate}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Processing Delay
                  </Typography>
                  {props.rowData.processingDelay}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Signature Algorithm
                  </Typography>
                  {props.rowData.signatureAlgorithm}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Enabled Protocols
                  </Typography>
                  {props.rowData.TLSenabledProtocols}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Temp Receive Directory
                  </Typography>
                  {props.rowData.tempReceiveDirectory}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Log Messages
                  </Typography>
                  {props.rowData.logMessages}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Send Filter
                  </Typography>
                  {props.rowData.sendFilter}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Save To Sent Folder
                  </Typography>
                  {props.rowData.savetoSentFolder}
                </Grid>

                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    created On
                  </Typography>
                  {props.rowData.createdOn}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    updated On
                  </Typography>
                  {props.rowData.updatedOn}
                </Grid>
              </Grid>
            </Grid>
          </div>
        </Modal>
      </MuiThemeProvider>
    </div>
  );
}
