import React, { useEffect, useContext, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Snackbar from '@material-ui/core/Snackbar';
import '../../css/commonStyle.css';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import Theme from '../../css/theme';
import Grid from '@material-ui/core/Grid';
import Checkbox from '@material-ui/core/Checkbox';
import FormGroup from '@material-ui/core/FormGroup';
import { Typography, Paper } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import Modal from '@material-ui/core/Modal';

import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
const getMuiTheme = () =>
  createMuiTheme(Theme, {
    overrides: {
      MuiSelect: {
        select: {
          minWidth: 180,
        },
      },
    },
  });

export default function UploadFile(props) {
  const [values, setValues] = React.useState({ certificate: [] });
  const [ErrMsg, setErrMsg] = React.useState(false);
  const [errorInfo, setErrorInfo] = React.useState({ certificate: false });
  const handleSubmit = async () => {
    console.log(values, values.certificate.length);
    if (values.certificate != null) {
      setErrMsg(false);

      setValues({
        ...values,
        certificate: values.certificate,
      });
      const uploadFile = async (file) => {
        var data = new FormData();
        data.append('myFile', file);
        data.append('user', 'arcescb');
        data.append('path', 'public/uploads/' + file[0].name);
        console.log('path', 'public/uploads/' + file[0].name);
        let fileUpload = {
          file: file,
          path: 'public/uploads/' + file[0].name,
        };
        fetch('http://localhost:5004/testUpload', {
          method: 'POST',
          body: data,
        })
          .then((res) => console.log(res))
          .then((json) => console.log(json))
          .catch((err) => console.error(err));
        console.log(data);
      };
      uploadFile(values.certificate);
      //props.handleCloseModal(false);
    } else {
      setErrMsg(true);
    }
  };
  const handelChange = (event) => {
    console.log(event.target.files);
    if (event.target.value == '') {
      setErrMsg(true);
    } else {
      setErrMsg(false);
      setValues({
        ...values,
        certificate: event.target.files,
      });
    }
  };
  const handleReset = async () => {
    setErrMsg(false);
    setValues({
      ...values,
      certificate: '',
    });
  };
  const handleCloseModal = () => {
    props.handleCloseModal(false);
  };
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={props.isModalOpen}
        onClose={handleCloseModal}
        className="fabmodal"
      >
        <div className="modalpaper">
          <Grid container className="header">
            <Grid item xs={10} sm={10}>
              <h2 id="modal-title" className="h2">
                Upload Certificate
              </h2>
            </Grid>
            <Grid item xs={2} sm={2} className="close">
              <CloseIcon onClick={handleCloseModal} />
            </Grid>
          </Grid>
          <div>
            {ErrMsg === true ? (
              <div style={{ textAlign: 'center' }} className="addfieldserror">
                Please fill the field
              </div>
            ) : (
              ''
            )}
            <form className="content" noValidate autoComplete="off">
              <Grid container spacing={3} className="userformcontent">
                <Grid
                  item
                  xs={12}
                  sm={12}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography
                    style={{
                      color: '#686457',
                    }}
                  >
                    To upload a certificate to the server, click Browse to
                    select a certificate file and then click upload certificate.
                  </Typography>
                  <Typography
                    style={{
                      color: '#686457',
                    }}
                  >
                    supported certificate file formats are .cer .crt .der .p7b
                    .pem and .pub
                  </Typography>
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={6}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography
                    style={{
                      fontWeight: 'bold',
                    }}
                  >
                    Certificate:
                  </Typography>
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={6}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <input
                    accept="image/*"
                    // className={classes.input}
                    id="outlined-button-file"
                    multiple
                    // value={values.certificate}
                    onChange={handelChange}
                    type="file"
                  />
                </Grid>
                <Grid container spacing={3}>
                  <Grid
                    item
                    xs={12}
                    sm={6}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
                    <Button
                      style={{
                        color: '#000006',
                        backgroundColor: '#E5CCFF',
                      }}
                      onClick={handleReset}
                      variant="contained"
                      fullWidth="true"
                      className="createpartnerbutton"
                    >
                      RESET
                    </Button>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <Button
                      color="textSecondary"
                      onClick={handleSubmit}
                      variant="contained"
                      fullWidth="true"
                      className="createpartnerbutton"
                    >
                      Upload certificate
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
            </form>
          </div>
        </div>
      </Modal>
    </MuiThemeProvider>
  );
}
