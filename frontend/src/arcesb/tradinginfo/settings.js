import React, { useEffect, useContext, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MySnackbarContentWrapper from '../../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import '../../css/commonStyle.css';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import Theme from '../../css/theme';
import Modal from '@material-ui/core/Modal';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import Checkbox from '@material-ui/core/Checkbox';
import FormGroup from '@material-ui/core/FormGroup';
import UploadCertificateForm from './uploadCertificateModel';
import UploadCertificateModel from '../../components/tmsmodal';
import {Typography, Paper } from '@material-ui/core';
import {
  createMuiTheme,
  MuiThemeProvider,
} from '@material-ui/core/styles';
const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiTypography:{ 
          body1:{
            fontSize: '10px !important',
            alignItems: 'center !important',
            fontFamily: 'Arial !important',
            paddingTop: '1px !important',
            paddingBottom: '1px !important',
        }
      },
      MuiInputLabel:{
        outlined:{
       fontSize:10
        }
        }
      }
    });

export default function AddProjectForm(props) {
  const [openModal, setOpenModal] = React.useState(false);
  const [values, setValues] = React.useState({
    connectorId:'',
    connectorDescription: '',
    as2Identifier:'',
    partnerURL:'',
    signSendData: true,
    encryptSendData:true,
    requireEncryption:true,
    requireSign:true,
    requestMDNReceipt:true, 
    Security: 'signed',
    Delivery:'delivery',
    encryptionCertificate:false});
  const [ErrMsgNameInfo, setErrMsgNameInfo] = React.useState(false);
  const [ErrMsg, setErrMsg] = React.useState(false);
  const [submitErr, setSubmitErr] = React.useState(false);
  const [errorInfo, setErrorInfo] = React.useState({
    connectorDescription: false,
    as2Identifier:false,
    partnerURL:false,
   
  });
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [variant, setSnackbarVariant] = React.useState('error');
  const [message, setSnackbarMessage] = React.useState();

  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }

  const handleChange = name => event => {
    setErrMsg(false);
    
    setValues({ ...values, [name]: event.target.value });
    switch (name) {
      case 'connectorId': if (event.target.value.length === 0) {
        setValues({ ...values, [name]: '' });
        setErrorInfo({ ...errorInfo, [name]: false });
      } else if (event.target.value.match('^(A-Z0-9)*$')) {
        setErrorInfo({ ...errorInfo, [name]: false });
        setValues({ ...values, [name]: event.target.value });
      } else {
        setErrorInfo({ ...errorInfo, [name]: true });
      }
      break;
      case 'connectorDescription':
        if (event.target.value.length === 0) {
            setValues({ ...values, [name]: '' });
            setErrorInfo({ ...errorInfo, [name]: false });
          } else if (event.target.value.match('^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$')) {
            setErrorInfo({ ...errorInfo, [name]: false });
            setValues({ ...values, [name]: event.target.value });
          } else {
            setErrorInfo({ ...errorInfo, [name]: true });
          }
          break;

   case 'as2Identifier':
        if (event.target.value.length === 0) {
            setValues({ ...values, [name]: '' });
            setErrorInfo({ ...errorInfo, [name]: false });
          } else if (event.target.value.match('^[a-zA-Z0-9]*$')) {
            setErrorInfo({ ...errorInfo, [name]: false });
            setValues({ ...values, [name]: event.target.value });
          } else {
            setErrorInfo({ ...errorInfo, [name]: true });
          }
          break;
 
  case 'partnerURL':
            if (event.target.value.length === 0) {
                setValues({ ...values, [name]: '' });
                setErrorInfo({ ...errorInfo, [name]: false });
              } else if (event.target.value.match('^(http(s)?://)?([a-z][a-zA-Z0-9./]*)?$')) {
                setErrorInfo({ ...errorInfo, [name]: false });
                setValues({ ...values, [name]: event.target.value });
              } else {
                setErrorInfo({ ...errorInfo, [name]: true });
              }
              break;

     
     case 'signSendData':
     case 'encryptSendData':  
     case 'requireSign':
     case 'requireEncryption':
     case 'requestMDNReceipt':
        setValues({ ...values, [event.target.name]: event.target.checked });
         break;

         case 'Security':
            setValues({
              ...values,
              [event.target.name]: event.target.value,
            });
            break;
            case 'Delivery':
                setValues({
                  ...values,
                  [event.target.name]: event.target.value,
                });
                break;
    case 'encryptionCertificate':
                  setValues({ ...values, [name]: event.target.value });
                 console.log(values);
                  if(values)
                  {
                    setOpenModal(true);
                   }
                   break;
      default:
        return false;
    }
  };

  const handleCloseForm =() =>
  {
      setOpenModal(false);
      setValues({...values,encryptionCertificate:false})
  }
  const handleSubmit = async () => {
    console.log(values);
    setErrMsg(false);
    
    if (values.connectorDescription !== '' && values.as2Identifier!== '' && values.partnerURL!== '' &&
    errorInfo.connectorDescription !== true &&
    errorInfo.as2Identifier !== true &&
    errorInfo.partnerURL !== true ) {
      setErrMsg(false);
      setValues({
        ...values,
        connectorDescription: values.connectorDescription,
        as2Identifier: values.as2Identifier,
        partnerURL: values.partnerURL,
    });
          } else if ( 
          values.connectorDescription == '' &&
          values.as2Identifier == '' &&
          values.partnerURL == '' 
          ) {
              setErrMsg(true);
            } else {
              setErrMsg(false);
              setSubmitErr(true);
            }
          };


        const handleReset = async () => {
          setErrMsg(false);
          setErrorInfo({
            connectorDescription: false,
            as2Identifier:false,
            partnerURL: false
          });
          setValues({
            ...values,
            connectorDescription: '',
            as2Identifier: '', 
            partnerURL:'',

          });
          setSubmitErr(false);
        // setNameError(false);
        };
        return (
          <MuiThemeProvider theme={getMuiTheme()}>
            
              
        <div>
          {ErrMsg === true ? (
            <div style={{ textAlign: 'center' }} className="addfieldserror">
              Please fill the fields
            </div>
          ) : (
            ''
          )}
      <form className="content" noValidate autoComplete="off">
        <Grid container spacing={3} >
      <Grid item xs={12} sm={3}>
                <TextField
                    id="outlined-dense"
                    label= "Connector ID"
                    className="textField"
                    margin="dense"
                    variant="outlined"
                    onChange={handleChange('connectorId')}
                    value={values.connectorId}
                    InputProps={{
                        readOnly: true,
                    }}
                    name="connectorId"
                    />

                {errorInfo.connectorId === true ? (
                <div className="addfieldserror">
                    "Allowed alphabets,no special characters and Numerics are
                    allowed.Maximum length is 25."
                </div>
                ) : (
                ''
                )}
                
            </Grid>
      <Grid item xs={12} sm={3}>
                <TextField
                id="outlined-dense"
                label="Connector Description "
                className="textField"
                margin="dense"
                variant="outlined"
                onChange={handleChange('connectorDescription')}
                value={values.connectorDescription}
                name="connectorDescription"
                />
        {/*
                {errorInfo.name === true ? (
                <div className="addfieldserror">
                    "Enter valid IP Address"
                </div>
                ) : (
                ''
                )}
                */}
                  {submitErr && !values.connectorDescription ? (
                <div id="nameid" className="addfieldserror">
                     Please enter  description
                 </div>
               ) : (
                          ''
                  )}

      </Grid>
      <Grid item xs={12} sm={3}>
            <TextField
                id="outlined-dense"
                label="AS2 Identifier"
                className="textField"
                margin="dense"
                variant="outlined"
                onChange={handleChange('as2Identifier')}
                value={values.as2Identifier}
                name="as2Identifier"
            />
                {errorInfo.as2Identifier === true ? (
                <div className="addfieldserror">
                "Enter valid AS2 Identifier "
                </div>
            ) : (
                ''
            )}

            {submitErr && !values.as2Identifier ? (
                <div id="nameid" className="addfieldserror">
                     Please enter  AS2 Identifier
                 </div>
               ) : (
                          ''
                  )}
                </Grid>

     <Grid item xs={12} sm={3}>
            <TextField
                id="outlined-dense"
                label="Partner URL"
                className="textField"
                margin="dense"
                variant="outlined"
                onChange={handleChange('partnerURL')}
                value={values.partnerURL}
                name="partnerURL"
                />
                {errorInfo.partnerURL === true ? (
                <div className="addfieldserror">
                    "Enter valid url "
                </div>
                ) : (
                ''
                )}
                   {submitErr && !values.partnerURL ? (
                <div id="nameid" className="addfieldserror">
                     Please enter  url
                 </div>
               ) : (
                          ''
                  )}
                
                </Grid>
            </Grid>

      <Grid item xs={12} sm={12}>
        <p className="advancedFrom" style={{ color: '#0b153e' }}>
                   Connection Info
         </p>
     </Grid>
      <Grid item xs={12} sm={12}>
        <p className="Advancedheader" style={{ color: '#0b153e'}}>
                     Send Message Security
         </p>
     </Grid>
         <FormGroup aria-label="position" 
         row>
        <Grid item xs={12} sm={6}>
           <Typography className="Advancedtext">
                    <Checkbox 
                     inputProps={{
                  'aria-label': 'uncontrolled-checkbox',
                   }}
                   value={values.signSendData}
                  checked={values.signSendData}
                  name='signSendData'
                  onChange={handleChange('signSendData')}
                  color="primary"
         />
                  Sign send data
                </Typography>
              </Grid>

       <Grid item xs={12} sm={6}>
          <Typography className="Advancedtext">
                  <Checkbox
                   inputProps={{
                 'aria-label': 'uncontrolled-checkbox',
                   }}
                    value={values.encryptSendData}
                   checked={values.encryptSendData}
                   name='encryptSendData'
                   onChange={handleChange('encryptSendData')}
                   color="primary"
              />
                 Encrypt send data
               </Typography>
               </Grid>
               <Grid item xs={12} sm={12}>
        <p className="Advancedheader" style={{ color: '#0b153e' }}>
                     Receive Message Security
         </p>
     </Grid>
     <Grid item xs={12} sm={6}>
         <Typography className="Advancedtext">
                    <Checkbox
                    inputProps={{
                    'aria-label': 'uncontrolled-checkbox',
                    }}
                        value={values.requireSign}
                    checked={values.requireSign}
                    name='requireSign'
                    onChange={handleChange('requireSign')}
                    color="primary"
                />
                    Require Signature
         </Typography>
     </Grid>

     <Grid item xs={12} sm={6}>
        <Typography className="Advancedtext">
                  <Checkbox
                   inputProps={{
                 'aria-label': 'uncontrolled-checkbox',
                   }}
                    value={values.requireEncryption}
                   checked={values.requireEncryption}
                   name='requireEncryption'
                   onChange={handleChange('requireEncryption')}
                  color="primary"
              />
                 Require Encryption
         </Typography>
     </Grid>
     <Grid item xs={12} sm={12}>
        <p className="advancedFrom" style={{ color: '#0b153e' }}>
                MDN Receipt(Request)
         </p>
        
     <Grid item xs={12} sm={6}>
       <Typography className="Advancedtext">
                  <Checkbox
                   inputProps={{
                 'aria-label': 'uncontrolled-checkbox',
                   }}
                    value={values.requestMDNReceipt}
                   checked={values.requestMDNReceipt}
                  name='requestMDNReceipt'
                  onChange={handleChange('requestMDNReceipt')}
                  color="primary"
              />
                Request MDN Receipt
               </Typography>
               </Grid>
            </Grid>
             </FormGroup>
              <p className="advancedFrom" style={{ color: '#0b153e'}}>
                     Security
             </p>
             
                    <RadioGroup 
                  
                    aria-label="  Security"
                    name="Security"
                    color="primary"
                    value={values.Security}
                    onChange={handleChange('Security')}
                    row
                  >
                     <Grid item xs={6} sm={6}  >
                       <FormControlLabel   className="Advancedtext" 
                        value="signed"
                        color="primary"
                        control={<Radio />}
                        label="Signed"
                      />
                    </Grid>
                    <Grid item xs={6} sm={6}>
                        <FormControlLabel
                        value="unSigned"
                        control={<Radio color="primary" />}
                        label="Unsigned"
                         />
                    </Grid>
                 </RadioGroup>
                  <p className="advancedFrom" style={{ color: '#0b153e'}}>
                     Delivery
                 </p>
                <RadioGroup
                    aria-label="Delivery"
                    name="Delivery"
                    color="primary"
                    value={values.Delivery}
                    onChange={handleChange('Delivery')}
                    row
                  >
                      <Grid item xs={6} sm={6}>
                           <FormControlLabel
                        value="delivery"
                        color="primary"
                        control={<Radio />}
                        label="Synchronous"
                            />
                     </Grid>
                     <Grid item xs={6} sm={6}>
                         <FormControlLabel
                        value="Asynchronous"
                        control={<Radio color="primary" />}
                        label="Asynchronous"
                      />
                    </Grid>
         </RadioGroup>
         <Grid item xs={12} sm={12}>
        <p className="advancedFrom" style={{ color: '#0b153e' }}>
        Trading Partner Certificate
         </p>
         </Grid>
         <Grid item xs={12} sm={4}>
                <FormControl>
                      <InputLabel
                        style={{ color: '#0b153e', fontSize: 12 }}
                        id="demo-simple-select-label"
                      >
                        Encryption Certificate
                      </InputLabel>
                      <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={values.encryptionCertificate}
                        onChange={handleChange('encryptionCertificate')}
                        name="encryptionCertificate"
                      >
                        <MenuItem value={true}>+ upload certificate</MenuItem>
                      </Select>
                    </FormControl>
                </Grid>
                  <Grid container spacing={3} >
                  <Grid
                        item
                        xs={12}
                        sm={6}
                        gutterBottom
                        variant="body2"
                        color="textSecondary"
                        className="text"
                      >
                        <Button
                          style={{
                            color: '#000006',
                            backgroundColor: '#E5CCFF',
                          }}
                         onClick={handleReset}
                          variant="contained"
                          fullWidth="true"
                          className="createpartnerbutton"
                        >
                          RESET
                        </Button>
                      </Grid>
                      
                  <Grid
                        item
                        xs={12}
                        sm={6}
                        gutterBottom
                        variant="body2"
                        color="textSecondary"
                        className="text"
                      >
        <Button
          onClick={handleSubmit}
          variant="contained"
          fullWidth="true"
          className="createpartnerbutton"
        >
          Create
        </Button>
        </Grid>
        </Grid>
      </form>
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center'
        }}
        autoHideDuration={5000}
        open={openSnackbar}
        onClose={handleCloseSnackbar}
      >
        <MySnackbarContentWrapper
          onClose={handleCloseSnackbar}
          variant={variant}
          message={message}
        />
      </Snackbar>
      <UploadCertificateModel
        handleCloseModal={handleCloseForm}
        isModalOpen={openModal}
      >
        <UploadCertificateForm
          handleCloseModal={handleCloseForm}
         // onSuccess={handleRefetch}
          isModalOpen={openModal}
        />
      </UploadCertificateModel>
    </div>
  
   
    </MuiThemeProvider>
  );
}