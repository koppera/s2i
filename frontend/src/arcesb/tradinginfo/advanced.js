import React, { useEffect, useContext, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MySnackbarContentWrapper from '../../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import '../../css/commonStyle.css';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import Theme from '../../css/theme';
import Modal from '@material-ui/core/Modal';
import Grid from '@material-ui/core/Grid';
import AddIcon from '@material-ui/icons/Add';
import Checkbox from '@material-ui/core/Checkbox';
import UploadCertificateForm from './uploadCertificateModel';
import UploadCertificateModel from '../../components/tmsmodal';
import CloseIcon from '@material-ui/icons/Close';
import {Typography } from '@material-ui/core';
import {
  createMuiTheme,
  MuiThemeProvider,
} from '@material-ui/core/styles';

export default function AdvancedForm(props) {
  const [openModal, setOpenModal] = React.useState(false);
  const [values, setValues] = React.useState({
       input: '',
       output: '',  
       processed: '',
       as2restart: false,
       relability:true,
       streaming:false,
       userprofile:false,
       privateCertificate:false,
       localas2identifier:'',
       certificatepassword:'',
       tlscertificatepassword:'',
       HTTPAuthenticationType:'Basic',
       user: '',
       password: '',
       characterset: '',
       headersName:'',
       headersValue:'',
       maxWorkers:'',
       maxFiles:'',
       asyncMDNTimeout:'60',
       duplicateFileAction:'',
       duplicateFileInterval:'',
       encryptionAlgorithm:'',
       extensionMap:'',
       HTTPSubject:'',
       logLevel:'',
       logRequests:true,
       messageId:'',
       parentConnector:'',
       parseFDAExtensions:false,
       partnerSigningCertificate:'',
       processingDelay:'',
       signatureAlgorithm:'',
       SSLv2:false,
       SSLv3:false,
       TLSv10:true,
       TLSv11:true,
       TLSv12:true,
       TLSv13:false,
       tempReceiveDirectory:'',
       logMessages:true,
       sendFilter:'',
       savetoSentFolder:true
     });
  const [ErrMsg, setErrMsg] = React.useState(false);
  const [errorInfo, setErrorInfo] = React.useState({
    input:false,
    output:false,
    processed:false,
    certificatepassword:false,
    tlscertificatepassword:false,
    localas2identifier:false,
    HTTPAuthenticationType:false,
    user: false,
    password: false,
    characterset: false,
    headersName:false,
   	headersValue:false,
    maxWorkers:false,
    maxFiles:false,
    asyncMDNTimeout:false,
    duplicateFileAction:false,
    duplicateFileInterval:false,
    encryptionAlgorithm:false,
    extensionMap:false,
    HTTPSubject:false,
    messageId:false,
    parentConnector:false,
    partnerSigningCertificate:false,
    processingDelay:false,
    signatureAlgorithm:false,
    sendFilter:false, 
  });
  const [submitErr, setSubmitErr] = React.useState(false);
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [variant, setSnackbarVariant] = React.useState('error');
  const [message, setSnackbarMessage] = React.useState();

  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }

  const handleChange = name => event => {
    setErrMsg(false);
    
    setValues({ ...values, [name]: event.target.value });
    switch (name) {
    case 'input':
    case 'processed':
    case 'output':
        if (event.target.value.length === 0) {
            setValues({ ...values, [name]: '' });
            setErrorInfo({ ...errorInfo, [name]: false });
          } else if (event.target.value.match('^[/]{1}[A-Za-z0-9/]*$')) {
            setErrorInfo({ ...errorInfo, [name]: false });
            setValues({ ...values, [name]: event.target.value });
          } else {
            setErrorInfo({ ...errorInfo, [name]: true });
          }
          break;
    case 'localas2identifier':
    case 'extensionMap':
            if (event.target.value.length === 0) {
                setValues({ ...values, [name]: '' });
                setErrorInfo({ ...errorInfo, [name]: false });
              } else if (event.target.value.match('')) {
                setErrorInfo({ ...errorInfo, [name]: false });
                setValues({ ...values, [name]: event.target.value });
              } else {
                setErrorInfo({ ...errorInfo, [name]: true });
              }
              break;
    case 'certificatepassword':
    case 'tlscertificatepassword':
    case 'password':
                if (event.target.value.length === 0) {
                  setValues({ ...values, [name]: '' });
                  setErrorInfo({ ...errorInfo, [name]: false });
                } else if (
                  event.target.value.match(
                    '^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,15}$'
                  )
                ) {
                  setErrorInfo({ ...errorInfo, [name]: false });
                  setValues({ ...values, [name]: event.target.value });
                } else {
                  setErrorInfo({ ...errorInfo, [name]: true });
                }
                break;
    case 'userprofile':
    case 'streaming':  
    case 'as2restart':
    case 'relability':
    case 'logRequests':
    case 'parseFDAExtensions':
    case 'SSLv2':
    case 'SSLv3':
    case 'TLSv10':
    case 'TLSv11':
    case 'TLSv12':
    case 'TLSv13':
    case 'logMessages':
    case 'savetoSentFolder':
        setValues({ ...values, [event.target.name]: event.target.checked });
         break;
    case 'user':
    case 'messageId':
            if (event.target.value.length === 0) {
              setValues({ ...values, [name]: '' });
              setErrorInfo({ ...errorInfo, [name]: false });
            } else if (event.target.value.match('^[A-Z0-9]*$')) {
              setErrorInfo({ ...errorInfo, [name]: false });
              setValues({ ...values, [name]: event.target.value });
            } else {
              setErrorInfo({ ...errorInfo, [name]: true });
            }
            break;
    case 'characterset':
    case 'headersName':
    case 'HTTPSubject':
    case 'parentConnector':
    case 'partnerSigningCertificate':
    case 'tempReceiveDirectory':
    case 'sendFilter':
                if (event.target.value.length === 0) {
                  setValues({ ...values, [name]: '' });
                  setErrorInfo({ ...errorInfo, [name]: false });
                } else if (event.target.value.match('^[a-zA-Z0-9/. ]*$')) {
                  setErrorInfo({ ...errorInfo, [name]: false });
                } else setErrorInfo({ ...errorInfo, [name]: true });
                break;
    case 'headersValue':
    case 'maxWorkers':
    case 'maxFiles':
    case 'asyncMDNTimeout':
    case 'duplicateFileInterval':
    case 'processingDelay':
                 if (event.target.value.length === 0) {
                    setValues({ ...values, [name]: '' });
                    setErrorInfo({ ...errorInfo, [name]: false });
                } else if (event.target.value.match('^[-(0-9)]*$')) {
                    setErrorInfo({ ...errorInfo, [name]: false });
                } else setErrorInfo({ ...errorInfo, [name]: true });
                break;
    case 'duplicateFileAction':
    case 'encryptionAlgorithm':
    case 'logLevel':
    case 'signatureAlgorithm':
                    setValues({
                      ...values,
                      [event.target.name]: event.target.value,
                    });
                    break;
    case 'privateCertificate':
             setValues({ ...values, [name]: event.target.value });
            console.log(values);
             if(values)
             {
               setOpenModal(true);
              }
              break;
    case 'FTPHost':
                    if (event.target.value.length === 0) {
                      setValues({ ...values, [name]: '' });
                      setErrorInfo({ ...errorInfo, [name]: false });
                    } else if (event.target.value.match(/^[a-zA-Z0-9./ ]+(\.\w{2,})+$/)) {
                      setErrorInfo({ ...errorInfo, [name]: false });
                      setValues({ ...values, [name]: event.target.value });
                    } else {
                      setErrorInfo({ ...errorInfo, [name]: true });
                    }
                    break;
    default:
        return false;
    }
  };

  const handleCloseModal = () => {
    props.handleCloseModal(false);
  };
  const handleSubmit = async () => {
    console.log(values);
    setErrMsg(false);
    if (values.input !== '' && 
        values.output!== '' &&
        values.processed!== ''&&
        //values.localas2identifier!== '' && 
       // values.certificatepassword!== '' &&
        values.tlscertificatepassword!== '' && 
        values.HTTPAuthenticationType!== '' && 
        values.user!== '' && 
        values.password!== '' && 
        values.characterset!== '' && 
        values.headersName!== '' && 
        values.headersValue!== '' && 
       // values.maxWorkers!== '' && 
       // values.maxFiles!== '' && 
        values.asyncMDNTimeout!== '' && 
        values.duplicateFileAction!== '' && 
        values.duplicateFileInterval!== '' && 
        values.encryptionAlgorithm!== '' && 
        values.extensionMap!== '' && 
        values.HTTPSubject!== '' && 
        values.messageId!== '' && 
        values.parentConnector!== '' && 
        values.partnerSigningCertificate!== '' && 
        values.processingDelay!== '' && 
        values.signatureAlgorithm!== '' && 
        values.sendFilter !== '' && 
        errorInfo.input !== true &&
        errorInfo.output !== true &&
        errorInfo.processed !== true &&
        errorInfo.localas2identifier !== true &&
        errorInfo.certificatepassword !== true &&
        errorInfo.tlscertificatepassword !== true&&
        errorInfo.HTTPAuthenticationType!== true &&
        errorInfo.user!== true &&
        errorInfo.password!== true &&
        errorInfo.characterset!== true &&
        errorInfo.headersName!== true &&
        errorInfo.headersValue!== true &&
        errorInfo.maxWorkers!== true &&
        errorInfo.maxFiles!== true &&
        errorInfo.asyncMDNTimeout!== true &&
        errorInfo.FileAction!== true &&
        errorInfo.duplicateFileInterval!== true &&
        errorInfo.encryptionAlgorithm!== true &&
        errorInfo.extensionMap!== true &&
        errorInfo.HTTPSubject!== true &&
        errorInfo.messageId!== true &&
        errorInfo.parentConnector!== true &&
        errorInfo.partnerSigningCertificate!== true &&
        errorInfo.processingDelay!== true &&
        errorInfo.signatureAlgorithm!== true &&
        errorInfo.sendFilter!== true   ) 
    {
      setErrMsg(false);
      setValues({
        ...values,
        input: values.input,
        output: values.output,
        processed: values.processed,
        //localas2identifier: values.localas2identifier,
        //certificatepassword: values.certificatepassword,
        tlscertificatepassword: values.tlscertificatepassword,
        HTTPAuthenticationType:values.HTTPAuthenticationType,
        user:values.user,
        password: values.password,
        characterset:values.characterset,
        headersName:values.headersName,
        headersValue:values.headersValue,
        //maxWorkers:values.maxWorkers,
        //maxFiles:values.maxFiles,
        asyncMDNTimeout:values.asyncMDNTimeout,
        duplicateFileAction:values.duplicateFileAction,
        duplicateFileInterval:values.duplicateFileInterval,
        encryptionAlgorithm:values.encryptionAlgorithm,
        extensionMap:values.extensionMap,
        HTTPSubject:values.HTTPSubject,
        logLevel:values.logLevel,
        logRequests:values.logRequests,
        messageId:values.messageId,
        parentConnector:values.parentConnector,
        parseFDAExtensions:values.parseFDAExtensions,
        partnerSigningCertificate:values.partnerSigningCertificate,
        processingDelay:values.processingDelay,
        signatureAlgorithm:values.signatureAlgorithm,
        SSLv2:values.SSLv2,
        SSLv3:values.SSLv3,
        TLSv10:values.TLSv10,
        TLSv11:values.TLSv11,
        TLSv12:values.TLSv12,
        TLSv13:values.TLSv13,
        tempReceiveDirectory:values.tempReceiveDirectory,
        logMessages:values.logMessages,
        sendFilter:values.sendFilter,
        savetoSentFolder:values.savetoSentFolder
    });
    //add submit functionality here
   }
   else  if ( 
    values.input == '' &&
    values.output == '' &&
    values.processed == '' &&
    //values.localas2identifier == '' &&
    //values.certificatepassword == '' &&
    values.tlscertificatepassword == ''&&
    values.HTTPAuthenticationType== '' && 
    values.user== '' && 
    values.password== '' && 
    values.characterset== '' && 
    values.headersName== '' && 
    values.headersValue== '' && 
    //values.maxWorkers== '' && 
    //values.maxFiles== '' && 
    values.asyncMDNTimeout== '' && 
    values.duplicateFileAction== '' && 
    values.duplicateFileInterval== '' && 
    values.encryptionAlgorithm== '' && 
    values.extensionMap== '' && 
    values.HTTPSubject== '' && 
    values.messageId== '' && 
    values.parentConnector== '' && 
    values.partnerSigningCertificate== '' && 
    values.processingDelay== '' && 
    values.signatureAlgorithm== '' && 
    values.sendFilter == '' 
    )
   {
    setErrMsg(true);
  } else 
  {
    setErrMsg(false);
    setSubmitErr(true);  
  }
};
 
  const handleReset = async () => {
    setErrMsg(false);
    setErrorInfo({
      input:false,
      output:false,
      processed:false,
      certificatepassword:false,
      tlscertificatepassword:false,
      localas2identifier:false,
      HTTPAuthenticationType:false,
      user: false,
      password: false,
      characterset: false,
      headersName:false,
       headersValue:false,
      maxWorkers:false,
      maxFiles:false,
      asyncMDNTimeout:false,
      duplicateFileAction:false,
      duplicateFileInterval:false,
      encryptionAlgorithm:false,
      extensionMap:false,
      HTTPSubject:false,
      messageId:false,
      parentConnector:false,
      partnerSigningCertificate:false,
      processingDelay:false,
      signatureAlgorithm:false,
      sendFilter:false, 
    });
    setValues({
      ...values,
      input: '',
       output: '',  
       processed: '',
       as2restart: false,
       relability:true,
       streaming:false,
       userprofile:false,
       localas2identifier:'',
       certificatepassword:'',
       tlscertificatepassword:'',
       HTTPAuthenticationType:'Basic',
       user: '',
       password: '',
       characterset: '',
       headersName:'',
       headersValue:'',
       maxWorkers:'',
       maxFiles:'',
       asyncMDNTimeout:'60',
       duplicateFileAction:'',
       duplicateFileInterval:'',
       encryptionAlgorithm:'',
       extensionMap:'',
       HTTPSubject:'',
       logLevel:'',
       logRequests:true,
       messageId:'',
       parentConnector:'',
       parseFDAExtensions:false,
       partnerSigningCertificate:'',
       processingDelay:'',
       signatureAlgorithm:'',
       SSLv2:false,
       SSLv3:false,
       TLSv10:true,
       TLSv11:true,
       TLSv12:true,
       TLSv13:false,
       tempReceiveDirectory:'',
       logMessages:true,
       sendFilter:'',
       savetoSentFolder:true
 
    });
    setSubmitErr(false);
   // setNameError(false);
  };
  const handleCloseForm =() =>
  {
      setOpenModal(false);
      setValues({...values,privateCertificate:false})
  }
  const getMuiTheme = () =>
  createMuiTheme(Theme, {
    overrides: {
      MuiInputLabel: {
        animated: {
          fontSize: 13,
        },
        outlined: {
          paddingTop: 3,
        },
      },
      MuiSelect: {
        select: {
          minWidth: '180px',
        },
      },
    },
  });
  return (
    <MuiThemeProvider theme={getMuiTheme()}> 
    <div>
      {ErrMsg === true ? (
        <div style={{ textAlign: 'center' }} className="addfieldserror">
          Please fill the fields
        </div>
      ) : (
        ''
      )}
      <form  noValidate autoComplete="off">
      <Grid className="AddressconfigGrid">
          <Grid container  spacing={2}className="userformcontent" >
         {/* <Grid item xs={12} sm={12}>
     <p className="advancedForm" style={{color:"#0b1352"}}> Very Large Message Support (VLM)</p></Grid>*/} 
      <Grid item xs={12} sm={4}>
          <Typography className="Advancedheader">Streaming</Typography>
          <Typography className="Advancedtext">
            <Checkbox
                 inputProps={{
                 'aria-label': 'uncontrolled-checkbox',
                 
                 }}
                 value={values.streaming}
                 checked={values.streaming}
               name='streaming'
             onChange={handleChange('streaming')}
               color="primary"
                />
               Enable Streaming-(HTTP chunked transfer encoding)
                 </Typography>
                </Grid>   
               
                <Grid item xs={12} sm={4}>
          <Typography className="Advancedheader">AS2 Restart</Typography>
      <Typography className="Advancedtext">
            <Checkbox
                 inputProps={{
                 'aria-label': 'uncontrolled-checkbox',
                 }} value={values.as2restart}
                 checked={values.as2restart}
               name='as2restart'
             onChange={handleChange('as2restart')}
               color="primary"

                />
              Enable AS2 Restart
                 </Typography>
                </Grid>  
                <Grid item xs={12} sm={4}>
          <Typography className="Advancedheader">AS2 Reliablity</Typography>
      <Typography className="Advancedtext">
            <Checkbox
                 inputProps={{
                 'aria-label': 'uncontrolled-checkbox',
                 
                 }}
                 value={values.relability}
                 checked={values.relability}
               name='relablity'
             onChange={handleChange('relability')}
               color="primary"
                />
               AS2 Reliability
                 </Typography>
                </Grid>   
                {/* <Grid item xs={12} sm={12}>
        <p className="advancedForm"style={{ color: '#0b153e' }}>
                   Local Folders
         </p>
                </Grid>*/}
      <Grid  item xs={12} sm={4}>
        <TextField
        style={{padding:"2px"}}
          id="outlined-dense"
          label="Input Folder(Send) "
          className="textField"
          margin="dense"
          variant="outlined"
          onChange={handleChange('input')}
          value={values.input}
          name="input"
        />

        {errorInfo.input === true ? (
          <div className="addfieldserror">
            "Start with / followed alphanumeric"
          </div>
        ) : (
          ''
        )}
         {submitErr && !values.input ? (
                    <div id="nameid" className="addfieldserror">
                      Please enter Input Folder
                    </div>
                  ) : (
                    ''
                  )}
        </Grid> 
       
        <Grid  item xs={12} sm={4}>
        <TextField
          id="outlined-dense"
          label="Output Folder(Receiver) "
          className="textField"
          margin="dense"
          variant="outlined"
          onChange={handleChange('output')}
          value={values.output}
          name="output"
        />

        {errorInfo.output === true ? (
          <div className="addfieldserror">
           "Start with / followed alphanumeric"
          </div>
        ) : (
          ''
        )}
         {submitErr && !values.output ? (
                    <div id="nameid" className="addfieldserror">
                      Please enter Output Folder
                    </div>
                  ) : (
                    ''
                  )}
        </Grid> 
        <Grid  item xs={12} sm={4}>
        <TextField
          id="outlined-dense"
          label="Processed Folder(Sent) "
          className="textField"
          margin="dense"
          variant="outlined"
          onChange={handleChange('processed')}
          value={values.processed}
          name="processed"
        />

        {errorInfo.processed === true ? (
          <div className="addfieldserror">
            "Start with / followed alphanumeric"
          </div>
        ) : (
          ''
        )}
         {submitErr && !values.processed ? (
                    <div id="nameid" className="addfieldserror">
                      Please enter Processed Folder
                    </div>
                  ) : (
                    ''
                  )}
        </Grid> 
     {/*   <Grid item xs={12} sm={12}>
        <p className="advancedForm"style={{ color: '#0b153e' }}>
                   Alternate Local Profile
         </p>
     </Grid>
        <Grid  item xs={12} sm={4}>
        <TextField
          id="outlined-dense"
          label="Local AS2 Identifier "
          className="textField"
          margin="dense"
          variant="outlined"
          onChange={handleChange('localas2identifier')}
          value={values.localas2identifier}
          name="localas2identifier"
        />

        {errorInfo.localas2identifier === true ? (
          <div className="addfieldserror">
            "enter localas2identifier"
          </div>
        ) : (
          ''
        )}
         {submitErr && !values.localas2identifier ? (
                    <div id="nameid" className="addfieldserror">
                      Please enter Local AS2 Identifier
                    </div>
                  ) : (
                    ''
                  )}
        </Grid> 
        <Grid item xs={12} sm={4}>
                <FormControl>
                      <InputLabel
                        style={{ color: '#0b153e', fontSize: 12 }}
                        id="demo-simple-select-label"
                      >
                        Private Certificate
                      </InputLabel>
                      <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={values.privateCertificate}
                        onChange={handleChange('privateCertificate')}
                        name="privateCertificate"
                      >
                        <MenuItem value={true}><AddIcon/>upload certificate</MenuItem>
                      </Select>
                    </FormControl>
                </Grid>
                <Grid  item xs={12} sm={4}>
            <TextField
            id="outlined-dense"
            label="Certificate Password "
            className="textField"
            margin="dense"
            type="password"
            variant="outlined"
            onChange={handleChange('certificatepassword')}
            value={values.certificatepassword}
            name="certificatepassword"
            />

            {errorInfo.certificatepassword === true ? (
            <div className="addfieldserror">
                " 8 to 15 characters must have atleast one[uppercase,lowercase,special character and Number]"
            </div>
            ) : (
            ''
            )}
             {submitErr && !values.certificatepassword ? (
                    <div id="nameid" className="addfieldserror">
                      Please enter valid Password
                    </div>
                  ) : (
                    ''
                  )}
            </Grid> 
            <Grid item xs={12} sm={12}>
            <p className="advancedForm"style={{ color: '#0b153e' }}>
            TLS Client Authentication
         </p>
            
                  </Grid>*/}
            <Grid item xs={12} sm={4}>
          <Typography  className="Advancedheader">User Profile Settings :</Typography>
      <Typography className="Advancedtext">
            <Checkbox
                 inputProps={{
                 'aria-label': 'uncontrolled-checkbox',
                 
                 }}
                 value={values.userprofile}
                 checked={values.userprofile}
               name='userprofile'
             onChange={handleChange('userprofile')}
               color="primary"
                />
               User Private Certificate from profile Tab
                 </Typography>
                </Grid> 
                <Grid
                    item
                    xs={12}
                    sm={4}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
                <FormControl>
                      <InputLabel
                        style={{ color: '#0b153e', fontSize: 12 }}
                        id="demo-simple-select-label"
                      >
                        Private Certificate
                      </InputLabel>
                      <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={values.privateCertificate}
                        onChange={handleChange('privateCertificate')}
                        name="privateCertificate"
                      >
                        <MenuItem value={true}><AddIcon/>upload certificate</MenuItem>
                      </Select>
                    </FormControl>
                </Grid>
                <Grid  item xs={12} sm={4}>
            <TextField
            id="outlined-dense"
            label="Certificate Password "
            className="textField"
            margin="dense"
            type="password"
            variant="outlined"
            onChange={handleChange('tlscertificatepassword')}
            value={values.tlscertificatepassword}
            name="tlscertificatepassword"
            />

            {errorInfo.tlscertificatepassword === true ? (
            <div className="addfieldserror">
                " 8 to 15 characters must have atleast one[uppercase,lowercase,special character and Number]"
            </div>
            ) : (
            ''
            )}
              {submitErr && !values.tlscertificatepassword ? (
                    <div id="nameid" className="addfieldserror">
                      Please enter valid Password
                    </div>
                  ) : (
                    ''
                  )}
            </Grid>
            <Grid
                  item
                  xs={12}
                  sm={8}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="Advancedheader">HTTP Authentication Type:</Typography>
                  <RadioGroup
                    aria-label="SettingsForma"
                    name="HTTPAuthenticationType"
                    color="primary"
                    value={values.HTTPAuthenticationType}
                    onChange={handleChange('HTTPAuthenticationType')}
                    row
                  >
                    <Grid item xs={6} sm={6}>
                      <FormControlLabel
                        value="Basic"
                        className="Advancedtext"
                        color="primary"
                        control={<Radio />}
                        label="Basic"
                      />
                    </Grid>
                    <Grid item xs={6} sm={6}>
                      <FormControlLabel
                        value="Digest"
                        className="Advancedtext"
                        control={<Radio color="primary" />}
                        label="Digest"
                      />
                    </Grid>
                  </RadioGroup>
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={4}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <TextField
                    id="outlined-dense"
                    label="User "
                    className="partnertextField"
                    margin="dense"
                    variant="outlined"
                    onChange={handleChange('user')}
                    value={values.user}
                    name="user"
                  />
                  {errorInfo.user === true ? (
                    <div id="nameid" className="addfieldserror">
                      "Enter only alphabets"
                    </div>
                  ) : (
                    ''
                  )}
                  {submitErr && !values.user ? (
                    <div id="nameid" className="addfieldserror">
                      Please enter user
                    </div>
                  ) : (
                    ''
                  )}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={4}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <TextField
                    id="outlined-dense"
                    label="Password "
                    type="password"
                    className="partnertextField"
                    margin="dense"
                    variant="outlined"
                    onChange={handleChange('password')}
                    value={values.password}
                    name="password"
                  />
                  {errorInfo.password === true ? (
                    <div id="nameid" className="addfieldserror">
                      "4 to 8 characters must have atleast
                      one[uppercase,lowercase,special character and number]"
                    </div>
                  ) : (
                    ''
                  )}
                  {submitErr && !values.password ? (
                    <div id="nameid" className="addfieldserror">
                      Please enter password
                    </div>
                  ) : (
                    ''
                  )}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={12}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                <p className="advancedForm" style={{color:"#0b1352"}}>   Custom Headers:</p>
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={4}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <TextField
                    id="outlined-dense"
                    label="Name"
                    className="partnertextField"
                    margin="dense"
                    variant="outlined"
                    //placeholder=""
                    onChange={handleChange('headersName')}
                    value={values.headersName}
                    name="headersName"
                  />
                  {errorInfo.headersName === true ? (
                    <div id="nameid" className="addfieldserror">
                      "Enter only alphabets"
                    </div>
                  ) : (
                    ''
                  )}
                  {submitErr && !values.headersName ? (
                    <div id="nameid" className="addfieldserror">
                      Please enter Name
                    </div>
                  ) : (
                    ''
                  )}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={4}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <TextField
                    id="outlined-dense"
                    label="Value"
                    className="partnertextField"
                    margin="dense"
                    variant="outlined"
                    //placeholder=""
                    onChange={handleChange('headersValue')}
                    value={values.headersValue}
                    name="headersValue"
                  />
                  {errorInfo.headersValue === true ? (
                    <div id="nameid" className="addfieldserror">
                      "Enter only alphanumeric characters"
                    </div>
                  ) : (
                    ''
                  )}
                  {submitErr && !values.headersValue ? (
                    <div id="nameid" className="addfieldserror">
                      Please enter value
                    </div>
                  ) : (
                    ''
                  )}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={4}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Button
                    // onClick={handleReset}
                    variant="contained"
                    fullWidth="true"
                    className="createpartnerbutton"
                  >
                    Add Header
                  </Button>
              </Grid>
             {/* <Grid
                  item
                  xs={12}
                  sm={12}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <p className="advancedForm" style={{color:"#0b1352"}}> Performance:</p>
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={6}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <TextField
                    id="outlined-dense"
                    label="Max Workers"
                    className="partnertextField"
                    margin="dense"
                    variant="outlined"
                    //placeholder=""
                    onChange={handleChange('maxWorkers')}
                    value={values.maxWorkers}
                    name="maxWorkers"
                  />
                  {errorInfo.maxWorkers === true ? (
                    <div id="nameid" className="addfieldserror">
                      "Enter only numeric digits "
                    </div>
                  ) : (
                    ''
                  )}
                  {submitErr && !values.maxWorkers ? (
                    <div id="nameid" className="addfieldserror">
                      Please enter Max Workers
                    </div>
                  ) : (
                    ''
                  )}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={6}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <TextField
                    id="outlined-dense"
                    label="Max Files"
                    className="partnertextField"
                    margin="dense"
                    variant="outlined"
                    //placeholder=""
                    onChange={handleChange('maxFiles')}
                    value={values.maxFiles}
                    name="maxFiles"
                  />
                  {errorInfo.maxFiles === true ? (
                    <div id="nameid" className="addfieldserror">
                      "Enter only numeric digits "
                    </div>
                  ) : (
                    ''
                  )}
                  {submitErr && !values.maxFiles ? (
                    <div id="nameid" className="addfieldserror">
                      Please enter Max Files
                    </div>
                  ) : (
                    ''
                  )}
                  </Grid>*/}
                <Grid
                  item
                  xs={12}
                  sm={12}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                   <p className="advancedForm" style={{color:"#0b1352"}}> Other settings:</p>
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <TextField
                    id="outlined-dense"
                    label="Async MDN Timeout(seconds)"
                    className="partnertextField"
                    margin="dense"
                    variant="outlined"
                    //placeholder=""
                    onChange={handleChange('asyncMDNTimeout')}
                    value={values.asyncMDNTimeout}
                    name="asyncMDNTimeout"
                  />
                  {errorInfo.asyncMDNTimeout === true ? (
                    <div id="nameid" className="addfieldserror">
                      "Enter only numeric digits "
                    </div>
                  ) : (
                    ''
                  )}
                  {submitErr && !values.asyncMDNTimeout ? (
                    <div id="nameid" className="addfieldserror">
                      Please enter async MDN Timeout
                    </div>
                  ) : (
                    ''
                  )}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <FormControl>
                    <InputLabel
                      style={{ color: '#0b153e', fontSize: 12 }}
                      id="demo-simple-select-label"
                    >
                     Duplicate File Action
                    </InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={values.duplicateFileAction}
                      onChange={handleChange('duplicateFileAction')}
                      name="duplicateFileAction"
                    >
                      <MenuItem value={'Continue'}>Continue</MenuItem>
                    </Select>
                  </FormControl>
                  {submitErr && !values.duplicateFileAction ? (
                    <div id="nameid" className="addfieldserror">
                      Please select Duplicate File Action
                    </div>
                  ) : (
                    ''
                  )}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <TextField
                    id="outlined-dense"
                    label="Duplicate File Interval"
                    className="partnertextField"
                    margin="dense"
                    variant="outlined"
                    //placeholder=""
                    onChange={handleChange('duplicateFileInterval')}
                    value={values.duplicateFileInterval}
                    name="duplicateFileInterval"
                  />
                  {errorInfo.duplicateFileInterval === true ? (
                    <div id="nameid" className="addfieldserror">
                      "Enter only numeric digits "
                    </div>
                  ) : (
                    ''
                  )}
                  {submitErr && !values.duplicateFileInterval ? (
                    <div id="nameid" className="addfieldserror">
                      Please enter Duplicate File Interval
                    </div>
                  ) : (
                    ''
                  )}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <FormControl>
                    <InputLabel
                      style={{ color: '#0b153e', fontSize: 12 }}
                      id="demo-simple-select-label"
                    >
                    Encryption Algorithm
                    </InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={values.encryptionAlgorithm}
                      onChange={handleChange('encryptionAlgorithm')}
                      name="encryptionAlgorithm"
                    >
                      <MenuItem value={'3DES'}>3DES</MenuItem>
                    </Select>
                  </FormControl>
                  {submitErr && !values.encryptionAlgorithm ? (
                    <div id="nameid" className="addfieldserror">
                      Please select Encryption Algorithm
                    </div>
                  ) : (
                    ''
                  )}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <TextField
                    id="outlined-dense"
                    label="Extension Map"
                    className="partnertextField"
                    margin="dense"
                    variant="outlined"
                    //placeholder=""
                    onChange={handleChange('extensionMap')}
                    value={values.extensionMap}
                    name="extensionMap"
                  />
                  {errorInfo.extensionMap === true ? (
                    <div id="nameid" className="addfieldserror">
                      "Enter only Alphanumeric characters "
                    </div>
                  ) : (
                    ''
                  )}
                  {submitErr && !values.duplicateFileInterval ? (
                    <div id="nameid" className="addfieldserror">
                      Please enter Extension Map
                    </div>
                  ) : (
                    ''
                  )}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <TextField
                    id="outlined-dense"
                    label="HTTP Subject"
                    className="partnertextField"
                    margin="dense"
                    variant="outlined"
                    //placeholder=""
                    onChange={handleChange('HTTPSubject')}
                    value={values.HTTPSubject}
                    name="HTTPSubject"
                  />
                  {errorInfo.HTTPSubject === true ? (
                    <div id="nameid" className="addfieldserror">
                      "Enter only Alphanumeric characters "
                    </div>
                  ) : (
                    ''
                  )}
                  {submitErr && !values.HTTPSubject ? (
                    <div id="nameid" className="addfieldserror">
                      Please enter HTTP Subject
                    </div>
                  ) : (
                    ''
                  )}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <FormControl>
                    <InputLabel
                      style={{ color: '#0b153e', fontSize: 12 }}
                      id="demo-simple-select-label"
                    >
                   Log Level
                    </InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={values.logLevel}
                      onChange={handleChange('logLevel')}
                      name="logLevel"
                    >
                      <MenuItem value={'Info'}>Info</MenuItem>
                    </Select>
                  </FormControl>
                  {submitErr && !values.logLevel ? (
                    <div id="nameid" className="addfieldserror">
                      Please select log Level
                    </div>
                  ) : (
                    ''
                  )}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >  <Typography className="Advancedtext">
                    <Checkbox
                     inputProps={{
                  'aria-label': 'uncontrolled-checkbox',
                   }}
                   value={values.logRequests}
                  checked={values.logRequests}
                name='logRequests'
              onChange={handleChange('logRequests')}
                color="primary"
                  />
                 Log Requests
                </Typography>
              </Grid>
              <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <TextField
                    id="outlined-dense"
                    label="Message Id"
                    className="partnertextField"
                    margin="dense"
                    variant="outlined"
                    //placeholder=""
                    onChange={handleChange('messageId')}
                    value={values.messageId}
                    name="messageId"
                  />
                  {errorInfo.messageId === true ? (
                    <div id="nameid" className="addfieldserror">
                      "Enter only Alphanumeric characters "
                    </div>
                  ) : (
                    ''
                  )}
                  {submitErr && !values.messageId ? (
                    <div id="nameid" className="addfieldserror">
                      Please enter Message Id
                    </div>
                  ) : (
                    ''
                  )}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <TextField
                    id="outlined-dense"
                    label="Parent Connector"
                    className="partnertextField"
                    margin="dense"
                    variant="outlined"
                    //placeholder=""
                    onChange={handleChange('parentConnector')}
                    value={values.parentConnector}
                    name="parentConnector"
                  />
                  {errorInfo.parentConnector === true ? (
                    <div id="nameid" className="addfieldserror">
                      "Enter only Alphanumeric characters "
                    </div>
                  ) : (
                    ''
                  )}
                  {submitErr && !values.parentConnector ? (
                    <div id="nameid" className="addfieldserror">
                      Please enter parent connector
                    </div>
                  ) : (
                    ''
                  )}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                     <Typography className="Advancedtext">
                    <Checkbox
                     inputProps={{
                  'aria-label': 'uncontrolled-checkbox',
                   }}
                   value={values.parseFDAExtensions}
                  checked={values.parseFDAExtensions}
                name='parseFDAExtensions'
              onChange={handleChange('parseFDAExtensions')}
                color="primary"
                  />
                Parse FDA Extensions
                </Typography>
              </Grid>
              <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <FormControl>
                    <InputLabel
                      style={{ color: '#0b153e', fontSize: 12 }}
                      id="demo-simple-select-label"
                    >
                Partner Signing Certificate
                    </InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={values.partnerSigningCertificate}
                      onChange={handleChange('partnerSigningCertificate')}
                      name="partnerSigningCertificate"
                    >
                      <MenuItem value={'None'}>None</MenuItem>
                    </Select>
                  </FormControl>
                  {submitErr && !values.partnerSigningCertificate ? (
                    <div id="nameid" className="addfieldserror">
                      Please select Partner Signing Certificate
                    </div>
                  ) : (
                    ''
                  )}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <TextField
                    id="outlined-dense"
                    label="Processing Delay"
                    className="partnertextField"
                    margin="dense"
                    variant="outlined"
                    //placeholder=""
                    onChange={handleChange('processingDelay')}
                    value={values.processingDelay}
                    name="processingDelay"
                  />
                  {errorInfo.processingDelay === true ? (
                    <div id="nameid" className="addfieldserror">
                      "Enter only numeric digits "
                    </div>
                  ) : (
                    ''
                  )}
                  {submitErr && !values.processingDelay ? (
                    <div id="nameid" className="addfieldserror">
                      Please enter processing delay
                    </div>
                  ) : (
                    ''
                  )}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <FormControl>
                    <InputLabel
                      style={{ color: '#0b153e', fontSize: 12 }}
                      id="demo-simple-select-label"
                    >
                     Signature Algorithm
                    </InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={values.signatureAlgorithm}
                      onChange={handleChange('signatureAlgorithm')}
                      name="signatureAlgorithm"
                    >
                      <MenuItem value={'SHA1'}>SHA1</MenuItem>
                    </Select>
                  </FormControl>
                  {submitErr && !values.signatureAlgorithm ? (
                    <div id="nameid" className="addfieldserror">
                      Please select Partner Signing Certificate
                    </div>
                  ) : (
                    ''
                  )}
                </Grid>
                <Grid container spacing={3}>
                <Grid
                  item
                  xs={12}
                  sm={12}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                <Typography  className="Advancedheader">  TLS Enabled Protocols:</Typography>
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                >
                   <Typography  className="Advancedtext">
                    <Checkbox
                     inputProps={{
                  'aria-label': 'uncontrolled-checkbox',
                   }}
                   value={values.SSLv2}
                  checked={values.SSLv2}
                name='SSLv2'
              onChange={handleChange('SSLv2')}
                color="primary"
                  />
                 SSLv2
                </Typography>
              </Grid>
              <Grid
                  item
                  xs={12}
                  sm={3}
                >
                   <Typography  className="Advancedtext">
                    <Checkbox
                     inputProps={{
                  'aria-label': 'uncontrolled-checkbox',
                   }}
                   value={values.TLSv10}
                  checked={values.TLSv10}
                name='SSLv3'
              onChange={handleChange('SSLv3')}
                color="primary"
                  />
                 SSLv3
                </Typography >
              </Grid>
              <Grid
                  item
                  xs={12}
                  sm={3}
                >
                   <Typography  className="Advancedtext">
                    <Checkbox
                     inputProps={{
                  'aria-label': 'uncontrolled-checkbox',
                   }}
                   value={values.SSLv3}
                  checked={values.SSLv3}
                name='SSLv3'
              onChange={handleChange('SSLv3')}
                color="primary"
                  />
                 SSLv3
                </Typography>
              </Grid>
              <Grid
                  item
                  xs={12}
                  sm={3}
                >
                   <Typography  className="Advancedtext">
                    <Checkbox
                     inputProps={{
                  'aria-label': 'uncontrolled-checkbox',
                   }}
                   value={values.TLSv10}
                  checked={values.TLSv10}
                name='TLSv10'
              onChange={handleChange('TLSv10')}
                color="primary"
                  />
                 TLSv1.0
                </Typography>
              </Grid>
              <Grid
                  item
                  xs={12}
                  sm={3}
                >
                   <Typography  className="Advancedtext">
                    <Checkbox
                     inputProps={{
                  'aria-label': 'uncontrolled-checkbox',
                   }}
                   value={values.TLSv11}
                  checked={values.TLSv11}
                name='TLSv11'
              onChange={handleChange('TLSv11')}
                color="primary"
                  />
                 TLSv1.1
                </Typography>
              </Grid>
              <Grid
                  item
                  xs={12}
                  sm={3}
                >
                   <Typography  className="Advancedtext">
                    <Checkbox
                     inputProps={{
                  'aria-label': 'uncontrolled-checkbox',
                   }}
                   value={values.TLSv12}
                  checked={values.TLSv12}
                name='TLSv12'
              onChange={handleChange('TLSv12')}
                color="primary"
                  />
                TLSv1.2
                </Typography>
              </Grid>
              <Grid
                  item
                  xs={12}
                  sm={3}
                >
                   <Typography  className="Advancedtext">
                    <Checkbox
                     inputProps={{
                  'aria-label': 'uncontrolled-checkbox',
                   }}
                   value={values.TLSv13}
                  checked={values.TLSv13}
                name='TLSv13'
              onChange={handleChange('TLSv13')}
                color="primary"
                  />
                TLSv1.3
                </Typography>
              </Grid>
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <TextField
                    id="outlined-dense"
                    label="Temp Receive Directory"
                    className="partnertextField"
                    margin="dense"
                    variant="outlined"
                    //placeholder=""
                    onChange={handleChange('tempReceiveDirectory')}
                    value={values.tempReceiveDirectory}
                    name="tempReceiveDirectory"
                  />
                  {errorInfo.tempReceiveDirectory === true ? (
                    <div id="nameid" className="addfieldserror">
                      "Enter Alphanumeric characters "
                    </div>
                  ) : (
                    ''
                  )}
                  {submitErr && !values.tempReceiveDirectory ? (
                    <div id="nameid" className="addfieldserror">
                      Please enter Temp Receive Directory
                    </div>
                  ) : (
                    ''
                  )}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="Advancedtext">
                    <Checkbox
                     inputProps={{
                  'aria-label': 'uncontrolled-checkbox',
                   }}
                   value={values.logMessages}
                  checked={values.logMessages}
                name='logMessages'
              onChange={handleChange('logMessages')}
                color="primary"
                  />
                Log Messages
                </Typography>
              </Grid>
              <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <TextField
                    id="outlined-dense"
                    label="Send Filter"
                    className="partnertextField"
                    margin="dense"
                    variant="outlined"
                    //placeholder=""
                    onChange={handleChange('sendFilter')}
                    value={values.sendFilter}
                    name="sendFilter"
                  />
                  {errorInfo.sendFilter === true ? (
                    <div id="nameid" className="addfieldserror">
                      "Enter Alphanumeric characters "
                    </div>
                  ) : (
                    ''
                  )}
                  {submitErr && !values.sendFilter ? (
                    <div id="nameid" className="addfieldserror">
                      Please enter Send Filter
                    </div>
                  ) : (
                    ''
                  )}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                     <Typography  className="Advancedtext">
                    <Checkbox
                     inputProps={{
                  'aria-label': 'uncontrolled-checkbox',
                   }}
                   value={values.savetoSentFolder}
                  checked={values.savetoSentFolder}
                name='savetoSentFolder'
              onChange={handleChange('savetoSentFolder')}
                color="primary"
                  />
                  Save to Sent Folder
                </Typography>
              </Grid>
           <Grid container spacing={2}>
           <Grid  item xs={12} sm={6}>
        <Button
          style={{
            color: '#000006',
            backgroundColor: '#E5CCFF',
          }}
          onClick={handleReset}
          variant="contained"
          fullWidth="true"
          className="createpartnerbutton"
        >
          reset
        </Button></Grid>
           <Grid  item xs={12} sm={6}>
        <Button
          onClick={handleSubmit}
          variant="contained"
          fullWidth="true"
          className="createpartnerbutton"
        >
          Create
        </Button>
        </Grid>
       
        </Grid> 
        </Grid>

        </Grid>
      </form>
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center'
        }}
        autoHideDuration={5000}
        open={openSnackbar}
        onClose={handleCloseSnackbar}
      >
        <MySnackbarContentWrapper
          onClose={handleCloseSnackbar}
          variant={variant}
          message={message}
        />
      </Snackbar>
      <UploadCertificateModel
        handleCloseModal={handleCloseForm}
        isModalOpen={openModal}
      >
        <UploadCertificateForm
          handleCloseModal={handleCloseForm}
         // onSuccess={handleRefetch}
          isModalOpen={openModal}
        />
      </UploadCertificateModel>
    </div>

   
    </MuiThemeProvider>
  );
}

