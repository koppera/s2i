import React, { useEffect, useContext, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { Grid, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { withStyles } from '@material-ui/core/styles';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import Modal from '@material-ui/core/Modal';
import '../../css/commonStyle.css';
import Theme from '../../css/theme';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(2),
    },
  },

  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));
const styles = (theme) => ({
  root: {
    Width: 1200,
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: '#FFFFFF',
  },

});

export default function ContactView(props) {



  const classes = useStyles();
  const [open, setOpen] = React.useState(true);
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSvgIcon: {
          root: {
            color: '#FFFFFF',
          },
        },
        MuiGrid: {
          container: {
            paddingLeft: '1.5%',
            paddingBottom: '1%',
            overflowX: 'hidden'

          },
          item: {
            fontSize: '10px',
          }
        },
        MuiDialogTitle: {
          root: {
            backgroundColor: '#0b153e',
            color: '#FFFFFF',
          },
        },
      },
    });
  const handleModalClose = () => {
    props.handleCloseModal(false);
  };

  return (
    <div>

      <MuiThemeProvider theme={getMuiTheme()}>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={props.isModalOpen}
          onClose={handleModalClose}
          className="fabmodal"
        >
          <div className="modalpaper">
            <Grid container className="header">
              <Grid item xs={10} sm={10}>
                <h2 id="modal-title" className="h2">
                  X12 Connector View
                </h2>
              </Grid>
              <Grid item xs={2} sm={2} className="close">
                <CloseIcon onClick={handleModalClose} />
              </Grid>
            </Grid>
            <Grid container className="pdashboardroot">
              <Grid container spacing={3}>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader"> Work Space</Typography>
                  {props.rowData.workspaceUUID}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader"> Connector ID</Typography>
                  {props.rowData.connectorId}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader"> Connector Description</Typography>
                  {props.rowData.connectorDescription}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader"> Transaction Type</Typography>
                  {props.rowData.transactionType}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader"> Sender Id Qualifier</Typography>
                  {props.rowData.senderIdQualifier}
                </Grid>
              </Grid>
              <Grid container spacing={3}>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Sender Identifier</Typography>
                  {props.rowData.profilesenderIdentifier}

                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Receiver Id Qualifier </Typography>
                  {props.rowData.receiverIdQualifier}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Receiver Identifier</Typography>
                  {props.rowData.profilereceiverIdentifier}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Control Version Number </Typography>
                  {props.rowData.controlVersionNumber}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Usage Indicator </Typography>
                  {props.rowData.usageIndicator}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Functional Group </Typography>
                  {props.rowData.functionalGroup}

                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Ta1 Acknowledgement </Typography>
                  {props.rowData.ta1Acknowledgement}

                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Functional ACk </Typography>
                  {props.rowData.functionalACK}

                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Functional ACk Type </Typography>
                  {props.rowData.functionalACKType}

                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Send </Typography>
                  {props.rowData.send}

                </Grid>

                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Resend Interval </Typography>
                  {props.rowData.resendInterval}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Resend Interval Maximum Attachments </Typography>
                  {props.rowData.resendIntervalMaximumAttachments}

                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Data Element Separator </Typography>
                  {props.rowData.dataElementSeparator}

                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Component Element Separator</Typography>
                  {props.rowData.componentElementSeparator}

                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Segment Terminator</Typography>
                  {props.rowData.segmentTerminator}

                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Suffix </Typography>
                  {props.rowData.suffix}

                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Authorization Qualifier </Typography>
                  {props.rowData.authorizationQualifier}

                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Authorization Id </Typography>
                  {props.rowData.authorizationId}

                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Security Qualifier</Typography>
                  {props.rowData.securityQualifier}

                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Security Password </Typography>
                  {props.rowData.securityPassword}

                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Control Standards Identifier </Typography>
                  {props.rowData.controlStandardsIdentifier}

                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Sender Identifier </Typography>
                  {props.rowData.senderIdentifier}
                </Grid>

                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Receiver Identifier </Typography>
                  {props.rowData.receiverIdentifier}
                </Grid>

                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Date Format </Typography>
                  {props.rowData.dateFormat}

                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Time Format </Typography>
                  {props.rowData.timeFormat}

                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Responsible Agency Code: </Typography>
                  {props.rowData.responsibleAgencyCode}

                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Identifier Code </Typography>
                  {props.rowData.identifierCode}

                </Grid>

                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Batch Transactions </Typography>
                  {props.rowData.batchTransactions}
                </Grid>

                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">User Reference ID</Typography>
                  {props.rowData.useReferenceId}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Encoding </Typography>
                  {props.rowData.encoding}

                </Grid>

                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Expand Qualifier Values </Typography>
                  {props.rowData.expandQualifierValues}

                </Grid>

                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Functional Acks</Typography>
                  {props.rowData.othersettingsFuntionalACKS}

                </Grid>

                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Generate Description As </Typography>
                  {props.rowData.generateDescriptionAs}

                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Hippa Schemas </Typography>
                  {props.rowData.hippaSchemas}

                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Hippa 278 </Typography>{' '}
                  {props.rowData.hippa278}

                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Nest Loops </Typography>{' '}
                  {props.rowData.nestLoops}

                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Parent Connector </Typography>{' '}
                  {props.rowData.parentConnector}

                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Processing Delay</Typography>{' '}
                  {props.rowData.processingDelay}

                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Snip Validation </Typography>{' '}
                  {props.rowData.snipValidation}

                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Strict Schema Validation</Typography>{' '}
                  {props.rowData.strictSchemaValidation}

                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Local File Scheme </Typography>{' '}
                  {props.rowData.localFileScheme}

                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Log Messages </Typography>{' '}
                  {props.rowData.logMessages}

                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Send Filter </Typography>{' '}
                  {props.rowData.sendFilter}

                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Save To Sent Folder</Typography>{' '}
                  {props.rowData.saveToSentFolder}

                </Grid>

                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Created On</Typography>{' '}
                  {props.rowData.createdOn}

                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Updated On</Typography>{' '}
                  {props.rowData.updatedOn}

                </Grid>
              </Grid>
            </Grid>
          </div>
        </Modal>
      </MuiThemeProvider>
    </div>
  );
}
