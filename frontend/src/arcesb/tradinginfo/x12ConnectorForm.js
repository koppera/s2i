import React, { useEffect, useState, useContext, useCallback } from 'react';
import { useMutation, useQuery } from 'graphql-hooks';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MySnackbarContentWrapper from '../../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import '../../css/commonStyle.css';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import Theme from '../../css/theme';
import Modal from '@material-ui/core/Modal';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import Checkbox from '@material-ui/core/Checkbox';
import FormGroup from '@material-ui/core/FormGroup';
import { Typography, Paper } from '@material-ui/core';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

//sam
const ADD_X12CONNECTOR = `mutation createX12Connector($X12ConnectorInput:X12ConnectorInput){
  createX12Connector(input:$X12ConnectorInput) {
    idX12Connector
    X12ConnectorUUID
    connectorId
    connectorDescription
    transactionType
    profilesenderIdentifier
    senderIdQualifier
    receiverIdQualifier
    profilereceiverIdentifier
    controlVersionNumber
    usageIndicator
    functionalGroup
    ta1Acknowledgement
    functionalACKType
    send
    resendInterval
    resendIntervalMaximumAttachments
    dataElementSeparator
    componentElementSeparator
    segmentTerminator
    suffix
    authorizationQualifier
    authorizationId
    securityQualifier
    securityPassword
    controlStandardsIdentifier
    senderIdentifier
    receiverIdentifier
    dateFormat
    timeFormat
    responsibleAgencyCode
    identifierCode
    encoding
    generateDescriptionAs
    hippaSchemas
    nestLoops
    parentConnector
    processingDelay
    snipValidation
    strictSchemaValidation
    localFileScheme
    sendFilter
    saveToSentFolder
    createdOn
    updatedOn
    hippa278
    logMessages
    batchTransactions
    functionalACK
    useReferenceId
    expandQualifierValues
    workspaceUUID
    othersettingsFuntionalACKS
 
  }
}
`;

const UPDATE_X12Connector = `
mutation updateX12Connector($X12ConnectorInput:X12ConnectorInput){
  updateX12Connector(input:$X12ConnectorInput) {
    idX12Connector
    connectorId
  } 
}`;

const DISPLAY_Workspace = `{
  allWorkspaceByStatus {
    workspaceUUID
    workspaceName
    senderQualifier
    APIStatus
    createdOn
    updatedOn
  }
}`;

const DISPLAY_All_DOCUMENTTYPE = `query fetchDocumentTypeArcesb($searchInput:String){
  fetchDocumentTypeArcesb(searchInput:$searchInput) {
    idDocumentType
    documentTypeUUID
    name
    description
    docType
    APIStatus
    createdOn
    lastUpdatedOn
  }
}
`;


export default function X12Connector(props) {
  console.log(props);
  const [searchInput, setSearchValue] = React.useState('');
  const [doctype, setDoctype] = React.useState([]);


  //Fetch ALL DocumentType
  const { loading, errors, data: alldoctypes, refetch } = useQuery(
    DISPLAY_All_DOCUMENTTYPE,
    {
      variables: {
        searchInput: searchInput,
      },
    }
  );

  console.log(doctype);
  console.log(alldoctypes);


  const {
    loading: wloading,
    data: wdata,
    errors: werrors,
    networkError: wnetworkError,
  } = useQuery(DISPLAY_Workspace);


  const [
    updateX12Connector,
    {
      loading: uploading,
      data: updata,
      errors: uperrors,
      networkError: unetworkError,
    },
  ] = useMutation(UPDATE_X12Connector);

  const [workspaceData, setWorksapceData] = React.useState([]);
  const [WorkspaceName, setWorkspaceName] = React.useState('');

  const [connectorId, setConnectorId] = React.useState('')
  const [apiStatusRead, setApiStatusRead] = useState(false)




  const [values, setValues] = React.useState({
    workspaceUUID: '',
    workspaceName: '',
    documentType: '',
    connectorId: '',
    connectorDescription: '',
    transactionType: 'X12 to XML',
    senderIdQualifier: 'ZZ-Mutually Defined[X12]',
    profilesenderIdentifier: '',
    receiverIdQualifier: 'ZZ-Mutually Defined[X12]',
    profilereceiverIdentifier: '',
    controlVersionNumber: '00401-Standards Approved for Publication by ASC X12 Procedures Review Board through October 1997',
    usageIndicator: 'T-Test Data',
    functionalGroup: 'Apply these identifiers to the Functional Group',
    ta1Acknowledgement: 'Automatic',

    functionalACKType: '997',
    send: 'Send',
    resendInterval: '1440',
    resendIntervalMaximumAttachments: '5',
    dataElementSeparator: '*',
    componentElementSeparator: ':',
    segmentTerminator: '-',
    suffix: 'CRLF',
    authorizationQualifier: '00-No Automation Information Present',
    authorizationId: '',
    securityQualifier: '00-No Security Information Present',
    securityPassword: '',
    controlStandardsIdentifier: 'U-U.S.EDI Community of ASC,X12,TDCC,and UCS',
    senderIdentifier: '',
    receiverIdentifier: '',
    dateFormat: 'CCYYMMDD',
    timeFormat: 'HHMM',
    responsibleAgencyCode: 'T-Transportation data Committee X12',
    identifierCode: '004010-Standards Approved for Publication by ASC X12 Procedures Review Board through October 1997',
    encoding: 'utf-8',
    generateDescriptionAs: 'XML Comment',
    hippaSchemas: null,

    nestLoops: null,
    parentConnector: '',
    processingDelay: '0',
    snipValidation: null,
    strictSchemaValidation: 'Ignore',
    localFileScheme: '',

    sendFilter: '',
    saveToSentFolder: 'Save to Sent folder',
    othersettingsFuntionalACKS: null,
    // expandQualifierValue: null,
    batchTransactions: null,
    hippa278: 'Treat HIPAA 278 as a request',
    logMessages: 'Log messages',
    functionalACK: null,
    useReferenceId: null,
    expandQualifierValues: null,
  });

  const usageIndicator = [
    'Not Specified',
    'I-Information',
    'P-Production Data',
    'T-Test Data',
  ];
  const TA1Acknowledgement = ['Automatic', 'Always', 'Never'];

  const functionalACKType = ['997', '999'];
  const senderIdQualifier = [
    '01-D-U-N-S [Dun&Bradstreet]',
    '02-SCAC [Standard Carrier Alpha Code][X12]',
    '03-Federal Maritime Commission',
    '04-IATA [International Air Transport Association]',
    '07-Global Location Number [GLN]',
    '08-UCC Communications ID [Uniform Code Council Communications]',
    '09-X.121 [CCITT]',
    '10-Department of Defense',
    '11-Drug Enforcement Administration',
    '12-Telephone Number',
    '13-UCS Code',
    '14-Duns Plus Suffix',
    '15-Petroleum Accountants Society of Canada Company Code',
    '16-D&B D-U-N-S Number Plus 4-character suffix',
    '17-American Bankers Association',
    '18-Association of American Flailroads[AAR] Standard Distribution Code',
    '19-EDI Council of Australia [EDICA] Communications ID Number [COMM ID]',
    '20-Health Industry Number',
    '21-Integrated Postsecondary Education Data System,or [IPEDS]',
    '22-Federal Integracy Commission on Education, or FICE',
    '23-National Center for Education Statistics Common Core of Data 12-Digit Number for Pre-K-Grade 12 Institutes, or NCES',
    '24-The College Boards Admission Testing Program 4-Digit Code of Postsecondary Institutes,or ATP',
    '25-ACT,Inc,4-Digit Code of Postsecondary Institutions',
    '26-Statistics of Canada List of Postsecondary Institutions',
    '27-Health Care Financing Administration Carrier ID',
    '28-Health Care Financing Administration Fiscal Intermediary',
    '29-Health Care Financing Administration Medicare Provider',
    '30-U.S.Federal Tax Identification Number',
    '31-Jurisdiction Identification Number Plus 4 as assigned by the International Association of Industrial Accident Boards and Commissions[IAIABC]',
    '32-U.S. Federal Employer identification Number',
    '33-National Association of Insurance Commissioners Company Code[NAIC]',
    '34-Medicaid Provider and Supplier Identification Number as assigned by individual State Medicaid Agencies in coniunction with Health Care Financing Administration',
    '35-Statistics Canada Canadian College Student Information System Institution Codes',
    '36-Statistics Canada University Student Information System Institution Codes',
    '37-Society of Property Information Compliers and Analysts',
    '38-The College Board and ACT, Inc,S-Digit Code List of Secondary Institutions',
    'AM-Association Merricana del Codigo de Producto[AMECOP] Communication ID',
    'BT-BizTalk EDI Identity[Deprecated]',
    'NR-National Retail Merchants Association',
    'SA-User Identification Number as assigned by the Safety and Fitness Electronic Records[SAFER] System',
    'SN-Standard Address Number',
    'ZZ-Mutually Defined[X12]',
  ];
  const controlVersion = [
    '00200-ASC X12 Standards Issued by ANSI in 1987',
    '00201-Draft Standards for Trail Use Approved for Publication by ASC X12 Procedures Review Board through October 1988',
    '00204-Draft Standards for Trail Use Approved for Publication by ASC X12 Procedures Review Board through October 1989',
    '00300-ASC X12 Standards Issued by ANSI in 1992',
    '00301-Draft Standards Approved for Publication by ASC X12 Procedures Review Board through October 1990',
    '00302-Draft Standards Approved for Publication by ASC X12 Procedures Review Board through October 1991',
    '00309-Draft Standards Approved for Publication by ASC X12 Procedures Review Board through October 1992',
    '00304-Draft Standards Approved for Publication by ASC X12 Procedures Review Board through October 1999',
    '00305-Draft Standards Approved for Publication by ASC X12 Procedures Review Board through October 1994',
    '00300-Draft Standards Approved for Publication by ASC X12 Procedures Review Board through October 1995',
    '00307-Draft Standards Approved for Publication by ASC X12 Procedures Review Board through October 1990',
    '00400-ASC X12 Standards Issued by ANSI in 1997',
    '00401-Standards Approved for Publication by ASC X12 Procedures Review Board through October 1997',
    '00402-Standards Approved for Publication by ASC X12 Procedures Review Board through October 1998',
    '00403-Standards Approved for Publication by ASC X12 Procedures Review Board through October 1999',
    '00404-Standards Approved for Publication by ASC X12 Procedures Review Board through October 2000',
    '00405-Standards Approved for Publication by ASC X12 Procedures Review Board through October 2001',
    '00406-Standards Approved for Publication by ASC X12 Procedures Review Board through October 2002',
    '00500-ASC X12 Standards Issued by ANSI in 2003',
    '00300-Draft Standards for Trail Use Approved for Publication by ASC X12 Procedures Review Board through October 1995',
    '00307-Draft Standards for Trail Use Approved for Publication by ASC X12 Procedures Review Board through October 1990',
    '00400-ASC X12 Standards Issued by ANSI in 1997',
    '00401-Standards Approved for Publication by ASC X12 Procedures Review Board through October 1997',
    '00402-Standards Approved for Publication by ASC X12 Procedures Review Board through October 1998',
    '00403-Standards Approved for Publication by ASC X12 Procedures Review Board through October 1999',
    '00404-Standards Approved for Publication by ASC X12 Procedures Review Board through October 2000',
    '00405-Standards Approved for Publication by ASC X12 Procedures Review Board through October 2001',
    '00406-Standards Approved for Publication by ASC X12 Procedures Review Board through October 2002',
    '00500-ASC X12 Standards Issued by ANSI in 2003',
    '00501-Standards Approved for Publication by ASC X12 Procedures Review Board through October 2003',
    '00502-Standards Approved for Publication by ASC X12 Procedures Review Board through October 2004',
    '00509-Standards Approved for Publication by ASC X12 Procedures Review Board through October 2005',
    '00504-Standards Approved for Publication by ASC X12 Procedures Review Board through October 2000',
    '00505-Standards Approved for Publication by ASC X12 Procedures Review Board through October 2007',
    '00601-Standards Approved for Publication by ASC X12 Procedures Review Board through October 2008',
    '00602-Standards Approved for Publication by ASC X12 Procedures Review Board through October 2009',
    '00603-Standards Approved for Publication by ASC X12 Procedures Review Board through October 2010',
    '00604-Standards Approved for Publication by ASC X12 Procedures Review Board through October 2011',
    '00605-Standards Approved for Publication by ASC X12 Procedures Review Board through October 2012',
  ];

  const identifierCode = [
    '001000-ASC X12 Standards Approved by ANSI in 1983',
    '002000-ASC X12 Standards Approved by ANSI in 1986',
    ' 002001-Draft Standars Approved by ASC X12 in November 1987',
    '002002-Draft Standars Approved by ASC X12 through February 1988',
    '002003-Draft Standars Approved by ASC X12 through August 1988',
    '002031-Draft Standars Approved by ASC X12 through February 1989',
    '002040-Draft Standars Approved by ASC X12 through May 1989',
    '002041-Draft Standars Approved by ASC X12 through October 1989',
    '002042-Draft Standars Approved by ASC X12 through February 1990',
    ' 003000-ASC X12 Standards Approved by ANSI in 1992',
    '003010-Draft Standars Approved by ASC X12 through June 1990',
    '003011-Draft Standars Approved for Publication by ASC X12 Procedures Review Board through February 1991',
    '003012-Draft Standars Approved for Publication by ASC X12 Procedures Review Board through June 1991',
    '003020-Draft Standars Approved for Publication by ASC X12 Procedures Review Board through October 1991',
    '003021-Draft Standars Approved for Publication by ASC X12 Procedures Review Board through February 1992',
    ' 003022-Draft Standars Approved for Publication by ASC X12 Procedures Review Board through June 1992',
    '003030-Draft Standars Approved for Publication by ASC X12 Procedures Review Board through October 1992',
    '003031-Draft Standars Approved for Publication by ASC X12 Procedures Review Board through February 1993',
    '003032-Draft Standars Approved for Publication by ASC X12 Procedures Review Board through June 1993',
    '003040-Draft Standars Approved for Publication by ASC X12 Procedures Review Board through October 1993',
    '003041-Draft Standars Approved for Publication by ASC X12 Procedures Review Board through February 1994',
    '003042-Draft Standars Approved for Publication by ASC X12 Procedures Review Board through June 1994',
    '003050-Draft Standars Approved for Publication by ASC X12 Procedures Review Board through October 1994',
    '003051-Draft Standars Approved for Publication by ASC X12 Procedures Review Board through February 1995',
    '003052-Draft Standars Approved for Publication by ASC X12 Procedures Review Board through June 1995',
    '003060-Draft Standars Approved for Publication by ASC X12 Procedures Review Board through October 1995',
    '003061-Draft Standars Approved for Publication by ASC X12 Procedures Review Board through February 1996',
    '003062-Draft Standars Approved for Publication by ASC X12 Procedures Review Board through June 1996',
    '003070-Draft Standars Approved for Publication by ASC X12 Procedures Review Board through October 1996',
    '003071-Draft Standars Approved for Publication by ASC X12 Procedures Review Board through February 1997',
    '003072-Draft Standars Approved for Publication by ASC X12 Procedures Review Board through June 1996',
    '004000-ASC X12 Standards Approved by ANSI in 1997',
    '004010-Standards Approved for Publication by ASC X12 Procedures Review Board through October 1997',
    '004011-Standards Approved for Publication by ASC X12 Procedures Review Board through February 1998',
    '004012-Standards Approved for Publication by ASC X12 Procedures Review Board through June 1998',
    '004020-Standards Approved for Publication by ASC X12 Procedures Review Board through October 1998',
    '004021-Standards Approved for Publication by ASC X12 Procedures Review Board through February 1999',
    '004022-Standards Approved for Publication by ASC X12 Procedures Review Board through June 1999',
    '004030-Standards Approved for Publication by ASC X12 Procedures Review Board through October 1999',
    '004031-Standards Approved for Publication by ASC X12 Procedures Review Board through February 2000',
    '004032-Standards Approved for Publication by ASC X12 Procedures Review Board through June 2000',
    '004040-Standards Approved for Publication by ASC X12 Procedures Review Board through October 2000',
    '004041-Standards Approved for Publication by ASC X12 Procedures Review Board through February 2001',
    '004042-Standards Approved for Publication by ASC X12 Procedures Review Board through June 2001',
    '005040-Standards Approved for Publication by ASC X12 Procedures Review Board through October 2006',
    '005041-Standards Approved for Publication by ASC X12 Procedures Review Board through February 2007',
    '005042-Standards Approved for Publication by ASC X12 Procedures Review Board through June 2007',
    '005050-Standards Approved for Publication by Procedures Review Board through October 2007',
    '005052-Standards Approved for Publication by Procedures Review Board through June 2008',
    '006000-ASC X12 Standards Approved by ANSI in 2008',
    '006010-Standards Approved for Publication by Procedures Review Board through October 2008',
    '006011-Standards Approved for Publication by Procedures Review Board through February 2009',
    '006012-Standards Approved for Publication by Procedures Review Board through June 2009',
    '006020-Standards Approved for Publication by Procedures Review Board through October 2009',
    '006021-Standards Approved for Publication by Procedures Review Board through February 2010',
    '006022-Standards Approved for Publication by Procedures Review Board through June 2010',
    '006030-Standards Approved for Publication by Procedures Review Board through October 2010',
    '006031-Standards Approved for Publication by Procedures Review Board through February 2011',
    '006032-Standards Approved for Publication by Procedures Review Board through June 2011',
    '006040-Standards Approved for Publication by Procedures Review Board through October 2011',
    '006041-Standards Approved for Publication by Procedures Review Board through February 2012',
    '006042-Standards Approved for Publication by Procedures Review Board through June 2012',
    '006050-Standards Approved for Publication by Procedures Review Board through October 2012',

  ];
  const [ErrMsgNameInfo, setErrMsgNameInfo] = React.useState(false);
  const [ErrMsg, setErrMsg] = React.useState(false);
  const [submitErr, setSubmitErr] = React.useState(false);
  const [errorInfo, setErrorInfo] = React.useState({
    workspaceUUID: false,
    connectorId: false,
    documentType: false,
    connectorDescription: false,
    resendInterval: false,
    resendIntervalMaximumAttachments: false,
    profilesenderIdentifier: false,
    profilereceiverIdentifier: false,
    senderIdQualifier: false,
    receiverIdQualifier: false,
    controlVersionNumber: false,
    usageIndicator: false,
    ta1Acknowledgement: false,
    functionalACKType: false,
    resendInterval: false,
    resendIntervalMaximumAttachments: false,
    dataElementSeparator: false,
    componentElementSeparator: false,
    segmentTerminator: false,
    suffix: false,
    authorizationQualifier: false,
    authorizationId: false,
    securityQualifier: false,
    securityPassword: false,
    controlStandardsIdentifier: false,
    senderIdentifier: false,
    receiverIdentifier: false,
    dateFormat: false,
    timeFormat: false,
    responsibleAgencyCode: false,
    identifierCode: false,
    encoding: false,
    generateDescriptionAs: false,
    parentConnector: false,
    processingDelay: false,
    strictSchemaValidation: false,
    localFileScheme: false,
    sendFilter: false,
    batchTransactions: false,
    useReferenceId: false,
    expandQualifierValues: false,
    othersettingsFuntionalACKS: false,



  });
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [variant, setSnackbarVariant] = React.useState('error');
  const [message, setSnackbarMessage] = React.useState();

  const [addX12Connector, addloading, adddata] = useMutation(ADD_X12CONNECTOR);


  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }

  const handleCloseModal = () => {
    props.handleCloseModal(false);
  };
  const symbolexp = /^[.*~]{1}$/;

  useEffect(() => {
    console.log(props.rowData);

    if (props.rowData != undefined && props.title != 'Add') {

      setValues({
        workspaceUUID: props.rowData.workspaceUUID,

        connectorId: props.rowData.connectorId,
        connectorDescription: props.rowData.connectorDescription,
        transactionType: props.rowData.transactionType,
        senderIdQualifier: props.rowData.senderIdQualifier,
        profilereceiverIdentifier: props.rowData.profilereceiverIdentifier,
        receiverIdQualifier: props.rowData.receiverIdQualifier,
        receiverIdentifier: props.rowData.receiverIdentifier,
        controlVersionNumber: props.rowData.controlVersionNumber,
        usageIndicator: props.rowData.usageIndicator,
        profilesenderIdentifier: props.rowData.profilesenderIdentifier,
        ta1Acknowledgement: props.rowData.ta1Acknowledgement,
        functionalACKType: props.rowData.functionalACKType,
        send: props.rowData.send,
        nestLoops: props.rowData.nestLoops,
        functionalGroup: props.rowData.functionalGroup,
        resendInterval: props.rowData.resendInterval,
        resendIntervalMaximumAttachments: props.rowData.resendIntervalMaximumAttachments,
        dataElementSeparator: props.rowData.dataElementSeparator,
        componentElementSeparator: props.rowData.componentElementSeparator,
        segmentTerminator: props.rowData.segmentTerminator,
        suffix: props.rowData.suffix,
        authorizationQualifier: props.rowData.authorizationQualifier,
        authorizationId: props.rowData.authorizationId,
        securityQualifier: props.rowData.securityQualifier,
        securityPassword: props.rowData.securityPassword,
        controlStandardsIdentifier: props.rowData.controlStandardsIdentifier,
        senderIdentifier: props.rowData.senderIdentifier,
        receiverIdentifier: props.rowData.receiverIdentifier,
        dateFormat: props.rowData.dateFormat,
        timeFormat: props.rowData.timeFormat,
        responsibleAgencyCode: props.rowData.responsibleAgencyCode,
        identifierCode: props.rowData.identifierCode,
        encoding: props.rowData.encoding,
        generateDescriptionAs: props.rowData.generateDescriptionAs,
        hippaSchemas: props.rowData.hippaSchemas,
        hippa278: props.rowData.hippa278,
        logMessages: props.rowData.logMessages,
        batchTransactions: props.rowData.batchTransactions,
        parentConnector: props.rowData.parentConnector,
        processingDelay: props.rowData.processingDelay,
        snipValidation: props.rowData.snipValidation,
        strictSchemaValidation: props.rowData.strictSchemaValidation,
        localFileScheme: props.rowData.localFileScheme,
        sendFilter: props.rowData.sendFilter,
        saveToSentFolder: props.rowData.saveToSentFolder,
        useReferenceId: props.rowData.useReferenceId,
        functionalACK: props.rowData.functionalACK,
        expandQualifierValues: props.rowData.expandQualifierValues,
        othersettingsFuntionalACKS: props.rowData.othersettingsFuntionalACKS,
      })
      if (props.rowData.APIStatus == 'Active') {
        setApiStatusRead(true)
      }
    }

    if (wdata !== undefined) {

      setWorksapceData(wdata.allWorkspaceByStatus);

    }
    if (alldoctypes !== undefined) {

      setDoctype(alldoctypes.fetchDocumentTypeArcesb);

    }

  }, [props, wdata, alldoctypes]);



  const handleChange = (name) => (event) => {
    console.log(event);
    console.log(event.target.value);
    console.log(name);

    setErrMsg(false);
    setValues({ ...values, [name]: event.target.value });
    switch (name) {
      case 'connectorId':
      case 'authorizationId':
      case 'profilesenderIdentifier':
      case 'profilereceiverIdentifier':
      case 'senderIdentifier':
      case 'receiverIdentifier':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[A-Z0-9]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          // setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'resendInterval':
      case 'resendIntervalMaximumAttachments':
      case 'processingDelay':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[0-9]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'connectorDescription':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'transactionType':
      case 'senderIdQualifier':
      case 'receiverIdQualifier':
      case 'controlVersionNumber':
      case 'usageIndicator':
      case 'workspaceUUID':
      case 'documentType':
      case 'suffix':
      case 'authorizationQualifier':
      case 'securityQualifier':
      case 'controlStandardsIdentifier':
      case 'generateDescriptionAs':
        setValues({
          ...values,
          [event.target.name]: event.target.value,
        });
        break;

      case 'dataElementSeparator':
      case 'componentElementSeparator':
      case 'segmentTerminator':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match(symbolexp)) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'functionalGroup':
      case 'functionalACK':
      case 'send':
      case 'hippaSchemas':
      case 'hippa278':
      case 'nestLoops':
      case 'snipValidation':
      case 'logMessages':
      case 'saveToSentFolder':
      case 'expandQualifierValues':
      case 'batchTransactions':
      case 'othersettingsFuntionalACKS':
      case 'useReferenceId':
        if (event.target.checked) {
          setValues({ ...values, [name]: event.target.name });
        } else {
          setValues({ ...values, [name]: null });
        }
        break;
      case 'securityPassword':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match(
            '^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,15}$'
          )
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'encoding':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[A-Za-z0-9-]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'parentConnector':
      case 'localFileScheme':
      case 'sendFilter':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[A-Za-z0-9]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;

      default:
        return false;
    }
  };



  const handleSubmit = async () => {
    console.log(values);
    if (
      values.workspaceUUID !== '' &&
      values.documentType !== '' &&
      // values.connectorId !== '' &&

      // values.authorizationId !== '' &&
      //values.authorizationQualifier !== '' &&
      // values.componentElementSeparator !== '' &&
      // values.connectorDescription !== '' &&
      // values.controlStandardsIdentifier !== '' &&
      // values.controlVersionNumber !== '' &&
      // values.dataElementSeparator !== '' &&
      // values.dateFormat !== '' &&
      // values.encoding !== '' &&
      // values.functionalACKType !== '' &&
      // values.generateDescriptionAs !== '' &&
      // values.identifierCode !== '' &&
      // values.localFileScheme !== '' &&
      // values.parentConnector !== '' &&
      // values.processingDelay !== '' &&
      values.receiverIdQualifier !== '' &&
      values.profilereceiverIdentifier !== '' &&
      // values.receiverIdentifier !== '' &&
      //values.resendInterval !== '' &&
      //values.resendIntervalMaximumAttachments !== '' &&
      // values.responsibleAgencyCode !== '' &&
      //values.securityPassword !== '' &&
      //values.securityQualifier !== '' &&
      //values.segmentTerminator !== '' &&
      //values.sendFilter !== '' &&
      values.senderIdQualifier !== '' &&
      values.profilesenderIdentifier !== '' &&
      //values.senderIdentifier !== '' &&
      //values.strictSchemaValidation !== '' &&
      //  values.suffix !== '' &&
      // values.ta1Acknowledgement !== '' &&
      // values.timeFormat !== '' &&
      // values.transactionType !== '' &&
      //  values.usageIndicator !== '' && 


      errorInfo.workspaceUUID !== true &&
      errorInfo.authorizationId !== true &&
      errorInfo.authorizationQualifier !== true &&
      errorInfo.componentElementSeparator !== true &&
      errorInfo.connectorId !== true &&
      errorInfo.controlStandardsIdentifier !== true &&
      errorInfo.controlVersionNumber !== true &&
      errorInfo.dataElementSeparator !== true &&
      errorInfo.dateFormat !== true &&
      errorInfo.encoding !== true &&
      errorInfo.functionalACKType !== true &&
      errorInfo.generateDescriptionAs !== true &&
      errorInfo.identifierCode !== true &&
      errorInfo.localFileScheme !== true &&
      errorInfo.parentConnector !== true &&
      errorInfo.processingDelay !== true &&
      errorInfo.receiverIdQualifier !== true &&
      errorInfo.profilereceiverIdentifier !== true &&
      errorInfo.receiverIdentifier !== true &&
      errorInfo.resendInterval !== true &&
      errorInfo.resendIntervalMaximumAttachments !== true &&
      errorInfo.responsibleAgencyCode !== true &&
      errorInfo.securityPassword !== true &&
      errorInfo.securityQualifier !== true &&
      errorInfo.segmentTerminator !== true &&
      errorInfo.sendFilter !== true &&
      errorInfo.senderIdQualifier !== true &&
      errorInfo.profilesenderIdentifier !== true &&
      errorInfo.senderIdentifier !== true &&
      errorInfo.strictSchemaValidation !== true &&
      errorInfo.suffix !== true &&
      errorInfo.ta1Acknowledgement !== true &&
      errorInfo.timeFormat !== true &&
      errorInfo.transactionType !== true &&
      errorInfo.usageIndicator !== true
    ) {
      console.log('TRUE COND');
      setErrMsg(false);



      setValues(
        {
          ...values,
          workspaceUUID: values.workspaceUUID,

          connectorId: values.connectorId,
          connectorDescription: values.connectorDescription,
          transactionType: values.transactionType,

          senderIdQualifier: values.senderIdQualifier,
          profilereceiverIdentifier: values.profilereceiverIdentifier,
          receiverIdQualifier: values.receiverIdQualifier,
          receiverIdentifier: values.receiverIdentifier,
          controlVersionNumber: values.controlVersionNumber,
          usageIndicator: values.usageIndicator,

          functionalGroup: values.functionalGroup,
          ta1Acknowledgement: values.ta1Acknowledgement,

          functionalACKType: values.functionalACKType,

          send: values.send,
          resendInterval: values.resendInterval,
          resendIntervalMaximumAttachments: values.resendIntervalMaximumAttachments,
          dataElementSeparator: values.dataElementSeparator,
          componentElementSeparator: values.componentElementSeparator,
          segmentTerminator: values.segmentTerminator,
          suffix: values.suffix,

          authorizationQualifier: values.authorizationQualifier,
          authorizationId: values.authorizationId,
          securityQualifier: values.securityQualifier,
          securityPassword: values.securityPassword,
          controlStandardsIdentifier: values.controlStandardsIdentifier,

          senderIdentifier: values.senderIdentifier,
          receiverIdentifier: values.receiverIdentifier,
          dateFormat: values.dateFormat,
          timeFormat: values.timeFormat,
          responsibleAgencyCode: values.responsibleAgencyCode,
          identifierCode: values.identifierCode,
          nestLoops: values.nestLoops,
          encoding: values.encoding,
          generateDescriptionAs: values.generateDescriptionAs,
          hippaSchemas: values.hippaSchemas,

          parentConnector: values.parentConnector,
          processingDelay: values.processingDelay,
          snipValidation: values.snipValidation,
          strictSchemaValidation: values.strictSchemaValidation,
          saveToSentFolder: values.saveToSentFolder,
          localFileScheme: values.localFileScheme,
          sendFilter: values.sendFilter,
          hippa278: values.hippa278,
          logMessages: values.logMessages,
          batchTransactions: values.batchTransactions,
          useReferenceId: values.useReferenceId,
          functionalACK: values.functionalACK,
          expandQualifierValues: values.expandQualifierValues,
          othersettingsFuntionalACKS: values.othersettingsFuntionalACKS,


        }
      );


      // submit fnctionality here
      if (props.title == 'Add') {

        let connectorID
        let bound
        let WorkspaceObj = workspaceData.find(o => o.workspaceUUID === values.workspaceUUID);
        let documentTypeName
        if (values.transactionType == 'X12 to XML') {
          bound = "Inbound"
        } else {
          bound = "Outbound"
        }
        documentTypeName = values.documentType.replace(/ /g, '');
        connectorID = `${WorkspaceObj.workspaceName}_${documentTypeName}_${bound}`;

        await addX12Connector({
          variables: {
            X12ConnectorInput: {
              workspaceUUID: values.workspaceUUID,

              connectorId: connectorID,
              connectorDescription: values.connectorDescription,
              transactionType: values.transactionType,
              senderIdQualifier: values.senderIdQualifier,
              profilesenderIdentifier: values.profilesenderIdentifier,
              receiverIdQualifier: values.receiverIdQualifier,
              profilereceiverIdentifier: values.profilereceiverIdentifier,
              controlVersionNumber: values.controlVersionNumber,
              usageIndicator: values.usageIndicator,
              functionalGroup: values.functionalGroup,
              ta1Acknowledgement: values.ta1Acknowledgement,

              functionalACKType: values.functionalACKType,
              send: values.send,
              resendInterval: values.resendInterval,
              resendIntervalMaximumAttachments: values.resendIntervalMaximumAttachments,
              dataElementSeparator: values.dataElementSeparator,
              componentElementSeparator: values.componentElementSeparator,
              segmentTerminator: values.segmentTerminator,
              suffix: values.suffix,
              authorizationQualifier: values.authorizationQualifier,
              authorizationId: values.authorizationId,
              securityQualifier: values.securityQualifier,
              securityPassword: values.securityPassword,
              controlStandardsIdentifier: values.controlStandardsIdentifier,
              senderIdentifier: values.senderIdentifier,
              receiverIdentifier: values.receiverIdentifier,
              dateFormat: values.dateFormat,
              timeFormat: values.timeFormat,
              responsibleAgencyCode: values.responsibleAgencyCode,
              identifierCode: values.identifierCode,
              encoding: values.encoding,
              generateDescriptionAs: values.generateDescriptionAs,
              hippaSchemas: values.hippaSchemas,
              saveToSentFolder: values.saveToSentFolder,
              parentConnector: values.parentConnector,
              processingDelay: values.processingDelay,
              snipValidation: values.snipValidation,
              strictSchemaValidation: values.strictSchemaValidation,
              localFileScheme: values.localFileScheme,
              sendFilter: values.sendFilter,
              hippa278: values.hippa278,
              nestLoops: values.nestLoops,
              logMessages: values.logMessages,
              batchTransactions: values.batchTransactions,
              useReferenceId: values.useReferenceId,
              functionalACK: values.functionalACK,
              expandQualifierValues: values.expandQualifierValues,
              othersettingsFuntionalACKS: values.othersettingsFuntionalACKS,

            },
          },
        })
          .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
            console.log(values);
            console.log(data, error, graphQLErrors, networkError, cacheHit);
            if (data !== null && error == false) {
              //  handleRefetch(error, 'Saved successfully');
              props.onSuccess(error, 'Saved successfully', true);

            } else if (error && graphQLErrors.length > 0) {
              let duperror = graphQLErrors[0].message;
              console.log(duperror);
              if (
                duperror !== null &&
                duperror.indexOf('ER_DUP_ENTRY') !== -1
              ) {
                props.onSuccess(error, 'Connector ID already exists', false);
                // handleRefetch(error, 'User ID already existed');
              }
            }
          })
          .catch((e) => {
            // you can do something with the error here
            console.log(e);
          });

      } else {
        let connectorID
        let finalValue
        let str = values.connectorId;
        let firstValue = workspaceData.find(o => o.workspaceUUID === values.workspaceUUID);
        let middleValue = str.substring(str.lastIndexOf("_") + 1, str.indexOf("_"));
        if (values.transactionType == 'X12 to XML') {
          finalValue = "Inbound"
        } else {
          finalValue = "Outbound"
        }
        connectorID = `${firstValue.workspaceName} ${middleValue} ${finalValue}`;
        //update functionality
        await updateX12Connector({
          variables: {
            X12ConnectorInput: {
              X12ConnectorUUID: values.X12ConnectorUUID,
              idX12Connector: values.idX12Connector,
              authorizationQualifier: values.authorizationQualifier,
              connectorId: connectorID,
              connectorDescription: values.connectorDescription,
              transactionType: values.transactionType,
              workspaceUUID: values.workspaceUUID,

              senderIdQualifier: values.senderIdQualifier,
              profilesenderIdentifier: values.profilesenderIdentifier,
              receiverIdQualifier: values.receiverIdQualifier,
              profilereceiverIdentifier: values.profilereceiverIdentifier,
              controlVersionNumber: values.controlVersionNumber,
              usageIndicator: values.usageIndicator,
              functionalGroup: values.functionalGroup,
              ta1Acknowledgement: values.ta1Acknowledgement,

              functionalACKType: values.functionalACKType,
              send: values.send,
              resendInterval: values.resendInterval,
              resendIntervalMaximumAttachments: values.resendIntervalMaximumAttachments,
              dataElementSeparator: values.dataElementSeparator,
              componentElementSeparator: values.componentElementSeparator,
              segmentTerminator: values.segmentTerminator,
              suffix: values.suffix,

              authorizationId: values.authorizationId,
              securityQualifier: values.securityQualifier,
              securityPassword: values.securityPassword,
              controlStandardsIdentifier: values.controlStandardsIdentifier,
              senderIdentifier: values.senderIdentifier,
              receiverIdentifier: values.receiverIdentifier,
              dateFormat: values.dateFormat,
              timeFormat: values.timeFormat,
              responsibleAgencyCode: values.responsibleAgencyCode,
              identifierCode: values.identifierCode,
              encoding: values.encoding,
              generateDescriptionAs: values.generateDescriptionAs,
              hippaSchemas: values.hippaSchemas,

              parentConnector: values.parentConnector,
              processingDelay: values.processingDelay,
              snipValidation: values.snipValidation,
              strictSchemaValidation: values.strictSchemaValidation,
              localFileScheme: values.localFileScheme,
              sendFilter: values.sendFilter,
              saveToSentFolder: values.saveToSentFolder,
              hippa278: values.hippa278,
              nestLoops: values.nestLoops,
              logMessages: values.logMessages,
              batchTransactions: values.batchTransactions,
              useReferenceId: values.useReferenceId,
              functionalACK: values.functionalACK,
              expandQualifierValues: values.expandQualifierValues,
              othersettingsFuntionalACKS: values.othersettingsFuntionalACKS,
              X12ConnectorUUID: props.rowData.X12ConnectorUUID
            },
          },
        })
          .then(
            ({
              data,
              error,
              graphQLErrors,
              networkError,
              cacheHit,
              loading,
            }) => {
              console.log(
                data,
                error,
                graphQLErrors,
                networkError,
                cacheHit,
                loading
              );
              if (data !== null && error == false) {

                setErrMsg(false);

                props.onSuccess(error, 'Updated successfully', true);
              } else {
                props.onSuccess(error, 'Graphql hooks error', false);
              }
            }
          )
          .catch((e) => {
            // you can do something with the error here
            console.log(e);
          });
      }

    } else if (
      values.workspaceUUID == '' &&
      values.documentType == '' &&
      values.connectorId == '' &&
      values.connectorDescription == '' &&
      values.senderIdQualifier == '' &&
      values.profilesenderIdentifier == '' &&
      values.receiverIdQualifier == '' &&
      values.profilereceiverIdentifier == '' &&
      values.controlVersionNumber == '' &&
      values.usageIndicator == '' &&
      values.ta1Acknowledgement == '' &&
      values.functionalACKType == '' &&
      values.resendInterval == '' &&
      values.resendIntervalMaximumAttachments == '' &&
      values.dataElementSeparator == '' &&
      values.componentElementSeparator == '' &&
      values.segmentTerminator == '' &&
      values.suffix == '' &&
      values.authorizationQualifier == '' &&
      values.authorizationId == '' &&
      values.securityQualifier == '' &&
      values.securityPassword == '' &&
      values.controlStandardsIdentifier == '' &&
      values.senderIdentifier == '' &&
      values.receiverIdentifier == '' &&
      values.dateFormat == '' &&
      values.timeFormat == '' &&
      values.responsibleAgencyCode == '' &&
      values.identifierCode == '' &&
      values.encoding == '' &&
      values.generateDescriptionAs == '' &&
      values.parentConnector == '' &&
      values.processingDelay == '' &&
      values.strictSchemaValidation == '' &&
      values.localFileScheme == '' &&
      values.sendFilter == ''
    ) {
      console.log('Else ');
      setErrMsg(true);
    } else {
      console.log('Else Condition On submit');

      setErrMsg(false);
      setSubmitErr(true);
    }
  };

  const handleReset = () => {
    setErrMsg(false);
    setSubmitErr(false);
    setErrorInfo({
      workspaceUUID: false,
      documentType: false,
      connectorId: false,
      connectorDescription: false,
      resendInterval: false,
      resendIntervalMaximumAttachments: false,
      profilesenderIdentifier: false,
      profilereceiverIdentifier: false,
      senderIdQualifier: false,
      receiverIdQualifier: false,
      controlVersionNumber: false,
      usageIndicator: false,
      ta1Acknowledgement: false,
      functionalACKType: false,
      resendInterval: false,
      resendIntervalMaximumAttachments: false,
      dataElementSeparator: false,
      componentElementSeparator: false,
      segmentTerminator: false,
      suffix: false,

      authorizationQualifier: false,
      authorizationId: false,
      securityQualifier: false,
      securityPassword: false,
      controlStandardsIdentifier: false,

      senderIdentifier: false,
      receiverIdentifier: false,
      dateFormat: false,
      timeFormat: false,
      responsibleAgencyCode: false,
      identifierCode: false,

      encoding: false,
      generateDescriptionAs: false,
      parentConnector: false,
      processingDelay: false,
      strictSchemaValidation: false,
      localFileScheme: false,
      sendFilter: false,
      batchTransactions: false,
      useReferenceId: false,
      expandQualifierValues: false,
      othersettingsFuntionalACKS: false,

    });
    setValues(
      {
        ...values,
        workspaceUUID: '',
        workspaceName: '',
        documentType: '',
        connectorId: '',
        connectorDescription: '',
        transactionType: 'X12 to XML',
        senderIdQualifier: 'ZZ-Mutually Defined[X12]',
        profilesenderIdentifier: '',
        receiverIdQualifier: 'ZZ-Mutually Defined[X12]',
        profilereceiverIdentifier: '',
        controlVersionNumber: '00401-Standards Approved for Publication by ASC X12 Procedures Review Board through October 1997',
        usageIndicator: 'T-Test Data',
        functionalGroup: 'Apply these identifiers to the Functional Group',
        ta1Acknowledgement: 'Automatic',
        functionalACK: null,
        functionalACKType: '997',
        send: 'Send',
        resendInterval: '1440',
        resendIntervalMaximumAttachments: '5',
        dataElementSeparator: '*',
        componentElementSeparator: ':',
        segmentTerminator: '-',
        suffix: 'CRLF',
        authorizationQualifier: '00-No Automation Information Present',
        authorizationId: '',
        securityQualifier: '00-No Security Information Present',
        securityPassword: '',
        controlStandardsIdentifier: 'U-U.S.EDI Community of ASC,X12,TDCC,and UCS',
        senderIdentifier: '',
        receiverIdentifier: '',
        dateFormat: 'CCYYMMDD',
        timeFormat: 'HHMM',
        responsibleAgencyCode: 'T-Transportation data Committee X12',
        identifierCode: '004010-Standards Approved for Publication by ASC X12 Procedures Review Board through October 1997',
        encoding: 'utf-8',
        generateDescriptionAs: 'XML Comment',
        hippaSchemas: null,

        nestLoops: null,
        parentConnector: '',
        processingDelay: '0',
        snipValidation: null,
        strictSchemaValidation: 'Ignore',
        localFileScheme: '',

        sendFilter: '',
        saveToSentFolder: 'Save to Sent folder',
        othersettingsFuntionalACKS: null,
        // expandQualifierValue: null,
        batchTransactions: null,
        hippa278: 'Treat HIPAA 278 as a request',
        logMessages: 'Log messages',
        useReferenceId: null,
        expandQualifierValues: null,
      }
    );
  }
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiTypography: {
          body1: {
            fontSize: '10px !important',
            alignItems: 'center !important',
            fontFamily: 'Arial !important',
            paddingTop: '1px !important',
            paddingBottom: '1px !important',
          },
        },
        MuiInputLabel: {
          outlined: {
            fontSize: 10,
          },
        },
        MuiSelect: {
          select: {
            wordBreak: 'break-all !important',
          },
          selectMenu: {
            whiteSpace: 'none !important',
          },
        },
        MuiMenuItem: {
          root: {
            lineHeight: '0.5 !important',
            fontSize: '13px',
          },
        },
      },
    });
  return (
    <div>
      <MuiThemeProvider theme={getMuiTheme()}>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={props.isModalOpen}
          onClose={handleCloseModal}
          className="fabmodal"
        >
          <div className="modalpaper">
            <Grid container className="header">
              <Grid item xs={10} sm={10}>
                <h2 id="modal-title" className="h2">
                  X12 Connector
              </h2>
              </Grid>
              <Grid item xs={2} sm={2} className="close">
                <CloseIcon onClick={handleCloseModal} />
              </Grid>
            </Grid>

            <div>
              {ErrMsg === true ? (
                <div style={{ textAlign: 'center' }} className="addfieldserror">
                  Please fill the fields
                </div>
              ) : (
                  ''
                )}

              <form className="X12content" noValidate autoComplete="off">
                <Grid >
                  <Grid container spacing={2} className="userformcontent">

                    <Grid item xs={12} sm={12}>
                      <p className="advancedFrom" style={{ color: '#0b153e' }}>
                        Translation Configuration
                    </p>
                    </Grid>
                    <Grid item xs={12} sm={4} className="interchangedropdown">
                      <FormControl>
                        <InputLabel
                          style={{ color: '#0b153e', fontSize: 10 }}
                          id="demo-simple-select-label"
                        >
                          Work Space
                      </InputLabel>
                        <Select className="dropdown"
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          disabled={apiStatusRead ? true : false}
                          value={values.workspaceUUID}
                          onChange={handleChange('workspaceUUID')}
                          name={'workspaceUUID'}
                        >
                          {workspaceData != undefined && workspaceData.length > 0
                            ? workspaceData.map((v, k) => {
                              return (
                                <MenuItem value={v.workspaceUUID} data-status={v.workspaceName} >
                                  {v.workspaceName}
                                </MenuItem>
                              );
                            })
                            : null}

                        </Select>
                      </FormControl>
                      {submitErr && !values.workspaceUUID ? (
                        <div id="nameid" className="addfieldserror">
                          Please select Work Space
                        </div>
                      ) : (
                          ''
                        )}
                    </Grid>

                    {props.title == 'Add' ? (
                      <Grid item xs={12} sm={4} className="interchangedropdown">
                        <FormControl>
                          <InputLabel
                            style={{ color: '#0b153e', fontSize: 10 }}
                            id="demo-simple-select-label"
                          >
                            Document Type
                          </InputLabel>
                          <Select className="dropdown"
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={values.name}
                            onChange={handleChange('documentType')}
                            name="documentType"
                          >
                            {doctype != undefined && doctype.length > 0
                              ? doctype.map((v, k) => {
                                return (
                                  <MenuItem value={v.name}>
                                    {v.name}
                                  </MenuItem>
                                );
                              })
                              : null}


                          </Select>
                        </FormControl>
                        {submitErr && !values.documentType ? (
                          <div id="nameid" className="addfieldserror">
                            Please select document Type
                          </div>
                        ) : (
                            ''
                          )}
                      </Grid>
                    ) : (
                        <Grid item xs={12} sm={4}>
                          <TextField
                            id="outlined-dense"
                            label="Connector ID"
                            className="partnertextField"
                            margin="dense"
                            variant="outlined"
                            onChange={handleChange('connectorId')}
                            value={values.connectorId}
                            name="connectorId"
                            disabled={true}
                          />

                          {errorInfo.connectorId === true ? (
                            <div className="addfieldserror">
                              "Allowed Upper case letters and number only"
                            </div>
                          ) : (
                              ''
                            )}
                          {submitErr && !values.connectorId ? (
                            <div id="nameid" className="addfieldserror">
                              Please enter connector Id
                            </div>
                          ) : (
                              ''
                            )}
                        </Grid>
                      )}
                    <Grid item xs={12} sm={4}>
                      <TextField
                        id="outlined-dense"
                        label="Connector Description "
                        className="partnertextField"
                        margin="dense"
                        variant="outlined"
                        onChange={handleChange('connectorDescription')}
                        value={values.connectorDescription}
                        name="connectorDescription"
                      />
                      {/*
                {errorInfo.name === true ? (
                <div className="addfieldserror">
                    "Enter valid IP Address"
                </div>
                ) : (
                ''
                )}
                
                      {submitErr && !values.connectorDescription ? (
                        <div id="nameid" className="addfieldserror">
                          Please enter Connector Description
                        </div>
                      ) : (
                          ''
                        )}*/}
                    </Grid>

                    {props.title == 'Add' || !apiStatusRead ? (<>
                      <Grid
                        item
                        xs={12}
                        sm={2}
                        gutterBottom
                        variant="body2"
                        color="textSecondary"
                        className="text"
                      >
                        <p className="Advancedheader1">Transaction Type:</p>
                      </Grid>
                      <Grid
                        item
                        xs={12}
                        sm={4}
                        gutterBottom
                        variant="body2"
                        color="textSecondary"
                        className="text"
                      >
                        <RadioGroup
                          aria-label="SettingsForma"
                          name="transactionType"
                          color="primary"
                          value={values.transactionType}
                          onChange={handleChange('transactionType')}
                          row
                        >
                          <Grid item xs={6} sm={6}>
                            <FormControlLabel
                              value="X12 to XML"
                              className="Advancedtext"
                              color="primary"
                              control={<Radio />}
                              label="X12 to XML"
                            />
                          </Grid>
                          <Grid item xs={6} sm={6}>
                            <FormControlLabel
                              value="XML to X12"
                              className="Advancedtext"
                              control={<Radio color="primary" />}
                              label="XML to X12"
                            />
                          </Grid>
                        </RadioGroup>
                      </Grid>
                    </>

                    ) : (
                        <Grid item xs={12} sm={4}>
                          <TextField
                            id="outlined-dense"
                            label="Transaction Type"
                            className="partnertextField"
                            margin="dense"
                            variant="outlined"
                            value={values.transactionType}
                            // name="transactionType"
                            disabled={true}
                          />
                        </Grid>
                      )}


                    {/* <Grid item xs={12} sm={4}>
                      <TextField
                        id="outlined-dense"
                        label="Connector ID"
                        className="partnertextField"
                        margin="dense"
                        variant="outlined"
                        onChange={handleChange('connectorId')}
                        value={values.connectorId}
                        name="connectorId"
                      />

                      {errorInfo.connectorId === true ? (
                        <div className="addfieldserror">
                          "Allowed Upper case letters and number only"
                        </div>
                      ) : (
                          ''
                        )}
                      {submitErr && !values.connectorId ? (
                        <div id="nameid" className="addfieldserror">
                          Please enter connector Id
                        </div>
                      ) : (
                          ''
                        )}
                    </Grid> */}


                    <Grid item xs={12} sm={12}>
                      <p className="advancedFrom" style={{ color: '#0b153e' }}>
                        Interchange Settings
                    </p>
                    </Grid>
                    <Grid item xs={12} sm={4} className="interchangedropdown">
                      <FormControl>
                        <InputLabel
                          style={{ color: '#0b153e', fontSize: 10 }}
                          id="demo-simple-select-label"
                        >
                          Sender Id Qualifier (ISA05)
                      </InputLabel>
                        <Select className="dropdown"
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          value={values.senderIdQualifier}
                          onChange={handleChange('senderIdQualifier')}
                          name="senderIdQualifier"
                        >
                          {senderIdQualifier.map((senderId) => {
                            return (
                              <MenuItem value={senderId}>{senderId}</MenuItem>
                            );
                          })}
                        </Select>
                      </FormControl>
                      {submitErr && !values.senderIdQualifier ? (
                        <div id="nameid" className="addfieldserror">
                          Please select sender Id Qualifier
                        </div>
                      ) : (
                          ''
                        )}
                    </Grid>
                    <Grid item xs={12} sm={4}>
                      <TextField
                        id="outlined-dense"
                        label="Sender Identifier (ISA06)"
                        className="partnertextField"
                        margin="dense"
                        variant="outlined"
                        onChange={handleChange('profilesenderIdentifier')}
                        value={values.profilesenderIdentifier}
                        name="profilesenderIdentifier"
                      />

                      {errorInfo.profilesenderIdentifier === true ? (
                        <div className="addfieldserror">
                          "Allowed Upper case letters and number only"
                        </div>
                      ) : (
                          ''
                        )}
                      {submitErr && !values.profilesenderIdentifier ? (
                        <div id="nameid" className="addfieldserror">
                          Please enter sender Identifier (ISA06)
                        </div>
                      ) : (
                          ''
                        )}
                    </Grid>
                    <Grid item xs={12} sm={4} className="interchangedropdown">
                      <FormControl>
                        <InputLabel
                          style={{ color: '#0b153e', fontSize: 10 }}
                          id="demo-simple-select-label"
                        >
                          Receiver Id Qualifier(ISA07)
                      </InputLabel>
                        <Select className="dropdown"
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          value={values.receiverIdQualifier}
                          onChange={handleChange('receiverIdQualifier')}
                          name="receiverIdQualifier"
                        >
                          {senderIdQualifier.map((senderId) => {
                            return (
                              <MenuItem value={senderId}>{senderId}</MenuItem>
                            );
                          })}
                        </Select>
                      </FormControl>
                      {submitErr && !values.receiverIdQualifier ? (
                        <div id="nameid" className="addfieldserror">
                          Please select receiverIdQualifier
                        </div>
                      ) : (
                          ''
                        )}
                    </Grid>
                    <Grid item xs={12} sm={4}>
                      <TextField
                        id="outlined-dense"
                        label="Receiver Identifier(ISA08)"
                        className="partnertextField"
                        margin="dense"
                        variant="outlined"
                        onChange={handleChange('profilereceiverIdentifier')}
                        value={values.profilereceiverIdentifier}
                        name="profilereceiverIdentifier"
                      />

                      {errorInfo.profilereceiverIdentifier === true ? (
                        <div className="addfieldserror">
                          "Allowed Upper case letters and number only"
                        </div>
                      ) : (
                          ''
                        )}
                      {submitErr && !values.profilereceiverIdentifier ? (
                        <div id="nameid" className="addfieldserror">
                          Please enter Receiver Identifier(ISA08)
                        </div>
                      ) : (
                          ''
                        )}
                    </Grid>
                    <Grid item xs={12} sm={4} className="interchangedropdown">
                      <FormControl>
                        <InputLabel
                          style={{ color: '#0b153e', fontSize: 10 }}
                          id="demo-simple-select-label"
                        >
                          Control Version Number (ISA12)
                      </InputLabel>
                        <Select className="dropdown"
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          value={values.controlVersionNumber}
                          onChange={handleChange('controlVersionNumber')}
                          name="controlVersionNumber"
                        >
                          {controlVersion.map((Version) => {
                            return <MenuItem value={Version}>{Version}</MenuItem>;
                          })}
                        </Select>
                      </FormControl>
                      {/* {submitErr && !values.controlVersionNumber ? (
                        <div id="nameid" className="addfieldserror">
                          Please select control Version Number
                        </div>
                      ) : (
                          ''
                      )} */}
                    </Grid>
                    <Grid item xs={12} sm={4} className="interchangedropdown">
                      <FormControl>
                        <InputLabel
                          style={{ color: '#0b153e', fontSize: 10 }}
                          id="demo-simple-select-label"
                        >
                          Usage Indicator (ISA15)
                      </InputLabel>
                        <Select className="dropdown"
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          value={values.usageIndicator}
                          onChange={handleChange('usageIndicator')}
                          name="usageIndicator"
                        >
                          {usageIndicator.map((indicator) => {
                            return (
                              <MenuItem value={indicator}>{indicator}</MenuItem>
                            );
                          })}
                        </Select>
                      </FormControl>
                      {/*  {submitErr && !values.usageIndicator ? (
                        <div id="nameid" className="addfieldserror">
                          Please select usageIndicator
                        </div>
                      ) : (
                          ''
                      )} */}
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={2}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <p className="Advancedheader2">Functional Group:</p>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <Typography className="Advancedtext">
                        <Checkbox
                          inputProps={{
                            'aria-label': 'uncontrolled-checkbox',
                          }}
                          value={values.functionalGroup != null ? true : false}
                          checked={values.functionalGroup != null ? true : false}
                          name="Apply these identifiers to the Functional Group"
                          onChange={handleChange('functionalGroup')}
                          color="primary"
                        />
                      Apply these identifiers to the Functional Group
                    </Typography>
                    </Grid>

                    <Grid item xs={12} sm={12}>
                      <p className="advancedFrom" style={{ color: '#0b153e' }}>
                        Acknowledgements
                    </p>
                    </Grid>
                    <Grid item xs={12} sm={4} className="interchangedropdown">
                      <FormControl>
                        <InputLabel
                          style={{ color: '#0b153e', fontSize: 10 }}
                          id="demo-simple-select-label"
                        >
                          TA1 Acknowledgement
                      </InputLabel>
                        <Select className="dropdown"
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          value={values.ta1Acknowledgement}
                          onChange={handleChange('ta1Acknowledgement')}
                          name="ta1Acknowledgement"
                        >
                          {TA1Acknowledgement.map((Acknowledgement) => {
                            return (
                              <MenuItem value={Acknowledgement}>
                                {Acknowledgement}
                              </MenuItem>
                            );
                          })}
                        </Select>
                      </FormControl>
                      {/*  {submitErr && !values.ta1Acknowledgement ? (
                        <div id="nameid" className="addfieldserror">
                          Please select TA1 Acknowledgement
                        </div>
                      ) : (
                          ''
                      )} */}
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={4}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <Typography className="Advancedheader">
                        Functional ACK:
                    </Typography>

                      <Typography className="Advancedtext">
                        <Checkbox
                          inputProps={{
                            'aria-label': 'uncontrolled-checkbox',
                          }}
                          value={values.functionalACK != null ? true : false}
                          checked={values.functionalACK != null ? true : false}
                          name="Functional ACK expected"
                          onChange={handleChange('functionalACK')}
                          color="primary"
                        />
                      Functional ACK expected
                    </Typography>
                    </Grid>
                    <Grid item xs={12} sm={4} className="interchangedropdown"

                    >

                      <FormControl>

                        <InputLabel
                          style={{ color: '#0b153e', fontSize: 10 }}
                          id="demo-simple-select-label"

                        >
                          Functional ACK Type
                      </InputLabel>
                        <Select className="dropdown"
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          value={values.functionalACKType}
                          onChange={handleChange('functionalACKType')}
                          name="functionalACKType"
                          disabled={values.functionalACK == 'Functional ACK expected' ? false : true}

                        >
                          {functionalACKType.map((ACKType) => {
                            return <MenuItem value={ACKType}>{ACKType}</MenuItem>;
                          })}
                        </Select>
                      </FormControl>
                      {/* {submitErr && !values.functionalACKType ? (
                        <div id="nameid" className="addfieldserror">
                          Please select functional ACK Type
                        </div>
                      ) : (
                          ''
                      )} */}
                    </Grid>
                    <Grid item xs={12} sm={12}>
                      <p className="advancedFrom" style={{ color: '#0b153e' }}>
                        Automation Settings:
                    </p>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={1}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <p className="sendtext">Send:</p>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={2}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <Typography className="Advancedtext">
                        <Checkbox className="checkoutresponsive"
                          inputProps={{
                            'aria-label': 'uncontrolled-checkbox',
                          }}
                          value={values.send != null ? true : false}
                          checked={values.send != null ? true : false}
                          name="Send"
                          onChange={handleChange('send')}
                          color="primary"
                        />
                      Send
                    </Typography>
                    </Grid>
                    <Grid item xs={12} sm={4}>
                      <TextField
                        id="outlined-dense"
                        label="Resend Interval(minutes)"
                        className="partnertextField"
                        margin="dense"
                        disabled={values.send == null ? true : false}
                        variant="outlined"
                        onChange={handleChange('resendInterval')}
                        value={values.resendInterval}
                        name="resendInterval"
                      />

                      {errorInfo.resendInterval === true ? (
                        <div className="addfieldserror">
                          "Allowed Numbers only"
                        </div>
                      ) : (
                          ''
                        )}
                      {/*  {submitErr && !values.resendInterval ? (
                        <div id="nameid" className="addfieldserror">
                          Please enter Resend Interval
                        </div>
                      ) : (
                          ''
                      )} */}
                    </Grid>
                    <Grid item xs={12} sm={4}>
                      <TextField
                        id="outlined-dense"
                        label="Resend Interval Maximum Attempts"
                        className="partnertextField"
                        margin="dense"
                        disabled={values.send == null ? true : false}
                        variant="outlined"
                        onChange={handleChange(
                          'resendIntervalMaximumAttachments'
                        )}
                        value={values.resendIntervalMaximumAttachments}
                        name="resendIntervalMaximumAttachments"
                      />

                      {errorInfo.resendIntervalMaximumAttachments === true ? (
                        <div className="addfieldserror">
                          "Allowed Numbers only"
                        </div>
                      ) : (
                          ''
                        )}
                      {/* {submitErr && !values.resendIntervalMaximumAttachments ? (
                        <div id="nameid" className="addfieldserror">
                          Please enter Resend Interval Maximum Attachments
                        </div>
                      ) : (
                          ''
                      )} */}
                    </Grid>
                    <Grid item xs={12} sm={12}>
                      <p className="advancedFrom" style={{ color: '#0b153e' }}>
                        EDI Delimiters:
                    </p>
                    </Grid>
                    <Grid item xs={12} sm={3}>
                      <TextField
                        id="outlined-dense"
                        label="Data Element Separator"
                        className="partnertextField"
                        margin="dense"
                        variant="outlined"
                        onChange={handleChange('dataElementSeparator')}
                        value={values.dataElementSeparator}
                        name="dataElementSeparator"
                      />

                      {errorInfo.dataElementSeparator === true ? (
                        <div className="addfieldserror">
                          "Allowed any one special characters * . ~ "
                        </div>
                      ) : (
                          ''
                        )}
                      {/* {submitErr && !values.dataElementSeparator ? (
                        <div id="nameid" className="addfieldserror">
                          Please enter data element separator
                        </div>
                      ) : (
                          ''
                      )} */}
                    </Grid>
                    <Grid item xs={12} sm={3}>
                      <TextField
                        id="outlined-dense"
                        label="Component Element Separator"
                        className="partnertextField"
                        margin="dense"
                        variant="outlined"
                        onChange={handleChange('componentElementSeparator')}
                        value={values.componentElementSeparator}
                        name="componentElementSeparator"
                      />

                      {errorInfo.componentElementSeparator === true ? (
                        <div className="addfieldserror">
                          "Allowed any one special characters * . ~ "
                        </div>
                      ) : (
                          ''
                        )}
                      {/*  {submitErr && !values.componentElementSeparator ? (
                        <div id="nameid" className="addfieldserror">
                          Please enter component element separator
                        </div>
                      ) : (
                          ''
                      )} */}
                    </Grid>
                    <Grid item xs={12} sm={3}>
                      <TextField
                        id="outlined-dense"
                        label="Segment Terminator"
                        className="partnertextField"
                        margin="dense"
                        variant="outlined"
                        onChange={handleChange('segmentTerminator')}
                        value={values.segmentTerminator}
                        name="segmentTerminator"
                      />

                      {errorInfo.segmentTerminator === true ? (
                        <div className="addfieldserror">
                          "Allowed any one special characters * . ~ "
                        </div>
                      ) : (
                          ''
                        )}
                      {/* {submitErr && !values.segmentTerminator ? (
                        <div id="nameid" className="addfieldserror">
                          Please enter segment terminator
                        </div>
                      ) : (
                          ''
                      )} */}
                    </Grid>
                    <Grid item xs={12} sm={3} className="interchangedropdown">
                      <FormControl>
                        <InputLabel
                          style={{ color: '#0b153e', fontSize: 10 }}
                          id="demo-simple-select-label"
                        >
                          Suffix
                      </InputLabel>
                        <Select className="dropdown"
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          value={values.suffix}
                          onChange={handleChange('suffix')}
                          name="suffix"
                        >

                          <MenuItem value={"NONE"}>NONE</MenuItem>
                          <MenuItem value={"CR"}>CR</MenuItem>
                          <MenuItem value={"LF"}>LF</MenuItem>
                          <MenuItem value={"CRLF"}>CRLF</MenuItem>


                        </Select>
                      </FormControl>
                      {/* {submitErr && !values.suffix ? (
                        <div id="nameid" className="addfieldserror">
                          Please select suffix
                        </div>
                      ) : (
                          ''
                      )} */}
                    </Grid>
                    <Grid item xs={12} sm={12}>
                      <p className="advancedFrom" style={{ color: '#0b153e' }}>
                        Interchange Settings:
                    </p>
                    </Grid>
                    <Grid item xs={12} sm={4} className="interchangedropdown">
                      <FormControl>
                        <InputLabel
                          style={{ color: '#0b153e', fontSize: 10 }}
                          id="demo-simple-select-label"
                        >
                          Authorization Qualifier (ISA01)
                      </InputLabel>
                        <Select className="dropdown"
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          value={values.authorizationQualifier}
                          onChange={handleChange('authorizationQualifier')}
                          name="authorizationQualifier"

                        >

                          <MenuItem value={"Not Specified"}>Not Specified</MenuItem>
                          <MenuItem value={"00-No Automation Information Present"}>00-No Automation Information Present</MenuItem>
                          <MenuItem value={"01-UCS Communications ID"}>01-UCS Communications ID</MenuItem>
                          <MenuItem value={"02-EDX Communications ID"}>02-EDX Communications ID</MenuItem>
                          <MenuItem value={"03-Additional Data Identification"}>03-Additional Data Identification</MenuItem>
                          <MenuItem value={"04-Rail Communications ID"}>04-Rail Communications ID</MenuItem>
                          <MenuItem value={"05-Department of Defense[DoD] Communication Identifier"}>05-Department of Defense[DoD] Communication Identifier</MenuItem>
                          <MenuItem value={"06-United States Federal Government Communication Identifier"}>06-United States Federal Government Communication Identifier</MenuItem>
                          <MenuItem value={"07-Truck Communications Id"}>07-Truck Communications Id</MenuItem>
                          <MenuItem value={"08-Ocean Communications Id "}>08-Ocean Communications Id </MenuItem>
                        </Select>
                      </FormControl>
                      {/* {submitErr && !values.authorizationQualifier ? (
                        <div id="nameid" className="addfieldserror">
                          Please select authorization qualifier
                        </div>
                      ) : (
                          ''
                      )} */}
                    </Grid>
                    <Grid item xs={12} sm={4}>
                      <TextField
                        id="outlined-dense"
                        label="Authorization Id (ISA02)"
                        className="partnertextField"
                        margin="dense"
                        variant="outlined"
                        onChange={handleChange('authorizationId')}
                        value={values.authorizationId}
                        name="authorizationId"
                        disabled={values.authorizationQualifier == '00-No Automation Information Present' ? true : false}

                      />

                      {errorInfo.authorizationId === true ? (
                        <div className="addfieldserror">
                          "Allowed Upper case letters and numbers only"
                        </div>
                      ) : (
                          ''
                        )}
                      {/*  {submitErr && !values.authorizationId ? (
                        <div id="nameid" className="addfieldserror">
                          Please enter authorization Id
                        </div>
                      ) : (
                          ''
                      )} */}
                    </Grid>
                    <Grid item xs={12} sm={4} className="interchangedropdown">
                      <FormControl>
                        <InputLabel
                          style={{ color: '#0b153e', fontSize: 10 }}
                          id="demo-simple-select-label"
                        >
                          Security Qualifier (ISA03)
                      </InputLabel>
                        <Select className="dropdown"
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          value={values.securityQualifier}
                          onChange={handleChange('securityQualifier')}
                          name="securityQualifier"
                        >

                          <MenuItem value={"Not Specified"}>Not Specified</MenuItem>
                          <MenuItem value={"00-No Security Information Present"}>00-No Security Information Present</MenuItem>
                          <MenuItem value={"01-Password"}>01-Password</MenuItem>
                          <MenuItem value={"03-Password(for backward compatibility)"}>03-Password(for backward compatibility)</MenuItem>

                        </Select>
                      </FormControl>
                      {/* {submitErr && !values.securityQualifier ? (
                        <div id="nameid" className="addfieldserror">
                          Please select security qualifier
                        </div>
                      ) : (
                          ''
                      )} */}
                    </Grid>
                    <Grid item xs={12} sm={4}>
                      <TextField
                        id="outlined-dense"
                        label="Security Password (ISA04)"
                        className="partnertextField"
                        type="password"
                        margin="dense"
                        variant="outlined"
                        onChange={handleChange('securityPassword')}
                        value={values.securityPassword}
                        name="securityPassword"
                        disabled={values.securityQualifier == '00-No Security Information Present' ? true : false}

                      />

                      {errorInfo.securityPassword === true ? (
                        <div className="addfieldserror">
                          "Allowed Uppercase Alphabets & Numbers.Minimum length is
                          5 and Maximum is 10"
                        </div>
                      ) : (
                          ''
                        )}
                      {/*{submitErr && !values.securityPassword ? (
                        <div id="nameid" className="addfieldserror">
                          Please enter security password
                        </div>
                      ) : (
                          ''
                      )} */}
                    </Grid>
                    <Grid item xs={12} sm={4} className="interchangedropdown">
                      <FormControl>
                        <InputLabel
                          style={{ color: '#0b153e', fontSize: 10 }}
                          id="demo-simple-select-label"
                        >
                          Control Standards Identifier (ISA11)
                      </InputLabel>
                        <Select className="dropdown"
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          value={values.controlStandardsIdentifier}
                          onChange={handleChange('controlStandardsIdentifier')}
                          name="controlStandardsIdentifier"
                        >

                          <MenuItem value={"Not Specified"}>Not Specified</MenuItem>
                          <MenuItem value={"U-U.S.EDI Community of ASC,X12,TDCC,and UCS"}>U-U.S.EDI Community of ASC,X12,TDCC,and UCS </MenuItem>

                        </Select>
                      </FormControl>
                      {/* {submitErr && !values.controlStandardsIdentifier ? (
                        <div id="nameid" className="addfieldserror">
                          Please select control standards identifier
                        </div>
                      ) : (
                          ''
                      )}*/}
                    </Grid>
                    <Grid
                      container
                      spacing={2}
                      style={{ display: values.functionalGroup !== null ? 'none' : '' }}
                    >
                      <Grid item xs={12} sm={12}>
                        <p className="advancedFrom" style={{ color: '#0b153e' }}>
                          Functional Group Settings:
                      </p>
                      </Grid>
                      <Grid item xs={12} sm={4}>
                        <TextField
                          id="outlined-dense"
                          label="Sender Identifier (GS02)"
                          className="partnertextField"
                          margin="dense"
                          variant="outlined"
                          onChange={handleChange('senderIdentifier')}
                          value={values.senderIdentifier}
                          name="senderIdentifier"
                        />

                        {errorInfo.senderIdentifier === true ? (
                          <div className="addfieldserror">
                            "Allowed Upper case letters and Number only"
                          </div>
                        ) : (
                            ''
                          )}
                        {/*  {submitErr && !values.senderIdentifier ? (
                          <div id="nameid" className="addfieldserror">
                            Please enter sender identifier
                          </div>
                        ) : (
                            ''
                        )} */}
                      </Grid>
                      <Grid item xs={12} sm={4}>
                        <TextField
                          id="outlined-dense"
                          label="Receiver Identifier (GS03)"
                          className="partnertextField"
                          margin="dense"
                          variant="outlined"
                          onChange={handleChange('receiverIdentifier')}
                          value={values.receiverIdentifier}
                          name="receiverIdentifier"
                        />

                        {errorInfo.receiverIdentifier === true ? (
                          <div className="addfieldserror">
                            "Allowed Upper case letters and Number only"
                          </div>
                        ) : (
                            ''
                          )}
                        {/* {submitErr && !values.receiverIdentifier ? (
                          <div id="nameid" className="addfieldserror">
                            Please enter receiver identifier
                          </div>
                        ) : (
                            ''
                        )} */}
                      </Grid>
                      <Grid item xs={12} sm={4} className="interchangedropdown">
                        <FormControl>
                          <InputLabel
                            style={{ color: '#0b153e', fontSize: 10 }}
                            id="demo-simple-select-label"
                          >
                            Date Format (GS04)
                        </InputLabel>
                          <Select className="dropdown"
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={values.dateFormat}
                            onChange={handleChange('dateFormat')}
                            name="dateFormat"
                          >
                            <MenuItem value={"Not Specified"}>Not Specified</MenuItem>
                            <MenuItem value={"YYMMDD"}>YYMMDD</MenuItem>
                            <MenuItem value={"CCYYMMDD"}>CCYYMMDD</MenuItem>  <MenuItem value={'NONE'}>NONE</MenuItem>
                          </Select>
                        </FormControl>
                        {/* {submitErr && !values.dateFormat ? (
                          <div id="nameid" className="addfieldserror">
                            Please select date format
                          </div>
                        ) : (
                            ''
                        )} */}
                      </Grid>
                      <Grid item xs={12} sm={4} className="interchangedropdown">
                        <FormControl>
                          <InputLabel
                            style={{ color: '#0b153e', fontSize: 10 }}
                            id="demo-simple-select-label"
                          >
                            Time Format (GS05)
                        </InputLabel>
                          <Select className="dropdown"
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={values.timeFormat}
                            onChange={handleChange('timeFormat')}
                            name="timeFormat"
                          >
                            <MenuItem value={"Not Specified"}>Not Specified</MenuItem>
                            <MenuItem value={"HHMM"}>HHMM</MenuItem>
                            <MenuItem value={"HHMMSS"}>HHMMSS</MenuItem>
                            <MenuItem value={"HHMMSSD"}>HHMMSSD</MenuItem>
                            <MenuItem value={"HHMMSSDD"}>HHMMSSDD</MenuItem>
                          </Select>
                        </FormControl>
                        {/* {submitErr && !values.timeFormat ? (
                          <div id="nameid" className="addfieldserror">
                            Please select time format
                          </div>
                        ) : (
                            ''
                        )} */}
                      </Grid>
                      <Grid item xs={12} sm={4} className="interchangedropdown">
                        <FormControl>
                          <InputLabel
                            style={{ color: '#0b153e', fontSize: 10 }}
                            id="demo-simple-select-label"
                          >
                            Responsible Agency Code (GS07)
                        </InputLabel>
                          <Select className="dropdown"
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={values.responsibleAgencyCode}
                            onChange={handleChange('responsibleAgencyCode')}
                            name="responsibleAgencyCode"
                          >
                            <MenuItem value={"Not Specified"}>Not Specified</MenuItem>
                            <MenuItem value={"T-Transportation data Committee X12"}>T-Transportation data Committee X12</MenuItem>
                            <MenuItem value={"X-Accredited  Standards Committee X12"}>X-Accredited  Standards Committee X12</MenuItem>
                          </Select>
                        </FormControl>
                        {/*  {submitErr && !values.responsibleAgencyCode ? (
                          <div id="nameid" className="addfieldserror">
                            Please select responsible agency code
                          </div>
                        ) : (
                            ''
                        )} */}
                      </Grid>
                      <Grid item xs={12} sm={2} className="interchangedropdown">
                        <FormControl>
                          <InputLabel
                            style={{ color: '#0b153e', fontSize: 10 }}
                            id="demo-simple-select-label"
                          >
                            Identifier Code (GS08)
                        </InputLabel>
                          <Select className="dropdown"
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={values.identifierCode}
                            onChange={handleChange('identifierCode')}
                            name="identifierCode"
                          >
                            {identifierCode.map((Identifier) => {
                              return <MenuItem value={Identifier}>{Identifier}</MenuItem>;
                            })}
                          </Select>
                        </FormControl>
                        {/*{submitErr && !values.identifierCode ? (
                          <div id="nameid" className="addfieldserror">
                            Please select Identifier Code
                          </div>
                        ) : (
                            ''
                        )} */}
                      </Grid>
                    </Grid>
                    <Grid item xs={12} sm={12}>
                      <p className="advancedFrom" style={{ color: '#0b153e' }}>
                        Other Settings:
                    </p>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={3}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <Typography className="Advancedheader">
                        Batch Transcations
                    </Typography>

                      <Typography className="Advancedtext">
                        <Checkbox
                          inputProps={{
                            'aria-label': 'uncontrolled-checkbox',
                          }}
                          value={values.batchTransactions != null ? true : false}
                          checked={
                            values.batchTransactions != null ? true : false
                          }
                          name="Batch transactions"
                          onChange={handleChange('batchTransactions')}
                          color="primary"
                        />
                      Batch transactions
                    </Typography>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={3}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <Typography className="Advancedheader">
                        Use Reference ID
                    </Typography>

                      <Typography className="Advancedtext">
                        <Checkbox
                          inputProps={{
                            'aria-label': 'uncontrolled-checkbox',
                          }}
                          value={values.useReferenceId != null ? true : false}
                          checked={values.useReferenceId != null ? true : false}
                          name="Use reference id as element name"
                          onChange={handleChange('useReferenceId')}
                          color="primary"
                        />
                      Use reference id as element name
                    </Typography>
                    </Grid>

                    <Grid item xs={12} sm={3}>
                      <TextField
                        id="outlined-dense"
                        label="Encoding"
                        className="partnertextField"
                        margin="dense"
                        variant="outlined"
                        onChange={handleChange('encoding')}
                        value={values.encoding}
                        name="encoding"
                      />

                      {errorInfo.encoding === true ? (
                        <div className="addfieldserror">
                          "Allowed Alphabets and numbers. No special Charecters
                          except - "
                        </div>
                      ) : (
                          ''
                        )}
                      {/*{submitErr && !values.encoding ? (
                        <div id="nameid" className="addfieldserror">
                          Please enter Encoding
                        </div>
                      ) : (
                          ''
                      )} */}
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={3}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <Typography className="Advancedheader">
                        Expand Qualifier values
                    </Typography>

                      <Typography className="Advancedtext">
                        <Checkbox
                          inputProps={{
                            'aria-label': 'uncontrolled-checkbox',
                          }}
                          value={
                            values.expandQualifierValues != null ? true : false
                          }
                          checked={
                            values.expandQualifierValues != null ? true : false
                          }
                          name="Expand qualifier values"
                          onChange={handleChange('expandQualifierValues')}
                          color="primary"
                        />
                      Expand qualifier values
                    </Typography>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={3}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <Typography className="Advancedheader">
                        Functional Acks:
                    </Typography>

                      <Typography className="Advancedtext">
                        <Checkbox
                          inputProps={{
                            'aria-label': 'uncontrolled-checkbox',
                          }}
                          value={values.othersettingsFuntionalACKS != null ? true : false}
                          checked={values.othersettingsFuntionalACKS != null ? true : false}
                          name="Return inbound functional acknowledgements"
                          onChange={handleChange('othersettingsFuntionalACKS')}
                          color="primary"
                        />
                      Return inbound functional acknowledgements
                    </Typography>
                    </Grid>

                    <Grid item xs={12} sm={3} className="interchangedropdown">
                      <FormControl>
                        <InputLabel
                          style={{ color: '#0b153e', fontSize: 10 }}
                          id="demo-simple-select-label"
                        >
                          Generate Description As
                      </InputLabel>
                        <Select className="dropdown"
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          value={values.generateDescriptionAs}
                          onChange={handleChange('generateDescriptionAs')}
                          name="generateDescriptionAs"
                        >
                          <MenuItem value={'XML Comment'}>XML Comment</MenuItem>
                          <MenuItem value={'Element Attribute'}>Element Attribute</MenuItem>
                          <MenuItem value={'None'}>None</MenuItem>
                        </Select>
                      </FormControl>
                      {/* {submitErr && !values.generateDescriptionAs ? (
                        <div id="nameid" className="addfieldserror">
                          Please select Generate Description As
                        </div>
                      ) : (
                          ''
                      )} */}
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={3}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <Typography className="Advancedheader">
                        Hippa Schemas:
                    </Typography>

                      <Typography className="Advancedtext">
                        <Checkbox
                          inputProps={{
                            'aria-label': 'uncontrolled-checkbox',
                          }}
                          value={values.hippaSchemas != null ? true : false}
                          checked={values.hippaSchemas != null ? true : false}
                          name="Use HIPAA schemas"
                          onChange={handleChange('hippaSchemas')}
                          color="primary"
                        />
                      Use HIPAA schemas
                    </Typography>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={3}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <Typography className="Advancedheader">
                        Hippa 278:
                    </Typography>

                      <Typography className="Advancedtext">
                        <Checkbox
                          inputProps={{
                            'aria-label': 'uncontrolled-checkbox',
                          }}
                          value={values.hippa278 != null ? true : false}
                          checked={values.hippa278 != null ? true : false}
                          name="Treat HIPAA 278 as a request"
                          onChange={handleChange('hippa278')}
                          color="primary"
                        />
                      Treat HIPAA 278 as a request
                    </Typography>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={3}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <Typography className="Advancedheader">
                        Nest Loops:
                    </Typography>

                      <Typography className="Advancedtext">
                        <Checkbox
                          inputProps={{
                            'aria-label': 'uncontrolled-checkbox',
                          }}
                          value={values.nestLoops != null ? true : false}
                          checked={values.nestLoops != null ? true : false}
                          name="Nest master-detail loops"
                          onChange={handleChange('nestLoops')}
                          color="primary"
                        />
                      Nest master-detail loops
                    </Typography>
                    </Grid>
                    <Grid item xs={12} sm={3}>
                      <TextField
                        id="outlined-dense"
                        label="Parent Connector"
                        className="partnertextField"
                        margin="dense"
                        variant="outlined"
                        onChange={handleChange('parentConnector')}
                        value={values.parentConnector}
                        name="parentConnector"
                      />

                      {errorInfo.parentConnector === true ? (
                        <div className="addfieldserror">
                          "Allowed Only Alphabets and Number"
                        </div>
                      ) : (
                          ''
                        )}
                      {/* {submitErr && !values.parentConnector ? (
                        <div id="nameid" className="addfieldserror">
                          Please enter Parent Connector
                        </div>
                      ) : (
                          ''
                      )} */}
                    </Grid>
                    <Grid item xs={12} sm={3}>
                      <TextField
                        id="outlined-dense"
                        label="Processing Delay"
                        className="partnertextField"
                        margin="dense"
                        variant="outlined"
                        onChange={handleChange('processingDelay')}
                        value={values.processingDelay}
                        name="processingDelay"
                      />

                      {errorInfo.processingDelay === true ? (
                        <div className="addfieldserror">
                          "Allowed Numbers only"
                        </div>
                      ) : (
                          ''
                        )}
                      {/* {submitErr && !values.processingDelay ? (
                        <div id="nameid" className="addfieldserror">
                          Please enter Processing Delay
                        </div>
                      ) : (
                          ''
                      )} */}
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={3}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <Typography className="Advancedheader">
                        SNIP Validation
                    </Typography>

                      <Typography className="Advancedtext">
                        <Checkbox
                          inputProps={{
                            'aria-label': 'uncontrolled-checkbox',
                          }}
                          value={values.snipValidation != null ? true : false}
                          checked={values.snipValidation != null ? true : false}
                          name="Enable SNIP validation"
                          onChange={handleChange('snipValidation')}
                          color="primary"
                        />
                      Enable SNIP validation
                    </Typography>
                    </Grid>
                    <Grid item xs={12} sm={3} className="interchangedropdown">
                      <FormControl>
                        <InputLabel
                          style={{ color: '#0b153e', fontSize: 12 }}
                          id="demo-simple-select-label"
                        >
                          Strict Schema Validation
                      </InputLabel>
                        <Select className="dropdown"
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          value={values.strictSchemaValidation}
                          onChange={handleChange('strictSchemaValidation')}
                          name="strictSchemaValidation"
                        >
                          <MenuItem value={'Ignore'}>Ignore</MenuItem>
                        </Select>
                      </FormControl>
                      {/* {submitErr && !values.strictSchemaValidation ? (
                        <div id="nameid" className="addfieldserror">
                          Please select Strict Schema Validation
                        </div>
                      ) : (
                          ''
                      )} */}
                    </Grid>
                    <Grid item xs={12} sm={3}>
                      <TextField
                        id="outlined-dense"
                        label="Local File Schema"
                        className="partnertextField"
                        margin="dense"
                        variant="outlined"
                        onChange={handleChange('localFileScheme')}
                        value={values.localFileScheme}
                        name="localFileScheme"
                      />

                      {errorInfo.localFileScheme === true ? (
                        <div className="addfieldserror">
                          "Allowed only Alphabets and Numbers."
                        </div>
                      ) : (
                          ''
                        )}
                      {/* {submitErr && !values.localFileScheme ? (
                        <div id="nameid" className="addfieldserror">
                          Please enter Local File Scheme
                        </div>
                      ) : (
                          ''
                      )} */}
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={1}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <p className="Advancedheader1">Log Messages</p>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={2}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <Typography className="Advancedtext">
                        <Checkbox className="checkoutresponsive"
                          inputProps={{
                            'aria-label': 'uncontrolled-checkbox',
                          }}
                          value={values.logMessages != null ? true : false}
                          checked={values.logMessages != null ? true : false}
                          name="Log messages"
                          onChange={handleChange('logMessages')}
                          color="primary"
                        />
                      Log messages
                    </Typography>
                    </Grid>
                    <Grid item xs={12} sm={3}>
                      <TextField
                        id="outlined-dense"
                        label="Send Filter"
                        className="partnertextField"
                        margin="dense"
                        variant="outlined"
                        onChange={handleChange('sendFilter')}
                        value={values.sendFilter}
                        name="sendFilter"
                      />

                      {errorInfo.sendFilter === true ? (
                        <div className="addfieldserror">
                          "Allowed only Alphabets and Numbers "
                        </div>
                      ) : (
                          ''
                        )}
                      {/*  {submitErr && !values.sendFilter ? (
                        <div id="nameid" className="addfieldserror">
                          Please enter Send Filter
                        </div>
                      ) : (
                          ''
                      )} */}
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={2}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <p className="Advancedheader1">Save to Sent Folder:</p>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={2}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <Typography className="Advancedtext">
                        <Checkbox
                          inputProps={{
                            'aria-label': 'uncontrolled-checkbox',
                          }}
                          value={values.saveToSentFolder != null ? true : false}
                          checked={values.saveToSentFolder != null ? true : false}
                          name="Save to Sent folder"
                          onChange={handleChange('saveToSentFolder')}
                          color="primary"
                        />
                      Save to Sent folder
                    </Typography>
                    </Grid>

                    <Grid container spacing={3}>
                      <Grid
                        item
                        xs={6}
                        sm={6}
                        gutterBottom
                        variant="body2"
                        color="textSecondary"
                        className="text"
                      >
                        <Button
                          style={{
                            color: '#000006',
                            backgroundColor: '#E5CCFF',
                          }}
                          onClick={handleReset}
                          variant="contained"
                          fullWidth="true"
                          className="createpartnerbutton"
                        >
                          RESET
                      </Button>
                      </Grid>

                      <Grid
                        item
                        xs={6}
                        sm={6}
                        gutterBottom
                        variant="body2"
                        color="textSecondary"
                        className="text"
                      >
                        <Button
                          onClick={handleSubmit}
                          variant="contained"
                          fullWidth="true"
                          className="createpartnerbutton"
                        >
                          {props.title !== 'Add' ? 'UPDATE' : 'SAVE'}

                        </Button>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </form>
              <Snackbar
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'center',
                }}
                autoHideDuration={3000}
                open={openSnackbar}
                onClose={handleCloseSnackbar}
              >
                <MySnackbarContentWrapper
                  onClose={handleCloseSnackbar}
                  variant={variant}
                  message={message}
                />
              </Snackbar>

            </div>
          </div>
        </Modal>

      </MuiThemeProvider>

    </div>
  );
}

