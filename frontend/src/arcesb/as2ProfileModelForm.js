import React, { useEffect, useContext, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { useMutation, useQuery } from 'graphql-hooks';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import '../css/commonStyle.css';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import Theme from '../css/theme';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import Checkbox from '@material-ui/core/Checkbox';
import FormGroup from '@material-ui/core/FormGroup';
import UploadCertificateForm from '../arcesb/tradinginfo/uploadCertificateModel';
import UploadCertificateModel from '../components/tmsmodal';
import { Typography, Paper } from '@material-ui/core';
import Modal from '@material-ui/core/Modal';
import AddIcon from '@material-ui/icons/Add';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
const ADD_AS2profile = `
mutation createAS2Profile($AS2ProfileInput:AS2ProfileInput){
  createAS2Profile(input:$AS2ProfileInput) {
    idas2Profile
    connectorID
  } 
}
`;
const UPDATE_AS2profile = `
mutation updateAS2Profile($AS2ProfileInput:AS2ProfileInput){
  updateAS2Profile(input:$AS2ProfileInput) {
    idas2Profile
    connectorID
  } 
}`;
const DISPLAY_Workspace = `{
  allWorkspaceByStatus {
    workspaceUUID
    workspaceName
    senderQualifier
    APIStatus
    createdOn
    updatedOn
  }
}`;
const getMuiTheme = () =>
  createMuiTheme(Theme, {
    overrides: {
      MuiSelect: {
        select: {
          wordBreak: 'break-all !important',
        }
      },
      MuiTypography: {
        body1: {
          fontSize: 10,
          fontWeight: 400,
        },
      },
      MuiInputLabel: {
        outlined: {
          fontSize: 10,
        },
      },
    },
  });

export default function AddProjectForm(props) {
  const {
    loading: wloading,
    data: wdata,
    errors: werrors,
    networkError: wnetworkError,
  } = useQuery(DISPLAY_Workspace);
  const [
    updateAS2Profile,
    {
      loading: uploading,
      data: updata,
      errors: uperrors,
      networkError: unetworkError,
    },
  ] = useMutation(UPDATE_AS2profile);
  //ADD as2form
  const [addAS2Form, addloading, adddata] = useMutation(ADD_AS2profile);
  const [openModal, setOpenModal] = React.useState(false);
  const [workspaceData, setWorksapceData] = React.useState([]);
  const [values, setValues] = React.useState({
    workspaceUUID: '',
    connectorId: '',
    connectorDescription: '',
    as2Identifier: '',
    partnerURL: '',
    signSendData: true,
    encryptSendData: true,
    requireEncryption: true,
    requireSign: true,
    requestMDNReceipt: null,
    Security: 'Signed',
    delivery: 'Synchronous',
    encryptionCertificate: false,
    inputFolder: '',
    outputFolder: '',
    processedFolder: '',
    as2Restart: null,
    enableAs2Reliability: null,
    streaming: null,
    userprofile: null,
    tlsPrivateCertificate: false,
    localAs2Identifier: '',
    //certificatePassword:'',
    certificatePassword: '',
    HTTPAuthenticationType: 'Basic',
    user: '',
    password: '',
    characterset: '',
    headersName: '',
    headersValue: '',
    maxWorkers: '',
    maxFiles: '',
    asyncMDNTimeout: '60',
    duplicateFileAction: '',
    duplicateFileInterval: '',
    encryptionAlgorithm: '',
    extensionMap: '',
    HTTPSubject: '',
    logLevel: '',
    logRequests: null,
    messageId: '',
    parentConnector: '',
    parseFDAExtensions: null,
    partnerSigningCertificate: '',
    processingDelay: '',
    signatureAlgorithm: '',
    SSLv2: false,
    SSLv3: false,
    TLSv10: true,
    TLSv11: true,
    TLSv12: true,
    TLSv13: false,
    tempReceiveDirectory: '',
    logMessages: null,
    sendFilter: '',
    savetoSentFolder: null,
  });
  const [ErrMsgNameInfo, setErrMsgNameInfo] = React.useState(false);
  const [ErrMsg, setErrMsg] = React.useState(false);
  const [submitErr, setSubmitErr] = React.useState(false);
  const [errorInfo, setErrorInfo] = React.useState({
    workspaceUUID: false,
    connectorId: false,
    connectorDescription: false,
    as2Identifier: false,
    partnerURL: false,
    inputFolder: false,
    outputFolder: false,
    processedFolder: false,
    // certificatePassword:false,
    certificatePassword: false,
    localAs2Identifier: false,
    HTTPAuthenticationType: false,
    user: false,
    password: false,
    characterset: false,
    headersName: false,
    headersValue: false,
    maxWorkers: false,
    maxFiles: false,
    asyncMDNTimeout: false,
    duplicateFileAction: false,
    duplicateFileInterval: false,
    encryptionAlgorithm: false,
    extensionMap: false,
    HTTPSubject: false,
    messageId: false,
    parentConnector: false,
    partnerSigningCertificate: false,
    processingDelay: false,
    signatureAlgorithm: false,
    sendFilter: false,
  });
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [variant, setSnackbarVariant] = React.useState('error');
  const [message, setSnackbarMessage] = React.useState();

  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }
  useEffect(() => {
    console.log(props.rowData);
    if (props.rowData != undefined && props.title != 'Add') {
      console.log(props.rowData, props.rowData.organizationUnit);

      setValues({
        workspaceUUID: props.rowData.workspaceUUID,
        connectorId: props.rowData.connectorID,
        connectorDescription: props.rowData.connectorDescription,
        as2Identifier: props.rowData.as2Identifier,
        partnerURL: props.rowData.partnerURL,
        signSendData: props.rowData.sendMessageSecurity.includes(
          'Sign send data'
        )
          ? true
          : false,
        encryptSendData: props.rowData.sendMessageSecurity.includes(
          'Encrypt send data'
        )
          ? true
          : false,
        requireEncryption: props.rowData.receiveMessageSecurity.includes(
          'Require Signature'
        )
          ? true
          : false,
        requireSign: props.rowData.receiveMessageSecurity.includes(
          'Require Encryption'
        )
          ? true
          : false,
        requestMDNReceipt: props.rowData.requestMDNReceipt,
        Security: props.rowData.security,
        delivery: props.rowData.delivery,
        //  encryptionCertificate: false,
        //  inputFolder: '',
        //  outputFolder: '',
        //  processedFolder: '',
        as2Restart: props.rowData.as2Restart,
        enableAs2Reliability: props.rowData.as2Reliability,
        streaming: props.rowData.streaming,
        //  tlsPrivateCertificate: false,
        //   localAs2Identifier: '',
        //certificatePassword:'',
        userprofile: props.rowData.userProfileSettings,
        certificatePassword: props.rowData.TLScertificatePassword,
        HTTPAuthenticationType: props.rowData.HTTPAuthenticationType,
        user: props.rowData.user,
        password: props.rowData.password,
        // characterset: '',
        headersName: props.rowData.headersName,
        headersValue: props.rowData.headersValue,
        // maxWorkers: '',
        //  maxFiles: '',
        asyncMDNTimeout: props.rowData.asyncMDNTimeout,
        duplicateFileAction: props.rowData.duplicateFileAction,
        duplicateFileInterval: props.rowData.duplicateFileInterval,
        encryptionAlgorithm: props.rowData.encryptionAlgorithm,
        extensionMap: props.rowData.extensionMap,
        HTTPSubject: props.rowData.hTTPSubject,
        logLevel: props.rowData.logLevel,
        logRequests: props.rowData.logRequests,
        messageId: props.rowData.messageId,
        parentConnector: props.rowData.parentConnector,
        parseFDAExtensions: props.rowData.parseFDAExtensions,
        partnerSigningCertificate: props.rowData.partnerSigningCertificate,
        processingDelay: props.rowData.processingDelay,
        signatureAlgorithm: props.rowData.signatureAlgorithm,
        SSLv2: props.rowData.TLSenabledProtocols.includes('SSLv2')
          ? true
          : false,
        SSLv3: props.rowData.TLSenabledProtocols.includes(' SSLv3')
          ? true
          : false,
        TLSv10: props.rowData.TLSenabledProtocols.includes('TLSv1.0')
          ? true
          : false,
        TLSv11: props.rowData.TLSenabledProtocols.includes(' TLSv1.1')
          ? true
          : false,
        TLSv12: props.rowData.TLSenabledProtocols.includes('TLSv1.2')
          ? true
          : false,
        TLSv13: props.rowData.TLSenabledProtocols.includes('TLSv1.3')
          ? true
          : false,
        tempReceiveDirectory: props.rowData.tempReceiveDirectory,
        logMessages: props.rowData.logMessages,
        sendFilter: props.rowData.sendFilter,
        savetoSentFolder: props.rowData.savetoSentFolder,
      });
    }
    if (wdata !== undefined) {
      console.log(wdata.allWorkspaceByStatus);
      setWorksapceData(wdata.allWorkspaceByStatus);
    }
  }, [props, wdata]);
  const handleChange = (name) => (event) => {
    console.log(
      event.target.value,
      event.target.name,
      event.target.checked,
      name
    );
    setErrMsg(false);

    setValues({ ...values, [name]: event.target.value });
    switch (name) {
      case 'connectorId':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^([A-Z0-9])*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'connectorDescription':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('')) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;

      case 'as2Identifier':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[a-zA-Z0-9]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;

      case 'partnerURL':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match('^(http(s)?://)?([a-z][a-zA-Z0-9./]*)?$')
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'sendMessageSecurity':
        if (event.target.checked) {
          setValues({ ...values, [name]: event.target.name });
        } else {
          setValues({ ...values, [name]: null });
        }

        break;
      case 'signSendData':
      case 'encryptSendData':
      case 'requireSign':
      case 'requireEncryption':
      case 'SSLv2':
      case 'SSLv3':
      case 'TLSv10':
      case 'TLSv11':
      case 'TLSv12':
      case 'TLSv13':
        setValues({ ...values, [event.target.name]: event.target.checked });
        break;

      case 'Security':
        setValues({
          ...values,
          [event.target.name]: event.target.value,
        });
        break;
      case 'delivery':
        setValues({
          ...values,
          [event.target.name]: event.target.value,
        });
        break;
      case 'encryptionCertificate':
        setValues({ ...values, [name]: event.target.value });
        console.log(values);
        if (values) {
          setOpenModal(true);
        }
        break;
      case 'inputFolder':
      case 'processedFolder':
      case 'outputFolder':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[/]{1}[A-Za-z0-9/]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'localAs2Identifier':
      case 'extensionMap':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('')) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'profileCertificatePassword':
      case 'tlsCertificatePassword':
      case 'password':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match(
            '^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,15}$'
          )
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'requestMDNReceipt':
      case 'userprofile':
      case 'streaming':
      case 'as2Restart':
      case 'enableAs2Reliability':
      case 'logRequests':
      case 'parseFDAExtensions':
      case 'logMessages':
      case 'savetoSentFolder':
        if (event.target.checked) {
          setValues({ ...values, [name]: event.target.name });
        } else {
          setValues({ ...values, [name]: null });
        }

        break;
      case 'user':
      case 'messageId':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[A-Z0-9]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'characterset':
      case 'headersName':
      case 'HTTPSubject':
      case 'parentConnector':
      case 'partnerSigningCertificate':
      case 'tempReceiveDirectory':
      case 'sendFilter':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[a-zA-Z0-9/. ]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
        } else setErrorInfo({ ...errorInfo, [name]: true });
        break;
      case 'headersValue':
      case 'maxWorkers':
      case 'maxFiles':
      case 'asyncMDNTimeout':
      case 'duplicateFileInterval':
      case 'processingDelay':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[-(0-9)]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
        } else setErrorInfo({ ...errorInfo, [name]: true });
        break;
      case 'workspaceUUID':
      case 'duplicateFileAction':
      case 'encryptionAlgorithm':
      case 'logLevel':
      case 'signatureAlgorithm':
        setValues({
          ...values,
          [event.target.name]: event.target.value,
        });
        break;
      case 'tlsPrivateCertificate':
        setValues({ ...values, [name]: event.target.value });
        console.log(values);
        if (values) {
          setOpenModal(true);
        }
        break;
      case 'FTPHost':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match(/^[a-zA-Z0-9./ ]+(\.\w{2,})+$/)) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      default:
        return false;
    }
  };

  const handleCloseForm = () => {
    setOpenModal(false);
    setValues({ ...values, encryptionCertificate: false });
  };
  const handleCloseModal = () => {
    props.handleCloseModal(false);
  };
  const handleSubmit = async () => {
    if (
      values.workspaceUUID !== '' &&
      values.connectorId !== '' &&
     // values.connectorDescription !== '' &&
      //values.as2Identifier !== '' &&
      //values.partnerURL !== '' &&
      //values.inputFolder !== '' &&
      // values.outputFolder !== '' &&
      //values.processedFolder !== '' &&
      //values.localAs2Identifier!== '' &&
      // values.profileCertificatePassword!== '' &&
      //values.tlsCertificatePassword !== '' &&
      //values.HTTPAuthenticationType !== '' &&
      // values.user !== '' &&
      //values.password !== '' &&
      //  values.characterset !== '' &&
      //values.headersName !== '' &&
      //values.headersValue !== '' &&
      // values.maxWorkers!== '' &&
      // values.maxFiles!== '' &&
      //values.asyncMDNTimeout !== '' &&
      //values.duplicateFileAction !== '' &&
      //values.duplicateFileInterval !== '' &&
      //values.encryptionAlgorithm !== '' &&
      // values.extensionMap !== '' &&
      //   values.HTTPSubject !== '' &&
      //  values.messageId !== '' &&
      // values.parentConnector !== '' &&
      //values.partnerSigningCertificate !== '' &&
      //values.processingDelay !== '' &&
      //values.signatureAlgorithm !== '' &&
      //values.sendFilter !== '' &&
      errorInfo.workspaceUUID !== true &&
      errorInfo.connectorId !== true &&
      errorInfo.connectorDescription !== true &&
      errorInfo.as2Identifier !== true &&
      errorInfo.partnerURL !== true &&
      errorInfo.inputFolder !== true &&
      errorInfo.outputFolder !== true &&
      errorInfo.processedFolder !== true &&
      //errorInfo.localAs2Identifier !== true &&
      //errorInfo.profileCertificatePassword !== true &&
      errorInfo.tlsCertificatePassword !== true &&
      errorInfo.HTTPAuthenticationType !== true &&
      //   errorInfo.user !== true &&
      errorInfo.password !== true &&
      // errorInfo.characterset !== true &&
      // errorInfo.headersName !== true &&
      //errorInfo.headersValue !== true &&
      // errorInfo.maxWorkers !== true &&
      //errorInfo.maxFiles !== true &&
      errorInfo.asyncMDNTimeout !== true &&
      errorInfo.duplicateFileAction !== true &&
      errorInfo.duplicateFileInterval !== true &&
      errorInfo.encryptionAlgorithm !== true &&
      //   errorInfo.extensionMap !== true &&
      //errorInfo.HTTPSubject !== true &&
      //errorInfo.messageId !== true &&
      //errorInfo.parentConnector !== true &&
      errorInfo.partnerSigningCertificate !== true &&
      errorInfo.processingDelay !== true &&
      errorInfo.signatureAlgorithm !== true &&
      errorInfo.sendFilter !== true
    ) {
      setErrMsg(false);
      setValues({
        ...values,

        connectorId: values.connectorId,
        connectorDescription: values.connectorDescription,
        as2Identifier: values.as2Identifier,
        partnerURL: values.partnerURL,
        inputFolder: values.inputFolder,
        outputFolder: values.outputFolder,
        processedFolder: values.processedFolder,
        //localAs2Identifier: values.localAs2Identifier,
        //profileCertificatePassword: values.profileCertificatePassword,
        tlsCertificatePassword: values.tlsCertificatePassword,
        HTTPAuthenticationType: values.HTTPAuthenticationType,
        user: values.user,
        password: values.password,
        // characterset: values.characterset,
        headersName: values.headersName,
        headersValue: values.headersValue,
        //maxWorkers:values.maxWorkers,
        //maxFiles:values.maxFiles,
        asyncMDNTimeout: values.asyncMDNTimeout,
        duplicateFileAction: values.duplicateFileAction,
        duplicateFileInterval: values.duplicateFileInterval,
        encryptionAlgorithm: values.encryptionAlgorithm,
        extensionMap: values.extensionMap,
        HTTPSubject: values.HTTPSubject,
        logLevel: values.logLevel,
        logRequests: values.logRequests,
        messageId: values.messageId,
        parentConnector: values.parentConnector,
        parseFDAExtensions: values.parseFDAExtensions,
        partnerSigningCertificate: values.partnerSigningCertificate,
        processingDelay: values.processingDelay,
        signatureAlgorithm: values.signatureAlgorithm,
        SSLv2: values.SSLv2,
        SSLv3: values.SSLv3,
        TLSv10: values.TLSv10,
        TLSv11: values.TLSv11,
        TLSv12: values.TLSv12,
        TLSv13: values.TLSv13,
        tempReceiveDirectory: values.tempReceiveDirectory,
        logMessages: values.logMessages,
        sendFilter: values.sendFilter,
        savetoSentFolder: values.savetoSentFolder,
        workspaceUUID: values.workspaceUUID,
      });
      console.log('submit form.......done 540');
      function getTrueKeys(obj) {
        var ret = [];
        for (var key in obj) {
          if (obj.hasOwnProperty(key) && obj[key] === true) {
            ret.push(key);
          }
        }
        return ret;
      }
      let sendMessageSecurity = {
        'Sign send data': values.signSendData,
        'Encrypt send data': values.signSendData,
      };

      let sendMessage = getTrueKeys(sendMessageSecurity).join(', ');
      let receiveMessageSecurity = {
        'Require Signature': values.requireEncryption,
        'Require Encryption': values.requireSign,
      };
      let TradingPartnerCertificate = {
        SSLv2: values.SSLv2,
        SSLv3: values.SSLv3,
        'TLSv1.0': values.TLSv10,
        'TLSv1.1': values.TLSv11,
        'TLSv1.2': values.TLSv12,
        'TLSv1.3': values.TLSv13,
      };
      let TradingPartner = getTrueKeys(TradingPartnerCertificate).join(', ');
      let receiveMessage = getTrueKeys(receiveMessageSecurity).join(', ');
      console.log(getTrueKeys(receiveMessageSecurity));
      console.log(getTrueKeys(receiveMessageSecurity).join(', '));
      //  console.log(ans);
      console.log(receiveMessageSecurity);
      //console.log(key);
      if (props.title == 'Add') {
        await addAS2Form({
          variables: {
            AS2ProfileInput: {
              connectorID: values.connectorId,
              connectorDescription: values.connectorDescription,
              as2Identifier: values.as2Identifier,
              partnerURL: values.partnerURL,
              streaming: values.streaming,
              sendMessageSecurity: sendMessage,
              receiveMessageSecurity: receiveMessage,
              requestMDNReceipt: values.requestMDNReceipt,
              security: values.Security,
              delivery: values.delivery,
              //  encryptionCertificate: values.encryptionCertificate,
              as2Restart: values.as2Restart,
              as2Reliability: values.enableAs2Reliability,
              //  inputFolder: values.inputFolder,
              //  outputFolder: values.outputFolder,
              //  processedFolder: values.processedFolder,
              userProfileSettings: values.userprofile,
              //private certifiacate
              TLScertificatePassword: values.tlsCertificatePassword,
              HTTPAuthenticationType: values.HTTPAuthenticationType,
              user: values.user,
              password: values.password,
              headersName: values.headersName,
              headersValue: values.headersValue,
              asyncMDNTimeout: values.asyncMDNTimeout,
              duplicateFileAction: values.duplicateFileAction,
              duplicateFileInterval: values.duplicateFileInterval,
              encryptionAlgorithm: values.encryptionAlgorithm,
              extensionMap: values.extensionMap,
              hTTPSubject: values.HTTPSubject,
              logLevel: values.logLevel,
              logRequests: values.logRequests,
              messageId: values.messageId,
              parentConnector: values.parentConnector,
              parseFDAExtensions: values.parseFDAExtensions,
              partnerSigningCertificate: values.partnerSigningCertificate,
              processingDelay: values.processingDelay,
              signatureAlgorithm: values.signatureAlgorithm,
              TLSenabledProtocols: TradingPartner,
              tempReceiveDirectory: values.tempReceiveDirectory,
              logMessages: values.logMessages,
              sendFilter: values.sendFilter,
              savetoSentFolder: values.savetoSentFolder,
              workspaceUUID: values.workspaceUUID,
            },
          },
        })
          .then(
            ({
              data,
              error,
              graphQLErrors,
              networkError,
              cacheHit,
              loading,
            }) => {
              console.log(
                data,
                error,
                graphQLErrors,
                networkError,
                cacheHit,
                loading
              );
              if (data !== null && error == false) {
                console.log(' created successfully');
                setErrMsg(false);

                props.onSuccess(error, 'Saved successfully', true);
              } else {
                props.onSuccess(error, 'Graphql hooks error', false);
              }
            }
          )
          .catch((e) => {
            // you can do something with the error here
            console.log(e);
          });
      } else {
        //update functionality
        await updateAS2Profile({
          variables: {
            AS2ProfileInput: {
              connectorID: values.connectorId,
              connectorDescription: values.connectorDescription,
              as2Identifier: values.as2Identifier,
              partnerURL: values.partnerURL,
              streaming: values.streaming,
              sendMessageSecurity: sendMessage,
              receiveMessageSecurity: receiveMessage,
              requestMDNReceipt: values.requestMDNReceipt,
              security: values.Security,
              delivery: values.delivery,
              //  encryptionCertificate: values.encryptionCertificate,
              as2Restart: values.as2Restart,
              as2Reliability: values.enableAs2Reliability,
              //  inputFolder: values.inputFolder,
              //  outputFolder: values.outputFolder,
              //  processedFolder: values.processedFolder,
              userProfileSettings: values.userprofile,
              //private certifiacate
              TLScertificatePassword: values.tlsCertificatePassword,
              HTTPAuthenticationType: values.HTTPAuthenticationType,
              user: values.user,
              password: values.password,
              headersName: values.headersName,
              headersValue: values.headersValue,
              asyncMDNTimeout: values.asyncMDNTimeout,
              duplicateFileAction: values.duplicateFileAction,
              duplicateFileInterval: values.duplicateFileInterval,
              encryptionAlgorithm: values.encryptionAlgorithm,
              extensionMap: values.extensionMap,
              hTTPSubject: values.HTTPSubject,
              logLevel: values.logLevel,
              logRequests: values.logRequests,
              messageId: values.messageId,
              parentConnector: values.parentConnector,
              parseFDAExtensions: values.parseFDAExtensions,
              partnerSigningCertificate: values.partnerSigningCertificate,
              processingDelay: values.processingDelay,
              signatureAlgorithm: values.signatureAlgorithm,
              TLSenabledProtocols: TradingPartner,
              tempReceiveDirectory: values.tempReceiveDirectory,
              logMessages: values.logMessages,
              sendFilter: values.sendFilter,
              savetoSentFolder: values.savetoSentFolder,
              workspaceUUID: values.workspaceUUID,
              as2ProfileUUID: props.rowData.as2ProfileUUID,
            },
          },
        })
          .then(
            ({
              data,
              error,
              graphQLErrors,
              networkError,
              cacheHit,
              loading,
            }) => {
              console.log(
                data,
                error,
                graphQLErrors,
                networkError,
                cacheHit,
                loading
              );
              if (data !== null && error == false) {
                console.log(' Updated successfully');
                setErrMsg(false);

                props.onSuccess(error, 'Updated successfully', true);
              } else {
                props.onSuccess(error, 'Graphql hooks error', false);
              }
            }
          )
          .catch((e) => {
            // you can do something with the error here
            console.log(e);
          });
      }
    } else if (
      values.connectorId == '' &&
      values.connectorDescription == '' &&
      values.as2Identifier == '' &&
      values.partnerURL == '' &&
      values.inputFolder == '' &&
      values.outputFolder == '' &&
      values.processedFolder == '' &&
      //values.localAs2Identifier == '' &&
      //values.profileCertificatePassword == '' &&
      values.tlsCertificatePassword == '' &&
      values.HTTPAuthenticationType == '' &&
      // values.user == '' &&
      values.password == '' &&
      // values.characterset == '' &&
      // values.headersName == '' &&
      //values.headersValue == '' &&
      //values.maxWorkers== '' &&
      //values.maxFiles== '' &&
      values.asyncMDNTimeout == '' &&
      values.duplicateFileAction == '' &&
      values.duplicateFileInterval == '' &&
      values.encryptionAlgorithm == '' &&
      // values.extensionMap == '' &&
      //  values.HTTPSubject == '' &&
      //values.messageId == '' &&
      //values.parentConnector == '' &&
      values.partnerSigningCertificate == '' &&
      values.processingDelay == '' &&
      values.signatureAlgorithm == '' &&
      values.sendFilter == ''
    ) {
      setErrMsg(true);
      console.log('submit form592.......');
    } else {
      setErrMsg(false);
      setSubmitErr(true);
      console.log('submit form596.......');
    }
  };
  const handleReset = async () => {
    setErrMsg(false);
    setErrorInfo({
      connectorDescription: false,
      as2Identifier: false,
      partnerURL: false,
      inputFolder: false,
      outputFolder: false,
      processedFolder: false,
      //profileCertificatePassword:false,
      tlsCertificatePassword: false,
      localAs2Identifier: false,
      HTTPAuthenticationType: false,
      user: false,
      password: false,
      characterset: false,
      headersName: false,
      headersValue: false,
      maxWorkers: false,
      maxFiles: false,
      asyncMDNTimeout: false,
      duplicateFileAction: false,
      duplicateFileInterval: false,
      encryptionAlgorithm: false,
      extensionMap: false,
      HTTPSubject: false,
      messageId: false,
      parentConnector: false,
      partnerSigningCertificate: false,
      processingDelay: false,
      signatureAlgorithm: false,
      sendFilter: false,
    });
    setValues({
      ...values,
      connectorDescription: '',
      as2Identifier: '',
      partnerURL: '',
      inputFolder: '',
      outputFolder: '',
      processedFolder: '',
      as2Restart: null,
      enableAs2Reliability: null,
      streaming: null,
      userprofile: null,
      localAs2Identifier: '',
      profileCertificatePassword: '',
      tlsCertificatePassword: '',
      HTTPAuthenticationType: 'Basic',
      user: '',
      password: '',
      characterset: '',
      headersName: '',
      headersValue: '',
      maxWorkers: '',
      maxFiles: '',
      asyncMDNTimeout: '60',
      duplicateFileAction: '',
      duplicateFileInterval: '',
      encryptionAlgorithm: '',
      extensionMap: '',
      HTTPSubject: '',
      logLevel: '',
      logRequests: true,
      messageId: '',
      parentConnector: '',
      parseFDAExtensions: false,
      partnerSigningCertificate: '',
      processingDelay: '',
      signatureAlgorithm: '',
      SSLv2: false,
      SSLv3: false,
      TLSv10: true,
      TLSv11: true,
      TLSv12: true,
      TLSv13: false,
      tempReceiveDirectory: '',
      logMessages: true,
      sendFilter: '',
      savetoSentFolder: true,
    });
    setSubmitErr(false);
    // setNameError(false);
  };
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={props.isModalOpen}
        onClose={handleCloseModal}
        className="fabmodal"
      >
        <div className="modalpaper">
          <Grid container className="header">
            <Grid item xs={10} sm={10}>
              <h2 id="modal-title" className="h2">
                AS2 Profile
              </h2>
            </Grid>
            <Grid item xs={2} sm={2} className="close">
              <CloseIcon onClick={handleCloseModal} />
            </Grid>
          </Grid>
          <div>
            {ErrMsg === true ? (
              <div style={{ textAlign: 'center' }} className="addfieldserror">
                Please fill the fields
              </div>
            ) : (
              ''
            )}
            <form className="X12content" noValidate autoComplete="off">
              <Grid >
                <Grid container spacing={2} className="userformcontent">
                  <Grid item xs={12} sm={4}  className="interchangedropdown">
                    <FormControl>
                      <InputLabel
                        style={{ color: '#0b153e', fontSize: 12 }}
                        id="demo-simple-select-label"
                      >
                        Work Space
                      </InputLabel>
                      <Select className="dropdown"
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={values.workspaceUUID}
                        onChange={handleChange('workspaceUUID')}
                        name="workspaceUUID"
                      >
                        {workspaceData != undefined && workspaceData.length > 0
                          ? workspaceData.map((v, k) => {
                              return (
                                <MenuItem value={v.workspaceUUID}>
                                  {v.workspaceName}
                                </MenuItem>
                              );
                            })
                          : null}
                      </Select>
                    </FormControl>
                    {submitErr && !values.workspaceUUID ? (
                      <div id="nameid" className="addfieldserror">
                        Please select workspace
                      </div>
                    ) : (
                      ''
                    )}
                  </Grid>
                  <Grid item xs={12} sm={4}>
                    <TextField
                      id="outlined-dense"
                      label="Connector ID"
                      className="partnertextField"
                      margin="dense"
                      variant="outlined"
                      onChange={handleChange('connectorId')}
                      value={values.connectorId}
                      name="connectorId"
                    />

                    {errorInfo.connectorId === true ? (
                      <div className="addfieldserror">
                        "Allowed uppercase alphabets and numbers"
                      </div>
                    ) : (
                      ''
                    )}
                      {submitErr && !values.connectorId ? (
                      <div id="nameid" className="addfieldserror">
                        Please enter connector Id
                      </div>
                    ) : (
                      ''
                    )}
                  </Grid>
                  <Grid item xs={12} sm={4}>
                    <TextField
                      id="outlined-dense"
                      label="Connector Description "
                      className="partnertextField"
                      margin="dense"
                      variant="outlined"
                      onChange={handleChange('connectorDescription')}
                      value={values.connectorDescription}
                      name="connectorDescription"
                    />
                    {/*
                    {errorInfo.name === true ? (
                    <div className="addfieldserror">
                        "Enter valid IP Address"
                    </div>
                    ) : (
                    ''
                    )}
                   
                    {submitErr && !values.connectorDescription ? (
                      <div id="nameid" className="addfieldserror">
                        Please enter description
                      </div>
                    ) : (
                      ''
                    )} */}
                  </Grid>
                  <Grid item xs={12} sm={4}>
                    <TextField
                      id="outlined-dense"
                      label="AS2 Identifier"
                      className="partnertextField"
                      margin="dense"
                      variant="outlined"
                      onChange={handleChange('as2Identifier')}
                      value={values.as2Identifier}
                      name="as2Identifier"
                    />
                    {errorInfo.as2Identifier === true ? (
                      <div className="addfieldserror">
                        "Enter only Alphanumeric Characters "
                      </div>
                    ) : (
                      ''
                    )}

                   {/* {submitErr && !values.as2Identifier ? (
                      <div id="nameid" className="addfieldserror">
                        Please enter AS2 Identifier
                      </div>
                    ) : (
                      ''
                    )} */}
                  </Grid>

                  <Grid item xs={12} sm={4}>
                    <TextField
                      id="outlined-dense"
                      label="Partner URL"
                      className="partnertextField"
                      margin="dense"
                      variant="outlined"
                      onChange={handleChange('partnerURL')}
                      value={values.partnerURL}
                      name="partnerURL"
                    />
                    {errorInfo.partnerURL === true ? (
                      <div className="addfieldserror">"Enter valid url "</div>
                    ) : (
                      ''
                    )}
                    {/* {submitErr && !values.partnerURL ? (
                      <div id="nameid" className="addfieldserror">
                        Please enter url
                      </div>
                    ) : (
                      ''
                    )}*/}
                  </Grid>
                </Grid>

                <Grid item xs={12} sm={12}>
                  <p className="advancedFrom" style={{ color: '#0b153e' }}>
                    Connection Info
                  </p>
                </Grid>
                <Grid container spacing={3} className="userformcontent1">
                  <Grid item xs={12} sm={6}>
                    <Typography
                      className="Advancedheader"
                      style={{ color: '#0b153e' }}
                    >
                      Send Message Security
                    </Typography>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <Typography
                      className="Advancedheader"
                      style={{ color: '#0b153e' }}
                    >
                      Receive Message Security
                    </Typography>
                  </Grid>
                </Grid>

                <Grid container spacing={3}>
                  <Grid item xs={12} sm={3}>
                    <Typography className="Advancedtext">
                      <Checkbox
                        inputProps={{
                          'aria-label': 'uncontrolled-checkbox',
                        }}
                        value={values.signSendData}
                        checked={values.signSendData}
                        name="signSendData"
                        onChange={handleChange('signSendData')}
                        color="primary"
                      />
                      Sign send data
                    </Typography>
                  </Grid>
                  <Grid item xs={12} sm={3}>
                    <Typography className="Advancedtext">
                      <Checkbox
                        inputProps={{
                          'aria-label': 'uncontrolled-checkbox',
                        }}
                        value={values.encryptSendData}
                        checked={values.encryptSendData}
                        name="encryptSendData"
                        onChange={handleChange('encryptSendData')}
                        color="primary"
                      />
                      Encrypt send data
                    </Typography>
                  </Grid>
                  <Grid item xs={12} sm={3}>
                    <Typography className="Advancedtext">
                      <Checkbox
                        inputProps={{
                          'aria-label': 'uncontrolled-checkbox',
                        }}
                        value={values.requireSign}
                        checked={values.requireSign}
                        name="requireSign"
                        onChange={handleChange('requireSign')}
                        color="primary"
                      />
                      Require Signature
                    </Typography>
                  </Grid>

                  <Grid item xs={12} sm={3}>
                    <Typography className="Advancedtext">
                      <Checkbox
                        inputProps={{
                          'aria-label': 'uncontrolled-checkbox',
                        }}
                        value={values.requireEncryption}
                        checked={values.requireEncryption}
                        name="requireEncryption"
                        onChange={handleChange('requireEncryption')}
                        color="primary"
                      />
                      Require Encryption
                    </Typography>
                  </Grid>
                </Grid>

                <Grid item xs={12} sm={6}>
                  <Typography className="Advancedheader">
                    MDN Receipt(Request)
                  </Typography>
                </Grid>
                <Grid item xs={12} sm={6}>
                  <Typography className="Advancedtext">
                    <Checkbox
                      inputProps={{
                        'aria-label': 'uncontrolled-checkbox',
                      }}
                      value={values.requestMDNReceipt != null ? true : false}
                      checked={values.requestMDNReceipt != null ? true : false}
                      name=" Request MDN Receipt"
                      onChange={handleChange('requestMDNReceipt')}
                      color="primary"
                    />
                    Request MDN Receipt
                  </Typography>
                </Grid>
                <Grid container spacing={3}>
                  <Grid item xs={12} sm={6}>
                    <Typography
                      className="Advancedheader"
                      style={{ color: '#0b153e' }}
                    >
                      Security
                    </Typography>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <Typography
                      className="Advancedheader"
                      style={{ color: '#0b153e' }}
                    >
                      Delivery
                    </Typography>
                  </Grid>
                </Grid>
                <Grid container spacing={3}>
                  <Grid item xs={12} sm={6}>
                    <RadioGroup
                      aria-label="  Security"
                      name="Security"
                      color="primary"
                      value={values.Security}
                      onChange={handleChange('Security')}
                      row
                    >
                      <Grid item xs={6} sm={6}>
                        <FormControlLabel
                          className="Advancedtext"
                          value="Signed"
                          color="primary"
                          control={<Radio />}
                          label="Signed"
                        />
                      </Grid>
                      <Grid item xs={6} sm={6}>
                        <FormControlLabel
                          value="Unsigned"
                          control={<Radio color="primary" />}
                          label="Unsigned"
                        />
                      </Grid>
                    </RadioGroup>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <RadioGroup
                      aria-label="Delivery"
                      name="delivery"
                      color="primary"
                      value={values.delivery}
                      onChange={handleChange('delivery')}
                      row
                    >
                      <Grid item xs={6} sm={6}>
                        <FormControlLabel
                          value="Synchronous"
                          color="primary"
                          control={<Radio />}
                          label="Synchronous"
                        />
                      </Grid>
                      <Grid item xs={6} sm={6}>
                        <FormControlLabel
                          value="Asynchronous"
                          control={<Radio color="primary" />}
                          label="Asynchronous"
                        />
                      </Grid>
                    </RadioGroup>
                  </Grid>
                </Grid>

                <Grid container spacing={2}>
                  <Grid item xs={12} sm={12}>
                    <p className="advancedFrom" style={{ color: '#0b153e' }}>
                      Trading Partner Certificate
                    </p>
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={3}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="interchangedropdown"
                  >
                    <FormControl>
                      <InputLabel
                        style={{ color: '#0b153e', fontSize: 12 }}
                        id="demo-simple-select-label"
                      >
                        Encryption Certificate
                      </InputLabel>
                      <Select className="dropdown"
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={values.encryptionCertificate}
                        onChange={handleChange('encryptionCertificate')}
                        name="encryptionCertificate"
                      >
                        <MenuItem value={true}>+ upload certificate</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} sm={3}>
                    <Typography className="Advancedheader">
                      Streaming
                    </Typography>
                    <Typography className="Advancedtext">
                      <Checkbox
                        inputProps={{
                          'aria-label': 'uncontrolled-checkbox',
                        }}
                        value={values.streaming != null ? true : false}
                        checked={values.streaming != null ? true : false}
                        name="Enable Streaming-(HTTP chunked transfer encoding)"
                        onChange={handleChange('streaming')}
                        color="primary"
                      />
                      Enable Streaming-(HTTP chunked transfer encoding)
                    </Typography>
                  </Grid>

                  <Grid item xs={12} sm={3}>
                    <Typography className="Advancedheader">
                      AS2 Restart
                    </Typography>
                    <Typography className="Advancedtext">
                      <Checkbox
                        inputProps={{
                          'aria-label': 'uncontrolled-checkbox',
                        }}
                        value={values.as2Restart != null ? true : false}
                        checked={values.as2Restart != null ? true : false}
                        name="Enable AS2 Restart"
                        onChange={handleChange('as2Restart')}
                        color="primary"
                      />
                      Enable AS2 Restart
                    </Typography>
                  </Grid>
                  <Grid item xs={12} sm={3}>
                    <Typography className="Advancedheader">
                      AS2 Reliablity
                    </Typography>
                    <Typography className="Advancedtext">
                      <Checkbox
                        inputProps={{
                          'aria-label': 'uncontrolled-checkbox',
                        }}
                        value={
                          values.enableAs2Reliability != null ? true : false
                        }
                        checked={
                          values.enableAs2Reliability != null ? true : false
                        }
                        name="AS2 Reliability"
                        onChange={handleChange('enableAs2Reliability')}
                        color="primary"
                      />
                      AS2 Reliability
                    </Typography>
                  </Grid>
                  {/* <Grid item xs={12} sm={12}>
        <p className="advancedForm"style={{ color: '#0b153e' }}>
                   Local Folders
         </p>
                </Grid>
                  <Grid item xs={12} sm={4}>
                    <TextField
                      style={{ padding: '2px' }}
                      id="outlined-dense"
                      label="Input Folder(Send) "
                      className="partnertextField"
                      margin="dense"
                      variant="outlined"
                      onChange={handleChange('inputFolder')}
                      value={values.inputFolder}
                      name="inputFolder"
                    />

                    {errorInfo.inputFolder === true ? (
                      <div className="addfieldserror">
                        "Start with / followed alphanumeric"
                      </div>
                    ) : (
                      ''
                    )}
                    {submitErr && !values.inputFolder ? (
                      <div id="nameid" className="addfieldserror">
                        Please enter Input Folder
                      </div>
                    ) : (
                      ''
                    )}
                  </Grid>

                  <Grid item xs={12} sm={4}>
                    <TextField
                      id="outlined-dense"
                      label="Output Folder(Receiver) "
                      className="partnertextField"
                      margin="dense"
                      variant="outlined"
                      onChange={handleChange('outputFolder')}
                      value={values.outputFolder}
                      name="outputFolder"
                    />

                    {errorInfo.outputFolder === true ? (
                      <div className="addfieldserror">
                        "Start with / followed alphanumeric"
                      </div>
                    ) : (
                      ''
                    )}
                    {submitErr && !values.outputFolder ? (
                      <div id="nameid" className="addfieldserror">
                        Please enter Output Folder
                      </div>
                    ) : (
                      ''
                    )}
                  </Grid>
                  <Grid item xs={12} sm={4}>
                    <TextField
                      id="outlined-dense"
                      label="Processed Folder(Sent) "
                      className="partnertextField"
                      margin="dense"
                      variant="outlined"
                      onChange={handleChange('processedFolder')}
                      value={values.processedFolder}
                      name="processedFolder"
                    />

                    {errorInfo.processedFolder === true ? (
                      <div className="addfieldserror">
                        "Start with / followed alphanumeric"
                      </div>
                    ) : (
                      ''
                    )}
                    {submitErr && !values.processedFolder ? (
                      <div id="nameid" className="addfieldserror">
                        Please enter Processed Folder
                      </div>
                    ) : (
                      ''
                    )}
                  </Grid>
                  {/*   <Grid item xs={12} sm={12}>
        <p className="advancedForm"style={{ color: '#0b153e' }}>
                   Alternate Local Profile
         </p>
     </Grid>
        <Grid  item xs={12} sm={4}>
        <TextField
          id="outlined-dense"
          label="Local AS2 Identifier "
          className="partnertextField"
          margin="dense"
          variant="outlined"
          onChange={handleChange('localAs2Identifier')}
          value={values.localAs2Identifier}
          name="localAs2Identifier"
        />

        {errorInfo.localAs2Identifier === true ? (
          <div className="addfieldserror">
            "enter localas2identifier"
          </div>
        ) : (
          ''
        )}
         {submitErr && !values.localAs2Identifier ? (
                    <div id="nameid" className="addfieldserror">
                      Please enter Local AS2 Identifier
                    </div>
                  ) : (
                    ''
                  )}
        </Grid> 
        <Grid item xs={12} sm={4}>
                <FormControl>
                      <InputLabel
                        style={{ color: '#0b153e', fontSize: 12 }}
                        id="demo-simple-select-label"
                      >
                        Private Certificate
                      </InputLabel>
                      <Select className="dropdown"
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={values.profilePrivateCertificate}
                        onChange={handleChange('profilePrivateCertificate')}
                        name="profilePrivateCertificate"
                      >
                        <MenuItem value={true}><AddIcon/>upload certificate</MenuItem>
                      </Select>
                    </FormControl>
                </Grid>
                <Grid  item xs={12} sm={4}>
            <TextField
            id="outlined-dense"
            label="Certificate Password "
            className="partnertextField"
            margin="dense"
            type="password"
            variant="outlined"
            onChange={handleChange('profileCertificatePassword')}
            value={values.profileCertificatePassword}
            name="profileCertificatePassword"profileCertificatePassword
            />

            {errorInfo.profileCertificatePassword === true ? (
            <div className="addfieldserror">
                " 8 to 15 characters must have atleast one[uppercase,lowercase,special character and Number]"
            </div>
            ) : (
            ''
            )}
             {submitErr && !values.profileCertificatePassword ? (
                    <div id="nameid" className="addfieldserror">
                      Please enter valid Password
                    </div>
                  ) : (
                    ''
                  )}
            </Grid> 
            <Grid item xs={12} sm={12}>
            <p className="advancedForm"style={{ color: '#0b153e' }}>
            TLS Client Authentication
         </p>
            
                  </Grid>*/}
                  <Grid item xs={12} sm={4}>
                    <Typography className="Advancedheader">
                      User Profile Settings :
                    </Typography>
                    <Typography className="Advancedtext">
                      <Checkbox
                        inputProps={{
                          'aria-label': 'uncontrolled-checkbox',
                        }}
                        value={values.userprofile != null ? true : false}
                        checked={values.userprofile != null ? true : false}
                        name="Use private certificate from the Profile tab"
                        onChange={handleChange('userprofile')}
                        color="primary"
                      />
                      Use private certificate from the Profile tab
                    </Typography>
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={4}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="interchangedropdown"
                  >
                    <FormControl>
                      <InputLabel
                        style={{ color: '#0b153e', fontSize: 12 }}
                        id="demo-simple-select-label"
                      >
                        Private Certificate
                      </InputLabel>
                      <Select className="dropdown"
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={values.tlsPrivateCertificate}
                        onChange={handleChange('tlsPrivateCertificate')}
                        name="tlsPrivateCertificate"
                      >
                        <MenuItem value={true}>
                          <AddIcon />
                          upload certificate
                        </MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item xs={12} sm={4}>
                    <TextField
                      id="outlined-dense"
                      label="Certificate Password "
                      className="partnertextField"
                      margin="dense"
                      type="password"
                      variant="outlined"
                      onChange={handleChange('tlsCertificatePassword')}
                      value={values.tlsCertificatePassword}
                      name="tlsCertificatePassword"
                    />

                    {errorInfo.tlsCertificatePassword === true ? (
                      <div className="addfieldserror">
                        " 8 to 15 characters must have atleast
                        one[uppercase,lowercase,special character and Number]"
                      </div>
                    ) : (
                      ''
                    )}
                        {/* {submitErr && !values.tlsCertificatePassword ? (
                      <div id="nameid" className="addfieldserror">
                        Please enter valid Password
                      </div>
                    ) : (
                      ''
                    )}*/}
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={4}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
                    <Typography className="Advancedheader">
                      HTTP Authentication Type:
                    </Typography>
                    <RadioGroup
                      aria-label="SettingsForma"
                      name="HTTPAuthenticationType"
                      color="primary"
                      value={values.HTTPAuthenticationType}
                      onChange={handleChange('HTTPAuthenticationType')}
                      row
                    >
                      <Grid item xs={6} sm={6}>
                        <FormControlLabel
                          value="Basic"
                          className="Advancedtext"
                          color="primary"
                          control={<Radio />}
                          label="Basic"
                        />
                      </Grid>
                      <Grid item xs={6} sm={6}>
                        <FormControlLabel
                          value="Digest"
                          className="Advancedtext"
                          control={<Radio color="primary" />}
                          label="Digest"
                        />
                      </Grid>
                    </RadioGroup>
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={4}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
                    <TextField
                      id="outlined-dense"
                      label="User "
                      className="partnertextField"
                      margin="dense"
                      variant="outlined"
                      onChange={handleChange('user')}
                      value={values.user}
                      name="user"
                    />
                    {/*   {errorInfo.user === true ? (
                      <div id="nameid" className="addfieldserror">
                        "Enter only alphabets"
                      </div>
                    ) : (
                      ''
                    )}
                    {submitErr && !values.user ? (
                      <div id="nameid" className="addfieldserror">
                        Please enter user
                      </div>
                    ) : (
                      ''
                    )} */}
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={4}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
                    <TextField
                      id="outlined-dense"
                      label="Password "
                      type="password"
                      className="partnertextField"
                      margin="dense"
                      variant="outlined"
                      onChange={handleChange('password')}
                      value={values.password}
                      name="password"
                    />
                    {errorInfo.password === true ? (
                      <div id="nameid" className="addfieldserror">
                        "4 to 8 characters must have atleast
                        one[uppercase,lowercase,special character and number]"
                      </div>
                    ) : (
                      ''
                    )}
                       {/*  {submitErr && !values.password ? (
                      <div id="nameid" className="addfieldserror">
                        Please enter password
                      </div>
                    ) : (
                      ''
                    )}*/}
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={12}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                  >
                    <p className="advancedFrom" style={{ color: '#0b1352' }}>
                      {' '}
                      Custom Headers:
                    </p>
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={4}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
                    <TextField
                      id="outlined-dense"
                      label="Name"
                      className="partnertextField"
                      margin="dense"
                      variant="outlined"
                      //placeholder=""
                      onChange={handleChange('headersName')}
                      value={values.headersName}
                      name="headersName"
                    />
                    {/*    {errorInfo.headersName === true ? (
                      <div id="nameid" className="addfieldserror">
                        "Enter only alphabets"
                      </div>
                    ) : (
                      ''
                    )}
                    {submitErr && !values.headersName ? (
                      <div id="nameid" className="addfieldserror">
                        Please enter Name
                      </div>
                    ) : (
                      ''
                    )}*/}
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={4}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
                    <TextField
                      id="outlined-dense"
                      label="Value"
                      className="partnertextField"
                      margin="dense"
                      variant="outlined"
                      //placeholder=""
                      onChange={handleChange('headersValue')}
                      value={values.headersValue}
                      name="headersValue"
                    />
                    {/*  {errorInfo.headersValue === true ? (
                      <div id="nameid" className="addfieldserror">
                        "Enter only alphanumeric characters"
                      </div>
                    ) : (
                      ''
                    )}
                    {submitErr && !values.headersValue ? (
                      <div id="nameid" className="addfieldserror">
                        Please enter value
                      </div>
                    ) : (
                      ''
                    )}*/}
                  </Grid>
                  {/*    <Grid
                    item
                    xs={12}
                    sm={4}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
                    <Button
                      // onClick={handleReset}
                      variant="contained"
                      fullWidth="true"
                      className="createpartnerbutton"
                    >
                      Add Header
                    </Button>
              </Grid>*/}
                  {/* <Grid
                  item
                  xs={12}
                  sm={12}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <p className="advancedForm" style={{color:"#0b1352"}}> Performance:</p>
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={6}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <TextField
                    id="outlined-dense"
                    label="Max Workers"
                    className="partnertextField"
                    margin="dense"
                    variant="outlined"
                    //placeholder=""
                    onChange={handleChange('maxWorkers')}
                    value={values.maxWorkers}
                    name="maxWorkers"
                  />
                  {errorInfo.maxWorkers === true ? (
                    <div id="nameid" className="addfieldserror">
                      "Enter only numeric digits "
                    </div>
                  ) : (
                    ''
                  )}
                  {submitErr && !values.maxWorkers ? (
                    <div id="nameid" className="addfieldserror">
                      Please enter Max Workers
                    </div>
                  ) : (
                    ''
                  )}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={6}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <TextField
                    id="outlined-dense"
                    label="Max Files"
                    className="partnertextField"
                    margin="dense"
                    variant="outlined"
                    //placeholder=""
                    onChange={handleChange('maxFiles')}
                    value={values.maxFiles}
                    name="maxFiles"
                  />
                  {errorInfo.maxFiles === true ? (
                    <div id="nameid" className="addfieldserror">
                      "Enter only numeric digits "
                    </div>
                  ) : (
                    ''
                  )}
                  {submitErr && !values.maxFiles ? (
                    <div id="nameid" className="addfieldserror">
                      Please enter Max Files
                    </div>
                  ) : (
                    ''
                  )}
                  </Grid>*/}
                  <Grid
                    item
                    xs={12}
                    sm={12}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
                    <p className="advancedFrom" style={{ color: '#0b1352' }}>
                      {' '}
                      Other settings:
                    </p>
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={3}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
                    <TextField
                      id="outlined-dense"
                      label="Async MDN Timeout(seconds)"
                      className="partnertextField"
                      margin="dense"
                      variant="outlined"
                      //placeholder=""
                      onChange={handleChange('asyncMDNTimeout')}
                      value={values.asyncMDNTimeout}
                      name="asyncMDNTimeout"
                    />
                    {errorInfo.asyncMDNTimeout === true ? (
                      <div id="nameid" className="addfieldserror">
                        "Enter only numeric digits "
                      </div>
                    ) : (
                      ''
                    )}
                        {/* {submitErr && !values.asyncMDNTimeout ? (
                      <div id="nameid" className="addfieldserror">
                        Please enter async MDN Timeout
                      </div>
                    ) : (
                      ''
                    )}*/}
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={3}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="interchangedropdown"
                  >
                    <FormControl>
                      <InputLabel
                        style={{ color: '#0b153e', fontSize: 12 }}
                        id="demo-simple-select-label"
                      >
                        Duplicate File Action
                      </InputLabel>
                      <Select className="dropdown"
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={values.duplicateFileAction}
                        onChange={handleChange('duplicateFileAction')}
                        name="duplicateFileAction"
                      >
                        <MenuItem value={'Continue'}>Continue</MenuItem>
                        <MenuItem value={'Warning'}>Warning</MenuItem>
                        <MenuItem value={'Warning-Ignore File'}>
                          Warning-Ignore File
                        </MenuItem>
                        <MenuItem value={'Failure'}>Failure</MenuItem>
                      </Select>
                    </FormControl>
                         {/*{submitErr && !values.duplicateFileAction ? (
                      <div id="nameid" className="addfieldserror">
                        Please select Duplicate File Action
                      </div>
                    ) : (
                      ''
                    )}*/}
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={3}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
                    <TextField
                      id="outlined-dense"
                      label="Duplicate File Interval"
                      className="partnertextField"
                      margin="dense"
                      variant="outlined"
                      //placeholder=""
                      onChange={handleChange('duplicateFileInterval')}
                      value={values.duplicateFileInterval}
                      name="duplicateFileInterval"
                    />
                    {errorInfo.duplicateFileInterval === true ? (
                      <div id="nameid" className="addfieldserror">
                        "Enter only numeric digits "
                      </div>
                    ) : (
                      ''
                    )}
                        {/* {submitErr && !values.duplicateFileInterval ? (
                      <div id="nameid" className="addfieldserror">
                        Please enter Duplicate File Interval
                      </div>
                    ) : (
                      ''
                    )}*/}
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={3}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="interchangedropdown"
                  >
                    <FormControl>
                      <InputLabel
                        style={{ color: '#0b153e', fontSize: 12 }}
                        id="demo-simple-select-label"
                      >
                        Encryption Algorithm
                      </InputLabel>
                      <Select className="dropdown"
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={values.encryptionAlgorithm}
                        onChange={handleChange('encryptionAlgorithm')}
                        name="encryptionAlgorithm"
                      >
                        <MenuItem value={'3DES'}>3DES</MenuItem>
                        <MenuItem value={'RC2'}>RC2</MenuItem>
                        <MenuItem value={'RC2CBC40'}>RC2CBC40</MenuItem>
                        <MenuItem value={'RC2CBC64'}>RC2CBC64</MenuItem>
                        <MenuItem value={'RC2CBC128'}>RC2CBC128</MenuItem>
                        <MenuItem value={'DES'}>DES</MenuItem>
                        <MenuItem value={'AES'}>AES</MenuItem>
                        <MenuItem value={'AESCBC192'}>AESCBC192</MenuItem>
                        <MenuItem value={'AESCBC256'}>AESCBC256</MenuItem>
                      </Select>
                    </FormControl>
                        {/* {submitErr && !values.encryptionAlgorithm ? (
                      <div id="nameid" className="addfieldserror">
                        Please select Encryption Algorithm
                      </div>
                    ) : (
                      ''
                    )}*/}
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={3}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
                    <TextField
                      id="outlined-dense"
                      label="Extension Map"
                      className="partnertextField"
                      margin="dense"
                      variant="outlined"
                      //placeholder=""
                      onChange={handleChange('extensionMap')}
                      value={values.extensionMap}
                      name="extensionMap"
                    />
                    {/*  {errorInfo.extensionMap === true ? (
                      <div id="nameid" className="addfieldserror">
                        "Enter only Alphanumeric characters "
                      </div>
                    ) : (
                      ''
                    )}
                    {submitErr && !values.duplicateFileInterval ? (
                      <div id="nameid" className="addfieldserror">
                        Please enter Extension Map
                      </div>
                    ) : (
                      ''
                    )}*/}
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={3}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
                    <TextField
                      id="outlined-dense"
                      label="HTTP Subject"
                      className="partnertextField"
                      margin="dense"
                      variant="outlined"
                      //placeholder=""
                      onChange={handleChange('HTTPSubject')}
                      value={values.HTTPSubject}
                      name="HTTPSubject"
                    />
                    {/*    {errorInfo.HTTPSubject === true ? (
                      <div id="nameid" className="addfieldserror">
                        "Enter only Alphanumeric characters "
                      </div>
                    ) : (
                      ''
                    )}
                    {submitErr && !values.HTTPSubject ? (
                      <div id="nameid" className="addfieldserror">
                        Please enter HTTP Subject
                      </div>
                    ) : (
                      ''
                    )}*/}
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={3}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="interchangedropdown"
                  >
                    <FormControl>
                      <InputLabel
                        style={{ color: '#0b153e', fontSize: 12 }}
                        id="demo-simple-select-label"
                      >
                        Log Level
                      </InputLabel>
                      <Select className="dropdown"
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={values.logLevel}
                        onChange={handleChange('logLevel')}
                        name="logLevel"
                      >
                        <MenuItem value={'None'}>None</MenuItem>
                        <MenuItem value={'Error'}>Error</MenuItem>
                        <MenuItem value={'Warning'}>Warning</MenuItem>
                        <MenuItem value={'Info'}>Info</MenuItem>
                        <MenuItem value={'Debug'}>Debug</MenuItem>
                        <MenuItem value={'Trace'}>Trace</MenuItem>
                      </Select>
                    </FormControl>
                         {/*{submitErr && !values.logLevel ? (
                      <div id="nameid" className="addfieldserror">
                        Please select log Level
                      </div>
                    ) : (
                      ''
                    )}*/}
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={3}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
                    {' '}
                    <Typography className="Advancedtext">
                      <Checkbox
                        inputProps={{
                          'aria-label': 'uncontrolled-checkbox',
                        }}
                        value={values.logRequests != null ? true : false}
                        checked={values.logRequests != null ? true : false}
                        name="Log requests"
                        onChange={handleChange('logRequests')}
                        color="primary"
                      />
                      Log requests
                    </Typography>
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={3}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
                    <TextField
                      id="outlined-dense"
                      label="Message Id"
                      className="partnertextField"
                      margin="dense"
                      variant="outlined"
                      //placeholder=""
                      onChange={handleChange('messageId')}
                      value={values.messageId}
                      name="messageId"
                    />
                    {/*   {errorInfo.messageId === true ? (
                      <div id="nameid" className="addfieldserror">
                        "Enter only Alphanumeric characters "
                      </div>
                    ) : (
                      ''
                    )}
                    {submitErr && !values.messageId ? (
                      <div id="nameid" className="addfieldserror">
                        Please enter Message Id
                      </div>
                    ) : (
                      ''
                    )}*/}
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={3}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
                    <TextField
                      id="outlined-dense"
                      label="Parent Connector"
                      className="partnertextField"
                      margin="dense"
                      variant="outlined"
                      //placeholder=""
                      onChange={handleChange('parentConnector')}
                      value={values.parentConnector}
                      name="parentConnector"
                    />
                    {/*   {errorInfo.parentConnector === true ? (
                      <div id="nameid" className="addfieldserror">
                        "Enter only Alphanumeric characters "
                      </div>
                    ) : (
                      ''
                    )}
                    {submitErr && !values.parentConnector ? (
                      <div id="nameid" className="addfieldserror">
                        Please enter parent connector
                      </div>
                    ) : (
                      ''
                    )}*/}
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={3}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
                    <Typography className="Advancedtext">
                      <Checkbox
                        inputProps={{
                          'aria-label': 'uncontrolled-checkbox',
                        }}
                        value={values.parseFDAExtensions != null ? true : false}
                        checked={
                          values.parseFDAExtensions != null ? true : false
                        }
                        name="Parse FDA extensions"
                        onChange={handleChange('parseFDAExtensions')}
                        color="primary"
                      />
                      Parse FDA extensions
                    </Typography>
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={3}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="interchangedropdown"
                  >
                    <FormControl>
                      <InputLabel
                        style={{ color: '#0b153e', fontSize: 12 }}
                        id="demo-simple-select-label"
                      >
                        Partner Signing Certificate
                      </InputLabel>
                      <Select className="dropdown"
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={values.partnerSigningCertificate}
                        onChange={handleChange('partnerSigningCertificate')}
                        name="partnerSigningCertificate"
                      >
                        <MenuItem value={'None'}>None</MenuItem>
                      </Select>
                    </FormControl>
                        {/* {submitErr && !values.partnerSigningCertificate ? (
                      <div id="nameid" className="addfieldserror">
                        Please select Partner Signing Certificate
                      </div>
                    ) : (
                      ''
                    )}*/}
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={3}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
                    <TextField
                      id="outlined-dense"
                      label="Processing Delay"
                      className="partnertextField"
                      margin="dense"
                      variant="outlined"
                      //placeholder=""
                      onChange={handleChange('processingDelay')}
                      value={values.processingDelay}
                      name="processingDelay"
                    />
                    {errorInfo.processingDelay === true ? (
                      <div id="nameid" className="addfieldserror">
                        "Enter only numeric digits "
                      </div>
                    ) : (
                      ''
                    )}
                        {/* {submitErr && !values.processingDelay ? (
                      <div id="nameid" className="addfieldserror">
                        Please enter processing delay
                      </div>
                    ) : (
                      ''
                    )}*/}
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={3}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="interchangedropdown"
                  >
                    <FormControl>
                      <InputLabel
                        style={{ color: '#0b153e', fontSize: 12 }}
                        id="demo-simple-select-label"
                      >
                        Signature Algorithm
                      </InputLabel>
                      <Select className="dropdown"
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={values.signatureAlgorithm}
                        onChange={handleChange('signatureAlgorithm')}
                        name="signatureAlgorithm"
                      >
                        <MenuItem value={'SHA1'}>SHA1</MenuItem>
                        <MenuItem value={'MD5'}>MD5</MenuItem>
                        <MenuItem value={'SHA256'}>SHA256</MenuItem>
                        <MenuItem value={'SHA384'}>SHA384</MenuItem>
                        <MenuItem value={'SHA512'}>SHA512</MenuItem>
                        <MenuItem value={'SHA224'}>SHA224</MenuItem>
                      </Select>
                    </FormControl>
                        {/* {submitErr && !values.signatureAlgorithm ? (
                      <div id="nameid" className="addfieldserror">
                        Please select Partner Signing Certificate
                      </div>
                    ) : (
                      ''
                    )}*/}
                  </Grid>
                  <Grid container spacing={2}>
                    <Grid
                      item
                      xs={12}
                      sm={12}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <Typography className="Advancedheader">
                        {' '}
                        TLS Enabled Protocols:
                      </Typography>
                    </Grid>
                    <Grid item xs={2} sm={2}>
                      <Typography className="Advancedtext">
                        <Checkbox
                          inputProps={{
                            'aria-label': 'uncontrolled-checkbox',
                          }}
                          value={values.SSLv2}
                          checked={values.SSLv2}
                          name="SSLv2"
                          onChange={handleChange('SSLv2')}
                          color="primary"
                        />
                        SSLv2
                      </Typography>
                    </Grid>
                    <Grid item xs={2} sm={2}>
                      <Typography className="Advancedtext">
                        <Checkbox
                          inputProps={{
                            'aria-label': 'uncontrolled-checkbox',
                          }}
                          value={values.SSLv3}
                          checked={values.SSLv3}
                          name="SSLv3"
                          onChange={handleChange('SSLv3')}
                          color="primary"
                        />
                        SSLv3
                      </Typography>
                    </Grid>

                    <Grid item xs={2} sm={2}>
                      <Typography className="Advancedtext">
                        <Checkbox
                          inputProps={{
                            'aria-label': 'uncontrolled-checkbox',
                          }}
                          value={values.TLSv10}
                          checked={values.TLSv10}
                          name="TLSv10"
                          onChange={handleChange('TLSv10')}
                          color="primary"
                        />
                        TLSv1.0
                      </Typography>
                    </Grid>
                    <Grid item xs={2} sm={2}>
                      <Typography className="Advancedtext">
                        <Checkbox
                          inputProps={{
                            'aria-label': 'uncontrolled-checkbox',
                          }}
                          value={values.TLSv11}
                          checked={values.TLSv11}
                          name="TLSv11"
                          onChange={handleChange('TLSv11')}
                          color="primary"
                        />
                        TLSv1.1
                      </Typography>
                    </Grid>
                    <Grid item xs={2} sm={2}>
                      <Typography className="Advancedtext">
                        <Checkbox
                          inputProps={{
                            'aria-label': 'uncontrolled-checkbox',
                          }}
                          value={values.TLSv12}
                          checked={values.TLSv12}
                          name="TLSv12"
                          onChange={handleChange('TLSv12')}
                          color="primary"
                        />
                        TLSv1.2
                      </Typography>
                    </Grid>
                    <Grid item xs={2} sm={2}>
                      <Typography className="Advancedtext">
                        <Checkbox
                          inputProps={{
                            'aria-label': 'uncontrolled-checkbox',
                          }}
                          value={values.TLSv13}
                          checked={values.TLSv13}
                          name="TLSv13"
                          onChange={handleChange('TLSv13')}
                          color="primary"
                        />
                        TLSv1.3
                      </Typography>
                    </Grid>

                    <Grid
                      item
                      xs={12}
                      sm={3}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <TextField
                        id="outlined-dense"
                        label="Temp Receive Directory"
                        className="partnertextField"
                        margin="dense"
                        variant="outlined"
                        //placeholder=""
                        onChange={handleChange('tempReceiveDirectory')}
                        value={values.tempReceiveDirectory}
                        name="tempReceiveDirectory"
                      />
                      {errorInfo.tempReceiveDirectory === true ? (
                        <div id="nameid" className="addfieldserror">
                          "Enter Alphanumeric characters "
                        </div>
                      ) : (
                        ''
                      )}
                          {/* {submitErr && !values.tempReceiveDirectory ? (
                        <div id="nameid" className="addfieldserror">
                          Please enter Temp Receive Directory
                        </div>
                      ) : (
                        ''
                      )}*/}
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={2}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <Typography className="Advancedtext">
                        <Checkbox
                          inputProps={{
                            'aria-label': 'uncontrolled-checkbox',
                          }}
                          value={values.logMessages != null ? true : false}
                          checked={values.logMessages != null ? true : false}
                          name="Log messages"
                          onChange={handleChange('logMessages')}
                          color="primary"
                        />
                        Log messages
                      </Typography>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={3}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <TextField
                        id="outlined-dense"
                        label="Send Filter"
                        className="partnertextField"
                        margin="dense"
                        variant="outlined"
                        //placeholder=""
                        onChange={handleChange('sendFilter')}
                        value={values.sendFilter}
                        name="sendFilter"
                      />
                      {errorInfo.sendFilter === true ? (
                        <div id="nameid" className="addfieldserror">
                          "Enter Alphanumeric characters "
                        </div>
                      ) : (
                        ''
                      )}
                           {/*{submitErr && !values.sendFilter ? (
                        <div id="nameid" className="addfieldserror">
                          Please enter Send Filter
                        </div>
                      ) : (
                        ''
                      )}*/}
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={2}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <Typography className="Advancedtext">
                        <Checkbox
                          inputProps={{
                            'aria-label': 'uncontrolled-checkbox',
                          }}
                          value={values.savetoSentFolder != null ? true : false}
                          checked={
                            values.savetoSentFolder != null ? true : false
                          }
                          name="Save to Sent folder"
                          onChange={handleChange('savetoSentFolder')}
                          color="primary"
                        />
                        Save to Sent folder
                      </Typography>
                    </Grid>
                  </Grid>
                  <Grid container spacing={3}>
                    <Grid
                      item
                      xs={6}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <Button
                        style={{
                          color: '#000006',
                          backgroundColor: '#E5CCFF',
                        }}
                        onClick={handleReset}
                        variant="contained"
                        fullWidth="true"
                        className="createpartnerbutton"
                      >
                        RESET
                      </Button>
                    </Grid>

                    <Grid
                      item
                      xs={6}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <Button
                        onClick={handleSubmit}
                        variant="contained"
                        fullWidth="true"
                        className="createpartnerbutton"
                      >
                        {props.title !== 'Add' ? 'UPDATE' : 'SAVE'}
                      </Button>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </form>
            <Snackbar
              anchorOrigin={{
                vertical: 'top',
                horizontal: 'center',
              }}
              autoHideDuration={5000}
              open={openSnackbar}
              onClose={handleCloseSnackbar}
            >
              <MySnackbarContentWrapper
                onClose={handleCloseSnackbar}
                variant={variant}
                message={message}
              />
            </Snackbar>
            <UploadCertificateModel
              handleCloseModal={handleCloseForm}
              isModalOpen={openModal}
            >
              <UploadCertificateForm
                handleCloseModal={handleCloseForm}
                // onSuccess={handleRefetch}
                isModalOpen={openModal}
              />
            </UploadCertificateModel>
          </div>
        </div>
      </Modal>
    </MuiThemeProvider>
  );
}
