import React from 'react';
import { useMutation, useQuery } from 'graphql-hooks';
import { forwardRef, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import { Grid, Typography } from '@material-ui/core';
import Theme from '../css/theme';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import Loader from '../components/loader';
import Error from '../components/emptyPage';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import  AuthContext  from "../authUtil/Auth";

const DISPLAY_ORGANIZATION_FROM_USERUUID = `query fetchOrganizationFromRoleAssignment($userUUID:String){
  fetchOrganizationFromRoleAssignment(userUUID:$userUUID) {
    idUserRoleAssignment
    userRoleAssignmentUUID
    userUUID
    userRoleUUID
    userOrgUUID
    userTypeUUID
    createdOn
    lastUpdatedOn
    organizatonName
  }
}
`;
const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: '12px',
    fontWeight: theme.typography.fontWeightRegular,
    flexBasis: '33.33%',
    flexShrink: 0,
    color: '#000000',
  },

  secondaryHeading: {
    fontSize: '12px',
    color: '#000000',
  },
}));
export default function FetchUserOrganization(props) {
  const { dispatch } = React.useContext(AuthContext);
  const classes = useStyles();
  const [value, setValue] = React.useState();
  const [ErrMsg, setErrMsg] = React.useState(false);
  const [checkOrg, setCheckOrg] = React.useState(false);

  const [serverError, setServerError] = React.useState(false);
  const [Organizationdata, setData] = React.useState([]);
  const { loading, error, data, refetch } = useQuery(
    DISPLAY_ORGANIZATION_FROM_USERUUID,
    {
      variables: {
        userUUID: props.userUUIDdata,
      },
    }
  );

  useEffect(() => {
    console.log();
    if (data !== undefined) {
      console.log(data.fetchOrganizationFromRoleAssignment);
      setData(data.fetchOrganizationFromRoleAssignment);
      setServerError(false);
    } else {
      console.log('server error');
      setServerError(true);
    }
  }, [data]);

  const handleChange = (event) => {
    console.log("Radio",event,"value",event.target.value,checkOrg)
   
    setValue(event.target.value);
  };
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiGrid: {
          container: {
            //paddingLeft:'50%',
            //marginTop:'2%'
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          },
        },
      },
    });
    const handleSubmit = async () => {
      console.log("Radio",value,checkOrg)
    if(value)
    { setErrMsg(false);
      //Add Procced functionality
      dispatch.userOrgDispatch({
        type: "FETCH_USER_ORG_RESPONSE",
        payload: value
      })
    
      props.handleProceed()
     
    }
    else
    {
      setErrMsg(true);
    }
    }
    const handleLogoutFn = () => {
      props.handleLogout();
    };
  if (loading) return <Loader />;
  if (serverError)
    return <Error type={'Server connection lost.Please try agian'} />;

  return (
    <div>
      <MuiThemeProvider theme={getMuiTheme()}>
             <Grid >
               <div style={{display:'flex',
                            justifyContent: 'flex-end'}}>
                <Tooltip title="Logout" >
                <IconButton>
                  <ExitToAppIcon style={{ color: '#0b153e' }} onClick={handleLogoutFn}/>
                </IconButton>
              </Tooltip>
              </div>
            </Grid>
           {Organizationdata !== undefined && Organizationdata.length > 0 ? (
          <div>
            <Grid container spacing={2}>
              <FormControl component="fieldset">
              {ErrMsg === true ? (
                <div style={{ textAlign: 'center' }} className="addfieldserror">
                Please select one among the list
                </div>
              ) : (
                ''
              )}
                <Grid
                  item
                  xs={12}
                  sm={12}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography
                    style={{
                      cursor: 'pointer',
                      textAlign: 'left',
                      fontFamily: 'roboto,arial,sans-serif',
                      marginBottom: '10px',
                      color:"#0b153e"
                    }}
                  >
                    Please select a organization
                  </Typography>
                  <FormLabel component="legend"></FormLabel>
                  <RadioGroup
                    aria-label="Organization"
                    name="Organization"
                    value={value}
                    onChange={handleChange}
                  >
                    {Organizationdata.map((v, k) => {
                      return (
                        <FormControlLabel
                        
                          value={v.userOrgUUID}
                          control={<Radio />}
                          label={v.organizatonName}
                        />
                      );
                    })}
                  </RadioGroup>
                </Grid> 
                <Grid>
                <Button
                onClick={handleSubmit} 
                 style={{
                marginLeft: "auto", 
                display: "flex",
                marginTop:'60px',
                marginBottom:'50px',
                }}
                variant="contained" color="primary"  >
                Proceed
                </Button>
                </Grid>
               
              </FormControl>
            </Grid>
            </div>
     ) : (
          <div className={classes.root}>
            <center>
              <Paper className="paperorgpage">
                Organizations are not added
              </Paper>
            </center>
          </div>
        )} 
      </MuiThemeProvider>
    </div>
  );
}
