import React, { useEffect, useContext, useCallback } from 'react';
import { useMutation, useQuery, useManualQuery,ClientContext  } from 'graphql-hooks';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import Theme from '../css/theme';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import '../css/commonStyle.css';
import Link from '@material-ui/core/Link';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import dataUseContext from '../Context/useContext';
import AuthContext  from "../authUtil/Auth";
import Loader from '../components/loader';
import Error from '../components/emptyPage';
import Imagepath from '../images/changemaker.png';
import Paper from '@material-ui/core/Paper';

const LOGIN_USER = `mutation loginUser($userID:String,$userPassword:String)
{loginUser(userID:$userID,userPassword:$userPassword){
  loginType
  userUUID
  tokenVal
}}`;
const VERIFY_LOGIN_USER = `mutation verifyUser($tokenValverify:String)
{loginUser(tokenValverify:$tokenValverify){
  loginType
  userUUID
}}`;

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

const getMuiTheme = () =>
  createMuiTheme(Theme, {
    overrides: {
        MuiInputBase:{
            input:{
                marginBottom:'10px',
                marginTop:'13px'
        }},
        MuiOutlinedInput:{
          inputMarginDense:{
          paddingBottom:"0 !important"
        }
      }
    },
  });

export default function LoginForm(props) {
 const { dispatch } = React.useContext(AuthContext);
  const initialState = {
    email: "",
    password: "",
    isSubmitting: false,
    errorMessage: null
  };
  const [dataUser, setDataUser] = React.useState(initialState);
  localStorage.setItem('token','');
  const client = useContext(ClientContext);
  console.log("client",client)
  const [values, setValues] = React.useState({ userID: '',userPassword:''});
  const [ErrMsg, setErrMsg] = React.useState(false);
  const [errorInfo, setErrorInfo] = React.useState({
    userID: false,
    userPassword:false
  });
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [variant, setSnackbarVariant] = React.useState('error');
  const [message, setSnackbarMessage] = React.useState();
  const [loginUser, loading,data,errors] = useMutation(LOGIN_USER);
  const [verifyLoginUser, {loading:vLULoading,data:vLUData,errors:vLUErrors}] = useMutation(VERIFY_LOGIN_USER);
  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }

  useEffect(()=>{

  },)
  

  const handleChange = (name) => (event) => {
    setErrMsg(false);

    setValues({ ...values, [name]: event.target.value });
    switch (name) {
      case 'userID':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        }  else {
            setErrorInfo({ ...errorInfo, [name]: true });
          }
        
        break;
        case 'userPassword':
            if (event.target.value.length === 0) {
              setValues({ ...values, [name]: '' });
              setErrorInfo({ ...errorInfo, [name]: false });
            }  else {
                setErrorInfo({ ...errorInfo, [name]: true });
              }
            
            break;
      default:
        return false;
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault()
    setErrMsg(false);

    if (values.userID === '' || values.userPassword==='') {
      setErrMsg(true);
      setSnackbarMessage(' Please fill the fields')
    } else {
        setErrMsg(false);  
        await loginUser({
            variables: {
              userID: values.userID ,
              userPassword:values.userPassword
            }
        }).then(async({ data, error, graphQLErrors, networkError, cacheHit }) => {
          console.log(data, error, graphQLErrors, networkError, cacheHit);
          if (data !== null && error == false) {
           
            console.log("success")
            setErrMsg(false);
            console.log("data.loginUser",data.loginUser)
            //const { token } = data.loginUser.tokenVal
            console.log("token",data.loginUser.tokenVal)
            localStorage.setItem('token',data.loginUser.tokenVal);
           const headers={'Authorization': `Bearer ${data.loginUser.tokenVal}`}
           console.log("headers",headers)
           client.setHeader('Authorization', `Bearer ${data.loginUser.tokenVal}`)
            //client.setHeaders(headers)
            dispatch.loginDispatch({
              type: "LOGIN",
              payload: data
             })
            /*await verifyLoginUser({
              variables:{
                tokenValverify: localStorage.getItem('token')
              }
            }).then(({vLUData, vLUError})=>{
              console.log("vLUData, vLUError",vLUData, vLUError)
              if (vLUData !== null && vLUError == false) {   
              dispatch({
                type: "LOGIN",
                payload: vLUData
               })
              }
            })*/
            /*dispatch({
              type: "LOGIN",
              payload: data
             })*/
           // props.loginSuccess(true);
           
          
          } else if (error && graphQLErrors.length > 0) {
            setErrMsg(true);
            setSnackbarMessage('Invalid Credentials')
          }
        })
        /*.then(async( res)=>{
          console.log("res",res)
          //if (data !== null && error == false) {            
            console.log("success")
            setErrMsg(false);
            //console.log("data.loginUser",data.loginUser)
              await verifyLoginUser({
                variables:{
                  tokenVal: localStorage.getItem('token')
                }
              }).then(({vLUData, vLUError})=>{
                console.log("vLUData, vLUError",vLUData, vLUError)
                if (vLUData !== null && vLUError == false) {   
                dispatch({
                  type: "LOGIN",
                  payload: data
                 })
                }
              })
           //}
        })*/
        .catch((e) => {
          // you can do something with the error here
          console.log(e);
        });;

       
        
        

        setValues({ userID: '',userPassword:''});
        
    }
  };
  if (vLULoading) return <Loader />;
    if (errors||vLUErrors) return <Error type={'Server connection lost.Please try again'} />;
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
       <div>
         <React.Fragment>
         <Grid contianer spacing={3} className="content" >
         <Container maxWidth="xs"  className="logincontainer"  >
        {ErrMsg === true ? (
          <div style={{ textAlign: 'center'}} className="addfieldserror">
           {message}
          </div>
        ) : (
          ''
        )}
         <Typography variant="h4" noWrap>
                        <img
                          alt="noimage"
                          style={{ width: '50%',marginLeft:"23%" }}
                          src={Imagepath}
                        />
                      </Typography>
        <form className="content" noValidate autoComplete="off">
           <Grid item sm={12} xs={12}>
          <TextField
            id="outlined-dense"
            label="User ID "
            className="partnertextField"
            margin="dense"
            variant="outlined"
           onChange={handleChange('userID')}
           value={values.userID}
            name="userID"
          /></Grid>
             <Grid item sm={12} xs={12}>
             <Link href="#"  color="inherit">
       
       <Typography    color="primary" style={{
             display:'inline-block',
             fontWeight:'bold',
             fontSize:'14px',
             letterSpacing:'.25px',
             backgroundColor:'transparent',
             cursor:'pointer',
             textAlign:'left',
             fontFamily:'roboto,arial,sans-serif'
             }}>Forgot UserID?</Typography>
     
      </Link>
      </Grid>
      <Grid item sm={12} xs={12}>
           <TextField
            id="outlined-dense"
            label="Enter your password "
            className="partnertextField"
            margin="dense"
            variant="outlined"
            type="password"
            onChange={handleChange('userPassword')}
            value={values.userPassword}
            name="userPassword"
          /></Grid>
           <Grid item sm={12} xs={12}>
         <Link href="#"  color="inherit">
       
          <Typography  color="primary" style={{
                display:'inline-block',
                fontWeight:'bold',
                fontSize:'14px',
                letterSpacing:'.25px',
                backgroundColor:'transparent',
                cursor:'pointer',
                textAlign:'left',
                fontFamily:'roboto,arial,sans-serif'
                }}>Forgot password?</Typography>
        
         </Link>
         </Grid>
         <Grid item sm={12} xs={12}>
          <Button
            onClick={handleSubmit} 
             style={{
               marginTop:'12px',
               marginBottom:'12px',
                marginLeft: "auto", 
                display: "flex",
                fontWeight:'bold',
                fontSize:'14px',
                letterSpacing:'.25px',
                fontFamily:'arial,sans-serif',
                }}
                 variant="contained" color="primary" size='large' >
          LOGIN
         </Button>
         </Grid>
        </form>
        </Container>
        <Grid><Typography  gutterBottom align="center" style={{color:"#0bl53e",marginTop:'2%',fontSize:11,fontWeight:750,padingTop:"20px !important"}}>
      CloudGen © 2020 All Rights Reserved
        </Typography></Grid>
        </Grid>
        </React.Fragment>
        <Snackbar
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
          autoHideDuration={5000}
          open={openSnackbar}
          onClose={handleCloseSnackbar}
        >
          <MySnackbarContentWrapper
            onClose={handleCloseSnackbar}
            variant={variant}
            message={message}
          />
        </Snackbar>
      </div>
    </MuiThemeProvider>
  );
}