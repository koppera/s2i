import React from 'react';
import Fab from '@material-ui/core/Fab';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import AddIcon from '@material-ui/icons/Add';
import '../css/commonStyle.css';
import Modal from '@material-ui/core/Modal';
import Grid from '@material-ui/core/Grid';
import ModalFab from '../components/modalfabtms';
import AddProcess from './addprocess';
import ModalPage from '../components/tmsmodal';

const useStyles = makeStyles({
  card: {
    minWidth: 100,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});


    
  export default function Orgcards(props) {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [openeditproject, setEditProject] = React.useState(false);
  
    const handleOpen = () => {
        setOpen(true);
    };

    let handleClose = () => {
        setOpen(false);
    } 
    const handleeditproject = () => {
        setEditProject(true);
          };
       
          const handleCloseModal = () => {
            setEditProject(false);
          };

  return (
    <div>  
    <Card className="maincard">
      <CardContent>
        <Typography className="cardprojectname" color="textSecondary" gutterBottom align="center">
         Process Management
        </Typography>
      
        <Typography variant="body2" component="p">
        Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
        Lorem Ipsum has been the industry's standard dummy text ever since the 1500
       </Typography>
      </CardContent>
    </Card>
    <ModalFab title="Add Process" >
            <AddProcess  configData={props.ConfigSettingData} location={props.location} {...props}/>
          </ModalFab>    
       
    </div>
    
  );
}