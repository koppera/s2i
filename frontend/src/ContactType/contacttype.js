import MaterialTable from 'material-table';
import { useMutation, useQuery } from 'graphql-hooks';
import { forwardRef, useEffect } from 'react';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import ReactDOM from 'react-dom';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import CloseIcon from '@material-ui/icons/Close';
import Loader from '../components/loader';
import { withStyles } from '@material-ui/core/styles';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import MTableToolbar from 'material-table';
import React, { Component } from 'react';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import OrganisationForm from '../Organisation/organisationModel';
import { Grid, Typography } from '@material-ui/core';
import LibraryBooksIcon from '@material-ui/icons/LibraryBooks';
import ModalFab from '../components/modalfabtms';
import FormControl from '@material-ui/core/FormControl';
import Timestamp from '../timestamp';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import Theme from '../css/theme';
import Error from '../components/emptyPage';
import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles,
} from '@material-ui/core/styles';

const DISPLAY_All_CONTACTTYPE = `{
  allContactsType {
    idContactType
    contactTypeUUID
    name
    description
    createdOn
    lastUpdatedOn
  }   
 }
`;
const ADD_CONTACTTYPE = `mutation createContactType($inputCreate:ContactTypeInput){
  createContactType(input:$inputCreate) {
    idContactType
    contactTypeUUID
    name
    description
    createdOn
    lastUpdatedOn
  }

}
`;
const REMOVE_CONTACTTYPE = `mutation removeAddressType($input:ContactTypeInput){
  removeContactType(input:$input) {
    idContactType
    contactTypeUUID
    name
    description
    createdOn
    lastUpdatedOn
  } 

}
`;
const UPDATE_CONTACTTYPE = `mutation updateAddressTypes($input:ContactTypeInput){
  updateContactType(input:$input) {
    idContactType
    contactTypeUUID
    name
    description
    createdOn
    lastUpdatedOn
  } 

}
`;
function iconStyles() {
  return {};
}
const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(2),
    },
  },

  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));
const styles = (theme) => ({
  root: {
    Width: 1200,
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: '#FFFFFF',
  },
});
/*
const validate = {
  contactTypeUUID: s => (s.length > 3 ? "Name not long enough" : ""),
};
const editComponent = ({ onChange, value, ...rest }) => {
  const [currentValue, setValue] = useState(value);
  const [error, setError] = useState("");
  const change = e => {
    const newValue = e.target.value;
    setValue(newValue);
    const errorMesasge = validate[rest.columnDef.field](newValue);
    setError(errorMesasge);
    if (!errorMesasge) {
      onChange(newValue);
    }
  };
  return (
    <TextField
      {...rest}
      error={error}
      helperText={error}
      value={currentValue}
      onChange={change}
    />
  );
};*/

export default function MaterialTableDemo() {
  //ADD ConatctTypes
  const [addConatctTypes, addloading, adddata] = useMutation(ADD_CONTACTTYPE);
  //FETCH ALL ConatctTypes
  const { loading, error, data, refetch } = useQuery(DISPLAY_All_CONTACTTYPE);

  //remove ConatctTypes
  const [removeConatctTypes, removeloading, removedata] = useMutation(
    REMOVE_CONTACTTYPE
  );
  //update ConatctTypes
  const [updateConatctTypes, updateloading, updatedata] = useMutation(
    UPDATE_CONTACTTYPE
  );
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [message, setSnackbarMessage] = React.useState();
  const [serverError, setServerError] = React.useState(false);
  const [variant, setSnackbarVariant] = React.useState('error');
  const [conatctTypedata, setData] = React.useState([]);
  const [open, setOpen] = React.useState(true);
  const [nameError, setNameError] = React.useState({
    error: false,
    label: "",
    helperText: "",
    validateInput: false,
});
const [state, setState] = React.useState({
 
});

  const [valid, setValid] = React.useState(false);
  const classes1 = useStyles();
  const columns = [
    { title: 'UID', field: 'projectUID', editable: 'never', hidden: true },
    { title: 'Contact Type UUID', field: 'contactTypeUUID', hidden: true },
    {
      title: 'Name',
      field: 'name',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder="Name"
              margin="normal"
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                    ? nameError.error
                    : false
            }
            helperText={
              !props.value &&
              nameError.validateInput &&
              props.rowData.submitted
                  ? nameError.helperText
                  : ""
          }
          
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
              value={props.value}
            />

            <div style={{ color: 'red' }}>
              {!props.rowData.name||
              /^[a-zA-Z.\s]+$/.test(props.rowData.name)
                ? ''
                : 'Enter only alphabets'}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.name}</div>;
      },
    },
    {
      title: 'Description',
      field: 'description',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder="Description"
              margin="normal"
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
              value={props.value}
            />
          </div>
        );
      },
    
      render: (rowData) => {
        return <div>{rowData.description}</div>;
      },
    },
    {
      title: 'Created',
      field: 'createdOn',
      editable: 'never',
      render: (rowData) => {
        console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.createdOn);
        } else {
          return;
        }
      },
    },
    {
      title: 'Last Modified',
      field: 'lastUpdatedOn',
      editable: 'never',
      render: (rowData) => {
        console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.lastUpdatedOn);
        } else {
          return;
        }
      },
    },
  ];
  
  useEffect(() => {
    if (data !== undefined) {
      console.log(data.allContactsType);
      setData(data.allContactsType);
      setServerError(false);
    }
    else {
      console.log('server error');
      setServerError(true);
    }
  }, [data]);
  const handleRefetch = (value, message) => {
    refetch({
      updateData(_, data) {
        return data;
      },
    });
    setOpenSnackbar(true);
    setSnackbarMessage(message);
    if (value) {
      setSnackbarVariant('error');
    } else {
      setSnackbarVariant('success');
    }
  };
  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }
  const classes = makeStyles(iconStyles)();
  const tableIcons = {
    Add: forwardRef((props, ref) => (
      <AddBox color="primary" {...props} ref={ref} />
    )),
    Check: forwardRef((props, ref) => (
      <Check color="primary" {...props} ref={ref} />
    )),
    Clear: forwardRef((props, ref) => (
      <Clear color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline color="error"   {...props} ref={ref} />
    )),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => (
      <Edit
        {...props}
        style={{marginLeft:"90px"}}
        ref={ref}
        color="primary"
        onClick={console.log('hello world')}
      />
    )),
    Export: forwardRef((props, ref) => (
      <SaveAlt color="primary" {...props} ref={ref} />
    )),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => (
      <FirstPage color="primary" {...props} ref={ref} />
    )),
    LastPage: forwardRef((props, ref) => (
      <LastPage color="primary" {...props} ref={ref} />
    )),
    NextPage: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft color="primary" {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => (
      <Clear  {...props} ref={ref} />
    )),
    Search: forwardRef((props, ref) => (
      <Search color="primary" {...props} ref={ref} />
    )),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };

  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiDialogTitle: {
          root: {
            backgroundColor: '#0b153e',
            color: '#FFFFFF',
          },
        },
        MuiSelect: {
          icon:{
            color:'#0b153e'
              }
        },
      
        MuiTypography: {
          h6: {
            fontSize: 14,
            fontFamily: 'Arial !important',
          },
        },

        MuiTablePagination: {
          input: {
            color: '#0b153e',
          },
        },
      },
    });
 
  const DialogTitle = withStyles(styles)((props) => {
    const { children, classes, onClose, ...other } = props;
    return (
      <MuiDialogTitle disableTypography className={classes.root} {...other}>
        <Typography variant="h6">{children}</Typography>
        {onClose ? (
          <IconButton
            aria-label="close"
            className={classes.closeButton}
            onClick={onClose}
          >
            <CloseIcon />
          </IconButton>
        ) : null}
      </MuiDialogTitle>
    );
  });
  async function handleAddContactType(newData) {
    return await new Promise(async(resolve) => {
      resolve();
      console.log(newData);
     await addConatctTypes({
        variables: {
          inputCreate: {
            name: newData.name,
            description: newData.description,
          },
        },
      }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
        console.log(data, error, graphQLErrors, networkError, cacheHit);
       if (data !== null && error == false) {
         handleRefetch(error, 'Saved successfully');
       } 
       else if (error && graphQLErrors.length > 0)
         {
          let duperror = graphQLErrors[0].message;
          console.log(duperror);
          if (
            duperror !== null &&
            duperror.indexOf('ER_DUP_ENTRY') !== -1
          ) {
            handleRefetch(error, 'Name already existed');
          }
        }
     })
     .catch((e) => {
       // you can do something with the error here
       console.log(e);
     });
    });
  }
 async function handleUpdateContactType(newData, oldData) {
    return await new Promise(async(resolve) => {
      resolve();
      console.log(newData);
     await updateConatctTypes({
        variables: {
          input: {
            name: newData.name,
            description: newData.description,
            contactTypeUUID: newData.contactTypeUUID,
          },
        },
      }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
        console.log(data, error, graphQLErrors, networkError, cacheHit);
       if (data !== null && error == false) {
         handleRefetch(error, 'Updated successfully');
       } 
       else if (error && graphQLErrors.length > 0)
         {
          let duperror = graphQLErrors[0].message;
          console.log(duperror);
          if (
            duperror !== null &&
            duperror.indexOf('ER_DUP_ENTRY') !== -1
          ) {
            handleRefetch(error, 'Name already existed');
          }
        }
     })
     .catch((e) => {
       // you can do something with the error here
       console.log(e);
     });
    });
  }
 async function handleRemoveContactType(oldData) {
    return await new Promise(async(resolve) => {
      resolve();
   await   removeConatctTypes({
        variables: {
          input: {
            contactTypeUUID: oldData.contactTypeUUID,
          },
        },
      })
      .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
        console.log(data, error, graphQLErrors, networkError, cacheHit);
        if (data !== null && error == false) {
          handleRefetch(error, 'Deleted successfully');
        } else if (error && graphQLErrors.length > 0) {
          let duperror = graphQLErrors[0].message;
          console.log(duperror);
          if (
            duperror !== null &&
            duperror.indexOf('ER_ROW_IS_REFERENCED') !== -1
          ) {
            setOpenSnackbar(true);
            setSnackbarVariant('error');
            setSnackbarMessage('Selected row is referenced in contact table');
          }
        }
      })
      .catch((e) => {
        // you can do something with the error here
        console.log(e);
      });
    });
  }
  if (loading) return <Loader />;
  if (serverError)
    return <Error type={'Server connection lost.Please try again'} />;

  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <MaterialTable
        icons={tableIcons}
        title="Contact Type"
        columns={columns}
        data={conatctTypedata}
        editable={{
          onRowAdd: (newData) =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
                newData.submitted = true;
                if (!newData.name||
                 !/^[a-zA-Z\s]+$/.test(newData.name)
                 ) {
                    setNameError({
                        error: true,
                        label: "required",
                        helperText: "Required.",
                        validateInput: true,
                    });
                    reject();
                    return;
                }
                resolve();
                handleAddContactType(newData)
            }, 600);
        }),
          onRowUpdate: (newData, oldData) =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
                newData.submitted = true;
                if (!newData.name||
                 !/^[a-zA-Z\s]+$/.test(newData.name)
                 ) {
                    setNameError({
                        error: true,
                        label: "required",
                        helperText: "Required.",
                        validateInput: true,
                    });
                    reject();
                    return;
                }
                resolve();
                handleUpdateContactType(newData, oldData)
            }, 600);
        }),
          onRowDelete: (oldData) => handleRemoveContactType(oldData),
        }}
        options={{
          headerStyle: {
            textAlign: 'center',
            fontSize: 12,
            fontWeight: 'bold',
            fontFamily: 'Arial !important',
            backgroundColor: '#0b153e',
            color: '#ffffff',
            padding: '4px',
          },

          searchFieldStyle: {
            color: '#0b153e',
          },
          actionsColumnIndex: -1,
          pageSize:10,
          pageSizeOptions: [10, 25 ,50,100 ],
          toolbar: true,
          paging: true
        }}
      />
        <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        autoHideDuration={1500}
        open={openSnackbar}
        onClose={handleCloseSnackbar}
      >
        <MySnackbarContentWrapper
          onClose={handleCloseSnackbar}
          variant={variant}
          message={message}
        />
      </Snackbar>
    </MuiThemeProvider>
  );
}
