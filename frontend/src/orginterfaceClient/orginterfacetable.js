import MaterialTable from 'material-table';
import { forwardRef } from 'react';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';

import MTableToolbar from 'material-table';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import CloseIcon from '@material-ui/icons/Close';
import { withStyles } from '@material-ui/core/styles';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import { Grid, Typography } from '@material-ui/core';
import MuiDialogActions from '@material-ui/core/DialogActions';
import React, { Component, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import Timestamp from '../timestamp';
import { useMutation, useQuery, useManualQuery } from 'graphql-hooks';
import Theme from '../css/theme';
import Loader from '../components/loader';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import Error from '../components/emptyPage';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import ProjectForm from './addProjectForm';
import ModalFab from '../components/modalfabtms';
import OrganisationModal from '../orgProjInterfacePanel/organizationForm';
import OrgInterfaceModelForm from './orgInteraceModelForm';
import ProjectModelForm from '../components/tmsmodal';
import SendIcon from '@material-ui/icons/Send';
import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles,
} from '@material-ui/core/styles';

function iconStyles() {
  return {};
}
const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(2),
    },
  },

  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));
const styles = (theme) => ({
  root: {
    Width: 1200,
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: '#FFFFFF',
  },
});
const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const API_CAll_DATA = `query allOrgInterfaceAPI($InputCreate:OrgInterfaceInput){ 
  allOrgInterfaceAPI(input:$InputCreate){
    success
  }
}`;

const ADD_ORG_INTERFACE = `mutation createOrgInterface($InputCreate:OrgInterfaceInput){
          createOrgInterface(input:$InputCreate){
            interfaceUUID 
              projectUUID 
              sourceProfileUUID
              targetProfileUUID
              docTypeUUID
              serviceUUID
              organizationUUID
          }
        }`;

const DISPLAY_PARTNERS = `{
          allPartners1{
            PartnerProfileUUID
            CorporationName            
          }
        }`;

const DISPLAY_All_ORGANIZATION = `query fetchOrganization1($searchInput:String){
          fetchOrganization1(searchInput:$searchInput) {    
            organizationUUID
            OrganizationName         
          } 
        }
        `;
const DISPLAY_All_INTERFACE = `query fetchInterface1($searchInput:String){
          fetchInterface1(searchInput:$searchInput) {           
            interfaceUUID
            name           
          }
        }
        `;

const DISPLAY_All_PROJECT = `query fetchProject1($searchInput:String){
          fetchProject1(searchInput:$searchInput) {
            projectUUID
            ProjectName           
          }
        }
        `;

const DISPLAY_All_DOCUMENTTYPE = `query fetchDocumentType1($searchInput:String){
          fetchDocumentType1(searchInput:$searchInput) {          
            documentTypeUUID
            name           
          }
        }
        `;
const DISPLAY_All_SERVICE = `query fetchSERVICE1($searchInput:String){
          fetchService1(searchInput:$searchInput) {
            serviceUUID
            name      
          } 
        }
        `;
const DISPLAY_All_ORGINTERFACE = `query organisationInterface($searchInput:String){
          organisationInterface(searchInput:$searchInput) {
            interfaceUUID 
            projectUUID 
            sourceProfileUUID
            targetProfileUUID
            docTypeUUID
            serviceUUID
            organizationUUID
              orgInterfaceUUID
              createdOn
              lastUpdatedOn     
          } 
        }
        `;

export default function OrgInterface() {
  const [searchInput, setSearchValue] = React.useState('');
  const [orgData, setOrgData] = React.useState([]);
  const [orgInterface, setOrgInterface] = React.useState([]);
  const [fromopen, setProjectForm] = React.useState(false);
  const [partnerData, setPartnerData] = React.useState([]);
  const [interfaceData, setInterfaceData] = React.useState([]);
  const [projectData, setProjectData] = React.useState([]);
  const [documentTypeData, setDocumentTypeData] = React.useState([]);
  const [serviceData, setServiceData] = React.useState([]);
  const [usertype, setUserType] = React.useState(1);
  const [hideorgproj, setHideOrgProj] = React.useState(true);
  const [serverError, setServerError] = React.useState(false);
  const [openmodel, setOpenorginterfaceModel] = React.useState(false);
  const [rowvalues, setRowDataValues] = React.useState();
  const [
    fetchOrgInterface,
    { loading: pTaloading, data: pTadata, errors: pTaerrors },
  ] = useManualQuery(API_CAll_DATA);
  const {
    loading: oloading,
    error: oerror,
    data: OrgData,
    refetch: orefetch,
  } = useQuery(DISPLAY_All_ORGANIZATION, {
    variables: {
      searchInput: searchInput,
    },
  });
  const {
    loading: ploading,
    error: perror,
    data: PartnerData,
    refetch: prefetch,
  } = useQuery(DISPLAY_PARTNERS);
  const {
    loading: iloading,
    error: ierror,
    data: InterfaceData,
    refetch: irefetch,
  } = useQuery(DISPLAY_All_INTERFACE, {
    variables: {
      searchInput: searchInput,
    },
  });

  const {
    loading: pjloading,
    error: pjerror,
    data: ProjectData,
    refetch: pjrefetch,
  } = useQuery(DISPLAY_All_PROJECT, {
    variables: {
      searchInput: searchInput,
    },
  });

  const {
    loading: dtloading,
    error: dterror,
    data: DocumentTypeData,
    refetch: dtrefetch,
  } = useQuery(DISPLAY_All_DOCUMENTTYPE, {
    variables: {
      searchInput: searchInput,
    },
  });
  const {
    loading: sloading,
    error: serror,
    data: ServiceData,
    refetch: srefetch,
  } = useQuery(DISPLAY_All_SERVICE, {
    variables: {
      searchInput: searchInput,
    },
  });

  const { loading: iodloading, error: ioderror, data, refetch } = useQuery(
    DISPLAY_All_ORGINTERFACE,
    {
      variables: {
        searchInput: searchInput,
      },
    }
  );

  const [addOrgInterface, { loading: ioloading, error: ioerror }] = useMutation(
    ADD_ORG_INTERFACE
  );

  const columns = [
    {
      title: 'UUID',
      field: 'orgInterfaceUUID',
      editable: 'never',
      hidden: true,
      render: (rowData) => {
        return <div>{rowData.orgInterfaceUUID}</div>;
      },
    },
    {
      title: 'Organization ',
      field: 'organizationUUID',
      render: (rowData) => {
        return <div>{rowData.organizationUUID}</div>;
      },
      hidden: hideorgproj,
    },
    {
      title: 'Project',
      field: 'projectUUID',
      render: (rowData) => {
        return <div>{rowData.projectUUID}</div>;
      },
      hidden: hideorgproj,
    },
    {
      title: 'Interface ',
      field: 'interfaceUUID',
      render: (rowData) => {
        return <div>{rowData.interfaceUUID}</div>;
      },
    },

    {
      title: 'Source Profile ',
      field: 'sourceProfileUUID',
      render: (rowData) => {
        return <div>{rowData.sourceProfileUUID}</div>;
      },
    },

    {
      title: 'Target Profile ',
      field: 'targetProfileUUID',

      render: (rowData) => {
        return <div>{rowData.targetProfileUUID}</div>;
      },
    },

    {
      title: 'Document ',
      field: 'docTypeUUID',
      render: (rowData) => {
        return <div>{rowData.docTypeUUID}</div>;
      },
    },

    {
      title: 'Service ',
      field: 'serviceUUID',
      render: (rowData) => {
        return <div>{rowData.serviceUUID}</div>;
      },
    },
    {
      title: 'API Status',

      // field: 'Status'
      render: (rowData) => {
        //console.log(rowData, rowData.APIStatus);
        if (rowData === undefined) {
          return '';
        } else if (
          rowData.APIStatus == null ||
          rowData.APIStatus === 'Inactive'
        ) {
          return <div style={{ color: 'red' }}>Failure</div>;
        } else {
          return <div style={{ color: 'green' }}>Success</div>;
        }
      },
    },
    {
      title: 'Created',
      field: 'createdOn',
      editable: 'never',
      render: (rowData) => {
        console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.createdOn);
        } else {
          return;
        }
      },
    },
    {
      title: 'Last Modified',
      field: 'lastUpdatedOn',
      editable: 'never',
      render: (rowData) => {
        console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.lastUpdatedOn);
        } else {
          return;
        }
      },
    },
  ];

  useEffect(() => {
    if (data !== undefined) {
      console.log(data.organisationInterface[0]);
      setOrgInterface(data.organisationInterface);

      /* console.log("data",data.organisationInterface[0].OrganizationUUID,
      orgData[data.organisationInterface[0].OrganizationUUID],partnerData[data.organisationInterface[0].sourceProfileUUID],
      interfaceData[data.organisationInterface[0].interfaceUUID],projectData[data.organisationInterface[0].projectUUID],
      documentTypeData[data.organisationInterface[0].docTypeUUID],partnerData[data.organisationInterface[0].targetProfileUUID],
      interfaceData[data.organisationInterface[0].interfaceUUID],projectData[data.organisationInterface[0].projectUUID],
    )*/
    setServerError(false);
    }
    else {
      console.log('server error');
      setServerError(true);
    }
    if (OrgData !== undefined) {
      var OrgdropDown = OrgData.fetchOrganization1.reduce(
        (obj, item) => (
          (obj[item.organizationUUID] = item.organizatonName), obj
        ),
        {}
      );
      console.log(OrgdropDown);
      setOrgData(OrgdropDown);
    }
    /*  if(OrgData!==undefined){
     var obj = OrgData.fetchOrganization1.reduce(function(acc, cur, i) {
        acc[cur.OrganizationUUID] = cur.OrganizationName;
        return acc;
        }, {});
        setOrgData(obj)
    }*/

    if (PartnerData !== undefined) {
      var PartnerDataDD = PartnerData.allPartners1.reduce(
        (obj, item) => (
          (obj[item.PartnerProfileUUID] = item.CorporationName), obj
        ),
        {}
      );
      console.log(PartnerDataDD);
      setPartnerData(PartnerDataDD);
    }
    if (InterfaceData !== undefined) {
      var InterfaceDataDD = InterfaceData.fetchInterface1.reduce(
        (obj, item) => ((obj[item.interfaceUUID] = item.name), obj),
        {}
      );
      console.log(InterfaceDataDD);
      setInterfaceData(InterfaceDataDD);
    }
    if (ProjectData !== undefined) {
      var ProjectDataDD = ProjectData.fetchProject1.reduce(
        (obj, item) => ((obj[item.projectUUID] = item.ProjectName), obj),
        {}
      );
      console.log(ProjectDataDD);
      setProjectData(ProjectDataDD);
    }
    if (DocumentTypeData !== undefined) {
      var DocumentTypeDataDD = DocumentTypeData.fetchDocumentType1.reduce(
        (obj, item) => ((obj[item.documentTypeUUID] = item.name), obj),
        {}
      );
      console.log(DocumentTypeDataDD);
      setDocumentTypeData(DocumentTypeDataDD);
    }
    if (ServiceData !== undefined) {
      var ServiceDataDD = ServiceData.fetchService1.reduce(
        (obj, item) => ((obj[item.serviceUUID] = item.name), obj),
        {}
      );
      console.log(ServiceDataDD);
      setServiceData(ServiceDataDD);
    }
    if (usertype == 1) {
      setHideOrgProj(true);
    } else {
      setHideOrgProj(false);
    }
  }, [
    OrgData,
    PartnerData,
    InterfaceData,
    ProjectData,
    DocumentTypeData,
    ServiceData,
    data,
    usertype,
  ]);
  const [state, setState] = React.useState({
    data: [],
  });
  const handleRefetch = () => {
    refetch({
      updateData(_, data) {
        return data;
      },
    });
  };
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const classes = makeStyles(iconStyles)();
  const tableIcons = {
    Add: forwardRef((props, ref) => (
      <AddBox color="primary" {...props} ref={ref} />
    )),
    Check: forwardRef((props, ref) => (
      <Check color="primary" {...props} ref={ref} />
    )),
    Clear: forwardRef((props, ref) => (
      <Clear color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline color="error" {...props} ref={ref} />
    )),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => (
      <Edit
        {...props}
        ref={ref}
        color="primary"
        onClick={console.log('hello world')}
      />
    )),
    Export: forwardRef((props, ref) => (
      <SaveAlt color="primary" {...props} ref={ref} />
    )),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => (
      <FirstPage color="primary" {...props} ref={ref} />
    )),
    LastPage: forwardRef((props, ref) => (
      <LastPage color="primary" {...props} ref={ref} />
    )),
    NextPage: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft color="primary" {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => (
      <Clear color="primary" {...props} ref={ref} />
    )),
    Search: forwardRef((props, ref) => (
      <Search color="primary" {...props} ref={ref} />
    )),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };

  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiTypography: {
          h6: {
            fontSize: 14,
            fontFamily: 'Arial !important',
          },
        },

        MuiSelect: {
          select: {
            minWidth: 10,
          },
        },

        MuiDialogTitle: {
          root: {
            backgroundColor: '#0b153e',
            color: '#FFFFFF',
          },
        },
        MuiGrid: {
          container: {
            flexWrap: 'nowrap',
          },
        },
      },
    });
  const classes1 = useStyles();
  async function handleAddOrgInterface(newData) {
    return await new Promise(async (resolve) => {
      console.log(newData);
      await addOrgInterface({
        variables: {
          input: {
            projectUUID: newData.projectUUID,
            sourceProfileUUID: newData.sourceProfileUUID,
            targetProfileUUID: newData.targetProfileUUID,
            docTypeUUID: newData.docTypeUUID,
            serviceUUID: newData.serviceUUID,
            interfaceUUID: newData.interfaceUUID,
            organizationUUID: newData.organizationUUID,
          },
        },
      });

      if (ioloading) {
        console.log('OrgInterface cannot created');
      } else {
        console.log('OrgInterface created successfully');
        handleRefetch();
      }
    });
  }

  const handleOpen = (e) => {
    setProjectForm(true);
  };
  const CloseProject = () => {
    setProjectForm(false);
  };
  const handleUpdateOrgInterface = (val, rowdata) => {
    setOpenorginterfaceModel(val);
    setRowDataValues(rowdata);
  };

  const handleCallAPI = async (event, rowData) => {
    console.log('rowData', rowData);
    fetchOrgInterface({
      variables: {
        InputCreate: {
          projectUUID: rowData.projectUUID,
          sourceProfileUUID: rowData.sourceProfileUUID,
          targetProfileUUID: rowData.targetProfileUUID,
          docTypeUUID: rowData.docTypeUUID,
          serviceUUID: rowData.serviceUUID,
          interfaceUUID: rowData.interfaceUUID,
          organizationUUID: rowData.organizationUUID,
          orgInterfaceUUID: rowData.orgInterfaceUUID,
        },
      },
    });
  };

  const handleDeleteOrgInterface = (newData) => {};

  if (ioloading || iodloading) return <Loader />;
  if (ioerror || ioderror) return <Error />;
  if (serverError)
  return <Error type={'Server connection lost.Please try again'} />;

  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <MaterialTable
        icons={tableIcons}
        title="Organization Interface"
        columns={columns}
        data={orgInterface}
        editable={{
         // onRowDelete: (oldData) => handleDeleteOrgInterface(oldData),
        }}
        actions={[
          /* {
            icon: AddCircleIcon,
            tooltip: 'Add Project',
            onClick: (event) => handleOpen(event),
          },*/
          {
            icon: SendIcon,
            tooltip: 'API',
            onClick: (event, rowData) => handleCallAPI(event, rowData),
          },
        ]}
        options={{
          headerStyle: {
            textAlign: 'center',
            fontSize: 12,
            fontWeight: 'bold',
            fontFamily: 'Arial !important',
            backgroundColor: '#0b153e',
            color: '#ffffff',
            padding: '4px',
          },

          searchFieldStyle: {
            color: '#0b153e',
          },
          actionsColumnIndex: -1,
        }}
      />
      {/* <ModalFab title="Organisation">
        <OrganisationModal />
      </ModalFab>*/}
      <ModalFab title="OrganizationInterface" open={openmodel}>
        <OrgInterfaceModelForm
          open={openmodel}
          usertype={usertype}
          rowdata={rowvalues}
          onSuccess={handleRefetch}
        />
      </ModalFab>
      <ProjectModelForm
        handleCloseModal={CloseProject}
        isModalOpen={fromopen}
        title={'Add Project Form'}
      >
        <ProjectForm handleCloseModal={CloseProject} isModalOpen={fromopen} />
      </ProjectModelForm>
    </MuiThemeProvider>
  );
}
