import React, { useContext, useCallback } from 'react';
import { useMutation, useQuery } from 'graphql-hooks';
import { forwardRef, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid';
import Theme from '../css/theme';

import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';

const ADD_ORG_INTERFACE = `mutation createOrgInterface($InputCreate:OrgInterfaceInput){
          createOrgInterface(input:$InputCreate){
            interfaceUUID 
              projectUUID 
              sourceProfileUUID
              targetProfileUUID
              docTypeUUID
              serviceUUID
              organizationUUID
          }
        }`;

const DISPLAY_PARTNERS = `{
          allPartners1{
            PartnerProfileUUID
            CorporationName            
          }
        }`;

const DISPLAY_All_ORGANIZATION = `query fetchOrganization1($searchInput:String){
  fetchOrganization1(searchInput:$searchInput) {
    organizationUUID
    organizatonName
  } 
}
        `;
const DISPLAY_All_INTERFACE = `query fetchInterfaceByprojectUUID($projectUUID:String){
  fetchInterfaceByprojectUUID(projectUUID:$projectUUID) {
    idInterface
    interfaceUUID
    name
    description
    projectUUID
    createdOn
    lastUpdatedOn
  } 
  }
        `;

const DISPLAY_All_PROJECT = `query fetchProjectByOrgUUID($organizationUUID:String){
  fetchProjectByOrgUUID(organizationUUID:$organizationUUID) {
    idOrgProject
    projectUUID
    projectName
    projectDescription
    organizationUUID
    createdOn
    updatedOn
  } 
  }`;

const DISPLAY_All_DOCUMENTTYPE = `query fetchDocumentType1($searchInput:String){
          fetchDocumentType1(searchInput:$searchInput) {          
            documentTypeUUID
            name           
          }
        }
        `;
const DISPLAY_All_SERVICE = `query fetchSERVICE1($searchInput:String){
          fetchService1(searchInput:$searchInput) {
            serviceUUID
            name      
          } 
        }
        `;
const DISPLAY_All_ORGINTERFACE = `query organisationInterface($searchInput:String){
          organisationInterface(searchInput:$searchInput) {
            interfaceUUID 
            projectUUID 
            sourceProfileUUID
            targetProfileUUID
            docTypeUUID
            serviceUUID
            organizationUUID
              orgInterfaceUUID
              createdOn
              lastUpdatedOn     
          } 
        }
        `;

const useStyles = makeStyles((theme) => ({}));

const getMuiTheme = () =>
  createMuiTheme(Theme, {
    overrides: {
      MuiSelect: {
        select: {
          minWidth: 180,
        },
      },
    },
  });
export default function OrgInterfaceModel(props) {
  const [searchInput, setSearchValue] = React.useState('');
  const [orgData, setOrgData] = React.useState([]);
  const [orgInterface, setOrgInterface] = React.useState([]);
  const [partnerData, setPartnerData] = React.useState([]);
  const [interfaceData, setInterfaceData] = React.useState([]);
  const [projectData, setProjectData] = React.useState([]);
  const [documentTypeData, setDocumentTypeData] = React.useState([]);
  const [serviceData, setServiceData] = React.useState([]);
  const classes = useStyles();
  const [dropdown, setDropdown] = React.useState('');
  const [values, setValues] = React.useState({
    organisation: '',
    project: '',
    interface: '',
    targetprofile: '',
    sourceprofile: '',
    document: '',
    service: '',
  });
  const [ErrMsgDropdown, setErrMsgDropdown] = React.useState(false);

  const [ErrMsg, setErrMsg] = React.useState(false);
  const [errorInfo, setErrorInfo] = React.useState({
    organisation: false,

    project: false,
    interface: false,
    sourceprofile: false,
    targetprofile: false,
    document: false,
    service: false,
  });
  const [openSnackbar, setOpenSnackbar] = React.useState(false);

  const [variant, setSnackbarVariant] = React.useState('error');
  const [message, setSnackbarMessage] = React.useState();
  const [displayOrgProj, setdisplayOrgProject] = React.useState('false');
  const [hideshowProject, setProjectSelect] = React.useState('false');
  const [hideshowInterface, setInterfaceSelect] = React.useState('false');
  const {
    loading: oloading,
    error: oerror,
    data: OrgData,
    refetch: orefetch,
  } = useQuery(DISPLAY_All_ORGANIZATION, {
    variables: {
      searchInput: searchInput,
    },
  });
  const {
    loading: ploading,
    error: perror,
    data: PartnerData,
    refetch: prefetch,
  } = useQuery(DISPLAY_PARTNERS);
  const {
    loading: iloading,
    error: ierror,
    data: InterfaceData,
    refetch: irefetch,
  } = useQuery(DISPLAY_All_INTERFACE, {
    variables: {
      projectUUID: values.project,
    },
  });
  const {
    loading: pjloading,
    error: pjerror,
    data: ProjectData,
    refetch: pjrefetch,
  } = useQuery(DISPLAY_All_PROJECT, {
    variables: {
      organizationUUID: values.organisation,
    },
  });

  const {
    loading: dtloading,
    error: dterror,
    data: DocumentTypeData,
    refetch: dtrefetch,
  } = useQuery(DISPLAY_All_DOCUMENTTYPE, {
    variables: {
      searchInput: searchInput,
    },
  });
  const {
    loading: sloading,
    error: serror,
    data: ServiceData,
    refetch: srefetch,
  } = useQuery(DISPLAY_All_SERVICE, {
    variables: {
      searchInput: searchInput,
    },
  });

  const [addOrgInterface, { loading: ioloading, error: ioerror }] = useMutation(
    ADD_ORG_INTERFACE
  );

  useEffect(() => {
    if (OrgData !== undefined) {
      console.log(OrgData.fetchOrganization1);
      setOrgData(OrgData.fetchOrganization1);
    }

    if (PartnerData !== undefined) {
      setPartnerData(PartnerData.allPartners1);
    }
    if (InterfaceData !== undefined) {
      setInterfaceData(InterfaceData.fetchInterfaceByprojectUUID);
    }
    if (ProjectData !== undefined) {
      setProjectData(ProjectData.fetchProjectByOrgUUID);
    }
    if (DocumentTypeData !== undefined) {
      setDocumentTypeData(DocumentTypeData.fetchDocumentType1);
    }
    if (ServiceData !== undefined) {
      setServiceData(ServiceData.fetchService1);
    }
    if (values.organisation != '') {
      setProjectSelect(false);
    } else {
      setProjectSelect(true);
    }
    if (values.project != '') {
      setInterfaceSelect(false);
    } else {
      setInterfaceSelect(true);
    }
    if (props.usertype == 1) {
      setdisplayOrgProject('none');
    } else {
      setdisplayOrgProject('show');
    }
  }, [
    OrgData,
    PartnerData,
    InterfaceData,
    ProjectData,
    DocumentTypeData,
    ServiceData,
    values,
    props,
  ]);

  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }

  const handleSubmit = async (event) => {
    console.log(event.detail);
    if (!event.detail || event.detail == 1) {
      console.log(values);
      setErrMsg(false);
      setErrMsgDropdown(false);
      if (
        values.organisation === '' ||
        values.project === '' ||
        values.interface === '' ||
        values.sourceprofile === '' ||
        values.targetprofile === '' ||
        values.document === '' ||
        values.service === ''
      ) {
        setErrMsg(true);
      } else {
        console.log(values);
        addOrgInterface({
          variables: {
            InputCreate: {
              projectUUID: values.project,
              sourceProfileUUID: values.sourceprofile,
              targetProfileUUID: values.targetprofile,
              docTypeUUID: values.document,
              serviceUUID: values.service,
              interfaceUUID: values.interface,
              organizationUUID: values.organisation,
              orgInterfaceUUID: '',
            },
          },
        });
        console.log(ioloading, ioerror);
        if (ioloading) {
          console.log('orginterface not created');
        } else {
          console.log('orginterface created successfully');
          setErrMsg(false);
          setValues({
            ...values,
            organisation: '',
            project: '',
            interface: '',
            sourceprofile: '',
            targetprofile: '',
            document: '',
            service: '',
          });
          props.onSuccess();
        }
      }
    }
  };
  function handleReset() {
    setErrMsg(false);
    setValues({
      ...values,
      organisation: '',
      project: '',
      interface: '',
      sourceprofile: '',
      targetprofile: '',
      document: '',
      service: '',
    });
  }
  const handleChangeDropdown = (name) => (event) => {
    setErrMsg(false);
    setErrMsgDropdown(false);
    console.log(values, name, event.target.value);

    switch (name) {
      case 'organisation':
        if (event.target.value !== undefined) {
          console.log('organisation');
          setValues({
            ...values,
            [event.target.name]: event.target.value,
            interface: '',
            project: '',
          });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'project':
        if (event.target.value !== undefined) {
          setValues({
            ...values,
            [event.target.name]: event.target.value,
            interface: '',
          });

          setErrorInfo({ ...errorInfo, [name]: false });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'interface':
        if (event.target.value !== undefined) {
          setValues({
            ...values,
            [event.target.name]: event.target.value,
          });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'sourceprofile':
      case 'targetprofile':
      case 'document':
      case 'service':
        if (event.target.value !== undefined) {
          setValues({
            ...values,
            [event.target.name]: event.target.value,
          });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }

      default:
        return false;
    }
  };
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <div>
        {ErrMsg === true ? (
          <div style={{ textAlign: 'center' }} className="addfieldserror">
            Please fill the fields
          </div>
        ) : (
          ''
        )}
        <Grid className="orgmodal" container spacing={3}>
          <Grid item xs={12} sm={3} style={{ display: displayOrgProj }}>
            <FormControl className={classes.formControl}>
              <InputLabel id="demo-simple-select-label">
                Organisation
              </InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={values.organisation}
                onChange={handleChangeDropdown('organisation')}
                name="organisation"
              >
                {orgData != undefined && orgData.length > 0
                  ? orgData.map((v, k) => {
                      return (
                        <MenuItem value={v.organizationUUID}>
                          {v.organizatonName}
                        </MenuItem>
                      );
                    })
                  : null}
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={12} sm={3} style={{ display: displayOrgProj }}>
            <FormControl
              className={classes.formControl}
              disabled={hideshowProject}
            >
              <InputLabel id="demo-simple-select-label">Project</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={values.project}
                onChange={handleChangeDropdown('project')}
                name="project"
              >
                {projectData != undefined && projectData.length > 0
                  ? projectData.map((v, k) => {
                      return (
                        <MenuItem value={v.projectUUID}>
                          {v.projectName}
                        </MenuItem>
                      );
                    })
                  : null}
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={12} sm={3}>
            <FormControl
              className={classes.formControl}
              disabled={hideshowInterface}
            >
              <InputLabel id="demo-simple-select-label">Interface</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={values.interface}
                onChange={handleChangeDropdown('interface')}
                name="interface"
              >
                {interfaceData != undefined && interfaceData.length > 0
                  ? interfaceData.map((v, k) => {
                      return (
                        <MenuItem value={v.interfaceUUID}>{v.name}</MenuItem>
                      );
                    })
                  : null}
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={12} sm={3}>
            <FormControl className={classes.formControl}>
              <InputLabel id="demo-simple-select-label">
                Source Profile
              </InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={values.sourceprofile}
                onChange={handleChangeDropdown('sourceprofile')}
                name="sourceprofile"
              >
                {partnerData != undefined && partnerData.length > 0
                  ? partnerData.map((v, k) => {
                      return (
                        <MenuItem value={v.PartnerProfileUUID}>
                          {v.CorporationName}
                        </MenuItem>
                      );
                    })
                  : null}
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={12} sm={3}>
            <FormControl className={classes.formControl}>
              <InputLabel id="demo-simple-select-label">
                Target Profile
              </InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={values.targetprofile}
                onChange={handleChangeDropdown('targetprofile')}
                name="targetprofile"
              >
                {partnerData != undefined && partnerData.length > 0
                  ? partnerData.map((v, k) => {
                      return (
                        <MenuItem value={v.PartnerProfileUUID}>
                          {v.CorporationName}
                        </MenuItem>
                      );
                    })
                  : null}
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={12} sm={3}>
            <FormControl className={classes.formControl}>
              <InputLabel id="demo-simple-select-label">Document</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={values.document}
                onChange={handleChangeDropdown('document')}
                name="document"
              >
                {documentTypeData != undefined && documentTypeData.length > 0
                  ? documentTypeData.map((v, k) => {
                      return (
                        <MenuItem value={v.documentTypeUUID}>{v.name}</MenuItem>
                      );
                    })
                  : null}
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={12} sm={3}>
            <FormControl className={classes.formControl}>
              <InputLabel id="demo-simple-select-label">Service</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={values.service}
                onChange={handleChangeDropdown('service')}
                name="service"
              >
                {serviceData != undefined && serviceData.length > 0
                  ? serviceData.map((v, k) => {
                      return (
                        <MenuItem value={v.serviceUUID}>{v.name}</MenuItem>
                      );
                    })
                  : null}
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={12} sm={6}>
            <Button
              variant="contained"
              style={{
                color: '#000006',
                backgroundColor: '#E5CCFF',
              }}
              variant="contained"
              //size="Large"
              className="createpartnerbutton"
              onClick={handleReset}
            >
              Reset
            </Button>
          </Grid>
          <Grid item xs={12} sm={6}>
            <Button
              onClick={handleSubmit}
              variant="contained"
              // size="Large"
              className="createpartnerbutton"
            >
              Create
            </Button>
          </Grid>
        </Grid>
      </div>
    </MuiThemeProvider>
  );
}
