import React from 'react';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import { Grid, Typography } from '@material-ui/core';
export default function RadioButtonsGroup() {
  const [value, setValue] = React.useState('1');
  const handleChange = (event) => {
    setValue(event.target.value);
  };

  return (
    <Grid container spacing={2}>
    <FormControl component="fieldset">
    <Grid
      item
      xs={12}
      sm={12}
      gutterBottom
      variant="body2"
      color="textSecondary"
      className="text"
    >
      <FormLabel component="legend">Address</FormLabel>
        <RadioGroup aria-label="address" name="address" value={value} onChange={handleChange}>
          <FormControlLabel value="1" control={<Radio />}  />
            <Typography variant="h5" component="h5">INDIA </Typography>
            <Typography variant="subtitle1">CloudGen Systems Pvt Ltd </Typography>
            <Typography variant="body1"> 9th Floor, 902, A-Wing.</Typography>
            <Typography variant="body1"> The Platina, Opp: Radisson Hotel</Typography>
            <Typography variant="body1">Gachibowli - Miyapur road, Hyderabad</Typography>
            <Typography variant="body1">Telangana - 500032 </Typography>
      </RadioGroup>
      </Grid>
      </FormControl>
      </Grid>
  );
}