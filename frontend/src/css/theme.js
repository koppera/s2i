import { createMuiTheme } from '@material-ui/core/styles';
import { red } from '@material-ui/core/colors';
import TableCell from '@material-ui/core/TableCell';

export default createMuiTheme({
  palette: {
    primary: {
      main: '#0b153e'
    },
    secondary: {
      main: '#0b153e'
    },
    error: {
      main: '#ed050c'
    }
  },

  overrides: {
    MuiSelect:{
      root:{
       color:"#0b153e"
    }},
    
    MTablePaginationInner:{
      root:{
       color:"#0b153e"
    }
  },  
   
    
    MuiTablePagination:{
      select:{
        color:"#0b153e !important"
      }},
      MuiTablePagination:{
        input:{
      color:"#0b153e !important"
        }},
      MuiCardHeader: {
        root: {
          background: '#0b153e' 
        },
          title: {
        textAlign: 'left',
        fontSize: 16,
        marginTop: 0,
        marginBottom: 0,
        fontFamily: 'Arial,Helvetica Neue,Helvetica,sans-serif',
        whiteSpace: 'normal',
        wordWrap: 'break-word',
        color: '#ffffff'
      }
    },
   
    
    MuiCardActions: {
      root: {
        background: '#0b153e'
      }
    },
   
    MuiSelect:{icon: {
      color:"#0b153e"
    }},
    MuiIconButton:{
      colorInherit:{
      color:'Inherit'
      } 
    },
    MuiDrawer: {
      paper: {
        background: '#0b153e'
      }
    },
    MuiTableHead: {
      root: {
        background: '#0b153e !important',
        color:"#0b153e !important",
      
      }
    },
    MuiTableSortLabel:{root:{
      color:"#fff !important"
  }},
   
    MuiInput: {
      underline: {
     
      '&:before': {
     
      borderBottomColor:'inherit !important'
                  },
      '&:hover:not($disabled):before': {
      
      borderBottomColor:'inherit !important'
                  },
      '&:after': {
       borderBottomColor:'inherit !important'
                  }
                }
              },

            MuiTableCell:{
              body:{
              paddingTop:'1px !important',
              paddingBottom:'1px !important',
              alignItems:'center !important',
              color:"#0b153e !important",
              alignItems:"center !important",
              fontFamily:"Arial !important",
              fontSize:'10px !important' ,
              },
              alignLeft:{
              textAlign: "center !important",
              }
            }, 
            
            MuiInputBase:{input:{
              fontFamily:"Arial !important",
              fontSize:10,
            }},
            MuiBox:{
              root:{
              padding:"0 !important",
            }},
                          
            MuiTablePagination:{
              toolbar:{
                backgroundColor:'white !important',
                color:'black !important'
        
              }
            },
            MuiTypography:{caption:{
               color:'#0b153e !important'
            }
          },

            MuiListItem:{
              root: {
                paddingTop: '2px',
                    paddingBottom: '5px',
                    
              }
    
        },
      
        MuiSelect:{
          select:{
            minWidth:150,
        }
      },
        
        MuiToolbar: {
          root: {
            backgroundColor: '#ffffff',
            color: '#0b153e'
          }
        },
        MuiTablePagination:{
          input:{
            color:"#0b153e"
          }},
         
          MuiPaper:{ elevation1: { boxShadow: '0px 2px 4px -1px rgb(11,21,62)', }},

    MuiCheckbox: {
      colorSecondary: {
        '&$checked': {
          color: '#0b153e'
        }
      },
      
      
      root: {
        '&$checked': {
          color: '#ed050c'
        }
      }
    }
  }
});
