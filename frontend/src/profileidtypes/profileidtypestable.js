import MaterialTable from 'material-table';
import { useMutation, useQuery } from 'graphql-hooks';
import { forwardRef, useEffect } from 'react';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import ReactDOM from 'react-dom';
import MTableToolbar from 'material-table';
import React, { Component } from 'react';
import Timestamp from '../timestamp';
import { TextField } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import Error from '../components/emptyPage';
import Theme from '../css/theme';
import Loader from '../components/loader';
import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles,
} from '@material-ui/core/styles';

const DISPLAY_All_PROFILEIDTYPES = `{
  allProfileIDTypes {
    idProfileIDTypes
    profileIDTypesUUID
    profileIDTypeName
    profileIDTypeDescription
    createdOn
    lastUpdatedOn
  }
}

`;

const ADD_PROFILEIDTYPES = `mutation createProfileIDTypes($input:ProfileIDTypesInput){
  createProfileIDTypes(input:$input) {
    idProfileIDTypes
    profileIDTypesUUID
    profileIDTypeName
    profileIDTypeDescription
    createdOn
    lastUpdatedOn
  } 
}
`;
const REMOVE_PROFILEIDTYPES = `mutation removeProfileIDTypes($input:ProfileIDTypesInput){
  removeProfileIDTypes(input:$input) {
    idProfileIDTypes
    profileIDTypesUUID
    profileIDTypeName
    profileIDTypeDescription
    createdOn
    lastUpdatedOn
  }

}
`;
const UPDATE_PROFILEIDTYPES = `mutation updateProfileIDTypes($input:ProfileIDTypesInput){
  updateProfileIDTypes(input:$input) {
    idProfileIDTypes
    profileIDTypesUUID
    profileIDTypeName
    profileIDTypeDescription
    createdOn
    lastUpdatedOn
  }

}
`;
function iconStyles() {
  return {};
}

export default function MaterialTableDemo() {
  //FETCH ALL PROFILEID TYPES
  const { loading, error, data, refetch } = useQuery(
    DISPLAY_All_PROFILEIDTYPES
  );
  //ADD PROFILEID TYPES
  const [addProfileIDTypes, addloading, adddata] = useMutation(
    ADD_PROFILEIDTYPES
  );
  //remove PROFILEID TYPES
  const [removeProfileIDTypes, removeloading, removedata] = useMutation(
    REMOVE_PROFILEIDTYPES
  );
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [serverError, setServerError] = React.useState(false);
  const [message, setSnackbarMessage] = React.useState();
  const [variant, setSnackbarVariant] = React.useState('error');
  const [nameError, setNameError] = React.useState({
    error: false,
    label: "",
    helperText: "",
    validateInput: false,
});
const nameexp =(/^[a-zA-Z0-9.+\s]+$/);
  //update PROFILEID TYPES
  const [updateProfileIDTypes, updateloading, updatedata] = useMutation(
    UPDATE_PROFILEIDTYPES
  );

  const [profileIDTypesdata, setData] = React.useState([]);
  const columns = [
    {
      title: ' UUID',
      field: 'profileIDTypesUUID',
      editable: 'never',
      hidden: true,
    },
    {
      title: 'Name',
      field: 'profileIDTypeName',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
           <TextField
             placeholder="Name"
             margin="normal"
             fullWidth
             getOptionLabel={(option) => option.name}
             value={props.value}
           
            error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                    ? nameError.error
                    : false
            }
            helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                    ? nameError.helperText
                    : ""
            }
           
            onChange={(v) => props.onChange(v.target.value)}
      
        />
            <div style={{ color: 'red' }}>
              {!props.rowData.profileIDTypeName||
              nameexp.test(props.rowData.profileIDTypeName)
                ? ''
                : 'Special characters are not allowed except [.,+]'}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.profileIDTypeName}</div>;
      },
    },
    {
      title: 'Description',
      field: 'profileIDTypeDescription',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder="Description"
              margin="normal"
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            />
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.profileIDTypeDescription}</div>;
      },
    },
    {
      title: 'Created',
      field: 'createdOn',
      editable: 'never',
      render: (rowData) => {
        if (rowData != undefined) {
          return Timestamp(rowData.createdOn);
        } else {
          return;
        }
      },
    },
    {
      title: 'Last Modified',
      field: 'lastUpdatedOn',
      editable: 'never',
      render: (rowData) => {
        if (rowData != undefined) {
          return Timestamp(rowData.lastUpdatedOn);
        } else {
          return;
        }
      },
    },
  ];

  useEffect(() => {
    if (data !== undefined) {
      console.log(data.allProfileIDTypes);
      setData(data.allProfileIDTypes);
      setServerError(false);
    }
    else {
      console.log('server error');
      setServerError(true);
    }
  }, [data]);

  const handleRefetch = (value, message) => {
    refetch({
      updateData(_, data) {
        return data;
      },
    });
    setOpenSnackbar(true);
    setSnackbarMessage(message);
    if (value) {
      setSnackbarVariant('error');
    } else {
      setSnackbarVariant('success');
    }
  };

  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }

  const classes = makeStyles(iconStyles)();
  const tableIcons = {
    Add: forwardRef((props, ref) => (
      <AddBox color="primary" {...props} ref={ref} />
    )),
    Check: forwardRef((props, ref) => (
      <Check color="primary" {...props} ref={ref} />
    )),
    Clear: forwardRef((props, ref) => (
      <Clear color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline  style={{marginLeft:"1px"}} color="error" {...props} ref={ref} />
    )),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => (
      <Edit style={{marginLeft:"25px"}} {...props} ref={ref} color="primary" />
    )),
    Export: forwardRef((props, ref) => (
      <SaveAlt color="primary" {...props} ref={ref} />
    )),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => (
      <FirstPage color="primary" {...props} ref={ref} />
    )),
    LastPage: forwardRef((props, ref) => (
      <LastPage color="primary" {...props} ref={ref} />
    )),
    NextPage: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft color="primary" {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => (
      <Search color="primary" {...props} ref={ref} />
    )),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };

  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiTypography: {
          h6: {
            fontSize: 14,
            fontFamily: 'Arial !important',
          },
        },
        MuiSelect: {
          icon:{
            color:'#0b153e'
              }
        },
    
      },
    });
  async function handleAddProfileIDTypes(newData) {
    return await new Promise(async (resolve) => {
      console.log(newData);
      resolve();
      await addProfileIDTypes({
        variables: {
          input: {
            profileIDTypeName: newData.profileIDTypeName,
            profileIDTypeDescription: newData.profileIDTypeDescription,
          },
        },
      }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
        console.log(data, error, graphQLErrors, networkError, cacheHit);
       if (data !== null && error == false) {
         handleRefetch(error, 'Saved successfully');
       } 
       else if (error && graphQLErrors.length > 0)
         {
          let duperror = graphQLErrors[0].message;
          console.log(duperror);
          if (
            duperror !== null &&
            duperror.indexOf('ER_DUP_ENTRY') !== -1
          ) {
            handleRefetch(error, 'Name already existed');
          }
        }
     })
     .catch((e) => {
       // you can do something with the error here
       console.log(e);
     });
    });
  }
  async function handleUpdateProfileIDTypes(newData, oldData) {
    return await new Promise(async (resolve) => {
      resolve();
      console.log(newData);
      await updateProfileIDTypes({
        variables: {
          input: {
            profileIDTypeName: newData.profileIDTypeName,
            profileIDTypeDescription: newData.profileIDTypeDescription,
            profileIDTypesUUID: newData.profileIDTypesUUID,
          },
        },
      }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
        console.log(data, error, graphQLErrors, networkError, cacheHit);
       if (data !== null && error == false) {
         handleRefetch(error, 'Updated successfully');
       } 
       else if (error && graphQLErrors.length > 0)
         {
          let duperror = graphQLErrors[0].message;
          console.log(duperror);
          if (
            duperror !== null &&
            duperror.indexOf('ER_DUP_ENTRY') !== -1
          ) {
            handleRefetch(error, 'Name already existed');
          }
        }
     })
     .catch((e) => {
       // you can do something with the error here
       console.log(e);
     });
    });
  }
  async function handleRemoveProfileIDTypes(oldData) {
    return await new Promise(async (resolve) => {
      resolve();
      await removeProfileIDTypes({
        variables: {
          input: {
            profileIDTypesUUID: oldData.profileIDTypesUUID,
          },
        },
      }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
        console.log(data, error, graphQLErrors, networkError, cacheHit);
        if (data !== null && error == false) {
          handleRefetch(error, 'Deleted successfully');
        } else if (error && graphQLErrors.length > 0) {
          let duperror = graphQLErrors[0].message;
          console.log(duperror);
          if (
            duperror !== null &&
            duperror.indexOf('ER_ROW_IS_REFERENCED') !== -1
          ) {
            setOpenSnackbar(true);
            setSnackbarVariant('error');
            setSnackbarMessage('Selected row is referenced in External IDs table');
          }
        }
      })
      .catch((e) => {
        // you can do something with the error here
        console.log(e);
      });
    });
  }
  if (loading) return <Loader />;
  if (serverError)
  return <Error type={'Server connection lost.Please try again'} />;
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <MaterialTable
        icons={tableIcons}
        title="Profile Id Types"
        columns={columns}
        data={profileIDTypesdata}
          editable={{
            onRowAdd: (newData) =>
            new Promise((resolve, reject) => {
           setTimeout(() => {
               newData.submitted = true;
               if (!newData.profileIDTypeName||
                !nameexp.test(newData.profileIDTypeName)
                ) {
                   setNameError({
                       error: true,
                       label: "required",
                       helperText: "Required.",
                       validateInput: true,
                   });
                   reject();
                   return;
               }
               resolve();
             
               handleAddProfileIDTypes(newData)
            
           }, 600);
       }),
          
         // onRowAdd: (newData) => handleAddProfileIDTypes(newData),
          onRowUpdate: (newData, oldData) =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
                newData.submitted = true;
                if (!newData.profileIDTypeName||
                 !nameexp.test(newData.profileIDTypeName)
                 ) {
                    setNameError({
                        error: true,
                        label: "required",
                        helperText: "Required.",
                        validateInput: true,
                    });
                    reject();
                    return;
                }
                resolve();
                handleUpdateProfileIDTypes(newData, oldData);
            }, 600);
        }),
          onRowDelete: (oldData) => handleRemoveProfileIDTypes(oldData),
        }}
        options={{
          headerStyle: {
            textAlign: 'center',
            fontSize: 12,
            fontWeight: 'bold',
            fontFamily: 'Arial, !important',
            backgroundColor: '#0b153e',
            color: '#ffffff',
            padding: '4px',
          },

          searchFieldStyle: {
            color: '#0b153e',
          },
          actionsColumnIndex: -1,
          pageSize:10,
          pageSizeOptions: [10, 25 ,50,100 ],
          toolbar: true,
          paging: true
        }}
      />
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        autoHideDuration={1500}
        open={openSnackbar}
        onClose={handleCloseSnackbar}
      >
        <MySnackbarContentWrapper
          onClose={handleCloseSnackbar}
          variant={variant}
          message={message}
        />
      </Snackbar>
    </MuiThemeProvider>
  );
}
