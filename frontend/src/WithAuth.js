/*import React, { useEffect } from 'react';
withPageLogging = Component => props => {
    
  useEffect(() => {
    fetch(`/logger?location=${ window.location}`);
  }, []);
  return <Component {...props } />;
};
export default withPageLogging;*/


export const useSample = WrappedComponent => props => { // curry
  const sampleCtx = useContext(SampleCtx);
  return (
    <WrappedComponent
      {...props}
      value={sampleCtx.value}
      setValue={sampleCtx.setValue}
    />
  );
};