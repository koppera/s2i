import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
//import { SampleProvider } from "./provider";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { GraphQLClient, ClientContext } from 'graphql-hooks';
import ResponsiveDrawer from './common/layout';
import Addorganisation from './Organisation/addorganisation';
import Orgcards from './Organisation/orgcards';
import ProjectListPage from './Projects/projectlistpage';
import ProcessListPage from './Process/processlistpage';
import Regioncards from './Region/regioncards';
import Address from './Address/addresstable';
import Projecttable from './Projects/projecttable';
import Transactiontable from './transaction/transactiontable';
import Orgtable from './Organisation/orgtable';
import Profiletable from './Profile/partnerProfileTable';
import AddressTypetable from './Addresstype/addresstypetable';
import Application from './applicationusers/applicationuserstable';
import Protocoltype from './fileprotocoltype/fileprotocoltypetable';
import Transferprotocol from './filetransferprotocol/filetransferprotocoltable';
import Module from './moduletype/moduletypetable';
import Profileaddress from './profileaddressrelation/profileaddressrelationtable';
import Profilidtypes from './profileidtypes/profileidtypestable';
import Protocolcertificatetable from './protocolcertificate/protocolcertificatetable';
import userroleassignmenttable from './userroleassignment/userroleassignmenttable';
import userroletypetable from './userroletype/userroletypetable';
import usertypestable from './usertypes/usertypestable';
import webtransferprotocoltable from './webtransferprotocol/webtransferprotocoltable';
import webtransferprotocoltypetable from './webtransferprotocoltype/webtransferprotocoltypetable';
import usertable from './user/usertable';
import Contact from './Contact/contact';
import ContactType from './ContactType/contacttype';
import interfacetable from './interface/interfacetable';
import orginterfacetable from './orginterface/orginterfacetable';
import ProfileIDValues from './ProfileIDValues/profileidvalues';
import ProfileContactRelation from './ProfileContactRelation/profilecontactrelation';
import CreatePartnerProfile from './Profile/createpartnerprofile';
import Monitoring from './monitoring/monitoringFilterTable';
import contactfrom from './Contact/contactfrom';
import addressform from './Address/addressform';
import Createprofileid from './ProfileIDValues/createprofileidvalues';
import PartnerProfileTabs from './Profile/PartnerProfileTabs';
import partnerprofileconfig from './Profile/partnerprofileconfig';
import Partnerprofiledetails from './partnerprofiledetails/partnerprofiledetails';
import SearchPartner from './partnerprofiledetails/searchPartner';
import PartnerProfile from './Profile/partnerProfile';
import Orgprojectinterface from './orgProjInterfacePanel/orgnazationalPanel';
import Service from './Service/serviceTable';
import Documenttype from './DocumentType/documentTypeTable';
import Loginpage from './signIn_signOut/loginPage';
import ProfileExpensionPanel from './profileExpensionPanel/profileExpensionPanel';
import ProtocolType from './protocoltypetable/protocolType';
import LoginPage from './loginPage/loginPage';
import FetchUserOrganization from './loginPage/fetchUserOrganization';
import kibanaPage from './kibana/KibanaDashboard';
import kibanaPage2 from './kibana/KibanaDashboard2';
import userprocessingrule from './UserProcessingRule/userProcessingRule';
import WebTransferProtocal from './webtransferprotocol/webTransferProtocolView';
import FTP from './filetransferprotocol/fileTransferProtocolView';
import memCache from 'graphql-hooks-memcache';
//import AuthContext from './authUtil/Auth';
import ResourcesTable from './Resources/resourcesTable';
import Accounts from './Accounts/resetpassword';
import ApplicationURL from './arcesbold/applicationurl';
import ASTwoProfile from './arcesbold/aSTwoProfile';
import Tabs from './arcesbold/arcesbTabs';
import Certificatetable from './arcesbold/certificateTable';
import As2profile from './arcesbold/as2profileform';
import TradingInfo from './arcesb/tradinginfo/tradingInfo';
import ResourcesTable1 from './arcesbold/resourcesTable';
import ActionTable from './arcesbold/actionTable';
import ConnectorsTable from './arcesbold/connectorsTable';
import ServerModalForm from './arcesbold/serverModalForm';
import InputTable from './arcesbold/inputTable.js';
import OutputTable from './arcesbold/outputTable.js';
import WorkspaceSettings from './arcesbold/workspaceSettings.js';
import AppicationURLForm from './arcesbold/applicationURLForm';
import WorkSpace from './arcesb/workSpace.js';
import Document from './arcesb/documentTypeTable';
import KibanaArcesb from './arcesb/arcESBMonitoring';
import Video from "./arcesb/documentation/video";
import Document1 from "./arcesb/documentation/workFlowDocument";
import Support from "./arcesb/documentation/supportContactInfo"
import Settings from "./arcesb/tradinginfo/settings"
import QualifiersTable from "./arcesb/qualifiersTable"
//import AutomationSettings from './arcesb/automationSettings';
import As2ProfileView from "./arcesb/tradinginfo/as2ProfileView"
import X12ConnectorTable from "./arcesb/tradinginfo/x12ConnectorTable";
import X12ConnectorView from "./arcesb/tradinginfo/x12ConnectorView"
import Controlversion from "./arcesb/controlVersion";
//import DragAndDropConnectors from "./arcesb/dragAndDropConnectors";
const client = new GraphQLClient({
  url: process.env.REACT_APP_GRAPHQL,
  //url: 'http://localhost:5004/graphql',
 // url:'https://graphql.tms.changemaker.community/graphql',
  //url:'https://arcesbgraphql.arcesb.changemaker.community/graphql',
});

console.log('clientIndx', client);
ReactDOM.render(
  <Router>
    <ClientContext.Provider value={client}>
      <Switch>
        <ResponsiveDrawer>
          <Switch>
            <Route exact path="/" component={WorkSpace} />
            <Route exact path="/partnerProfiles" component={PartnerProfile} />
            <Route
              path="/orgprojectinterface"
              component={Orgprojectinterface}
            />
            <Route path="/FTP" component={FTP} />
            <Route
              path="/fetchUserOrganization"
              component={FetchUserOrganization}
            />
           {/* <Route path="/connectorFlow" component={DragAndDropConnectors}/>*/}
            <Route path="/connectorsTable" component={ConnectorsTable} />
            <Route path="/actionTable" component={ActionTable} />
            <Route path="/resourcesTable" component={ResourcesTable1} />
            <Route path="/monitoring" component={kibanaPage} />
            <Route path="/kibanaPage2" component={kibanaPage2} />
            <Route path="/profiles" component={Tabs} />
            {/**  <Route  path="/login" component={LoginPage} />*/}
            <Route path="/certificateTable" component={Certificatetable} />
            {/**  <Route  path="/login" component={LoginPage} />*/}
            <Route path="/SearchPartner" component={SearchPartner} />
            <Route path="/Addorganisation" component={Addorganisation} />
            <Route path="/orgcards" component={Orgcards} />
            <Route path="/projectlistpage" component={ProjectListPage} />
            <Route path="/processlistpage" component={ProcessListPage} />
            <Route path="/regioncards" component={Regioncards} />
            <Route path="/addorganisation" component={Addorganisation} />
            <Route path="/PartnerProfileTabs" component={PartnerProfileTabs} />
            <Route path="/orginterface" component={orginterfacetable} />
            <Route path="/interface" component={interfacetable} />
            <Route path="/address" component={Address} />
            <Route path="/project" component={Projecttable} />
            <Route path="/transaction" component={Transactiontable} />
            <Route path="/organisation" component={Orgtable} />
            <Route path="/PartnerProfile" component={Profiletable} />
            <Route path="/addresstype" component={AddressTypetable} />
            <Route path="/addressform" component={addressform} />
            <Route path="/projecttable" component={Projecttable} />
            <Route path="/applicationusers" component={Application} />
            <Route path="/fileprotocoltype" component={Protocoltype} />
            <Route path="/filetransferprotocol" component={Transferprotocol} />
            <Route path="/moduletypetable" component={Module} />
            <Route path="/inputtable" component={InputTable} />
            <Route path="/outputtable" component={OutputTable} />
            <Route path="/arcESBMonitoring" component={KibanaArcesb} />
            <Route path="/settings" component={Settings} />
            <Route path="/qualifiersTable" component={QualifiersTable} />


            <Route path="/controlVersion" component={Controlversion} />




            <Route
              path="/WebTransferProtocal"
              component={WebTransferProtocal}
            />
            <Route
              path="/profileExpensionPanel"
              component={ProfileExpensionPanel}
            />
            <Route path="/resourcesTable" component={ResourcesTable} />
            <Route
              path="/profileaddressrelationtable"
              component={Profileaddress}
            />
            <Route path="/profilidtypestable" component={Profilidtypes} />
            <Route
              path="/protocolcertificate"
              component={Protocolcertificatetable}
            />
            <Route
              path="/userroleassignment"
              component={userroleassignmenttable}
            />
            <Route path="/userroletype" component={userroletypetable} />
            <Route path="/usertypes" component={usertypestable} />
            <Route
              path="/webtransferprotocol"
              component={webtransferprotocoltable}
            />
            <Route
              path="/webtransferprotocoltype"
              component={webtransferprotocoltypetable}
            />
            <Route path="/user" component={usertable} />
            <Route path="/contact" component={Contact} />
            <Route path="/contacttype" component={ContactType} />
            <Route path="/ProfileIDValues" component={ProfileIDValues} />
            <Route
              path="/ProfileContactRelation"
              component={ProfileContactRelation}
            />
            <Route
              path="/CreatePartnerProfile"
              component={CreatePartnerProfile}
            />
            <Route
              path="/partnerprofiledetails"
              component={Partnerprofiledetails}
            />
            <Route path="/monitoringFilterTable" component={Monitoring} />
            <Route path="/createprofileidvalues" component={Createprofileid} />
            <Route
              path="/partnerprofileconfig"
              component={partnerprofileconfig}
            />

            <Route path="/contactfrom" component={contactfrom} />
            <Route path="/documenttypetable" component={Documenttype} />
            <Route path="/servicetable" component={Service} />
            <Route path="/resourcesTable" component={ResourcesTable} />
            <Route path="/applicationurl" component={ApplicationURL} />
            <Route path="/aSTwoProfile" component={ASTwoProfile} />
            <Route path="/applicationURLForm" component={AppicationURLForm} />
            <Route path="/workspaceSettings" component={WorkspaceSettings} />
            <Route path="/tradingInfo" component={TradingInfo} />
            <Route path="/loginPage" component={Loginpage} />
            <Route path="/protocolType" component={ProtocolType} />
            <Route path="/SignIn" component={LoginPage} />
            <Route path="/monitoring" component={kibanaPage} />
            <Route path="/userprocessingrule" component={userprocessingrule} />
            <Route path="/accounts" component={Accounts} />
            <Route path="/as2profile" component={As2profile} />
            <Route path="/serverModalForm" component={ServerModalForm} />
            <Route path="/documentTypeTablearcesb" component={Document} />
            <Route path="/video" component={Video} />
            <Route path="/workflowDocumentation" component={Document1} />
            <Route path="/support" component={Support} />
            <Route path="/x12ConnectorTable" component={X12ConnectorTable} />
            <Route path="/x12ConnectorView" component={X12ConnectorView} />
            <Route path="/as2ProfileView" component={As2ProfileView} />



          </Switch>
        </ResponsiveDrawer>
      </Switch>
    </ClientContext.Provider>
  </Router>,
  document.getElementById('root')
);
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();