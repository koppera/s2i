import React, { useEffect, useContext, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { Grid, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { withStyles } from '@material-ui/core/styles';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import Theme from '../css/theme';
import Modal from '@material-ui/core/Modal';
import {
  createMuiTheme,
  MuiThemeProvider
} from '@material-ui/core/styles';
function rand() {
  return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
} const useStyles = makeStyles(theme => ({  
     
}));
export default function AdressFromModel(props) {
  const classes = useStyles();
  const [TypeUUID, setTypeUUID] = React.useState('');
  const [state, setState] = React.useState({
    checkedA: true,
    checkedB: true
  });
  const [open, setOpen] = React.useState(true);
  const handleChange1 = event => {
    setTypeUUID(event.target.value);
  };

  const handleChange = event => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const [values, setValues] = React.useState({
    addressNameID: '',
    addressLines1: '',
    addressLines2: '',
    addressLines3: '',
    city: '',
    state: '',
    country: '',
    zip: ''
  });
  const [ErrMsg, setErrMsg] = React.useState(false);
  const [errorInfo, setErrorInfo] = React.useState({
    addressNameID: false,
    addressLines1: false,
    addressLines2: false,
    addressLines3: false,
    city: false,
    state: false,
    country: false,
    zip: false
  });
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [value, setValue] = React.useState('IS');
  const [variant, setSnackbarVariant] = React.useState('error');
  const [message, setSnackbarMessage] = React.useState();

  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }
  const handleChange2 = name => event => {
    setErrMsg(false);
    setValues({ ...values, [name]: event.target.value });
    switch (name) {
      case 'addressNameID':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match('^([a-zA-Z])[a-zA-Z]*$') &&
          event.target.value.length <= 25
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'addressLines1':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match('^([a-zA-Z0-9])[a-zA-Z0-9]*$') &&
          event.target.value.length <= 25
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
        case 'addressLines2':
          if (event.target.value.length === 0) {
            setValues({ ...values, [name]: '' });
            setErrorInfo({ ...errorInfo, [name]: false });
          } else if (
            event.target.value.match('^([a-zA-Z0-9])[a-zA-Z0-9]*$') &&
            event.target.value.length <= 25
          ) {
            setErrorInfo({ ...errorInfo, [name]: false });
            setValues({ ...values, [name]: event.target.value });
          } else {
            setErrorInfo({ ...errorInfo, [name]: true });
          }
          break;
          case 'addressLines3':
            if (event.target.value.length === 0) {
              setValues({ ...values, [name]: '' });
              setErrorInfo({ ...errorInfo, [name]: false });
            } else if (
              event.target.value.match('^([a-zA-Z0-9])[a-zA-Z0-9]*$') &&
              event.target.value.length <= 25
            ) {
              setErrorInfo({ ...errorInfo, [name]: false });
              setValues({ ...values, [name]: event.target.value });
            } else {
              setErrorInfo({ ...errorInfo, [name]: true });
            }
            break;
      case 'city':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match('^([a-zA-Z])[a-zA-Z]*$') &&
          event.target.value.length <= 25
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
        } else setErrorInfo({ ...errorInfo, [name]: true });
        break;
      case 'state':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match('^([a-zA-Z])[a-zA-Z]*$') &&
          event.target.value.length <= 25
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
        } else setErrorInfo({ ...errorInfo, [name]: true });
        break;

      case 'country':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match('^([a-zA-Z])[a-zA-Z]*$') &&
          event.target.value.length <= 25
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
        } else setErrorInfo({ ...errorInfo, [name]: true });
        break;
      case 'zip':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match('^[0-9]+$') &&
          event.target.value.length <= 6
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
          console.log(event.target.value);
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      default:
        return false;
    }
  };
  const handleSubmit = async () => {
    console.log(values);
    setErrMsg(false);
    if (
      values.addressNameID === '' ||
      values.addressLines1 === '' ||
      values.addressLines2 === '' ||
      values.addressLines3 === '' ||
      values.city === '' ||
      values.state === '' ||
      values.country === '' ||
      values.zip === ''
    ) {
      setErrMsg(true);
    } else {
      setErrMsg(false);
      setValues({
        ...values,
        addressNameID: '',
        addressLines1: '',
        addressLines2: '',
        addressLines3: '',
        city: '',
        state: '',
        country: '',
        zip: ''
      });
    }
  };
  const handlereset = () => {
    setErrMsg(false);
    setErrorInfo({
      addressNameID: false,
      addressLines1: false,
      addressLines2: false,
      addressLines3: false,
      city: false,
      state: false,
      country: false,
      zip: false
    });
    setValues({
      ...values,
        addressNameID: '',
        addressLines1: '',
        addressLines2: '',
        addressLines3: '',
        city: '',
        state: '',
        country: '',
        zip: ''
     
    });
  };

  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSvgIcon:{
          root:{
         color:"#FFFFFF"
        }},
        MuiSelect:{
          select:{
            minWidth:'490px',
        }
      },
        
        MuiDialogTitle: {
            root: {
              backgroundColor: '#0b153e',
              color: '#FFFFFF'
            }
          },}});
          const handleModalClose = () => {
            props.handleCloseModal(false);
          };

return (
        
    <div>
        <MuiThemeProvider theme={getMuiTheme()}>
        <Grid container className="header">
          <Grid item xs={10} sm={10}>
            <h2 className="h2">Address Form</h2>
          </Grid>
          <Grid item xs={2} sm={2} className="close">
            <CloseIcon onClick={handleModalClose} />
          </Grid>
        </Grid>
      
       
      <div>
{ErrMsg === true ? (
  <div style={{ textAlign: 'center' }} className="addfieldserror">
    Please fill the fields
  </div>
) : (
  ''
)}

<form className="content" noValidate autoComplete="off">
<Grid className="configGrid">
<Grid container spacing={2}>  
        <Grid
         item
         xs={6}
         sm={6}
         gutterBottom
         variant="body2"
         color="textSecondary"
         className="text"
        >
 <TextField
    id="outlined-dense"
    label="Address Name "
    className="textField"
    margin="dense"
    variant="outlined"
    onChange={handleChange2('addressNameID')}
    value={values.addressNameID}
    name="addressNameID"
  />
  {errorInfo.addressNameID === true ? (
    <div id="nameid" className="addfieldserror">
      "Allowed alphabets,no special characters and Numerics are
      allowed.Maximum length is 25."
    </div>
  ) : (
    ''
  )}
        </Grid>
        <Grid
         item
         xs={6}
         sm={6}
         gutterBottom
         variant="body2"
         color="textSecondary"
         className="text"
      >
  <TextField
    id="outlined-dense"
    label="Address Lines1 "
    className="textField"
    margin="dense"
    variant="outlined"
    onChange={handleChange2('addressLines1')}
    value={values.addressLines1}
    name="addressLines1"
  />
    {errorInfo.addressLines1 === true ? (
    <div id="nameid" className="addfieldserror">
      "Allowed alphabets and Numerics,no special characters and are
      allowed.Maximum length is 25."
     </div>
    ) : (
    ''
    )}
   </Grid>
   <Grid
       item
       xs={6}
       sm={6}
       gutterBottom
       variant="body2"
       color="textSecondary"
       className="text"
      >
    <TextField
    id="outlined-dense"
    label="Address Lines2 "
    className="textField"
    margin="dense"
    variant="outlined"
    onChange={handleChange2('addressLines2')}
    value={values.addressLines2}
    name="addressLines2"
  />
    {errorInfo.addressLines2 === true ? (
    <div id="nameid" className="addfieldserror">
      "Allowed alphabets and Numerics,no special characters and are
      allowed.Maximum length is 25."
     </div>
    ) : (
    ''
    )}
  </Grid>
  <Grid
   item
   xs={6}
   sm={6}
   gutterBottom
   variant="body2"
   color="textSecondary"
   className="text"
   >
   <TextField
    id="outlined-dense"
    label="Address Lines3 "
    className="textField"
    margin="dense"
    variant="outlined"
    onChange={handleChange2('addressLines3')}
    value={values.addressLines3}
    name="addressLines3"
  />
    {errorInfo.addressLines3 === true ? (
    <div id="nameid" className="addfieldserror">
      "Allowed alphabets and Numerics,no special characters and are
      allowed.Maximum length is 25."
     </div>
    ) : (
    ''
    )}
  </Grid>
 <Grid
    item
    xs={6}
    sm={6}
    gutterBottom
    variant="body2"
    color="textSecondary"
    className="text"
    >
  <TextField
    id="outlined-dense"
    label="City "
    className="textField"
    margin="dense"
    variant="outlined"
    onChange={handleChange2('city')}
    value={values.city}
    name="city"
  />
  {errorInfo.city === true ? (
  <div id="nameid" className="addfieldserror">
      "Allowed alphabets,no special characters and Numerics are
      allowed.Maximum length is 25."
    </div>
    ) : (
    ''
   )}
  </Grid>
  <Grid
      item
      xs={6}
      sm={6}
      gutterBottom
      variant="body2"
      color="textSecondary"
      className="text"
      >
  <TextField
    id="outlined-dense"
    label="State"
    className="textField"
    margin="dense"
    variant="outlined"
    onChange={handleChange2('state')}
    value={values.state}
    name="state"
  />
  {errorInfo.state === true ? (
  <div id="nameid" className="addfieldserror">
  "Allowed alphabets,no special characters and Numerics are
  allowed.Maximum length is 25."
  </div>
  ) : (
    ''
  )}
</Grid>
<Grid
      item
      xs={6}
      sm={6}
      gutterBottom
      variant="body2"
      color="textSecondary"
      className="text"
      >
  <TextField
    id="outlined-dense"
    label="Country "
    className="textField"
    margin="dense"
    variant="outlined"
    onChange={handleChange2('country')}
    value={values.country}
    name="country"
  />
  {errorInfo.country === true ? (
    <div id="nameid" className="addfieldserror">
      "Allowed alphabets,no special characters and Numerics are
      allowed.Maximum length is 25."
    </div>
     ) : (
    ''
    )}
    </Grid>
    <Grid
        item
        xs={6}
        sm={6}
        gutterBottom
        variant="body2"
        color="textSecondary"
        className="text"
       >
     <TextField
       id="outlined-dense"
       label="Zip"
       className="textField"
       margin="dense"
       variant="outlined"
       onChange={handleChange2('zip')}
       value={values.zip}
       name="zip"
    />
     {errorInfo.zip === true ? (
     <div id="nameid" className="addfieldserror">
      "Allowed Numerics,no special characters and alphabets are
      allowed.Maximum length is 6."
    </div>
      ) : (
     ''
    )}
  </Grid>
  <Grid
      item
      xs={6}
      sm={6}
      gutterBottom
      variant="body2"
      color="textSecondary"
      className="text"
     >
  <FormControlLabel
    label="Status"
    control={
      <Switch
        checked={state.checkedA}
        onChange={handleChange}
        name="checkedA"
      />
    }
  />
</Grid>
<Grid
     item
     xs={6}
     sm={6}
     gutterBottom
     variant="body2"
     color="textSecondary"
     className="text"
    >
    <FormControl className={classes.formControl}>
    <InputLabel id="demo-simple-select-label">TypeUUID</InputLabel>
    <Select
      fullWidth
      labelId="demo-simple-select-label"
      id="demo-simple-select"
      value={TypeUUID}
      required onChange={handleChange1}
    >
      <MenuItem value={10}>CNP</MenuItem>
      <MenuItem value={20}>TMS</MenuItem>
      <MenuItem value={30}>Cloudgen</MenuItem>
    </Select>
    </FormControl>
 </Grid>
 <Grid
      item
      xs={6}
      sm={6}
      gutterBottom
      variant="body2"
      color="textSecondary"
      className="text"
 >
      <Button
       style={{
           color: '#000006',
           backgroundColor:'#E5CCFF' 
        }}
         variant="contained"
         //size="Large"
         className="createpartnerbutton"
        onClick={handlereset}
      >
       RESET
     </Button>
 </Grid>
 <Grid
       item
       xs={6}
       sm={6}
       gutterBottom
       variant="body2"
       color="textSecondary"
       className="text"
    >
    <Button
     onClick={handleSubmit}
     variant="contained"
     fullWidth="true"
     className="createpartnerbutton"
    >
     Submit
    </Button>
  </Grid>
  </Grid>
  </Grid>
</form>
</div>

      </MuiThemeProvider>
</div>
  )
}