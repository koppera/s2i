import React, { useEffect, useContext, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import { useMutation, useQuery } from 'graphql-hooks';
import Snackbar from '@material-ui/core/Snackbar';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Switch from '@material-ui/core/Switch';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import Modal from '@material-ui/core/Modal';
import Theme from '../css/theme';
import Loader from '../components/loader';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
const DISPLAY_ADDRESS = `query fetchAddressByUUID($addressUUID:String,$contactUUID:String){
  fetchAddressByUUID(addressUUID:$addressUUID,contactUUID:$contactUUID) {
    idAddress
    addressUUID
    addressNameID
    addressLines1
    addressLines2
    addressLines3
    city
    state
    country
    zip
    addressStatus
    addressTypeUUID
    partnerProfileUUID
    createdOn
    lastUpdatedOn
    contactUUID
  } 
   
}
`;

const UPDATE_ADDRESS = `mutation updateAddress($input:AddressInput){
  updateAddress(input:$input) {
    idAddress
    addressUUID
    addressNameID
    addressLines1
    addressLines2
    addressLines3
    city
    state
    country
    zip
    addressStatus
    addressTypeUUID
    partnerProfileUUID
    createdOn
    lastUpdatedOn
  } 
}`;

const DISPLAY_All_ADDRESSTYPES = `{
    allAddressTypes {
      idAddressType
      addressTypeUUID
      addressTypeName
      addressTypeDescription
      createdOn
      lastUpdatedOn
    }
  }
  `;
const DISPLAY_All_ADDRESS = `query fetchAddressByPartnerProfile($partnerProfileUUID:String,$searchInput:String,$contact:Int){
    fetchAddressByPartnerProfile(partnerProfileUUID:$partnerProfileUUID,searchInput:$searchInput,contact:$contact) {
      idAddress
      addressUUID
      addressNameID
      addressLines1
      addressLines2
      addressLines3
      city
      state
      country
      zip
      addressStatus
      addressTypeUUID
      partnerProfileUUID 
      contactUUID
    }
  }`;

const ADD_ADDRESS = `mutation createAddress($input:AddressInput){
    createAddress(input:$input) {
      idAddress
      addressUUID
      addressNameID
      addressLines1
      addressLines2
      addressLines3
      city
      state
      country
      zip
      addressStatus
      addressTypeUUID
      partnerProfileUUID 
      createdOn
      lastUpdatedOn
      contactUUID
    } 
  }
  `;
const UPDATE_CONTACT = `mutation updateContact($input:ContactInput){
    updateContact(input:$input) {
      idContact
      contactUUID
      role
      firstName
      lastName
      email
      phone
      fax
      addressUUID
      contactTypeUUID
      partnerProfileUUID
      createdOn
      lastUpdatedOn
    } 
     
  }`;
const RemoveAddress_Add_ADDDRESSUUID = `mutation removeAddressandaddAddressUUID($AddressInput:ContactInput){
    removeAddressandaddAddressUUID(input:$AddressInput) {
      idContact
      contactUUID
      role
      firstName
      lastName
      email
      phone
      fax
      addressUUID
      contactTypeUUID
      partnerProfileUUID
      createdOn
      lastUpdatedOn
      idAddress
      addressNameID
      addressLines1
      addressLines2
      addressLines3
      city
      state
      country
      zip
      addressStatus
      addressTypeUUID
      addressconatctUUID
      contactAddressUUID
    } }
  `;
export default function AddAddress(props) {
  console.log(props);
  const [updateAddress, updateloading, updatedata] = useMutation(
    UPDATE_ADDRESS
  );
  const addressData = useQuery(DISPLAY_All_ADDRESS, {
    variables: {
      partnerProfileUUID: props.uuid,
      searchInput: '',
      contact: 1,
    },
  });
  const fAData = useQuery(DISPLAY_ADDRESS, {
    variables: {
      addressUUID: props.rowData.addressUUID,
      contactUUID: props.rowData.contactUUID,
    },
  });
  //UPDATE CONTACT
  const [updateContact] = useMutation(UPDATE_CONTACT);
  const [removeAddressandADDUUID] = useMutation(RemoveAddress_Add_ADDDRESSUUID);

  const [addAddress, addloading, adddata] = useMutation(ADD_ADDRESS);
  const [addreValue, setAddressTYpe] = React.useState([]);
  const [address, setAddress] = React.useState([]);
  const { loading, data, errors } = useQuery(DISPLAY_All_ADDRESSTYPES);
  const [tempvalue, setTempValue] = React.useState(false);
  //const classes = useStyles();
  console.log(props.rowData);
  const [values, setValues] = React.useState({
    addressName: '',
    addressLines1: '',
    addressLines2: '',
    zip: '',
    addressType: '',
    state: '',
    country: '',
    city: '',
    addressStatus: '',
    addressLines3: '',
    addressUUID: null,
    contactUUID: null,
  });
  const [Existingvalues, setExisingValues] = React.useState({
    addressName: '',
    addressLines1: '',
    addressLines2: '',
    zip: '',
    addressType: '',
    state: '',
    country: '',
    city: '',
    addressStatus: '',
    addressLines3: '',
    addressUUID: null,
    contactUUID: null,
  });

  const [readValue, setReadOnly] = React.useState(false);
  const [submitErr,setSubmitErr] = React.useState(false);
  const [ErrMsg, setErrMsg] = React.useState(false);
  const [errorInfo, setErrorInfo] = React.useState({
    country: false,
    zip: false,
    addressType: false,
    state: false,
    addressName: false,
    addressLines1: false,
    addressLines2: false,
    addressLines3: false,
    city: false,
    addressUUID: false,
  });
  const zipexp = /^[A-Z0-9]{5,6}$/;

  const handleChange = (name) => (event) => {
    console.log(event.target.value, name);
    setErrMsg(false);
    setValues({ ...values, [name]: event.target.value });
    switch (name) {
      case 'addressUUID':
        if (event.target.value == null) {
          console.log(tempvalue);
          setReadOnly(false);
          console.log(props.rowData);
          if (
            // (props.title == 'add' && tempvalue == false) ||
            // (props.title !== 'add' &&
            //   tempvalue == true &&
            //   event.target.value == null)
            tempvalue == true
            // (props.title == 'add' && props.rowData.addressUUID == null) ||
            // (props.title !== 'add' &&
            //    props.rowData.addressUUID == null &&
            //   values.contactUUID == '')
          ) {
            setValues({
              addressName: '',
              addressLines1: '',
              addressLines2: '',
              zip: '',
              addressType: '',
              state: '',
              country: '',
              city: '',
              addressStatus: 'Active',
              addressLines3: '',
              addressUUID: null,
              contactUUID: null,
              //addressconatctUUID: props.rowData.addressconatctUUID,
              //contactAddressUUID: props.rowData.contactAddressUUID,
            });
          } else {
            console.log(props.rowData, Existingvalues);
            setValues({
              addressName: Existingvalues.addressName,
              addressLines1: Existingvalues.addressLines1,
              addressLines2: Existingvalues.addressLines2,
              zip: Existingvalues.zip,
              addressType: Existingvalues.addressType,
              state: Existingvalues.state,
              country: Existingvalues.country,
              city: Existingvalues.city,
              addressStatus: Existingvalues.addressStatus,
              addressLines3: Existingvalues.addressLines3,
              addressUUID: Existingvalues.addressUUID,
              contactUUID: Existingvalues.contactUUID,
              //addressconatctUUID: props.rowData.addressconatctUUID,
              // contactAddressUUID: props.rowData.contactAddressUUID,
            });
          }
          //  setErrorInfo({ ...errorInfo, [name]: false });
        } else {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
          var result = address.filter((obj) => {
            return obj.addressUUID === event.target.value;
          });
          console.log(result);
          setReadOnly(true);

          setValues({
            addressName: result[0].addressNameID,
            addressLines1: result[0].addressLines1,
            addressLines2: result[0].addressLines2,
            zip: result[0].zip,
            addressType: result[0].addressTypeUUID,
            state: result[0].state,
            country: result[0].country,
            city: result[0].city,
            addressStatus: result[0].addressStatus,
            addressLines3: result[0].addressLines3,
            addressUUID: result[0].addressUUID,
            contactUUID: result[0].contactUUID,
            // addressconatctUUID: props.rowData.addressconatctUUID,
            // contactAddressUUID: props.rowData.contactAddressUUID,
          });
        }
        break;
      case 'addressName':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[a-zA-Z0-9,-.:/ ]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'addressLines1':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[a-zA-Z0-9,-.:/ ]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'addressLines2':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[a-zA-Z0-9,-.:/ ]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'addressLines3':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[a-zA-Z0-9,-.:/ ]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'city':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[a-zA-Z ]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
        } else setErrorInfo({ ...errorInfo, [name]: true });
        break;
      case 'state':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[a-zA-Z ]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
        } else setErrorInfo({ ...errorInfo, [name]: true });
        break;

      case 'country':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[a-zA-Z ]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
        } else setErrorInfo({ ...errorInfo, [name]: true });
        break;
      case 'zip':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match(zipexp) 
          //event.target.value.length <= 6
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
          console.log(event.target.value);
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'addressType':
        setValues({
          ...values,
          [event.target.name]: event.target.value,
        });
        break;
      case 'addressStatus':
        if (event.target.checked) {
          setValues({
            ...values,
            [event.target.name]: 'Active',
          });
        } else {
          setValues({
            ...values,
            [event.target.name]: 'Inactive',
          });
        }

        break;

      default:
        return false;
    }
  };

  useEffect(() => {
    var obj = [];
    if (data !== undefined && data.allAddressTypes.length > 0) {
      data.allAddressTypes.map(function (v, i) {
        obj.push({ value: v.addressTypeUUID, label: v.addressTypeName });
      }, {});

      console.log('obj', obj);
      setAddressTYpe(obj);
    }
    console.log(fAData);
    if (
      fAData !== undefined &&
      fAData.data !== undefined &&
      fAData.data.fetchAddressByUUID !== undefined
    ) {
      let addressRes = fAData.data.fetchAddressByUUID[0];
      console.log(addressRes);
      if (addressRes !== undefined) {
        setValues({
          addressName: addressRes.addressNameID,
          addressLines1: addressRes.addressLines1,
          addressLines2: addressRes.addressLines2,
          zip: addressRes.zip,
          addressType: addressRes.addressTypeUUID,
          state: addressRes.state,
          country: addressRes.country,
          city: addressRes.city,
          addressStatus: addressRes.addressStatus,
          addressLines3: addressRes.addressLines3,
          addressUUID: addressRes.addressUUID,
          contactUUID: addressRes.contactUUID,
        });
        if (props.rowData.addressUUID == null) {
          setExisingValues({
            addressName: addressRes.addressNameID,
            addressLines1: addressRes.addressLines1,
            addressLines2: addressRes.addressLines2,
            zip: addressRes.zip,
            addressType: addressRes.addressTypeUUID,
            state: addressRes.state,
            country: addressRes.country,
            city: addressRes.city,
            addressStatus: addressRes.addressStatus,
            addressLines3: addressRes.addressLines3,
            addressUUID: addressRes.addressUUID,
            contactUUID: addressRes.contactUUID,
          });
        }
      }
    }
    if (
      addressData !== undefined &&
      addressData.data !== undefined &&
      addressData.data.fetchAddressByPartnerProfile.length > 0
    ) {
      setAddress(addressData.data.fetchAddressByPartnerProfile);
      if (values.addressUUID != null) {
        if (values.addressUUID != null && props.title !== 'add') {
          setReadOnly(true);
          // setTempValue(false);
        }
        var result = addressData.data.fetchAddressByPartnerProfile.filter(
          (obj) => {
            return obj.addressUUID === values.addressUUID;
          }
        );
        console.log(result);
        if (result != null && result.length == 0) {
          setTempValue(false);
          setReadOnly(false);
        } else {
          setTempValue(true);
          setReadOnly(true);
        }
      } else if ((values.addressUUID = null && props.title == 'add')) {
        setReadOnly(false);
        // setTempValue(false);
      } else if ((values.addressUUID = null && props.title !== 'add')) {
        setReadOnly(false);
        // setTempValue(false);
      } else {
        // setTempValue(false);
        //setReadOnly(true);
      }
    }
    console.log(values, props.title);
    if (values.addressUUID == null) {
      // setReadOnly(true);
    }
  }, [data, fAData.data, addressData.data]);
  async function addIntialAddress() {
    console.log('add' + values);
    if (values.addressUUID !== null) {
      //update contact
      console.log('update conatct');
      await updateContact({
        variables: {
          input: {
            role: props.rowData.role,
            firstName: props.rowData.firstName,
            lastName: props.rowData.lastName,
            email: props.rowData.email,
            phone: props.rowData.phone,
            fax: props.rowData.fax,
            contactTypeUUID: props.rowData.contactTypeUUID,
            addressUUID: values.addressUUID,
            partnerProfileUUID: props.uuid,
            contactUUID: props.rowData.contactUUID,
          },
        },
      })
        .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
          console.log(data, error, graphQLErrors, networkError, cacheHit);
          if (data !== null && error == false) {
            props.onSuccess(error, 'Saved successfully', true);
          } else if (error && graphQLErrors.length > 0) {
            let duperror = graphQLErrors[0].message;
            console.log(duperror);
            if (duperror !== null && duperror.indexOf('ER_DUP_ENTRY') !== -1) {
              props.onSuccess(error, 'Email already existed', false);
            }
          }
        })
        .catch((e) => {
          // you can do something with the error here
          console.log(e);
        });
    } else {
      //create personal address for contact
      console.log('create personal address for contact');
      await addAddress({
        variables: {
          input: {
            addressNameID: values.addressName,
            addressLines1: values.addressLines1,
            addressLines2: values.addressLines2,
            addressLines3: values.addressLines3,
            city: values.city,
            county: values.county,
            state: values.state,
            country: values.country,
            zip: values.zip,
            addressStatus: values.addressStatus,
            addressTypeUUID: values.addressType,
            partnerProfileUUID: props.uuid,
            contactUUID: props.rowData.contactUUID,
          },
        },
      })
        .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
          console.log(data, error, graphQLErrors, networkError, cacheHit);
          if (data !== null && error == false) {
            props.onSuccess(error, 'Saved successfully', true);
          } else if (error && graphQLErrors.length > 0) {
            let duperror = graphQLErrors[0].message;
            console.log(duperror);
            if (duperror !== null && duperror.indexOf('ER_DUP_ENTRY') !== -1) {
              props.onSuccess(error, 'Name already existed', false);
            }
          }
        })
        .catch((e) => {
          // you can do something with the error here
          console.log(e);
        });
    }
  }
  const handleSubmit = async (event) => {
    console.log(event.detail);
    if (!event.detail || event.detail == 1) {
      console.log(values);
      setErrMsg(false);
      if (
        !values.addressName &&
        !values.addressLines1 &&
        !values.addressLines2 &&
        !values.addressLines3 &&
        !values.city &&
        !values.country &&
        !values.state &&
        !values.zip &&
        !values.addressType 
      ) {
        setErrMsg(true);
      }else if(!values.addressName ||
        !values.addressLines1 ||
        !values.city ||
        !values.country ||
        !values.state ||
        !values.zip ||
        !values.addressType )
      {
        setSubmitErr(true);
      }
       else if(values.addressName!=="" &&
        values.addressLines1!=="" &&
        values.city!=="" &&
        values.country !=="" &&
        values.state!=="" &&
        values.zip!=="" &&
        values.addressType!=="" &&
        errorInfo.addressName!==true&&
        errorInfo.addressLines1!==true&&
        errorInfo.addressLines2!==true&&
        errorInfo.addressLines3!==true&&
        errorInfo.city!==true&&
        errorInfo.country!==true&&
        errorInfo.state!==true&&
        errorInfo.zip!==true)
      {
        if (props.title == 'add'&&props.rowData.addressUUID == null)
     {
            addIntialAddress();
     }
    else{
        console.log('update ');
        console.log(values, Existingvalues);
        console.log(
          values.addressUUID,
          values.contactUUID,
          values.addressconatctUUID,
          values.contactAddressUUID
        );
        if (
          props.rowData.addressUUID == null &&
          Existingvalues.addressName == ''
        ) {
          addIntialAddress();
        } else if (
          Existingvalues.addressName != '' &&
          values.addressUUID != props.rowData.addressUUID &&
          values.contactUUID !== null
        ) {
          //new adderess //only update address
          console.log('new adderess //only update address');
          console.log(values);
          await updateAddress({
            variables: {
              input: {
                addressNameID: values.addressName,
                addressLines1: values.addressLines1,
                addressLines2: values.addressLines2,
                addressLines3: values.addressLines3,
                city: values.city,
                county: values.county,
                state: values.state,
                country: values.country,
                zip: values.zip,
                addressStatus: values.addressStatus,
                addressTypeUUID: values.addressType,
                partnerProfileUUID: props.uuid,
                contactUUID: props.rowData.contactUUID,
                addressUUID: values.addressUUID,
              },
            },
          })
            .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
              console.log(data, error, graphQLErrors, networkError, cacheHit);
              if (data !== null && error == false) {
                props.onSuccess(error, 'Updated successfully', true);
              } else if (error && graphQLErrors.length > 0) {
                let duperror = graphQLErrors[0].message;
                console.log(duperror);
                if (
                  duperror !== null &&
                  duperror.indexOf('ER_DUP_ENTRY') !== -1
                ) {
                  props.onSuccess(error, 'Name already existed', false);
                }
              }
            })
            .catch((e) => {
              // you can do something with the error here
              console.log(e);
            });
        } else if (
          props.rowData.addressUUID !== null &&
          values.addressUUID !== null &&
          values.addressUUID != props.rowData.addressUUID
        ) {
          //update in conatct table address uuid
          console.log('update in conatct table address uuid');
          await updateContact({
            variables: {
              input: {
                role: props.rowData.role,
                firstName: props.rowData.firstName,
                lastName: props.rowData.lastName,
                email: props.rowData.email,
                phone: props.rowData.phone,
                fax: props.rowData.fax,
                contactTypeUUID: props.rowData.contactTypeUUID,
                addressUUID: values.addressUUID,
                partnerProfileUUID: props.uuid,
                contactUUID: props.rowData.contactUUID,
              },
            },
          })
            .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
              console.log(data, error, graphQLErrors, networkError, cacheHit);
              if (data !== null && error == false) {
                props.onSuccess(error, 'Saved successfully', true);
              } else if (error && graphQLErrors.length > 0) {
                let duperror = graphQLErrors[0].message;
                console.log(duperror);
                if (
                  duperror !== null &&
                  duperror.indexOf('ER_DUP_ENTRY') !== -1
                ) {
                  props.onSuccess(error, 'Email already existed', false);
                }
              }
            })
            .catch((e) => {
              // you can do something with the error here
              console.log(e);
            });
        } else if (
          //values.addressconatctUUID !== null &&
          //values.addressUUID != props.rowData.addressUUID
          props.rowData.addressUUID == null &&
          values.addressUUID != null &&
          values.contactUUID == null
        ) {
          //selected address //delete personal address and add uuid to contact
          console.log(
            'selected address //delete personal address and add uuid to contact'
          );
          console.log(values.addressUUID, values.addressconatctUUID);
          await removeAddressandADDUUID({
            variables: {
              AddressInput: {
                addressUUID: values.addressUUID,
                contactUUID: props.rowData.contactUUID,
              },
            },
          })
            .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
              console.log(data, error, graphQLErrors, networkError, cacheHit);
              if (data !== null && error == false) {
                props.onSuccess(error, 'Updated successfully', true);
              } else if (error && graphQLErrors.length > 0) {
                let duperror = graphQLErrors[0].message;
                console.log(duperror);
                if (
                  duperror !== null &&
                  duperror.indexOf('ER_DUP_ENTRY') !== -1
                ) {
                  props.onSuccess(error, 'Email already existed', false);
                }
              }
            })
            .catch((e) => {
              // you can do something with the error here
              console.log(e);
            });
        } else if (
          // values.contactAddressUUID !== null &&
          //values.addressUUID == null
          props.rowData.addressUUID != null &&
          values.addressUUID == null &&
          values.contactUUID == null
        ) {
          //create new adddress with conatctuuid and address uuid null in conatct
          console.log(
            'create new adddress with conatctuuid and address uuid null in conatct'
          );
          console.log();
          await addAddress({
            variables: {
              input: {
                addressUUID: values.addressUUID,
                addressNameID: values.addressName,
                addressLines1: values.addressLines1,
                addressLines2: values.addressLines2,
                addressLines3: values.addressLines3,
                city: values.city,
                county: values.county,
                state: values.state,
                country: values.country,
                zip: values.zip,
                addressStatus: values.addressStatus,
                addressTypeUUID: values.addressType,
                partnerProfileUUID: props.uuid,
                contactUUID: props.rowData.contactUUID,
              },
            },
          })
            .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
              console.log(data, error, graphQLErrors, networkError, cacheHit);
              if (data !== null && error == false) {
                props.onSuccess(error, 'Updated successfully', true);
              } else if (error && graphQLErrors.length > 0) {
                let duperror = graphQLErrors[0].message;
                console.log(duperror);
                if (
                  duperror !== null &&
                  duperror.indexOf('ER_DUP_ENTRY') !== -1
                ) {
                  props.onSuccess(error, 'Name already existed', false);
                }
              }
            })
            .catch((e) => {
              // you can do something with the error here
              console.log(e);
            });
        } else {
          //nothing to change
          console.log('nothing to change');
          props.onSuccess(false, 'Updated Successfully', true);
        }}
      }
    }
  };
  const handleCloseModal = () => {
    if (props.title == 'add') {
      props.onSuccess(false, 'Saved successfully', true);
    } else {
      props.handleCloseModal(false);
    }
  };
  const handleReset = async () => {
    setErrMsg(false);
    setErrorInfo({
      addressName: false,
      addressLines1: false,
      addressLines2: false,
      addressLines3: false,
      city: false,
      state: false,
      country: false,
      addressType: false,
      zip: false,
      addressUUID: false,
    });
    setValues({
      ...values,
      addressName: '',
      addressLines1: '',
      addressLines2: '',
      addressLines3: '',
      addressType: '',
      city: '',
      state: '',
      country: '',
      zip: '',
      addressUUID: '',
    });
    setSubmitErr(false);
  };
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSvgIcon: {
          root: {
            color: '#FFFFFF',
          },
        },

        MuiInputLabel: {
          animated: {
            fontSize: 13,
          },
          outlined: {
            paddingTop: 3,
          },
        },
        MuiSelect: {
          select: {
            minWidth: '180px',
          },
        },
      },
    });
  console.log(props.rowData);
  if (fAData.loading) return <Loader />;
  return (
    <div>
      <MuiThemeProvider theme={getMuiTheme()}>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={props.isModalOpen}
          onClose={handleCloseModal}
          className="fabmodal"
        >
          <div className="modalpaper">
            <Grid container className="header">
              <Grid item xs={10} sm={10}>
                <h2 id="modal-title" className="h2">
                  {props.title == 'add' ||
                  (props.rowData.addressUUID == null &&
                    Existingvalues.addressName == '')
                    ? 'Add Address'
                    : 'Update Address'}
                </h2>
              </Grid>
              <Grid item xs={2} sm={2} className="close">
                <CloseIcon onClick={handleCloseModal} />
              </Grid>
            </Grid>
            <div>
              {ErrMsg === true ? (
                <div style={{ textAlign: 'center' }} className="addfieldserror">
                  Please fill the fields
                </div>
              ) : (
                ''
              )}
              <form noValidate autoComplete="off">
                <Grid className="AddressconfigGrid">
                  <div style={{ textAlign: 'center' }}>
                    <Grid container spacing={1} className="userformcontent ">
                      <Grid
                        item
                        xs={12}
                        sm={12}
                        gutterBottom
                        variant="body2"
                        color="textSecondary"
                        className="text"
                      >
                        <FormControl>
                          <InputLabel
                            style={{
                              align: 'center',
                              color: '#0b153e',
                              fontSize: 12,
                            }}
                            id="demo-simple-select-label"
                          >
                            Address
                          </InputLabel>
                          <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={values.addressUUID}
                            onChange={handleChange('addressUUID')}
                            name="addressUUID"
                          >
                            <MenuItem value={null}>{'Select'}</MenuItem>
                            {address != undefined && address.length > 0
                              ? address.map((v, k) => {
                                  return (
                                    <MenuItem value={v.addressUUID}>
                                      {v.addressNameID}
                                    </MenuItem>
                                  );
                                })
                              : null}
                          </Select>
                        </FormControl>
                      </Grid>
                    </Grid>
                  </div>
                  <Grid container spacing={3} className="userformcontent ">
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <TextField
                        id="outlined-dense"
                        label="Address Name "
                        className="partnertextField"
                        margin="dense"
                        variant="outlined"
                        //placeholder=""
                        onChange={handleChange('addressName')}
                        value={values.addressName}
                        name="addressName"
                        InputProps={{
                          readOnly: readValue,
                        }}
                      />
                      {errorInfo.addressName === true ? (
                        <div id="nameid" className="addfieldserror">
                       "Allowed alphanumerics and no special characters allowed except[ . , - : /]"

                        </div>
                      ) : (
                        ''
                      )}
                         {submitErr&&!values.addressName? (
                        <div id="nameid" className="addfieldserror">
                          Please enter address name
                        </div>
                      )
                       : ""}
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <TextField
                        id="outlined-dense"
                        label="Address Line  1 "
                        className="partnertextField"
                        margin="dense"
                        variant="outlined"
                        onChange={handleChange('addressLines1')}
                        value={values.addressLines1}
                        name="addressLines1"
                        InputProps={{
                          readOnly: readValue,
                        }}
                      />
                      {errorInfo.addressLines1 === true ? (
                        <div id="nameid" className="addfieldserror">
                         "Allowed alphanumerics and no special characters allowed except[ . , - : /]"

                        </div>
                      ) : (
                        ''
                      )}
                       {submitErr&&!values.addressLines1? (
                        <div id="nameid" className="addfieldserror">
                          Please enter address line  1
                        </div>
                      )
                       : ""}
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <TextField
                        id="outlined-dense"
                        label="Address Line 2 "
                        className="partnertextField"
                        margin="dense"
                        variant="outlined"
                        onChange={handleChange('addressLines2')}
                        value={values.addressLines2}
                        name="addressLines2"
                        InputProps={{
                          readOnly: readValue,
                        }}
                      />
                      {errorInfo.addressLines2 === true ? (
                        <div id="nameid" className="addfieldserror">
                         "Allowed alphanumerics and no special characters allowed except[ . , - : /]"

                        </div>
                      ) : (
                        ''
                      )}
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <TextField
                        id="outlined-dense"
                        label="Address Line 3 "
                        className="partnertextField"
                        margin="dense"
                        variant="outlined"
                        onChange={handleChange('addressLines3')}
                        value={values.addressLines3}
                        name="addressLines3"
                        InputProps={{
                          readOnly: readValue,
                        }}
                      />
                      {errorInfo.addressLines3 === true ? (
                        <div id="nameid" className="addfieldserror">
                          "Allowed alphanumerics and no special characters allowed except[ . , - : /]"
                           
                        </div>
                      ) : (
                        ''
                      )}
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <TextField
                        id="outlined-dense"
                        label="City "
                        className="partnertextField"
                        margin="dense"
                        variant="outlined"
                        //placeholder=""
                        onChange={handleChange('city')}
                        value={values.city}
                        name="city"
                        InputProps={{
                          readOnly: readValue,
                        }}
                      />
                      {errorInfo.city === true ? (
                        <div id="nameid" className="addfieldserror">
                          "Allowed alphabets,no special characters and Numerics
                          are allowed."
                        </div>
                      ) : (
                        ''
                      )}
                       {submitErr&&!values.city? (
                        <div id="nameid" className="addfieldserror">
                          Please enter city
                        </div>
                      )
                       : ""}
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <TextField
                        id="outlined-dense"
                        label="State "
                        className="partnertextField"
                        margin="dense"
                        variant="outlined"
                        onChange={handleChange('state')}
                        value={values.state}
                        name="state"
                        InputProps={{
                          readOnly: readValue,
                        }}
                      />
                      {errorInfo.state === true ? (
                        <div id="nameid" className="addfieldserror">
                          "Allowed alphabets,no special characters and Numerics
                          are allowed."
                        </div>
                      ) : (
                        ''
                      )}
                      {submitErr&&!values.state? (
                        <div id="nameid" className="addfieldserror">
                          Please enter state
                        </div>
                      )
                       : ""}
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <TextField
                        id="outlined-dense"
                        label="Country "
                        className="partnertextField"
                        margin="dense"
                        variant="outlined"
                        onChange={handleChange('country')}
                        value={values.country}
                        name="country"
                        InputProps={{
                          readOnly: readValue,
                        }}
                      />
                      {errorInfo.country === true ? (
                        <div id="nameid" className="addfieldserror">
                          "Allowed alphabets,no special characters and Numerics
                          are allowed."
                        </div>
                      ) : (
                        ''
                      )}
                       {submitErr&&!values.country? (
                        <div id="nameid" className="addfieldserror">
                          Please enter country
                        </div>
                      )
                       : ""}
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <TextField
                        id="outlined-dense"
                        label="Zip "
                        className="partnertextField"
                        margin="dense"
                        variant="outlined"
                        onChange={handleChange('zip')}
                        value={values.zip}
                        name="zip"
                        InputProps={{
                          readOnly: readValue,
                        }}
                      />
                      {errorInfo.zip === true ? (
                        <div id="nameid" className="addfieldserror">
                          "Allowed Uppercase letters & Numbers.Minimum length is 5 and Maximum is 6"
                        </div>
                      ) : (
                        ''
                      )}
                        {submitErr&&!values.zip? (
                        <div id="nameid" className="addfieldserror">
                          Please enter zip
                        </div>
                      )
                       : ""}
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <FormControl disabled={readValue}>
                        <InputLabel
                          style={{ color: '#0b153e', fontSize: 12 }}
                          id="demo-simple-select-label"
                        >
                          Address Type
                        </InputLabel>
                        <Select
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          // value={searchPartner.prefferedProtocolUUID}
                          value={values.addressType}
                          onChange={handleChange('addressType')}
                          name="addressType"
                        >
                          {addreValue !== undefined
                            ? addreValue.map((v, i) => (
                                <MenuItem key={v.value} value={v.value}>
                                  {v.label}
                                </MenuItem>
                              ))
                            : ''}
                        </Select>
                      </FormControl>
                      {submitErr&&!values.addressType? (
                        <div id="nameid" className="addfieldserror">
                          Please select address type
                        </div>
                      )
                       : ""}
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <FormControlLabel
                        disabled={readValue}
                        label="Status"
                        control={
                          <Switch
                            checked={
                              values.addressStatus == 'Active' ? true : false
                            }
                            onChange={handleChange('addressStatus')}
                            name="addressStatus"
                          />
                        }
                      />
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <Button
                        style={{
                          color: '#000006',
                          backgroundColor: '#E5CCFF',
                          display: props.title == 'add'|| (props.rowData.addressUUID == null &&
                            Existingvalues.addressName == '') ? 'block' : 'none',
                        }}
                        onClick={handleReset}
                        variant="contained"
                        fullWidth="true"
                        className="createpartnerbutton"
                      >
                        RESET
                      </Button>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <Button
                        onClick={handleSubmit}
                        vvariant="contained"
                        fullWidth="true"
                        className="createpartnerbutton"
                      >
                        {props.title == 'add' ||
                        (props.rowData.addressUUID == null &&
                          Existingvalues.addressName == '')
                          ? 'SAVE'
                          : 'UPDATE'}
                      </Button>
                    </Grid>
                  </Grid>
                </Grid>
              </form>
            </div>
          </div>
        </Modal>
      </MuiThemeProvider>
    </div>
  );
}
