import MaterialTable from 'material-table';
import { useMutation, useQuery } from 'graphql-hooks';
import { forwardRef, useEffect } from 'react';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import ReactDOM from 'react-dom';
import MTableToolbar from 'material-table';
import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import AddressFrom from './addressForm';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import HomeIcon from '@material-ui/icons/Home';
import Theme from '../css/theme';
import Timestamp from '../timestamp';
import ContactView from './contactView';
import ContactViewModelForm from '../components/tmsmodal';
import AddressAddModelForm from '../components/tmsmodal';
import MenuItem from '@material-ui/core/MenuItem';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import Error from '../components/emptyPage';
import CircularIndeterminateLoader from '../components/loader';
import Loader from '../components/loader';
import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles,
} from '@material-ui/core/styles';
const DISPLAY_All_ADDRESS = `query fetchAddressByPartnerProfile($partnerProfileUUID:String,$searchInput:String,$contact:Int){
  fetchAddressByPartnerProfile(partnerProfileUUID:$partnerProfileUUID,searchInput:$searchInput,contact:$contact) {
    idAddress
    addressUUID
    addressNameID
    addressLines1
    addressLines2
    addressLines3
    city
    state
    country
    zip
    addressStatus
    addressTypeUUID
    partnerProfileUUID 
   
  }
}`;

const DISPLAY_All_CONTACTTYPES = `{
  allContactsType {
    idContactType
    contactTypeUUID
    name
    description
    createdOn
    lastUpdatedOn
  }
}
`;
const ADD_CONTACT = `mutation createContact($input:ContactInput){
  createContact(input:$input) {
    idContact
    contactUUID
    role
    firstName
    lastName
    email
    phone
    fax
    addressUUID
    contactTypeUUID
    createdOn
    lastUpdatedOn
  } 
   
}

`;
const UPDATE_CONTACT = `mutation updateContact($input:ContactInput){
  updateContact(input:$input) {
    idContact
    contactUUID
    role
    firstName
    lastName
    email
    phone
    fax
    addressUUID
    contactTypeUUID
    partnerProfileUUID
    createdOn
    lastUpdatedOn
  } 
   
}


`;

const REMOVE_CONTACT = `mutation removeContact($input:ContactInput){
  removeContact(input:$input) {
    idContact
    contactUUID
    role
    firstName
    lastName
    email
    phone
    fax
    addressUUID
    contactTypeUUID
    partnerProfileUUID
    createdOn
    lastUpdatedOn
  } 
}
`;
const FETCH_CONTACT_BY_PARTNERPROFILEUUIS = `query fetchContactByPartnerProfile($partnerProfileUUID:String){
  fetchContactByPartnerProfile(partnerProfileUUID:$partnerProfileUUID) {
    idContact
    contactUUID
    role
    firstName
    lastName
    email
    phone
    fax
    addressUUID
    contactTypeUUID
    partnerProfileUUID
    createdOn
    lastUpdatedOn
    idAddress
    addressNameID
    addressLines1
    addressLines2
    addressLines3
    city
    state
    country
    zip
    addressStatus
    addressTypeUUID
    addressconatctUUID
    contactAddressUUID
  } 
}`;
const FETCH_CONTACT = `query fetchContactByPartnerProfile1($partnerProfileUUID:String){
  fetchContactByPartnerProfile1(partnerProfileUUID:$partnerProfileUUID) {
    idContact
    contactUUID
    role
    firstName
    lastName
    email
    phone
    fax
    addressUUID
    contactTypeUUID
    contactTypeName
    partnerProfileUUID
    createdOn
    lastUpdatedOn
   
   
  } 
}`;

const DISPLAY_All_ADDRESSTYPES = `{
  allAddressTypes {
    idAddressType
    addressTypeUUID
    addressTypeName
    addressTypeDescription
    createdOn
    lastUpdatedOn
  }
}
`;

function iconStyles() {
  return {};
}
export default function Contact(props) {
  console.log(props);
  //all address
  const addrestypeData = useQuery(DISPLAY_All_ADDRESSTYPES);
  const addressData = useQuery(DISPLAY_All_ADDRESS, {
    variables: {
      partnerProfileUUID: props.uuid,
      searchInput: '',
      contact: 1,
    },
  });
  //all contacttypeData
  const contacttypeData = useQuery(DISPLAY_All_CONTACTTYPES);

  //Fetch ALL ADDRESS BY PARTNER PROFILE id
  /* const { loading, error, data, refetch } = useQuery(
    FETCH_CONTACT_BY_PARTNERPROFILEUUIS,
    {
      variables: {
        partnerProfileUUID: props.uuid,
      },
    }
  );*/
  const { loading, error, data, refetch } = useQuery(FETCH_CONTACT, {
    variables: {
      partnerProfileUUID: props.uuid,
    },
  });
  //UPDATE CONTACT
  const [updateContact, updateloading, updatedata] = useMutation(
    UPDATE_CONTACT
  );
  //remove CONTACT
  const [removeContact, removeloading, removedata] = useMutation(
    REMOVE_CONTACT
  );
  //
  const [addContact, addloading, adddata] = useMutation(ADD_CONTACT);
  const [contactdata, setData] = React.useState([]);
  const [contactIddata, setcontactId] = React.useState('');
  const [openAddressModal, setOpenAddressModal] = React.useState(false);
  const [addressvalData, setAddressData] = React.useState({});
  const [contactTypeData, setContactTypeData] = React.useState({});
  const [addreValue, setAddressTYpe] = React.useState({});
  const [viewopen, setViewForm] = React.useState(false);
  const [contactRowData, setContactRowData] = React.useState();
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [message, setSnackbarMessage] = React.useState();
  const [variant, setSnackbarVariant] = React.useState('error');
  const [serverError, setServerError] = React.useState(false);
  const [addAddressForm, setAddressForm] = React.useState('update');
  const [addressFormLoader, setAddressFormLoader] = React.useState(false);
  const [nameError, setNameError] = React.useState({
    error: false,
    label: '',
    helperText: '',
    validateInput: false,
  });
  const emailexp = /^(\w+([\.-]?\w+)*@(\w{3,})+([\.-]?\w+)*(\.\w{2,3})+$)/;
  const phonexp = /^(\+\d{1,2}\s?)?1?\-?\.?\s?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/;
  const nameexp = /^[a-zA-Z\s]+$/;
  const faxexp = /^[a-z+0-9-\s]*$/;
  const columns = [
    { title: 'UUID', field: 'idContact', editable: 'never', hidden: true },
    {
      title: 'conatctUUID',
      field: 'contactUUID',
      editable: 'never',
      hidden: true,
    },
    {
      title: 'Contact Type ',
      field: 'contactTypeUUID',
      //lookup: contactTypeData,
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              //placeholder="Description"
              margin="normal"
              select
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            >
              {contacttypeData.data != undefined &&
              contacttypeData.data.allContactsType.length > 0
                ? contacttypeData.data.allContactsType.map((v, k) => {
                    return (
                      <MenuItem value={v.contactTypeUUID}>{v.name}</MenuItem>
                    );
                  })
                : null}
            </TextField>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.contactTypeName}</div>;
      },
    },
    {
      title: 'Role',
      field: 'role',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
                      
            <TextField
              placeholder="Role"
              margin="normal"
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            />
                     
            <div style={{ color: 'red' }}>
                          
              {!props.rowData.role || nameexp.test(props.rowData.role)
                ? ''
                : 'Enter only alphabets'}
                        
            </div>{' '}
                    
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.role}</div>;
      },
    },
    {
      title: 'Last Name',
      field: 'lastName',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
                     
            <TextField /*variant="Standard"*/ /* label="addresstypeName"*/
              placeholder="Last Name"
              margin="normal"
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            />
                      
            <div style={{ color: 'red' }}>
                          
              {!props.rowData.lastName || nameexp.test(props.rowData.lastName)
                ? ''
                : 'Enter only alphabets'}
                       
            </div>{' '}
                    
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.lastName}</div>;
      },
    },
    {
      title: 'First Name',
      field: 'firstName',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
                      
            <TextField /*variant="Standard"*/ /* label="addresstypeName"*/
              placeholder="First Name"
              margin="normal"
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            />
                   
            <div style={{ color: 'red' }}>
                      
              {!props.rowData.firstName || nameexp.test(props.rowData.firstName)
                ? ''
                : 'Enter only alphabets'}
                        
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.firstName}</div>;
      },
    },

    {
      title: 'Email',
      field: 'email',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            {' '}
                      
            <TextField /*variant="Standard"*/ /* label="addresstypeName"*/
              required
              placeholder="Email"
              margin="normal"
              type="email"
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            />
                      
            <div style={{ color: 'red' }}>
              {!props.rowData.email || emailexp.test(props.rowData.email)
                ? ''
                : 'Enter valid email address'}
            </div>
                    
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.email}</div>;
      },
    },
    {
      title: 'Phone',
      field: 'phone',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
                      
            <TextField /*variant="Standard"*/ /* label="addresstypeName"*/
              required="true"
              placeholder="Phone"
              margin="normal"
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            />
                      
            <div style={{ color: 'red' }}>
              {!props.rowData.phone || phonexp.test(props.rowData.phone)
                ? ' '
                : 'Enter valid phone number'}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.phone}</div>;
      },
    },
    {
      title: 'Fax',
      field: 'fax',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
                      
            <TextField /*variant="Standard"*/ /* label="addresstypeName"*/
              placeholder="Fax"
              margin="normal"
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            />
                      
            <div style={{ color: 'red' }}>
              {!props.rowData.fax || faxexp.test(props.rowData.fax)
                ? ''
                : 'Enter valid fax number'}
                        
            </div>
                  
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.fax}</div>;
      },
    },
    {
      title: 'Address',
      field: 'addressUUID',
      hidden: true,
      lookup: addressvalData /* editComponent: (props) => {
          console.log('editComponent', props, props.value);
          return (
            <Select
              placeholder="Fax"
              margin="normal"
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
              value={props.value}
            >
              <MenuItem key={1} value="Not selected">
                Not selected
              </MenuItem>
              <MenuItem key={2} value="Mr">
                Mr.
              </MenuItem>
              <MenuItem key={3} value="Mrs">
                Mrs.
              </MenuItem>
              <MenuItem key={4} value="Ms">
                Ms.
              </MenuItem>
              <MenuItem key={5} value="Dr">
                Dr.
              </MenuItem>
            </Select>
          );
        },*/,
    },
    {
      title: 'Partner Profile ',
      field: 'partnerProfileUUID',
      hidden: true,
    },

    {
      title: 'Created',
      field: 'createdOn',
      editable: 'never',
      render: (rowData) => {
        console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.createdOn);
        } else {
          return;
        }
      },
    },
    {
      title: 'Last Modified',
      field: 'lastUpdatedOn',
      editable: 'never',
      render: (rowData) => {
        console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.lastUpdatedOn);
        } else {
          return;
        }
      },
    },
  ];
  const classes = makeStyles(iconStyles)();
  const tableIcons = {
    Add: forwardRef((props, ref) => (
      <AddBox color="primary" {...props} ref={ref} />
    )),
    Check: forwardRef((props, ref) => (
      <Check color="primary" {...props} ref={ref} />
    )),
    Clear: forwardRef((props, ref) => (
      <Clear color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline color="error" {...props} ref={ref} />
    )),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => (
      <Edit color="primary" {...props} ref={ref} />
    )),
    Export: forwardRef((props, ref) => (
      <SaveAlt color="primary" {...props} ref={ref} />
    )),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => (
      <FirstPage color="primary" {...props} ref={ref} />
    )),
    LastPage: forwardRef((props, ref) => (
      <LastPage color="primary" {...props} ref={ref} />
    )),
    NextPage: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft color="primary" {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => (
      <Clear color="primary" {...props} ref={ref} />
    )),
    Search: forwardRef((props, ref) => (
      <Search color="primary" {...props} ref={ref} />
    )),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };
  const handleRefetch = (value, message, modelClose) => {
    refetch({
      updateData(_, data) {
        return data;
      },
    });
    setOpenSnackbar(true);
    setSnackbarMessage(message);
    if (value) {
      setSnackbarVariant('error');
    } else {
      setSnackbarVariant('success');
    }
    if (modelClose) {
      handleCloseAddress();
    }
  };

  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }
  async function handleAddContact(newData) {
    return await new Promise(async (resolve) => {
      resolve();
      await addContact({
        variables: {
          input: {
            role: newData.role,
            firstName: newData.firstName,
            lastName: newData.lastName,
            email: newData.email,
            phone: newData.phone,
            fax: newData.fax,
            addressUUID: newData.addressUUID,
            contactTypeUUID: newData.contactTypeUUID,
            partnerProfileUUID: props.uuid,
          },
        },
      })
        .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
          console.log(data, error, graphQLErrors, networkError, cacheHit);
          if (data !== null && error == false) {
            setAddressForm('add');
            console.log(addressFormLoader, 'addressFormLoader value');
            setAddressFormLoader(true);
            console.log(addressFormLoader, 'addressFormLoader value');
            handleAddressModal(true, data.createContact[0], 'add');
          } else if (error && graphQLErrors.length > 0) {
            let duperror = graphQLErrors[0].message;
            console.log(duperror);
            if (
              duperror !== null &&
              duperror.indexOf('ER_DUP_ENTRY') !== -1 
              
            ) {
              if(duperror.includes('email') )
              {
              handleRefetch(error, 'Email already existed');
              }
              else if(duperror.includes('phone'))
              {
                handleRefetch(error, 'Phone number already existed');
              }
            }
          }
        })
        .catch((e) => {
          // you can do something with the error here
          console.log(e);
        });
    });
    /*  console.log(addloading, adddata);
      if (addloading.loading) {
        console.log('contact not created');
        handleRefetch(addloading.loading, 'Graphql hooks error');
      } else {
        console.log('contact added successfully');
        handleRefetch(addloading.loading, 'Saved successfully');
      }
    });*/
  }
  async function handleUpdateContact(newData, oldData) {
    return await new Promise(async (resolve) => {
      resolve();
      await updateContact({
        variables: {
          input: {
            role: newData.role,
            firstName: newData.firstName,
            lastName: newData.lastName,
            email: newData.email,
            phone: newData.phone,
            fax: newData.fax,
            addressUUID: newData.addressUUID,
            contactTypeUUID: newData.contactTypeUUID,
            partnerProfileUUID: props.uuid,
            contactUUID: newData.contactUUID,
          },
        },
      })
        .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
          console.log(data, error, graphQLErrors, networkError, cacheHit);
          if (data !== null && error == false) {
            handleRefetch(error, 'Updated successfully');
          } else if (error && graphQLErrors.length > 0) {
            let duperror = graphQLErrors[0].message;
            console.log(duperror);
            if (
              duperror !== null &&
              duperror.indexOf('ER_DUP_ENTRY') !== -1 
              
            ) {
              if(duperror.includes('email') )
              {
              handleRefetch(error, 'Email already existed');
              }
              else if(duperror.includes('phone'))
              {
                handleRefetch(error, 'Phone number already existed');
              }
            }
          }
        })
        .catch((e) => {
          // you can do something with the error here
          console.log(e);
        });
      /*  if (updateloading.loading) {
        console.log('contact not updated');
        handleRefetch(updateloading.loading, 'Graphql hooks error');
      } else {
        console.log('contact updated successfully');
        handleRefetch(updateloading.loading, 'Updated successfully');
      }*/
    });
  }
  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }
  useEffect(() => {
    if (data !== undefined) {
      console.log(data.fetchContactByPartnerProfile1);
      setData(data.fetchContactByPartnerProfile1);
      setServerError(false);
    } else {
      console.log('server error');
      setServerError(true);
    }
    // address drop down
    /* if (addressData !== undefined ){
      console.log(addressData);
      }*/
    if (
      addressData !== undefined &&
      addressData.data !== undefined &&
      addressData.data.fetchAddressByPartnerProfile.length > 0
    ) {
      let addressDetails = addressData.data.fetchAddressByPartnerProfile;
      console.log(addressDetails);

      var objectVal = addressDetails.reduce(
        (obj, item) => ((obj[item.addressUUID] = item.addressNameID), obj),
        {}
      );
      setAddressData(objectVal);
    }
    //conatct types drop down
    if (
      contacttypeData !== undefined &&
      contacttypeData.data !== undefined &&
      contacttypeData.data.allContactsType.length > 0
    ) {
      let contactTypeDetails = contacttypeData.data.allContactsType;
      console.log(contactTypeDetails);

      var objectVal = contactTypeDetails.reduce(
        (obj, item) => ((obj[item.contactTypeUUID] = item.name), obj),
        {}
      );
      console.log('objectVal', objectVal);
      setContactTypeData(objectVal);
    }
    if (
      addrestypeData !== undefined &&
      addrestypeData.data !== undefined &&
      addrestypeData.data.allAddressTypes.length > 0
    ) {
      let addressTypeDetails = addrestypeData.data.allAddressTypes;
      console.log(addressTypeDetails);

      var objectVal = addressTypeDetails.reduce(
        (obj, item) => (
          (obj[item.addressTypeUUID] = item.addressTypeName), obj
        ),
        {}
      );
      setAddressTYpe(objectVal);
    }
  }, [data]);
  async function handleRemoveContact(oldData) {
    return await new Promise(async (resolve) => {
      resolve();
      await removeContact({
        variables: {
          input: {
            contactUUID: oldData.contactUUID,
          },
        },
      });
      console.log(removeloading, removedata);
      if (removeloading.loading) {
        console.log('contact  not deleted');
        handleRefetch(removeloading.loading, 'Graphql hooks error');
      } else {
        console.log('contact deleted successfully');
        //handleRefetch();
        handleRefetch(removeloading.loading, 'Deleted successfully');
      }
    });
  }
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSelect: {
          select: {
            minWidth: 60,
          },
          icon: {
            color: '#0b153e',
          },
        },
        MuiTypography: {
          h6: {
            fontSize: 14,
            fontFamily: 'Arial !important',
          },
        },

        MuiTableSortLabel: {
          root: {
            color: '#ffffff !important',
          },
        },
        MuiTableSortLabel: {
          root: {
            hover: {
              color: '#ffffff !important',
            },
          },
          MuiTableSortLabel: {
            root: {
              MuiTableSortLabel: {
                active: {
                  color: '#ffffff !important',
                },
              },
            },
          },
        },
      },
    });
  const handleClickOpen = (e, rowData) => {
    console.log(e, rowData);
    setContactRowData(rowData);
    setViewForm(true);
  };
  const handleCloseContact = () => {
    setViewForm(false);
  };
  const handleCloseAddress = (val) => {
    setOpenAddressModal(val);
  };
  const handleAddressModal = (val, rowData, title) => {
    setAddressForm(title);
    setContactRowData(rowData);
    setOpenAddressModal(val);
  };
  console.log(
    loading,
    'loading value',
    addressFormLoader,
    'addressFormLoader value'
  );
  if (loading) return <Loader />;
  // if (loading) return<Loader type={'Please wait....'} />;
  if (serverError)
    return <Error type={'Sever connection lost.Please try again'} />;
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <MaterialTable
        icons={tableIcons}
        title=""
        columns={columns}
        data={contactdata}
        editable={{
          // onRowAdd: (newData) => handleAddContact(newData),
          onRowAdd: (newData, rowData) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                newData.submitted = true;
                if (
                  !newData.role ||
                  !nameexp.test(newData.role) ||
                  !newData.firstName ||
                  !nameexp.test(newData.firstName) ||
                  !newData.lastName ||
                  !nameexp.test(newData.lastName) ||
                  !newData.email ||
                  !emailexp.test(newData.email) ||
                  !newData.phone ||
                  !phonexp.test(newData.phone) ||
                  !faxexp.test(newData.fax) ||
                  !newData.contactTypeUUID
                ) {
                  setNameError({
                    error: true,
                    label: 'required',
                    helperText: 'Required.',
                    validateInput: true,
                  });
                  reject();
                  return;
                }
                resolve();
                handleAddContact(newData);
              }, 600);
            }),

          onRowUpdate: (newData, oldData) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                newData.submitted = true;
                if (
                  !newData.role ||
                  !nameexp.test(newData.role) ||
                  !newData.firstName ||
                  !nameexp.test(newData.firstName) ||
                  !newData.lastName ||
                  !nameexp.test(newData.lastName) ||
                  !newData.email ||
                  !emailexp.test(newData.email) ||
                  !newData.phone ||
                  !phonexp.test(newData.phone) ||
                  !faxexp.test(newData.fax) ||
                  !newData.contactTypeUUID
                ) {
                  setNameError({
                    error: true,
                    label: 'required',
                    helperText: 'Required.',
                    validateInput: true,
                  });
                  reject();
                  return;
                }
                resolve();
                handleUpdateContact(newData, oldData);
              }, 600);
            }),
          onRowDelete: (oldData) => handleRemoveContact(oldData),
        }}
        actions={[
          {
            icon: HomeIcon,
            tooltip: 'Add Address',
            onClick: (event, rowData) =>
              handleAddressModal(true, rowData, 'update'),
          },
        ]}
        options={{
          headerStyle: {
            textAlign: 'center',
            fontSize: 12,
            fontWeight: 'bold',
            fontFamily: 'Arial !important',
            backgroundColor: '#0b153e',
            color: '#ffffff',
            padding: '4px',
          },

          searchFieldStyle: {
            color: '#0b153e',
          },
          actionsColumnIndex: -1,
        }}
        onRowClick={(event, rowData) => handleClickOpen(event, rowData)}
      />

      <AddressAddModelForm
        handleCloseModal={handleCloseAddress}
        isModalOpen={openAddressModal}
      >
        <AddressFrom
          handleCloseModal={handleCloseAddress}
          isModalOpen={openAddressModal}
          uuid={props.uuid}
          rowData={contactRowData}
          onSuccess={handleRefetch}
          title={addAddressForm}
        />
      </AddressAddModelForm>
      <ContactViewModelForm
        handleCloseModal={handleCloseContact}
        isModalOpen={viewopen}
        title={'Contact'}
      >
        <ContactView
          handleCloseModal={handleCloseContact}
          isModalOpen={viewopen}
          rowData={contactRowData}
        />
      </ContactViewModelForm>
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        autoHideDuration={1500}
        open={openSnackbar}
        onClose={handleCloseSnackbar}
      >
        <MySnackbarContentWrapper
          onClose={handleCloseSnackbar}
          variant={variant}
          message={message}
        />
      </Snackbar>
    </MuiThemeProvider>
  );
}
