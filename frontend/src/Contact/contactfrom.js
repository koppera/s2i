import React, { useEffect, useContext, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing(2)
  }
}));

export default function Switches() {
  const classes = useStyles();
  const [ContactTypeUUID, setContactTypeUUID] = React.useState('');
  const [state, setState] = React.useState({
    checkedA: true,
    checkedB: true
  });

  const handleChange1 = event => {
    setContactTypeUUID(event.target.value);
  };

  const handleChange = event => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };

  const [values, setValues] = React.useState({
    role: '',
    firstname: '',
    lastname: '',
    country: '',
    addressUUID: '',
    phone: '',
    email: ''
  });
  const [ErrMsg, setErrMsg] = React.useState(false);
  const [errorInfo, setErrorInfo] = React.useState({
    role: false,
    firstname: false,
    lastname: false,
    country: false,
    addressUUID: false,
    phone: false,
    email: false
  });
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [value, setValue] = React.useState('IS');
  const [variant, setSnackbarVariant] = React.useState('error');
  const [message, setSnackbarMessage] = React.useState();

  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false); 
  }

  const handleChange2 = name => event => {
    setErrMsg(false);
    setValues({ ...values, [name]: event.target.value });

    switch (name) {
      case 'role':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match('^([a-zA-Z])[a-zA-Z]*$') &&
          event.target.value.length <= 25
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
        } else setErrorInfo({ ...errorInfo, [name]: true });
        break;
      case 'firstname':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match('^([a-zA-Z])[a-zA-Z]*$') &&
          event.target.value.length <= 25
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
        } else setErrorInfo({ ...errorInfo, [name]: true });
        break;

      case 'lastname':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match('^([a-zA-Z])[a-zA-Z]*$') &&
          event.target.value.length <= 25
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
        } else setErrorInfo({ ...errorInfo, [name]: true });
        break;
      case 'country':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match('^([a-zA-Z])[a-zA-Z]*$') &&
          event.target.value.length <= 25
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
        } else setErrorInfo({ ...errorInfo, [name]: true });
        break;

      case 'phone':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[7-9][0-9]{9}$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
          console.log(event.target.value);
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;

      case 'email':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match(
            '^[a-zA-Z0-9.]+(@[a-zA-Z0-9]*)+([a-zA-Z]{3,})+(.[a-zA-Z]{2,3})$'
          )
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
        } else setErrorInfo({ ...errorInfo, [name]: true });
        break;

      case 'addressUUID':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match('^([a-zA-Z])[a-zA-Z]*$') &&
          event.target.value.length <= 25
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
        } else setErrorInfo({ ...errorInfo, [name]: true });
        break;

      default:
        return false;
    }
  };

  const handleSubmit = async () => {
    console.log(values);
    setErrMsg(false);
    if (
      values.role === '' ||
      values.firstname === '' ||
      values.lastname === '' ||
      values.country === '' ||
      values.addressUUID === '' ||
      values.phone === '' ||
      values.email === ''
    ) {
      setErrMsg(true);
    } else {
      setErrMsg(false);
      setValues({
        ...values,
        role: '',
        firstname: '',
        lastname: '',
        country: '',
        addressUUID: '',
        phone: '',
        email: ''
      });
    }
  };

  return (
    <div>
      {ErrMsg === true ? (
        <div style={{ textAlign: 'center' }} className="addfieldserror">
          Please fill the fields
        </div>
      ) : (
        ''
      )}

      <form className="content" noValidate autoComplete="off">
        <TextField
          id="outlined-dense"
          label="Role "
          className="textField"
          margin="dense"
          variant="outlined"
          onChange={handleChange2('role')}
          value={values.name}
          name="role"
        />

        {errorInfo.role === true ? (
          <div id="nameid" className="addfieldserror">
            "Allowed alphabets,no special characters and Numerics are
            allowed.Maximum length is 25."
          </div>
        ) : (
          ''
        )}

        <TextField
          id="outlined-dense"
          label="First Name "
          className="textField"
          margin="dense"
          variant="outlined"
          onChange={handleChange2('firstname')}
          value={values.firstName}
          name="firstname"
        />

        {errorInfo.firstname === true ? (
          <div id="nameid" className="addfieldserror">
            "Allowed alphabets,no special characters and Numerics are
            allowed.Maximum length is 25."
          </div>
        ) : (
          ''
        )}

        <TextField
          id="outlined-dense"
          label="Last Name "
          className="textField"
          margin="dense"
          onChange={handleChange2('lastname')}
          value={values.lastName}
          variant="outlined"
          name="lastname"
        />

        {errorInfo.lastname === true ? (
          <div id="nameid" className="addfieldserror">
            "Allowed alphabets,no special characters and Numerics are
            allowed.Maximum length is 25."
          </div>
        ) : (
          ''
        )}

        <TextField
          id="outlined-dense"
          label="Email "
          className="textField"
          margin="dense"
          onChange={handleChange2('email')}
          value={values.email}
          variant="outlined"
          name="email"
        />

        {errorInfo.email === true ? (
          <div id="nameid" className="addfieldserror">
            "Please provide valid Email Address"
          </div>
        ) : (
          ''
        )}

        <TextField
          id="outlined-dense"
          label="Phone "
          className="textField"
          margin="dense"
          onChange={handleChange2('phone')}
          value={values.phone}
          variant="outlined"
          name="phone"
        />
        {errorInfo.phone === true ? (
          <div id="nameid" className="addfieldserror">
            "Please provide valid Number.Maximum length is 10"
          </div>
        ) : (
          ''
        )}

        <TextField
          id="outlined-dense"
          label="Fax "
          className="textField"
          margin="dense"
          variant="outlined"
          name="Fax"
        />

        <TextField
          id="outlined-dense"
          label="Country "
          className="textField"
          margin="dense"
          onChange={handleChange2('country')}
          value={values.country}
          variant="outlined"
          name="country"
        />

        {errorInfo.country === true ? (
          <div id="nameid" className="addfieldserror">
            "Allowed alphabets,no special characters and Numerics are
            allowed.Maximum length is 25."
          </div>
        ) : (
          ''
        )}

        <TextField
          id="outlined-dense"
          label="AddressUUID "
          className="textField"
          margin="dense"
          onChange={handleChange2('addressUUID')}
          value={values.addressUUID}
          variant="outlined"
          name="addressUUID"
        />

        {errorInfo.addressUUID === true ? (
          <div id="nameid" className="addfieldserror">
            "Allowed alphabets,no special characters and Numerics are
            allowed.Maximum length is 25."
          </div>
        ) : (
          ''
        )}

        <FormControl className={classes.formControl}>
          <InputLabel id="demo-simple-select-label">UUID</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={ContactTypeUUID}
            onChange={handleChange}
          >
            <MenuItem value={10}>CNP</MenuItem>
            <MenuItem value={20}>TMS</MenuItem>
            <MenuItem value={30}>Cloudgen</MenuItem>
          </Select>
        </FormControl>
        <Button
          variant="contained"
          fullWidth="true"
          className="createpartnerbutton"
        >
          submit
        </Button>
      </form>
    </div>
  );
}
