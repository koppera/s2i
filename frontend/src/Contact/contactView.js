import React, { useEffect, useContext, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import { useMutation, useQuery } from 'graphql-hooks';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { Grid, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { withStyles } from '@material-ui/core/styles';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import '../css/commonStyle.css';
import Theme from '../css/theme';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
const DISPLAY_ADDRESS = `query fetchAddressByUUID($addressUUID:String,$contactUUID:String){
  fetchAddressByUUID(addressUUID:$addressUUID,contactUUID:$contactUUID) {
    idAddress
    addressUUID
    addressNameID
    addressLines1
    addressLines2
    addressLines3
    city
    state
    country
    zip
    addressStatus
    addressTypeUUID
    partnerProfileUUID
    createdOn
    lastUpdatedOn
    contactUUID
  } 
   
}
`;
const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(2),
    },
  },

  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));
const styles = (theme) => ({
  root: {
    Width: 1200,
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: '#FFFFFF',
  },
});
const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});
export default function ContactView(props) {
  console.log(props);
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);
  const [addressData, setData] = React.useState([]);
  const handleClickOpen = () => {
    setOpen(true);
  };
  const { loading, error, data, refetch } = useQuery(DISPLAY_ADDRESS, {
    variables: {
      addressUUID: props.rowData.addressUUID,
      contactUUID: props.rowData.contactUUID,
    },
  });
  const handleClose = () => {
    setOpen(false);
  };
  console.log(data, addressData);
  useEffect(() => {
    if (data !== undefined && data.fetchAddressByUUID !== undefined) {
      console.log(data.fetchAddressByUUID);
      setData(data.fetchAddressByUUID[0]);
    }
  }, [data]);
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSvgIcon: {
          root: {
            color: '#FFFFFF',
          },
        },
        MuiGrid: {
          container: {},
          item: {
            fontSize: '10px',
          },
        },
        MuiDialogTitle: {
          root: {
            backgroundColor: '#0b153e',
            color: '#FFFFFF',
          },
        },
      },
    });
  const handleModalClose = () => {
    props.handleCloseModal(false);
  };
  return (
    <div>
      <MuiThemeProvider theme={getMuiTheme()}>
        <Grid container className="header">
          <Grid item xs={10} sm={10}>
            <h2 className="h2">Contact</h2>
          </Grid>
          <Grid item xs={2} sm={2} className="close">
            <CloseIcon onClick={handleModalClose} />
          </Grid>
        </Grid>
        <Grid container className="pdashboardroot">
          <Grid container spacing={3} className="contactcontent">
            <Grid
              item
              xs={12}
              sm={3}
              variant="body2"
              color="textSecondary"
              className="text"
            >
              <Typography className="typographysubheader"> Role </Typography>
              {props.rowData.role}
            </Grid>
            <Grid
              item
              xs={12}
              sm={3}
              variant="body2"
              color="textSecondary"
              className="text"
            >
              <Typography className="typographysubheader">
                First Name{' '}
              </Typography>
              {props.rowData.firstName}
            </Grid>
            <Grid
              item
              xs={12}
              sm={3}
              variant="body2"
              color="textSecondary"
              className="text"
            >
              <Typography className="typographysubheader">
                Last Name{' '}
              </Typography>
              {props.rowData.lastName}
            </Grid>
            <Grid
              item
              xs={12}
              sm={3}
              gutterBottom
              variant="body2"
              color="textSecondary"
              className="text"
            >
              <Typography className="typographysubheader">Email </Typography>
              {props.rowData.email}
            </Grid>

            <Grid
              item
              xs={12}
              sm={3}
              gutterBottom
              variant="body2"
              color="textSecondary"
              className="text"
            >
              <Typography className="typographysubheader">Phone </Typography>
              {props.rowData.phone}
            </Grid>
            <Grid
              item
              xs={12}
              sm={3}
              gutterBottom
              variant="body2"
              color="textSecondary"
              className="text"
            >
              <Typography className="typographysubheader">Fax</Typography>
              {props.rowData.fax}
            </Grid>
            <Grid
              item
              xs={12}
              sm={3}
              gutterBottom
              variant="body2"
              color="textSecondary"
              className="text"
            >
              <Typography className="typographysubheader">
                Address Name
              </Typography>
              {addressData != undefined ? addressData.addressNameID : ''}
            </Grid>
            <Grid
              item
              xs={12}
              sm={3}
              gutterBottom
              variant="body2"
              color="textSecondary"
              className="text"
            >
              <Typography className="typographysubheader">
                Address Line 1
              </Typography>
              {addressData != undefined ? addressData.addressLines1 : ''}
            </Grid>

            <Grid
              item
              xs={12}
              sm={3}
              gutterBottom
              variant="body2"
              color="textSecondary"
              className="text"
            >
              <Typography className="typographysubheader">
                Address Line 2
              </Typography>
              {addressData != undefined ? addressData.addressLines2 : ''}
            </Grid>
            <Grid
              item
              xs={12}
              sm={3}
              gutterBottom
              variant="body2"
              color="textSecondary"
              className="text"
            >
              <Typography className="typographysubheader">
                Address Line 3
              </Typography>
              {addressData != undefined ? addressData.addressLines3 : ''}
            </Grid>
            <Grid
              item
              xs={12}
              sm={3}
              gutterBottom
              variant="body2"
              color="textSecondary"
              className="text"
            >
              <Typography className="typographysubheader">City </Typography>
              {addressData != undefined ? addressData.city : ''}
            </Grid>
            <Grid
              item
              xs={12}
              sm={3}
              gutterBottom
              variant="body2"
              color="textSecondary"
              className="text"
            >
              <Typography className="typographysubheader">State </Typography>
              {addressData != undefined ? addressData.state : ''}
            </Grid>

            <Grid
              item
              xs={12}
              sm={3}
              gutterBottom
              variant="body2"
              color="textSecondary"
              className="text"
            >
              <Typography className="typographysubheader">Country </Typography>
              {addressData != undefined ? addressData.country : ''}
            </Grid>
            <Grid
              item
              xs={12}
              sm={3}
              gutterBottom
              variant="body2"
              color="textSecondary"
              className="text"
            >
              <Typography className="typographysubheader">Zip </Typography>
              {addressData != undefined ? addressData.zip : ''}
            </Grid>
            <Grid
              item
              xs={12}
              sm={3}
              gutterBottom
              variant="body2"
              color="textSecondary"
              className="text"
            >
              <Typography className="typographysubheader">
                Address Status
              </Typography>
              {addressData != undefined ? addressData.addressStatus : ''}
            </Grid>
          </Grid>
        </Grid>
      </MuiThemeProvider>
    </div>
  );
}
