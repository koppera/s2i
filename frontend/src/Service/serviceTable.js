import MaterialTable from 'material-table';
import { useMutation, useQuery } from 'graphql-hooks';
import { forwardRef, useEffect } from 'react';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import Loader from '../components/loader';
import ViewColumn from '@material-ui/icons/ViewColumn';
import ReactDOM from 'react-dom';
import MTableToolbar from 'material-table';
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Theme from '../css/theme';
import {TextField } from "@material-ui/core";
import Timestamp from '../timestamp';
import Error from '../components/emptyPage';
import Snackbar from '@material-ui/core/Snackbar';
import MySnackbarContentWrapper from '../components/Snackbar';

import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles
} from '@material-ui/core/styles';
const DISPLAY_All_SERVICE = `query fetchSERVICE($searchInput:String){
  fetchService(searchInput:$searchInput) {
    idService
    serviceUUID
    name
    description
    createdOn
    lastUpdatedOn
  } 
}
`;
const ADD_SERVICE = `mutation createService($input:ServiceInput){
  createService(input:$input) {
    idService

    serviceUUID
    name
    description
    createdOn
    lastUpdatedOn  } 
}`;
const UPDATE_SERVICE = `mutation updateService($input:ServiceInput){
  updateService(input:$input) {
    idService
    serviceUUID
    name
    description
    createdOn
    lastUpdatedOn
  }
}`;
const REMOVE_SERVICE = `mutation removeSERVICE($input:ServiceInput){
  removeService(input:$input) {
    idService
    serviceUUID
    name
    description
    createdOn
    lastUpdatedOn
  } 
}
`;
function iconStyles() {
  return {};
}
export default function ServiceTable() {
  const [searchInput, setSearchValue] = React.useState('');
  //Fetch ALL Service
  const { loading, error, data, refetch } = useQuery(DISPLAY_All_SERVICE, {
    variables: {
      searchInput: searchInput
    }
  });
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [message, setSnackbarMessage] = React.useState();
  const [variant, setSnackbarVariant] = React.useState('error');
  const [nameError, setNameError] = React.useState({
    error: false,
    label: "",
    helperText: "",
    validateInput: false,
});
const nameexp =( /^[a-zA-Z0-9_\s]+$/);
  //ADD Service
  const [addService, addloading, adddata] = useMutation(ADD_SERVICE);
  const [Servicedata, setData] = React.useState([]);
  const [serverError, setServerError] = React.useState(false);
  //EDIT Service
  const [updateService, updateloading, updatedata] = useMutation(
    UPDATE_SERVICE
  );
  //remove Service
  const [removeService, removeloading, removedata] = useMutation(
    REMOVE_SERVICE
  );
  const columns = [
    { title: 'Name', field: 'name' , editComponent: (props) => {
      console.log('editComponent', props, props.value);
      return (
        <div>
           <TextField
             placeholder="Name"
             margin="normal"
             fullWidth
             getOptionLabel={(option) => option.name}
             value={props.value}
           
            error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                    ? nameError.error
                    : false
            }
            helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                    ? nameError.helperText
                    : ""
            }
            onChange={(v) => props.onChange(v.target.value)}

              
        />
    
            <div style={{ color: 'red' }}>
              {!props.rowData.name||
              nameexp.test(props.rowData.name)
                ? ''
                : 'Special characters are not allowed except [ _ ]'}
            </div>
          </div>
        );
      },
        
    render: (rowData) => {
      return <div>{rowData.name}</div>;
    },
  }, 
    { title: 'Description', field: 'description', editComponent: (props) => {
      console.log('editComponent', props, props.value);
      return (
        <div>
          <TextField
            /*variant="Standard"*/
           /* label="addresstypeName"*/
            placeholder="Description"
            margin="normal"
            fullWidth
            getOptionLabel={(option) => option.name}
            value={props.value}
            onChange={(v) => props.onChange(v.target.value)}
      
          />
        </div>
      );
    },
    render: (rowData) => {
      return <div>{rowData.description}</div>;
    },
  },
  { title: 'Created',
  field: 'createdOn',
  editable: 'never',
 render : (rowData) => {
   console.log(rowData)
   if(rowData!= undefined){
     return Timestamp(rowData.createdOn)
   }else{
     return 
   }
 } }, 
 {
  title: 'Last Modified',
  field: 'lastUpdatedOn',
  editable: 'never',
  render: (rowData) => {
    console.log(rowData);
    if (rowData != undefined) {
      return Timestamp(rowData.lastUpdatedOn);
    } else {
      return;
    }
  },
},
  ];

  useEffect(() => {
    console.log();
    if (data !== undefined) {
      console.log(data.fetchService);
      setData(data.fetchService);
      setServerError(false);
    }
    else {
      console.log('server error');
      setServerError(true);
    }
  }, [data]);
  const handleRefetch = (value, message) => {
    refetch({
      updateData(_, data) {
        return data;
      }
    });
    setOpenSnackbar(true);
    setSnackbarMessage(message);
    if (value) {
      setSnackbarVariant('error');
    } else {
      setSnackbarVariant('success');
    }
  };
  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }

  const classes = makeStyles(iconStyles)();
  const tableIcons = {
    Add: forwardRef((props, ref) => (
      <AddBox color="primary" {...props} ref={ref} />
    )),
    Check: forwardRef((props, ref) => (
      <Check color="primary" {...props} ref={ref} />
    )),
    Clear: forwardRef((props, ref) => (
      <Clear color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline   color="error" {...props} ref={ref} />
    )),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => (
      <Edit
      style={{marginLeft:"40%",marginRight:"60%"}}
        {...props}
        ref={ref}
        color="primary"
        onClick={console.log('hello world')}
      />
    )),
    Export: forwardRef((props, ref) => (
      <SaveAlt color="primary" {...props} ref={ref} />
    )),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => (
      <FirstPage color="primary" {...props} ref={ref} />
    )),
    LastPage: forwardRef((props, ref) => (
      <LastPage color="primary" {...props} ref={ref} />
    )),
    NextPage: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft color="primary" {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => (
      <Clear  {...props} ref={ref} />
    )),
    Search: forwardRef((props, ref) => (
      <Search color="primary" {...props} ref={ref} />
    )),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };

  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        
        MuiSelect: {
          icon:{
            color:'#0b153e'
              }
        },
      
        MuiTypography:{h6:{
          fontSize:14,
          fontFamily:"Arial !important"      
          }},

      }
    });
  const handleSearchValue = val => {
    console.log(val);
    setSearchValue(val);
  };
  async function handleAddService(newData) {
    return await new Promise(async(resolve) => {
      resolve();
      console.log(newData);
      await addService({
        variables: {
          input: {
            name: newData.name,
            description: newData.description
          }
        }
      }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
        console.log(data, error, graphQLErrors, networkError, cacheHit);
       if (data !== null && error == false) {
         handleRefetch(error, 'Saved successfully');
       } 
       else if (error && graphQLErrors.length > 0)
         {
          let duperror = graphQLErrors[0].message;
          console.log(duperror);
          if (
            duperror !== null &&
            duperror.indexOf('ER_DUP_ENTRY') !== -1
          ) {
            handleRefetch(error, 'Name already existed');
          }
        }
     })
     .catch((e) => {
       // you can do something with the error here
       console.log(e);
     });
    });
  }
  async function handleRemoveService(oldData) {
    return await new Promise(async(resolve) => {
      resolve();
      await removeService({
        variables: {
          input: {
            serviceUUID: oldData.serviceUUID
          }
        }
      }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
        console.log(data, error, graphQLErrors, networkError, cacheHit);
        if (data !== null && error == false) {
          handleRefetch(error, 'Deleted successfully');
        } else if (error && graphQLErrors.length > 0) {
          let duperror = graphQLErrors[0].message;
          console.log(duperror);
          if (
            duperror !== null &&
            duperror.indexOf('ER_ROW_IS_REFERENCED') !== -1
          ) {
            setOpenSnackbar(true);
            setSnackbarVariant('error');
            setSnackbarMessage('Selected row is referenced in Organization interface table');
          }
        }
      })
      .catch((e) => {
        // you can do something with the error here
        console.log(e);
      });
    });
  }
  async function handleUpdateService(newData, oldData) {
    return await new Promise(async(resolve) => {
      resolve();
      console.log(newData);
      await  updateService({
        variables: {
          input: {
            name: newData.name,
            description: newData.description,
            serviceUUID: oldData.serviceUUID
          }
        }
      }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
        console.log(data, error, graphQLErrors, networkError, cacheHit);
       if (data !== null && error == false) {
         handleRefetch(error, 'Updated successfully');
       } 
       else if (error && graphQLErrors.length > 0)
         {
          let duperror = graphQLErrors[0].message;
          console.log(duperror);
          if (
            duperror !== null &&
            duperror.indexOf('ER_DUP_ENTRY') !== -1
          ) {
            handleRefetch(error, 'Name already existed');
          }
        }
     })
     .catch((e) => {
       // you can do something with the error here
       console.log(e);
     });
    });
  }
  if (loading) return <Loader />;
  if (serverError)
    return <Error type={'Server connection lost.Please try again'} />;

  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <MaterialTable
        icons={tableIcons}
        title="Service"
        columns={columns}
        data={Servicedata}
       //onSearchChange={e => handleSearchValue(e)}
        editable={{
        
          onRowAdd: (newData) =>
          new Promise((resolve, reject) => {
         setTimeout(() => {
             newData.submitted = true;
             if (!newData.name||
              !nameexp.test(newData.name)
              ) {
                 setNameError({
                     error: true,
                     label: "required",
                     helperText: "Required.",
                     validateInput: true,
                 });
                 reject();
                 return;
             }
             resolve();
             handleAddService(newData);          
         }, 600);
     }),
        
       //   onRowAdd: newData => handleAddService(newData),
          onRowUpdate: (newData, oldData) =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
                newData.submitted = true;
                if (!newData.name||
                 !nameexp.test(newData.name)
                 ) {
                    setNameError({
                        error: true,
                        label: "required",
                        helperText: "Required.",
                        validateInput: true,
                    });
                    reject();
                    return;
                }
                resolve();
                handleUpdateService(newData, oldData);
            }, 600);
        }), 
          onRowDelete: oldData => handleRemoveService(oldData)
        }}
        options={{
          headerStyle: {
          
          textAlign: 'center',
          fontSize:12,
          fontWeight:'bold',
          fontFamily:'Arial !important',
          backgroundColor:"#0b153e",
          color:"#ffffff",
          padding:'4px',
         
          },
         
          searchFieldStyle: {
            color: '#0b153e'
          },
          actionsColumnIndex: -1,
          pageSize:10,
          pageSizeOptions: [10, 25 ,50,100 ],
          toolbar: true,
          paging: true
        }}
      />
       <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        autoHideDuration={1500}
        open={openSnackbar}
        onClose={handleCloseSnackbar}
      >
        <MySnackbarContentWrapper
          onClose={handleCloseSnackbar}
          variant={variant}
          message={message}
        />
      </Snackbar>
    </MuiThemeProvider>
  );
}
