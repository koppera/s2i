import MaterialTable from 'material-table';
import { useMutation, useQuery } from 'graphql-hooks';
import { forwardRef, useEffect } from 'react';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import ReactDOM from 'react-dom';
import MTableToolbar from 'material-table';
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Timestamp from '../timestamp';
import Theme from '../css/theme';
import { TextField } from '@material-ui/core';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import Error from '../components/emptyPage';
import CircularIndeterminateLoader from '../components/loader';

import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles,
} from '@material-ui/core/styles';

const DISPLAY_All_PROTOCOLTYPE = `{
  allProtocolTypes {
    idFileProtocolType
    protocolTypeUUID
    protocolTypeName
    protocolTypeDescription
    createdOn
    lastUpdatedOn
  }

}
`;
const ADD_PROTOCOLTYPE = `mutation createProtocolTypes($inputCreate:ProtocolTypesInput){
  createProtocolTypes(input:$inputCreate) {
    idFileProtocolType
    protocolTypeUUID
    protocolTypeName
    protocolTypeDescription
    createdOn
    lastUpdatedOn
  } 

}
`;
const REMOVE_PROTOCOLTYPE = `mutation removeProtocolTypes($input:ProtocolTypesInput){
  removeProtocolTypes(input:$input) {
    idFileProtocolType
    protocolTypeUUID
    protocolTypeName
    protocolTypeDescription
    createdOn
    lastUpdatedOn
  }

}
`;
const UPDATE_PROTOCOLTYPE = `mutation updateProtocolTypes($input:ProtocolTypesInput){
  updateProtocolTypes(input:$input) {
      idFileProtocolType
      protocolTypeUUID
      protocolTypeName
      protocolTypeDescription
      createdOn
      lastUpdatedOn
    }
  
  }
`;
function iconStyles() {
  return {};
}
export default function ProtocolType() {
  //ADD _PROTOCOLTYPE
  const [addProtocolTypes, addloading, adddata] = useMutation(ADD_PROTOCOLTYPE);
  //FETCH ALL _PROTOCOLTYPE
  const { loading, error, data, refetch } = useQuery(DISPLAY_All_PROTOCOLTYPE);

  //remove_PROTOCOLTYPE
  const [removeProtocolTypes, removeloading, removedata] = useMutation(
    REMOVE_PROTOCOLTYPE
  );
  //update _PROTOCOLTYPE
  const [updateProtocolTypes, updateloading, updatedata] = useMutation(
    UPDATE_PROTOCOLTYPE
  );
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [message, setSnackbarMessage] = React.useState();
  const [serverError, setServerError] = React.useState(false);
  const [variant, setSnackbarVariant] = React.useState('error');
  const [nameError, setNameError] = React.useState({
    error: false,
    label: "",
    helperText: "",
    validateInput: false,
});
const nameexp =( /^[a-zA-Z\s]+$/);
  const [protocolTypedata, setData] = React.useState([]);
  const columns = [
    {
      title: 'protocolTypeUUID',
      field: 'protocolTypeUUID',
      editable: 'never',
      hidden: true,
    },
    {
      title: 'Name',
      field: 'protocolTypeName',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);

        return (
          <div>
             <TextField
             placeholder="Name"
             margin="normal"
             fullWidth
             getOptionLabel={(option) => option.name}
             value={props.value}
           
            error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                    ? nameError.error
                    : false
            }
            helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                    ? nameError.helperText
                    : ""
            }
            onChange={(v) => props.onChange(v.target.value)}
        />
           

           <div style={{ color: 'red' }}>
              {!props.rowData.protocolTypeName||
              nameexp.test(props.rowData.protocolTypeName)
                ? ''
                : 'Enter only alphabets'}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.protocolTypeName}</div>;
      },
    },
    {
      title: 'Description',
      field: 'protocolTypeDescription',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder="Description"
              margin="normal"
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
              value={props.value}
            />
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.protocolTypeDescription}</div>;
      },
    },
    {
      title: 'Created',
      field: 'createdOn',
      editable: 'never',
      render: (rowData) => {
        console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.createdOn);
        } else {
          return;
        }
      },
    },
    {
      title: 'Last Modified',
      field: 'lastUpdatedOn',
      editable: 'never',
      render: (rowData) => {
        console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.lastUpdatedOn);
        } else {
          return;
        }
      },
    },
  ];
  useEffect(() => {
    if (data !== undefined) {
      console.log(data.allProtocolTypes);
      setData(data.allProtocolTypes);
      setServerError(false);
    }
    else {
      console.log('server error');
      setServerError(true);
    }
  }, [data]);
  const handleRefetch = (value, message) => {
    refetch({
      updateData(_, data) {
        return data;
      },
    });
    setOpenSnackbar(true);
    setSnackbarMessage(message);
    if (value) {
      setSnackbarVariant('error');
    } else {
      setSnackbarVariant('success');
    }
  };
  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }
  const classes = makeStyles(iconStyles)();
  const tableIcons = {
    Add: forwardRef((props, ref) => (
      <AddBox color="primary" {...props} ref={ref} />
    )),
    Check: forwardRef((props, ref) => (
      <Check color="primary" {...props} ref={ref} />
    )),
    Clear: forwardRef((props, ref) => (
      <Clear color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline style={{marginLeft:"140%"}} color="error" {...props} ref={ref} />
    )),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => (
      <Edit
      style={{marginLeft:"60%"}}
        {...props}
        ref={ref}
        color="primary"
        onClick={console.log('hello world')}
      />
    )),
    Export: forwardRef((props, ref) => (
      <SaveAlt color="primary" {...props} ref={ref} />
    )),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => (
      <FirstPage color="primary" {...props} ref={ref} />
    )),
    LastPage: forwardRef((props, ref) => (
      <LastPage color="primary" {...props} ref={ref} />
    )),
    NextPage: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft color="primary" {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => (
      <Clear  {...props} ref={ref} />
    )),
    Search: forwardRef((props, ref) => (
      <Search color="primary" {...props} ref={ref} />
    )),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };

  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiTypography: {
          h6: {
            fontSize: 14,
            fontFamily: 'Arial !important',
          },
        },
        MuiSelect: {
          icon:{
            color:'#0b153e'
              }
        },
      },
    });
    async function handleAddProtocolType(newData) {
    return await new Promise(async(resolve) => {
      resolve();
      console.log(newData);
      await addProtocolTypes({
        variables: {
          inputCreate: {
            protocolTypeName: newData.protocolTypeName,
            protocolTypeDescription: newData.protocolTypeDescription,
          },
        },
      }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
        console.log(data, error, graphQLErrors, networkError, cacheHit);
       if (data !== null && error == false) {
         handleRefetch(error, 'Saved successfully');
       } 
       else if (error && graphQLErrors.length > 0)
         {
          let duperror = graphQLErrors[0].message;
          console.log(duperror);
          if (
            duperror !== null &&
            duperror.indexOf('ER_DUP_ENTRY') !== -1
          ) {
            handleRefetch(error, 'Name already existed');
          }
        }
     })
     .catch((e) => {
       // you can do something with the error here
       console.log(e);
     });
    });
  }
  async function handleUpdateProtocolType(newData, oldData) {
    return await new Promise(async(resolve) => {
      resolve();
      console.log(newData);
      await updateProtocolTypes({
        variables: {
          input: {
            protocolTypeName: newData.protocolTypeName,
            protocolTypeDescription: newData.protocolTypeDescription,
            protocolTypeUUID: newData.protocolTypeUUID,
          },
        },
      }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
        console.log(data, error, graphQLErrors, networkError, cacheHit);
       if (data !== null && error == false) {
         handleRefetch(error, 'Updated successfully');
       } 
       else if (error && graphQLErrors.length > 0)
         {
          let duperror = graphQLErrors[0].message;
          console.log(duperror);
          if (
            duperror !== null &&
            duperror.indexOf('ER_DUP_ENTRY') !== -1
          ) {
            handleRefetch(error, 'Name already existed');
          }
        }
     })
     .catch((e) => {
       // you can do something with the error here
       console.log(e);
     });
    });
  }
  async function handleRemoveProtocolType(oldData) {
    return await new Promise(async(resolve) => {
      resolve();
      await  removeProtocolTypes({
        variables: {
          input: {
            protocolTypeUUID: oldData.protocolTypeUUID,
          },
        },
      }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
        console.log(data, error, graphQLErrors, networkError, cacheHit);
        if (data !== null && error == false) {
          handleRefetch(error, 'Deleted successfully');
        } else if (error && graphQLErrors.length > 0) {
          let duperror = graphQLErrors[0].message;
          console.log(duperror);
          if (
            duperror !== null &&
            duperror.indexOf('ER_ROW_IS_REFERENCED') !== -1
          ) {
            setOpenSnackbar(true);
            setSnackbarVariant('error');
            setSnackbarMessage('Selected row is referenced in Partner Profile');
          }
        }
      })
      .catch((e) => {
        // you can do something with the error here
        console.log(e);
      });
    });
  }
  if (loading) return <CircularIndeterminateLoader />;
  if (serverError)
  return <Error type={'Server connection lost.Please try again'} />;

  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <MaterialTable
        icons={tableIcons}
        title="Protocol Type"
        columns={columns}
        data={protocolTypedata}
          editable={{
            onRowAdd: (newData) =>
            new Promise((resolve, reject) => {
           setTimeout(() => {
               newData.submitted = true;
               if (!newData.protocolTypeName||
                !nameexp.test(newData.protocolTypeName)
                ) {
                   setNameError({
                       error: true,
                       label: "required",
                       helperText: "Required.",
                       validateInput: true,
                   });
                   reject();
                   return;
               }
               resolve();
             
               handleAddProtocolType(newData)
              
            
           }, 600);
       }),
          onRowUpdate: (newData, oldData) =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
                newData.submitted = true;
                if (!newData.protocolTypeName||
                 !nameexp.test(newData.protocolTypeName)
                 ) {
                    setNameError({
                        error: true,
                        label: "required",
                        helperText: "Required.",
                        validateInput: true,
                    });
                    reject();
                    return;
                }
                resolve();
                handleUpdateProtocolType(newData, oldData)
            }, 600);
        }),
          onRowDelete: (oldData) => handleRemoveProtocolType(oldData),
        }}
        options={{
          headerStyle: {
            textAlign: 'center',
            fontSize: 12,
            fontWeight: 'bold',
            fontFamily: 'Arial !important',
            backgroundColor: '#0b153e',
            color: '#ffffff',
            padding: '4px',
          },

          searchFieldStyle: {
            color: '#0b153e',
          },
          actionsColumnIndex: -1,
          pageSize:10,
          pageSizeOptions: [10, 25 ,50,100 ],
          toolbar: true,
          paging: true
        }}
      />
       <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        autoHideDuration={1500}
        open={openSnackbar}
        onClose={handleCloseSnackbar}
      >
        <MySnackbarContentWrapper
          onClose={handleCloseSnackbar}
          variant={variant}
          message={message}
        />
      </Snackbar>
    </MuiThemeProvider>
  );
}
