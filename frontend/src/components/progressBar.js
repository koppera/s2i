import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import { Grid, Typography } from '@material-ui/core';
import Modal from '@material-ui/core/Modal';
import '../css/commonStyle.css';
function LinearProgressWithLabel(props) {
  // console.log(props);
  return (
    <div>
      <Modal open={props.isModalOpen} className="fabmodal">
        <div className="Linearprogress">
          <Grid container spacing={3}>
            <Grid items xs={12} sm={11}>
              <LinearProgress
                style={{ backgroundColor: '#ffffff' }}
                variant="determinate"
                {...props}
              />
            </Grid>
            <Grid items xs={12} sm={1}>
              <Typography
                variant="body2"
                color="h2"
                style={{ color: '#ffffff', fontSize: '15px', paddingBottom: 6, paddingLeft:'6px' }}
              >{`${Math.round(props.value)}%`}</Typography>
            </Grid>
          </Grid>
          <Grid container spacing={1}>
            <Grid item xs={12} sm={3}>
              <Typography style={{ color: '#ffffff' }}>
                {props.value == 25 ||
                props.value == 26 ||
                props.value == 27 ||
                props.value == 28 ||
                props.value == 29 ||
                props.value == 30
                ? 'Conversion of EDI file to XML' : ''}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={3}>
              <Typography style={{ color: '#ffffff' }}>
                {props.value == 50 ||
                props.value == 51 ||
                props.value == 52 ||
                props.value == 53 ||
                props.value == 54 ||
                props.value == 55 

                ? 'XML Mapped' : ''}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={3}>
              <Typography style={{ color: '#ffffff' }}>
                {props.value == 75 ||
                props.value == 76 ||
                props.value == 77 ||
                props.value == 78 ||
                props.value == 79 ||
                props.value == 80 
                ? 'XML converted to JSON' : ''}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={3}>
              <Typography style={{ color: '#ffffff' }}>
                {props.value == 95 ||
                props.value == 96 ||
                props.value == 97 ||
                props.value == 98 ||
                props.value == 99 ||
                props.value == 100

                  ? 'JSON trasaction pushed to ElasticSearch'
                  : ''}
              </Typography>
            </Grid>
          </Grid>
        </div>
      </Modal>
    </div>
  );
}

LinearProgressWithLabel.propTypes = {
  /**
   * The value of the progress indicator for the determinate and buffer variants.
   * Value between 0 and 100.
   */
  value: PropTypes.number.isRequired,
};

const useStyles = makeStyles({
  root: {
    padding: 6,
  },
});

export default function LinearWithValueLabel(props) {
  const classes = useStyles();
  const [progress, setProgress] = React.useState(
    props.value == undefined ? 0 : props.value
  );

  React.useEffect(() => {
    console.log(progress, props.time);
    if (props.value != undefined) {
      setProgress(props.value);
    }
    if (progress == 0) {
      clearInterval(0);
    } else {
      const timer = setInterval(() => {
        setProgress((prevProgress) =>
          prevProgress >= props.time ? props.time : prevProgress + 1
        );
      }, 300);
      return () => {
        clearInterval(timer);
      };
    }
  }, [props.value, props.time]);
  if (progress == 100) {
    setProgress(0);
    clearInterval(0);
    props.successProgressLoader(100);
  }
  if (props.msg !== null) {
    // console.log(props.msg);
    props.maxTimeLoader();
  }
  return (
    <LinearProgressWithLabel
      value={progress}
      labelText={props.labelText !== undefined ? props.labelText : ''}
      isModalOpen={props.isModalOpen}
      className={classes.root}
    />
  );
}
