import React, { useEffect, useContext, useCallback } from 'react';
import Iframe from 'react-iframe';
export default function KibanaDashboard(props) {
  console.log(props);
  
  return (
    <iframe
      src={props.url}
      //src={process.env.REACT_APP_KIBANA+"app/kibana#/dashboard/1d692930-9396-11ea-bcb0-ad0109361958?embed=true&_g=(refreshInterval%3A(display%3AOff%2Cpause%3A!f%2Cvalue%3A0)%2Ctime%3A(from%3Anow-6M%2Cmode%3Aquick%2Cto%3Anow))"}
      allowtransparency="true"
      frameBorder="0"
      width="100%"
      height="720"
      allowFullScreen="true"
      webkitallowfullscreen="true"
      mozallowfullscreen="true"
    ></iframe>
  );
}
