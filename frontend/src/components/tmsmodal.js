import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';

const useStyles = makeStyles((theme) => ({}));

export default function SimpleModal(props) {
  const classes = useStyles();

  let handleClose = () => {
    props.handleCloseModal(false);
  };

  return (
    <div>
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={props.isModalOpen}
        className="fabmodal"
        onClose={handleClose}
      >
        <div className="modalpaper">{props.children}</div>
      </Modal>
    </div>
  );
}
