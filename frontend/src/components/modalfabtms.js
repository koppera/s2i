import React from 'react';
import Fab from '@material-ui/core/Fab';
import { makeStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import Modal from '@material-ui/core/Modal';
import CloseIcon from '@material-ui/icons/Close';
import Grid from '@material-ui/core/Grid';
import Tooltip from '@material-ui/core/Tooltip';
import '../css/commonStyle.css';

function rand() {
  return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles(theme => ({  
     
}));

export default function ModalFab(props) {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
  
    const handleOpen = () => {
        setOpen(true);
    };

    let handleClose = () => {
        setOpen(false);
    } 
       
        return (  
                <div>  
                  
                  <Fab  onClick={handleOpen} aria-label="Add-Icon" className="fab" color="primary">
                        <AddIcon  />   
                    </Fab>

                        <Modal
                            aria-labelledby="simple-modal-title"
                            aria-describedby="simple-modal-description"
                            open={open}
                            onClose={handleClose}
                            className="fabmodal"
                        >
                            <div  className="modalpaper">
                        
                                <Grid container className="header">          
                                      <Grid item xs={10} sm={10}> 
        <h2 id="modal-title" className="h2"> {props.title}</h2>                      
                                        </Grid>
                                        <Grid item xs={2} sm={2} className="close">                              
                                        <CloseIcon onClick={handleClose}/>
                                        </Grid>
                                </Grid>         
                               
                                {props.children}
                            </div>
                        </Modal>                 
                </div>                       
        );
}  