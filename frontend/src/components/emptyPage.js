import React from "react";
import { Link } from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Paper from '@material-ui/core/Paper';
const EmptyPage = props => (
    <Paper className="emptypage">
      <Typography variant="h5" component="h3">
      <center style={{ marginTop: "10%",marginBottom:"10%" }}>
      <Typography className="errorpage"
        variant="subtitle1"
        gutterBottom
      >
        {props.type}
      </Typography>
    </center>
      </Typography>
    </Paper>
);

export default EmptyPage;
