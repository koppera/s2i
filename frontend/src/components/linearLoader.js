import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import { Grid, Typography } from '@material-ui/core';
import Modal from '@material-ui/core/Modal';
import '../css/commonStyle.css';
function LinearProgressWithLabel(props) {
  // console.log(props);
  return (
    <div>
      <Modal open={props.isModalOpen} className="fabmodal"> 
        <div className="LinearLoader">
          <Grid container={3} style={{ alignItems: 'center' }}>
            <Grid items xs={12} sm={11}>
              <LinearProgress style={{ height: '15px',backgroundColor:'#ffffff'}} />
            </Grid>
          </Grid>
          <Grid items xs={12} sm={12}>
            <Typography
              style={{
                color: '#ffffff',
                fontSize: '25px',
                paddingLeft: '200px',
                paddingTop: '10px',
                fontWeight: '400',
              }}
            >
              Please wait.... We are processing your request!
            </Typography>
          </Grid>
        </div>
      </Modal> 
    </div>
  );
}

LinearProgressWithLabel.propTypes = {
  /**
   * The value of the progress indicator for the determinate and buffer variants.
   * Value between 0 and 100.
   */
  value: PropTypes.number.isRequired,
};

const useStyles = makeStyles({
  root: {
    padding: 16,
    height: '5px',
  },
});

export default function LinearWithValueLabel(props) {
  const classes = useStyles();

  return (
    <LinearProgressWithLabel
      isModalOpen={props.isModalOpen}
      className={classes.root}
    />
  );
}


