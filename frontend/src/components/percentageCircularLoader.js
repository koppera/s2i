import React from 'react';
import PropTypes from 'prop-types';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

function CircularProgressWithLabel(props) {
  return (
    <Box position="relative" display="inline-flex">
      <CircularProgress
        variant="static"
        style={{ width: '70px' }}
        id="Circularloading-content"
        {...props}
      />
      <Box
        top={0}
        left={0}
        bottom={0}
        right={0}
        position="center"
        display="flex"
        alignItems="center"
        justifyContent="center"
      >
        <Typography
          variant="caption"
          style={{ width: '100px', paddingTop: '10px' }}
          id="Circularloading-content"
          color="textSecondary"
        >{`${Math.round(props.value)}%`}</Typography>
      </Box>
    </Box>
  );
}
CircularProgressWithLabel.propTypes = {
  /**
   * The value of the progress indicator for the determinate and static variants.
   * Value between 0 and 100.
   */
  value: PropTypes.number.isRequired,
};

export default function CircularStatic(props) {
  const [progress, setProgress] = React.useState(
    props.value == undefined ? 1 : props.value
  );

  React.useEffect(() => {
    const timer = setInterval(() => {
      setProgress((prevProgress) =>
        prevProgress >= props.time
          ? props.time
          : (console.log(props.time, 'progress value'), prevProgress + 1)
      );
    }, 0);
  }, []);
  if (progress == 85 && props.msg !== null) {
    console.log(props.msg);
    props.maxTimeLoader();
  }
  return <CircularProgressWithLabel value={progress} />;
}
