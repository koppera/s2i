import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import '../css/commonStyle.css';

const useStyles = makeStyles(theme => ({
  
}));
export default function CircularIndeterminateLoader(props) {
  const classes = useStyles();
if(props.type==undefined)
return (
    <div id="cover-spin">
       <div id="loading-content">
          Loading.....
        </div>
    </div>
)
else
  return (
    <div id="cover-spin">
       <div id="loading-content">
       {props.type}
        </div>
    </div>
)
}
