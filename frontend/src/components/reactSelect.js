import React, { useEffect} from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import Select from 'react-select';
import { emphasize, makeStyles, useTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import NoSsr from '@material-ui/core/NoSsr';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Chip from '@material-ui/core/Chip';
import MenuItem from '@material-ui/core/MenuItem';
import CancelIcon from '@material-ui/icons/Cancel';
import Tooltip from '@material-ui/core/Tooltip';
import   "../css/commonStyle.css";
import { Grid} from '@material-ui/core';


const suggestions = [
  'ALL',
  'Read',
  'Write',
  'Remove',
].map(suggestion => ({
  value: suggestion,
  label: suggestion,
}))

const useStyles = makeStyles(theme => ({
  chip: {
    margin: theme.spacing(0.5, 0.25),
    height:"fit-content",
    wordBreak:"break-word",
    '& span':{
      whiteSpace:"normal",
    },
    '@media (max-width: 600px)':{
        width:140,
    },
  },
  chipFocused: {
    backgroundColor: emphasize(
      theme.palette.type === 'light' ? theme.palette.grey[300] : theme.palette.grey[700],
      0.08, 
    ), 
  },
  divider: {
    height: theme.spacing(2),
  }
}));

function NoOptionsMessage(props) {
  return (
    <Typography
      color="textSecondary"
      className={'${props.selectProps} noOptionsMessage'}
      {...props.innerProps}
    >
      {props.children}
    </Typography>
  );
}

NoOptionsMessage.propTypes = {
  /**
   * The children to be rendered.
   */
  children: PropTypes.node,
  /**
   * Props to be passed on to the wrapper.
   */
  innerProps: PropTypes.object.isRequired,
  selectProps: PropTypes.object.isRequired,
};

function inputComponent({ inputRef, ...props }) {
  return <div ref={inputRef} {...props} />;
}

inputComponent.propTypes = {
  inputRef: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.shape({
      current: PropTypes.any.isRequired,
    }),
  ]),
};

function Control(props) {
  const {
    children,
    innerProps,
    innerRef,
    selectProps: { classes, TextFieldProps },
  } = props;
  const [tooltipOpen, setTooltipOpen] = React.useState(false)
  function handleTooltipOpen() {
      setTooltipOpen(true);
  }

  function handleTooltipClose() {
      setTooltipOpen(false);
  }

  return (
    <Grid item xs={12} sm={12}>
   <Tooltip title={""} 
    onOpen={handleTooltipOpen}
    onClose={handleTooltipClose}
    open={tooltipOpen}>
    <TextField 
      fullWidth
      InputProps={{
        inputComponent,
        inputProps: {
          className: "input",
          ref: innerRef,
          children,
          ...innerProps,
        },
      }}
      {...TextFieldProps}
    /></Tooltip>
    </Grid>
  );
}

Control.propTypes = {
  /**
   * Children to render.
   */
  children: PropTypes.node,
  /**
   * The mouse down event and the innerRef to pass down to the controller element.
   */
  innerProps: PropTypes.shape({
    onMouseDown: PropTypes.func.isRequired,
  }).isRequired,
  innerRef: PropTypes.oneOfType([
    PropTypes.oneOf([null]),
    PropTypes.func,
    PropTypes.shape({
      current: PropTypes.any.isRequired,
    }),
  ]).isRequired,
  selectProps: PropTypes.object.isRequired,
};

function Option(props) {
  return (
    <MenuItem
      ref={props.innerRef}
      selected={props.isFocused}
      component="div"
      style={{
        fontWeight: props.isSelected ? 500 : 400,
        whiteSpace:"normal",
        wordBreak:"break-all"
      }}
      {...props.innerProps}
    >
      {props.children}
    </MenuItem>
  );
}

Option.propTypes = {
  /**
   * The children to be rendered.
   */
  children: PropTypes.node,
  /**
   * props passed to the wrapping element for the group.
   */
  innerProps: PropTypes.shape({
    id: PropTypes.string.isRequired,
    key: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    onMouseMove: PropTypes.func.isRequired,
    onMouseOver: PropTypes.func.isRequired,
    tabIndex: PropTypes.number.isRequired,
  }).isRequired,
  /**
   * Inner ref to DOM Node
   */
  innerRef: PropTypes.oneOfType([
    PropTypes.oneOf([null]),
    PropTypes.func,
    PropTypes.shape({
    current: PropTypes.any.isRequired,
    }),
  ]).isRequired,
  /**
   * Whether the option is focused.
   */
  isFocused: PropTypes.bool.isRequired,
  /**
   * Whether the option is selected.
   */
  isSelected: PropTypes.bool.isRequired,
};

function Placeholder(props) {
  const { selectProps, innerProps = {}, children } = props;
  return (
    <Typography color="textSecondary" className={'${selectProps} EIplaceholder'} {...innerProps}>
      {children}
    </Typography>
  );
}

Placeholder.propTypes = {
  /**
   * The children to be rendered.
   */
  children: PropTypes.node,
  /**
   * props passed to the wrapping element for the group.
   */
  innerProps: PropTypes.object,
  selectProps: PropTypes.object.isRequired,
};

function SingleValue(props) {
  return (
    <Typography className={'${props.selectProps} singleValue'}{...props.innerProps}>
      {props.children}
    </Typography>
  );
}

SingleValue.propTypes = {
  /**
   * The children to be rendered.
   */
  children: PropTypes.node,
  /**
   * Props passed to the wrapping element for the group.
   */
  innerProps: PropTypes.any.isRequired,
  selectProps: PropTypes.object.isRequired,
};

function ValueContainer(props) {
  return <div className={'${props.selectProps} EIvalueContainer'}
  >{props.children}</div>;
}

ValueContainer.propTypes = {
  /**
   * The children to be rendered.
   */
  children: PropTypes.node,
  selectProps: PropTypes.object.isRequired,
};

function MultiValue(props) {
  return (
    <Chip 
      tabIndex={-1}
      label={props.children}
      className={clsx(props.selectProps.classes.chip,{
        [props.selectProps.classes.chipFocused]: props.isFocused,
      })}
      onDelete={props.removeProps.onClick}
      deleteIcon={<CancelIcon {...props.removeProps} />}
    />
  );
}

MultiValue.propTypes = {
  children: PropTypes.node,
  isFocused: PropTypes.bool.isRequired,
  removeProps: PropTypes.shape({
    onClick: PropTypes.func.isRequired,
    onMouseDown: PropTypes.func.isRequired,
    onTouchEnd: PropTypes.func.isRequired,
  }).isRequired,
  selectProps: PropTypes.object.isRequired,
};

function Menu(props) {
  return (
    <Paper square className={'${props.selectProps} dropdownselect'} {...props.innerProps}>
      {props.children}
    </Paper>
  );
}

Menu.propTypes = {
  /**
   * The children to be rendered.
   */
  children: PropTypes.element.isRequired,
  /**
   * Props to be passed to the menu wrapper.
   */
  innerProps: PropTypes.object.isRequired,
  selectProps: PropTypes.object.isRequired,
};

const components = {
  Control,
  Menu,
  MultiValue,
  NoOptionsMessage,
  Option,
  Placeholder,
  ValueContainer,
};

export default function ReactSelect(props) {
  const classes = useStyles();
  const theme = useTheme(); 
  var allValue = 
  [{value: 'Read', label: 'Read'},
   {value: 'Write', label: 'Write'},
   {value: 'Remove', label: 'Remove'},
  ];
  const [multi, setMulti] = React.useState(null);
  useEffect(() => {
    if (props.updateData!="") {
      console.log(props.updateData);
      if(props.updateData=="ALL")
      {
        setMulti(allValue);            
      //  props.handlePermissions(allValue);
      }else{
      var updateDataVal=[{value: props.updateData,label: props.updateData}];
      setMulti(updateDataVal); 
     // props.handlePermissions(updateDataVal);
    }
    }
    else
    {
      setMulti('');
      props.handlePermissions('')
    }
  },[props.updateData]);
  const handleChangeMulti = async(val)=> {
    console.log(val);
    try{  
     if(val!==null){
          const newData=await val.map((v,k)=>{

                    return (v.value);
              });
              console.log(newData);
                  if(newData.includes('ALL')){
                    setMulti(suggestions.slice(1))             
                    props.handlePermissions(newData)
                  }else{
                    setMulti(val)             
                    props.handlePermissions(newData)
                  }
        }
      else 
       {  setMulti('');
          props.handlePermissions('')
        }
      }catch(err){
         throw err;
      }
     };

  const selectStyles = {
    input: base => ({
      ...base,
      color: theme.palette.text.primary,
      '& input': {
        font: 'inherit',
      },
    }),
  };
  console.log(props.updateData);
  return (
    <div className="reactselectroot">
      <NoSsr>     
        <Select 
          classes={classes}
          styles={selectStyles}
          className="permissions"
          inputId="react-select-multiple"
          TextFieldProps={{
            label: 'Permissions',
            InputLabelProps: {
              htmlFor: 'react-select-multiple',
              shrink: true,
            },
          }}
          placeholder="" 
          options={suggestions}
          components={components}
          value={multi}
          onChange={handleChangeMulti}
          isMulti
          withAll={true}
        />
      </NoSsr>
    </div>
  );
}
