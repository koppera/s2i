import MaterialTable from 'material-table';
import { forwardRef } from 'react';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import ReactDOM from 'react-dom';
import MTableToolbar  from 'material-table';
import TextField from'@material-ui/core/TextField';
import Dialog from'@material-ui/core/Dialog';
import DialogContent from'@material-ui/core/DialogContent';
import DialogContentText from'@material-ui/core/DialogContentText';
import Fab from'@material-ui/core/Fab';
import AddIcon from'@material-ui/icons/Add';
import CloseIcon from'@material-ui/icons/Close';
import { withStyles } from'@material-ui/core/styles';
import MuiDialogTitle from'@material-ui/core/DialogTitle';
import IconButton from'@material-ui/core/IconButton';
import { Grid, Typography } from '@material-ui/core';
import MuiDialogActions from'@material-ui/core/DialogActions';
import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import Timestamp from '../timestamp';
import Theme from '../css/theme';
import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles
} from '@material-ui/core/styles';
function iconStyles() {
  return { 
   
  };
}
const useStyles = makeStyles(theme=> ({
  root: {
  '& > *': {
  margin:theme.spacing(2),
  
        },
          },
         


  extendedIcon: {
  marginRight:theme.spacing(1),
      },
    }));
    const styles = theme=> ({
      root: {
       Width:1200,
      margin:0,
      padding:theme.spacing(2),
        },
      closeButton: {
      position:'absolute',
      right:theme.spacing(1),
      top:theme.spacing(1),
      color:'#FFFFFF',
        },
      });
      const DialogTitle = withStyles(styles)(props=> {
        const { children, classes, onClose, ...other } = props;
        return (
        <MuiDialogTitle  disableTypography className={classes.root}{...other}>
        <Typography variant="h6">{children}</Typography>
        {onClose ? (
        <IconButton aria-label="close"className={classes.closeButton}onClick={onClose}>
        <CloseIcon/>
        </IconButton>
              ) : null}
        </MuiDialogTitle>
          );
        });
export default function MaterialTableDemo() {
  const [state, setState] = React.useState({
    columns: [
      { title: 'Source Profile', field: 'SourceProfile' },
      { title: 'Target Profile', field: 'TargetProfile' },
      { title: 'Interface Name', field: 'InterfaceName' },
      { title: 'Document Type', field: 'DocumentType' },
      { title: 'Related Doc', field: 'RelatedDoc' },
      { title: 'Content', field: 'content' },
      { title: 'Created', field: 'createdOn',editable: 'never',
      render : (rowData) => {
        console.log(rowData)
        if(rowData!= undefined){
          return Timestamp(rowData.createdOn)
        }else{
          return 
        }
      } },
      { title: 'Last Updated', field: 'LastUpdatedOn',editable: 'never' ,
      render : (rowData) => {
        console.log(rowData)
        if(rowData!= undefined){
          return Timestamp(rowData.LastUpdatedOn)
        }else{
          return 
        }
      } },
          
    ],
    data:[]
   
  });
  const [open, setOpen] = React.useState(false);
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
const classes = makeStyles(iconStyles)();
const tableIcons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear color="error"{...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline  color="error" {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => (<ChevronRight  {...props} ref={ref} />)),
  Edit: forwardRef((props, ref) => (
    <Edit
     
      {...props}
      ref={ref}
      onClick={console.log('hello world')}
    />
  )),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => (
    <ChevronLeft {...props} ref={ref} />
  )),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search  {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => (
    <ArrowDownward {...props} ref={ref} />
  )),
  ThirdStateCheck: forwardRef((props, ref) => (
    <Remove {...props} ref={ref} />
  )),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
};
const getMuiTheme = () =>
  createMuiTheme(Theme, {
    overrides: {
     
      MuiDialogTitle: {
        root: {
        
        backgroundColor:'#0b153e',
        color:'#FFFFFF'
                }
        
              },
      MuiGrid:{
      container:{
        flexWrap:'nowrap' 
      }
    },
      MuiSvgIcon:{
        root:{
      color:"#0b153e"
      }
      },
      MuiTypography:{h6:{
        fontSize:14, 
        fontFamily:"Arial !important"     
        }},
    }
  });
  const classes1 = useStyles(); 
  
return (
  <MuiThemeProvider theme={getMuiTheme()}>
  <MaterialTable
   
   icons={tableIcons}
       title="Transaction"
    columns={state.columns}
    data={state.data}
    editable={{
      onRowAdd: newData =>
        new Promise(resolve => {
          setTimeout(() => {
            resolve();
            setState(prevState => {
              const data = [...prevState.data];
              data.push(newData);
              return { ...prevState, data };
            });
          }, 600);
        }),
      onRowUpdate: (newData, oldData) =>
        new Promise(resolve => {
          setTimeout(() => {
            resolve();
            if (oldData) {
              setState(prevState => {
                const data = [...prevState.data];
                data[data.indexOf(oldData)] = newData;
                return { ...prevState, data };
              });
            }
          }, 600);
        }),
      onRowDelete: oldData =>
        new Promise(resolve => {
          setTimeout(() => {
            resolve();
            setState(prevState => {
              const data = [...prevState.data];
              data.splice(data.indexOf(oldData), 1);
              return { ...prevState, data };
            });
          }, 600);
        }),
    }} 
    options={{
      headerStyle: {
      
      textAlign: 'center',
      fontSize:12,
      fontWeight:'bold',
      fontFamily:'Arial !important',
      backgroundColor:"#0b153e",
      color:"#ffffff",
      padding:'4px',
     
      },
      
        
        searchFieldStyle: {
          color: '#0b153e'
      },    
      actionsColumnIndex: -1
      }}
      onRowClick={(handleClickOpen)}
      />
       <div>
          <Dialog
           fullWidth
           onClose={handleClose}
           open={open}           
           >
          <DialogTitle   id="customized-dialog-title"onClose={handleClose}>
          View 
          </DialogTitle>
         <DialogContent>
      <Grid container spacing={2}>
         <Grid item xs={2} sm={6} gutterBottom variant="body2" color="textSecondary" className="text">
          <Typography className="subheader">Source Profile</Typography> 
         </Grid>
         <Grid item xs={2} sm={6} variant="body2" color="textSecondary" className="text">
          <Typography  className="subheader">Target Profile</Typography> 
         </Grid>
         <Grid item xs={2} sm={6} variant="body2" color="textSecondary" className="text">           
          <Typography  className="subheader">Interface Name </Typography>
         </Grid>
         <Grid item xs={2} sm={6} variant="body2" color="textSecondary" className="text">           
          <Typography  className="subheader">Document Type </Typography>
         </Grid>
     </Grid>
     <Grid container spacing={2}>
     <Grid item xs={2} sm={6} variant="body2" color="textSecondary" className="text">           
          <Typography  className="subheader">Related Doc </Typography>
         </Grid>
     <Grid item xs={2} sm={6} gutterBottom variant="body2" color="textSecondary" className="text"> 
            <Typography  className="subheader">Content </Typography>
        </Grid>
        <Grid item xs={2} sm={6} gutterBottom variant="body2" color="textSecondary" className="text"> 
           <Typography  className="subheader">Created On </Typography>
        </Grid>
        <Grid item xs={2} sm={6} gutterBottom variant="body2" color="textSecondary" className="text"> 
            <Typography  className="subheader">Last Updated On</Typography>
        </Grid>        
     </Grid> 
     </DialogContent>
    </Dialog>
       </div>           
      </MuiThemeProvider>
      );
   }
