import MaterialTable from 'material-table';
import { useMutation, useQuery } from 'graphql-hooks';
import { forwardRef, useEffect } from 'react';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import Loader from '../components/loader';
import ViewColumn from '@material-ui/icons/ViewColumn';
import ReactDOM from 'react-dom';
import MTableToolbar from 'material-table';
import React, { Component } from 'react';
import Timestamp from '../timestamp';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Theme from '../css/theme';
import Error from '../components/emptyPage';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import MenuItem from '@material-ui/core/MenuItem';

import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles,
} from '@material-ui/core/styles';
const DISPLAY_All_PROFILEID_TYPES = `{
  allProfileIDTypes {
    idProfileIDTypes
    profileIDTypesUUID
    profileIDTypeName
    profileIDTypeDescription
    createdOn
    lastUpdatedOn
  }
}

`;

const ADD_PROFILEIDVALUES = `mutation createProfileIDValue($input:ProfileIDValueInput){
  createProfileIDValue(input:$input) {
    idProfileIDValues
    profileIDValuesUUID
    profileIDTypesUUID
    profileIDValue
    partnerProfileUUID
    createdOn
    lastUpdatedOn
  }
}

`;
const UPDATE_PROFILEIDVALUES = `mutation updateProfileIDValues($input:ProfileIDValueInput){
  updateProfileIDValues(input:$input) {
    idProfileIDValues
    profileIDValuesUUID
    profileIDTypesUUID
    profileIDValue
    partnerProfileUUID
    createdOn
    lastUpdatedOn
  }
}`;
const REMOVE_PROFILEIDVALUES = `mutation removeProfileIDValues($input:ProfileIDValueInput){
  removeProfileIDValues(input:$input) {
    idProfileIDValues
    profileIDValuesUUID
    profileIDTypesUUID
    profileIDValue
    partnerProfileUUID
    createdOn
    lastUpdatedOn
  } 

}`;
const FETCH_PROFILEIDVALUES_BY_PARTNERPROFILEUUIDS = `query fetchProfileIDValuesByPartnerProfile($partnerProfileUUID:String){
  fetchProfileIDValuesByPartnerProfile(partnerProfileUUID:$partnerProfileUUID) {
    idProfileIDValues
    profileIDValuesUUID
    profileIDTypesUUID
    profileIDValue
    partnerProfileUUID
    profileIDTypeName
    createdOn
    lastUpdatedOn
  }
}`;
function iconStyles() {
  return {};
}

export default function ProfileIdValues(props) {
  //all PROFILE ID TYPES
  const profileidtypesData = useQuery(DISPLAY_All_PROFILEID_TYPES);
  //ADD PROFILE ID VALUES
  const [addProfileIDValues, addloading, adddata] = useMutation(
    ADD_PROFILEIDVALUES
  );
  const { loading, error, data, refetch } = useQuery(
    FETCH_PROFILEIDVALUES_BY_PARTNERPROFILEUUIDS,
    {
      variables: {
        partnerProfileUUID: props.uuid,
      },
    }
  );

  //EDIT  PROFILE ID VALUES
  const [updateProfileIDValues, updateloading, updatedata] = useMutation(
    UPDATE_PROFILEIDVALUES
  );
  //remove address
  const [removeProfileIDValues, removeloading, removedata] = useMutation(
    REMOVE_PROFILEIDVALUES
  );

  const [profileIdData, setProfileIDtypesValue] = React.useState({});
  const [profileidvaluedata, setData] = React.useState([]);
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [message, setSnackbarMessage] = React.useState();
  const [variant, setSnackbarVariant] = React.useState('error');
  const [serverError, setServerError] = React.useState(false);
  //Mandatory validation
  const [nameError, setNameError] = React.useState({
    error: false,
    label: '',
    helperText: '',
    validateInput: false,
  });
  const [valid, setValid] = React.useState(false);

  const columns = [
    {
      title: 'UUID',
      field: 'profileIDValuesUUID',
      editable: 'never',
      hidden: true,
    },
    {
      title: 'Type ',
      field: 'profileIDTypesUUID',
      //lookup: profileIdData,
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              //placeholder="Description"
              margin="normal"
              select
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            >
              {profileidtypesData.data != undefined &&
              profileidtypesData.data.allProfileIDTypes.length > 0
                ? profileidtypesData.data.allProfileIDTypes.map((v, k) => {
                    return (
                      <MenuItem value={v.profileIDTypesUUID}>
                        {v.profileIDTypeName}
                      </MenuItem>
                    );
                  })
                : null}
            </TextField>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.profileIDTypeName}</div>;
      },
    },

    {
      title: 'Value ',
      field: 'profileIDValue',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder="Value"
              margin="normal"
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            />

            <div style={{ color: 'red' }}>
              {props.rowData.profileIDValue === '' ||
              /^[a-zA-Z0-9 ]+$/.test(props.rowData.profileIDValue)
                ? ''
                : 'Special characters are not allowed'}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.profileIDValue}</div>;
      },
    },
    {
      title: 'Partner Profile  ',
      field: 'partnerProfileUUID',
      hidden: true,
    },
    {
      title: 'Created',
      field: 'createdOn',
      editable: 'never',
      render: (rowData) => {
        // console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.createdOn);
        } else {
          return;
        }
      },
    },
    {
      title: 'Last Modified',
      field: 'lastUpdatedOn',
      editable: 'never',
      render: (rowData) => {
        // console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.lastUpdatedOn);
        } else {
          return;
        }
      },
    },
  ];

  useEffect(() => {
    if (data !== undefined) {
      console.log(data.fetchProfileIDValuesByPartnerProfile);
      setData(data.fetchProfileIDValuesByPartnerProfile);
      setServerError(false);
    } else {
      console.log('server error');
      setServerError(true);
    }
    /// profile id types drop down
    if (
      profileidtypesData !== undefined &&
      profileidtypesData.data !== undefined &&
      profileidtypesData.data.allProfileIDTypes.length > 0
    ) {
      let allProfileIDtypesDetails = profileidtypesData.data.allProfileIDTypes;
      console.log(allProfileIDtypesDetails);

      var objectVal = allProfileIDtypesDetails.reduce(
        (obj, item) => (
          (obj[item.profileIDTypesUUID] = item.profileIDTypeName), obj
        ),
        {}
      );

      setProfileIDtypesValue(objectVal);
    }
  }, [data]);
  const handleRefetch = (value, message) => {
    refetch({
      updateData(_, data) {
        return data;
      },
    });
    setOpenSnackbar(true);
    setSnackbarMessage(message);
    if (value) {
      setSnackbarVariant('error');
    } else {
      setSnackbarVariant('success');
    }
  };
  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }
  const classes = makeStyles(iconStyles)();
  const tableIcons = {
    Add: forwardRef((props, ref) => (
      <AddBox color="primary" {...props} ref={ref} />
    )),
    Check: forwardRef((props, ref) => (
      <Check color="primary" {...props} ref={ref} />
    )),
    Clear: forwardRef((props, ref) => (
      <Clear color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline color="error" {...props} ref={ref} />
    )),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => (
      <Edit
        color="primary"
        style={{ marginLeft: '80px' }}
        {...props}
        ref={ref}
      />
    )),
    Export: forwardRef((props, ref) => (
      <SaveAlt color="primary" {...props} ref={ref} />
    )),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => (
      <FirstPage color="primary" {...props} ref={ref} />
    )),
    LastPage: forwardRef((props, ref) => (
      <LastPage color="primary" {...props} ref={ref} />
    )),
    NextPage: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft color="primary" {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => (
      <Clear color="primary" {...props} ref={ref} />
    )),
    Search: forwardRef((props, ref) => (
      <Search color="primary" {...props} ref={ref} />
    )),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };

  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiTypography: {
          h6: {
            fontSize: 14,
            fontFamily: 'Arial !important',
          },
        },
        MuiSelect: {
          select: {
            minWidth: 160,
          },
          icon: {
            color: '#0b153e',
          },
        },

        MuiToolbar: {
          root: {
            backgroundColor: '#ffffff',
            color: '#0b153e',
            padding: '0 !important',
          },
        },
        MuiTablePagination: {
          input: {
            color: '#0b153e',
          },
        },

        MuiTableSortLabel: {
          root: {
            color: '#ffffff ',
          },
        },
        MuiTableSortLabel: {
          root: {
            hover: {
              color: '#ffffff ',
            },
          },
          MuiTableSortLabel: {
            root: {
              MuiTableSortLabel: {
                active: {
                  color: '#ffffff !important',
                },
              },
            },
          },
        },
      },
    });
  async function handleAddProfileIDValues(newData) {
    return await new Promise(async (resolve) => {
      resolve();
      await addProfileIDValues({
        variables: {
          input: {
            profileIDTypesUUID: newData.profileIDTypesUUID,
            profileIDValue: newData.profileIDValue,
            partnerProfileUUID: props.uuid,
          },
        },
      })
        .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
          console.log(data, error, graphQLErrors, networkError, cacheHit);
          if (data !== null && error == false) {
            handleRefetch(error, 'Saved successfully');
          } else if (error && graphQLErrors.length > 0) {
            let duperror = graphQLErrors[0].message;
            console.log(duperror);
            if (duperror !== null && duperror.indexOf('ER_DUP_ENTRY') !== -1) {
              handleRefetch(error, 'Value already existed');
            }
            if (
              duperror !== null &&
              duperror.indexOf('Unexpected error value') !== -1
            ) {
              handleRefetch(error, 'Combination already exist');
            }
          }
        })
        .catch((e) => {
          // you can do something with the error here
          console.log(e);
        });
      /* console.log(addloading, adddata);
      if (addloading.loading) {
        console.log('profile id values not created');
        handleRefetch(addloading.loading, 'Graphql hooks error');
      } else {
        console.log('profile id values successfully');
        handleRefetch(addloading.loading, 'Saved successfully');
      }*/
    });
  }
  async function handleUpdateProfileIDValues(newData, oldData) {
    console.log(newData, oldData);

    if (
      newData.profileIDTypesUUID === oldData.profileIDTypesUUID &&
      newData.profileIDValue === oldData.profileIDValue
    ) {
      handleRefetch(false, 'Updated successfully');
    } else {
      return await new Promise(async (resolve) => {
        resolve();
        console.log(newData);
        await updateProfileIDValues({
          variables: {
            input: {
              profileIDTypesUUID: newData.profileIDTypesUUID,
              profileIDValue: newData.profileIDValue,
              partnerProfileUUID: props.uuid,
              profileIDValuesUUID: newData.profileIDValuesUUID,
            },
          },
        })
          .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
            console.log(data, error, graphQLErrors, networkError, cacheHit);
            if (data !== null && error == false) {
              handleRefetch(error, 'Updated successfully');
            } else if (error && graphQLErrors.length > 0) {
              let duperror = graphQLErrors[0].message;
              console.log(duperror);
              if (
                duperror !== null &&
                duperror.indexOf('ER_DUP_ENTRY') !== -1
              ) {
                handleRefetch(error, 'Value already existed');
              }
              if (
                duperror !== null &&
                duperror.indexOf('Unexpected error value') !== -1
              ) {
                handleRefetch(error, 'Combination already exist');
              }
            }
          })
          .catch((e) => {
            // you can do something with the error here
            console.log(e);
          });
        /*if (updateloading.loading) {
        console.log('profileidvalues not updated');
        handleRefetch(updateloading.loading, 'Graphql hooks error');
      } else {
        console.log('profileidvalues updated successfully');
        handleRefetch(updateloading.loading, 'Updated successfully');
      }*/
      });
    }
  }
  async function handleRemoveProfileIDValues(oldData) {
    return await new Promise(async (resolve) => {
      resolve();
      await removeProfileIDValues({
        variables: {
          input: {
            profileIDValuesUUID: oldData.profileIDValuesUUID,
          },
        },
      });
      console.log(removeloading, removedata);
      if (removeloading.loading) {
        console.log('profileidvalues  not deleted');
        handleRefetch(removeloading.loading, 'Graphql hooks error');
      } else {
        console.log('profileidvalues deleted successfully');
        handleRefetch(removeloading.loading, 'Deleted successfully');
      }
    });
  }
  if (loading) return <Loader />;
  if (serverError)
    return <Error type={'Server connection lost.Please try again'} />;
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <MaterialTable
        icons={tableIcons}
        title=""
        columns={columns}
        data={profileidvaluedata}
        editable={{
          onRowAdd: (newData) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                newData.submitted = true;
                if (
                  !newData.profileIDValue ||
                  !newData.profileIDTypesUUID ||
                  !/^[a-zA-Z0-9 ]+$/.test(newData.profileIDValue)
                ) {
                  setNameError({
                    error: true,
                    label: 'required',
                    helperText: 'Required.',
                    validateInput: true,
                  });
                  reject();
                  return;
                }
                resolve();
                handleAddProfileIDValues(newData);
              }, 600);
            }),

          onRowUpdate: (newData, oldData) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                newData.submitted = true;
                if (
                  !newData.profileIDValue ||
                  !newData.profileIDTypesUUID ||
                  !/^[a-zA-Z0-9 ]+$/.test(newData.profileIDValue)
                ) {
                  setNameError({
                    error: true,
                    label: 'required',
                    helperText: 'Required.',
                    validateInput: true,
                  });
                  reject();
                  return;
                }
                resolve();
                handleUpdateProfileIDValues(newData, oldData);
              }, 600);
            }),

          onRowDelete: (oldData) => handleRemoveProfileIDValues(oldData),
        }}
        options={{
          toolbarStyle: {
            minHeight: '80px',
          },
          headerStyle: {
            textAlign: 'center',
            fontSize: 12,
            fontWeight: 'bold',
            fontFamily: 'Arial !important',
            backgroundColor: '#0b153e',
            color: '#ffffff',
            padding: '4px',
          },
          searchFieldStyle: {
            color: '#0b153e',
          },
          actionsColumnIndex: -1,
        }}
      />
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        autoHideDuration={1500}
        open={openSnackbar}
        onClose={handleCloseSnackbar}
      >
        <MySnackbarContentWrapper
          onClose={handleCloseSnackbar}
          variant={variant}
          message={message}
        />
      </Snackbar>
    </MuiThemeProvider>
  );
}
