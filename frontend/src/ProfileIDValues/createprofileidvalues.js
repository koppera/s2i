import React, { useEffect, useContext, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import '../css/commonStyle.css';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import Theme from '../css/theme';
import Grid from '@material-ui/core/Grid';
import {
  createMuiTheme,
  MuiThemeProvider,
} from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing(2)
  }
}));


const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSelect:{
          select:{
            minWidth:180
          }
          }
      }
    });

export default function AddProjectForm(props) {
  const classes = useStyles();
  const [dropdown, setDropdown] = React.useState('');
  const [state, setState] = React.useState({
    checkedA: true,
    checkedB: true
  });

  const handleChangeDropdown = name => event => {
    setErrMsg(false)
    setErrMsgDropdown(false)
    setErrMsgNameInfo(false)
    setValues({
      ...values,
      [event.target.name]: event.target.value
    });
  
  };

  const [values, setValues] = React.useState({ name: '', Description: '', prefferedProtocol:''});
  const [ErrMsgDropdown, setErrMsgDropdown] = React.useState(false);
  const [ErrMsgNameInfo, setErrMsgNameInfo] = React.useState(false);
  const [ErrMsg, setErrMsg] = React.useState(false);
  const [errorInfo, setErrorInfo] = React.useState({
    name: false,
    prefferedProtocol:false,
    Description: false
  });
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [value, setValue] = React.useState('IS');
  const [variant, setSnackbarVariant] = React.useState('error');
  const [message, setSnackbarMessage] = React.useState();

  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }

  const handleChange = name => event => {
    setErrMsg(false);
    setErrMsgDropdown(false);
    setValues({ ...values, [name]: event.target.value });
    switch (name) {
      case 'name':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else {
          if (
            event.target.value.match('^([a-zA-Z])[a-zA-Z]*$') &&
            event.target.value.length <= 25
          ) {
            setErrorInfo({ ...errorInfo, [name]: false });
            setValues({ ...values, [name]: event.target.value });
          } else {
            setErrorInfo({ ...errorInfo, [name]: true });
          }
        }
        break;

      case 'Description':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else {
          if (
            event.target.value.match('^([a-zA-Z])[a-zA-Z]*$') &&
            event.target.value.length <= 25
          ) {
            setErrorInfo({ ...errorInfo, [name]: false });
            setValues({ ...values, [name]: event.target.value });
          } else {
            setErrorInfo({ ...errorInfo, [name]: true });
          }
        }
        break;

      default:
        return false;
    }
  };

  const handleSubmit = async () => {
    console.log(values);
    setErrMsg(false);
    setErrMsgDropdown(false);
    if (values.name === '' || values.Description === '' || values.prefferedProtocol === '') {
      setErrMsg(true);
    } else {
      setErrMsg(false);
      setValues({ ...values, name: '', Description: '' ,prefferedProtocol:''});
    }
  };

  return (
    <MuiThemeProvider theme={getMuiTheme()}>
    <div>
      {ErrMsg === true ? (
        <div style={{ textAlign: 'center' }} className="addfieldserror">
          Please fill the fields
        </div>
      ) : (
        ''
      )}
      <form className="content" noValidate autoComplete="off">
        <TextField
          id="outlined-dense"
          label="Name "
          className="textField"
          margin="dense"
          variant="outlined"
          onChange={handleChange('name')}
          value={values.name}
          name="name"
        />

        {errorInfo.name === true ? (
          <div className="addfieldserror">
            "Allowed alphabets,no special characters and Numerics are
            allowed.Maximum length is 25."
          </div>
        ) : (
          ''
        )}

        <TextField
          id="outlined-dense"
          label="Description "
          className="textField"
          margin="dense"
          variant="outlined"
          //placeholder=""
          onChange={handleChange('Description')}
          value={values.Description}
          name="Description"
        />

        {errorInfo.Description === true ? (
          <div id="nameid" className="addfieldserror">
            "Allowed alphabets,no special characters and Numerics are
            allowed.Maximum length is 25."
          </div>
        ) : (
          ''
        )}

        <Grid item xs={12} sm={6}>
          <FormControl className={classes.formControl}>
            <InputLabel id="demo-simple-select-label">
              PrefferedProtocolUUID
            </InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={values.prefferedProtocol}
              onChange={handleChangeDropdown(values.prefferedProtocol)}
              name="prefferedProtocol"
            >
              <MenuItem value={10}>TMS</MenuItem>
              <MenuItem value={20}>Changemaker</MenuItem>
              <MenuItem value={30}>Cloudgen</MenuItem>
            </Select>
          </FormControl>
        </Grid>

        <Button
          onClick={handleSubmit}
          variant="contained"
          fullWidth="true"
          className="createpartnerbutton"
        >
          Create
        </Button>
      </form>
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center'
        }}
        autoHideDuration={5000}
        open={openSnackbar}
        onClose={handleCloseSnackbar}
      >
        <MySnackbarContentWrapper
          onClose={handleCloseSnackbar}
          variant={variant}
          message={message}
        />
      </Snackbar>
    </div>
    </MuiThemeProvider>
  );
}
