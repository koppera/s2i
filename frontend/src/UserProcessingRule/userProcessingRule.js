import MaterialTable from 'material-table';
import { forwardRef } from 'react';
import { useMutation, useQuery, useManualQuery } from 'graphql-hooks';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import { TextField } from '@material-ui/core';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import ReactDOM from 'react-dom';
import MTableToolbar from 'material-table';
import React, { Component, useEffect } from 'react';
import Timestamp from '../timestamp';
import Button from '@material-ui/core/Button';
import Theme from '../css/theme';
import SendIcon from '@material-ui/icons/Send';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import Error from '../components/emptyPage';
import Loader from '../components/loader';
import MenuItem from '@material-ui/core/MenuItem';
import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles,
} from '@material-ui/core/styles';
const FETCH_BY_INTERFACE_UUID = `query fetchorginterfaceByInteerfaceUUID($interfaceUUID:String){
  fetchorginterfaceByInteerfaceUUID(interfaceUUID:$interfaceUUID) {
    idOrgInterface
    orgInterfaceUUID
    interfaceUUID
    projectUUID
    sourceProfileUUID
    targetProfileUUID
    sourceorganizationUnit
    targetorganizationUnit
    docTypeUUID
    serviceUUID
    organizationUUID
    APIStatus
    createdOn
    lastUpdatedOn
  }
}`;
const DISPLAY_PARTNERS = `query allPartnersByStatusandProjandorgUUID($projectUUID:String){
  allPartnersByStatusandProjandorgUUID(projectUUID:$projectUUID) {
    PartnerProfileUUID
    CorporationName
  } 
   
}`;
const DISPLAY_All_DOCUMENTTYPE = `query fetchDocumentType1($searchInput:String){
  fetchDocumentType1(searchInput:$searchInput) {          
    documentTypeUUID
    name           
  }
}
`;
const DISPLAY_All_SERVICE = `query fetchSERVICE1($searchInput:String){
  fetchService1(searchInput:$searchInput) {
    serviceUUID
    name      
  } 
}
`;
const API_CAll_DATA = `query allOrgInterfaceAPI($InputCreate:OrgInterfaceInput){ 
  allOrgInterfaceAPI(input:$InputCreate){

   statusAPI
   msg
  }
}`;
const ADD_ORG_INTERFACE = `mutation createOrgInterface($InputCreate:OrgInterfaceInput){
  createOrgInterface(input:$InputCreate){
    interfaceUUID 
      projectUUID 
      sourceProfileUUID
      targetProfileUUID
      docTypeUUID
      serviceUUID
      organizationUUID
  }
}`;
const Fetch_ORG_FROM_PROJECT = `query fetchProjectByUUID($projectUUID:String){
  fetchProjectByUUID(projectUUID:$projectUUID) {
    organizationUUID
  } 
}`;
const DISPLAY_PARTNERS_BY_ORGUUID_STATUS = `query allPartnersByStatusandOrgUUID($organizationUUID:String){
  allPartnersByStatusandOrgUUID(organizationUUID:$organizationUUID) {
    PartnerProfileUUID
    CorporationName
  }    
}
`;
function iconStyles() {
  return {};
}

export default function UserProcessingRule(props) {
  const [searchInput, setSearchValue] = React.useState('');
  const [partnerData, setPartnerData] = React.useState([]);
  const [documentTypeData, setDocumentTypeData] = React.useState([]);
  const [serviceData, setServiceData] = React.useState([]);
  const [orgInterface, setOrgInterface] = React.useState([]);
  const [serverError, setServerError] = React.useState(false);
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [message, setSnackbarMessage] = React.useState();
  const [variant, setSnackbarVariant] = React.useState('error');
  const [orgUUID, setOrgUUID] = React.useState('error');

  const [nameError, setNameError] = React.useState({
    error: false,
    label: '',
    helperText: '',
    validateInput: false,
  });
  console.log(props);
  const {
    loading: poloading,
    error: poerror,
    data: projectorgData,
    refetch: porefetch,
  } = useQuery(Fetch_ORG_FROM_PROJECT, {
    variables: {
      projectUUID: props.rowData.projectUUID,
    },
  });
  const [
    fetchOrgInterface,
    { loading: pTaloading, data: pTadata, error: pTaerrors },
  ] = useManualQuery(API_CAll_DATA);
  const { loading, error, data, refetch } = useQuery(FETCH_BY_INTERFACE_UUID, {
    variables: {
      interfaceUUID: props.rowData.interfaceUUID,
    },
  });
  const {
    loading: dtloading,
    error: dterror,
    data: DocumentTypeData,
    refetch: dtrefetch,
  } = useQuery(DISPLAY_All_DOCUMENTTYPE, {
    variables: {
      searchInput: searchInput,
    },
  });
  const {
    loading: ploading,
    error: perror,
    data: PartnerData,
    refetch: prefetch,
  } = useQuery(DISPLAY_PARTNERS_BY_ORGUUID_STATUS, {
    variables: {
      organizationUUID: orgUUID,
    },
  });
  const {
    loading: sloading,
    error: serror,
    data: ServiceData,
    refetch: srefetch,
  } = useQuery(DISPLAY_All_SERVICE, {
    variables: {
      searchInput: searchInput,
    },
  });
  const [addOrgInterface] = useMutation(ADD_ORG_INTERFACE);
  const columns = [
    {
      title: 'UUID',
      field: 'orgInterfaceUUID',
      editable: 'never',
      hidden: true,
    },
    {
      title: 'Source Profile',
      field: 'sourceProfileUUID',
      lookup: partnerData,
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              //placeholder="Description"
              margin="normal"
              select
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            >
              {PartnerData != undefined &&
              PartnerData.allPartnersByStatusandOrgUUID != undefined &&
              PartnerData.allPartnersByStatusandOrgUUID.length > 0
                ? PartnerData.allPartnersByStatusandOrgUUID.map((v, k) => {
                    return (
                      <MenuItem value={v.PartnerProfileUUID}>
                        {v.CorporationName}
                      </MenuItem>
                    );
                  })
                : null}
            </TextField>
          </div>
        );
      },

      render: (rowData) => {
        return <div>{rowData.sourceProfileUUID}</div>;
      },
    },
    {
      title: 'Target Profile',
      field: 'targetProfileUUID',
      lookup: partnerData,
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              //placeholder="Description"
              margin="normal"
              select
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            >
              {PartnerData != undefined &&
              PartnerData.allPartnersByStatusandOrgUUID != undefined &&
              PartnerData.allPartnersByStatusandOrgUUID.length > 0
                ? PartnerData.allPartnersByStatusandOrgUUID.map((v, k) => {
                    return (
                      <MenuItem value={v.PartnerProfileUUID}>
                        {v.CorporationName}
                      </MenuItem>
                    );
                  })
                : null}
            </TextField>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.targetProfileUUID}</div>;
      },
    },

    {
      title: 'Document',
      field: 'docTypeUUID',
      //lookup: documentTypeData,
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              //placeholder="Description"
              margin="normal"
              select
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            >
              {DocumentTypeData != undefined &&
              DocumentTypeData.fetchDocumentType1 != undefined &&
              DocumentTypeData.fetchDocumentType1.length > 0
                ? DocumentTypeData.fetchDocumentType1.map((v, k) => {
                    return (
                      <MenuItem value={v.documentTypeUUID}>{v.name}</MenuItem>
                    );
                  })
                : null}
            </TextField>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.docTypeUUID}</div>;
      },
    },
    {
      title: 'Service',
      field: 'serviceUUID',
      lookup: serviceData,
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              //placeholder="Description"
              margin="normal"
              select
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            >
              {ServiceData != undefined &&
              ServiceData.fetchService1 != undefined &&
              ServiceData.fetchService1.length > 0
                ? ServiceData.fetchService1.map((v, k) => {
                    return <MenuItem value={v.serviceUUID}>{v.name}</MenuItem>;
                  })
                : null}
            </TextField>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.serviceUUID}</div>;
      },
    },
    {
      title: 'API Status',

      // field: 'Status'
      render: (rowData) => {
        //console.log(rowData, rowData.APIStatus);
        if (rowData === undefined) {
          return '';
        } else if (
          rowData.APIStatus == null ||
          rowData.APIStatus === 'Inactive'
        ) {
          return <div style={{ color: 'red' }}>Failure</div>;
        } else {
          return <div style={{ color: 'green' }}>Success</div>;
        }
      },
    },
    {
      title: 'Created',
      field: 'createdOn',
      editable: 'never',
      render: (rowData) => {
        if (rowData != undefined) {
          return Timestamp(rowData.createdOn);
        } else {
          return;
        }
      },
    },
    {
      title: 'Last Modified',
      field: 'lastUpdatedOn',
      editable: 'never',
      render: (rowData) => {
        if (rowData != undefined) {
          return Timestamp(rowData.lastUpdatedOn);
        } else {
          return;
        }
      },
    },
  ];
  useEffect(() => {
    /*if (pTadata !== undefined) {
      console.log(pTadata.allOrgInterfaceAPI);
      handleAPISuccess(
        pTadata.allOrgInterfaceAPI.statusAPI,
        pTadata.allOrgInterfaceAPI.msg
      );
    }*/
    if (data !== undefined) {
      console.log(data.fetchorginterfaceByInteerfaceUUID);
      setOrgInterface(data.fetchorginterfaceByInteerfaceUUID);
      setServerError(false);
    } else {
      console.log('server error');
      setServerError(true);
    }
    if (projectorgData !== undefined) {
      console.log(projectorgData.fetchProjectByUUID);
      var projectorgDataDD = projectorgData.fetchProjectByUUID;
      console.log(projectorgDataDD[0].organizationUUID);
      setOrgUUID(projectorgDataDD[0].organizationUUID);
    }
    if (PartnerData !== undefined) {
      var PartnerDataDD = PartnerData.allPartnersByStatusandOrgUUID.reduce(
        (obj, item) => (
          (obj[item.PartnerProfileUUID] = item.CorporationName), obj
        ),
        {}
      );
      console.log(PartnerDataDD);
      setPartnerData(PartnerDataDD);
    }
    if (DocumentTypeData !== undefined) {
      var DocumentTypeDataDD = DocumentTypeData.fetchDocumentType1.reduce(
        (obj, item) => ((obj[item.documentTypeUUID] = item.name), obj),
        {}
      );
      console.log(DocumentTypeDataDD);
      setDocumentTypeData(DocumentTypeDataDD);
    }
    if (ServiceData !== undefined) {
      var ServiceDataDD = ServiceData.fetchService1.reduce(
        (obj, item) => ((obj[item.serviceUUID] = item.name), obj),
        {}
      );
      console.log(ServiceDataDD);
      setServiceData(ServiceDataDD);
    }
  }, [PartnerData, DocumentTypeData, ServiceData, data, projectorgData]);
  const handleRefetch = (value, message) => {
    refetch({
      updateData(_, data) {
        return data;
      },
    });
    setOpenSnackbar(true);
    setSnackbarMessage(message);
    if (value) {
      setSnackbarVariant('error');
    } else {
      setSnackbarVariant('success');
    }
  };

  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }
  const classes = makeStyles(iconStyles)();
  const tableIcons = {
    Add: forwardRef((props, ref) => (
      <AddBox color="primary" {...props} ref={ref} />
    )),
    Check: forwardRef((props, ref) => (
      <Check color="primary" {...props} ref={ref} />
    )),
    Clear: forwardRef((props, ref) => (
      <Clear color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline
        style={{ marginLeft: '130%' }}
        color="error"
        {...props}
        ref={ref}
      />
    )),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => (
      <Edit
        style={{ marginLeft: '60%' }}
        {...props}
        ref={ref}
        color="primary"
        onClick={console.log('hello world')}
      />
    )),
    Export: forwardRef((props, ref) => (
      <SaveAlt color="primary" {...props} ref={ref} />
    )),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => (
      <FirstPage color="primary" {...props} ref={ref} />
    )),
    LastPage: forwardRef((props, ref) => (
      <LastPage color="primary" {...props} ref={ref} />
    )),
    NextPage: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft color="primary" {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => (
      <Search color="primary" {...props} ref={ref} />
    )),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSelect: {
          icon: {
            color: '#0b153e',
          },
        },

        MuiTypography: {
          h6: {
            fontSize: 14,
            fontFamily: 'Arial !important',
          },
        },
      },
    });

  const handleAPISuccess = async (Status, aPIMsg) => {
    //console.log('MsgAPI', msgAPI);
    //setOpenSnackbar(true);
    //setSnackbarMessage(aPIMsg);

    if (Status === 'failure' || Status === null) {
      // setSnackbarVariant('error');
      handleRefetch(true, aPIMsg);
    } else {
      // setSnackbarVariant('success');
      handleRefetch(false, aPIMsg);
    }
  };
  const handleCallAPI = async (event, rowData) => {
    console.log('rowData', rowData);
    await fetchOrgInterface({
      variables: {
        InputCreate: {
          projectUUID: props.rowData.projectUUID,
          sourceProfileUUID: rowData.sourceProfileUUID,
          targetProfileUUID: rowData.targetProfileUUID,
          sourceorganizationUnit: rowData.sourceorganizationUnit,
          targetorganizationUnit: rowData.targetorganizationUnit,
          docTypeUUID: rowData.docTypeUUID,
          serviceUUID: rowData.serviceUUID,
          interfaceUUID: props.rowData.interfaceUUID,
          organizationUUID: orgUUID,
          orgInterfaceUUID: rowData.orgInterfaceUUID,
        },
      },
    }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
      console.log(data, error, graphQLErrors, networkError, cacheHit);
      if (data !== null && error == false) {
        handleAPISuccess(
          data.allOrgInterfaceAPI.statusAPI,
          data.allOrgInterfaceAPI.msg
        );
      } else {
        handleRefetch(error, 'graphql hooks error');
      }
    });
  };
  async function handleAddOrgInterface(newData) {
    return await new Promise(async (resolve) => {
      resolve();
      console.log(newData);
      addOrgInterface({
        variables: {
          InputCreate: {
            projectUUID: props.rowData.projectUUID,
            sourceProfileUUID: newData.sourceProfileUUID,
            targetProfileUUID: newData.targetProfileUUID,
            docTypeUUID: newData.docTypeUUID,
            serviceUUID: newData.serviceUUID,
            interfaceUUID: props.rowData.interfaceUUID,
            organizationUUID: orgUUID,
          },
        },
      })
        .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
          console.log(data, error, graphQLErrors, networkError, cacheHit);
          if (data !== null && error == false) {
            handleRefetch(error, 'Saved successfully');
          } else if (error && graphQLErrors.length > 0) {
            let duperror = graphQLErrors[0].message;
            console.log(duperror);
            if (duperror !== null && duperror.indexOf('ER_DUP_ENTRY') !== -1) {
              handleRefetch(error, 'Name already existed');
            }
          }
        })
        .catch((e) => {
          // you can do something with the error here
          console.log(e);
        });
    });
  }
  if (loading) return <Loader />;
  if (serverError)
    return <Error type={'Server connection lost.Please try again'} />;
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <MaterialTable
        icons={tableIcons}
        title="User Processing Rule"
        columns={columns}
        data={orgInterface}
        editable={{
          onRowAdd: (newData) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                newData.submitted = true;
                if (
                  !newData.sourceProfileUUID ||
                  !newData.targetProfileUUID ||
                  !newData.docTypeUUID ||
                  !newData.serviceUUID
                ) {
                  setNameError({
                    error: true,
                    label: 'required',
                    helperText: 'Required.',
                    validateInput: true,
                  });
                  reject();
                  return;
                }
                resolve();

                handleAddOrgInterface(newData);
              }, 600);
            }),
        }}
        actions={[
          (rowData) => {
            console.log(rowData, rowData.APIStatus);
            return rowData.APIStatus == null || rowData.APIStatus == 'Inactive'
              ? {
                  icon: SendIcon,
                  disabled: false,
                  onClick: (event, rowData) => handleCallAPI(event, rowData),
                }
              : {
                  icon: SendIcon,
                  disabled: true,
                  //hidden: true
                };
          },
        ]}
        /*  actions={[
          {
            icon: SendIcon,
            tooltip: 'API',
            onClick: (event, rowData) => handleCallAPI(event, rowData),
          },
        ]}*/
        options={{
          headerStyle: {
            textAlign: 'center',
            fontSize: 12,
            fontWeight: 'bold',
            fontFamily: 'Arial !important',
            backgroundColor: '#0b153e',
            color: '#ffffff',
            padding: '4px',
          },
          searchFieldStyle: {
            color: '#0b153e',
          },
          actionsColumnIndex: -1,
        }}
      />
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        autoHideDuration={1500}
        open={openSnackbar}
        onClose={handleCloseSnackbar}
      >
        <MySnackbarContentWrapper
          onClose={handleCloseSnackbar}
          variant={variant}
          message={message}
        />
      </Snackbar>
    </MuiThemeProvider>
  );
}
