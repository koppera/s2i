import React, { useEffect, useContext, useCallback } from 'react';
import KibanaComponent from '../components/kibanaComponent.js';
import Iframe from 'react-iframe';
export default function KibanaDashboard(props) {
  console.log(props);
  

  return (
  
   < KibanaComponent url={process.env.REACT_APP_KIBANA+"app/kibana#/dashboard/1d692930-9396-11ea-bcb0-ad0109361958?_g=(refreshInterval:(%27$$hashKey%27:%27object:4225%27,display:%275%20seconds%27,pause:!f,section:1,value:5000),time:(from:now%2FM,mode:quick,to:now%2FM))&_a=(description:%27%27,filters:!(),fullScreenMode:!f,options:(darkTheme:!f,hidePanelTitles:!f,useMargins:!t),panels:!((embeddableConfig:(spy:(mode:(name:table)),vis:(params:(sort:(columnIndex:!n,direction:!n)))),gridData:(h:3,i:%271%27,w:12,x:0,y:2),id:%277a3c9770-8313-11ea-bf9f-d77f82ebca63%27,panelIndex:%271%27,type:visualization,version:%276.2.4%27),(gridData:(h:2,i:%272%27,w:12,x:0,y:0),id:%2780effb50-9396-11ea-bcb0-ad0109361958%27,panelIndex:%272%27,type:visualization,version:%276.2.4%27)),query:(language:lucene,query:%27%27),timeRestore:!f,title:B2BLogs_Transactions_MainDashboard,viewMode:view)"}/>
   

   
    
  );
}
