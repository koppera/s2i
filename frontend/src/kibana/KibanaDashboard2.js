import React, { useEffect, useContext, useCallback } from 'react';
import KibanaComponent from '../components/kibanaComponent.js';
import Iframe from 'react-iframe';
export default function KibanaDashboard(props) {
  console.log(props);
 

  
  return (
    < KibanaComponent url={process.env.REACT_APP_KIBANA+"app/kibana#/dashboard/937bdf90-9a0e-11ea-bcb0-ad0109361958?embed=true&_g=(refreshInterval%3A(display%3AOff%2Cpause%3A!f%2Cvalue%3A0)%2Ctime%3A(from%3Anow-60d%2Cmode%3Aquick%2Cto%3Anow))"}/>
  );
}
