import MaterialTable from 'material-table';
import { useMutation, useQuery, useManualQuery } from 'graphql-hooks';
import { forwardRef, useEffect } from 'react';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import ReactDOM from 'react-dom';
import MTableToolbar from 'material-table';
import MenuItem from '@material-ui/core/MenuItem';
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Theme from '../css/theme';
import { TextField } from '@material-ui/core';
import Timestamp from '../timestamp';
import Error from '../components/emptyPage';
import Snackbar from '@material-ui/core/Snackbar';
import SendIcon from '@material-ui/icons/Send';
import MySnackbarContentWrapper from '../components/Snackbar';
import Loader from '../components/loader';
import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles,
} from '@material-ui/core/styles';
const DISPLAY_All_DOCUMENTTYPE = `query fetchDocumentType($searchInput:String){
  fetchDocumentType(searchInput:$searchInput) {
    idDocumentType
    documentTypeUUID
    name
    description
    docType
    APIStatus
    createdOn
    lastUpdatedOn
  }
}
`;
const ADD_DOCUMENTTYPE = `mutation createDocumentType($input:DocumentTypeInput){
  createDocumentType(input:$input) {
    idDocumentType
    documentTypeUUID
    name
    description
    docType
    APIStatus
    createdOn
    lastUpdatedOn
  } 
}`;
const UPDATE_DOCUMENTTYPE = `mutation updateDOCUMENTTYPE($input:DocumentTypeInput){
  updateDocumentType(input:$input) {
    idDocumentType
    documentTypeUUID
    name
    description
    docType
    APIStatus
    createdOn
    lastUpdatedOn
  }
}`;
const REMOVE_DOCUMENTTYPE = `mutation removeDocumentType($input:DocumentTypeInput){
  removeDocumentType(input:$input) {
    idDocumentType
    documentTypeUUID
    name
    description
    docType
    APIStatus
    createdOn
    lastUpdatedOn
  } 
}
`;
const API_CAll_DATA = `query DocumnetTypeAPI($InputCreate:DocumentTypeInput){ 
  DocumnetTypeAPI(input:$InputCreate){
   statusAPI
   msg
  }
}`;
function iconStyles() {
  return {};
}
export default function MaterialTableDemo() {
  const [searchInput, setSearchValue] = React.useState('');
  //Fetch ALL DocumentType
  const { loading, errors, data, refetch } = useQuery(
    DISPLAY_All_DOCUMENTTYPE,
    {
      variables: {
        searchInput: searchInput,
      },
    }
  );
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [message, setSnackbarMessage] = React.useState();
  const [variant, setSnackbarVariant] = React.useState('error');
  const [nameError, setNameError] = React.useState({
    error: false,
    label: '',
    helperText: '',
    validateInput: false,
  });
  const nameexp = /^[a-zA-Z0-9\s]+$/;
  //ADD DocumentType
  const [addDocumentType, addloading, adddata] = useMutation(ADD_DOCUMENTTYPE);
  const [DocumentTypedata, setData] = React.useState([]);
  const [serverError, setServerError] = React.useState(false);
  //EDIT DocumentType
  const [updateDocumentType, updateloading, updatedata] = useMutation(
    UPDATE_DOCUMENTTYPE
  );
  //remove DocumentType
  const [removeDocumentType, removeloading, removedata] = useMutation(
    REMOVE_DOCUMENTTYPE
  );
  //fetch api for document type
  const [
    fetchAPIdocumentType,
    { loading: pTaloading, data: pTadata, error: pTaerrors },
  ] = useManualQuery(API_CAll_DATA);
  const columns = [
    { title: 'UUID', field: 'UUID', editable: 'never', hidden: true },
    {
      title: 'Type',
      field: 'docType',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              //placeholder="Description"
              margin="normal"
              select
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            >
              <MenuItem value={'EDI'}> EDI </MenuItem>
              <MenuItem value={'FF'}> FF </MenuItem>
            </TextField>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.docType}</div>;
      },
    },
    {
      title: 'Name',
      field: 'name',
      editComponent: (props) => {
        return (
          <div>
            <TextField
              placeholder="Name"
              margin="normal"
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              onChange={(v) => props.onChange(v.target.value)}
            />

            <div style={{ color: 'red' }}>
              {!props.rowData.name || nameexp.test(props.rowData.name)
                ? ''
                : 'Special characters are not allowed'}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.name}</div>;
      },
    },
    {
      title: 'Description',
      field: 'description',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder="Description"
              margin="normal"
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            />
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.description}</div>;
      },
    },
    {
      title: 'API Status',
      //  field: 'APIStatus',
      render: (rowData) => {
        //console.log(rowData, rowData.APIStatus);
        if (rowData === undefined) {
          return '';
        } else if (
          rowData.APIStatus == null ||
          rowData.APIStatus === 'Inactive'
        ) {
          return <div style={{ color: 'red' }}>Failure</div>;
        } else {
          return <div style={{ color: 'green' }}>Success</div>;
        }
      },
    },
    {
      title: 'Created ',
      field: 'createdOn',
      editable: 'never',
      render: (rowData) => {
        console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.createdOn);
        } else {
          return;
        }
      },
    },
    {
      title: 'Last Modified',
      field: 'lastUpdatedOn',
      editable: 'never',
      render: (rowData) => {
        console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.lastUpdatedOn);
        } else {
          return;
        }
      },
    },
  ];
  useEffect(() => {
    console.log('error', errors);

    if (data !== undefined) {
      console.log(data.fetchDocumentType);
      if (data.fetchDocumentType !== null) {
        setData(data.fetchDocumentType);
        setServerError(false);
      } else {
        console.log('server error');
        setServerError(true);
      }
    } else {
      console.log('server error');
      setServerError(true);
    }
  }, [data, errors]);
  const handleRefetch = (value, message) => {
    refetch({
      updateData(_, data) {
        return data;
      },
    });
    setOpenSnackbar(true);
    setSnackbarMessage(message);
    if (value) {
      setSnackbarVariant('error');
    } else {
      setSnackbarVariant('success');
    }
  };

  const handleAPISuccess = async (Status, aPIMsg) => {
    //console.log('MsgAPI', msgAPI);
    //setOpenSnackbar(true);
    //setSnackbarMessage(aPIMsg);

    if (Status === 'failure' || Status === null) {
      // setSnackbarVariant('error');
      handleRefetch(true, aPIMsg);
    } else {
      // setSnackbarVariant('success');
      handleRefetch(false, aPIMsg);
    }
    // handleRefetch();
  };

  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }
  const classes = makeStyles(iconStyles)();
  const tableIcons = {
    Add: forwardRef((props, ref) => (
      <AddBox color="primary" {...props} ref={ref} />
    )),
    Check: forwardRef((props, ref) => (
      <Check color="primary" {...props} ref={ref} />
    )),
    Clear: forwardRef((props, ref) => (
      <Clear color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline
        style={{ marginLeft: '130%' }}
        color="error"
        {...props}
        ref={ref}
      />
    )),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => (
      <Edit
        style={{ marginLeft: '60%' }}
        {...props}
        ref={ref}
        color="primary"
        onClick={console.log('hello world')}
      />
    )),
    Export: forwardRef((props, ref) => (
      <SaveAlt color="primary" {...props} ref={ref} />
    )),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => (
      <FirstPage color="primary" {...props} ref={ref} />
    )),
    LastPage: forwardRef((props, ref) => (
      <LastPage color="primary" {...props} ref={ref} />
    )),
    NextPage: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft color="primary" {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => (
      <Search color="primary" {...props} ref={ref} />
    )),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };

  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiTypography: {
          h6: {
            fontSize: 14,
            fontFamily: 'Arial !important',
          },
        },
        MuiSelect: {
          icon: {
            color: '#0b153e',
          },
        },
      },
    });
  const handleSearchValue = (val) => {
    console.log(val);
    setSearchValue(val);
  };
  async function handleAddDocumentType(newData) {
    return await new Promise(async (resolve) => {
      resolve();
      console.log(newData);
      await addDocumentType({
        variables: {
          input: {
            name: newData.name,
            description: newData.description,
            docType: newData.docType,
          },
        },
      })
        .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
          console.log(data, error, graphQLErrors, networkError, cacheHit);
          if (data !== null && error == false) {
            handleRefetch(error, 'Saved successfully');
          } else if (error && graphQLErrors.length > 0) {
            let duperror = graphQLErrors[0].message;
            console.log(duperror);
            if (duperror !== null && duperror.indexOf('ER_DUP_ENTRY') !== -1) {
              handleRefetch(error, 'Name already existed');
            }
          }
        })
        .catch((e) => {
          // you can do something with the error here
          console.log(e);
        });
    });
  }
  async function handleRemoveDocumentType(oldData) {
    return await new Promise(async (resolve) => {
      resolve();
      await removeDocumentType({
        variables: {
          input: {
            documentTypeUUID: oldData.documentTypeUUID,
          },
        },
      })
        .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
          console.log(data, error, graphQLErrors, networkError, cacheHit);
          if (data !== null && error == false) {
            handleRefetch(error, 'Deleted successfully');
          } else if (error && graphQLErrors.length > 0) {
            let duperror = graphQLErrors[0].message;
            console.log(duperror);
            if (
              duperror !== null &&
              duperror.indexOf('ER_ROW_IS_REFERENCED') !== -1
            ) {
              setOpenSnackbar(true);
              setSnackbarVariant('error');
              setSnackbarMessage(
                'Selected row is referenced in Organization interface table'
              );
            }
          }
        })
        .catch((e) => {
          // you can do something with the error here
          console.log(e);
        });
    });
  }
  async function handleUpdateDocumentType(newData, oldData) {
    return await new Promise(async (resolve) => {
      resolve();
      console.log(newData);
      await updateDocumentType({
        variables: {
          input: {
            name: newData.name,
            description: newData.description,
            docType: newData.docType,
            documentTypeUUID: oldData.documentTypeUUID,
          },
        },
      })
        .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
          console.log(data, error, graphQLErrors, networkError, cacheHit);
          if (data !== null && error == false) {
            handleRefetch(error, 'Updated successfully');
          } else if (error && graphQLErrors.length > 0) {
            let duperror = graphQLErrors[0].message;
            console.log(duperror);
            if (duperror !== null && duperror.indexOf('ER_DUP_ENTRY') !== -1) {
              handleRefetch(error, 'Name already existed');
            }
          }
        })
        .catch((e) => {
          // you can do something with the error here
          console.log(e);
        });
    });
  }
  const handleCallAPI = async (event, rowData) => {
    console.log('rowData', rowData);
    await fetchAPIdocumentType({
      variables: {
        InputCreate: {
          name: rowData.name,
          description: rowData.description,
          docType: rowData.docType,
          documentTypeUUID: rowData.documentTypeUUID,
        },
      },
    }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
      console.log(data, error, graphQLErrors, networkError, cacheHit);
      if (data !== null && error == false) {
        handleAPISuccess(
          data.DocumnetTypeAPI.statusAPI,
          data.DocumnetTypeAPI.msg
        );
      } else {
        handleRefetch(error, 'graphql hooks error');
      }
    });
  };
  console.log('errors', errors);
  if (loading) return <Loader />;
  if (errors && data.fetchDocumentType === null)
    return <Error type={'Invalid token'} />;
  if (serverError)
    return <Error type={'Server connection lost.Please try agian'} />;
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <MaterialTable
        icons={tableIcons}
        title=" Document "
        columns={columns}
        data={DocumentTypedata}
        //onSearchChange={(e) => handleSearchValue(e)}
        editable={{
          onRowAdd: (newData) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                newData.submitted = true;
                if (
                  !newData.docType ||
                  !newData.name ||
                  !nameexp.test(newData.name)
                ) {
                  setNameError({
                    error: true,
                    label: 'required',
                    helperText: 'Required.',
                    validateInput: true,
                  });
                  reject();
                  return;
                }
                resolve();

                handleAddDocumentType(newData);
              }, 600);
            }),

          onRowUpdate: (newData, oldData) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                newData.submitted = true;
                if (
                  !newData.docType ||
                  !newData.name ||
                  !nameexp.test(newData.name)
                ) {
                  setNameError({
                    error: true,
                    label: 'required',
                    helperText: 'Required.',
                    validateInput: true,
                  });
                  reject();
                  return;
                }
                resolve();

                handleUpdateDocumentType(newData, oldData);
              }, 600);
            }),

          onRowDelete: (oldData) => handleRemoveDocumentType(oldData),
        }}
        actions={[
          (rowData) => {
            console.log(rowData, rowData.APIStatus);
            return rowData.APIStatus == null || rowData.APIStatus == 'Inactive'
              ? {
                  icon: SendIcon,
                  disabled: false,
                  onClick: (event, rowData) => handleCallAPI(event, rowData),
                }
              : {
                  icon: SendIcon,
                  disabled: true,
                  //hidden: true
                };
          },
        ]}
        /* actions={[
          {
            icon: SendIcon,
            tooltip: 'API',
            onClick: (event, rowData) => handleCallAPI(event, rowData),
        
          },
        ]}*/
        options={{
          headerStyle: {
            textAlign: 'center',
            fontSize: 12,
            fontWeight: 'bold',
            fontFamily: 'Arial !important',
            backgroundColor: '#0b153e',
            color: '#ffffff',
            padding: '4px',
          },

          searchFieldStyle: {
            color: '#0b153e',
          },
          actionsColumnIndex: -1,
          pageSize: 10,
          pageSizeOptions: [10, 25, 50, 100],
          toolbar: true,
          paging: true,
        }}
      />
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        autoHideDuration={1500}
        open={openSnackbar}
        onClose={handleCloseSnackbar}
      >
        <MySnackbarContentWrapper
          onClose={handleCloseSnackbar}
          variant={variant}
          message={message}
        />
      </Snackbar>
    </MuiThemeProvider>
  );
}
