import MaterialTable from 'material-table';
import { forwardRef, useEffect } from 'react';
import { useMutation, useQuery, useManualQuery } from 'graphql-hooks';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import ReactDOM from 'react-dom';
import MTableToolbar from 'material-table';
import TextField from '@material-ui/core/TextField';
import React, { Component } from 'react';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import Timestamp from '../timestamp';
import Error from '../components/emptyPage';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import Theme from '../css/theme';
import Loader from '../components/loader';
import CircularIndeterminateLoader from '../components/loader';
import UserForm from './userModelForm';
import UserTableForm from '../components/tmsmodal';
import AddIcon from '@material-ui/icons/Add';
import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles,
} from '@material-ui/core/styles';
function iconStyles() {
  return {};
}

const DISPLAY_All_USER = `{
  allUsers {
    idUser
    userUUID
    userID
    userPrivilage
    loginType
    createdOn
    lastUpdatedOn
  } 
}
`;
const DISPLAY_PAGINATED_USERS = `query fetchUsers($limit:Int,$cursor:Int){
  fetchUsers(limit:$limit,cursor:$cursor){
    edges{
      idUser
    userUUID
    userID
    userPassword
    loginType
    createdOn
    lastUpdatedOn
      }
      pageInfo{
        endCursor
      }
  }
}`;

const ADD_USER = `mutation createUsers($inputCreate:UserInput){
  createUsers(input:$inputCreate)
  {
   tokenVal
  }
}`;
const UPDATE_USER = `mutation updateUsers($input:UserInput){
  updateUsers(input:$input) {
   
    tokenVal
  }
  }`;

const ADD_USER1 = `mutation createUsers($inputCreate:UserInput){
  createUsers(input:$inputCreate)
  {
    idUser
    userUUID
    userID
    userPassword
    loginType
    createdOn
    lastUpdatedOn
  }
}`;
const UPDATE_USER1 = `mutation updateUsers($input:UserInput){
  updateUsers(input:$input) {
    idUser
    userUUID
    userID
    userPassword
    loginType
    createdOn
    lastUpdatedOn
  }
  }`;
const REMOVE_USER = `mutation removeUsers($input:UserInput){
  removeUsers(input:$input) {
    idUser
    userUUID
    userID
    userPassword
    loginType
    createdOn
    lastUpdatedOn
  }
  }
`;
//INSERT INTO `user`(`iduser`, `userUUID`, `userID`, `userPassword`, `loginType`, `createdon`, `usercol`) VALUES (2,"22","24","pwd","admin",NOW(),NOW())
const updatedData = (prevData, data) => ({
  ...data,
  allData: [...prevData.allData, ...data.allData],
});
const limit = 3;
const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(2),
    },
  },

  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));
const styles = (theme) => ({
  root: {
    Width: 1200,
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: '#FFFFFF',
  },
});
export default function UserTable() {
  const [userData, setUserData] = React.useState([]);
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [message, setSnackbarMessage] = React.useState();
  const [variant, setSnackbarVariant] = React.useState('error');
  const [cursorVal, setCursorVal] = React.useState(1);
  const [totalCount, setTotalCount] = React.useState(0);
  const [manualQuery, setManualQuery] = React.useState(0);
  const [Password,setPassword] = React.useState({});
  const [rowData, setrowData] = React.useState();
  const [userForm, setUserForm] = React.useState('update');
  {
    /*const [
    paginatedData,
    { loading: mloading, error: merror, data: mdata, refetch: refetch },
  ] = useManualQuery(DISPLAY_PAGINATED_USERS, {
    variables: {
      limit: limit,
      cursor: 0,
    },
  });*/
  }

  //EDIT user
  const [updateUser, updateloading, updatedata] = useMutation(UPDATE_USER);
  //remove user
  const [removeUser, removeloading, removedata] = useMutation(REMOVE_USER);
  //ADD USER ROLE TYPES
  const [serverError, setServerError] = React.useState(false);
  const [addUser, addloading, adddata] = useMutation(ADD_USER);
  //FETCH ALL USER ROLE TYPES
  //if(manualQuery===0)
  // {
  /*const {
    loading,
    error,
    graphQLErrors,
    networkError,
    data,
    cacheHit
   } = useQuery(DISPLAY_PAGINATED_USERS,{
     variables:{
       limit:limit,
       cursor:cursorVal
     }
   });*/
  //}
  const {
    loading,
    error,
    graphQLErrors,
    networkError,
    data,
    cacheHit,
    refetch,
  } = useQuery(DISPLAY_All_USER);


  const [nameError, setNameError] = React.useState({
    error: false,
    label: "",
    helperText: "",
    validateInput: false,
});
  const useridexp = ( '^[A-Z\s]+[A-Z0-9\s]*$');
  const passwordexp = (   /^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,15}$/);
  const logintypeexp =(  /^[a-zA-Z ]+$/);

  const [openUserModal, setOpenUserModal] = React.useState(false);
  const columns = [
    { title: 'UUID', field: 'userUUID', editable: 'never', hidden: true },
    { title: 'User ID', field: 'userID',
      editComponent: (props) => {
      console.log('editComponent', props, props.value);
      return (
        <div>
          <TextField
            placeholder="User ID"
            margin="normal"
            error={
              !props.value &&
              nameError.validateInput &&
              props.rowData.submitted
                  ? nameError.error
                  : false
          }
          helperText={
            !props.value &&
            nameError.validateInput &&
            props.rowData.submitted
                ? nameError.helperText
                : ""
        }
            fullWidth
            getOptionLabel={(option) => option.name}
            value={props.value}
            onChange={(v) => props.onChange(v.target.value)}
          />
            <div style={{ color: 'red' }}>
            {!props.rowData.userID||
           useridexp.test(props.rowData.userID)
              ? ''
              : 'Start with uppercase letter followed by Alphanumerics'}
          </div>
        </div>
      );
    },
    render: (rowData) => {
      return <div>{rowData.userID}</div>;
    },
  }, 
    { title: 'Password', 
    hidden: true,
     field: 'userPassword',
    editComponent: (props) => {
    console.log('editComponent', props, props.value);
    return (
      <div>
        <TextField
          placeholder="Password"
         type="Password"
         required
          margin="normal"
          error={
            !props.value &&
            nameError.validateInput &&
            props.rowData.submitted
                ? nameError.error
                : false
        }
        helperText={
          !props.value &&
          nameError.validateInput &&
          props.rowData.submitted
              ? nameError.helperText
              : ""
      }
          fullWidth
          getOptionLabel={(option) => option.name}
          value={props.value}
          onChange={(v) => props.onChange(v.target.value)}
        />
          <div style={{ color: 'red' }}>
          {!props.rowData.userPassword||
        passwordexp.test(props.rowData.userPassword)
            ? ''
            : '8 to 15 characters must have atleast one[uppercase,lowercase,special character and number]'}
        </div>
      </div>
    );
  },
  render: (rowData) => {
    return <div>{rowData.userPassword}</div>; 

  },
}, 
//
    { title: 'Login Type', field: 'loginType' ,
    editComponent: (props) => {
    console.log('editComponent', props, props.value);
    
    return (
      <div>
        <TextField
          placeholder="Login Type"
          margin="normal"
          error={
            !props.value &&
            nameError.validateInput &&
            props.rowData.submitted
                ? nameError.error
                : false
        }
        helperText={
          !props.value &&
          nameError.validateInput &&
          props.rowData.submitted
              ? nameError.helperText
              : ""
      }
          fullWidth
          getOptionLabel={(option) => option.name}
          value={props.value}
          onChange={(v) => props.onChange(v.target.value)}
        />

          <div style={{ color: 'red' }}>
          {!props.rowData.loginType  ||
          logintypeexp.test(props.rowData.loginType)
            ? ''
            : 'Enter only alphabets'}
        </div>
      </div>
    );
  },
  render: (rowData) => {
    return <div>{rowData.loginType}</div>;
  },
}, 
{
  title: 'Permissions', 
  field: 'userPrivilage'
  },
    {
      title: 'Created',
      field: 'createdOn',
      editable: 'never',
      render: (rowData) => {
        console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.createdOn);
        } else {
          return;
        }
      },
    },
    {
      title: 'Last Modified',
      field: 'lastUpdatedOn',
      editable: 'never',
      render: (rowData) => {
        console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.lastUpdatedOn);
        } else {
          return;
        }
      },
    },
  ];
  console.log(data);

  const handleNextpage = () => {};
  useEffect(() => {
    if (data !== undefined) {
      console.log(data.allUsers);
      setUserData(data.allUsers);
      // setTotalCount(data.fetchUsers.totalCount);
      // setState({ data: data.allUserTypes });
      /*    setState(prevState => {
        const data1 = [...prevState.data];
        console.log(data1);
        data1.push(data.allUserTypes);
        return { ...prevState, data1 };
      });*/
     setServerError(false);
    }
    else {
      console.log('server error');
      setServerError(true);
    }
  }, [data]);
  const handleRefetch = (value, message,modelClose) => {
    refetch({
      updateData(_, data) {
        return data;
      },
    });
    setOpenSnackbar(true);
    setSnackbarMessage(message);
    if (value) {
      setSnackbarVariant('error');
    } else {
      setSnackbarVariant('success');
    }
    if (modelClose) {
      handleCloseUser();
    }
  };
  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }
  //data:data===undefined?'':data['allUsers'],
  const classes = makeStyles(iconStyles)();
  const tableIcons = {
    Add: forwardRef((props, ref) => (
      <AddBox color="primary" {...props} ref={ref} />
    )),
    Check: forwardRef((props, ref) => (
      <Check color="primary" {...props} ref={ref} />
    )),
    Clear: forwardRef((props, ref) => (
      <Clear color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline color="error" {...props} ref={ref} />
    )),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => (
      <Edit
        {...props}
        ref={ref}
        color="primary"
        onClick={console.log('hello world')}
      />
    )),
    Export: forwardRef((props, ref) => (
      <SaveAlt color="primary" {...props} ref={ref} />
    )),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => (
      <FirstPage color="primary" {...props} ref={ref} />
    )),
    LastPage: forwardRef((props, ref) => (
      <LastPage color="primary" {...props} ref={ref} />
    )),
    NextPage: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft color="primary" {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => (
      <Clear color="primary" {...props} ref={ref} />
    )),
    Search: forwardRef((props, ref) => (
      <Search color="primary" {...props} ref={ref} />
    )),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };

  const [open, setOpen] = React.useState(false);
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSelect:{
          icon:{
            color:'#0b153e'
          }
          },
        
        MuiTypography: {
          h6: {
            fontSize: 14,
            fontFamily: 'Arial !important',
          },
        },
        MuiDialogTitle: {
          root: {
            backgroundColor: '#0b153e',
            color: '#FFFFFF',
          },
        },
        MuiGrid: {
          container: {
            flexWrap: 'nowrap',
          },
        },
      },
    });

  if (loading) return <CircularIndeterminateLoader />;
  if (loading && data !== undefined) return <div>Loading</div>;
 async function handleAddUser(newData) {
    return await new Promise(async (resolve) => {
      resolve();
      console.log(newData);

    await  addUser({
        variables: {
          inputCreate: {
            userID: newData.userID,
            loginType: newData.loginType,
            userPassword: newData.userPassword,
            //confirmPassword: newData.confirmPassword,
          },
        },
      }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
        console.log(data, error, graphQLErrors, networkError, cacheHit);
       if (data !== null && error == false) {
         handleRefetch(error, 'Saved successfully');
       } 
       else if (error && graphQLErrors.length > 0)
         {
          let duperror = graphQLErrors[0].message;
          console.log(duperror);
          if (
            duperror !== null &&
            duperror.indexOf('ER_DUP_ENTRY') !== -1
          ) {
            handleRefetch(error, 'User ID already existed');
          }
        }
     }) 
     .catch((e) => {
       // you can do something with the error here
       console.log(e);
     });
    });
  }
  function handleUpdateUser(newData, oldData) {
    return new Promise((resolve) => {
      resolve();
      console.log(newData);
      updateUser({
        variables: {
          input: {
            userID: newData.userID,
            loginType: newData.loginType,
            userPassword: newData.userPassword,
            userUUID: newData.userUUID,
          },
        },
      }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
        console.log(data, error, graphQLErrors, networkError, cacheHit);
       if (data !== null && error == false) {
         handleRefetch(error, 'Updated successfully');
       } 
       else if (error && graphQLErrors.length > 0)
         {
          let duperror = graphQLErrors[0].message;
          console.log(duperror);
          if (
            duperror !== null &&
            duperror.indexOf('ER_DUP_ENTRY') !== -1
          ) {
            handleRefetch(error, 'User ID already existed');
          }
        }
     })
     .catch((e) => {
       // you can do something with the error here
       console.log(e);
     });
    });
  }
  function handleRemoveUser(oldData) {
    return new Promise((resolve) => {
      resolve();
      removeUser({
        variables: {
          input: {
            userUUID: oldData.userUUID,
          },
        },
      }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
        console.log(data, error, graphQLErrors, networkError, cacheHit);
        if (data !== null && error == false) {
          handleRefetch(error, 'Deleted successfully');
        } else if (error && graphQLErrors.length > 0) {
          let duperror = graphQLErrors[0].message;
          console.log(duperror);
          if (
            duperror !== null &&
            duperror.indexOf('ER_ROW_IS_REFERENCED') !== -1
          ) {
            setOpenSnackbar(true);
            setSnackbarVariant('error');
            setSnackbarMessage('Selected row is referenced in User Role Assignment table');
          }
        }
      })
      .catch((e) => {
        // you can do something with the error here
        console.log(e);
      });
    });
  } 
  const handleClickAddOpen = (e) => {
    setUserForm('Add');
    setOpenUserModal(true);
  };
  const handleClickUpdateOpen = (e, rowData) => {
    console.log(e, rowData);
    setUserForm('Update');
    setrowData(rowData);
    setOpenUserModal(true);
  };
  const handleCloseUser = (e) => {
    setOpenUserModal(false);
  };
  
  if (loading) return <Loader />;
    if (serverError) return <Error type={'Server connection lost.Please try again'} />;
 
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <MaterialTable
        icons={tableIcons}
        title="Users"
        columns={columns}
        data={userData}
        editable={{
         // onRowAdd: (newData) => handleAddUser(newData),z
       /*  onRowAdd: (newData) =>
         new Promise((resolve, reject) => {
             setTimeout(() => {
                 newData.submitted = true;
                 if (!newData.userID||
                   !newData.userPassword||
                   !newData.loginType||
                   ! useridexp.test(newData.userID)||
                   ! passwordexp.test(newData.userPassword)||
                   !logintypeexp.test(newData.loginType)||
                   newData.userPassword !=newData.confirmPassword)
                    {
                     setNameError({
                         error: true,
                         label: "required",
                         helperText: "Required.",
                         validateInput: true,
                     });
                     reject();
                     return;
                 }
                 resolve();
                 handleAddUser(newData); 
             }, 600);
         }),
          onRowUpdate: (newData, oldData) =>
          new Promise((resolve, reject) => {
              setTimeout(() => {
                  newData.submitted = true;
                  if (!newData.userID||
                    !newData.userPassword||
                    !newData.loginType||
                    ! useridexp.test(newData.userID)||
                    ! passwordexp.test(newData.userPassword)||
                    !logintypeexp.test(newData.loginType)||
                    newData.userPassword !=newData.confirmPassword)
                     {
                      setNameError({
                          error: true,
                          label: "required",
                          helperText: "Required.",
                          validateInput: true,
                      });
                      reject();
                      return;
                  }
                  resolve();
                  handleUpdateUser(newData);
              }, 600);
          }),*/
          onRowDelete: (oldData) => handleRemoveUser(oldData),
        }}
        options={{
          headerStyle: {
            textAlign: 'center',
            fontSize: 12,
            fontWeight: 'bold',
            fontFamily: 'Arial !important',
            backgroundColor: '#0b153e',
            color: '#ffffff',
            padding: '4px',
          },
          searchFieldStyle: {
            color: '#0b153e',
          },
          actionsColumnIndex: -1,
          pageSize:10,
          pageSizeOptions: [10, 25 ,50,100 ],
          toolbar: true,
          paging: true
        }}
        actions={[
        {
          icon: AddIcon,
          tooltip: 'Add',
          isFreeAction: true,
          onClick: (event) => handleClickAddOpen(event),
        },
        {
          icon: Edit,
          tooltip: 'Edit',
          onClick: (event, rowData) => handleClickUpdateOpen(event, rowData),
        },
        ]}
      />
       <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        autoHideDuration={1500}
        open={openSnackbar}
        onClose={handleCloseSnackbar}
      >
        <MySnackbarContentWrapper
          onClose={handleCloseSnackbar}
          variant={variant}
          message={message}
        />
      </Snackbar>
      <UserTableForm
        handleCloseModal={handleCloseUser}
        isModalOpen={openUserModal}
      >
        <UserForm
          handleCloseModal={handleCloseUser}
          onSuccess={handleRefetch}
          isModalOpen={openUserModal}
          rowData={rowData}
          title={userForm}
        />
      </UserTableForm>
    </MuiThemeProvider>
  );
}