import React, { useEffect, useContext, useCallback } from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import { useMutation, useQuery } from 'graphql-hooks';
import Snackbar from '@material-ui/core/Snackbar';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Switch from '@material-ui/core/Switch';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { Grid, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import Modal from '@material-ui/core/Modal';
import Theme from '../css/theme';
import '../css/commonStyle.css';
import Input from '@material-ui/core/Input';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import Chip from '@material-ui/core/Chip';
import ReactSelect from '../components/reactSelect';

const ADD_USER = `mutation createUsers($inputCreate:UserInput){
  createUsers(input:$inputCreate)
  {
   tokenVal
  }
}`;
const UPDATE_USER = `mutation updateUsers($input:UserInput){
    updateUsers(input:$input) {
      tokenVal
    }
    }`;
export default function UserModelForm(props) {
  console.log(props);
  const [readValue, setReadOnly] = React.useState(false);
  const [editPassword, setEditPassword] = React.useState(false);
  const [nameError, setNameError] = React.useState(false);
  const [ErrMsg, setErrMsg] = React.useState(false);
  const [updateData, setUpdateData] = React.useState('');
  const [values, setValues] = React.useState({
    userID: '',
    userPassword: '',
    confirmPassword: '',
    loginType: '',
    permission:[],
   
  });
  const [errorInfo, setErrorInfo] = React.useState({
    userID: false,
    userPassword: false,
    confirmPassword: false,
    loginType: false,
    permission:false
  });
  const [opendropdown,setOpendropdown] = React.useState(false);

  const [addUser, addloading, adddata] = useMutation(ADD_USER);
  const [updateUser, updateloading, updatedata] = useMutation(UPDATE_USER);
  useEffect(() => {
    console.log(props.rowData);
    if (props.rowData != undefined && props.title != 'Add') {
      console.log(props.rowData, props.rowData.organizationUnit);
      setValues({
        userID: props.rowData.userID,
        userPassword: '',
        confirmPassword: '',
        loginType: props.rowData.loginType,
        permission:props.rowData.userPrivilage,
      });
      if(values.loginType!='Admin')
      {
      setUpdateData(props.rowData.userPrivilage);
      }
      else
      {
        setUpdateData(''); 
      }
    }
   
  }, [props]);
  const useridexp = /^[A-Z0-9]{5,10}$/;

  const handleChange = (name) => (event) => {
    console.log(event.target.checked,event.target.value, name);
    setErrMsg(false);
    setValues({ ...values, [name]: event.target.value });
    switch (name) {
      case 'userID':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match(useridexp)) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'userPassword':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,15}$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'confirmPassword':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if(event.target.value == values.userPassword)
        {
          setErrorInfo({ ...errorInfo, [name]: false });
        }
        else
        {
         setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
    case 'loginType':
        setValues({
            ...values,
            [event.target.name]: event.target.value,
          });
      
        break;
         /* case 'loginType':
        if (event.target.value.length === 0) {
            setValues({ ...values, [name]: '' });
            setErrorInfo({ ...errorInfo, [name]: false });
          } else if (event.target.value.match('^[a-zA-Z ]*$')) {
            setErrorInfo({ ...errorInfo, [name]: false });
            setValues({ ...values, [name]: event.target.value });
          } else {
            setErrorInfo({ ...errorInfo, [name]: true });
          }
          break;*/
      default:
        return false;
    }
  };
  const handleSubmit = async () => {
    console.log(values,"props.title",props.title);
    setErrMsg(false);
    if (props.title === 'Add') {
      if(
        values.userID!=="" &&
        values.userPassword !=="" &&
        values.confirmPassword!=="" &&
        values.loginType!=="" &&
        errorInfo.userID !== true &&
        errorInfo.userPassword!== true &&
        errorInfo.confirmPassword!== true &&
        ((values.permission.length!==0 && values.loginType=='User')||(values.loginType=='Admin')||(values.loginType=='Arcesb'))
      )
      {console.log("163")
        setErrMsg(false);
        setValues({
          ...values,
          userID: values.userID,
          userPassword: values.userPassword,
          confirmPassword: values.confirmPassword,
          loginType: values.loginType,
          permission:values.permission,
        });
        return await new Promise(async (resolve) => {console.log("173")
          resolve();
          console.log(values);
        await  addUser({
            variables: {
              inputCreate: {
               userID: values.userID,
               loginType: values.loginType,
               userPassword: values.userPassword,
               permission:values.permission.toString(),
              },
            },
          }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
            console.log(data, error, graphQLErrors, networkError, cacheHit);
           if (data !== null && error == false) {
             //handleRefetch(error, 'Saved successfully');
             props.onSuccess(error, 'Saved successfully',true);
           } 
           else if (error && graphQLErrors.length > 0)
             {
              let duperror = graphQLErrors[0].message;
              console.log(duperror);
              if (
                duperror !== null &&
                duperror.indexOf('ER_DUP_ENTRY') !== -1
              ) {
                props.onSuccess(error, 'User ID already exists', false);
               // handleRefetch(error, 'User ID already existed');
              }
            }
         }) 
         .catch((e) => {
           // you can do something with the error here
           console.log(e);
         });
        });
      }else if(!values.userID &&
               !values.userPassword &&
               !values.confirmPassword &&
               !values.loginType 
       )
      {
        setErrMsg(true);
      }
       else
    {
        setErrMsg(false);
        setNameError(true);
      }
    } else{
      console.log(values,editPassword,values.permission);
         if(((values.permission.length!==0 && values.loginType=='User')||(values.loginType=='Admin'))&&
            ((editPassword==false)||
            (editPassword==true&&values.userPassword!==""&&
             values.confirmPassword!==""&&
             errorInfo.userPassword!== true &&
             errorInfo.confirmPassword!== true 
            ))
         )
        {
          console.log(values);
           setErrMsg(false);
            setValues({
              ...values,
              userID: values.userID,
              userPassword: values.userPassword,
              confirmPassword: values.confirmPassword,
              loginType: values.loginType,
              permission:values.permission,
            });
          
            return await  new Promise((resolve) => {
                resolve();
                console.log(values);
                updateUser({
                  variables: {
                    input: {
                      userID: values.userID,
                      loginType: values.loginType,
                      userPassword: values.userPassword,
                      permission:values.permission.toString(),
                      userUUID: props.rowData.userUUID,
                    },
                  },
                }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
                  console.log(data, error, graphQLErrors, networkError, cacheHit);
                 if (data !== null && error == false) {
                   //handleRefetch(error, 'Updated successfully');
                   props.onSuccess(error, 'Updated successfully',true);
                 } 
                 else if (error && graphQLErrors.length > 0)
                   {
                    let duperror = graphQLErrors[0].message;
                    console.log(duperror);
                    if (
                      duperror !== null &&
                      duperror.indexOf('ER_DUP_ENTRY') !== -1
                    ) {
                        props.onSuccess(error, 'User ID already existed',false);
                    }
                  }
               })
               .catch((e) => {
                 // you can do something with the error here
                 console.log(e);
               });
              });
        }else if(  
          (values.permission.length==0||values.permission==undefined&& values.loginType=='User')
          ||(editPassword==true&&(values.userPassword==""||values.confirmPassword==""))
       
         )
        {
          console.log(values,editPassword);
        setNameError(true);
        }else{
          console.log('faile')
        }
    }
  };
  const handleCloseModal = () => {
    props.handleCloseModal(false);
  };
  const handleReset = async () => {
    setErrMsg(false);
    setNameError(false);
    setErrorInfo({
        userID: false,
        userPassword: false,
        confirmPassword: false,
        loginType: false,
        permission:false
    });
    setValues({
      ...values,
      userID: '',
      userPassword: '',
      confirmPassword: '',
      loginType: '',
      permission:[],
    });

  };
  useEffect(() => {
    if (props.title != 'Add') {
      setReadOnly(true);
      console.log('read only' + readValue);
    }
  });
  const handleEdit = async () =>{
    setEditPassword(!editPassword);
    setErrMsg(false);
    setErrorInfo({
        userID: false,
        userPassword: false,
        confirmPassword: false,
        loginType: false,
        permission:false
    });
    if(editPassword==true){
    setValues({
      ...values,
      userPassword: '',
      confirmPassword:''
    });
    setNameError(false);
  }
  }
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiInputLabel: {
          formControl: {
            fontSize: '12px',
          },
        },
        MuiSelect: {
          select: {
            minWidth: 180,
          },
        },
        MuiGrid:{
          item:{
            fontSize:'10px',
          }
        },
      },
    });
      
    function handlePermissions(val) {
        console.log(val)
        console.log("permission", val);
        setValues({ ...values, permission: val }); 
      }

      return (
        <div>
          <MuiThemeProvider theme={getMuiTheme()}>
            <Modal
              aria-labelledby="simple-modal-title"
              aria-describedby="simple-modal-description"
              open={props.isModalOpen}
              onClose={handleCloseModal}
              className="fabmodal"
            >
              <div className="modalpaper">
                <Grid container className="header">
                  <Grid item xs={10} sm={10}>
                    <h2 id="modal-title" className="h2">
                      {props.title == 'Add'
                        ? 'Add User'
                        : 'Update User'}
                    </h2>
                  </Grid>
                  <Grid item xs={2} sm={2} className="close">
                    <CloseIcon onClick={handleCloseModal} />
                  </Grid>
                </Grid>
                <div>
                  {ErrMsg === true ? (
                    <div style={{ textAlign: 'center' }} className="addfieldserror">
                      Please fill the fields
                    </div>
                  ) : (
                    ''
                  )}
                     <Grid className="AddressconfigGrid">
                     <form noValidate autoComplete="off">
                      <Grid container spacing={3}  className="userformcontent " >
                      <Grid item xs={12} sm={6}>
                        <TextField
                          id="outlined-dense"
                          label="User ID"
                          className="partnertextField"
                          margin="dense"
                          variant="outlined"
                          //placeholder=""
                          onChange={handleChange('userID')}
                          value={values.userID}
                          name="userID"
                          InputProps={{
                            readOnly: readValue,
                          }}
                        />
                        {errorInfo.userID === true ? (
                          <div id="nameid" className="addfieldserror">
                            "Allowed Uppercase Alphabets & Numbers.Minimum length is 5 and Maximum is 10"
                          </div>
                        ) : (
                          ''
                        )}
                          {nameError&&!values.userID? (
                            <div id="nameid" className="addfieldserror">
                              Please enter userID.
                            </div>
                          )
                           : ""}
                      </Grid>
                      <Grid item xs={12} sm={6}
                       style={{
                        display: editPassword==false&&props.title != 'Add' ? 'none' : 'block',
                      }}>
                        <TextField
                          id="outlined-dense"
                          label="Password"
                          type="Password"
                          className="partnertextField"
                          margin="dense"
                          variant="outlined"
                          onChange={handleChange('userPassword')}
                          value={values.userPassword}
                          name="userPassword"
                        />
                        {errorInfo.userPassword === true ? (
                          <div id="nameid" className="addfieldserror">
                            "8 to 15 characters must have atleast one[uppercase,lowercase,special character and number]"
                          </div>
                        ) : (
                          ''
                        )}
                            {nameError&&!values.userPassword? (
                            <div id="nameid" className="addfieldserror">
                              Please enter Password.
                            </div>
                          )
                           : ""}
                       <Button style={{marginLeft: "auto",display: "flex",
                                      fontSize:'11px',fontWeight: 400,color:'#FF0000',
                                      display:props.title == 'Add' ? 'none' : 'block',}}
                         onClick={handleEdit}>
                         cancel
                        </Button>
                       </Grid>
                       <Grid item xs={12} sm={6}
                        style={{
                          display: editPassword==false&&props.title != 'Add' ? 'block' :'none' ,
                        }}>
                       <Typography className="typographysubheader">Password </Typography>
                        <Button style={{marginLeft: "auto",display: "flex",fontSize:'11px',fontWeight: 400,
                        }}
                         color="primary"
                         onClick={handleEdit}>
                         Edit
                        </Button>
                      </Grid>
                      <Grid item xs={12} sm={6}
                        style={{
                        display: editPassword==false&&props.title != 'Add'? 'none' : 'block',
                        }}>
                        <TextField
                          id="outlined-dense"
                          label="Confirm Password"
                          type="Password"
                          className="partnertextField"
                          margin="dense"
                          variant="outlined"
                          onChange={handleChange('confirmPassword')}
                          value={values.confirmPassword}
                          name="confirmPassword"
                        />
                        {errorInfo.confirmPassword === true ? (
                          <div id="nameid" className="addfieldserror">
                            "Password Mismatch"
                          </div>
                        ) : (
                          ''
                        )}
                         {nameError&&!values.confirmPassword? (
                            <div id="nameid" className="addfieldserror">
                              Please enter Confirm Password 
                               </div>
                          )
                           : ""}
                        </Grid>
                        <Grid item xs={12} sm={6}
                           gutterBottom
                           variant="body2"
                           color="textSecondary"
                           className="text">
                        <FormControl>
                          <InputLabel
                            style={{
                              align: 'center',
                              color: '#0b153e',
                              fontSize: 12,
                            }}
                            id="demo-simple-select-label"
                          >
                          Login Type
                         </InputLabel>
                         <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={values.loginType}
                            onChange={handleChange('loginType')}
                            name="loginType"
                          >
                            <MenuItem value={'Arcesb'}>Arcesb</MenuItem>
                        <MenuItem value={'Admin'}>Admin</MenuItem>
                        <MenuItem value={'User'}>User</MenuItem>          
                        </Select>
                        </FormControl>
                        {nameError&&!values.loginType? (
                            <div id="nameid" className="addfieldserror">
                              Please Select Login Type
                               </div>
                          )
                           : ""}
                        </Grid>
                        <Grid item xs={12} sm={6}
                           style={{
                            display:values.loginType== 'User' ? 'block' : 'none',
                          }}
                          gutterBottom
                            >
                        <FormControl style={{width:200}}> 
                           <ReactSelect 
                         
                           updateData={values.loginType!='Admin'?updateData:''}
                           style={{color: '#0b153e',}}
                            handlePermissions={handlePermissions}/> 
                        </FormControl>
                        {nameError&&(values.permission==undefined||values.permission.length===0)? (
                            <div id="nameid" className="addfieldserror">
                              Please Select permissions
                               </div>
                          )
                           : ""}
                        </Grid>
                       {/* <Grid item xs={12} sm={6}>
                        <TextField
                          id="outlined-dense"
                          label="Login Type"
                          className="textField"
                          margin="dense"
                          variant="outlined"
                          //placeholder=""
                          onChange={handleChange('loginType')}
                          value={values.loginType}
                          name="loginType"
                        />
                        {errorInfo.loginType === true ? (
                          <div id="nameid" className="addfieldserror">
                            "Enter only alphabets"
                          </div>
                        ) : (
                          ''
                        )}
                      </Grid>*/}
                  </Grid>
                    <Grid container spacing={3}  className="userformcontent ">
                      <Grid item xs={6} sm={6}>
                        <Button
                          style={{
                            color: '#000006',
                            backgroundColor: '#E5CCFF',
                            display: props.title !== 'Add' ? 'none' : 'block',
                          }}
                          onClick={handleReset}
                          variant="contained"
                          fullWidth="true"
                          className="createpartnerbutton"
                        >
                          RESET
                        </Button>
                      </Grid>
                      <Grid item xs={6} sm={6}>
                        <Button
                          onClick={handleSubmit}
                          vvariant="contained"
                          fullWidth="true"
                          className="createpartnerbutton"
                        >
                          {props.title !== 'Add' ? 'UPDATE' : 'SAVE'}
                        </Button>
                      </Grid>
                    </Grid>
                  </form>
                  </Grid>
                </div>
              </div>
            </Modal>
          </MuiThemeProvider>
        </div>
      );
    }
