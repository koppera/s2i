import MaterialTable from 'material-table';
import { forwardRef } from 'react';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import ReactDOM from 'react-dom';
import MTableToolbar  from 'material-table';
import TextField from'@material-ui/core/TextField';
import Dialog from'@material-ui/core/Dialog';
import DialogContent from'@material-ui/core/DialogContent';
import DialogContentText from'@material-ui/core/DialogContentText';
import Fab from'@material-ui/core/Fab';
import AddIcon from'@material-ui/icons/Add';
import CloseIcon from'@material-ui/icons/Close';
import { withStyles } from'@material-ui/core/styles';
import MuiDialogTitle from'@material-ui/core/DialogTitle';
import IconButton from'@material-ui/core/IconButton';
import { Grid, Typography } from '@material-ui/core';
import MuiDialogActions from'@material-ui/core/DialogActions';
import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import Timestamp from '../timestamp';
import Theme from '../css/theme';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import Error from '../components/emptyPage';
import DeleteIcon from '@material-ui/icons/Delete';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles
} from '@material-ui/core/styles';
import { red } from '@material-ui/core/colors';
const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  input: {
    display: 'none',
  },
}));
export default function MaterialTableDemo() {
  
  const [state, setState] = React.useState({
    columns: [
     
      { 
        title: 'Serial Number',
        field: 'serialNumber',
      },
      { 
        title: 'Issuer DN',
        field: 'issuerDN',
    },
    { 
        title: 'Subject',
        field: 'subject',
    },
    { 
        title: 'Expiration Date',
        field: 'expirationDate',
    },
  
    ],
   data:[]
   });
  const [open, setOpen] = React.useState(false);
  const [serverError, setServerError] = React.useState(false);
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const tableIcons = {
    Add: forwardRef((props, ref) => (
      <AddBox color="primary" {...props} ref={ref} />
    )),
    Check: forwardRef((props, ref) => (
      <Check color="primary" {...props} ref={ref} />
    )),
    Clear: forwardRef((props, ref) => (
      <Clear color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline color="error" {...props} ref={ref} />
    )),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => (
      <Edit color="primary" {...props} ref={ref} />
    )),
    Export: forwardRef((props, ref) => (
      <SaveAlt color="primary" {...props} ref={ref} />
    )),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => (
      <FirstPage color="primary" {...props} ref={ref} />
    )),
    LastPage: forwardRef((props, ref) => (
      <LastPage color="primary" {...props} ref={ref} />
    )),
    NextPage: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft color="primary" {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => (
      <Clear color="primary" {...props} ref={ref} />
    )),
    Search: forwardRef((props, ref) => (
      <Search color="primary" {...props} ref={ref} />
    )),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };
  
const getMuiTheme = () =>
  createMuiTheme(Theme, {
    overrides: {
    MuiSelect: {
        icon:{
          color:'#0b153e'
            }
      },
      MuiCheckbox:{ 
        colorSecondary:{
        color: 'grey'
      }},
     MuiCheckbox:{
      root:{
      color:'grey'
     }},
     MuiIconButton:{
     colorInherit:{
      color:'red !important',

     }},
    MuiIconButton:{
    colorSecondary: {
    color:'inherit !important'
    }
    },
    MuiTypography:{h6:{
      fontSize:14,     
      fontFamily:"Arial !important" 
      }},
      MuiDialogTitle: {
      root: {
      backgroundColor:'#0b153e',
      color:'#FFFFFF'
      }},
      MuiGrid:{
      container:{
        flexWrap:'nowrap' 
      }
    },
    }
  });
  return (
  <MuiThemeProvider theme={getMuiTheme()}>
  <MaterialTable
    icons={tableIcons}
    title="Certificate Details"
    columns={state.columns}
    data={state.data}
    editable={{
      onRowAdd: newData =>
        new Promise(resolve => {
          setTimeout(() => {
            resolve();
            setState(prevState => {
              const data = [...prevState.data];
              data.push(newData);
              return { ...prevState, data };
            });
          }, 600);
        }),
      onRowUpdate: (newData, oldData) =>
        new Promise(resolve => {
          setTimeout(() => {
            resolve();
            if (oldData) {
              setState(prevState => {
                const data = [...prevState.data];
                data[data.indexOf(oldData)] = newData;
                return { ...prevState, data };
              });
            }
          }, 600);
        }),
      onRowDelete: oldData =>
        new Promise(resolve => {
          setTimeout(() => {
            resolve();
            setState(prevState => {
              const data = [...prevState.data];
              data.splice(data.indexOf(oldData), 1);
              return { ...prevState, data };
            });
          }, 600);
        }),
    }} 
    options={{
      selection:true,
      color:"#ffffff",
      headerStyle: {
      textAlign: 'center',
      fontSize:12,
      fontWeight:'bold',
      fontFamily:'Arial !important',
      backgroundColor:"#0b153e",
      color:"#ffffff",
      padding:'4px',
     
      },
       searchFieldStyle: {
       color: '#0b153e'
      },
     
      actionsColumnIndex: -1,
      pageSize:5,
      pageSizeOptions: [5,10, 20],
      toolbar: true,
      paging: true,
      
    }}
    actions={[
      {
        tooltip: 'Remove All Selected Rows',
        icon:DeleteOutline,
      }
    ]}
    />           
      </MuiThemeProvider>
      );
   }
