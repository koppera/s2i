import MaterialTable from 'material-table';
import { useMutation, useQuery } from 'graphql-hooks';
import { forwardRef, useEffect } from 'react';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import Error from '../components/emptyPage';
import ReactDOM from 'react-dom';
import MTableToolbar from 'material-table';
import React, { Component } from 'react';
import ModalFab from '../components/modalfabtms';
import Timestamp from '../timestamp';
import Button from '@material-ui/core/Button';
import Theme from '../css/theme';
import { TextField } from '@material-ui/core';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles,
} from '@material-ui/core/styles';
import Loader from '../components/loader';
const DISPLAY_PROJECT_BY_ORGANIZATIONUUID = `query fetchProjectByOrgUUID($organizationUUID: String){
  fetchProjectByOrgUUID(organizationUUID:$organizationUUID) {
    idOrgProject
    projectUUID
    projectName
    projectDescription
    organizationUUID
    createdOn
    updatedOn
  }
}`;
const DISPLAY_All_ORGANIZATION = `{
  allOrganization {
    idOrganization
    organizationUUID
    organizatonName
    organizationDescription
    createdOn
    lastUpdatedOn
  } 
}
`;
const DISPLAY_All_PROJECT = `query fetchProject($searchInput:String){
  fetchProject(searchInput:$searchInput) {
    idOrgProject
    projectUUID
    projectName
    projectDescription
    organizationUUID
    createdOn
    updatedOn
  }
}
`;
const ADD_PROJECT = `mutation createAddress($input:ProjectInput){
  createProject(input:$input) {
    idOrgProject
    projectUUID
    projectName
    projectDescription
    organizationUUID
    createdOn
    updatedOn
  } 
}`;
const UPDATE_PROJECT = `mutation updateProject($input:ProjectInput){
  updateProject(input:$input) {
    idOrgProject
    projectUUID
    projectName
    projectDescription
    organizationUUID
    createdOn
    updatedOn
  } 
  }`;
const REMOVE_PROJECT = `mutation removeProject($input:ProjectInput){
  removeProject(input:$input) {
    idOrgProject
    projectUUID
    projectName
    projectDescription
    organizationUUID
    createdOn
    updatedOn
  } 
}
`;
function iconStyles() {
  return {};
}

export default function ProjectTable() {
  const [searchInput, setSearchValue] = React.useState('');
  const [Projectdata, setData] = React.useState([]);
  //all ORGANIZATION types
  const organizationData = useQuery(DISPLAY_All_ORGANIZATION);
  //Fetch ALL projects

  const { loading, error, data, refetch } = useQuery(DISPLAY_All_PROJECT, {
    variables: {
      searchInput: searchInput,
    },
  });
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [message, setSnackbarMessage] = React.useState();
  const [variant, setSnackbarVariant] = React.useState('error');
  const [serverError, setServerError] = React.useState(false);
  /*
  const [state, setState] = React.useState({
    columns: [
       
        { title: 'Name', field: 'projectName',editable: 'never'},
        { title: 'Description', field: 'projectDescription' },
        { title: 'Organization UUID', field: 'OrganizationUnit' },
        { title: 'Created', field: 'createdOn',editable:'never',
        render : (rowData) => {
          console.log(rowData)
          if(rowData!= undefined){
            return Timestamp(rowData.createdOn)
          }else{
            return 
          }
        }
        },           
        { title: 'Updated', field: 'LastUpdatedOn', editable:'never',
        render : (rowData) => {
          console.log(rowData)
          if(rowData!= undefined){
            return Timestamp(rowData.LastUpdatedOn)
          }else{
            return 
          }
        }
        },         
        { title: 'UID', field: 'ProjectUID',editable: 'never',hidden:true },
      
    ],
    data:[]
  });*/
  //ADD projects
  const [addProject, addloading, adddata] = useMutation(ADD_PROJECT);
  //EDIT projects
  const [updateprojects, updateloading, updatedata] = useMutation(
    UPDATE_PROJECT
  );
  //remove organizaton
  const [removeproject, removeloading, removedata] = useMutation(
    REMOVE_PROJECT
  );
  const [organization, setOrganization] = React.useState({});
  const [nameError, setNameError] = React.useState({
    error: false,
    label: '',
    helperText: '',
    validateInput: false,
  });
  const columns = [
    {
      title: 'Organization',
      field: 'organizationUUID',
     // lookup: organization,
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              //placeholder="Description"
              margin="normal"
              select
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            >
              {organizationData.data != undefined &&
              organizationData.data.allOrganization.length > 0
                ? organizationData.data.allOrganization.map((v, k) => {
                    return (
                      <MenuItem value={v.organizationUUID}>
                        {v.organizatonName}
                      </MenuItem>
                    );
                  })
                : null}
            </TextField>
          </div>
        );
      },
    },
    {
      title: 'Name',
      field: 'projectName',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder="Name"
              margin="normal"
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            />

            <div style={{ color: 'red' }}>
              {!props.rowData.projectName ||
              /^[a-zA-Z\s]+$/.test(props.rowData.projectName)
                ? ''
                : 'Enter only alphabets'}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.projectName}</div>;
      },
    },
    {
      title: 'Description',
      field: 'projectDescription',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder="Description"
              margin="normal"
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            />
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.projectDescription}</div>;
      },
    },

    {
      title: 'Created',
      field: 'createdOn',
      editable: 'never',
      render: (rowData) => {
        console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.createdOn);
        } else {
          return;
        }
      },
    },
    {
      title: 'Last Modified',
      field: 'updatedOn',
      editable: 'never',
      render: (rowData) => {
        console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.updatedOn);
        } else {
          return;
        }
      },
    },
    { title: 'UID', field: 'projectUUID', editable: 'never', hidden: true },
  ];
  useEffect(() => {
    if (data !== undefined) {
      console.log(data.fetchProject);
      setData(data.fetchProject);
      setServerError(false);
    } else {
      console.log('server error');
      setServerError(true);
    }
    if (
      organizationData !== undefined &&
      organizationData.data !== undefined &&
      organizationData.data.allOrganization.length > 0
    ) {
      let organizationDetails = organizationData.data.allOrganization;
      console.log(organizationDetails);

      var objectVal = organizationDetails.reduce(
        (obj, item) => (
          (obj[item.organizationUUID] = item.organizatonName), obj
        ),
        {}
      );
      setOrganization(objectVal);
    }
  }, [data]);
  const handleRefetch = (value, message) => {
    refetch({
      updateData(_, data) {
        return data;
      },
    });
    setOpenSnackbar(true);
    setSnackbarMessage(message);
    if (value) {
      setSnackbarVariant('error');
    } else {
      setSnackbarVariant('success');
    }
  };
  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }
  const classes = makeStyles(iconStyles)();
  const tableIcons = {
    Add: forwardRef((props, ref) => (
      <AddBox color="primary" {...props} ref={ref} />
    )),
    Check: forwardRef((props, ref) => (
      <Check color="primary" {...props} ref={ref} />
    )),
    Clear: forwardRef((props, ref) => (
      <Clear color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline color="error" {...props} ref={ref} />
    )),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => (
      <Edit
        {...props}
        ref={ref}
        color="primary"
        onClick={console.log('hello world')}
      />
    )),
    Export: forwardRef((props, ref) => (
      <SaveAlt color="primary" {...props} ref={ref} />
    )),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => (
      <FirstPage color="primary" {...props} ref={ref} />
    )),
    LastPage: forwardRef((props, ref) => (
      <LastPage color="primary" {...props} ref={ref} />
    )),
    NextPage: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft color="primary" {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => (
      <Search color="primary" {...props} ref={ref} />
    )),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };

  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiTypography: {
          h6: {
            fontSize: 14,
            fontFamily: 'Arial !important',
          },
        },
        MuiSelect: {
          select: {
            minWidth: 150,
          },
          icon: {
            color: '#0b153e',
          },
        },
      },
    });
  async function handleAddProject(newData) {
    return await new Promise(async (resolve) => {
      resolve();
      console.log(newData);
      await addProject({
        variables: {
          input: {
            projectName: newData.projectName,
            projectDescription: newData.projectDescription,
            organizationUUID: newData.organizationUUID,
          },
        },
      })
        .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
          console.log(data, error, graphQLErrors, networkError, cacheHit);
          if (data !== null && error == false) {
            handleRefetch(error, 'Saved successfully');
          } else if (error && graphQLErrors.length > 0) {
            let duperror = graphQLErrors[0].message;
            console.log(duperror);
            if (duperror !== null && duperror.indexOf('ER_DUP_ENTRY') !== -1) {
              handleRefetch(error, 'Name already existed');
            }
          }
        })
        .catch((e) => {
          // you can do something with the error here
          console.log(e);
        });
    });
  }
  async function handleUpdateProject(newData, oldData) {
    return await new Promise(async (resolve) => {
      resolve();
      console.log(newData);
      await updateprojects({
        variables: {
          input: {
            projectName: newData.projectName,
            projectDescription: newData.projectDescription,
            organizationUUID: newData.organizationUUID,
            projectUUID: newData.projectUUID,
          },
        },
      })
        .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
          console.log(data, error, graphQLErrors, networkError, cacheHit);
          if (data !== null && error == false) {
            handleRefetch(error, 'Updated successfully');
          } else if (error && graphQLErrors.length > 0) {
            let duperror = graphQLErrors[0].message;
            console.log(duperror);
            if (duperror !== null && duperror.indexOf('ER_DUP_ENTRY') !== -1) {
              handleRefetch(error, 'Name already existed');
            }
          }
        })
        .catch((e) => {
          // you can do something with the error here
          console.log(e);
        });
    });
  }

  async function handleRemoveProject(oldData) {
    return await new Promise(async (resolve) => {
      resolve();
      await removeproject({
        variables: {
          input: {
            projectUUID: oldData.projectUUID,
          },
        },
      })
        .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
          console.log(data, error, graphQLErrors, networkError, cacheHit);
          if (data !== null && error == false) {
            handleRefetch(error, 'Deleted successfully');
          } else if (error && graphQLErrors.length > 0) {
            let duperror = graphQLErrors[0].message;
            console.log(duperror);
            if (
              duperror !== null &&
              duperror.indexOf('ER_ROW_IS_REFERENCED') !== -1
            ) {
              setOpenSnackbar(true);
              setSnackbarVariant('error');
              setSnackbarMessage(
                'Selected row is referenced in Interface table'
              );
            }
          }
        })
        .catch((e) => {
          // you can do something with the error here
          console.log(e);
        });
    });
  }
  const handleSearchValue = (val) => {
    console.log(val);
    setSearchValue(val);
  };
  if (loading) return <Loader />;
  if (serverError)
    return <Error type={'Server connection lost.Please try again'} />;
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <MaterialTable
        icons={tableIcons}
        title="Project"
        columns={columns}
        data={Projectdata}
        //onSearchChange={(e) => handleSearchValue(e)}
        editable={{
          //onRowAdd: (newData) => handleAddProject(newData),
          onRowAdd: (newData) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                newData.submitted = true;
                if (
                  !newData.projectName ||
                  !newData.organizationUUID ||
                  !/^[a-zA-Z\s]+$/.test(newData.projectName)
                ) {
                  setNameError({
                    error: true,
                    label: 'required',
                    helperText: 'Required.',
                    validateInput: true,
                  });
                  reject();
                  return;
                }
                resolve();
                handleAddProject(newData);
              }, 600);
            }),
          /*onRowUpdate: (newData, oldData) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                newData.submitted = true;
                if (
                  !newData.projectName ||
                  !newData.organizationUUID ||
                  !/^[a-zA-Z\s]+$/.test(newData.projectName)
                ) {
                  setNameError({
                    error: true,
                    label: 'required',
                    helperText: 'Required.',
                    validateInput: true,
                  });
                  reject();
                  return;
                }
                resolve();
                handleUpdateProject(newData, oldData);
              }, 600);
            }),*/
          onRowDelete: (oldData) => handleRemoveProject(oldData),
        }}
        options={{
          headerStyle: {
            textAlign: 'center',
            fontSize: 12,
            fontWeight: 'bold',
            fontFamily: 'Arial !important',
            backgroundColor: '#0b153e',
            color: '#ffffff',
            padding: '4px',
          },

          searchFieldStyle: {
            color: '#0b153e',
          },

          actionsColumnIndex: -1,
          pageSize:10,
          pageSizeOptions: [10, 25 ,50,100 ],
          toolbar: true,
          paging: true
        }}
      />
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        autoHideDuration={1500}
        open={openSnackbar}
        onClose={handleCloseSnackbar}
      >
        <MySnackbarContentWrapper
          onClose={handleCloseSnackbar}
          variant={variant}
          message={message}
        />
      </Snackbar>
    </MuiThemeProvider>
  );
}
