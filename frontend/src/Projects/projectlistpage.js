import React from 'react';
import Fab from '@material-ui/core/Fab';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import AddIcon from '@material-ui/icons/Add';
import '../css/commonStyle.css';
import Modal from '@material-ui/core/Modal';
import Grid from '@material-ui/core/Grid';
import ModalFab from '../components/modalfabtms';
import AddProject from './addproject';
import ModalPage from '../components/tmsmodal';
import EditProject from './editproject';

const useStyles = makeStyles({
  card: {
    minWidth: 100,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});


    
  export default function Orgcards(props) {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [openeditproject, setEditProject] = React.useState(false);
  
    const handleOpen = () => {
        setOpen(true);
    };

    let handleClose = () => {
        setOpen(false);
    } 
    const handleeditproject = () => {
        setEditProject(true);
          };
       
          const handleCloseModal = () => {
            setEditProject(false);
          };

  return (
    <div>  
    <Card className="maincard">
      <CardContent>
        <Typography className="cardprojectname" color="textSecondary" gutterBottom>
         Projects
        </Typography>
      
        <Typography variant="body2" component="p">
        Name of the project
          <br />
          {'"a benevolent smile"'}
        </Typography>
      </CardContent>
      <CardActions className="bgcolor">
        <Button size="small" className="cardbutton" onClick={handleeditproject}>
            <span className="buttontext">Edit</span>
        </Button>
        <Button size="small" className="cardbutton">
            <span className="buttontext">Assign Process</span>
        </Button>
      </CardActions>
    </Card>
    <ModalFab title="Add Project" >
            <AddProject  configData={props.ConfigSettingData} location={props.location} {...props}/>
          </ModalFab>    
          <ModalPage
        handleCloseModal={handleCloseModal}
        title="Edit Project"
        isModalOpen={openeditproject}
      >
        <EditProject
          handleCloseModal={handleCloseModal}
        />
      </ModalPage>
    </div>
    
  );
}