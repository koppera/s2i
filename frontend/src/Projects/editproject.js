import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Tooltip from "@material-ui/core/Tooltip";
import MySnackbarContentWrapper from "../components/Snackbar";
import Snackbar from "@material-ui/core/Snackbar";
import   "../css/commonStyle.css";
import Grid from '@material-ui/core/Grid';
import CloseIcon from "@material-ui/icons/Close";

const useStyles = makeStyles(theme => ({
}));

export default function AddProjectForm(props) {
  const classes = useStyles();
  const [values, setValues] = React.useState({ name: ""});
  const [ErrMsg, setErrMsg] = React.useState(false);
  const [errorInfo, setErrorInfo] = React.useState(
    {
      name:false,
    });
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [message, setSnackbarMessage] = React.useState();
  const [open, setOpen] = React.useState(false);
 

  function handleTooltipClose() {
    setOpen(false);
  }

  function handleTooltipOpen() {
    if (values.name === "") setOpen(true);
  }
  function handleClose() {
    setOpenSnackbar(false);
  }

  const handleModalClose = () => {
    props.handleCloseModal(false);
  };

  const handleChange = name => event => {
    setErrMsg(false);
    setValues({...values,[name]:event.target.value}); 
    setOpen(false)
        if (event.target.value.length === 0) {
            setValues({ ...values, [name]: "" });
            setErrorInfo({...errorInfo,[name]:false});
        }
        else {
        if (event.target.value.match("^([a-zA-Z])[a-zA-Z0-9_]*$") && event.target.value.length <= 25) {
            setErrorInfo({...errorInfo,[name]:false});
            setValues({ ...values, [name]: event.target.value });
        } else {
            setErrorInfo({...errorInfo,[name]:true});
        }
        }
    
 
    }

  const handleSubmit =async () => {
    console.log(values)
    setErrMsg(false);
    if (values.name === "" || Object.values(errorInfo).includes(true)){
      setErrMsg(true);
    } 
    else 
    {
      setErrMsg(false);
      setOpenSnackbar(true);
      setSnackbarMessage('Successfully Saved');
      setValues({ ...values, name: ""});
    }
  };
  

  return (
    <div>
         <Grid container className="header">
        <Grid item xs={10} sm={10}>
          <h2 className="h2">
           Edit Project
          </h2>
        </Grid>
        <Grid item xs={2} sm={2} className="close">
          <CloseIcon onClick={handleModalClose} />
        </Grid>
      </Grid>
      <Grid container spacing={3} className="content">
      {ErrMsg === true ? (
        <div style={{ textAlign: "center" }} className="addfieldserror">
          Please fill the fields
        </div>
      ) : (
        ""
      )}
    
      <TextField
         id="standard-basic"
         label="ID"
         className="textField"
         defaultValue="Sample ID"
         name="ID"
         variant="outlined"
         InputProps={{
             readOnly: true,
           }}
         margin="dense"
         />

        <Tooltip
          title="Starts with alphabets followed by alphanumeric.
         No special characters allowed except underscore'_'."
          onOpen={handleTooltipOpen}
          onClose={handleTooltipClose}
          open={open}
        >
          <TextField
            id="outlined-dense"
            label="Name "
            className="textField"
            margin="dense"
            variant="outlined"
            //placeholder=""
            onChange={handleChange("name")}
            value={values.name}
            name="name"
          />
        </Tooltip>
        {errorInfo.name === true ? (
          <div id="nameid" className="addfieldserror">
            Starts with alphabets followed by alphanumeric. No special
            characters allowed except underscore"_". Maximum length is 25.
          </div>
        ) : (
          ""
        )}
       
        <Button
          onClick={handleSubmit}
          variant="contained"
          fullWidth="true"
          className="createbutton" color="secondary"
        >
         Save
        </Button>
    
      </Grid>
      <Snackbar
            anchorOrigin={{
              vertical: 'top',
              horizontal: 'center'
            }}
            autoHideDuration={5000}
            open={openSnackbar}
            onClose={handleClose}
          >
            <MySnackbarContentWrapper
              onClose={handleClose}
              variant="success"
              message={message}
            />
          </Snackbar>
    </div>
  );
}
