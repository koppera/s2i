import React,{useEffect,useState} from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Axios from 'axios';
import Loader from '../components/loader';

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor:'#4d4d4d',
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
}))(TableRow);



const useStyles = makeStyles({
  table: {
    minWidth: 700,
  }
});

export default function Resources(props) {
  
  const classes = useStyles();
   const [resourcesData,setResourcesData]=useState([]);
  React.useEffect(() => {
    
    const header = { "Content-Type": "application/json", Authorization: `Bearer ${process.env.REACT_APP_POD_TOKEN}` };      
    const fetchData = async () => {
      //console.log("fetchData url3",url3)
      //const url2='https://node.akonlogs.changemaker.community/runningPods';          
      //const url2='http://localhost:5001/runningPods';
      const url2=  process.env.REACT_APP_CLUSTER_NODE_PATH+'/runningPods';       
        await Axios.get(`${url2}`, {
          header,
      }).then(response => {//console.log("response",response)   
      setResourcesData(response);
       })              
    }
    setTimeout(async() => { await fetchData();}, 10000);
});
//console.log("resourcesData",resourcesData)
if(resourcesData.length!==0 )
 { 
   return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell>Pod</StyledTableCell>
            <StyledTableCell >Status</StyledTableCell>
           { /*<StyledTableCell align="right">Fat&nbsp;(g)</StyledTableCell>
            <StyledTableCell align="right">Carbs&nbsp;(g)</StyledTableCell>
            <StyledTableCell align="right">Protein&nbsp;(g)</StyledTableCell>*/}
          </TableRow>
        </TableHead>
        <TableBody>
          {resourcesData.data.map((allDataval) => (
            <StyledTableRow >
              <StyledTableCell>{allDataval.metadata.labels.app}</StyledTableCell>
              <StyledTableCell >{allDataval.status.phase}</StyledTableCell>
            
            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
 }else 
 return <Loader/>
  
}
