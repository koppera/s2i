import MaterialTable from 'material-table';
import { forwardRef } from 'react';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import ReactDOM from 'react-dom';
import MTableToolbar  from 'material-table';
import TextField from'@material-ui/core/TextField';
import Dialog from'@material-ui/core/Dialog';
import DialogContent from'@material-ui/core/DialogContent';
import DialogContentText from'@material-ui/core/DialogContentText';
import Fab from'@material-ui/core/Fab';
import AddIcon from'@material-ui/icons/Add';
import CloseIcon from'@material-ui/icons/Close';
import { withStyles } from'@material-ui/core/styles';
import MuiDialogTitle from'@material-ui/core/DialogTitle';
import IconButton from'@material-ui/core/IconButton';
import { Grid, Typography } from '@material-ui/core';
import MuiDialogActions from'@material-ui/core/DialogActions';
import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import Timestamp from '../timestamp';
import Theme from '../css/theme';
import Error from '../components/emptyPage';
import Loader from '../components/loader';
import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles
} from '@material-ui/core/styles';
function iconStyles() {
  return { 
   
  };
}


export default function MaterialTableDemo() {
  const columns = [
    {
      title: 'Service',
      field: 'Service',
    },
    {
     title: 'Status',
     field: 'Status',
},
  ];
  const [open, setOpen] = React.useState(false);
  const [serverError, setServerError] = React.useState(false);
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  const handleClickOpen = () => {
    setOpen(true);
  };
const classes = makeStyles(iconStyles)();
const tableIcons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear color="error"{...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline  color="error" {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => (<ChevronRight  {...props} ref={ref} />)),
  Edit: forwardRef((props, ref) => (
    <Edit
     
      {...props}
      ref={ref}
      onClick={console.log('hello world')}
    />
  )),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => (
    <ChevronLeft {...props} ref={ref} />
  )),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search  {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => (
    <ArrowDownward {...props} ref={ref} />
  )),
  ThirdStateCheck: forwardRef((props, ref) => (
    <Remove {...props} ref={ref} />
  )),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
};
const getMuiTheme = () =>
  createMuiTheme(Theme, {
    overrides: {
      MuiSvgIcon:{
        root:{
       color:"#0b153e"
      }
      },
      MuiSelect: {
        icon:{
          color:'#0b153e'
            }
      },
      MuiTypography:{h6:{
        fontSize:14,     
        fontFamily:"Arial !important" 
        }},
      MuiDialogTitle: {
        root: {
        
        backgroundColor:'#0b153e',
        color:'#FFFFFF'
                }
        
              },
      MuiGrid:{
      container:{
        flexWrap:'nowrap' 
      }
    },
  
    }
  });
 /* if (loading || addressFormLoader) return <Loader />;
  if (serverError)
    return <Error type={'Sever connection lost.Please try again'} />;*/
  
return (
  <MuiThemeProvider theme={getMuiTheme()}>
  <MaterialTable
   
   icons={tableIcons}
       title="JBoss Resources"
    columns={columns}
    
    options={{
      headerStyle: {
      
      textAlign: 'center',
      fontSize:12,
      fontWeight:'bold',
      fontFamily:'Arial !important',
      backgroundColor:"#0b153e",
      color:"#ffffff",
      padding:'4px',
     
      },
      
        
        searchFieldStyle: {
          color: '#0b153e'
      },
     
      actionsColumnIndex: -1,
      pageSize:10,
      pageSizeOptions: [10, 25 ,50,100 ],
      toolbar: true,
      paging: true
          
      }}
     />
       
      </MuiThemeProvider>
      );
   }
