import React, { useEffect, useContext, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import { useMutation, useQuery } from 'graphql-hooks';
import Snackbar from '@material-ui/core/Snackbar';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Switch from '@material-ui/core/Switch';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import Modal from '@material-ui/core/Modal';
import Theme from '../css/theme';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

const ADD_FTPPROTOCOL = `mutation createFTPProtocol($inputFTP:FTPProtocolInput){
    createFTPProtocol(input:$inputFTP) {
      FTPPort 
      FTPHost 
      FTPuserName 
      FTPPassword 
      FTPTYPEUUID 
      Status 
      Directory 
      partnerProfileUUID 
      certificateUUID 
    }
  }`;
const UPDATE_FTPPROTOCOL = `mutation updateFTPProtocol($input:FTPProtocolInput){
    updateFTPProtocol(input:$input) {
      FTPUUID 
      FTPPort 
      FTPHost 
      FTPuserName 
      FTPPassword 
      FTPTYPEUUID 
      Status 
      Directory 
      partnerProfileUUID 
      certificateUUID 
     
    } 
  }`;

export default function FileTransferProtocol(props) {
  console.log(props);
  console.log(props.proData.protocolName);
  const [updateFTPProtocol, updateloading, updatedata] = useMutation(
    UPDATE_FTPPROTOCOL
  );
  const [
    addFTPProtocol,
    {
      loading: addloading,
      data: pdata,
      errors: perrors,
      networkError: anetworkError,
    },
  ] = useMutation(ADD_FTPPROTOCOL);
  const [typeValue, setTypeValue] = React.useState([]);

  //const classes = useStyles();

  const [values, setValues] = React.useState({
    FTPHost: '',
    FTPPort: '',
    FTPuserName: '',
    FTPPassword: '',
    certificateUUID: null,
    FTPTYPEUUID: '',
    Status: 'Active',
  });
  const [ErrMsg, setErrMsg] = React.useState(false);
  const [errorInfo, setErrorInfo] = React.useState({
    FTPHost: false,
    FTPPort: false,
    FTPuserName: false,
    FTPPassword: false,
    certificateUUID: false,
    FTPTYPEUUID: false,
  });
  const [nameError, setNameError] = React.useState(false);
  const handleChange = (name) => (event) => {
    console.log(event.target.checked, name);
    setErrMsg(false);
    setValues({ ...values, [name]: event.target.value });
    switch (name) {
      case 'FTPHost':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match(/^[a-zA-Z0-9./ ]+(\.\w{2,})+$/)) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'FTPPort':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match(
            '^([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$'
          )
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'FTPuserName':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[a-zA-Z s]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'FTPPassword':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match(
            '^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,15}$'
          )
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'Status':
        if (event.target.checked) {
          setValues({
            ...values,
            [event.target.name]: 'Active',
          });
        } else {
          setValues({
            ...values,
            [event.target.name]: 'Inactive',
          });
        }

        break;
      case 'Directory':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^/+([A-Za-z0-9/]+)$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else setErrorInfo({ ...errorInfo, [name]: true });
        break;
      default:
        return false;
    }
  };
  useEffect(() => {
    console.log(props);
    if (props.data !== undefined && props.data.length > 0) {
      setValues({
        FTPHost: props.data[0].FTPHost,
        FTPPort: props.data[0].FTPPort,
        FTPuserName: props.data[0].FTPuserName,
        FTPPassword: props.data[0].FTPPassword,
        certificateUUID: props.data[0].certificateUUID,
        Directory: props.data[0].Directory,
        Status: props.data[0].Status,
      });
    }
  }, [props]);
  const handleSubmit = async (event) => {
    console.log(event.detail);
    if (!event.detail || event.detail == 1) {
      console.log(values);
      if (
        values.FTPPort !=="" &&
        values.FTPHost !=="" &&
        values.FTPuserName !=="" &&
        values.FTPPassword !=="" &&
        values.Directory !=="" &&
        errorInfo.FTPPort !== true &&
        errorInfo.FTPHost !== true &&
        errorInfo.FTPuserName !== true &&
        errorInfo.FTPPassword !== true &&
        errorInfo.Directory !== true
      ) {
        console.log(values);
        if (props.title == 'AddFTP') {
          addFTPProtocol({
            variables: {
              inputFTP: {
                FTPPort: values.FTPPort,
                FTPHost: values.FTPHost,
                FTPuserName: values.FTPuserName,
                FTPPassword: values.FTPPassword,
                Directory: values.Directory,
                certificateUUID: values.certificateUUID,
                FTPTYPEUUID: props.proData.protocolId,
                Status: values.Status,
                partnerProfileUUID: props.uuid,
              },
            },
          })
            .then(
              ({
                data,
                error,
                graphQLErrors,
                networkError,
                cacheHit,
                loading,
              }) => {
                console.log(
                  data,
                  error,
                  graphQLErrors,
                  networkError,
                  cacheHit,
                  loading
                );
                if (data !== null && error == false) {
                  console.log(' created successfully');
                  setErrMsg(false);

                  setTimeout(() => {
                    props.onSuccess(error, 'Saved successfully', true);
                  }, 1000);
                } else {
                  props.onSuccess(error, 'Graphql hooks error', false);
                }
              }
            )
            .catch((e) => {
              // you can do something with the error here
              console.log(e);
            });
        } else {
          await updateFTPProtocol({
            variables: {
              input: {
                FTPPort: values.FTPPort,
                FTPHost: values.FTPHost,
                FTPuserName: values.FTPuserName,
                FTPPassword: values.FTPPassword,
                Directory: values.Directory,
                certificateUUID: values.certificateUUID,
                Status: values.Status,
                FTPTYPEUUID: props.proData.protocolId,
                partnerProfileUUID: props.uuid,
                FTPUUID: props.data[0].FTPUUID,
              },
            },
          })
            .then(
              ({
                data,
                error,
                graphQLErrors,
                networkError,
                cacheHit,
                loading,
              }) => {
                console.log(
                  data,
                  error,
                  graphQLErrors,
                  networkError,
                  cacheHit,
                  loading
                );
                if (data !== null && error == false) {
                  console.log(' updated successfully');
                  setErrMsg(false);

                  setTimeout(() => {
                    props.onSuccess(error, 'Updated successfully', true);
                  }, 1000);
                } else {
                  props.onSuccess(error, 'Graphql hooks error', false);
                }
              }
            )
            .catch((e) => {
              // you can do something with the error here
              console.log(e);
            });
          props.handleCloseModal(false);
        }
      }
      else if( !values.FTPPort&&
      !values.FTPHost&&
      !values.FTPuserName&&
      !values.FTPPassword&&
      !values.Directory)
      {
        setErrMsg(true);
      }
      else
     if(values.FTPPort=="" ||
      values.FTPHost=="" ||
      values.FTPuserName=="" ||
      values.FTPPassword=="" ||
      values.Directory=="")
      {
        setErrMsg(false);
        setNameError(true);
      }
    }
  };

  const handleReset = async () => {
    setErrMsg(false);
    setErrorInfo({
      FTPHost: false,
      FTPPort: false,
      FTPuserName: false,
      FTPPassword: false,
      Directory: false,
      Status: false,
      certificateUUID: false,
    });
    setValues({
      ...values,
      FTPHost: '',
      FTPPort: '',
      FTPuserName: '',
      Directory: '',
      FTPPassword: '',
      certificateUUID: '',
      Status: false,
    });
  };
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSvgIcon: {
          root: {
            color: '#FFFFFF',
          },
        },
        MuiSelect: {
          select: {
            minWidth: '150px',
          },
        },
      },
    });
  const handleCloseModal = () => {
    props.handleCloseModal(false);
  };
  console.log(values);
  return (
    <div>
      <MuiThemeProvider theme={getMuiTheme()}>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={props.isModalOpen}
          onClose={handleCloseModal}
          className="fabmodal"
        >
          <div className="modalpaper">
            <Grid container className="header">
              <Grid item xs={10} sm={10}>
                {props.title == 'AddFTP' ? (
                  <h2 id="modal-title" className="h2">
                    Add File Transfer Protocol
                  </h2>
                ) : (
                  <h2 id="modal-title" className="h2">
                    Edit File Transfer Protocol
                  </h2>
                )}
              </Grid>
              <Grid item xs={2} sm={2} className="close">
                <CloseIcon onClick={handleCloseModal} />
              </Grid>
            </Grid>
            <div>
              {ErrMsg === true ? (
                <div style={{ textAlign: 'center' }} className="addfieldserror">
                  Please fill the fields
                </div>
              ) : (
                ''
              )}
              <form className="content" noValidate autoComplete="off">
                <Grid className="ftpconfigGrid">
                  <Grid container spacing={3}>
                    <Grid
                      item
                      xs={6}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <TextField
                        id="outlined-dense"
                        label="Host"
                        className="textField"
                        margin="dense"
                        variant="outlined"
                        //placeholder=""
                        onChange={handleChange('FTPHost')}
                        value={values.FTPHost}
                        name="FTPHost"
                      />
                      {errorInfo.FTPHost === true ? (
                        <div id="nameid" className="addfieldserror">
                          "Enter valid host address"
                        </div>
                      ) : (
                        ''
                      )}
                        {nameError&&!values.FTPHost? (
                        <div id="nameid" className="addfieldserror">
                          Please enter host
                        </div>
                      )
                       : ""}
                    </Grid>
                    <Grid
                      item
                      xs={6}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <TextField
                        id="outlined-dense"
                        label="Port "
                        className="textField"
                        margin="dense"
                        variant="outlined"
                        onChange={handleChange('FTPPort')}
                        value={values.FTPPort}
                        name="FTPPort"
                      />
                      {errorInfo.FTPPort === true ? (
                        <div id="nameid" className="addfieldserror">
                          "Allows numbers only between 0 and 65535"
                        </div>
                      ) : (
                        ''
                      )}
                      {nameError&&!values.FTPPort? (
                        <div id="nameid" className="addfieldserror">
                          Please enter port
                        </div>
                      )
                       : ""}
                    </Grid>
                    <Grid
                      item
                      xs={6}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <TextField
                        id="outlined-dense"
                        label="Name"
                        className="textField"
                        margin="dense"
                        variant="outlined"
                        onChange={handleChange('FTPuserName')}
                        value={values.FTPuserName}
                        name="FTPuserName"
                      />
                      {errorInfo.FTPuserName === true ? (
                        <div id="nameid" className="addfieldserror">
                          "Enter only alphabets"
                        </div>
                      ) : (
                        ''
                      )}
                      {nameError&&!values.FTPuserName? (
                        <div id="nameid" className="addfieldserror">
                          Please enter name
                        </div>
                      )
                       : ""}
                    </Grid>
                    <Grid
                      item
                      xs={6}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <TextField
                        id="outlined-dense"
                        type="password"
                        label="Password"
                        className="textField"
                        margin="dense"
                        variant="outlined"
                        onChange={handleChange('FTPPassword')}
                        value={values.FTPPassword}
                        name="FTPPassword"
                      />
                      {errorInfo.FTPPassword === true ? (
                        <div id="nameid" className="addfieldserror">
                          "8 to 15 characters must have atleast
                          one[uppercase,lowercase,special character and number]"
                        </div>
                      ) : (
                        ''
                      )}
                       {nameError&&!values.FTPPassword? (
                        <div id="nameid" className="addfieldserror">
                          Please enter password
                        </div>
                      )
                       : ""}
                    </Grid>

                    <Grid
                      item
                      xs={6}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <TextField
                        id="outlined-dense"
                        label="Directory"
                        className="textField"
                        margin="dense"
                        variant="outlined"
                        onChange={handleChange('Directory')}
                        value={values.Directory}
                        name="Directory"
                      />
                      {errorInfo.Directory === true ? (
                        <div id="nameid" className="addfieldserror">
                          "Enter valid path"
                        </div>
                      ) : (
                        ''
                      )}
                         {nameError&&!values.Directory? (
                        <div id="nameid" className="addfieldserror">
                          Please enter directory
                        </div>
                      )
                       : ""}
                    </Grid>
                    <Grid
                      item
                      xs={6}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <FormControlLabel
                        label="Status"
                        control={
                          <Switch
                            // value={values.Status === 'Active' ? true : false}
                            checked={values.Status === 'Active' ? true : false}
                            onChange={handleChange('Status')}
                            name="Status"
                          />
                        }
                      />
                    </Grid>
                    <Grid
                      item
                      xs={6}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                      style={{
                        display:
                          props.proData.protocolName == 'FTP' ||
                          props.proData.protocolName == 'SFTP'
                            ? 'none'
                            : 'show',
                      }}
                    >
                      <FormControl>
                        <InputLabel
                          style={{ color: '#0b153e', fontSize: 12 }}
                          id="demo-simple-select-label"
                        >
                          Certificate
                        </InputLabel>
                        <Select
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          // value={searchPartner.prefferedProtocolUUID}
                          value={values.certificateUUID}
                          onChange={handleChange('certificateUUID')}
                          name="certificateUUID"
                        >
                          {/* {certificateUUID !== undefined
                            ? certificateUUID.map((v, i) => (
                                <MenuItem key={v.value} value={v.value}>
                                  {v.label}
                                </MenuItem>
                              ))
                            : ''}
                          <MenuItem value={10}>FTP</MenuItem>
                                        <MenuItem value={20}>FTPS</MenuItem>
                                        <MenuItem value={30}>HTTP</MenuItem>
                                          <MenuItem value={10}>HTTPS</MenuItem>*/}
                        </Select>
                      </FormControl>
                    </Grid>
                  </Grid>
                  <Grid container spacing={3}>
                    <Grid
                      item
                      xs={6}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <Button
                        style={{
                          color: '#000006',
                          backgroundColor: '#E5CCFF',
                          display:props.title == 'AddFTP' ? 'SAVE' : 'none'
                        }}
                        onClick={handleReset}
                        variant="contained"
                        fullWidth="true"
                        className="createpartnerbutton"
                      >
                        RESET
                      </Button>
                    </Grid>
                    <Grid
                      item
                      xs={6}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <Button
                        onClick={handleSubmit}
                        vvariant="contained"
                        fullWidth="true"
                        className="createpartnerbutton"
                      >
                        {props.title == 'AddFTP' ? 'SAVE' : 'UPDATE'}
                      </Button>
                    </Grid>
                  </Grid>
                </Grid>
              </form>
            </div>
          </div>
        </Modal>
      </MuiThemeProvider>
    </div>
  );
}
