import MaterialTable from 'material-table';
import { forwardRef, useEffect } from 'react';
import { useMutation, useQuery } from 'graphql-hooks';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import ReactDOM from 'react-dom';
import MTableToolbar from 'material-table';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import CloseIcon from '@material-ui/icons/Close';
import { withStyles } from '@material-ui/core/styles';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import { Grid, Typography } from '@material-ui/core';
import MuiDialogActions from '@material-ui/core/DialogActions';
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Timestamp from '../timestamp';
import Theme from '../css/theme';
import Error from '../components/emptyPage';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import Loader from '../components/loader';
import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles,
} from '@material-ui/core/styles';
function iconStyles() {
  return {};
}

const DISPLAY_All_FTPPROTOCOL = `query fetchFTPProtocol($searchInput:String){
  fetchFTPProtocol(searchInput:$searchInput) {
    FTPUUID 
    FTPPort 
    FTPHost 
    FTPuserName 
    FTPPassword 
    FTPTYPEUUID 
    Status 
    Directory 
    partnerProfileUUID 
    certificateUUID 
    createdOn
    lastUpdatedOn
  } 
}
`;
const ADD_FTPPROTOCOL = `mutation createFTPProtocol($inputFTP:FTPProtocolInput){
  createFTPProtocol(input:$inputFTP) {
    FTPPort 
    FTPHost 
    FTPuserName 
    FTPPassword 
    FTPTYPEUUID 
    Status 
    Directory 
    partnerProfileUUID 
    certificateUUID 
  }
}`;
const UPDATE_FTPPROTOCOL = `mutation updateFTPProtocol($input:FTPProtocolInput){
  updateFTPProtocol(input:$input) {
    FTPUUID 
    FTPPort 
    FTPHost 
    FTPuserName 
    FTPPassword 
    FTPTYPEUUID 
    Status 
    Directory 
    partnerProfileUUID 
    certificateUUID 
   
  } 
}`;
const REMOVE_FTPPROTOCOL = `mutation removeFTPProtocol($input:FTPProtocolInput){
  removeFTPProtocol(input:$input) {
    FTPUUID 
    FTPPort 
    FTPHost 
    FTPuserName 
    FTPPassword 
    FTPTYPEUUID 
    Status 
    Directory 
    partnerProfileUUID 
    certificateUUID 
    createdOn
    lastUpdatedOn
  } 
}
`;
const DISPLAY_PROTOCOL_TYPE = `{
  allProtocolType{
    protocolTypeUUID
    protocolTypeName
  }
}`;

const DISPLAY_FTPPROTOCOL_PARTNERPROFILE = `query fetchFTPProtocolByPartnerProfile($partnerProfileUUID:String){
  fetchFTPProtocolByPartnerProfile(partnerProfileUUID:$partnerProfileUUID) {
    FTPUUID
    FTPPort
    FTPHost
    FTPuserName
    FTPPassword
    FTPTYPEUUID
    Status
    Directory
    partnerProfileUUID
    certificateUUID
    createdOn
    lastUpdatedOn
  } 
}
`;
const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(2),
    },
  },

  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));
const styles = (theme) => ({
  root: {
    Width: 1200,
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: '#FFFFFF',
  },
});
const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

export default function FTPProtocol(props) {
  console.log('props', props);
  const [certificateUUIDData, setCertificateUUIDData] = React.useState([]);
  const [FTPTYPEUUIDData, setFTPTYPEUUIDData] = React.useState([]);
  const [searchInput, setSearchValue] = React.useState('');
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [serverError, setServerError] = React.useState(false);
  const [message, setSnackbarMessage] = React.useState();
  const [variant, setSnackbarVariant] = React.useState('error');
  const [certificateValue, setCertificateValue] = React.useState(false);
  const [nameError, setNameError] = React.useState({
    error: false,
    label: "",
    helperText: "",
    validateInput: false,
});
  const nameexp =(/^[a-zA-Z\s]+$/);
  const pswexp = (/^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,15}$/);
  const pathexp= (/^([A-Za-z0-9\/]+)$/);
  const {
    loading: pTloading,
    data: pTdata,
    errors: pTerrors,
    networkError: pTnetworkError,
  } = useQuery(DISPLAY_PROTOCOL_TYPE);
  const columns = [
    {
      title: 'UUID',
      field: 'FTPUUID',
      editable: 'never',
      hidden: true,
    },
    {
      title: 'Host',
      field: 'FTPHost',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder="Host"
              margin="normal"
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                    ? nameError.error
                    : false
               }
                helperText={
               !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ""
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
              value={props.value}
            />

            <div style={{ color: 'red' }}>
              {!props.rowData.FTPHost||
              /^[a-zA-Z0-9./ ]*$/.test(props.rowData.FTPHost)
                ? ''
                : 'Enter valid host address'}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.FTPHost}</div>;
      },
    },
    {
      title: 'Port',
      field: 'FTPPort',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder="Port"
              margin="normal"
             
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
              value={props.value}
            />

            <div style={{ color: 'red' }}>
              {!props.rowData.FTPPort||
              /^([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$/.test(props.rowData.FTPPort)
                ? '  '
                : 'Allows numbers only between 0 and 65535'}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.FTPPort}</div>;
      },
    },
    {
      title: 'Name',
      field: 'FTPuserName',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder="Name"
              margin="normal"
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                    ? nameError.error
                    : false
               }
                helperText={
               !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ""
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
              value={props.value}
            />

            <div style={{ color: 'red' }}>
              {!props.rowData.FTPuserName||
              nameexp.test(props.rowData.FTPuserName)
                ? ''
                : 'Enter only alphabets'}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.FTPuserName}</div>;
      },
    },
    {
      title: 'Password',
      field: 'FTPPassword',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder="Password"
              input
              type="password"
              margin="normal"
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                    ? nameError.error
                    : false
               }
                helperText={
               !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ""
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
              value={props.value}
            />

            <div style={{ color: 'red' }}>
              {!props.rowData.FTPPassword||pswexp.test(props.rowData.FTPPassword)?
               ' ' : '8 to 15 characters must have atleast one[uppercase,lowercase,special character and number]'}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.FTPPassword}</div>;
      },
    },
    {
      title: 'Certificate',
      field: 'certificateUUID',
      lookup: certificateUUIDData,
      hidden: certificateValue,
    },
    {
      title: 'Directory',
      field: 'Directory',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder="Directory"
              margin="normal"
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                    ? nameError.error
                    : false
               }
                helperText={
               !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ""
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
              value={props.value}
            />

            <div style={{ color: 'red' }}>
              {!props.rowData.Directory||
              pathexp.test(props.rowData.Directory)
                ? ' '
                : 'Enter valid path'}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.Directory}</div>;
      },
    },
    {
      title: 'Type',
      field: 'FTPTYPEUUID',
      lookup: FTPTYPEUUIDData,
      hidden: true,
    },
    {
      title: 'Status',
      //field: 'FileTransferProtocolStatus',
      render: (rowData) => {
        console.log(rowData);
        return (
          <FormControlLabel
            style={{ backgroundColor: 'transparent' }}
            control={
              <Switch
                onChange={props.handler}
                color="primary"
                checked={props.check}
              />
            }
          />
        );
      },
    },

    {
      title: 'Created',
      field: 'createdOn',
      editable: 'never',
      render: (rowData) => {
        console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.createdOn);
        } else {
          return;
        }
      },
    },
    {
      title: 'Last Modified',
      field: 'lastUpdatedOn',
      editable: 'never',
      render: (rowData) => {
        console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.lastUpdatedOn);
        } else {
          return;
        }
      },
    },
  ];
  /* const { loading, error, data, refetch } = useQuery(DISPLAY_All_FTPPROTOCOL, {
    variables: {
      searchInput: searchInput,
    },
  });*/
  const { loading, error, data, refetch } = useQuery(
    DISPLAY_FTPPROTOCOL_PARTNERPROFILE,
    {
      variables: {
        partnerProfileUUID: props.uuid,
      },
    }
  );
  const [
    addFTPProtocol,
    {
      loading: addloading,
      data: pdata,
      errors: perrors,
      networkError: anetworkError,
    },
  ] = useMutation(ADD_FTPPROTOCOL);
  const [FTPProtocolData, setFTPProtocolData] = React.useState([]);
  //EDIT FTPProtocol
  const [updateFTPProtocol, updateloading, updatedata] = useMutation(
    UPDATE_FTPPROTOCOL
  );
  //remove FTPProtocol
  const [removeFTPProtocol, removeloading, removedata] = useMutation(
    REMOVE_FTPPROTOCOL
  );
  const handleRefetch = (value, message) => {
    refetch({
      updateData(_, data) {
        return data;
      },
    });
    setOpenSnackbar(true);
    setSnackbarMessage(message);
    if (value) {
      setSnackbarVariant('error');
    } else {
      setSnackbarVariant('success');
    }
  };
  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }
  useEffect(() => {
    var obj = {};

    if (props.proData !== undefined && props.proData.protocolName == 'FTP') {
      setCertificateValue(true);
    } else {
      setCertificateValue(false);
    }
    if (data !== undefined) {
      console.log(data.fetchFTPProtocolByPartnerProfile);
      setFTPProtocolData(data.fetchFTPProtocolByPartnerProfile);
      setServerError(false);
    } else {
      console.log('server error');
      setServerError(true);
    }
    if (pTdata !== undefined) {
      obj = pTdata.allProtocolType.reduce(function (acc, cur, i) {
        acc[cur.protocolTypeUUID] = cur.protocolTypeName;
        return acc;
      }, {});
      setFTPTYPEUUIDData(obj);
    }
  }, [data, pTdata, props]);
  const [open, setOpen] = React.useState(false);
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const classes = makeStyles(iconStyles)();
  const tableIcons = {
    Add: forwardRef((props, ref) => (
      <AddBox color="primary" {...props} ref={ref} />
    )),
    Check: forwardRef((props, ref) => (
      <Check color="primary" {...props} ref={ref} />
    )),
    Clear: forwardRef((props, ref) => (
      <Clear color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline color="error"z {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline  style={{marginLeft:"10%"}} color="error"  {...props} ref={ref} />
    )),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => (
      <Edit  style={{marginLeft:"60%"}} color="primary" {...props} ref={ref} />
    )),
    Export: forwardRef((props, ref) => (
      <SaveAlt  color="primary" {...props} ref={ref} />
    )),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => (
      <FirstPage color="primary" {...props} ref={ref} />
    )),
    LastPage: forwardRef((props, ref) => (
      <LastPage color="primary" {...props} ref={ref} />
    )),
    NextPage: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft color="primary" {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => (
      <Clear color="primary" {...props} ref={ref} />
    )),
    Search: forwardRef((props, ref) => (
      <Search color="primary" {...props} ref={ref} />
    )),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiDialogTitle: {
          root: {
            backgroundColor: '#0b153e',
            color: '#FFFFFF',
          },
        },
        
        MuiGrid: {
          container: {
            flexWrap: 'nowrap',
          },
        },
      },
    });
  const classes1 = useStyles();

  const handleSearchValue = (val) => {
    console.log(val);
    setSearchValue(val);
  };
  async function handleAddFTPProtocol(newData) {console.log("nd",newData);
    return await new Promise(async (resolve) => {
      // resolve();
      console.log(newData);
      await addFTPProtocol({
        variables: {
          inputFTP: {
            FTPHost: newData.FTPHost,
            FTPPort: newData.FTPPort,
            FTPuserName: newData.FTPuserName,
            FTPPassword: newData.FTPPassword,
            FTPTYPEUUID: props.proData.protocolId,
            Status: newData.Status,
            Directory: newData.Directory,
            partnerProfileUUID: props.uuid,
            certificateUUID: newData.certificateUUI,
          },
        },
      });

      if (addloading.loading) {
        console.log('FTPProtocolnot created');
        handleRefetch(addloading.loading, 'Graphql hooks error');
      } else {
        console.log('FTPProtocol created successfully');
        setTimeout(() => {
          handleRefetch(addloading.loading, 'Saved successfully');
        }, 1000);
      }
    });
  }

  async function handleRemoveFTPProtocol(oldData) {
    return await new Promise(async (resolve) => {
      await removeFTPProtocol({
        variables: {
          input: {
            FTPUUID: oldData.FTPUUID,
          },
        },
      });
      console.log(removeloading, removedata);
      if (removeloading.loading) {
        console.log('FTPProtocol  not deleted');
        handleRefetch(removeloading.loading, 'Graphql hooks error');
      } else {
        console.log('FTPProtocol deleted successfully');
        setTimeout(() => {
          handleRefetch(removeloading.loading, 'Deleted successfully');
        }, 1000);
      }
    });
  }
  async function handleUpdateFTPProtocol(newData, oldData) {
    return await new Promise(async (resolve) => {
      resolve();
      console.log(newData);
      await updateFTPProtocol({
        variables: {
          input: {
            FTPHost: newData.FTPHost,
            FTPPort: newData.FTPPort,
            FTPuserName: newData.FTPuserName,
            FTPPassword: newData.FTPPassword,
            FTPTYPEUUID: props.proData.protocolId,
            Status: newData.Status,
            Directory: newData.Directory,
            partnerProfileUUID: props.uuid,
            certificateUUID: newData.certificateUUI,
            FTPUUID: oldData.FTPUUID,
          },
        },
      });
      if (updateloading.loading) {
        console.log('FTPProtocol not updated');
        handleRefetch(updateloading.loading, 'Graphql hooks error');
      } else {
        console.log('FTPProtocol updated successfully');
        setTimeout(() => {
          handleRefetch(updateloading.loading, 'Updated successfully');
        }, 1000);
      }
    });
  }
  if (loading) return <Loader />;
  if (serverError)
    return <Error type={'Server connection lost.Please try again'} />;
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <MaterialTable
        icons={tableIcons}
        title="File Transfer Protocol"
        columns={columns}
        data={FTPProtocolData}
        //onSearchChange={(e) => handleSearchValue(e)}
        editable={{
          onRowAdd:
            data !== undefined &&
            data.fetchFTPProtocolByPartnerProfile.length != 1
              ? (newData) =>  
              new Promise((resolve, reject) => {
                setTimeout(() => {
                    newData.submitted = true;
                    if (!newData.FTPHost||
                      !/^[a-zA-Z0-9./ ]*$/.test(newData.FTPHost)||
                     (!/^([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$/.test(newData.FTPPort)&&(newData.FTPPort))||
                        !newData.FTPuserName||
                        !nameexp.test(newData.FTPuserName)||
                        !newData.FTPPassword||
                        !pswexp.test(newData.FTPPassword)||
                        !newData.Directory||
                        !pathexp.test(newData.Directory)
                      )
                       {
                        setNameError({
                            error: true,
                            label: "required",
                            helperText: "Required.",
                            validateInput: true,
                        });
                        reject();
                        return;
                    }
                    resolve();
                    handleAddFTPProtocol(newData);
                }, 600);
            })
              : null,
          onRowUpdate: (newData, oldData) =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
                newData.submitted = true;
                if (!newData.FTPHost||
                  !/^[a-zA-Z0-9./ ]*$/.test(newData.FTPHost)||
                  (!/^([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$/.test(newData.FTPPort)&&(newData.FTPPort))||
                  !newData.FTPuserName||
                  !nameexp.test(newData.FTPuserName)||
                  !newData.FTPPassword||
                  !pswexp.test(newData.FTPPassword)||
                  !newData.Directory||
                  !pathexp.test(newData.Directory)
                )
                   {
                    setNameError({
                        error: true,
                        label: "required",
                        helperText: "Required.",
                        validateInput: true,
                    });
                    reject();
                    return;
                }
                resolve();
                handleUpdateFTPProtocol(newData, oldData);
            }, 600);
        }),  
          onRowDelete: (oldData) => handleRemoveFTPProtocol(oldData),
        }}
        options={{
          headerStyle: {
            textAlign: 'center',
            fontSize: 12,
            fontWeight: 'bold',
            fontFamily: 'Arial !important',
            backgroundColor: '#0b153e',
            color: '#ffffff',
            padding: '4px',
          },

          searchFieldStyle: {
            color: '#0b153e',
          },
          actionsColumnIndex: -1,
        }}
      />
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        autoHideDuration={1500}
        open={openSnackbar}
        onClose={handleCloseSnackbar}
      >
        <MySnackbarContentWrapper
          onClose={handleCloseSnackbar}
          variant={variant}
          message={message}
        />
      </Snackbar>
    </MuiThemeProvider>
  );
}
