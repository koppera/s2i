import React, { useEffect, useContext, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useMutation, useQuery } from 'graphql-hooks';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { Grid, Typography, Paper } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { withStyles } from '@material-ui/core/styles';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import FiletransferProtocolForm from '../components/tmsmodal';
import FiletransferProtocolAdd from './fileTransferProtocolForm';
import '../css/commonStyle.css';
import AddIcon from '@material-ui/icons/Add';
import Theme from '../css/theme';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import Fab from '@material-ui/core/Fab';
import DeleteFrom from './deleteProtocol';
import DeletProtocol from '../components/tmsmodal';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import Error from '../components/emptyPage';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import Loader from '../components/loader';

const DISPLAY_FTPPROTOCOL_PARTNERPROFILE = `query fetchFTPProtocolByPartnerProfile($partnerProfileUUID:String){
  fetchFTPProtocolByPartnerProfile(partnerProfileUUID:$partnerProfileUUID) {
    FTPUUID
    FTPPort
    FTPHost
    FTPuserName
    FTPPassword
    FTPTYPEUUID
    Status
    Directory
    partnerProfileUUID
    certificateUUID
    createdOn
    lastUpdatedOn
  } 
}
`;
const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(2),
    },
  },

  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));
const styles = (theme) => ({
  root: {
    Width: 1200,
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: '#FFFFFF',
  },
});

export default function FileTransferProtocalView(props) {
  const [viewopen, setViewForm] = React.useState(false);
  const [deleteOpen, setDeleteOpen] = React.useState(false);
  const [title, setTitle] = React.useState();
  const [certificateUUIDData, setCertificateUUIDData] = React.useState([]);
  const [FTPTYPEUUIDData, setFTPTYPEUUIDData] = React.useState([]);
  const [searchInput, setSearchValue] = React.useState('');
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [serverError, setServerError] = React.useState(false);
  const [message, setSnackbarMessage] = React.useState();
  const [variant, setSnackbarVariant] = React.useState('error');
  const [certificateValue, setCertificateValue] = React.useState(false);
  const [FTPProtocolData, setFTPProtocolData] = React.useState([]);
  const [nameError, setNameError] = React.useState({
    error: false,
    label: '',
    helperText: '',
    validateInput: false,
  });
  const { loading, error, data, refetch } = useQuery(
    DISPLAY_FTPPROTOCOL_PARTNERPROFILE,
    {
      variables: {
        partnerProfileUUID: props.uuid,
      },
    }
  );
  const handelDeleteOpen = () => {
    setDeleteOpen(true);
  };
  const handelDeleteClose = () => {
    setDeleteOpen(false);
    setViewForm(false);
  };
  const handleClickOpen = () => {
    setTitle('AddFTP');
    setViewForm(true);
  };
  const handleClickEdit = () => {
    setTitle('EditFTP');
    setViewForm(true);
  };
  const handleClose = () => {
    setViewForm(false);
  };
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSvgIcon: {
          root: {
            color: '#FFFFFF',
          },
        },
        MuiGrid: {
          container: {
            paddingLeft: '1.5%',
            paddingBottom: '1%',
            overflowX: 'hidden',
          },
          item: {
            fontSize: '10px',
          },
        },
        MuiDialogTitle: {
          root: {
            backgroundColor: '#0b153e',
            color: '#FFFFFF',
          },
        },
      },
    });
  useEffect(() => {
    var obj = {};

    if (data !== undefined) {
      console.log(data.fetchFTPProtocolByPartnerProfile);
      setFTPProtocolData(data.fetchFTPProtocolByPartnerProfile);
      setServerError(false);
    } else {
      console.log('server error');
      setServerError(true);
    }
  }, [data, props.proData.protocolName]);
  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }
  const handleRefetch = (value, message, modelClose) => {
    refetch({
      updateData(_, data) {
        return data;
      },
    });
    setOpenSnackbar(true);
    setSnackbarMessage(message);
    if (value) {
      setSnackbarVariant('error');
    } else {
      setSnackbarVariant('success');
    }
    if (modelClose) {
      handelDeleteClose();
    }
  };
  console.log(FTPProtocolData);
  if (loading) return <Loader />;
  if (serverError)
    return <Error type={'Server connection lost.Please try again'} />;
  return (
    <div>
      <MuiThemeProvider theme={getMuiTheme()}>
        <Grid container spacing={3}  className="content">
          <Grid item xs={10} sm={10}>
            <h2 className="h2" style={{ color: '#0b153e' }}>
              File Transfer Protocol
            </h2>
          </Grid>
          {FTPProtocolData !== undefined && FTPProtocolData.length > 0 ? (
            <Grid item xs={2} sm={2}   className="text">
              <Tooltip title="Edit" onClick={handleClickEdit}>
                <IconButton>
                  <EditIcon style={{ color: '#0b153e', marginTop: '10px' }} />
                </IconButton>
              </Tooltip>
              <Tooltip title="Delete" onClick={handelDeleteOpen}>
                <IconButton>
                  <DeleteIcon style={{ color: 'red', marginTop: '10px' }} />
                </IconButton>
              </Tooltip>
            </Grid>
          ) : (
            ''
          )}
        </Grid>
        {FTPProtocolData !== undefined && FTPProtocolData.length > 0 ? (
          <Paper>
            <Grid container >
              <Grid container spacing={3} className="content">
                <Grid
                  item
                  xs={3}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Host</Typography>
                  {FTPProtocolData[0].FTPHost}
                </Grid>
                <Grid
                  item
                  xs={3}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Port</Typography>
                  {FTPProtocolData[0].FTPPort}
                </Grid>
                <Grid
                  item
                  xs={3}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Name</Typography>
                  {FTPProtocolData[0].FTPuserName}
                </Grid>
                <Grid
                  item
                  xs={3}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Password
                  </Typography>
                  {FTPProtocolData[0].FTPPassword}
                </Grid>
              </Grid>
              <Grid container spacing={3}>
                <Grid
                  item
                  xs={3}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Directory
                  </Typography>
                  {FTPProtocolData[0].Directory}
                </Grid>

                <Grid
                  item
                  xs={3}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Status
                  </Typography>
                  {FTPProtocolData[0].Status}
                </Grid>
                <Grid
                  item
                  xs={3}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                  style={{
                    display:
                      props.proData.protocolName == 'FTP' ||
                      props.proData.protocolName == 'SFTP'
                        ? 'none'
                        : 'block',
                  }}
                >
                  <Typography className="typographysubheader">
                    Certificate
                  </Typography>
                  {FTPProtocolData[0].certificateUUID}
                </Grid>
              </Grid>
            </Grid>
          </Paper>
        ) : (
          <div>
            <center>
              <Paper className="paperorgpage">
                No Protocol...Please add Protocol
              </Paper>
            </center>
          </div>
        )}
        {FTPProtocolData !== undefined && FTPProtocolData.length == 0 ? (
          <Fab
            onClick={handleClickOpen}
            aria-label="Add-Icon"
            className="fab"
            color="primary"
          >
            <AddIcon />
          </Fab>
        ) : (
          ''
        )}
        <FiletransferProtocolForm
          handleCloseModal={handelDeleteClose}
          isModalOpen={viewopen}
        >
          <FiletransferProtocolAdd
            handleCloseModal={handelDeleteClose}
            isModalOpen={viewopen}
            title={title}
            proData={props.proData}
            uuid={props.uuid}
            data={FTPProtocolData}
            onSuccess={handleRefetch}
          />
        </FiletransferProtocolForm>
        <DeletProtocol
          handleCloseModal={handelDeleteClose}
          isModalOpen={deleteOpen}
        >
          <DeleteFrom
            handleCloseModal={handelDeleteClose}
            isModalOpen={deleteOpen}
            data={FTPProtocolData}
            onSuccess={handleRefetch}
          />
        </DeletProtocol>
        <Snackbar
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
          autoHideDuration={1500}
          open={openSnackbar}
          onClose={handleCloseSnackbar}
        >
          <MySnackbarContentWrapper
            onClose={handleCloseSnackbar}
            variant={variant}
            message={message}
          />
        </Snackbar>
      </MuiThemeProvider>
    </div>
  );
}
