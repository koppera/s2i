import MaterialTable from 'material-table';
import { forwardRef, useEffect } from 'react';
import { useMutation, useQuery } from 'graphql-hooks';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import ReactDOM from 'react-dom';
import MTableToolbar from 'material-table';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import CloseIcon from '@material-ui/icons/Close';
import { withStyles } from '@material-ui/core/styles';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import { Grid, Typography } from '@material-ui/core';
import MuiDialogActions from '@material-ui/core/DialogActions';
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Timestamp from '../timestamp';
import Theme from '../css/theme';
import Error from '../components/emptyPage';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import Loader from '../components/loader';
import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles,
} from '@material-ui/core/styles';
function iconStyles() {
  return {};
}

const DISPLAY_PROTOCOL_TYPE = `{
  allProtocolType{
    protocolTypeUUID
    protocolTypeName
  }
}`;
const DISPLAY_All_WTPPROTOCOL_PARTNERPROFILEID = `query fetchAllProtocol($partnerProfileUUID:String){
    fetchAllProtocol(partnerProfileUUID:$partnerProfileUUID) {
      UUID
      Port
      Host
      UserName
      Password
      TypeUUID
      Status
      Directory
      partnerProfileUUID
      certificateUUID
      createdOn
      lastUpdatedOn
    }
  }`;

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(2),
    },
  },

  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));
const styles = (theme) => ({
  root: {
    Width: 1200,
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: '#FFFFFF',
  },
});
const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

export default function WTPPROTOCOL(props) {
  const [certificateUUIDData, setCertificateUUIDData] = React.useState([]);
  const [protocolTypeUUIDData, setprotocolTypeUUIDData] = React.useState([]);
  const [serverError, setServerError] = React.useState(false);
  const [searchInput, setSearchValue] = React.useState('');
  const [certificateValue, setCertificateValue] = React.useState(false);
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [message, setSnackbarMessage] = React.useState();
  const [variant, setSnackbarVariant] = React.useState('error');
  const [nameError, setNameError] = React.useState({
    error: false,
    label: '',
    helperText: '',
    validateInput: false,
  });
  const nameexp = /^[a-zA-Z\s]+$/;
  const pswexp = /^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,15}$/;
  const pathexp = /^([A-Za-z0-9\\]+)$/;
  const {
    loading: pTloading,
    data: pTdata,
    errors: pTerrors,
    networkError: pTnetworkError,
  } = useQuery(DISPLAY_PROTOCOL_TYPE);
  const columns = [
    {
      title: 'UUID',
      field: 'UUID',
      editable: 'never',
      hidden: true,
    },
    {
      title: 'Host',
      field: 'Host',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder="Host"
              margin="normal"
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            />

            <div style={{ color: 'red' }}>
              {!props.rowData.Host ||
              /^[a-zA-Z0-9./ ]*$/.test(props.rowData.Host)
                ? ''
                : 'Enter valid host address'}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.Host}</div>;
      },
    },
    {
      title: 'Port',
      field: 'Port',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder="Port"
              margin="normal"
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
              value={props.value}
            />
            <div style={{ color: 'red' }}>
              {!props.rowData.Port ||
              /^([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$/.test(
                props.rowData.Port
              )
                ? '  '
                : 'Allows numbers only between 0 and 65535'}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.Port}</div>;
      },
    },
    {
      title: 'Name',
      field: 'UserName',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder="Name"
              margin="normal"
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
              value={props.value}
            />
            <div style={{ color: 'red' }}>
              {!props.rowData.UserName || nameexp.test(props.rowData.UserName)
                ? ''
                : 'Enter only alphabets'}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.UserName}</div>;
      },
    },
    {
      title: 'Password',
      field: 'Password',
      /* editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
           
              placeholder="Password"
              input
              type="password"
              margin="normal"
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
              value={props.value}
            />
            <div style={{ color: 'red' }}>
              {!props.rowData.Password || pswexp.test(props.rowData.Password)
                ? ' '
                : '8 to 15 characters must have atleast one[uppercase,lowercase,special character and number]'}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.webTransferProtocolPassword}</div>;
      },*/
    },
    /* {
      title: 'Certificate',
      field: 'TypeUUID',
      lookup: certificateUUIDData,
      hidden: certificateValue,
    },
    {
      title: 'Location',
      field: 'Directory',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
    /* label="addresstypeName"*/
    /*    placeholder="Location"
              margin="normal"
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
              value={props.value}
            />

            <div style={{ color: 'red' }}>
              {!props.rowData.Directory || pathexp.test(props.rowData.Directory)
                ? ' '
                : 'Enter valid path'}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.webTransferProtocolLocation}</div>;
      },
    },*/
    {
      title: 'Type',
      field: 'protocolTypeUUID',
      lookup: protocolTypeUUIDData,
      hidden: true,
    },
    {
      title: 'Status',
      field: 'Status',
      render: (rowData) => {
        console.log(rowData);
        return (
          <FormControlLabel
            style={{ backgroundColor: 'transparent' }}
            control={
              <Switch
                onChange={props.handler}
                color="primary"
                checked={
                  rowData === undefined
                    ? false
                    : rowData.Status === 'Inactive'
                    ? false
                    : true
                }
              />
            }
          />
        );
      },
    },

    {
      title: 'Created',
      field: 'createdOn',
      editable: 'never',
      render: (rowData) => {
        console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.createdOn);
        } else {
          return;
        }
      },
    },
    {
      title: 'Last Modified',
      field: 'lastUpdatedOn',
      editable: 'never',
      render: (rowData) => {
        console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.lastUpdatedOn);
        } else {
          return;
        }
      },
    },
  ];

  const { loading, error, data, refetch } = useQuery(
    DISPLAY_All_WTPPROTOCOL_PARTNERPROFILEID,
    {
      variables: {
        partnerProfileUUID: props.uuid,
      },
    }
  );

  const [WTPProtocolData, setWTPProtocolData] = React.useState([]);

  const handleRefetch = (value, message) => {
    refetch({
      updateData(_, data) {
        return data;
      },
    });
    setOpenSnackbar(true);
    setSnackbarMessage(message);
    if (value) {
      setSnackbarVariant('error');
    } else {
      setSnackbarVariant('success');
    }
  };
  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }
  useEffect(() => {
    var obj = {};
    if (props.proData !== undefined && props.proData.protocolName == 'HTTP') {
      setCertificateValue(true);
    } else {
      setCertificateValue(false);
    }
    console.log();
    if (data !== undefined) {
      console.log(data.fetchAllProtocol);
      setWTPProtocolData(data.fetchAllProtocol);
      setServerError(false);
    } else {
      console.log('server error');
      setServerError(true);
    }
    if (pTdata !== undefined) {
      obj = pTdata.allProtocolType.reduce(function (acc, cur, i) {
        acc[cur.protocolTypeUUID] = cur.protocolTypeName;
        return acc;
      }, {});
      setprotocolTypeUUIDData(obj);
    }
  }, [data, pTdata, props]);
  const [open, setOpen] = React.useState(false);
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const classes = makeStyles(iconStyles)();
  const tableIcons = {
    Add: forwardRef((props, ref) => (
      <AddBox color="primary" {...props} ref={ref} />
    )),
    Check: forwardRef((props, ref) => (
      <Check color="primary" {...props} ref={ref} />
    )),
    Clear: forwardRef((props, ref) => (
      <Clear color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline
        style={{ marginLeft: '10%' }}
        color="error"
        {...props}
        ref={ref}
      />
    )),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => (
      <Edit
        style={{ marginleft: '100%' }}
        color="primary"
        {...props}
        ref={ref}
      />
    )),
    Export: forwardRef((props, ref) => (
      <SaveAlt color="primary" {...props} ref={ref} />
    )),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => (
      <FirstPage color="primary" {...props} ref={ref} />
    )),
    LastPage: forwardRef((props, ref) => (
      <LastPage color="primary" {...props} ref={ref} />
    )),
    NextPage: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft color="primary" {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => (
      <Clear color="primary" {...props} ref={ref} />
    )),
    Search: forwardRef((props, ref) => (
      <Search color="primary" {...props} ref={ref} />
    )),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSelect:{
          icon :{
            color : '#0b153e',
          },
        },
        MuiDialogTitle: {
          root: {
            backgroundColor: '#0b153e',
            color: '#FFFFFF',
          },
        },
        MuiGrid: {
          container: {
            flexWrap: 'nowrap',
          },
        },
      },
    });
  const classes1 = useStyles();

  const handleSearchValue = (val) => {
    console.log(val);
    setSearchValue(val);
  };

  if (loading) return <Loader />;
  if (serverError)
    return <Error type={'Server connection lost.Please try again'} />;
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <MaterialTable
        icons={tableIcons}
        title="All Protocols"
        columns={columns}
        data={WTPProtocolData}
        onSearchChange={(e) => handleSearchValue(e)}
        options={{
          headerStyle: {
            textAlign: 'center',
            fontSize: 12,
            fontWeight: 'bold',
            fontFamily: 'Arial !important',
            backgroundColor: '#0b153e',
            color: '#ffffff',
            padding: '4px',
          },

          searchFieldStyle: {
            color: '#0b153e',
          },
          actionsColumnIndex: -1,
        }}
      />
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        autoHideDuration={1500}
        open={openSnackbar}
        onClose={handleCloseSnackbar}
      >
        <MySnackbarContentWrapper
          onClose={handleCloseSnackbar}
          variant={variant}
          message={message}
        />
      </Snackbar>
    </MuiThemeProvider>
  );
}
