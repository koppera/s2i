import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { useMutation, useQuery } from 'graphql-hooks';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

const DISPLAY_PROTOCOL_TYPE = `{
    allProtocolType{
      protocolTypeUUID
      protocolTypeName
    } 
  }`;

export default function SimpleSelect(props) {
  const classes = useStyles();
  var ddPT = [];
  const [dataProtocolType, setDataProtocolType] = React.useState([]);
  const [dataProtocolTypeObj, setDataProtocolTypeObj] = React.useState([]);
  const {
    loading: pTloading,
    data: pTdata,
    errors: pTerrors,
    networkError: pTnetworkError,
  } = useQuery(DISPLAY_PROTOCOL_TYPE);

  const [valueDD, setValueDD] = React.useState('');

  const handleChange = (event) => {
    setValueDD(event.target.value);
    props.handleProtocolType(
      event.target.value,
      dataProtocolTypeObj[event.target.value]
    );
  };

  useEffect(() => {
    var obj = {};
    if (pTdata !== undefined) {
      obj = pTdata.allProtocolType.reduce(function (acc, cur, i) {
        acc[cur.protocolTypeUUID] = cur.protocolTypeName;
        return acc;
      }, {});
      setDataProtocolTypeObj(obj);

      pTdata.allProtocolType.map((v, i) => {
        ddPT.push({ label: v.protocolTypeName, value: v.protocolTypeUUID });
      });
      setDataProtocolType(ddPT);
    }
  }, [pTdata]);

  return (
    <FormControl className={classes.formControl}>
      <InputLabel id="demo-simple-select-label">Protocol Type</InputLabel>
      <Select
        labelId="demo-simple-select-label"
        id="demo-simple-select"
        value={valueDD}
        onChange={handleChange}
      >
        <MenuItem value="" selected>Select Protocol Type</MenuItem>
        {dataProtocolType !== undefined
          ? dataProtocolType.map((v, i) => (
              <MenuItem value={v.value}>{v.label}</MenuItem>
            ))
          : ''}
      </Select>
      <FormHelperText>Please select Protocol Type</FormHelperText>
    </FormControl>
  );
}
