import React, { useEffect } from 'react';
import { makeStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { useMutation, useQuery } from 'graphql-hooks';
import Button from '@material-ui/core/Button';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import Grid from '@material-ui/core/Grid';
import '../css/commonStyle.css';
import Typography from '@material-ui/core/Typography';
import Theme from '../css/theme';
import { useTheme, createMuiTheme } from '@material-ui/core/styles';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import FTPProtocol from '../filetransferprotocol/filetransferprotocoltable';
import FTPProtocolView from '../filetransferprotocol/fileTransferProtocolView';
import WTPProtocol from '../webtransferprotocol/webtransferprotocoltable';
import WTPProtocolView from '../webtransferprotocol/webTransferProtocolView';
import AllProtocol from './allProtocolTable';
import SimpleSelect from './SimpleSelect';
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
}));

const getMuiTheme = () =>
  createMuiTheme(Theme, {
    overrides: {
      MuiTypography: {
        h6: {
          color: '#0b153e',
        },
      },

      MuiGrid: {
        container: {
          '&$spacing-xs-3': {
            margin: 0,
            '& > $item': {
              padding: '0 !important',
            },
          },
        },
      },
    },
  });
export default function ProtocolTypePanel(props) {
  const classes = useStyles();
  const [dataVal, setDataVal] = React.useState(0);
  const [protocolVal, setProtocolVal] = React.useState({
    protocolId: '',
    protocolName: '',
  });

  const handleProtocolType = (valId, val) => {
    console.log('val', val, 'valId', valId);
    setProtocolVal({
      protocolId: valId,
      protocolName: val,
    });
    // props={...props,"UUID":props.uuid,"protocolId":val}
    if (valId == '') {
      val = '';
      console.log('val', val);
    }
    switch (val) {
      case '':
        setDataVal(0);
        break;
      case 'SFTP':
      case 'FTP':
      case 'FTPS':
        setDataVal(1);
        console.log('valprops', props);
        break;
      case 'HTTP':
      case 'HTTPS':
      case 'WTP':
        setDataVal(2);
        console.log('valprops', props);
        break;
      default:
        return false; // setDataVal(0);
        break;
    }
  };
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <div className={classes.root}>
        <Grid container spacing={3}>
          <Grid item xs={12} style={{ marginLeft: '40%' }}>
            <SimpleSelect handleProtocolType={handleProtocolType} />
          </Grid>
          {dataVal === 0 ? (
            <Grid item xs={12}>
              <AllProtocol proData={protocolVal} uuid={props.uuid} />
            </Grid>
          ) : (
            console.log('0')
          )}
          {dataVal === 1 ? (
            <Grid item xs={12}>
              {/*    <FTPProtocol proData={protocolVal} uuid={props.uuid} />*/}
              <FTPProtocolView proData={protocolVal} uuid={props.uuid} />
            </Grid>
          ) : (
            console.log('1')
          )}
          {dataVal === 2 ? (
            <Grid item xs={12}>
              {/*     <WTPProtocol proData={protocolVal} uuid={props.uuid} />*/}
              <WTPProtocolView proData={protocolVal} uuid={props.uuid} />
            </Grid>
          ) : (
            console.log('2')
          )}
        </Grid>
      </div>
    </MuiThemeProvider>
  );
}
