import React, { useEffect, useContext, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import { useMutation, useQuery } from 'graphql-hooks';
import Snackbar from '@material-ui/core/Snackbar';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Switch from '@material-ui/core/Switch';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import Modal from '@material-ui/core/Modal';
import Theme from '../css/theme';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

const ADD_WTPPROTOCOL = `mutation createWebTransferProtocol($input:WebTransferProtocolInput){
  createWebTransferProtocol(input:$input) {
    webTransferProtocolPort 
    webTransferProtocolHost 
    webTransferProtocolUserName 
    webTransferProtocolPassword 
    protocolTypeUUID 
    status 
    webTransferProtocolLocation 
    partnerProfileID 
    certificateUUID 
  }
}`;
const UPDATE_WTPPROTOCOL = `mutation updateWebTransferProtocol($input:WebTransferProtocolInput){
  updateWebTransferProtocol(input:$input) {
    webTransferProtocolUUID 
    webTransferProtocolPort 
    webTransferProtocolHost 
    webTransferProtocolUserName 
    webTransferProtocolPassword 
    protocolTypeUUID 
    status 
    webTransferProtocolLocation 
    partnerProfileID 
    certificateUUID 
   
  } 
}`;

export default function WebTransferProtocol(props) {
  
  const [updateWTPProtocol, updateloading, updatedata] = useMutation(
    UPDATE_WTPPROTOCOL
  );
  const [
    addWTPProtocol,
    {
      loading: addloading,
      data: pdata,
      errors: perrors,
      networkError: anetworkError,
    },
  ] = useMutation(ADD_WTPPROTOCOL);

  const [typeValue, setTypeValue] = React.useState([]);

  //const classes = useStyles();

  const [values, setValues] = React.useState({
    webTransferProtocolHost: '',
    webTransferProtocolPort: '',
    webTransferProtocolUserName: '',
    webTransferProtocolPassword: '',
    webTransferProtocolLocation: '',
    certificateUUID: null,
    Status: 'Active',
  });
  const [ErrMsg, setErrMsg] = React.useState(false);
  const [errorInfo, setErrorInfo] = React.useState({
    webTransferProtocolHost: false,
    webTransferProtocolPort: false,
    webTransferProtocolUserName: false,
    webTransferProtocolLocation: false,
    webTransferProtocolPassword: false,
    certificateUUID: false,
  });
const [nameError, setNameError] = React.useState(false);
  const handleChange = (name) => (event) => {
    console.log(event.target.checked, name);
    setErrMsg(false);
    setValues({ ...values, [name]: event.target.value });
    switch (name) {
      case 'webTransferProtocolHost':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match(/^[a-zA-Z0-9./ ]+(\.\w{2,})+$/)) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'webTransferProtocolPort':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match(
            '^([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$'
          )
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'webTransferProtocolUserName':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[a-zA-Z s]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'webTransferProtocolPassword':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match(
            '^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,15}$'
          )
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'Status':
        if (event.target.checked) {
          setValues({
            ...values,
            [event.target.name]: 'Active',
          });
        } else {
          setValues({
            ...values,
            [event.target.name]: 'Inactive',
          });
        }
        break;
      case 'webTransferProtocolLocation':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match(/^\\+([A-Za-z0-9\\]+)$/)) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else setErrorInfo({ ...errorInfo, [name]: true });
        break;
      default:
        return false;
    }
  };
  useEffect(() => {
    console.log(props);
    if (props.data !== undefined && props.data.length > 0) {
      setValues({
        webTransferProtocolHost: props.data[0].webTransferProtocolHost,
        webTransferProtocolPort: props.data[0].webTransferProtocolPort,
        webTransferProtocolUserName: props.data[0].webTransferProtocolUserName,
        webTransferProtocolPassword: props.data[0].webTransferProtocolPassword,
        webTransferProtocolLocation: props.data[0].webTransferProtocolLocation,
        certificateUUID: props.data[0].certificateUUID,
        Status: props.data[0].status,
      });
    }
  }, [props]);
  const handleSubmit = async (event) => {
    console.log(event.detail);
    if (!event.detail || event.detail == 1) 
    {
      console.log(values);
      if (
        values.webTransferProtocolPort !=="" &&
        values.webTransferProtocolHost !=="" &&
        values.webTransferProtocolUserName !=="" &&
        values.webTransferProtocolPassword !=="" &&
        values.webTransferProtocolLocation !=="" &&
        errorInfo.webTransferProtocolPort !== true &&
        errorInfo.webTransferProtocolHost !== true &&
        errorInfo.webTransferProtocolUserName !== true &&
        errorInfo.webTransferProtocolPassword !== true &&
        errorInfo.webTransferProtocolLocation !== true
      ){
    
        console.log(values);
        if (props.title == 'AddWebTransferProtocol') {
           addWTPProtocol({
            variables: {
              input: {
                webTransferProtocolHost: values.webTransferProtocolHost,
                webTransferProtocolPort: values.webTransferProtocolPort,
                webTransferProtocolUserName: values.webTransferProtocolUserName,
                webTransferProtocolPassword: values.webTransferProtocolPassword,
                protocolTypeUUID: props.proData.protocolId,
                Status: values.Status,
                webTransferProtocolLocation: values.webTransferProtocolLocation,
                partnerProfileID: props.uuid,
                certificateUUID: values.certificateUUID,
              },
            },
          })
            .then(
              ({
                data,
                error,
                graphQLErrors,
                networkError,
                cacheHit,
                loading,
              }) => {
                 console.log(
                data,
                error,
                graphQLErrors,
                networkError,
                cacheHit,
                loading
              );
                if (data !== null && error == false) {
                console.log(' created successfully');
                  setErrMsg(false);

                  
                  props.onSuccess(error, 'Saved successfully');
                } else {
                  props.onSuccess(error, 'Graphql hooks error');
                }
              }
            )
            .catch((e) => {
              // you can do something with the error here
              console.log(e);
            });
        } else {
          await updateWTPProtocol({
            variables: {
              input: {
                webTransferProtocolHost: values.webTransferProtocolHost,
                webTransferProtocolPort: values.webTransferProtocolPort,
                webTransferProtocolUserName: values.webTransferProtocolUserName,
                webTransferProtocolPassword: values.webTransferProtocolPassword,
                webTransferProtocolLocation: values.webTransferProtocolLocation,
                certificateUUID: values.certificateUUID,
                Status: values.Status,
                protocolTypeUUID: props.proData.protocolId,
                partnerProfileID: props.uuid,
                webTransferProtocolUUID: props.data[0].webTransferProtocolUUID,
              },
            },
          })
            .then(
              ({
                data,
                error,
                graphQLErrors,
                networkError,
                cacheHit,
                loading,
              }) => {
                console.log(
                  data,
                  error,
                  graphQLErrors,
                  networkError,
                  cacheHit,
                  loading
                );
                if (data !== null && error == false) {
                  console.log(' updated successfully');
                  setErrMsg(false);

                  props.onSuccess(error, 'Updated successfully');
                } else {
                  props.onSuccess(error, 'Graphql hooks error');
                }
              }
            )
            .catch((e) => {
              // you can do something with the error here
              console.log(e);
            });
            props.handleCloseModal(false);
        }
       }
       else if(  values.webTransferProtocolPort =="" &&
                 values.webTransferProtocolHost =="" &&
                 values.webTransferProtocolUserName =="" &&
                 values.webTransferProtocolPassword =="" &&
                 values.webTransferProtocolLocation =="" )
        {
          setErrMsg(true);
        }
        else
       if( values.webTransferProtocolPort =="" ||
       values.webTransferProtocolHost =="" ||
       values.webTransferProtocolUserName =="" ||
       values.webTransferProtocolPassword =="" ||
       values.webTransferProtocolLocation =="")
        {
          setErrMsg(false);
          setNameError(true);
        }
      }
    };
  

  const handleReset = async () => {
    setErrMsg(false);
    setNameError(false);
    setErrorInfo({
      webTransferProtocolPort: false,
      webTransferProtocolHost: false,
      webTransferProtocolUserName: false,
      webTransferProtocolPassword: false,
      certificateUUID: false,
      webTransferProtocolLocation: false,
    });
    setValues({
      ...values,
      webTransferProtocolHost: '',
      webTransferProtocolPort: '',
      webTransferProtocolUserName: '',
      webTransferProtocolPassword: '',
      webTransferProtocolLocation: '',
      certificateUUID: '',
      Status: false,
    });
  };
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSvgIcon: {
          root: {
            color: '#FFFFFF',
          },
        },
        MuiSelect: {
          select: {
            minWidth: '150px',
          },
        },
      },
    });
    const handleCloseModal = () => {
      props.handleCloseModal(false);
    };
    console.log(values);
  return (
    <div>
      <MuiThemeProvider theme={getMuiTheme()}>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={props.isModalOpen}
          onClose={handleCloseModal}
          className="fabmodal"
        >
          <div className="modalpaper">
            <Grid container className="header">
              <Grid item xs={10} sm={10}>
                {props.title == 'AddWebTransferProtocol' ? (
                  <h2 id="modal-title" className="h2">
                    Add Web Transfer Protocol
                  </h2>
                ) : (
                  <h2 id="modal-title" className="h2">
                    Edit Web Transfer Protocol
                  </h2>
                )}
              </Grid>
              <Grid item xs={2} sm={2} className="close">
                <CloseIcon onClick={handleCloseModal} />
              </Grid>
            </Grid>
            <div>
              <div>
                {ErrMsg === true ? (
                  <div
                    style={{ textAlign: 'center' }}
                    className="addfieldserror"
                  >
                    Please fill the fields
                  </div>
                ) : (
                  ''
                )}
                <form className="content" noValidate autoComplete="off">
                  <Grid className="wtpconfigGrid">
                    <Grid container spacing={3}>
                      <Grid
                        item
                        xs={6}
                        sm={6}
                        gutterBottom
                        variant="body2"
                        color="textSecondary"
                        className="text"
                      >
                        <TextField
                          id="outlined-dense"
                          label="Host"
                          className="textField"
                          margin="dense"
                          variant="outlined"
                          //placeholder=""
                          onChange={handleChange('webTransferProtocolHost')}
                          value={values.webTransferProtocolHost}
                          name="webTransferProtocolHost"
                        />
                        {errorInfo.webTransferProtocolHost === true ? (
                          <div id="nameid" className="addfieldserror">
                            "Enter valid host address"
                          </div>
                        ) : (
                          ''
                        )}
                         {nameError&&!values.webTransferProtocolHost? (
                        <div id="nameid" className="addfieldserror">
                          Please enter host
                        </div>
                      )
                       : ""}

                      </Grid>
                      <Grid
                        item
                        xs={6}
                        sm={6}
                        gutterBottom
                        variant="body2"
                        color="textSecondary"
                        className="text"
                      >
                        <TextField
                          id="outlined-dense"
                          label="Port "
                          className="textField"
                          margin="dense"
                          variant="outlined"
                          onChange={handleChange('webTransferProtocolPort')}
                          value={values.webTransferProtocolPort}
                          name="webTransferProtocolPort"
                        />
                        {errorInfo.webTransferProtocolPort === true ? (
                          <div id="nameid" className="addfieldserror">
                            "Allows numbers only between 0 and 65535"
                          </div>
                        ) : (
                          ''
                        )}
                        {nameError&&!values.webTransferProtocolPort? (
                        <div id="nameid" className="addfieldserror">
                          Please enter port
                        </div>
                      )
                       : ""}

                      </Grid>
                      <Grid
                        item
                        xs={6}
                        sm={6}
                        gutterBottom
                        variant="body2"
                        color="textSecondary"
                        className="text"
                      >
                        <TextField
                          id="outlined-dense"
                          label="Name"
                          className="textField"
                          margin="dense"
                          variant="outlined"
                          onChange={handleChange('webTransferProtocolUserName')}
                          value={values.webTransferProtocolUserName}
                          name="webTransferProtocolUserName"
                        />
                        {errorInfo.webTransferProtocolUserName === true ? (
                          <div id="nameid" className="addfieldserror">
                            "Enter only alphabets"
                          </div>
                        ) : (
                          ''
                        )}
                          {nameError&&!values.webTransferProtocolUserName? (
                        <div id="nameid" className="addfieldserror">
                          Please enter name
                        </div>
                      )
                       : ""}

                      </Grid>
                      <Grid
                        item
                        xs={6}
                        sm={6}
                        gutterBottom
                        variant="body2"
                        color="textSecondary"
                        className="text"
                      >
                        <TextField
                          id="outlined-dense"
                          type="password"
                          label="Password"
                          className="textField"
                          margin="dense"
                          variant="outlined"
                          onChange={handleChange('webTransferProtocolPassword')}
                          value={values.webTransferProtocolPassword}
                          name="webTransferProtocolPassword"
                        />
                        {errorInfo.webTransferProtocolPassword === true ? (
                          <div id="nameid" className="addfieldserror">
                            "8 to 15 characters must have atleast
                            one[uppercase,lowercase,special character and
                            number]"
                          </div>
                        ) : (
                          ''
                        )}
                         {nameError&&!values.webTransferProtocolPassword? (
                        <div id="nameid" className="addfieldserror">
                          Please enter Password
                        </div>
                      )
                       : ""}
                      </Grid>
                      <Grid
                        item
                        xs={6}
                        sm={6}
                        gutterBottom
                        variant="body2"
                        color="textSecondary"
                        className="text"
                      >
                        <TextField
                          id="outlined-dense"
                          label="Location"
                          className="textField"
                          margin="dense"
                          variant="outlined"
                          onChange={handleChange('webTransferProtocolLocation')}
                          value={values.webTransferProtocolLocation}
                          name="webTransferProtocolLocation"
                        />
                        {errorInfo.webTransferProtocolLocation === true ? (
                          <div id="nameid" className="addfieldserror">
                            "Enter valid path"
                          </div>
                        ) : (
                          ''
                        )}
                         {nameError&&!values.webTransferProtocolLocation? (
                        <div id="nameid" className="addfieldserror">
                          Please enter location
                        </div>
                      )
                       : ""}
                      </Grid>
                      <Grid
                        item
                        xs={6}
                        sm={6}
                        gutterBottom
                        variant="body2"
                        color="textSecondary"
                        className="text"
                      >
                        <FormControlLabel
                          label="Status"
                          control={
                            <Switch
                              checked={
                                values.Status === 'Active' ? true : false
                              }
                              onChange={handleChange('Status')}
                              name="Status"
                            />
                          }
                        />
                      </Grid>
                    </Grid>
                    <Grid
                      item
                      xs={6}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                      style={{
                        display:
                          props.proData.protocolName == 'HTTP' ||
                          props.proData.protocolName == 'WTP'
                            ? 'none'
                            : 'show',
                      }}
                    >
                      <FormControl>
                        <InputLabel
                          style={{ color: '#0b153e', fontSize: 12 }}
                          id="demo-simple-select-label"
                        >
                          Certificate
                        </InputLabel>
                        <Select
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          // value={searchPartner.prefferedProtocolUUID}
                          value={values.certificateUUID}
                          onChange={handleChange('certificateUUID')}
                          name="certificateUUID"
                        >
                          {/* {certificateUUID !== undefined
                            ? certificateUUID.map((v, i) => (
                                <MenuItem key={v.value} value={v.value}>
                                  {v.label}
                                </MenuItem>
                              ))
                            : ''}
                          <MenuItem value={10}>FTP</MenuItem>
                                        <MenuItem value={20}>FTPS</MenuItem>
                                        <MenuItem value={30}>HTTP</MenuItem>
                                          <MenuItem value={10}>HTTPS</MenuItem>*/}
                        </Select>
                      </FormControl>
                    </Grid>
                    <Grid container spacing={3}>
                      <Grid
                        item
                        xs={6}
                        sm={6}
                        gutterBottom
                        variant="body2"
                        color="textSecondary"
                        className="text"
                      >
                        <Button
                          style={{
                            display:props.title == 'AddWebTransferProtocol'
                            ? 'block'
                            : 'none',
                            color: '#000006',
                            backgroundColor: '#E5CCFF',
                          }}
                          onClick={handleReset}
                          variant="contained"
                          fullWidth="true"
                          className="createpartnerbutton"
                        >
                          RESET
                        </Button>
                      </Grid>
                      <Grid
                        item
                        xs={6}
                        sm={6}
                        gutterBottom
                        variant="body2"
                        color="textSecondary"
                        className="text"
                      >
                        <Button
                          onClick={handleSubmit}
                          vvariant="contained"
                          fullWidth="true"
                          className="createpartnerbutton"
                        >
                          {props.title == 'AddWebTransferProtocol'
                            ? 'SAVE'
                            : 'UPDATE'}
                        </Button>
                      </Grid>
                    </Grid>
                  </Grid>
                </form>
              </div>
            </div>
          </div>
        </Modal>
      </MuiThemeProvider>
    </div>
  );
}
