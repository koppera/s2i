import React, { useEffect, useContext, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useMutation, useQuery } from 'graphql-hooks';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { Grid, Typography, Paper } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { withStyles } from '@material-ui/core/styles';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import ModalFab from '../components/modalfabtms';
import '../css/commonStyle.css';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import Theme from '../css/theme';
import WebTransferProtocolForm from '../components/tmsmodal';
import WebTransferProtocolAdd from './webTransferrProtocolForm';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import DeleteFrom from './deleteView';
import Fab from '@material-ui/core/Fab';
import Tooltip from '@material-ui/core/Tooltip';
import DeleteView from '../components/tmsmodal';
import Error from '../components/emptyPage';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import Loader from '../components/loader';
const DISPLAY_All_WTPPROTOCOL_PARTNERPROFILEID = `query fetchWebTransferProtocolByPartnerProfile($partnerProfileID:String){
  fetchWebTransferProtocolByPartnerProfile(partnerProfileID:$partnerProfileID) {
    webTransferProtocolUUID
    webTransferProtocolPort
    webTransferProtocolHost
    webTransferProtocolUserName
    webTransferProtocolPassword
    protocolTypeUUID
    status
    webTransferProtocolLocation
    partnerProfileID
    certificateUUID
    createdOn
    lastUpdatedOn
  } 
}`;

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(2),
    },
  },

  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));
const styles = (theme) => ({
  root: {
    Width: 1200,
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: '#FFFFFF',
  },
});
export default function WebTransferProtocalView(props) {
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSelect: {
          select: {
            minWidth: 150,
          },
        },
        MuiSvgIcon: {
          root: {
            color: '#FFFFFF',
          },
        },

        MuiGrid: {
          container: {
            paddingLeft: '1.5%',
            paddingBottom: '1%',
            overflowX: 'hidden',
          },
          item: {
            fontSize: '10px',
          },
        },
        MuiDialogTitle: {
          root: {
            backgroundColor: '#0b153e',
            color: '#FFFFFF',
          },
        },
      },
    });
  const handleModalClose = () => {
    props.handleCloseModal(false);
  };

  const [viewopen, setViewForm] = React.useState(false);
  const [title, setTitle] = React.useState();
  const [certificateUUIDData, setCertificateUUIDData] = React.useState([]);
  const [protocolTypeUUIDData, setprotocolTypeUUIDData] = React.useState([]);
  const [serverError, setServerError] = React.useState(false);
  const [searchInput, setSearchValue] = React.useState('');
  const [certificateValue, setCertificateValue] = React.useState(false);
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [message, setSnackbarMessage] = React.useState();
  const [variant, setSnackbarVariant] = React.useState('error');
  const [WTPProtocolData, setWTPProtocolData] = React.useState([]);
  const { loading, error, data, refetch } = useQuery(
    DISPLAY_All_WTPPROTOCOL_PARTNERPROFILEID,
    {
      variables: {
        partnerProfileID: props.uuid,
      },
    }
  );
  const handleClickOpen = () => {
    setTitle('AddWebTransferProtocol');
    setViewForm(true);
  };
  const handleClickEdit = () => {
    setTitle('EditWebTransferProtocol');
    setViewForm(true);
  };
  const handleClose = () => {
    setViewForm(false);
  };
  const [deleteOpen, setDeleteOpen] = React.useState(false);

  const handelDeleteOpen = () => {
    setDeleteOpen(true);
  };
  const handelDeleteClose = () => {
    setDeleteOpen(false);
    setViewForm(false);
  };
  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }
  useEffect(() => {
    if (data !== undefined) {
      console.log(data.fetchWebTransferProtocolByPartnerProfile);
      setWTPProtocolData(data.fetchWebTransferProtocolByPartnerProfile);
      setServerError(false);
    } else {
      console.log('server error');
      setServerError(true);
    }
  }, [data]);
  const handleRefetch = (value, message, modelClose) => {
    handleClose();
    refetch({
      updateData(_, data) {
        return data;
      },
    });
    setOpenSnackbar(true);
    setSnackbarMessage(message);
    if (value) {
      setSnackbarVariant('error');
    } else {
      setSnackbarVariant('success');
    }
    if (modelClose) {
      handelDeleteClose();
    }
  };
  if (loading) return <Loader />;
  if (serverError)
    return <Error type={'Server connection lost.Please try again'} />;
  return (
    <div>
      <MuiThemeProvider theme={getMuiTheme()}>
        <Grid container spacing={3} className="content">
          <Grid item xs={10} sm={10}>
            <h2 className="h2" style={{ color: '#0b153e' }}>
              Web Transfer Protocol
            </h2>
          </Grid>
          {WTPProtocolData !== undefined && WTPProtocolData.length > 0 ? (
            <Grid item xs={2} sm={2}>
              <Tooltip title="Edit" onClick={handleClickEdit}>
                <IconButton>
                  <EditIcon style={{ color: '#0b153e', marginTop: '10px' }} />
                </IconButton>
              </Tooltip>
              <Tooltip title="Delete" onClick={handelDeleteOpen}>
                <IconButton>
                  <DeleteIcon style={{ color: 'red', marginTop: '10px' }} />
                </IconButton>
              </Tooltip>
            </Grid>
          ) : (
            ''
          )}
        </Grid>
        {WTPProtocolData !== undefined && WTPProtocolData.length > 0 ? (
          <Paper className="paperpage1">
            <Grid container >
              <Grid container spacing={3} className="content">
                <Grid
                  item
                  xs={3}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Host</Typography>
                  {WTPProtocolData[0].webTransferProtocolHost}
                </Grid>
                <Grid
                  item
                  xs={3}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Port</Typography>
                  {WTPProtocolData[0].webTransferProtocolPort}
                </Grid>
                <Grid
                  item
                  xs={3}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">Name</Typography>
                  {WTPProtocolData[0].webTransferProtocolUserName}
                </Grid>
                <Grid
                  item
                  xs={3}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Password
                  </Typography>
                  {WTPProtocolData[0].webTransferProtocolPassword}
                </Grid>

                <Grid
                  item
                  xs={3}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Location
                  </Typography>
                  {WTPProtocolData[0].webTransferProtocolLocation}
                </Grid>

                <Grid
                  item
                  xs={3}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography className="typographysubheader">
                    Status
                  </Typography>
                  {WTPProtocolData[0].status}
                </Grid>
                <Grid
                  item
                  xs={3}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                  style={{
                    display:
                      props.proData.protocolName == 'HTTP' ||
                      props.proData.protocolName == 'WTP'
                        ? 'none'
                        : 'block',
                  }}
                >
                  <Typography className="typographysubheader">
                    Certificate
                  </Typography>
                  {WTPProtocolData[0].certificateUUID}
                </Grid>
              </Grid>
            </Grid>
          </Paper>
        ) : (
          <div>
            <center>
              <Paper className="paperorgpage">
                No Protocol...Please add Protocol
              </Paper>
            </center>
          </div>
        )}
        {WTPProtocolData !== undefined && WTPProtocolData.length == 0 ? (
          <Fab
            onClick={handleClickOpen}
            aria-label="Add-Icon"
            className="fab"
            color="primary"
          >
            <AddIcon />
          </Fab>
        ) : (
          ''
        )}
        <WebTransferProtocolForm
          handleCloseModal={handelDeleteClose}
          isModalOpen={viewopen}
        >
          <WebTransferProtocolAdd
            handleCloseModal={handelDeleteClose}
            isModalOpen={viewopen}
            title={title}
            proData={props.proData}
            uuid={props.uuid}
            data={WTPProtocolData}
            onSuccess={handleRefetch}
          />
        </WebTransferProtocolForm>
        <DeleteView
          handleCloseModal={handelDeleteClose}
          isModalOpen={deleteOpen}
        >
          <DeleteFrom
            handleCloseModal={handelDeleteClose}
            isModalOpen={deleteOpen}
            data={WTPProtocolData}
            onSuccess={handleRefetch}
          />
        </DeleteView>
        <Snackbar
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
          autoHideDuration={1500}
          open={openSnackbar}
          onClose={handleCloseSnackbar}
        >
          <MySnackbarContentWrapper
            onClose={handleCloseSnackbar}
            variant={variant}
            message={message}
          />
        </Snackbar>
      </MuiThemeProvider>
    </div>
  );
}
