import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import DoneIcon from '@material-ui/icons/Done';
import Button from '@material-ui/core/Button';
import { useMutation } from 'graphql-hooks';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import '../css/commonStyle.css';
import Modal from '@material-ui/core/Modal';
import Theme from '../css/theme';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

const REMOVE_WTPPROTOCOL = `mutation removeWebTransferProtocol($input:WebTransferProtocolInput){
  removeWebTransferProtocol(input:$input) {
    webTransferProtocolUUID 
    webTransferProtocolPort 
    webTransferProtocolHost 
    webTransferProtocolUserName 
    webTransferProtocolPassword 
    protocolTypeUUID 
    status 
    webTransferProtocolLocation 
    partnerProfileID 
    certificateUUID 
    createdOn
    lastUpdatedOn
  } 
}
`;
export default function DeleteProtocol(props) {
  const [removeWTPProtocol, removeloading, removedata] = useMutation(
    REMOVE_WTPPROTOCOL
  );
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSvgIcon: {
          root: {
            color: '#FFFFFF',
          },
        },
        MuiSelect: {
          select: {
            minWidth: '400px',
          },
        },
      },
    });
  async function handleDelete() {
    await removeWTPProtocol({
      variables: {
        input: {
          webTransferProtocolUUID: props.data[0].webTransferProtocolUUID,
        },
      },
    });
    console.log(removeloading, removedata);
    if (removeloading.loading) {
      console.log('WTPProtocol  not deleted');
      props.onSuccess(removeloading.loading, 'Graphql hooks error', false);
    } else {
      console.log('WTPProtocol deleted successfully');
      setTimeout(() => {
        props.onSuccess(removeloading.loading, 'Deleted successfully', true);
      }, 1000);
    }
  }
  const handleCloseModal = () => {
    props.handleCloseModal(false);
  };
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={props.isModalOpen}
        onClose={handleCloseModal}
        className="fabmodal"
      >
        <div className="modalpaper">
          <Grid container className="header">
            <Grid item xs={10} sm={10}>
              <h2 id="modal-title" className="h2">
                Delete
              </h2>
            </Grid>
            <Grid item xs={2} sm={2} className="close">
              <CloseIcon onClick={handleCloseModal} />
            </Grid>
          </Grid>
          <Grid container spacing={3} className="content">
            <Grid
              item
              xs={12}
              sm={12}
              gutterBottom
              variant="body2"
              style={{ paddingBottom: 0 }}
              color="textSecondary"
            >
              <p className="deletetext">Are you sure you want to delete ?</p>
            </Grid>
            <Grid
              item
              xs={12}
              sm={12}
              gutterBottom
              variant="body2"
              style={{ paddingTop: 0, paddingBottom: 0 }}
              color="textSecondary"
              component="p"
            >
              <p className="deletetext" style={{ fontWeight: 700 }}>
                Note: This process cannot be undone
              </p>
            </Grid>
            <Grid item xs={6} sm={6}>
              <Button
                variant="contained"
                fullWidth="true"
                align="right"
                onClick={handleDelete}
                className="button1"
              >
                Yes
                <DoneIcon />
              </Button>
            </Grid>
            <Grid item xs={6} sm={6}>
              <Button
                variant="contained"
                fullWidth="true"
                align="right"
                onClick={handleCloseModal}
                className="button2"
              >
                No
                <CloseIcon />
              </Button>
            </Grid>
          </Grid>
        </div>
      </Modal>
    </MuiThemeProvider>
  );
}
