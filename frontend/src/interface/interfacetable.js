import MaterialTable from 'material-table';
import { useMutation, useQuery } from 'graphql-hooks';
import { forwardRef, useEffect } from 'react';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import ReactDOM from 'react-dom';
import MTableToolbar from 'material-table';
import React, { Component } from 'react';
import Timestamp from '../timestamp';
import Button from '@material-ui/core/Button';
import Theme from '../css/theme';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import InterafaceExpension from '../UserProcessingRule/userProcessingRule';
import { TextField } from '@material-ui/core';
import Error from '../components/emptyPage';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles,
} from '@material-ui/core/styles';
import Loader from '../components/loader';
const DISPLAY_INTERFACE_ORGUUID = `
query fetchInterfaceByorgUUID($orgUUID:String){
  fetchInterfaceByorgUUID(orgUUID:$orgUUID) {
    idInterface
    interfaceUUID
    name
    description
    projectUUID
    createdOn
    lastUpdatedOn
  } 
}`;
const DISPLAY_All_PROJECT = `{
  allProject {
    idOrgProject
    projectUUID
    projectName
    projectDescription
    organizationUUID
    createdOn
    updatedOn
  } 
    
}
`;
const DISPLAY_All_INTERFACE = `query fetchInterface($searchInput:String){
  fetchInterface(searchInput:$searchInput) {
    idInterface
    interfaceUUID
    name
    description
    projectUUID
    createdOn
    lastUpdatedOn
    projectName
  }
}
`;
const ADD_INTERFACE = `mutation createInterface($input:InterfaceInput){
  createInterface(input:$input) {
    idInterface
    interfaceUUID
    name
    description
    projectUUID
    createdOn
    lastUpdatedOn
  }
}`;
const UPDATE_INTERFACE = `mutation updateInterface($input:InterfaceInput){
  updateInterface(input:$input) {
    idInterface
    interfaceUUID
    name
    description
    projectUUID
    createdOn
    lastUpdatedOn
  } 
  }`;
const REMOVE_INTERFACE = `mutation removeINTERFACE($input:InterfaceInput){
  removeInterface(input:$input) {
    idInterface
    interfaceUUID
    name
    description
    projectUUID
    createdOn
    lastUpdatedOn
  } 
}
`;
function iconStyles() {
  return {};
}
export default function InterfaceTable() {
  const [searchInput, setSearchValue] = React.useState('');
  const [Interfacedata, setData] = React.useState([]);
  //all projectData
  const projectData = useQuery(DISPLAY_All_PROJECT);
  //Fetch ALL Interface

  const { loading, errors, data, refetch } = useQuery(DISPLAY_All_INTERFACE, {
    variables: {
      searchInput: searchInput,
    },
  });

  /*const [state, setState] = React.useState({
    columns: [
      { title: 'UUID', field: 'UUID' ,editable: 'never',hidden:true},
        if(rowData!= undefined){
          return Timestamp(rowData.createdOn)
        }else{
        }
      }  }, 
      { title: 'Updated', field: 'lastUpdatedOn',editable: 'never',
      render : (rowData) => {
        console.log(rowData)
        if(rowData!= undefined){
          return Timestamp(rowData.lastUpdatedOn)
        }else{
          return 
        }
      }  },  
     
    ],
    data:[]
   
  });*/
  //ADD Interface
  const [addInterface, addloading, adddata] = useMutation(ADD_INTERFACE);
  //EDIT Interface
  const [updateInterface, updateloading, updatedata] = useMutation(
    UPDATE_INTERFACE
  );
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [message, setSnackbarMessage] = React.useState();
  const [serverError, setServerError] = React.useState(false);
  const [variant, setSnackbarVariant] = React.useState('error');
  //Mandatory validation
  const [nameError, setNameError] = React.useState({
    error: false,
    label: '',
    helperText: '',
    validateInput: false,
  });
  const [valid, setValid] = React.useState(false);
  //remove Interface
  const [removeInterface, removeloading, removedata] = useMutation(
    REMOVE_INTERFACE
  );
  const [project, setProject] = React.useState({});
  const columns = [
    { title: 'UUID', field: 'interfaceUUID', editable: 'never', hidden: true },
    {
      title: 'Project',
      field: 'projectUUID',
      //lookup: project,
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              margin="normal"
              select
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            >
              {projectData.data != undefined &&
              projectData.data.allProject.length > 0
                ? projectData.data.allProject.map((v, k) => {
                    return (
                      <MenuItem value={v.projectUUID}>{v.projectName}</MenuItem>
                    );
                  })
                : null}
            </TextField>
          </div>
        );
      },
      render: (rowData) => {
        console.log(rowData);
        return <div>{rowData.projectName}</div>;
      },
    },

    {
      title: 'Name',
      field: 'name',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder="Name"
              margin="normal"
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            />

            <div style={{ color: 'red' }}>
              {props.rowData.name === '' ||
              /^[a-zA-Z. ]+$/.test(props.rowData.name)
                ? ''
                : 'Enter only alphabets'}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.name}</div>;
      },
    },

    {
      title: 'Description',
      field: 'description',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder="Description"
              margin="normal"
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            />
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.description}</div>;
      },
    },

    {
      title: 'Created',
      field: 'createdOn',
      editable: 'never',
      render: (rowData) => {
        console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.createdOn);
        } else {
          return;
        }
      },
    },
    {
      title: 'Last Modified',
      field: 'lastUpdatedOn',
      editable: 'never',
      render: (rowData) => {
        console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.lastUpdatedOn);
        } else {
          return;
        }
      },
    },
  ];
  useEffect(() => {
    if (data !== undefined) {
      console.log(data.fetchInterface);
      setData(data.fetchInterface);
      setServerError(false);
    } else {
      console.log('server error');
      setServerError(true);
    }
    if (
      projectData !== undefined &&
      projectData.data !== undefined &&
      projectData.data.allProject.length > 0
    ) {
      let projectDetails = projectData.data.allProject;
      console.log(projectDetails);

      var objectVal = projectDetails.reduce(
        (obj, item) => ((obj[item.projectUUID] = item.projectName), obj),
        {}
      );
      setProject(objectVal);
    }
  }, [data]);
  const handleRefetch = (value, message) => {
    refetch({
      updateData(_, data) {
        return data;
      },
    });
    setOpenSnackbar(true);
    setSnackbarMessage(message);
    if (value) {
      setSnackbarVariant('error');
    } else {
      setSnackbarVariant('success');
    }
  };
  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }
  const classes = makeStyles(iconStyles)();
  const tableIcons = {
    Add: forwardRef((props, ref) => (
      <AddBox color="primary" {...props} ref={ref} />
    )),
    Check: forwardRef((props, ref) => (
      <Check color="primary" {...props} ref={ref} />
    )),
    Clear: forwardRef((props, ref) => (
      <Clear color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline color="error" {...props} ref={ref} />
    )),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => (
      <Edit color="primary" {...props} ref={ref} />
    )),
    Export: forwardRef((props, ref) => (
      <SaveAlt color="primary" {...props} ref={ref} />
    )),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => (
      <FirstPage color="primary" {...props} ref={ref} />
    )),
    LastPage: forwardRef((props, ref) => (
      <LastPage color="primary" {...props} ref={ref} />
    )),
    NextPage: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft color="primary" {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => (
      <Clear color="primary" {...props} ref={ref} />
    )),
    Search: forwardRef((props, ref) => (
      <Search color="primary" {...props} ref={ref} />
    )),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSelect: {
          select: {
            minWidth: 150,
          },
          icon: {
            color: '#0b153e',
          },
        },
        MuiTypography: {
          h6: {
            fontSize: 14,
            fontFamily: 'Arial !important',
          },
        },
      },
    });
  async function handleAddInterface(newData) {
    return await new Promise(async (resolve) => {
      resolve();
      console.log(newData);
      await addInterface({
        variables: {
          input: {
            name: newData.name,
            description: newData.description,
            projectUUID: newData.projectUUID,
          },
        },
      })
        .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
          console.log(data, error, graphQLErrors, networkError, cacheHit);
          if (data !== null && error == false) {
            handleRefetch(error, 'Saved successfully');
          } else if (error && graphQLErrors.length > 0) {
            let duperror = graphQLErrors[0].message;
            console.log(duperror);
            if (duperror !== null && duperror.indexOf('ER_DUP_ENTRY') !== -1) {
              handleRefetch(error, 'Name already existed');
            }
          }
        })
        .catch((e) => {
          // you can do something with the error here
          console.log(e);
        });
    });
  }
  async function handleUpdateInterface(newData, oldData) {
    return await new Promise(async (resolve) => {
      resolve();
      console.log(newData);
      await updateInterface({
        variables: {
          input: {
            name: newData.name,
            description: newData.description,
            projectUUID: newData.projectUUID,
            interfaceUUID: newData.interfaceUUID,
          },
        },
      })
        .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
          console.log(data, error, graphQLErrors, networkError, cacheHit);
          if (data !== null && error == false) {
            handleRefetch(error, 'Updated successfully');
          } else if (error && graphQLErrors.length > 0) {
            let duperror = graphQLErrors[0].message;
            console.log(duperror);
            if (duperror !== null && duperror.indexOf('ER_DUP_ENTRY') !== -1) {
              handleRefetch(error, 'Name already existed');
            }
          }
        })
        .catch((e) => {
          // you can do something with the error here
          console.log(e);
        });
    });
  }

  async function handleRemoveInterface(oldData) {
    return await new Promise(async (resolve) => {
      resolve();
      await removeInterface({
        variables: {
          input: {
            interfaceUUID: oldData.interfaceUUID,
          },
        },
      })
        .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
          console.log(data, error, graphQLErrors, networkError, cacheHit);
          if (data !== null && error == false) {
            handleRefetch(error, 'Deleted successfully');
          } else if (error && graphQLErrors.length > 0) {
            let duperror = graphQLErrors[0].message;
            console.log(duperror);
            if (
              duperror !== null &&
              duperror.indexOf('ER_ROW_IS_REFERENCED') !== -1
            ) {
              setOpenSnackbar(true);
              setSnackbarVariant('error');
              setSnackbarMessage(
                'Selected row is referenced in Organization interface table'
              );
            }
          }
        })
        .catch((e) => {
          // you can do something with the error here
          console.log(e);
        });
    });
  }
  const handleSearchValue = (val) => {
    console.log(val);
    setSearchValue(val);
  };
  if (loading) return <Loader />;
  if (serverError)
    return <Error type={'Server connection lost.Please try again'} />;
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <MaterialTable
        icons={tableIcons}
        title="Interface"
        columns={columns}
        data={Interfacedata}
        //onSearchChange={(e) => handleSearchValue(e)}
        editable={{
          onRowAdd: (newData) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                newData.submitted = true;
                if (
                  !newData.name ||
                  !newData.projectUUID ||
                  !/^[a-zA-Z\s]+$/.test(newData.name)
                ) {
                  setNameError({
                    error: true,
                    label: 'required',
                    helperText: 'Required.',
                    validateInput: true,
                  });
                  reject();
                  return;
                }
                resolve();
                handleAddInterface(newData);
              }, 600);
            }),

          onRowUpdate: (newData, oldData) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                newData.submitted = true;
                if (
                  !newData.name ||
                  !newData.projectUUID ||
                  !/^[a-zA-Z\s]+$/.test(newData.name)
                ) {
                  setNameError({
                    error: true,
                    label: 'required',
                    helperText: 'Required.',
                    validateInput: true,
                  });
                  reject();
                  return;
                }
                resolve();
                handleUpdateInterface(newData, oldData);
              }, 600);
            }),

          onRowDelete: (oldData) => handleRemoveInterface(oldData),
        }}
        detailPanel={[
          {
            render: (rowData) => {
              return <InterafaceExpension rowData={rowData} />;
            },
          },
        ]}
        options={{
          headerStyle: {
            textAlign: 'center',
            fontSize: 12,
            fontWeight: 'bold',
            fontFamily: 'Arial !important',
            backgroundColor: '#0b153e',
            color: '#ffffff',
            padding: '4px',
          },

          searchFieldStyle: {
            color: '#0b153e',
          },
          actionsColumnIndex: -1,
          pageSize: 10,
          pageSizeOptions: [10, 25, 50, 100],
          toolbar: true,
          paging: true,
        }}
      />
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        autoHideDuration={1500}
        open={openSnackbar}
        onClose={handleCloseSnackbar}
      >
        <MySnackbarContentWrapper
          onClose={handleCloseSnackbar}
          variant={variant}
          message={message}
        />
      </Snackbar>
    </MuiThemeProvider>
  );
}
