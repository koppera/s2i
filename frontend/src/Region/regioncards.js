import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Divider from "@material-ui/core/Divider";
import Grid from "@material-ui/core/Grid";
import CardActionArea from "@material-ui/core/CardActionArea";
import '../css/commonStyle.css';
import Fab from '@material-ui/core/Fab';
import Tooltip from '@material-ui/core/Tooltip';
import AddIcon from '@material-ui/icons/Add';
import ModalFab from '../components/modalfabtms';
import AddRegion from './addregion';
import ModalPage from '../components/tmsmodal';
import EditRegion from './editregion';
import DeleteRegion from './deleteregion';

const useStyles = makeStyles({
  card: {
    minWidth: 100,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});
export default function RegionCards(props) {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [openeditregion, setEditRegion] = React.useState(false);
    const [opendeleteregion, setDeleteRegion] = React.useState(false);
  
  
    const handleOpen = () => {
        setOpen(true);
    };

    let handleClose = () => {
        setOpen(false);
    } 
    const handleeditregion = () => {
      setEditRegion(true);
        };
     
        const handleCloseModal = () => {
          setEditRegion(false);
        };
        const handledeleteregion = () => {
          setDeleteRegion(true);
            };
         
            const handleCloseModal1 = () => {
              setDeleteRegion(false);
            };
    

       
  return (
    <div>
                            <Fab aria-label="Add-Icon" className="fab" color="primary">
                                <AddIcon/>   
                            </Fab>
                    
      <Card className="maincard">
        <CardActionArea
        classes={{
          root: classes.actionArea,
          focusHighlight: classes.focusHighlight
        }}
        >
          <CardContent>
            <Grid container spacing={2} >
              <Grid item xs={8} sm={8} style={{ padding: 1 }}>
                <Typography
                  gutterBottom
                  variant="h6"
                  component="h4"
                  className="cardprojectname"
                >
                    <Typography className="cardprojectname" color="textSecondary" gutterBottom>
                    Name
                    </Typography>
                </Typography>
              </Grid>
              <Grid item xs={6} sm={6} style={{ padding: 5 }}>
                <Typography
                  variant="body2"
                  color="textSecondary"
                  className="ownername"
                >
                    <Typography className="cardprojectname" color="textSecondary" gutterBottom>
                ID
                  </Typography>
                </Typography>
              </Grid>
              </Grid>
              <Grid item xs={12} sm={12} style={{ padding: 1 }}>
                <Typography
                  variant="body2"
                  color="textSecondary"
                  component="p"
                  className="carddateValue"
                >
                  29-11-2019
                </Typography>
              </Grid>
          </CardContent>
        </CardActionArea>
        <Divider />
        <CardActions className="bgcolor">
          <Button
            size="small"
            className="cardbutton" onClick={handleeditregion}>
          
            <span className="buttontext"> EDIT </span>
          </Button>
         
            <Button size="small" className="cardbutton"  onClick={handledeleteregion}>
              <span className="buttontext"> DELETE</span>
            </Button>
       
        </CardActions>
      </Card>
      <ModalFab title="Add Region" >
            <AddRegion  configData={props.ConfigSettingData} location={props.location} {...props}/>
          </ModalFab>
          <ModalPage
        handleCloseModal={handleCloseModal}
        title="Edit Region"
        isModalOpen={openeditregion}
      >
        <EditRegion
          handleCloseModal={handleCloseModal}
        />
      </ModalPage>
      <ModalPage
        handleCloseModal1={handleCloseModal1}
        title="Delete Region"
        isModalOpen={opendeleteregion}
      >
        <DeleteRegion
          handleCloseModal1={handleCloseModal1}
        />
      </ModalPage>
</div>
  );
}





