import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import CloseIcon from "@material-ui/icons/Close";
import DoneIcon from "@material-ui/icons/Done";
import Button from "@material-ui/core/Button";
import Snackbar from "@material-ui/core/Snackbar";
import IconButton from "@material-ui/core/IconButton";
import '../css/commonStyle.css';

const useStyles = makeStyles(theme => ({
  media: {
    height: 0,
    paddingTop: "56.25%" // 16:9
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
    duration: theme.transitions.duration.shortest
    })
  },
  expandOpen: {
    transform: "rotate(180deg)"
  },
  paper: {
    textAlign: "center",
    color: theme.palette.text.secondary
  },
}));

export default function DeleteProject(props) {
  console.log(props);
  const classes = useStyles();
  const [state, setState] = React.useState({
    checked: false
  });
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  

  function handleCloseSnackbar(event, reason) {
    if (reason === "clickaway") {
      return;
    }
    setOpenSnackbar(false);
  }
  const handleModalClose1 = () => {
    props.handleCloseModal1(false);
  };
  return (
    <div>
      <Grid container className="header">
        <Grid item xs={10} sm={10}>
          <h2 className="h2">
            Delete Region
          </h2>
        </Grid>
        <Grid item xs={2} sm={2} className="close">
          <CloseIcon onClick={handleModalClose1} />
        </Grid>
      </Grid>
      <Grid container spacing={3} className="content">
        <Grid
          item
          xs={12}
          sm={12}
          gutterBottom
          variant="body2"
          style={{ paddingBottom: 0 }}
          color="textSecondary"
        >
          <p className="deletetext">
            Are you sure you want to delete region & all of its content?
          </p>
        </Grid>
        <Grid item xs={12} sm={12} style={{ paddingTop: 0, paddingBottom: 0 }}>
        </Grid>
        <Grid
          item
          xs={12}
          sm={12}
          gutterBottom
          variant="body2"
          style={{ paddingTop: 0, paddingBottom: 0 }}
          color="textSecondary"
          component="p"
        >
          <p className="deletetext" style={{fontWeight:700}}>Note: This process cannot be undone</p>
        </Grid>
        <Grid item xs={6} sm={6}>
          <Button
            variant="contained"
            fullWidth="true"
            align="right"
           
            className="button1"
          >
            Yes
            <DoneIcon />
          </Button>
        </Grid>
        <Grid item xs={6} sm={6}>
          <Button
            variant="contained"
            fullWidth="true"
            align="right"
            
            className="button2"
          >
            No
            <CloseIcon />
          </Button>
        </Grid>
      </Grid>
      <Snackbar
        anchorOrigin={{
          vertical: "top",
          horizontal: "center"
        }}
        className="SnackbarCss"
        open={openSnackbar}
        autoHideDuration={1000}
        onClose={handleCloseSnackbar}
        ContentProps={{
          "aria-describedby": "message-id"
        }}
      
        action={[
          <IconButton
            key="close"
            aria-label="Close"
            color="inherit"
            onClick={handleCloseSnackbar}
          >
            <CloseIcon />
          </IconButton>
        ]}
      />
    </div>
  );
}
