import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from "@material-ui/core/Button";
import Grid from '@material-ui/core/Grid';
import Snackbar from "@material-ui/core/Snackbar";
import MySnackbarContentWrapper from "../components/Snackbar";
import CloseIcon from "@material-ui/icons/Close";
import '../css/commonStyle.css';
const useStyles = makeStyles(theme => ({
}));

export default function AddRegion(props) {
  const classes = useStyles();
  const [values,setValues] = React.useState({regionname : ""});
  const [error,setError] = React.useState(false);
  const [errmsg,setErrMsg] = React.useState(false);
  const [snackbarMessage, setSnackbarMessage] = React.useState('');
  const [openSnackbar, setOpenSnackbar] = React.useState(false);

  function handleClose() {
    setOpenSnackbar(false);
  }

  const handleModalClose = () => {
    props.handleCloseModal(false);
  };

  const handleChange = name => event => {
      setErrMsg(false);
      setValues({ ...values, [name]: event.target.value });
    if(event.target.value.length === 0){
        setValues({ ...values, [name]: ""});
        setError(false);
      }
      else{
        if (event.target.value.match("^([a-zA-Z0-9])[a-zA-Z0-9_]*$") && event.target.value.length <= 25) {
            setError(false);
            setValues({ ...values, [name]: event.target.value });
        }
        else {
         setError(true);
        }
      }
}

const handleSubmit = () => {
    setErrMsg(false);
    if (values.regionname === "" || error === true){
      setErrMsg(true);
  }
    else{
        setErrMsg(false);
        setSnackbarMessage('Saved Successfully');
        setOpenSnackbar(true);
        setValues({ ...values, regionname: ""})
    }
}


  return (
  <div>
       <Grid container className="header">
        <Grid item xs={10} sm={10}>
          <h2 className="h2">
           Edit Region
          </h2>
        </Grid>
        <Grid item xs={2} sm={2} className="close">
          <CloseIcon onClick={handleModalClose} />
        </Grid>
      </Grid>
    <form noValidate autoComplete="off">
            <Grid container spacing={3} className="content">
                <Grid item xs={12}>
                {errmsg === true ? (
                <div id="nameid" className="addfieldserror">
                please fill the field
                </div>
                ) : (
                ""
                )}
                <TextField
                id="standard-basic"
                className="textField"
                label="RegionName"
                onChange={handleChange("regionname")}
                margin="dense"
                variant="outlined"
                value={values.regionname}
                name="regionname"
                />
                {error === true ? (
                <div id="nameid" className="addfieldserror">
                    Starts with alphabets followed by alphanumeric. No special
                    characters allowed except underscore"_". Maximum length is 25.
                </div>
                ) : (
                ""
                )}
            </Grid>
         
            <Button
                    variant="contained"
                    color="primary"
                    size="Large"
                    className="createbutton" color="secondary"
                    onClick={handleSubmit}
                >
                     SAVE
                </Button>
         
        </Grid>
    </form>
    <Snackbar
            anchorOrigin={{
              vertical: 'top',
              horizontal: 'center'
            }}
            autoHideDuration={5000}
            open={openSnackbar}
            onClose={handleClose}
          >
            <MySnackbarContentWrapper
              onClose={handleClose}
              variant="success"
              message={snackbarMessage}
            />
          </Snackbar>
  </div>
  ); 
}