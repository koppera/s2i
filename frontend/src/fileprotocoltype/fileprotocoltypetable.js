import MaterialTable from 'material-table';
import { forwardRef } from 'react';
 import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import ReactDOM from 'react-dom';
import MTableToolbar  from 'material-table';
import React, { Component } from "react";
import {TextField } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Theme from '../css/theme';
import Timestamp from '../timestamp';
import Error from '../components/emptyPage';
import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles
} from '@material-ui/core/styles';

function iconStyles() {
  return {
  };
}
export default function MaterialTableDemo() {
  const [state, setState] = React.useState({
    columns: [
      { title: 'UUID', field: 'ProtocolTypeUUID',editable: 'never',hidden:true},
      { title: 'Name', field: 'ProtocolTypeName',editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
             /* label="addresstypeName"*/
              placeholder="Name"
              margin="normal"
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
              
            />
            
            <div style={{color:'red'}}>
               {
                  props.rowData.ProtocolTypeName===''||
                  !/^[a-zA-Z\s]+$/.test(props.rowData.ProtocolTypeName) ?'Enter only alphabets':''
               }
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.ProtocolTypeName}</div>;
      },
    }, 
      { title: 'Description', field: 'ProtocolTypeDescription',editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
             /* label="addresstypeName"*/
              placeholder="Description"
              margin="normal"
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
              
            />
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.ProtocolTypeDescription}</div>;
      },
    },
      { title: 'Created', field: 'CreatedOn',editable: 'never' ,
      render : (rowData) => {
        console.log(rowData)
        if(rowData!= undefined){
          return Timestamp(rowData.CreatedOn)
        }else{
          return 
        }
      }},
      { title: 'Last Modified', field: 'LastUpdatedOn',editable: 'never',
      render : (rowData) => {
        console.log(rowData)
        if(rowData!= undefined){
          return Timestamp(rowData.LastUpdatedOn)
        }else{
          return 
        }
      } },

    ],
    data: []
     
  });

  const classes = makeStyles(iconStyles)();
  const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear color="error"{...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline  color="error" {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => (<ChevronRight  {...props} ref={ref} />)),
    Edit: forwardRef((props, ref) => (
      <Edit
        {...props}
        ref={ref}
        onClick={console.log('hello world')}
      />
    )),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search  {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
  };
 
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSvgIcon:{
          root:{
         color:"#0b153e"
        }
        },
        MuiTypography:{h6:{
          fontSize:14,  
          fontFamily:"Arial !important"    
          }},
      }
      
    });
  
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
    <MaterialTable
     
     icons={tableIcons}
         title="File Protocol Type"
      columns={state.columns}
      data={state.data}
      editable={{
        onRowAdd: newData =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              setState(prevState => {
                const data = [...prevState.data];

                data.push(newData);
                return { ...prevState, data };

              });
            }, 600);
          }),
        onRowUpdate: (newData, oldData) =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              if (oldData) {
                setState(prevState => {
                  const data = [...prevState.data];
                  data[data.indexOf(oldData)] = newData;
                  return { ...prevState, data };
                });
              }
            }, 600);
          }),
        onRowDelete: oldData =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              setState(prevState => {
                const data = [...prevState.data];
                data.splice(data.indexOf(oldData), 1);
                return { ...prevState, data };
              });
            }, 600);
          }),
      }} options={{
        headerStyle: {
        
        textAlign: 'center',
        fontSize:12,
        fontWeight:'bold',
        fontFamily:'Arial !important',
        backgroundColor:"#0b153e",
        color:"#ffffff",
        padding:'4px',
       
        },
        
          
          searchFieldStyle: {
            color: '#0b153e'
        },
        actionsColumnIndex: -1
       
        }}
      />
  
       
        </MuiThemeProvider>
      
  );
 
    }
