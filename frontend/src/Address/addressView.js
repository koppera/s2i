import React, { useEffect, useContext, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { Grid, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { withStyles } from '@material-ui/core/styles';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import '../css/commonStyle.css';
import Theme from '../css/theme';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(2),
    },
  },

  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));
const styles = (theme) => ({
  root: {
    Width: 1200,
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: '#FFFFFF',
  },
  
});
const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});
export default function ContactView(props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSvgIcon: {
          root: {
            color: '#FFFFFF',
          },
        },
        MuiGrid:{
          container:{
         paddingLeft:'1.5%',
         paddingBottom:'1%',
         overflowX: 'hidden'

          },
          item:{
            fontSize:'10px',
          }
        },
        MuiDialogTitle: {
          root: {
            backgroundColor: '#0b153e',
            color: '#FFFFFF',
          },
        },
      },
    });
  const handleModalClose = () => {
    props.handleCloseModal(false);
  };
  return (
    <div>
      <MuiThemeProvider theme={getMuiTheme()}>
        <Grid container className="header">
          <Grid item xs={10} sm={10}>
            <h2 className="h2">Address</h2>
          </Grid>
          <Grid item xs={2} sm={2} className="close">
            <CloseIcon onClick={handleModalClose} />
          </Grid>
        </Grid>
        <Grid container className="pdashboardroot">
        <Grid container spacing={3}>
          <Grid
            item
            xs={12}
            sm={3}
            gutterBottom
            variant="body2"
            color="textSecondary"
            className="text"
          >
            <Typography className="typographysubheader"> Address Name</Typography>
            {props.rowData.addressNameID}
          </Grid>
          <Grid
            item
            xs={12}
            sm={3}
            gutterBottom
            variant="body2"
            color="textSecondary"
            className="text"
          >
            <Typography className="typographysubheader"> Address Line 1</Typography>
            {props.rowData.addressLines1}
          </Grid>
          <Grid
            item
            xs={12}
            sm={3}
            gutterBottom
            variant="body2"
            color="textSecondary"
            className="text"
          >
            <Typography className="typographysubheader"> Address Line 2</Typography>
            {props.rowData.addressLines2}
          </Grid>
          <Grid
            item
            xs={12}
            sm={3}
            gutterBottom
            variant="body2"
            color="textSecondary"
            className="text"
          >
            <Typography className="typographysubheader"> Address Line 3</Typography>
            {props.rowData.addressLines3}
          </Grid>
          </Grid>
          <Grid container spacing={3}>
          <Grid
            item
            xs={12}
            sm={3}
            gutterBottom
            variant="body2"
            color="textSecondary"
            className="text"
          >
            <Typography className="typographysubheader">City </Typography>{' '}
            {props.rowData.city}
          </Grid>
          <Grid
            item
            xs={12}
            sm={3}
            gutterBottom
            variant="body2"
            color="textSecondary"
            className="text"
          >
            <Typography className="typographysubheader">State </Typography>{' '}
            {props.rowData.state}
          </Grid>
          <Grid
            item
            xs={12}
            sm={3}
            gutterBottom
            variant="body2"
            color="textSecondary"
            className="text"
          >
            <Typography className="typographysubheader">Country </Typography>{' '}
            {props.rowData.country}
          </Grid>
          <Grid
            item
            xs={12}
            sm={3}
            gutterBottom
            variant="body2"
            color="textSecondary"
            className="text"
          >
            <Typography className="typographysubheader">Zip </Typography>{' '}
            {props.rowData.zip}
          </Grid>
          <Grid
            item
            xs={12}
            sm={3}
            gutterBottom
            variant="body2"
            color="textSecondary"
            className="text"
          >
            <Typography className="typographysubheader">Status </Typography>{' '}
            {props.rowData.addressStatus}
          </Grid>
          </Grid>
        </Grid>
      </MuiThemeProvider>
    </div>
  );
}
