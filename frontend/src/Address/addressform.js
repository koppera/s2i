import React, { useEffect, useContext, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid';
const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing(2)
  }
}));
export default function Switches() {
  const classes = useStyles();
  const [TypeUUID, setTypeUUID] = React.useState('');
  const [state, setState] = React.useState({
    checkedA: true,
    checkedB: true
  });

  const handleChange1 = event => {
    setTypeUUID(event.target.value);
  };

  const handleChange = event => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };

  const [values, setValues] = React.useState({
    name: '',
    line1: '',
    city: '',
    state: '',
    country: '',
    zip: ''
  });
  const [ErrMsg, setErrMsg] = React.useState(false);
  const [errorInfo, setErrorInfo] = React.useState({
    name: false,
    line1: false,
    city: false,
    state: false,
    country: false,
    zip: false
  });
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [value, setValue] = React.useState('IS');
  const [variant, setSnackbarVariant] = React.useState('error');
  const [message, setSnackbarMessage] = React.useState();

  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }
  const handleChange2 = name => event => {
    setErrMsg(false);
    setValues({ ...values, [name]: event.target.value });
    switch (name) {
      case 'name':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match('^([a-zA-Z])[a-zA-Z]*$') &&
          event.target.value.length <= 25
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'line1':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match('^([a-zA-Z0-9])[a-zA-Z0-9]*$') &&
          event.target.value.length <= 25
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'city':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match('^([a-zA-Z])[a-zA-Z]*$') &&
          event.target.value.length <= 25
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
        } else setErrorInfo({ ...errorInfo, [name]: true });
        break;
      case 'state':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match('^([a-zA-Z])[a-zA-Z]*$') &&
          event.target.value.length <= 25
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
        } else setErrorInfo({ ...errorInfo, [name]: true });
        break;

      case 'country':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match('^([a-zA-Z])[a-zA-Z]*$') &&
          event.target.value.length <= 25
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
        } else setErrorInfo({ ...errorInfo, [name]: true });
        break;
      case 'zip':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match('^[0-9]+$') &&
          event.target.value.length <= 6
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
          console.log(event.target.value);
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      default:
        return false;
    }
  };
  const handleSubmit = async () => {
    console.log(values);
    setErrMsg(false);
    if (
      values.name === '' ||
      values.line1 === '' ||
      values.city === '' ||
      values.state === '' ||
      values.country === '' ||
      values.zip === ''
    ) {
      setErrMsg(true);
    } else {
      setErrMsg(false);
      setValues({
        ...values,
        name: '',
        line1: '',
        city: '',
        state: '',
        country: '',
        zip: ''
      });
    }
  };

  return (
    <div>
      {ErrMsg === true ? (
        <div style={{ textAlign: 'center' }} className="addfieldserror">
          Please fill the fields
        </div>
      ) : (
        ''
      )}

      <form className="content" noValidate autoComplete="off">
        <TextField
          id="outlined-dense"
          label="Name "
          className="textField"
          margin="dense"
          variant="outlined"
          onChange={handleChange2('name')}
          value={values.name}
          name="name"
        />
        {errorInfo.name === true ? (
          <div id="nameid" className="addfieldserror">
            "Allowed alphabets,no special characters and Numerics are
            allowed.Maximum length is 25."
          </div>
        ) : (
          ''
        )}

        <TextField
          id="outlined-dense"
          label="Line1 "
          className="textField"
          margin="dense"
          variant="outlined"
          onChange={handleChange2('line1')}
          value={values.line1}
          name="line1"
        />
        {errorInfo.line1 === true ? (
          <div id="nameid" className="addfieldserror">
            "Allowed alphabets and Numerics,no special characters and are
            allowed.Maximum length is 25."
          </div>
        ) : (
          ''
        )}

        <TextField
          id="outlined-dense"
          label="Line2 "
          className="textField"
          margin="dense"
          variant="outlined"
          name="line2"
        />

        <TextField
          id="outlined-dense"
          label="Line3 "
          className="textField"
          margin="dense"
          variant="outlined"
          name="line3"
        />

        <TextField
          id="outlined-dense"
          label="City "
          className="textField"
          margin="dense"
          variant="outlined"
          onChange={handleChange2('city')}
          value={values.city}
          name="city"
        />
        {errorInfo.city === true ? (
          <div id="nameid" className="addfieldserror">
            "Allowed alphabets,no special characters and Numerics are
            allowed.Maximum length is 25."
          </div>
        ) : (
          ''
        )}

        <TextField
          id="outlined-dense"
          label="State"
          className="textField"
          margin="dense"
          variant="outlined"
          onChange={handleChange2('state')}
          value={values.state}
          name="state"
        />
        {errorInfo.state === true ? (
          <div id="nameid" className="addfieldserror">
            "Allowed alphabets,no special characters and Numerics are
            allowed.Maximum length is 25."
          </div>
        ) : (
          ''
        )}

        <TextField
          id="outlined-dense"
          label="Country "
          className="textField"
          margin="dense"
          variant="outlined"
          onChange={handleChange2('country')}
          value={values.country}
          name="country"
        />
        {errorInfo.country === true ? (
          <div id="nameid" className="addfieldserror">
            "Allowed alphabets,no special characters and Numerics are
            allowed.Maximum length is 25."
          </div>
        ) : (
          ''
        )}

        <TextField
          id="outlined-dense"
          label="Zip"
          className="textField"
          margin="dense"
          variant="outlined"
          onChange={handleChange2('zip')}
          value={values.zip}
          name="zip"
        />
        {errorInfo.zip === true ? (
          <div id="nameid" className="addfieldserror">
            "Allowed Numerics,no special characters and alphabets are
            allowed.Maximum length is 6."
          </div>
        ) : (
          ''
        )}
        <FormControlLabel
          label="Status"
          control={
            <Switch
              checked={state.checkedA}
              onChange={handleChange}
              name="checkedA"
            />
          }
        />

        <FormControl className={classes.formControl}>
          <InputLabel id="demo-simple-select-label">TypeUUID</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={TypeUUID}
            onChange={handleChange1}
          >
            <MenuItem value={10}>CNP</MenuItem>
            <MenuItem value={20}>TMS</MenuItem>
            <MenuItem value={30}>Cloudgen</MenuItem>
          </Select>
        </FormControl>

        <Button
          onClick={handleSubmit}
          variant="contained"
          fullWidth="true"
          className="createpartnerbutton"
        >
          Submit
        </Button>
      </form>
    </div>
  );
}
