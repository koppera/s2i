import MaterialTable from 'material-table';
import { useMutation, useQuery } from 'graphql-hooks';
import { forwardRef, useEffect } from 'react';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import ReactDOM from 'react-dom';
import MTableToolbar from 'material-table';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import CloseIcon from '@material-ui/icons/Close';
import { withStyles } from '@material-ui/core/styles';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import { Grid, Typography } from '@material-ui/core';
import MuiDialogActions from '@material-ui/core/DialogActions';
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import Theme from '../css/theme';
import AddressView from './addressView';
import Error from '../components/emptyPage';
import AddressViewModelForm from '../components/tmsmodal';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import Timestamp from '../timestamp';
import Loader from '../components/loader';
import MenuItem from '@material-ui/core/MenuItem';
import VisibilityIcon from '@material-ui/icons/Visibility';
import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles,
} from '@material-ui/core/styles';

const DISPLAY_All_ADDRESSTYPES = `{
  allAddressTypes {
    idAddressType
    addressTypeUUID
    addressTypeName
    addressTypeDescription
    createdOn
    lastUpdatedOn
  }
}
`;
const FETCH_ADDRESS_BY_PARTNERPROFILEUUIS = `query fetchAddressByPartnerProfile($partnerProfileUUID:String,$searchInput:String,$contact:Int){
  fetchAddressByPartnerProfile(partnerProfileUUID:$partnerProfileUUID,searchInput:$searchInput,contact:$contact) {
    idAddress
    addressUUID
    addressNameID
    addressLines1
    addressLines2
    addressLines3
    city
    state
    country
    zip
    addressStatus
    addressTypeUUID
    addressTypeName
    partnerProfileUUID
    createdOn
    lastUpdatedOn

  }
}`;

const ADD_ADDRESS = `mutation createAddress($input:AddressInput){
  createAddress(input:$input) {
    idAddress
    addressUUID
    addressNameID
    addressLines1
    addressLines2
    addressLines3
    city
    state
    country
    zip
    addressStatus
    addressTypeUUID
    partnerProfileUUID 
    createdOn
    lastUpdatedOn
    contactUUID
  } 
}
`;
const UPDATE_ADDRESS = `mutation updateAddress($input:AddressInput){
  updateAddress(input:$input) {
    idAddress
    addressUUID
    addressNameID
    addressLines1
    addressLines2
    addressLines3
    city
    state
    country
    zip
    addressStatus
    addressTypeUUID
    partnerProfileUUID
    createdOn
    lastUpdatedOn
  } 
}`;
const REMOVE_ADDRESS = `mutation removeAddress($input:AddressInput){
  removeAddress(input:$input) {
    idAddress
    addressUUID
    addressNameID
    addressLines1
    addressLines2
    addressLines3
    city
    state
    country
    zip
    addressStatus
    addressTypeUUID
    partnerProfileUUID
    createdOn
    lastUpdatedOn
  } 

}`;
const UPDATE_CONTACT_BY_STATUS = `mutation UpdateaddressUUIDOnStatus($addressUUID:String){
  UpdateaddressUUIDOnStatus(addressUUID:$addressUUID) {
    idContact
    contactUUID
    role
    firstName
    lastName
    email
    phone
    fax
  } 
}`;
function iconStyles() {
  return {};
}

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(2),
    },
  },

  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));
const styles = (theme) => ({
  root: {
    Width: 1200,
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: '#FFFFFF',
  },
});

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

export default function Address(props) {
  //all address types
  const addrestypeData = useQuery(DISPLAY_All_ADDRESSTYPES);
  //ADD address
  const [addAddress, addloading, adddata] = useMutation(ADD_ADDRESS);
  const [searchInput, setSearchValue] = React.useState('');
  const [serverError, setServerError] = React.useState(false);
  //Fetch ALL ADDRESS BY PARTNER PROFILE id
  const { loading, error, data, refetch } = useQuery(
    FETCH_ADDRESS_BY_PARTNERPROFILEUUIS,
    {
      variables: {
        searchInput: searchInput,
        partnerProfileUUID: props.uuid,
        contact: 0,
      },
    }
  );
  //EDIT ADDRESS
  const [updateAddress, updateloading, updatedata] = useMutation(
    UPDATE_ADDRESS
  );
  //remove address
  const [removeAddress, removeloading, removedata] = useMutation(
    REMOVE_ADDRESS
  );
  //update contact by changing status
  const [updateContact, contactloading, conatctdata] = useMutation(
    UPDATE_CONTACT_BY_STATUS
  );

  const [addressdata, setData] = React.useState([]);
  const [addressRowData, setAddressRowData] = React.useState();
  const [addreValue, setAddressTYpe] = React.useState({});
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [message, setSnackbarMessage] = React.useState();
  const [variant, setSnackbarVariant] = React.useState('error');
  const [nameError, setNameError] = React.useState({
    error: false,
    label: '',
    helperText: '',
    validateInput: false,
  });
  const [valid, setValid] = React.useState(false);
  const addressNameIDexp = /^[a-zA-Z0-9,-.:/ ]*$/;
  const line1exp = /^[a-zA-Z0-9,-.:/ ]*$/;
  const cityexp = /^[a-zA-Z,-. ]+$/;
  const stateexp = /^[a-zA-Z,-. ]+$/;
  const countryexp = /^[a-zA-Z,-./ ]+$/;
  const zipexp = /^[A-Z0-9]{5,6}$/; 
  const columns = [
    // { title: 'Id address', field: 'idaddress' },
    { title: 'UUID', field: 'addressUUID', editable: 'never', hidden: true },
    {
      title: 'Type',
      field: 'addressTypeUUID',
      //lookup: addreValue,
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              //placeholder="Description"
              margin="normal"
              select
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            >
              {addrestypeData.data != undefined &&
              addrestypeData.data.allAddressTypes.length > 0
                ? addrestypeData.data.allAddressTypes.map((v, k) => {
                    return (
                      <MenuItem value={v.addressTypeUUID}>
                        {v.addressTypeName}
                      </MenuItem>
                    );
                  })
                : null}
            </TextField>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.addressTypeName}</div>;
      },
    },

    {
      title: ' Name',
      field: 'addressNameID',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder="Name"
              margin="normal"
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            />

            <div style={{ color: 'red' }}>
              {!props.rowData.addressNameID ||
              addressNameIDexp.test(props.rowData.addressNameID)
                ? ''
                : 'Special characters are not allowed except[,-.:/]'}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.addressNameID}</div>;
      },
    },

    {
      title: ' Line 1',
      field: 'addressLines1',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              //type="textarea"
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder=" Line 1"
              margin="normal"
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            />

            <div style={{ color: 'red' }}>
              {!props.rowData.addressLines1 ||
              line1exp.test(props.rowData.addressLines1)
                ? ''
                : 'Special characters are not allowed except[,-.:/]'}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.addressLines1}</div>;
      },
    },
    {
      title: 'Line 2',
      field: 'addressLines2',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder="Line 2"
              margin="normal"
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            />

            <div style={{ color: 'red' }}>
              {!props.rowData.addressLines2 ||
              line1exp.test(props.rowData.addressLines2)
                ? ''
                : 'Special characters are not allowed except[,-.:/]'}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.addressLines2}</div>;
      },
    },
    {
      title: 'Line 3',
      field: 'addressLines3',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder="Line 3"
              margin="normal"
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            />

            <div style={{ color: 'red' }}>
              {!props.rowData.addressLines3 ||
              line1exp.test(props.rowData.addressLines3)
                ? ' '
                : 'Special characters are not allowed except[,-.:/]'}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.addressLines3}</div>;
      },
    },
    {
      title: 'City',
      field: 'city',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder="city"
              margin="normal"
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            />

            <div style={{ color: 'red' }}>
              {!props.rowData.city || cityexp.test(props.rowData.city)
                ? ''
                : 'Enter only alphabets'}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.city}</div>;
      },
    },
    {
      title: 'State',
      field: 'state',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder="state"
              margin="normal"
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            />

            <div style={{ color: 'red' }}>
              {!props.rowData.state || stateexp.test(props.rowData.state)
                ? ''
                : 'Enter only alphabets'}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.state}</div>;
      },
    },
    {
      title: 'Country',
      field: 'country',

      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder="country"
              margin="normal"
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            />

            <div style={{ color: 'red' }}>
              {!props.rowData.country || countryexp.test(props.rowData.country)
                ? ''
                : 'Enter only alphabets'}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.country}</div>;
      },
    },
    {
      title: 'Zip',
      field: 'zip',

      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder="zip"
              margin="normal"
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            />

            <div style={{ color: 'red' }}>
              {!props.rowData.zip || zipexp.test(props.rowData.zip)
                ? ''
                : 'Allowed Uppercase letters & Numbers.Minimum length is 5 and Maximun is 6'}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.zip}</div>;
      },
    },
    {
      title: 'Status',
      field: 'addressStatus',
      // hidden: true,
      editComponent: (props) => {
        //  console.log(props);
        var statusVal;
        if (props.value == 'Active' || props.value == true) {
          statusVal = true;
        } else {
          statusVal = false;
        }
        return (
          <FormControlLabel
            style={{ backgroundColor: 'transparent' }}
            control={
              <Switch
                color="primary"
                onChange={(v) => props.onChange(v.target.checked)}
                checked={statusVal}
              />
            }
          />
        );
      },
      render: (rowData) => {
        console.log(rowData);
        return (
          <FormControlLabel
            style={{ backgroundColor: 'transparent' }}
            control={
              <Switch
                color="primary"
                onClick={() => handleShowComponent1(rowData)}
                checked={
                  rowData === undefined
                    ? false
                    : rowData.addressStatus === 'Inactive'
                    ? false
                    : true
                }
              />
            }
          />
        );
      },
    },

    {
      title: 'Partner Profile',
      field: 'partnerProfileUUID',
      hidden: true,
    },
    {
      title: 'Created',
      field: 'createdOn',
      editable: 'never',
      render: (rowData) => {
        console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.createdOn);
        } else {
          return;
        }
      },
    },
    {
      title: 'Last Modified',
      field: 'lastUpdatedOn',
      editable: 'never',
      render: (rowData) => {
        console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.lastUpdatedOn);
        } else {
          return;
        }
      },
    },
  ];

  const [state, setState] = React.useState({
    data: addressdata,
  });
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const [viewopen, setViewForm] = React.useState(false);
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const [values, setValues] = React.useState({
    name: '',
    line1: '',
    city: '',
    state: '',
    country: '',
    zip: '',
  });
  const [ErrMsg, setErrMsg] = React.useState(false);
  const [errorInfo, setErrorInfo] = React.useState({
    name: false,
    line1: false,
    city: false,
    state: false,
    country: false,
    zip: false,
  });
  const handleSubmit = async () => {
    console.log(values);
    setErrMsg(false);
    if (
      values.addressNameID === '' ||
      values.addressLines1 === '' ||
      values.city === '' ||
      values.state === '' ||
      values.country === '' ||
      values.zip === ''
    ) {
      setErrMsg(true);
    } else {
      setErrMsg(false);
      setValues({
        ...values,
        name: '',
        line1: '',
        city: '',
        state: '',
        country: '',
        zip: '',
      });
    }
  };

  useEffect(() => {
    console.log(addrestypeData);
    if (data !== undefined) {
      console.log(data.fetchAddressByPartnerProfile);
      setData(data.fetchAddressByPartnerProfile);
      setServerError(false);
    } else {
      console.log('server error');
      setServerError(true);
    }
    if (
      addrestypeData !== undefined &&
      addrestypeData.data !== undefined &&
      addrestypeData.data.allAddressTypes.length > 0
    ) {
      let addressTypeDetails = addrestypeData.data.allAddressTypes;
      console.log(addressTypeDetails);

      /*   var objectVal = addressTypeDetails.map((v, k) => {
        return (
          <MenuItem value={v.addressTypeUUID}>{v.addressTypeName}</MenuItem>
        );
      });*/
      var objectVal = addressTypeDetails.reduce(
        (obj, item) => (
          (obj[item.addressTypeUUID] = item.addressTypeName), obj
        ),
        {}
      );
      setAddressTYpe(objectVal);
    }
  }, [data]);
  const handleRefetch = (value, message) => {
    refetch({
      updateData(_, data) {
        return data;
      },
    });
    setOpenSnackbar(true);
    setSnackbarMessage(message);
    if (value) {
      setSnackbarVariant('error');
    } else {
      setSnackbarVariant('success');
    }
  };
  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }

  const classes = makeStyles(iconStyles)();
  const tableIcons = {
    Add: forwardRef((props, ref) => (
      <AddBox color="primary" {...props} ref={ref} />
    )),
    Check: forwardRef((props, ref) => (
      <Check color="primary" {...props} ref={ref} />
    )),
    Clear: forwardRef((props, ref) => (
      <Clear color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline
        color="error"
        {...props}
        ref={ref}
      />
    )),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => (
      <Edit  color="primary" {...props} ref={ref} />
    )),
    Export: forwardRef((props, ref) => (
      <SaveAlt color="primary" {...props} ref={ref} />
    )),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => (
      <FirstPage color="primary" {...props} ref={ref} />
    )),
    LastPage: forwardRef((props, ref) => (
      <LastPage color="primary" {...props} ref={ref} />
    )),
    NextPage: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft color="primary" {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => (
      <Clear color="primary" {...props} ref={ref} />
    )),
    Search: forwardRef((props, ref) => (
      <Search color="primary" {...props} ref={ref} />
    )),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };

  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSelect: {
          select: {
            minWidth: 70,
          },
          icon: {
            color: '#0b153e',
          },
        },
        MuiTypography: {
          h6: {
            fontSize: 14,
          },
        },

        MuiTableSortLabel: {
          root: {
            color: '#ffffff !important',
          },
        },
        MuiDialogTitle: {
          root: {
            backgroundColor: '#0b153e',
            color: '#FFFFFF',
          },
        },
        MuiTableSortLabel: {
          root: {
            hover: {
              color: '#ffffff !important',
            },
          },
          MuiTableSortLabel: {
            root: {
              MuiTableSortLabel: {
                active: {
                  color: '#ffffff !important',
                },
              },
            },
          },

          MuiDialogTitle: {
            root: {
              backgroundColor: '#0b153e',
              color: '#FFFFFF',
            },
          },
          MuiTablePagination: {
            input: {
              color: '#0b153e',
            },
          },
          MuiSvgIcon: {
            root: {
              color: '#0b153e',
            },
          },
          MuiGrid: {
            container: {
              flexWrap: 'nowrap',
            },
          },
        },
      },
    });
  const classes1 = useStyles();

  function handleShowComponent1(rowData) {
    console.log(rowData.status);
    if (rowData !== undefined) {
      if (rowData.addressStatus === 'Active') {
        rowData.addressStatus = false;
        console.log(rowData);
        handleUpdateAddress(rowData);
      } else {
        rowData.addressStatus = true;
        handleUpdateAddress(rowData);
      }
    }
  }
  async function handleAddAddress(newData) {
    return await new Promise(async (resolve) => {
      resolve();
      console.log(newData);
      await addAddress({
        variables: {
          input: {
            addressNameID: newData.addressNameID,
            addressLines1: newData.addressLines1,
            addressLines2: newData.addressLines2,
            addressLines3: newData.addressLines3,
            city: newData.city,
            county: newData.county,
            state: newData.state,
            country: newData.country,
            zip: newData.zip,
            addressStatus:
              newData.addressStatus == true ? 'Active' : 'Inactive',
            addressTypeUUID: newData.addressTypeUUID,
            partnerProfileUUID: props.uuid,
            contactUUID: null,
          },
        },
      })
        .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
          console.log(data, error, graphQLErrors, networkError, cacheHit);
          if (data !== null && error == false) {
            handleRefetch(error, 'Saved successfully');
          } else if (error && graphQLErrors.length > 0) {
            let duperror = graphQLErrors[0].message;
            console.log(duperror);
            if (duperror !== null && duperror.indexOf('ER_DUP_ENTRY') !== -1) {
              handleRefetch(error, 'Name already existed');
            }
          }
        })
        .catch((e) => {
          // you can do something with the error here
          console.log(e);
        });
    });

    /* if (addloading.loading) {
        console.log('address not created');
        handleRefetch(addloading.loading, 'Graphql hooks error');
      } else {
        console.log('address created successfully');
        handleRefetch(addloading.loading, 'Saved successfully');
      }
    });*/
  }
  async function handleUpdateAddress(newData, oldData) {
    return await new Promise(async (resolve) => {
      resolve();
      console.log(newData, oldData);
      await updateAddress({
        variables: {
          input: {
            addressNameID: newData.addressNameID,
            addressLines1: newData.addressLines1,
            addressLines2: newData.addressLines2,
            addressLines3: newData.addressLines3,
            city: newData.city,
            state: newData.state,
            country: newData.country,
            zip: newData.zip,
            addressStatus:
              newData.addressStatus == true ? 'Active' : 'Inactive',
            addressTypeUUID: newData.addressTypeUUID,
            partnerProfileUUID: props.uuid,
            addressUUID: newData.addressUUID,
          },
        },
      })
        .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
          console.log(data, error, graphQLErrors, networkError, cacheHit);
          if (data !== null && error == false) {
            console.log(newData.addressStatus, newData.addressUUID);
            handleRefetch(error, 'Updated successfully');
            /*   updateContact({
              variables: {
                addressUUID: newData.addressUUID,
              },
            });
            console.log(conatctdata, contactloading);
         ({
                  contactdata,
                  contacterror,
                  graphQLErrors,
                  networkError,
                  cacheHit,
                }) => {
                  if (contactdata !== null && contacterror == false) {
                    console.log(
                      contactdata,
                      contacterror,
                      graphQLErrors,
                      networkError,
                      cacheHit
                    );
                    handleRefetch(contacterror, 'Updated successfully');
                  }
                }
              )
              .catch((e) => {
                // you can do something with the error here
                console.log(e);
              });*/
          } else if (error && graphQLErrors.length > 0) {
            let duperror = graphQLErrors[0].message;
            console.log(duperror);
            if (duperror !== null && duperror.indexOf('ER_DUP_ENTRY') !== -1) {
              handleRefetch(error, 'Name already existed');
            }
          }
        })
        .catch((e) => {
          // you can do something with the error here
          console.log(e);
        });
    });
    /*  if (updateloading.loading) {
        console.log('address not updated');
        handleRefetch(updateloading.loading, 'Graphql hooks error');
      } else {
        console.log('address updated successfully');
        handleRefetch(updateloading.loading, 'Updated successfully');
      }
    });*/
  }
  async function handleRemoveAddress(oldData) {
    return await new Promise(async (resolve) => {
      resolve();
      await removeAddress({
        variables: {
          input: {
            addressUUID: oldData.addressUUID,
          },
        },
      })
        .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
          console.log(data, error, graphQLErrors, networkError, cacheHit);
          if (data !== null && error == false) {
            handleRefetch(error, 'Deleted successfully');
          } else if (error && graphQLErrors.length > 0) {
            let duperror = graphQLErrors[0].message;
            console.log(duperror);
            if (
              duperror !== null &&
              duperror.indexOf('ER_ROW_IS_REFERENCED') !== -1
            ) {
              setOpenSnackbar(true);
              setSnackbarVariant('error');
              setSnackbarMessage('Selected row is referenced in Contact table');
            }
          }
        })
        .catch((e) => {
          // you can do something with the error here
          console.log(e);
        });
    });
  }
  const handleSearchValue = (val) => {
    console.log(val);
    setSearchValue(val);
  };
  const handleClickOpen = (e, rowData) => {
    console.log(e, rowData);
    setAddressRowData(rowData);
    setViewForm(true);
  };
  const handleCloseAddress = () => {
    setViewForm(false);
  };
  if (loading) return <Loader />;
  if (serverError)
    return <Error type={'Server connection lost.Please try again'} />;
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <MaterialTable
        icons={tableIcons}
        title=""
        columns={columns}
        data={addressdata}
        //onSearchChange={(e) => handleSearchValue(e)}
        editable={{
          onRowAdd: (newData) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                newData.submitted = true;
                if (
                  !newData.addressNameID ||
                  !newData.addressLines1 ||
                  !newData.city ||
                  !newData.state ||
                  !newData.country ||
                  !newData.zip ||
                  !addressNameIDexp.test(newData.addressNameID) ||
                  !line1exp.test(newData.addressLines1) ||
                  !cityexp.test(newData.city) ||
                  !stateexp.test(newData.state) ||
                  !countryexp.test(newData.country) ||
                  !zipexp.test(newData.zip) ||
                  !newData.addressTypeUUID ||
                  !line1exp.test(newData.addressLines3) ||
                  !line1exp.test(newData.addressLines2)
                ) {
                  setNameError({
                    error: true,
                    label: 'required',
                    helperText: 'Required.',
                    validateInput: true,
                  });
                  reject();
                  return;
                }
                resolve();

                handleAddAddress(newData);
              }, 600);
            }),

          onRowUpdate: (newData, oldData) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                newData.submitted = true;
                if (
                  !newData.addressNameID ||
                  !newData.addressLines1 ||
                  !newData.city ||
                  !newData.state ||
                  !newData.country ||
                  !newData.zip ||
                  !addressNameIDexp.test(newData.addressNameID) ||
                  !line1exp.test(newData.addressLines1) ||
                  !cityexp.test(newData.city) ||
                  !stateexp.test(newData.state) ||
                  !countryexp.test(newData.country) ||
                  !zipexp.test(newData.zip) ||
                  !newData.addressTypeUUID ||
                  !line1exp.test(newData.addressLines3) ||
                  !line1exp.test(newData.addressLines2)
                ) {
                  setNameError({
                    error: true,
                    label: 'required',
                    helperText: 'Required.',
                    validateInput: true,
                  });
                  reject();
                  return;
                }
                resolve();
                handleUpdateAddress(newData, oldData);
              }, 600);
            }),
          onRowDelete: (oldData) => handleRemoveAddress(oldData),
        }}
        options={{
          headerStyle: {
            textAlign: 'center',
            fontSize: 12,
            fontWeight: 'bold',
            fontFamily: 'Arial !important',
            backgroundColor: '#0b153e',
            color: '#ffffff',
            padding: '4px',
          },
          searchFieldStyle: {
            color: '#0b153e',
          },
          actionsColumnIndex: -1,
        }}
        actions={[
          {
            icon: VisibilityIcon,
            tooltip: 'Address View',
            onClick: (event, rowData) => handleClickOpen(event, rowData),
          }
        ]}
       // onRowClick={(event, rowData) => handleClickOpen(event, rowData)}
      />
      <AddressViewModelForm
        handleCloseModal={handleCloseAddress}
        isModalOpen={viewopen}
        title={'Address'}
      >
        <AddressView
          handleCloseModal={handleCloseAddress}
          isModalOpen={viewopen}
          rowData={addressRowData}
        />
      </AddressViewModelForm>
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        autoHideDuration={1500}
        open={openSnackbar}
        onClose={handleCloseSnackbar}
      >
        <MySnackbarContentWrapper
          onClose={handleCloseSnackbar}
          variant={variant}
          message={message}
        />
      </Snackbar>
    </MuiThemeProvider>
  );
}
