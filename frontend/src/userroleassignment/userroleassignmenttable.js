import MaterialTable from 'material-table';
import { forwardRef, useEffect } from 'react';
import { useMutation, useQuery, useManualQuery } from 'graphql-hooks';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import ReactDOM from 'react-dom';
import MTableToolbar from 'material-table';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import CloseIcon from '@material-ui/icons/Close';
import { withStyles } from '@material-ui/core/styles';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import { Grid, Typography } from '@material-ui/core';
import MuiDialogActions from '@material-ui/core/DialogActions';
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import Timestamp from '../timestamp';
import Theme from '../css/theme';
import Loader from '../components/loader';
import Error from '../components/emptyPage';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import CircularIndeterminateLoader from '../components/loader';
import MenuItem from '@material-ui/core/MenuItem';
import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles,
} from '@material-ui/core/styles';
function iconStyles() {
  return {};
}

const DISPLAY_All_USERROLEASSIGNED = `{
  allUserRoleAssignment {
    userUUID
  userRoleUUID
  userOrgUUID
  userTypeUUID
  idUserRoleAssignment
  userRoleAssignmentUUID
    createdOn
    lastUpdatedOn
  } 
}
`;

const DISPLAY_All_USER = `{
  allUsers {    
    userUUID
    userID       
  } 
}
`;
const DISPLAY_All_USER_ROLETYPES = `{
  allUserRoleTypes{  
    userRoleTypeUUID
    userRoleTypeName   
  }
}
`;
const DISPLAY_All_USERTYPES = `{
  allUserTypes {   
    userTypesUUID
    userTypesName   
  }
}
`;
const DISPLAY_All_ORGANIZATION = `query fetchOrganization($searchInput:String){
  fetchOrganization(searchInput:$searchInput) {  
    organizationUUID
    organizatonName 
  } 
}
`;
const DISPLAY_PAGINATED_USERROLEASSIGNEDS = `query fetchUsers($limit:Int,$cursor:Int){
  fetchUsers(limit:$limit,cursor:$cursor){
    edges{
      idUser
    userUUID
    userID
    userPassword
    loginType
    createdOn
    lastUpdatedOn
      }
      pageInfo{
        endCursor
      }
  }
}`;

const ADD_USERROLEASSIGNED = `mutation createUserRoleAssignment($inputCreate:UserRoleAssignmentInput){
  createUserRoleAssignment(input:$inputCreate)
  {
    userUUID
    userRoleUUID
    userOrgUUID
    userTypeUUID
  }
}`;
const UPDATE_USERROLEASSIGNED = `mutation updateUserRoleAssignment($input:UserRoleAssignmentInput){
  updateUserRoleAssignment(input:$input) {
    idUserRoleAssignment
    userRoleAssignmentUUID
    userUUID
    userRoleUUID
    userOrgUUID
    userTypeUUID
    createdOn
    lastUpdatedOn
    organizatonName
  } 
}`;
const REMOVE_USERROLEASSIGNED = `mutation removeUserRoleAssignment($input:UserRoleAssignmentInput){
  removeUserRoleAssignment(input:$input) {
    idUserRoleAssignment
    userRoleAssignmentUUID
    userUUID
    userRoleUUID
    userOrgUUID
    userTypeUUID
    createdOn
    lastUpdatedOn
    organizatonName
  } 
}
`;
//INSERT INTO `user`(`iduser`, `userUUID`, `userID`, `userPassword`, `loginType`, `createdon`, `usercol`) VALUES (2,"22","24","pwd","admin",NOW(),NOW())
const updatedData = (prevData, data) => ({
  ...data,
  allData: [...prevData.allData, ...data.allData],
});
const limit = 3;
const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(2),
    },
  },

  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));
const styles = (theme) => ({
  root: {
    Width: 1200,
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: '#FFFFFF',
  },
});
const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

export default function MaterialTableDemo() {
  const [userData, setUserData] = React.useState([]);
  const [cursorVal, setCursorVal] = React.useState(1);
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [message, setSnackbarMessage] = React.useState();
  const [serverError, setServerError] = React.useState(false);
  const [variant, setSnackbarVariant] = React.useState('error');
  const [totalCount, setTotalCount] = React.useState(0);
  const [manualQuery, setManualQuery] = React.useState(0);
  const [usersUUID, setusersUUID] = React.useState([]);
  const [userRoleUUID, setuserRoleUUID] = React.useState([]);
  const [userTypeUUID, setuserTypeUUID] = React.useState([]);
  const [userOrgUUID, setuserOrgUUID] = React.useState([]);
  const {
    loading: ploading,
    error: perror,
    data: UserData,
    refetch: prefetch,
  } = useQuery(DISPLAY_All_USER);
  const {
    loading: iloading,
    error: ierror,
    data: UserRoleTypeData,
    refetch: irefetch,
  } = useQuery(DISPLAY_All_USER_ROLETYPES);

  const {
    loading: pjloading,
    error: pjerror,
    data: UserTypeData,
    refetch: pjrefetch,
  } = useQuery(DISPLAY_All_USERTYPES);

  const {
    loading: dtloading,
    error: dterror,
    data: OrganizationData,
    refetch: dtrefetch,
  } = useQuery(DISPLAY_All_ORGANIZATION, {
    variables: {
      searchInput: '',
    },
  });

  {
    /*const [
    paginatedData,
    { loading: mloading, error: merror, data: mdata, refetch: refetch },
  ] = useManualQuery(DISPLAY_PAGINATED_USERROLEASSIGNEDS, {
    variables: {
      limit: limit,
      cursor: 0,
    },
  });*/
  }

  //EDIT user
  const [updateUser, updateloading, updatedata] = useMutation(
    UPDATE_USERROLEASSIGNED
  );
  //remove user
  const [removeUser, removeloading, removedata] = useMutation(
    REMOVE_USERROLEASSIGNED
  );
  //ADD USER ROLE TYPES
  const [nameError, setNameError] = React.useState({
    error: false,
    label: '',
    helperText: '',
    validateInput: false,
  });
  const [addUserRoleAssign, addloading, adddata] = useMutation(
    ADD_USERROLEASSIGNED
  );
  //FETCH ALL USER ROLE TYPES
  //if(manualQuery===0)
  // {
  /*const {
    loading,
    error,
    graphQLErrors,
    networkError,
    data,
    cacheHit
   } = useQuery(DISPLAY_PAGINATED_USERROLEASSIGNEDS,{
     variables:{
       limit:limit,
       cursor:cursorVal
     }
   });*/
  //}
  const {
    loading,
    error,
    graphQLErrors,
    networkError,
    data,
    cacheHit,
    refetch,
  } = useQuery(DISPLAY_All_USERROLEASSIGNED);

  const columns = [
    {
      title: 'UUID',
      field: 'userRoleAssignmentUUID',
      hidden: true,
      editable: 'never',
    },
    {
      title: 'User',
      field: 'userUUID',
      lookup: usersUUID,
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              //placeholder="Description"
              margin="normal"
              select
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            >
              {UserData != undefined && UserData.allUsers.length > 0
                ? UserData.allUsers.map((v, k) => {
                    return <MenuItem value={v.userUUID}>{v.userID}</MenuItem>;
                  })
                : null}
            </TextField>
          </div>
        );
      },
    },
    {
      title: 'Role',
      field: 'userRoleUUID',
      lookup: userRoleUUID,
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              //placeholder="Description"
              margin="normal"
              select
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            >
              {UserRoleTypeData != undefined &&
              UserRoleTypeData.allUserRoleTypes.length > 0
                ? UserRoleTypeData.allUserRoleTypes.map((v, k) => {
                    return (
                      <MenuItem value={v.userRoleTypeUUID}>
                        {v.userRoleTypeName}
                      </MenuItem>
                    );
                  })
                : null}
            </TextField>
          </div>
        );
      },
    },
    {
      title: 'Organization',
      field: 'userOrgUUID',
      lookup: userOrgUUID,
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              //placeholder="Description"
              margin="normal"
              select
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            >
              {OrganizationData != undefined &&
              OrganizationData.fetchOrganization.length > 0
                ? OrganizationData.fetchOrganization.map((v, k) => {
                    return (
                      <MenuItem value={v.organizationUUID}>
                        {v.organizatonName}
                      </MenuItem>
                    );
                  })
                : null}
            </TextField>
          </div>
        );
      },
    },
    {
      title: 'Type',
      field: 'userTypeUUID',
      lookup: userTypeUUID,
     /*{ editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
            
              margin="normal"
              select
              error={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.error
                  : false
              }
              helperText={
                !props.value &&
                nameError.validateInput &&
                props.rowData.submitted
                  ? nameError.helperText
                  : ''
              }
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            >
              {UserTypeData != undefined && UserTypeData.allUserTypes.length > 0
                ? UserTypeData.allUserTypes.map((v, k) => {
                    return (
                      <MenuItem value={v.userTypesUUID}>
                        {v.userTypesName}
                      </MenuItem>
                    );
                  })
                : null}
            </TextField>
          </div>
        );
      }*/
    },
    {
      title: 'Created',
      field: 'createdOn',
      editable: 'never',
      render: (rowData) => {
        console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.createdOn);
        } else {
          return;
        }
      },
    },
    {
      title: 'Last Modified',
      field: 'lastUpdatedOn',
      editable: 'never',
      render: (rowData) => {
        console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.lastUpdatedOn);
        } else {
          return;
        }
      },
    },
  ];
  console.log(data);

  const handleNextpage = () => {};
  useEffect(() => {
    if (data !== undefined) {
      console.log(data.allUserRoleAssignment);
      setUserData(data.allUserRoleAssignment);
      // setTotalCount(data.fetchUsers.totalCount);
      // setState({ data: data.allUserTypes });
      /*    setState(prevState => {
        const data1 = [...prevState.data];
        console.log(data1);
        data1.push(data.allUserTypes);
        return { ...prevState, data1 };
      });*/
      setServerError(false);
    } else {
      console.log('server error');
      setServerError(true);
    }

    if (UserData !== undefined) {
      var UserDataDD = UserData.allUsers.reduce(
        (obj, item) => ((obj[item.userUUID] = item.userID), obj),
        {}
      );
      console.log(UserDataDD);
      setusersUUID(UserDataDD);
    }

    if (UserRoleTypeData !== undefined) {
      var UserRoleTypeDataDD = UserRoleTypeData.allUserRoleTypes.reduce(
        (obj, item) => (
          (obj[item.userRoleTypeUUID] = item.userRoleTypeName), obj
        ),
        {}
      );
      console.log(UserRoleTypeDataDD);
      setuserRoleUUID(UserRoleTypeDataDD);
    }

    if (UserTypeData !== undefined) {
      var UserTypeDataDD = UserTypeData.allUserTypes.reduce(
        (obj, item) => ((obj[item.userTypesUUID] = item.userTypesName), obj),
        {}
      );
      console.log(UserTypeDataDD);
      setuserTypeUUID(UserTypeDataDD);
    }

    if (OrganizationData !== undefined) {
      var OrganizationDataDD = OrganizationData.fetchOrganization.reduce(
        (obj, item) => (
          (obj[item.organizationUUID] = item.organizatonName), obj
        ),
        {}
      );
      console.log(OrganizationDataDD);
      setuserOrgUUID(OrganizationDataDD);
    }
  }, [data]);
  const handleRefetch = (value, message) => {
    refetch({
      updateData(_, data) {
        return data;
      },
    });
    setOpenSnackbar(true);
    setSnackbarMessage(message);
    if (value) {
      setSnackbarVariant('error');
    } else {
      setSnackbarVariant('success');
    }
  };
  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }
  //data:data===undefined?'':data['allUsers'],
  const classes = makeStyles(iconStyles)();
  const tableIcons = {
    Add: forwardRef((props, ref) => (
      <AddBox color="primary" {...props} ref={ref} />
    )),
    Check: forwardRef((props, ref) => (
      <Check color="primary" {...props} ref={ref} />
    )),
    Clear: forwardRef((props, ref) => (
      <Clear color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline color="error" {...props} ref={ref} />
    )),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => (
      <Edit
        {...props}
        ref={ref}
        color="primary"
        onClick={console.log('hello world')}
      />
    )),
    Export: forwardRef((props, ref) => (
      <SaveAlt color="primary" {...props} ref={ref} />
    )),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => (
      <FirstPage color="primary" {...props} ref={ref} />
    )),
    LastPage: forwardRef((props, ref) => (
      <LastPage color="primary" {...props} ref={ref} />
    )),
    NextPage: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft color="primary" {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => (
      <Clear color="primary" {...props} ref={ref} />
    )),
    Search: forwardRef((props, ref) => (
      <Search color="primary" {...props} ref={ref} />
    )),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };
  const [open, setOpen] = React.useState(false);
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiTypography: {
          h6: {
            fontSize: 14,
            fontFamily: 'Arial !important',
          },
        },
        MuiDialogTitle: {
          root: {
            backgroundColor: '#0b153e',
            color: '#FFFFFF',
          },
        },
        MuiSelect: {
          icon: {
            color: '#0b153e',
          },
        },
        MuiGrid: {
          container: {
            flexWrap: 'nowrap',
          },
        },
      },
    });

  if (loading) return <CircularIndeterminateLoader />;
  if (loading && data !== undefined) return <div>Loading</div>;

  async function handleAddUserRoleAssign(newData) {
    return await new Promise(async (resolve) => {
      resolve();
      console.log(newData);

      await addUserRoleAssign({
        variables: {
          inputCreate: {
            userUUID: newData.userUUID,
            userRoleUUID: newData.userRoleUUID,
            userOrgUUID: newData.userOrgUUID,
            userTypeUUID: newData.userTypeUUID,
          },
        },
      });

      if (addloading.loading) {
        console.log('user created');
        handleRefetch(addloading.loading, 'Graphql hooks error');
      } else {
        console.log('user created successfully');
        handleRefetch(addloading.loading, 'Saved successfully');
      }
    });
  }
  async function handleUpdateUserRoleAssign(newData, oldData) {
    return await new Promise(async (resolve) => {
      resolve();
      console.log(newData);
      await updateUser({
        variables: {
          input: {
            userRoleAssignmentUUID: newData.userRoleAssignmentUUID,
            userUUID: newData.userUUID,
            userRoleUUID: newData.userRoleUUID,
            userOrgUUID: newData.userOrgUUID,
            userTypeUUID: newData.userTypeUUID,
          },
        },
      });
      if (updateloading.loading) {
        console.log('user not updated');
        handleRefetch(updateloading.loading, 'Graphql hooks error');
      } else {
        console.log('user updated successfully');
        handleRefetch(updateloading.loading, 'Updated successfully');
      }
    });
  }
  async function handleRemoveUserRoleAssign(oldData) {
    return await new Promise(async (resolve) => {
      resolve();
      await removeUser({
        variables: {
          input: {
            userRoleAssignmentUUID: oldData.userRoleAssignmentUUID,
          },
        },
      });
      console.log(removeloading, removedata);
      if (removeloading.loading) {
        console.log('User Role  not deleted');
        handleRefetch(removeloading.loading, 'Graphql hooks error');
      } else {
        console.log('user deleted successfully');
        handleRefetch(removeloading.loading, 'Deleted successfully');
      }
    });
  }
  if (loading) return <Loader />;
  if (serverError)
    return <Error type={'Server connection lost.Please try again'} />;
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <MaterialTable
        icons={tableIcons}
        title="User Role Assignment"
        columns={columns}
        data={userData}
        editable={{
          onRowAdd: (newData) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                newData.submitted = true;
                if (
                  !newData.userUUID ||
                  !newData.userRoleUUID ||
                  !newData.userOrgUUID 
                ) {
                  setNameError({
                    error: true,
                    label: 'required',
                    helperText: 'Required.',
                    validateInput: true,
                  });
                  reject();
                  return;
                }
                resolve();
                handleAddUserRoleAssign(newData);
              }, 600);
            }),
          onRowUpdate: (newData, oldData) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                newData.submitted = true;
                if (
                  !newData.userUUID ||
                  !newData.userRoleUUID ||
                  !newData.userOrgUUID 
                ) {
                  setNameError({
                    error: true,
                    label: 'required',
                    helperText: 'Required',
                    validateInput: true,
                  });
                  reject();
                  return;
                }
                resolve();
                handleUpdateUserRoleAssign(newData, oldData);
              }, 600);
            }),
          onRowDelete: (oldData) => handleRemoveUserRoleAssign(oldData),
        }}
        options={{
          headerStyle: {
            textAlign: 'center',
            fontSize: 12,
            fontWeight: 'bold',
            fontFamily: 'Arial !important',
            backgroundColor: '#0b153e',
            color: '#ffffff',
            padding: '4px',
          },
          searchFieldStyle: {
            color: '#0b153e',
          },
          actionsColumnIndex: -1,
          pageSize:10,
          pageSizeOptions: [10, 25 ,50,100 ],
          toolbar: true,
          paging: true
        }}
        onRowClick={handleClickOpen}
      />
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        autoHideDuration={1500}
        open={openSnackbar}
        onClose={handleCloseSnackbar}
      >
        <MySnackbarContentWrapper
          onClose={handleCloseSnackbar}
          variant={variant}
          message={message}
        />
      </Snackbar>
    </MuiThemeProvider>
  );
}