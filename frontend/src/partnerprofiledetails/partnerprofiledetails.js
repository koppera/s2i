import React, { useEffect } from 'react';
import { makeStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { useMutation, useQuery, useManualQuery } from 'graphql-hooks';
import Button from '@material-ui/core/Button';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import Grid from '@material-ui/core/Grid';
import '../css/commonStyle.css';
import Typography from '@material-ui/core/Typography';
import SearchPartner from './searchPartner';
import PartnerTabs from './partnerTabs';
import Theme from '../css/theme';
import { useTheme, createMuiTheme } from '@material-ui/core/styles';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import DoubleArrowIcon from '@material-ui/icons/DoubleArrow';
import IconButton from '@material-ui/core/IconButton';
import SendIcon from '@material-ui/icons/Send';
import Tooltip from '@material-ui/core/Tooltip';
import Paper from '@material-ui/core/Paper';
const API_CAll_DATA = `query allPartnerDetailsAPI($SearchInput:String){ 
  allPartnerDetailsAPI(search:$SearchInput){
    statusAPI
    msg
    partnerID
  }
}`;
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
}));

const getMuiTheme = () =>
  createMuiTheme(Theme, {
    overrides: {
      MuiTypography: {
        root: {
          fontSize: '10px',
          paddingBottom: '8px',
          paddingLeft: '22px',
        },
        h6: {
          color: '#0b153e',
        },
      },
      MuiTypography: {
        root: {
          paddingLeft: 0,
        },
      },

      MuiGrid: {
        container: {
          marginLeft: '2%',
        },
      },
      MuiTypography: {
        body1: {
          fontSize: 10,
        },
      },
    },
  });

export default function PartnerProfileDetails(props) {
  const classes = useStyles();
  const [TypeUUID, setTypeUUID] = React.useState('');
  const [ContactTypeUUID, setContactTypeUUID] = React.useState('');
  const [partnerProfileuuid, setPartnerProfilUUID] = React.useState(props.uuid);
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [message, setSnackbarMessage] = React.useState();
  const [variant, setSnackbarVariant] = React.useState('error');
  const backToPartnerProfile = () => {
    props.handleShowComponent(false);
  };
  const [fetchProfile] = useManualQuery(API_CAll_DATA);
  useEffect(() => {
    if (props.uuid !== undefined) {
      setPartnerProfilUUID(props.uuid);
      localStorage.setItem('partnerprofileuuid', props.uuid);
    }
    console.log(props.resultVal);
    if (
      props.resultVal !== undefined &&
      localStorage.getItem('partnerprofileuuid') == props.UUID
    ) {
      if (props.resultVal) {
        handleRefetch(true, 'ProfileID values Combination already exist');
      } else {
        handleRefetch(false, 'Saved Successfully');
      }
    }
  }, [props, partnerProfileuuid]);
  const setProfileUUID = (uuid) => {
    console.log(uuid);
    setPartnerProfilUUID(uuid);
    props.profilUUID(uuid);
  };
  const handleRefetch = (value, message) => {
    console.log(value, message);
    setOpenSnackbar(true);
    setSnackbarMessage(message);
    if (value) {
      setSnackbarVariant('error');
    } else {
      setSnackbarVariant('success');
    }
  };
  const handleCallAPI = async () => {
    await fetchProfile({
      variables: {
        SearchInput: partnerProfileuuid,
      },
    }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
      console.log(data, error, graphQLErrors, networkError, cacheHit);
      if (data !== null && error == false) {
        handleAPISuccess(
          data.allPartnerDetailsAPI.statusAPI,
          data.allPartnerDetailsAPI.msg
        );
      } else if (
        error &&
        graphQLErrors != undefined &&
        graphQLErrors.length > 0
      ) {
        let Unexperror = graphQLErrors[0].message;
        console.log(Unexperror);
        if (
          Unexperror !== null &&
          Unexperror.indexOf('Unexpected error value') !== -1
        ) {
          setOpenSnackbar(true);
          setSnackbarVariant('error');
          setSnackbarMessage('Searched Partner does not exist');
        }
      } else {
        console.log(error, 'graphql hooks error');
        handleRefetch(error, 'graphql hooks error');
      }
    });
  };
  const handleAPISuccess = (Status, aPIMsg) => {
    console.log('MsgAPI', aPIMsg);
    if (Status === 'failure') {
      handleRefetch(true, aPIMsg);
    } else {
      handleRefetch(false, aPIMsg);
    }
  };

  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <div className={classes.root}>
        <Breadcrumbs
          separator={<DoubleArrowIcon fontSize="small" />}
          aria-label="breadcrumb"
        >
          <Link color="inherit" href="/">
            Partner Profile
          </Link>
          <text color="inherit">Details</text>
        </Breadcrumbs>
        <Grid container spacing={3}>
          <Grid item item xs={11} sm={11}>
            <Typography variant="h6" component="h2">
              <ArrowBackIosIcon
                className="backarrow"
                onClick={backToPartnerProfile}
              />
              Partner Profile Details
            </Typography>
          </Grid>
          <Grid
            item
            xs={1}
            sm={1}
            style={{ display: partnerProfileuuid != '' ? 'block' : 'none' }}
          >
            <Tooltip title="Send">
              <IconButton>
                <SendIcon
                  onClick={handleCallAPI}
                  style={{ color: '#0b153e' }}
                />
              </IconButton>
            </Tooltip>
          </Grid>
          <Grid item xs={12}>
            <SearchPartner
              UUID={partnerProfileuuid}
              profilUUID={setProfileUUID}
              onSuccess={handleRefetch}
            />
          </Grid>
          {partnerProfileuuid != '' ? (
            <Grid item xs={12}>
              <PartnerTabs UUID={partnerProfileuuid} />
            </Grid>
          ) : (
            <Paper className="paperorgpage">
              Searched Partner does not exist
            </Paper>
          )}
        </Grid>
      </div>
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        autoHideDuration={1500}
        open={openSnackbar}
        onClose={handleCloseSnackbar}
      >
        <MySnackbarContentWrapper
          onClose={handleCloseSnackbar}
          variant={variant}
          message={message}
        />
      </Snackbar>
    </MuiThemeProvider>
  );
}
