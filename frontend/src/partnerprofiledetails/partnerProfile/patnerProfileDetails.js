import React, { useEffect } from 'react';
import { makeStyles, MuiThemeProvider } from '@material-ui/core/styles';
import {useMutation, useQuery} from 'graphql-hooks';
import Button from '@material-ui/core/Button';
import MySnackbarContentWrapper from "../components/Snackbar";
import Snackbar from "@material-ui/core/Snackbar";
import Grid from "@material-ui/core/Grid";
import '../css/commonStyle.css';
import Typography from'@material-ui/core/Typography';
import SearchPartner from './searchPartner';
import PartnerTabs from './partnerTabs';
import Theme from '../css/theme';
import {
  useTheme,
  createMuiTheme
} from '@material-ui/core/styles';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';
 
const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  }
  }));

  const getMuiTheme = () =>
  createMuiTheme(Theme, {
    overrides: {
      MuiTypography:{
        h6:{
   color:'#0b153e',



      }
    }
        
    }
  });

 
 
  
export default function () {
    const classes = useStyles();
    const [TypeUUID, setTypeUUID] = React.useState('');
    const [ContactTypeUUID, setContactTypeUUID] = React.useState('');
    console.log(window.location)
    let search = window.location.search;
    let params = new URLSearchParams(search);
    let id = params.get('id')
     id="aca91f69-7269-11ea-a315-54e1ade7ee5f";
   
     
      const [state, setState] = React.useState({
        checkedA: true,
        checkedB: true,
      });
    
     
        const handleChangetoggle = (event) => {
          setState({ ...state, [event.target.name]: event.target.checked });
        };
        function handleClick(event) {
          event.preventDefault();
          console.info('You clicked a breadcrumb.');
        }
            
    return (  
    
      <MuiThemeProvider theme={Theme,getMuiTheme()}>
      
   {/*}  <Breadcrumbs aria-label="breadcrumb">
      <Link color="inherit" href="/" onClick={handleClick}>
        PartnerProfile
      </Link>
      <Link color="inherit" href="/getting-started/installation/" onClick={handleClick}>
        
      </Link>
     
    </Breadcrumbs>
    */}



      <div className={classes.root}>
      <Grid container spacing={3}>
         
          <Grid item xs={12} >
      <Typography variant="h6" component="h2"
      >
  Partner Profile Details
</Typography>
      </Grid>
          <Grid item xs={12} >
            <SearchPartner UUID={id}/>
          
          </Grid>
          <Grid item xs={12} >
            <PartnerTabs/>
          
          </Grid>

        
          </Grid>
      </div>
      
      </MuiThemeProvider>
  );
}   