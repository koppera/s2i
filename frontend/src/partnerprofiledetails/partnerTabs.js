import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import ProfileIdValues from '../ProfileIDValues/profileidvalues';
import Address from '../Address/addresstable';
import Contact from '../Contact/contact';
import Delivery from '../ProtocolType/ProtocolTypePanel';
import ProtocolCertificate from '../protocolcertificate/protocolcertificatetable.js';
import Theme from '../css/theme';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </Typography>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));

const getMuiTheme = () =>
  createMuiTheme(Theme, {
    overrides: {
      MuiTab: {
        root: {
          paddingRight: 8,
        },
      },
    },
  });

export default function PartnerTabs(props) {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  console.log('props.uuid', props.UUID);
  return (
    <div className={classes.root}>
      <MuiThemeProvider theme={getMuiTheme()}>
        <AppBar position="static">
          <Tabs
            value={value}
            onChange={handleChange}
            aria-label="simple tabs example"
          >
            <Tab label="External ID's" {...a11yProps(0)} />
            <Tab label="Address" {...a11yProps(1)} />
            <Tab label="Contact" {...a11yProps(2)} />
            <Tab
              style={{ paddingRight: 35 }}
              label="Delivery"
              {...a11yProps(3)}
            />
            <Tab  style={{ paddingRight: 35 }} label="Certificates" />
          </Tabs>
        </AppBar>
        <TabPanel value={value} index={0}>
          <ProfileIdValues uuid={props.UUID} />
        </TabPanel>
        <TabPanel value={value} index={1}>
          <Address uuid={props.UUID} />
        </TabPanel>
        <TabPanel value={value} index={2}>
          <Contact uuid={props.UUID} />
        </TabPanel>
        <TabPanel value={value} index={3}>
          <Delivery uuid={props.UUID} />
        </TabPanel>
        <TabPanel value={value} index={4}>
          <ProtocolCertificate uuid={props.UUID} />
        </TabPanel>
      </MuiThemeProvider>
    </div>
  );
}
