import React, { useEffect } from 'react';
import { makeStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { useMutation, useQuery, useManualQuery } from 'graphql-hooks';
import Button from '@material-ui/core/Button';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import Divider from '@material-ui/core/Divider';
import '../css/commonStyle.css';
import TextField from '@material-ui/core/TextField';
import Logout from '@material-ui/icons/ExitToApp';
import Theme from '../css/theme';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Switch from '@material-ui/core/Switch';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import SearchIcon from '@material-ui/icons/Search';
import Typography from '@material-ui/core/Typography';
import { useTheme, createMuiTheme } from '@material-ui/core/styles';

const API_CAll_DATA = `query allPartnerDetailsAPI($SearchInput:String){ 
  allPartnerDetailsAPI(search:$SearchInput){
    success
  }
}`;

const API_CAll_DATA1 = `{ 
  allPartnerDetailsAPI1{
    corporationName
    organizationUnit
    status
    prefferedProtocolUUID
    profile{
      profileidtypesUUID
      profileIDValue
    }    
    addresses{
          addressNameID
          addresslines1
          addressLines2
          addresslines3
          city
          state
          country
          zip
    }
    contact{
          role
          firstName
          lastName
          email
          phone
          fax
          address{
                  addressNameID
                  addresslines1
                  addressLines2
                  addresslines3
                  city
                  state
                  country
                  zip
                  }
          contactType
          }
}}
`;

const DISPLAY_PARTNERS = `query allPartnersSearch($SearchInput:String){
  allPartnersSearch(search:$SearchInput){
    partnerProfileUUID
    corporationName
    partnerProfileDescription
    organizationUnit
    status
    prefferedProtocolUUID
  }
}`;

const DISPLAY_PROTOCOL_TYPE = `{
  allProtocolType{
    protocolTypeUUID
    protocolTypeName
  }
}`;
const FETCH_PROFILE_SEARCH = `mutation fetchPartnerProfileBySearch($input:PartnerInput){
  fetchPartnerProfileBySearch(input:$input) {
    partnerProfileUUID
    corporationName
    organizationUUID
    partnerProfileDescription
    organizationUnit
    status
    prefferedProtocolUUID
    createdOn
    lastUpdatedOn
  } 
}
`;
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
}));
const getMuiTheme = () =>
  createMuiTheme(Theme, {
    overrides: {
      MuiSelect: {
        select: {
          minWidth: 80,
          marginTop: '8%',
        },
      },
      MuiTypography: {
        body1: {
          fontSize: 11,
          fontWeight: 150,
          marginLeft: '10%',
        },
      },
      MuiFormLabel: {
        root: {
          fontSize: 14,
        },
      },
      MuiInputBase: {
        root: {
          fontSize: 14,
        },
      },
      MuiFormControlLabel: {
        root: {
          marginTop: '4%',
          display: 'grid',
        },
      },
      MuiButton: {
        root: {
          paddingTop: 10,
          paddingRight: 16,
          paddingBottom: 6,
          paddingLeft: 10,
          marginRight: '20%',
        },
      },
      MuiInputLabel: {
        animated: {
          color: '#0b153e',
        },
      },
    },
  });

export default function SearchPartner(props) {
  console.log('UUID', props);
  const classes = useStyles();
  const [protocolType, setProtocolType] = React.useState([]);
  const [protocolTypeData, setProtocolTypeData] = React.useState([]);
  const [partnerProfileuuid, setPartnerProfilUUID] = React.useState(props.UUID);
  /*const { loading:pTaloading, data:pTadata, errors:pTaerrors, networkError:pTanetworkError } = useQuery(API_CAll_DATA,{
      variables:{
        PartnerProfileUUID:props.UUID
      }
    });*/
  const {
    loading: pTloading,
    data: pTdata,
    errors: pTerrors,
    networkError: pTnetworkError,
  } = useQuery(DISPLAY_PROTOCOL_TYPE);
  const [searchPartner, setSearchPartner] = React.useState({
    partnerProfileUUID: '',
    CorporationName: '',
    partnerProfileDescription: '',
    organizationUnit: '',
    status: '',
    prefferedProtocolUUID: '',
  });

  /*const [
    fetchProfile,
    { loading: pTaloading, data: pTadata, errors: pTaerrors },
  ] = useManualQuery(API_CAll_DATA, {
    variables: {
      PartnerProfileUUID: props.UUID,
    },
  });*/

  const { loading, data, errors, networkError, refetch } = useQuery(
    DISPLAY_PARTNERS,
    {
      variables: {
        SearchInput: partnerProfileuuid,
      },
    }
  );
  //fetch SEARCH GO
  const [
    SearchPartnerProfile,
    { searchdata, searchloading, searcherror },
  ] = useMutation(FETCH_PROFILE_SEARCH);
  const [ErrMsg, setErrMsg] = React.useState(false);

  const [errorInfo, setErrorInfo] = React.useState({
    corporationName: false,
    organisationUnit: false,
    prefferedProtocol: false,
  });
  /*const handleCallAPI = async () => {
    fetchProfile({
      variables: {
        SearchInput: props.UUID,
      },
    });
  };*/
  var obj = [];
  var dropDown = [];

  useEffect(() => {
    console.log(localStorage.getItem('partnerprofileuuid'));
    if (props.UUID !== undefined) {
      setPartnerProfilUUID(props.UUID);
    }
    if (
      data !== undefined &&
      data.allPartnersSearch.length > 0 &&
      localStorage.getItem('partnerprofileuuid') == props.UUID
    ) {
      console.log(
        'data.allPartnersSearch.partnerProfileUUID',
        data.allPartnersSearch[0].partnerProfileUUID
      );
      setSearchPartner({
        partnerProfileUUID: data.allPartnersSearch[0].partnerProfileUUID,
        corporationName: data.allPartnersSearch[0].corporationName,
        partnerProfileDescription:
          data.allPartnersSearch[0].partnerProfileDescription,
        organizationUnit: data.allPartnersSearch[0].organizationUnit,
        status: data.allPartnersSearch[0].status === 'Inactive' ? false : true,
        prefferedProtocolUUID: data.allPartnersSearch[0].prefferedProtocolUUID,
      });
    } else {
      console.log(searchPartner);
    }
    if (pTdata !== undefined) {
      dropDown = pTdata.allProtocolType.reduce(function (acc, cur, i) {
        acc[cur.protocolTypeUUID] = cur.protocolTypeName;
        return acc;
      }, {});
      setProtocolTypeData(dropDown);
    }
    if (pTdata !== undefined) {
      pTdata.allProtocolType.map(function (v, i) {
        obj.push({ value: v.protocolTypeUUID, label: v.protocolTypeName });
      }, {});
      setProtocolType(obj);
    }
  }, [props, partnerProfileuuid, data, pTdata]);

  const handleSubmit = async () => {
    setErrMsg(false);

    if (
      searchPartner.corporationName === '' ||
      searchPartner.organizationUnit === ''
    ) {
      setErrMsg(true);
    } else {
      setErrMsg(false);
      SearchPartnerProfile({
        variables: {
          input: {
            // status: searchPartner.status,
            prefferedProtocolUUID: searchPartner.prefferedProtocolUUID,
            corporationName: searchPartner.corporationName,
            organizationUnit: searchPartner.organizationUnit,
          },
        },
      })
        .then(
          ({ data, error, graphQLErrors, networkError, cacheHit, loading }) => {
            console.log(
              data,
              error,
              graphQLErrors,
              networkError,
              cacheHit,
              loading
            );
            if (data !== null && error == false) {
              if (data.fetchPartnerProfileBySearch.length > 0) {
                console.log(
                  data.fetchPartnerProfileBySearch[0].partnerProfileUUID
                );
                localStorage.setItem(
                  'partnerprofileuuid',
                  data.fetchPartnerProfileBySearch[0].partnerProfileUUID
                );
                setPartnerProfilUUID(
                  data.fetchPartnerProfileBySearch[0].partnerProfileUUID
                );
                props.profilUUID(
                  data.fetchPartnerProfileBySearch[0].partnerProfileUUID
                );
              } else {
                localStorage.setItem('partnerprofileuuid', '');
                setPartnerProfilUUID('');
                props.profilUUID('');
                props.onSuccess(true, 'Searched Partner does not exist');
              }
            } else {
              console.log(error);
            }
          }
        )
        .catch((e) => {
          // you can do something with the error here
          console.log(e);
        });
      /*  setSearchPartner({
        ...searchPartner,
        corporationName: '',
        organizationUnit: '',
        prefferedProtocol: '',
      });*/
    }
  };

  const handleChange = (name) => (event) => {
    console.log(event.target.value);
    setErrMsg(false);
    setSearchPartner({ ...searchPartner, [name]: event.target.value });
    switch (name) {
      case 'corporationName':
        if (event.target.value.length === 0) {
          setSearchPartner({ ...searchPartner, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else {
          if (
            event.target.value.match('^([a-zA-Z])[a-zA-Z]*$') &&
            event.target.value.length <= 25
          ) {
            setErrorInfo({ ...errorInfo, [name]: false });
            setSearchPartner({ ...searchPartner, [name]: event.target.value });
          } else {
            setErrorInfo({ ...errorInfo, [name]: true });
          }
        }
        break;
      case 'organizationUnit':
        if (event.target.value.length === 0) {
          setSearchPartner({ ...searchPartner, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else {
          if (
            event.target.value.match('^([a-zA-Z])[a-zA-Z]*$') &&
            event.target.value.length <= 25
          ) {
            setErrorInfo({ ...errorInfo, [name]: false });
            setSearchPartner({ ...searchPartner, [name]: event.target.value });
          } else {
            setErrorInfo({ ...errorInfo, [name]: true });
          }
        }
        break;
      case 'status':
        setSearchPartner({ ...searchPartner, [name]: event.target.checked });
        break;
      case 'prefferedProtocol':
        setSearchPartner({ ...searchPartner, [name]: event.target.value });
        break;
      default:
        return false;
    }
  };

  return (
    <MuiThemeProvider theme={(Theme, getMuiTheme())}>
      <div className={classes.root}>
        {ErrMsg === true ? (
          <div style={{ textAlign: 'center' }} className="addfieldserror">
            Please fill the fields
          </div>
        ) : (
          ''
        )}
        <form className="usercontent" noValidate autoComplete="off">
          <Grid style={{ marginLeft: '1.5%' }} container spacing={3}>
            <Grid item xs={2} sm={2}>
              <TextField
                id="standard-basic"
                label="Name"
                InputLabelProps={{
                  shrink: true,
                }}
                margin="normal"
                value={searchPartner.corporationName}
                onChange={handleChange('corporationName')}
                name="corporationName"
              />
              {errorInfo.CorporationName === true ? (
                <div id="Name" className="addfieldserror">
                  Allowed alphabets,no special characters and Numerics are
                  allowed.Maximum length is 25."
                </div>
              ) : (
                ''
              )}
            </Grid>
            <Grid item xs={2} sm={2}>
              <TextField
                id="standard-basic"
                label="Organization Unit"
                InputLabelProps={{
                  shrink: true,
                }}
                margin="normal"
                value={searchPartner.organizationUnit}
                onChange={handleChange('organizationUnit')}
                name="organisationUnit"
              />
              {errorInfo.OrganizationUnit === true ? (
                <div id="OrganizationUnit" className="addfieldserror">
                  Allowed alphabets,no special characters and Numerics are
                  allowed.Maximum length is 25.
                </div>
              ) : (
                ''
              )}
            </Grid>
            <Grid item xs={3} sm={3}>
              <FormControl className={classes.formControl}>
                <InputLabel
                  style={{ color: '#0b153e', fontSize: 14 }}
                  id="demo-simple-select-label"
                >
                  Preferred Protocol
                </InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  // value={searchPartner.prefferedProtocolUUID}
                  value={searchPartner.prefferedProtocolUUID}
                  onChange={handleChange('prefferedProtocolUUID')}
                  name="prefferedProtocol"
                >
                  {protocolType !== undefined
                    ? protocolType.map((v, i) => (
                        <MenuItem key={v.value} value={v.value}>
                          {v.label}
                        </MenuItem>
                      ))
                    : ''}
                  {/* <MenuItem value={10}>FTP</MenuItem>
                                        <MenuItem value={20}>FTPS</MenuItem>
                                        <MenuItem value={30}>HTTP</MenuItem>
                                          <MenuItem value={10}>HTTPS</MenuItem>*/}
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={1} sm={1}>
              <FormControlLabel
                style={{ color: '#0b153e', fontSize: 12 }}
                label="Status"
                control={
                  <Switch
                    checked={searchPartner.status}
                    onChange={handleChange('status')}
                    name="status"
                  />
                }
              />
            </Grid>
            <Grid item xs={2} sm={2}>
              <Button
                variant="contained"
                fullWidth="true"
                className="partnergobutton"
                onClick={handleSubmit}
              >
                GO
              </Button>
            </Grid>
          </Grid>
        </form>
      </div>
    </MuiThemeProvider>
  );
}
