import React, { useEffect, useContext, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import { useMutation, useQuery } from 'graphql-hooks';
import Snackbar from '@material-ui/core/Snackbar';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Switch from '@material-ui/core/Switch';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import Modal from '@material-ui/core/Modal';
import Theme from '../css/theme';
import Loader from '../components/loader';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import CertificateFormModel from '../components/tmsmodal';
import CertificateForm from './certificateModelForm';
import Checkbox from '@material-ui/core/Checkbox';
import Typography from '@material-ui/core/Typography';
import MySnackbarContentWrapper from '../components/Snackbar';
const ADD_AS2profile = `
mutation createAS2Profile($AS2ProfileInput:AS2ProfileInput){
  createAS2Profile(input:$AS2ProfileInput) {
    idAs2profile
    as2profileUUID
    aS2Identifier
    rolloverCertificatePassword
    certificatePassword
    rolloverPrivatecertificate
    privateCertificate
    publicDomain
    asynchronousMDNURL
    receivingURL
    publicURL
    publicCertificate
    createdOn
    updatedOn
  } 
}
`;
const UPDATE_AS2profile = `
mutation updateAS2Profile($AS2ProfileInput:AS2ProfileInput){
  updateAS2Profile(input:$AS2ProfileInput) {
    idAs2profile
    as2profileUUID
    aS2Identifier
    rolloverCertificatePassword
    certificatePassword
    rolloverPrivatecertificate
    privateCertificate
    publicDomain
    asynchronousMDNURL
    receivingURL
    publicURL
    publicCertificate
    createdOn
    updatedOn
  }
}

`;
export default function As2ProfileFormModel(props) {
  console.log(props);
  //ADD as2form
  const [addAS2Form, addloading, adddata] = useMutation(ADD_AS2profile);
  //Update as2form
  const [updateAS2Form, updateloading, updatedata] = useMutation(
    UPDATE_AS2profile
  );
  const [values, setValues] = React.useState({
    as2identifier: '',
    privatecertificate: '',
    // createcertificate: '',
    certificatepassword: '',
    rolloverprivatecertificate: '',
    rollovercertificatepassword: '',
    publicDomains: '',
    asynchronousMDNURL: '',
    receivingURL: '',
    publicURL: '',
    publicCertificate: '',
  });
  const [certificateData, setData] = React.useState([]);
  const [openCertificateModal, setOpenCertificateModal] = React.useState(false);
  const [certificateFormTitle, setCertificateFormTitle] = React.useState(
    'update'
  );
  const [rowData, setrowData] = React.useState();
  const [submitErr, setSubmitErr] = React.useState(false);
  const [ErrMsg, setErrMsg] = React.useState(false);
  const [errorInfo, setErrorInfo] = React.useState({
    as2identifier: false,
    privatecertificate: false,
    //createcertificate: false,
    certificatepassword: false,
    rolloverprivatecertificate: false,
    rollovercertificatepassword: false,
    publicDomains: false,
    asynchronousMDNURL: false,
    receivingURL: false,
    publicURL: false,
    publicCertificate: false,
  });

  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [message, setSnackbarMessage] = React.useState();
  const [variant, setSnackbarVariant] = React.useState('error');
  const paswdexp = /^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{4,8}$/;
  useEffect(() => {
    console.log(props.rowData);
    if (props.rowData != undefined && props.title != 'Add') {
      console.log(props.rowData, props.rowData.organizationUnit);
      setValues({
        as2identifier: props.rowData.aS2Identifier,
        privatecertificate: props.rowData.privateCertificate,
        certificatepassword: props.rowData.certificatePassword,
        rolloverprivatecertificate: props.rowData.rolloverPrivatecertificate,
        rollovercertificatepassword: props.rowData.rolloverCertificatePassword,
        publicDomains: props.rowData.publicDomain,
        asynchronousMDNURL: props.rowData.asynchronousMDNURL,
        receivingURL: props.rowData.receivingURL,
        publicURL: props.rowData.publicURL,
        publicCertificate: props.rowData.publicCertificate,
      });
    }
  }, [props]);
  const handleChange = (name) => (event) => {
    console.log(event.target.value, name);
    setErrMsg(false);
    setValues({ ...values, [name]: event.target.value });
    switch (name) {
      case 'as2identifier':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[a-zA-Z0-9]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'createcertificate':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[a-zA-Z0-9,-.:/ ]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'certificatepassword':
        //case 'rollovercertificatepassword':

        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match(paswdexp)) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'rollovercertificatepassword':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match(paswdexp)) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'publicDomains':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[a-zA-Z ]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'asynchronousMDNURL':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match('^(http(s)?://)?([a-z][a-zA-Z0-9./]*)?$')
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'receivingURL':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match('^(http(s)?://)?([a-z][a-zA-Z0-9./]*)?$')
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'publicURL':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match('^(http(s)?://)?([a-z][a-zA-Z0-9./]*)?$')
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'publicCertificate':
        setValues({
          ...values,
          [event.target.name]: event.target.value,
        });
        break;
      default:
        return false;
    }
  };
  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }

  const handleRefetchSnackbar = (value, message, modelClose) => {
    setOpenSnackbar(true);
    setSnackbarMessage(message);
    if (value) {
      setSnackbarVariant('error');
    } else {
      setSnackbarVariant('success');
    }
    if (modelClose) {
      handleCloseForm();
    }
  };
  const handleSubmit = async () => {
    console.log(values);
    setErrMsg(false);
    if (
      values.as2identifier === '' &&
      values.privatecertificate === '' &&
      values.certificatepassword === '' &&
      values.rollovercertificatepassword === '' &&
      values.rollovercertificatepassword === '' &&
      values.asynchronousMDNURL !== '' &&
      values.receivingURL !== '' &&
      values.publicURL !== '' &&
      values.publicCertificate !== '' &&
      errorInfo.as2identifier !== true &&
      errorInfo.privatecertificate !== true &&
      errorInfo.certificatepassword !== true &&
      errorInfo.rolloverprivatecertificate !== true &&
      errorInfo.rollovercertificatepassword == true &&
      errorInfo.publicDomains !== true &&
      errorInfo.asynchronousMDNURL !== true &&
      errorInfo.receivingURL !== true &&
      errorInfo.publicURL !== true
    ) {
      setErrMsg(false);
      setSubmitErr(false);
    } else if (
      values.as2identifier === '' &&
      values.privatecertificate === '' &&
      values.certificatepassword === '' &&
      values.rolloverprivatecertificate === '' &&
      values.rollovercertificatepassword === '' &&
      values.publicDomains === '' &&
      values.asynchronousMDNURL === '' &&
      values.receivingURL === '' &&
      values.publicURL === '' &&
      values.publicCertificate === ''
    ) {
      setErrMsg(true);
    } else if (
      values.as2identifier === '' ||
      values.privatecertificate === '' ||
      // values.createcertificate === '' ||
      values.certificatepassword === '' ||
      values.rolloverprivatecertificate === '' ||
      values.rollovercertificatepassword === '' ||
      values.publicDomains !== '' ||
      values.asynchronousMDNURL !== '' ||
      values.receivingURL !== '' ||
      values.publicURL !== '' ||
      values.publicCertificate !== ''
    ) {
      setErrMsg(false);
      setSubmitErr(true);
      console.log('submitform');
      if (props.title == 'Add') {
        //ADD AS2 Profile form
        await addAS2Form({
          variables: {
            AS2ProfileInput: {
              aS2Identifier: values.as2identifier,
              rolloverPrivatecertificate: values.rolloverprivatecertificate,
              certificatePassword: values.certificatepassword,
              rolloverCertificatePassword: values.rollovercertificatepassword,
              privateCertificate: values.privatecertificate,
              publicDomain: values.publicDomains,
              asynchronousMDNURL: values.asynchronousMDNURL,
              receivingURL: values.receivingURL,
              publicURL: values.publicURL,
              publicCertificate: values.publicCertificate,
            },
          },
        })
          .then(
            ({
              data,
              error,
              graphQLErrors,
              networkError,
              cacheHit,
              loading,
            }) => {
              console.log(
                data,
                error,
                graphQLErrors,
                networkError,
                cacheHit,
                loading
              );
              if (data !== null && error == false) {
                console.log(' created successfully');
                setErrMsg(false);

                props.onSuccess(error, 'Saved successfully', true);
              } else {
                props.onSuccess(error, 'Graphql hooks error', false);
              }
            }
          )
          .catch((e) => {
            // you can do something with the error here
            console.log(e);
          });
      } else {
        await updateAS2Form({
          variables: {
            AS2ProfileInput: {
              aS2Identifier: values.as2identifier,
              rolloverPrivatecertificate: values.rolloverprivatecertificate,
              certificatePassword: values.certificatepassword,
              rolloverCertificatePassword: values.rollovercertificatepassword,
              privateCertificate: values.privatecertificate,
              publicDomain: values.publicDomains,
              asynchronousMDNURL: values.asynchronousMDNURL,
              receivingURL: values.receivingURL,
              publicURL: values.publicURL,
              publicCertificate: values.publicCertificate,
              as2profileUUID: props.rowData.as2profileUUID,
            },
          },
        })
          .then(
            ({
              data,
              error,
              graphQLErrors,
              networkError,
              cacheHit,
              loading,
            }) => {
              console.log(
                data,
                error,
                graphQLErrors,
                networkError,
                cacheHit,
                loading
              );
              if (data !== null && error == false) {
                console.log(' updated successfully');
                setErrMsg(false);

                props.onSuccess(error, 'Updated successfully', true);
              } else {
                props.onSuccess(error, 'Graphql hooks error', false);
              }
            }
          )
          .catch((e) => {
            // you can do something with the error here
            console.log(e);
          });
      }
    }
  };
  const handleReset = async () => {
    setErrMsg(false);
    setErrorInfo({
      as2identifier: false,
      privatecertificate: false,
      certificatepassword: false,
      rolloverprivatecertificate: false,
      rollovercertificatepassword: false,
      publicDomains: false,
      asynchronousMDNURL: false,
      receivingURL: false,
      publicURL: false,
      publicCertificate: false,
    });
    setValues({
      ...values,
      as2identifier: '',
      privatecertificate: '',
      certificatepassword: '',
      rolloverprivatecertificate: '',
      rollovercertificatepassword: '',
      publicDomains: '',
      asynchronousMDNURL: '',
      receivingURL: '',
      publicURL: '',
      publicCertificate: '',
    });
    setSubmitErr(false);
  };
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiInputLabel: {
          animated: {
            fontSize: 13,
          },
          outlined: {
            paddingTop: 3,
          },
        },
        MuiSelect: {
          select: {
            minWidth: '180px',
          },
        },
      },
    });
  const handleClickAddOpen = (e) => {
    setOpenCertificateModal(true);
  };
  const handleCloseForm = (e) => {
    setOpenCertificateModal(false);
  };
  const handleCloseModal = () => {
    props.handleCloseModal(false);
  };
  return (
    <div>
      <MuiThemeProvider theme={getMuiTheme()}>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={props.isModalOpen}
          onClose={handleCloseModal}
          className="fabmodal"
        >
          <div className="modalpaper">
            <Grid container className="header">
              <Grid item xs={10} sm={10}>
                <h2 id="modal-title" className="h2">
                  AS2 Profile
                </h2>
              </Grid>
              <Grid item xs={2} sm={2} className="close">
                <CloseIcon onClick={handleCloseModal} />
              </Grid>
            </Grid>
            <div>
              {ErrMsg === true ? (
                <div style={{ textAlign: 'center' }} className="addfieldserror">
                  Please fill the fields
                </div>
              ) : (
                ''
              )}
              <Grid
                style={{
                  height: 400,
                  overflowY: 'auto',
                  overflowX: 'hidden',
                }}
              >
                <form noValidate autoComplete="off">
                  <Grid item xs={12} sm={12}>
                    <h2 className="h2" style={{ color: '#0b153e' }}>
                      Personal ID
                    </h2>
                  </Grid>
                  <Grid>
                    <Grid container spacing={3} className="userformcontent">
                      <Grid
                        item
                        xs={12}
                        sm={12}
                        gutterBottom
                        variant="body2"
                        color="textSecondary"
                        className="text"
                      >
                        <TextField
                          id="outlined-dense"
                          label="AS2 Identifier"
                          className="partnertextField"
                          margin="dense"
                          variant="outlined"
                          //placeholder=""
                          onChange={handleChange('as2identifier')}
                          value={values.as2identifier}
                          name="as2identifier"
                        />
                        {errorInfo.as2identifier === true ? (
                          <div id="nameid" className="addfieldserror">
                            Enter only Aplhabets and Numerics
                          </div>
                        ) : (
                          ''
                        )}
                        {submitErr && !values.as2identifier ? (
                          <div id="nameid" className="addfieldserror">
                            Please enter AS2 Identifier
                          </div>
                        ) : (
                          ''
                        )}
                      </Grid>
                    </Grid>

                    <Grid item xs={12} sm={12}>
                      <h2 className="h2" style={{ color: '#0b153e' }}>
                        Personal Certificate
                      </h2>
                    </Grid>
                    <Grid container spacing={3} className="userformcontent">
                      <Grid
                        item
                        xs={12}
                        sm={6}
                        gutterBottom
                        variant="body2"
                        color="textSecondary"
                        className="text"
                      >
                        <FormControl>
                          <InputLabel
                            style={{ color: '#0b153e', fontSize: 12 }}
                            id="demo-simple-select-label"
                          >
                            Private Certificate
                          </InputLabel>
                          <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={values.privatecertificate}
                            onChange={handleChange('privatecertificate')}
                            name="privatecertificate"
                          >
                            <MenuItem value={'testcer'}>testcer</MenuItem>
                          </Select>
                        </FormControl>
                        {submitErr && !values.privatecertificate ? (
                          <div id="nameid" className="addfieldserror">
                            Please select Private Certificate
                          </div>
                        ) : (
                          ''
                        )}
                      </Grid>
                      <Grid
                        item
                        xs={12}
                        sm={6}
                        gutterBottom
                        variant="body2"
                        color="textSecondary"
                        className="text"
                      >
                        <Button
                          onClick={handleClickAddOpen}
                          vvariant="contained"
                          fullWidth="true"
                          className="createpartnerbutton"
                        >
                          <LockOutlinedIcon style={{ paddingRight: '10px' }} />
                          Create Certificate
                        </Button>
                      </Grid>
                      <Grid
                        item
                        xs={12}
                        sm={6}
                        gutterBottom
                        variant="body2"
                        color="textSecondary"
                        className="text"
                      >
                        <TextField
                          id="outlined-dense"
                          label="Certificate Password"
                          className="partnertextField"
                          margin="dense"
                          variant="outlined"
                          onChange={handleChange('certificatepassword')}
                          value={values.certificatepassword}
                          name="certificatepassword"
                        />
                        {errorInfo.certificatepassword === true ? (
                          <div id="nameid" className="addfieldserror">
                            4 to 8 characters must have atleast
                            one[uppercase,lowercase,special character and
                            number]
                          </div>
                        ) : (
                          ''
                        )}
                        {submitErr && !values.certificatepassword ? (
                          <div id="nameid" className="addfieldserror">
                            Please enter Certificate Password
                          </div>
                        ) : (
                          ''
                        )}
                      </Grid>
                      <Grid
                        item
                        xs={12}
                        sm={6}
                        gutterBottom
                        variant="body2"
                        color="textSecondary"
                        className="text"
                      >
                        <FormControl>
                          <InputLabel
                            style={{ color: '#0b153e', fontSize: 12 }}
                            id="demo-simple-select-label"
                          >
                            Rollover Private Certificate
                          </InputLabel>
                          <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            // value={searchPartner.prefferedProtocolUUID}
                            value={values.rolloverprivatecertificate}
                            onChange={handleChange(
                              'rolloverprivatecertificate'
                            )}
                            name="rolloverprivatecertificate"
                          >
                            <MenuItem value={'testcer'}>testcer</MenuItem>
                          </Select>
                        </FormControl>
                        {submitErr && !values.rolloverprivatecertificate ? (
                          <div id="nameid" className="addfieldserror">
                            Please select Rollover Private Certificate
                          </div>
                        ) : (
                          ''
                        )}
                      </Grid>
                      <Grid
                        item
                        xs={12}
                        sm={6}
                        gutterBottom
                        variant="body2"
                        color="textSecondary"
                        className="text"
                      >
                        <TextField
                          id="outlined-dense"
                          label="Rollover Certificate Password"
                          className="partnertextField"
                          margin="dense"
                          variant="outlined"
                          onChange={handleChange('rollovercertificatepassword')}
                          value={values.rollovercertificatepassword}
                          name="rollovercertificatepassword"
                        />
                        {errorInfo.rollovercertificatepassword === true ? (
                          <div id="nameid" className="addfieldserror">
                            4 to 8 characters must have atleast
                            one[uppercase,lowercase,special character and
                            number]
                          </div>
                        ) : (
                          ''
                        )}
                        {submitErr && !values.rollovercertificatepassword ? (
                          <div id="nameid" className="addfieldserror">
                            Please enter Rollover Certificate Password
                          </div>
                        ) : (
                          ''
                        )}
                      </Grid>
                    </Grid>
                    <Grid item xs={12} sm={12}>
                      <h2 className="h2" style={{ color: '#0b153e' }}>
                        Application URL
                      </h2>
                    </Grid>
                    <Grid container spacing={3} className="userformcontent">
                      <Grid
                        item
                        xs={12}
                        sm={6}
                        gutterBottom
                        variant="body2"
                        color="textSecondary"
                        className="text"
                      >
                        <TextField
                          id="outlined-dense"
                          label="Public Domain "
                          className="partnertextField"
                          margin="dense"
                          variant="outlined"
                          //placeholder=""
                          onChange={handleChange('publicDomains')}
                          value={values.publicDomains}
                          name="publicDomains"
                        />
                        {errorInfo.publicDomains === true ? (
                          <div id="nameid" className="addfieldserror">
                            "Enter only alphabets"
                          </div>
                        ) : (
                          ''
                        )}
                        {submitErr && !values.publicDomains ? (
                          <div id="nameid" className="addfieldserror">
                            Please enter public domains
                          </div>
                        ) : (
                          ''
                        )}
                      </Grid>
                      <Grid
                        item
                        xs={12}
                        sm={6}
                        gutterBottom
                        variant="body2"
                        color="textSecondary"
                        className="text"
                      >
                        <TextField
                          id="outlined-dense"
                          label="Asynchronous MDN URL"
                          className="partnertextField"
                          margin="dense"
                          variant="outlined"
                          onChange={handleChange('asynchronousMDNURL')}
                          value={values.asynchronousMDNURL}
                          name="asynchronousMDNURL"
                        />
                        {errorInfo.asynchronousMDNURL === true ? (
                          <div id="nameid" className="addfieldserror">
                            "Enter valid URL"
                          </div>
                        ) : (
                          ''
                        )}
                        {submitErr && !values.asynchronousMDNURL ? (
                          <div id="nameid" className="addfieldserror">
                            Please enter Asynchronous MDN URL
                          </div>
                        ) : (
                          ''
                        )}
                      </Grid>
                      <Grid
                        item
                        xs={12}
                        sm={6}
                        gutterBottom
                        variant="body2"
                        color="textSecondary"
                        className="text"
                      >
                        <TextField
                          id="outlined-dense"
                          label="Receiving URL"
                          className="partnertextField"
                          margin="dense"
                          variant="outlined"
                          onChange={handleChange('receivingURL')}
                          value={values.receivingURL}
                          name="receivingURL"
                        />
                        {errorInfo.receivingURL === true ? (
                          <div id="nameid" className="addfieldserror">
                            "Enter valid URL"
                          </div>
                        ) : (
                          ''
                        )}
                        {submitErr && !values.receivingURL ? (
                          <div id="nameid" className="addfieldserror">
                            Please enter Receiving URL
                          </div>
                        ) : (
                          ''
                        )}
                      </Grid>
                      <Grid
                        item
                        xs={12}
                        sm={6}
                        gutterBottom
                        variant="body2"
                        color="textSecondary"
                        className="text"
                      >
                        <TextField
                          id="outlined-dense"
                          label="Public URL "
                          className="partnertextField"
                          margin="dense"
                          variant="outlined"
                          onChange={handleChange('publicURL')}
                          value={values.publicURL}
                          name="publicURL"
                        />
                        {errorInfo.publicURL === true ? (
                          <div id="nameid" className="addfieldserror">
                            "Enter valid URL"
                          </div>
                        ) : (
                          ''
                        )}
                        {submitErr && !values.publicURL ? (
                          <div id="nameid" className="addfieldserror">
                            Please enter Public URL
                          </div>
                        ) : (
                          ''
                        )}
                      </Grid>

                      <Grid item xs={12} sm={12}>
                        <Typography>
                          <Checkbox
                            inputProps={{
                              'aria-label': 'uncontrolled-checkbox',
                            }}
                          />
                          Publish my AS2 Profile Settings at public rst
                        </Typography>
                      </Grid>
                      <Grid
                        item
                        xs={12}
                        sm={6}
                        gutterBottom
                        variant="body2"
                        color="textSecondary"
                        className="text"
                      >
                        <FormControl>
                          <InputLabel
                            style={{ color: '#0b153e', fontSize: 12 }}
                            id="demo-simple-select-label"
                          >
                            Public Certificate
                          </InputLabel>
                          <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={values.publicCertificate}
                            onChange={handleChange('publicCertificate')}
                            name="publicCertificate"
                          >
                            <MenuItem value={'testcer'}>testcer</MenuItem>
                          </Select>
                        </FormControl>
                        {submitErr && !values.publicCertificate ? (
                          <div id="nameid" className="addfieldserror">
                            Please enter Public certificate
                          </div>
                        ) : (
                          ''
                        )}
                      </Grid>
                    </Grid>
                  </Grid>
                  <Grid container spacing={3} className="userformcontent">
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <Button
                        style={{
                          color: '#000006',
                          backgroundColor: '#E5CCFF',
                        }}
                        onClick={handleReset}
                        variant="contained"
                        fullWidth="true"
                        className="createpartnerbutton"
                      >
                        RESET
                      </Button>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <Button
                        onClick={handleSubmit}
                        vvariant="contained"
                        fullWidth="true"
                        className="createpartnerbutton"
                      >
                        SAVE
                      </Button>
                    </Grid>
                  </Grid>
                </form>
              </Grid>
            </div>
          </div>
        </Modal>
      </MuiThemeProvider>
      <CertificateFormModel
        handleCloseModal={handleCloseForm}
        isModalOpen={openCertificateModal}
      >
        <CertificateForm
          handleCloseModal={handleCloseForm}
          onSuccess={handleRefetchSnackbar}
          isModalOpen={openCertificateModal}
          rowData={rowData}
          title={certificateFormTitle}
        />
      </CertificateFormModel>

      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        autoHideDuration={3000}
        open={openSnackbar}
        onClose={handleCloseSnackbar}
      >
        <MySnackbarContentWrapper
          onClose={handleCloseSnackbar}
          variant={variant}
          message={message}
        />
      </Snackbar>
    </div>
  );
}
