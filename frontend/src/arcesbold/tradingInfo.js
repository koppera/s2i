import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Theme from '../css/theme';
import {
  createMuiTheme,
  MuiThemeProvider,
  } from '@material-ui/core/styles';
function TabPanel(props) {
  const { children, value, index, ...other } = props;
 
  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={5}>{children}</Box>}
    </Typography>
    
  );
 
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};
 
function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}
 
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));
 
const getMuiTheme = () =>
createMuiTheme(Theme, {
  overrides: {
    MuiTab: {
      root: {
        textTransform: 'none !important',
        
      },
    },
  },
});

export default function SimpleTabs() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
 
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
 
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
       <Typography>
       <h2 className="h2" style={{ color: '#0b153e' }}>Trading Info</h2>
  
</Typography>
    <div className={classes.root}>
      <AppBar position="static">
        
        <Tabs value={value} onChange={handleChange} aria-label="simple tabs example">
          <Tab label="Settings" {...a11yProps(0)} />
          <Tab label="Automation" {...a11yProps(1)} />
          <Tab label="Advanced" {...a11yProps(2)} />
          <Tab label="Input" {...a11yProps(3)} />
          <Tab label="Output" {...a11yProps(4)} />
          <Tab label="Events" {...a11yProps(5)} />
         
        </Tabs>
      </AppBar>
     
      <TabPanel value={value} index={0}>
      Settings
      </TabPanel>
      <TabPanel value={value} index={1}>
      Automation
      </TabPanel>
      <TabPanel value={value} index={2}>
      Advanced
      </TabPanel>
      <TabPanel value={value} index={3}>
      Input
      </TabPanel>
      <TabPanel value={value} index={4}>
      Output
      </TabPanel>
      <TabPanel value={value} index={5}>
      Events
      </TabPanel>
     
    </div>
    </MuiThemeProvider>
  );
}