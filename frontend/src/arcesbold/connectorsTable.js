import MaterialTable from 'material-table';
import { useMutation, useQuery, useManualQuery } from 'graphql-hooks';
import { forwardRef, useEffect } from 'react';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import ReactDOM from 'react-dom';
import MTableToolbar from 'material-table';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import CloseIcon from '@material-ui/icons/Close';
import { withStyles } from '@material-ui/core/styles';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import { Grid, Typography } from '@material-ui/core';
import MuiDialogActions from '@material-ui/core/DialogActions';
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import Timestamp from '../timestamp';
import Error from '../components/emptyPage';
import Loader from '../components/loader';
import Theme from '../css/theme';
import SendIcon from '@material-ui/icons/Send';
import Snackbar from '@material-ui/core/Snackbar';
import MySnackbarContentWrapper from '../components/Snackbar';
import Connectors from '../components/tmsmodal';
import ConnectorsForm from './connectorsModal1';

import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles,
} from '@material-ui/core/styles';
import { NonceProvider } from 'react-select';
const Post_Connectors_API = `query postConnectorsAPI($conectorInput:conectorInput){
  postConnectorsAPI(input:$conectorInput) {
    idConnectors
    connectorsUUID
    connectorID
    workspaceUUID
    ConnectorType
    ParentConnector
    createdOn
    updatedOn
  }
}
`;
const All_Connectors = `{
  allConnectors {
    idConnectors
    connectorsUUID
    connectorID
    workspaceUUID
    ConnectorType
    ParentConnector
    createdOn
    updatedOn
  }
}
`;
const Connectors_BY_workspace = `query fetchConnectorsByWorkspaceUUID($workspaceuuid:String){
  fetchConnectorsByWorkspaceUUID (workspaceuuid:$workspaceuuid) {
    idConnectors
    connectorsUUID
    connectorID
    workspaceUUID
    workspaceName
    ConnectorType
    ParentConnector
    createdOn
    updatedOn
  }
    
}

`;
const ALL_WorkSpace = `{
  fetchWorkSpace {
    idworkspace
    workspaceUUID
    workspaceName
    createdOn
    updatedOn
  }
}
`;
function iconStyles() {
  return {};
}
const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(2),
    },
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));
const styles = (theme) => ({
  root: {
    Width: 1200,
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: '#FFFFFF',
  },
});
export default function ConectorsTable(props) {
  console.log(props.rowData.workspaceUUID);
  //FETCH connector
  //const { loading, error, data, refetch } = useQuery(All_Connectors);
  const { loading, error, data, refetch } = useQuery(Connectors_BY_workspace, {
    variables: {
      workspaceuuid: props.rowData.workspaceUUID,
    },
  });
  console.log(loading, error, data, refetch);
  const [
    postAPIConnector,
    { loading: cloading, data: cdata, error: cerrors },
  ] = useManualQuery(Post_Connectors_API);
  //workspace drop down
  const workspaceData = useQuery(ALL_WorkSpace);

  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [serverError, setServerError] = React.useState(false);
  const [message, setSnackbarMessage] = React.useState();
  const [variant, setSnackbarVariant] = React.useState('error');
  const [OpenConnectors, setOpenConnectors] = React.useState(false);
  const [connectorsForm, setOpenConnectorsForm] = React.useState('update');
  const [rowData, setrowData] = React.useState();
  const [workspace, setWorkSpace] = React.useState([]);
  const [connectorData, setData] = React.useState([]);
  const [parentConnector, setParenConnector] = React.useState([]);

  const columns = [
    {
      title: 'Connector Type',
      field: 'ConnectorType',
      editable: 'never',
    },

    {
      title: 'Connector ID',
      field: 'connectorID',
    },
    {
      title: ' Workspace',
      field: 'workspaceUUID',
      lookup: workspace,
    },
    {
      title: ' Parent Connector',
      field: 'ParentConnector',
      lookup: parentConnector,
    },
  ];
  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }
  const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => (
      <Clear color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline color="error" {...props} ref={ref} />
    )),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => (
      <Edit {...props} ref={ref} onClick={console.log('hello world')} />
    )),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };

  const handleClickUpdateOpen = (e, rowData) => {
    console.log(e, rowData);
    setOpenConnectors('Update');
    setrowData(rowData);
    setOpenConnectorsForm(true);
  };
  const handleClickAddOpen = (e) => {
    setOpenConnectors('Add');
    setOpenConnectorsForm(true);
  };
  const handleCloseForm = (e) => {
    setOpenConnectors(false);
  };
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiDialogTitle: {
          root: {
            backgroundColor: '#0b153e',
            color: '#FFFFFF',
          },
        },
        MuiSelect: {
          icon: {
            color: '#0b153e',
          },
        },
      },
    });
  const classes1 = useStyles();
  useEffect(() => {
    if (data !== undefined) {
      console.log(data.fetchConnectorsByWorkspaceUUID);
      setData(data.fetchConnectorsByWorkspaceUUID);
      setServerError(false);
      var objectVal = data.fetchConnectorsByWorkspaceUUID.reduce(
        (obj, item) => ((obj[item.connectorsUUID] = item.connectorID), obj),
        {}
      );
      setParenConnector(objectVal);
    } else {
      console.log('server error');
      setServerError(true);
    }
    if (
      workspaceData !== undefined &&
      workspaceData.data !== undefined &&
      workspaceData.data.fetchWorkSpace.length > 0
    ) {
      let workspaceDetails = workspaceData.data.fetchWorkSpace;
      console.log(workspaceDetails);

      var objectVal = workspaceDetails.reduce(
        (obj, item) => ((obj[item.workspaceUUID] = item.workspaceName), obj),
        {}
      );
      setWorkSpace(objectVal);
    }
  }, [data, workspaceData.data]);

  const handleRefetch = (value, message, modelClose) => {
    refetch({
      updateData(_, data) {
        return data;
      },
    });
    setOpenSnackbar(true);
    setSnackbarMessage(message);
    if (value) {
      setSnackbarVariant('error');
    } else {
      setSnackbarVariant('success');
    }
    if (modelClose) {
      localStorage.setItem('connector', 'success');
      props.onSuccess(true);
      handleCloseForm();
    }
  };

  const handleAPISuccess = async (Status, aPIMsg) => {
    if (Status === 'failure' || Status === null) {
      handleRefetch(true, aPIMsg);
    } else {
      handleRefetch(false, aPIMsg);
    }
  };
  const handleCallAPI = async (event, rowData) => {
    console.log('rowData', rowData);
    await postAPIConnector({
      variables: {
        conectorInput: {
          connectorID: rowData.connectorID,
          workspaceUUID: rowData.workspaceName,
          ConnectorType: rowData.ConnectorType,
        },
      },
    }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
      console.log(data, error, graphQLErrors, networkError, cacheHit);
      if (data !== null && error == false) {
        handleAPISuccess(
          data.fetchConnectorsAPI.statusAPI,
          data.fetchConnectorsAPI.msg
        );
      } else {
        handleRefetch(error, 'graphql hooks error');
      }
    });
  };
  if (loading) return <Loader />;
  if (serverError)
    return <Error type={'Server connection lost.Please try again'} />;
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <MaterialTable
        icons={tableIcons}
        title="Connectors"
        data={connectorData}
        columns={columns}
        editable={{
          onRowDelete: (oldData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                resolve();
              }, 600);
            }),
        }}
        actions={[
          {
            icon: AddIcon,
            tooltip: 'Add',
            className: 'addIconColor',
            isFreeAction: true,
            onClick: (event) => handleClickAddOpen(event),
          },
          {
            icon: Edit,
            tooltip: 'Edit',
            className: 'addIconColor',
            onClick: (event, rowData) => handleClickUpdateOpen(event, rowData),
          },
          {
            icon: SendIcon,
            tooltip: 'send',
            onClick: (event, rowData) => handleCallAPI(event, rowData),
          },
        ]}
        options={{
          headerStyle: {
            textAlign: 'center',
            fontSize: 12,
            fontWeight: 'bold',
            fontFamily: 'Arial !important',
            backgroundColor: '#0b153e',
            color: '#ffffff',
            padding: '4px',
          },
          searchFieldStyle: {
            color: '#0b153e',
          },
          actionsColumnIndex: -1,
          pageSize: 10,
          pageSizeOptions: [10, 25, 50, 100],
          toolbar: true,
          paging: true,
        }}
      />
      <Connectors
        handleCloseModal={handleCloseForm}
        isModalOpen={OpenConnectors}
      >
        <ConnectorsForm
          handleCloseModal={handleCloseForm}
          isModalOpen={OpenConnectors}
          rowData={rowData}
          wsRowData={props.rowData}
          title={connectorsForm}
          onSuccess={handleRefetch}
        />
      </Connectors>
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        autoHideDuration={1500}
        open={openSnackbar}
        onClose={handleCloseSnackbar}
      >
        <MySnackbarContentWrapper
          onClose={handleCloseSnackbar}
          variant={variant}
          message={message}
        />
      </Snackbar>
    </MuiThemeProvider>
  );
}
