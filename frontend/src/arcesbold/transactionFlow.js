import React from 'react';
import { useMutation, useQuery, useManualQuery } from 'graphql-hooks';
import { forwardRef, useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Theme from '../css/theme';
import { Typography, Paper } from '@material-ui/core';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import Loader from '../components/loader';
import Error from '../components/emptyPage';
const Connectors_BY_workspace = `query fetchConnectorsByWorkspaceUUID($workspaceuuid:String){
    fetchConnectorsByWorkspaceUUID (workspaceuuid:$workspaceuuid) {
      idConnectors
      connectorsUUID
      connectorID
      workspaceUUID
      workspaceName
      ConnectorType
      ParentConnector
      createdOn
      updatedOn
    }
      
  }
  
  `;
export default function Transaction(props) {
  console.log(props);
  const flow = [
    'connector 1',
    'connector 2',
    'connector 3',
    'connector 4',
    'connector 5',
    'connector 6',
  ];
  const { loading, error, data, refetch } = useQuery(Connectors_BY_workspace, {
    variables: {
      workspaceuuid: props.rowData.workspaceUUID,
    },
  });
  const [serverError, setServerError] = React.useState(false);
  const [connectorData, setData] = React.useState([]);
  useEffect(() => {
    console.log(localStorage.getItem('connector'), props.reloadValue);
    if (props.reloadValue && localStorage.getItem('connector') == 'success') {
      localStorage.setItem('connector', 'failure');
      handleRefetch();
    }
    if (data !== undefined) {
      console.log(data.fetchConnectorsByWorkspaceUUID);
      let result = data.fetchConnectorsByWorkspaceUUID.map(
        ({ connectorID }) => connectorID
      );
      console.log(result);
      setData(result.reverse());
      setServerError(false);
    } else {
      console.log('server error');
      setServerError(true);
    }
  }, [data, props.reloadValue]);
  const handleRefetch = (value, message) => {
    refetch({
      updateData(_, data) {
        return data;
      },
    });
  };
  if (loading) return <Loader />;
  if (serverError)
    return <Error type={'Server connection lost.Please try again'} />;
  return (
    <div>
      {connectorData.length > 0
        ? connectorData.map((flows, i) => (
            <Grid container spacing={1}>
              <Grid item xs={12} sm={12}>
                <Paper
                  style={{
                    padding: '10px',
                    marginLeft: '150px',
                    marginRight: '150px',
                  }}
                >
                  {connectorData[i]}
                </Paper>
              </Grid>
              <Grid item xs={12} sm={12}>
                {console.log('ivalue', connectorData.length, i)}
                <ArrowDownwardIcon
                  style={{
                    marginLeft: '260px',
                    alignItems: 'center',
                    marginRight: '150px',
                    display: i == connectorData.length - 1 ? 'none' : 'block',
                  }}
                />
              </Grid>
            </Grid>
          ))
        : ''}
    </div>
  );
}
