import React, { useEffect, useContext, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import '../css/commonStyle.css';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import Theme from '../css/theme';
import Grid from '@material-ui/core/Grid';
import Checkbox from '@material-ui/core/Checkbox';
import FormGroup from '@material-ui/core/FormGroup';
import {Typography, Paper } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import Modal from '@material-ui/core/Modal';

import {
  createMuiTheme,
  MuiThemeProvider,
} from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing(2)
  },
   
}));


const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSelect:{
          select:{
            minWidth:180
          }
          }
      },
      MuiTypography:{
        root:{
       
        
            fontSize:'15px'
            
        
    }}
     
    });
   
export default function UserForm(props) {
  const classes = useStyles();
  const [selectedValue, setSelectedValue] = React.useState('a');
  const [values, setValues] = React.useState({ 
      user: '',
      Maxrequest: '', 
      Maxconcurrent:'',
      Authtoken:'',
      get: true,
      post: false,
      putMergPatch: false,
      delete:false,
      active:true,});
  const [ErrMsgDropdown, setErrMsgDropdown] = React.useState(false);
  const [ErrMsgNameInfo, setErrMsgNameInfo] = React.useState(false);
  const [ErrMsg, setErrMsg] = React.useState(false);
  const [errorInfo, setErrorInfo] = React.useState({
    user: false,
    Maxrequest:false,
    Maxconcurrent: false
  });
  const [nameError, setNameError] = React.useState(false);
  const handleChange = name => event => {
    setErrMsg(false);
    setErrMsgDropdown(false);
    setValues({ ...values, [name]: event.target.value });
    switch (name) {
      case 'user':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else {
          if (
            event.target.value.match('^[a-zA-Z]*$') &&
            event.target.value.length <= 25
          ) {
            setErrorInfo({ ...errorInfo, [name]: false });
            setValues({ ...values, [name]: event.target.value });
          } else {
            setErrorInfo({ ...errorInfo, [name]: true });
          }
        }
        break;

      case 'Maxrequest':
        if (event.target.value.length ===0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else {
          if (
            event.target.value.match('^[0-9]*$') &&
            event.target.value.length <= 25
          ) {
            setErrorInfo({ ...errorInfo, [name]: false });
            setValues({ ...values, [name]: event.target.value });
          } else {
            setErrorInfo({ ...errorInfo, [name]: true });
          }
        }
        break;
        case 'Maxconcurrent':
          if (event.target.value.length === 0) {
            setValues({ ...values, [name]: '' });
            setErrorInfo({ ...errorInfo, [name]: false });
          } else {
            if (
              event.target.value.match('^[0-9]*$') &&
              event.target.value.length <= 25
            ) {
              setErrorInfo({ ...errorInfo, [name]: false });
              setValues({ ...values, [name]: event.target.value });
            } else {
              setErrorInfo({ ...errorInfo, [name]: true });
            }
          }
          break;
    case 'Authtoken':
      if (event.target.value.length === 0) {
        setValues({ ...values, [name]: '' });
        setErrorInfo({ ...errorInfo, [name]: false });
      } else {
        if (
          event.target.value.match('^[a-zA-Z]*$') &&
          event.target.value.length <= 25
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
      }
      break;      
    case 'get':
    case 'post':
    case 'putMergPatch':
    case 'delete':
    case 'active':
        setValues({ ...values, [event.target.name]: event.target.checked });
        break;
      default:
        return false;
    }
  };

const handleSubmit = async () => {
 console.log(values);
if(values.user!== ''&&
values.Maxrequest!== ''&& 
values.Maxconcurrent!==''&&
errorInfo.Maxrequest!==true&&
errorInfo.user!==true&
errorInfo.Maxconcurrent!==true&&
errorInfo.Authtoken!==true
)
{  setErrMsg(false);
   setNameError(false);
   setValues({
    ...values,
    user:values.user,
    Maxrequest:values.Maxrequest, 
    Maxconcurrent:values.Maxconcurrent,
    get:values.get,
    post:values.post,
    putMergPatch:values.putMergPatch,
    delete:values.delete,
    active:values.active,
    Authtoken:values.Authtoken,
  });
   if(props.title=='Add')
   {
   //add row sbmite code here
   }
   else
   {
       //update sbmite code here 
   }
}
 else if(values.user== ''&&
 values.Maxrequest== ''&&
 values.Maxconcurrent=='')
 {
    setErrMsg(true);
 }
 else
 {
    setNameError(true);  
 }  
  };
  const handleReset = async () => {
    setErrMsg(false);
    setErrorInfo({
        user: false,
        Maxrequest:false,
        Maxconcurrent: false
    });
    setValues({
      ...values,
      user: '',
      Maxrequest: '', 
      Maxconcurrent:'',
      get: true,
      post: false,
      putMergPatch: false,
      delete:false,
      active:true,
    });
    setNameError(false);
  };
  const handleCloseModal = () => {
    props.handleCloseModal(false);
  };
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
       <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={props.isModalOpen}
          onClose={handleCloseModal}
          className="fabmodal"
        >
          <div className="modalpaper">
            <Grid container className="header">
              <Grid item xs={10} sm={10}>
                <h2 id="modal-title" className="h2">
                {props.title=='Add'?'Add User':'Edit User'}
                </h2>
              </Grid>
              <Grid item xs={2} sm={2} className="close">
                <CloseIcon onClick={handleCloseModal} />
              </Grid>
            </Grid>
    <div>
      {ErrMsg === true ? (
        <div style={{ textAlign: 'center' }} className="addfieldserror">
          Please fill the fields
        </div>
      ) : (
        ''
      )}
      <form className="content" noValidate autoComplete="off">
            <Grid container spacing={3} className="userformcontent">
            <Grid
                  item
                  xs={12} 
                  sm={4}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                <TextField
                id="outlined-dense"
                label="User "
                className="textField"
                margin="dense"
                variant="outlined"
                onChange={handleChange('user')}
                value={values.user}
                name="user"
                />
                {errorInfo.user === true ? (
                <div className="addfieldserror">
                    "Allowed only alphabets.Maximum length is 25."
                </div>
                ) : (
                ''
                )}
              {nameError && !values.user ? (
               <div id="nameid" className="addfieldserror">
                Please enter User
                </div>
                 ) : (
                 ''
                )}
                </Grid>
                <Grid item xs={12} sm={4}>
              <TextField
             id="outlined-dense"
               label="Max.Requests(hr) "
             className="textField"
             margin="dense"
             variant="outlined"
             //placeholder=""
             onChange={handleChange('Maxrequest')}
              value={values.Maxrequest}
              name="Maxrequest"
              />

             {errorInfo.Maxrequest === true ? (
             <div id="nameid" className="addfieldserror">
             "Allowed numbers only."
               </div>
             ) : (
                 ''
                )}
                {nameError && !values.Maxrequest ? (
               <div id="nameid" className="addfieldserror">
                Please enter Max.Requests(hr)
                </div>
                 ) : (
                 ''
                )}
           </Grid>
            <Grid item xs={12} sm={4}>
            <TextField
           id="outlined-dense"
           label="Max.Concurrent "
          className="textField"
          margin="dense"
          variant="outlined"
          //placeholder=""
          onChange={handleChange('Maxconcurrent')}
          value={values.Maxconcurrent}
          name="Maxconcurrent"
          />
          {errorInfo.Maxconcurrent === true ? (
          <div id="nameid" className="addfieldserror">
            "Allowed numbers only"
          </div>
          ) : (
          ''
          )}
          {nameError && !values.Maxconcurrent ? (
         <div id="nameid" className="addfieldserror">
           Please enter Max.Concurrent
          </div>
          ) : (
            ''
       )}  
     </Grid> <Grid
                  item
                  xs={12} 
                  sm={12}
                >
                <FormControl >
                
                <Typography className="textField" >Privileges :</Typography>
          
                <FormGroup aria-label="position" 
                row>
                <Grid item xs={12} sm={6}>
                     <Typography>
                    <Checkbox
                     inputProps={{
                  'aria-label': 'uncontrolled-checkbox',
                   }}
                   value={values.get}
                  checked={values.get}
                name='get'
              onChange={handleChange('get')}
                color="primary"
         />
                  GET
                </Typography >
              </Grid>
               <Grid item xs={12} sm={6} >
                 <Typography>
                 <div style={{ color: '#0b153e', marginRight:'80px'}} >
                 <Checkbox
                   inputProps={{
                 'aria-label': 'uncontrolled-checkbox',
                   }}
                    value={values.post}
                   checked={values.post}
                  name='post'
                onChange={handleChange('post')}
                color="primary"
              />
                 POST</div>
               </Typography>
              </Grid>
                <Grid item xs={12} sm={6}>
                <Typography>
                    <Checkbox
                   inputProps={{
                   'aria-label': 'uncontrolled-checkbox',
                      }}
                   value={values.putMergPatch}
                  checked={values.putMergPatch}
                     name='putMergPatch'
                     onChange={handleChange('putMergPatch')}
                     />
                    PUT,MERGE,PATCH
                    </Typography>
                   </Grid>
                  <Grid item xs={12} sm={6}>
                 <Typography>
                 <Checkbox
                inputProps={{
               'aria-label': 'uncontrolled-checkbox',
               }}
              value={values.delete}
             checked={values.delete}
              name='delete'
               onChange={handleChange('delete')}
             />
             DELETE
        </Typography>
       </Grid>
      </FormGroup>
    </FormControl>
    </Grid>
     <Grid item xs={12} sm={12}>
     <Typography>
     <Checkbox
     inputProps={{
      'aria-label': 'uncontrolled-checkbox',
         }}                     
      value={values.active}
      checked={values.active}
      name='active'
      onChange={handleChange('active')}
      color="primary"
            />
         Active
      </Typography>
         </Grid>
         <Grid item xs={12} sm={4}
       style={{
        display:props.title != 'Add' ? 'block' :'none' 
      }}>
         <TextField
          id="outlined-dense"
          label="Authtoken"
          className="textField"
          margin="dense"
          variant="outlined"
          //placeholder=""
          onChange={handleChange('Authtoken')}
          value={values.Authtoken}
          name="Authtoken"
        />
        {errorInfo.Maxconcurrent === true ? (
          <div id="nameid" className="addfieldserror">
            "Allowed only alphanumaric characters"
          </div>
        ) : (
          ''
        )}
    {nameError && !values.Maxconcurrent ? (
    <div id="nameid" className="addfieldserror">
    Please enter Max.Concurrent
     </div>
      ) : (
    ''
       )}  
     </Grid> 
        <Grid container spacing={3} >
        <Grid
                        item
                        xs={12}
                        sm={6}
                        gutterBottom
                        variant="body2"
                        color="textSecondary"
                        className="text"
                      >
                        <Button
                          style={{
                            color: '#000006',
                            backgroundColor: '#E5CCFF',
                          }}
                          onClick={handleReset}
                          variant="contained"
                          fullWidth="true"
                          className="createpartnerbutton"
                        >
                          RESET
                        </Button>
                      </Grid>
       <Grid item xs={12} sm={6}>
        <Button
         color="textSecondary"
          onClick={handleSubmit}
          variant="contained"
          fullWidth="true"
          className="createpartnerbutton"
        >
         {props.title=='Add'? 'Save':'Update'}
        </Button>
    </Grid>
    </Grid>
    </Grid>
      </form>
      </div>
    </div>
    </Modal>
    </MuiThemeProvider>
  );
}