import React from 'react';
import { useMutation, useQuery } from 'graphql-hooks';
import { forwardRef, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import { Grid, Typography } from '@material-ui/core';
import Theme from '../css/theme';
import '../css/commonStyle.css';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import Loader from '../components/loader';
import Error from '../components/emptyPage';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import  AuthContext  from "../authUtil/Auth";

import CloseIcon from '@material-ui/icons/Close';
import Modal from '@material-ui/core/Modal';

export default function FetchUserOrganization(props) {

  const [value, setValue] = React.useState();
  const [ErrMsg, setErrMsg] = React.useState(false);
  const [serverError, setServerError] = React.useState(false);
  const handleChange = (event) => {   
    setValue(event.target.value);
  };
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSelect:{
          select:{
            minWidth:180
          }
          }
      }
    });
    const handleSubmit = async () => {
    if(value)
    { setErrMsg(false);
      //Add Procced functionality 
    }
    else
    {
      setErrMsg(true);
    }
    }
    const handleCloseModal = () => {
      props.handleCloseModal(false);
    };
  return (
    <div>
      <MuiThemeProvider theme={getMuiTheme()}>
      <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={props.isModalOpen}
          onClose={handleCloseModal}
          className="fabmodal"
        >
          <div className="modalpaper">
            <Grid container className="header">
              <Grid item xs={10} sm={10}>
                <h2 id="modal-title" className="h2">
              Create Work Flow
                </h2>
              </Grid>
              <Grid item xs={2} sm={2} className="close">
                <CloseIcon onClick={handleCloseModal} />
              </Grid>
            </Grid>
            <div>             
              {ErrMsg === true ? (
                <div   style={{ textAlign: 'center' }}  className="addfieldserror">
                Please select one among the list
                </div>
              ) : (
                ''
              )}
              <Grid container spacing={3} className="userformcontent">
               <FormControl className="content">
   
                  <Grid
                  item
                  xs={12}
                  sm={12}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Typography
                    style={{
                      cursor: 'pointer',
                      textAlign: 'left',
                      fontFamily: 'roboto,arial,sans-serif',
                      marginBottom: '10px',
                      color:"#0b153e"
                    }}
                  >
                    Please select a Work Flow
                  </Typography>
                  </Grid>
                  <Grid
                  item
                  xs={12}
                  sm={12}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <RadioGroup
                    aria-label="WorkFlow"
                    name="WorkFlow"
                    value={value}
                    onChange={handleChange}
                
                  >
                      
                      <FormControlLabel
                        value="X124010850"
                        color="primary"
                        control={<Radio />}
                        label="X124010850"
                      />
            
                      <FormControlLabel
                        value="X124010825"
                        control={<Radio color="primary" />}
                        label="X124010825"
                      />
               
                   
                  </RadioGroup>
                  </Grid> 

             </FormControl>
           
             <Grid
                  item
                  xs={12}
                  sm={12}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Button
                    onClick={handleSubmit}
              
                    variant="contained"
                    fullWidth="true"
                    className="createpartnerbutton"
                  >
                    Create Work Flow
                  </Button>
                </Grid>
                </Grid>
            </div>
           
            </div>
            </Modal>
      </MuiThemeProvider>
    </div>
  );
}
