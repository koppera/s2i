import React, { useEffect, useContext, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import { useMutation, useQuery } from 'graphql-hooks';
import Snackbar from '@material-ui/core/Snackbar';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Switch from '@material-ui/core/Switch';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Grid from '@material-ui/core/Grid';
import Collapse from '@material-ui/core/Collapse';
import CloseIcon from '@material-ui/icons/Close';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Modal from '@material-ui/core/Modal';
import Theme from '../css/theme';
import Loader from '../components/loader';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import FormLabel from '@material-ui/core/FormLabel';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import SyncAltIcon from '@material-ui/icons/SyncAlt';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { Typography } from '@material-ui/core';
export default function ConnectorsMain() {
  const [values, setValues] = React.useState({
    connectorID: '',
    connection: '',
    settingsFormat: 'Property List',
    server: '',
    port: '',
    dataBase: '',
    user: '',
    password: '',
    characterset: '',
    integratedSecurity: '',
    maxRows: '',
    other: '',
    queryPassthrough: '',
    readonly: '',
    sslServerCert: '',
    timeOut: '',
    usessl: '',
    fireWallPassword: '',
    fireWallPort: '',
    fireWallServer: '',
    fireWallType: '',
    zeroDatesToNull: '',
  });
  const [submitErr, setSubmitErr] = React.useState(false);
  const [advcData, setAdvcData] = React.useState(false);
  const [firewallData, setFirewallData] = React.useState(false);
  const [authenticationData, setAuthenticationData] = React.useState(false);
  const [ErrMsg, setErrMsg] = React.useState(false);
  const [errorInfo, setErrorInfo] = React.useState({
    connectorID: false,
    connection: false,
    settingsFormat: false,
    server: false,
    port: false,
    dataBase: false,
    user: false,
    password: false,
    characterset: false,
    integratedSecurity: false,
    maxRows: false,
    queryPassthrough: false,
    sslServerCert: false,
    fireWallPassword: false,
    fireWallPort: false,
    fireWallServer: false,
    fireWallType: false,
    zeroDatesToNull: false,
  });
  const handleChange = (name) => (event) => {
    console.log(event.target.value, name);
    setErrMsg(false);
    setValues({ ...values, [name]: event.target.value });
    switch (name) {
      case 'connectorID':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[A-Z0-9]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'connection':
        setValues({
          ...values,
          [event.target.name]: event.target.value,
        });
        break;
      case 'settingsFormat':
        setValues({
          ...values,
          [event.target.name]: event.target.value,
        });
        break;
      case 'server':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[a-zA-z/ ]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'port':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match(
            '^([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$'
          )
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
        } else setErrorInfo({ ...errorInfo, [name]: true });
        break;
      case 'dataBase':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[a-zA-z/ ]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
        } else setErrorInfo({ ...errorInfo, [name]: true });
        break;

      case 'user':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[a-zA-z/ ]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
        } else setErrorInfo({ ...errorInfo, [name]: true });
        break;
      case 'password':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match(
            '^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{4,8}$'
          )
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'integratedSecurity':
        setValues({
          ...values,
          [event.target.name]: event.target.value,
        });
        break;
      case 'maxRows':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[-(0-9)]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
        } else setErrorInfo({ ...errorInfo, [name]: true });
        break;
      case 'other':
        setValues({
          ...values,
          [event.target.name]: event.target.value,
        });
        break;
      case 'queryPassthrough':
        setValues({
          ...values,
          [event.target.name]: event.target.value,
        });
        break;
      case 'readonly':
        setValues({
          ...values,
          [event.target.name]: event.target.value,
        });
        break;
      case 'sslServerCert':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[a-zA-Z0-9/. ]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
        } else setErrorInfo({ ...errorInfo, [name]: true });
        break;
      case 'timeOut':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[0-9]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
        } else setErrorInfo({ ...errorInfo, [name]: true });
        break;
      case 'zeroDatesToNull':
        setValues({
          ...values,
          [event.target.name]: event.target.value,
        });
        break;
      case 'usessl':
        setValues({
          ...values,
          [event.target.name]: event.target.value,
        });
        break;
      case 'fireWallPassword':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match(
            '^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{4,8}$'
          )
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'fireWallPort':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match(
            '^([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$'
          )
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
        } else setErrorInfo({ ...errorInfo, [name]: true });
        break;
      case 'dataBase':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[a-zA-z/ ]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
        } else setErrorInfo({ ...errorInfo, [name]: true });
        break;
      case 'fireWallServer':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[a-zA-z/ ]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'fireWallType':
        setValues({
          ...values,
          [event.target.name]: event.target.value,
        });
        break;
      default:
        return false;
    }
  };
  const handleAdvanced = () => {
    setAdvcData(!advcData);
    console.log(advcData, 'line 269');
    if (advcData == false) {
      setValues({
        ...values,
        characterset: '',
        integratedSecurity: '',
        maxRows: '',
        other: '',
        queryPassthrough: '',
        readonly: '',
        sslServerCert: '',
        timeOut: '',
        zeroDatesToNull: '',
      });
    }
  };
  const handleFirewallData = () => {
    setFirewallData(!firewallData);
    if (firewallData == false) {
      setValues({
        ...values,
        fireWallPassword: '',
        fireWallPort: '',
        fireWallServer: '',
        fireWallType: '',
      });
    }
  };
  const handleAuthentication = () => {
    setAuthenticationData(!authenticationData);
    if (authenticationData == false) {
      setValues({ ...values, usessl: '' });
    }
  };
  const handleSubmit = async () => {
    console.log(values, 'line 263', advcData);
    if (
      values.connectorID !== '' &&
      values.connection !== '' &&
      values.server !== '' &&
      values.port !== '' &&
      values.dataBase !== '' &&
      values.user !== '' &&
      values.password !== '' &&
      errorInfo.connectorID !== true &&
      errorInfo.server !== true &&
      errorInfo.port !== true &&
      errorInfo.dataBase !== true &&
      errorInfo.user !== true &&
      errorInfo.password !== true &&
      (advcData == false ||
        (advcData == true &&
          values.characterset !== '' &&
          values.integratedSecurity !== '' &&
          values.maxRows !== '' &&
          values.queryPassthrough !== '' &&
          values.readonly !== '' &&
          values.sslServerCert !== '' &&
          values.timeOut !== '' &&
          values.zeroDatesToNull !== '' &&
          errorInfo.characterset !== true &&
          errorInfo.maxRows !== true &&
          errorInfo.timeOut !== true &&
          errorInfo.sslServerCert !== true)) &&
      (authenticationData == false ||
        (authenticationData == true && values.usessl !== '')) &&
      (firewallData == false ||
        (firewallData == true &&
          values.fireWallPassword !== '' &&
          values.fireWallPort !== '' &&
          values.fireWallServer !== '' &&
          values.fireWallType !== '' &&
          errorInfo.fireWallPassword !== true &&
          errorInfo.fireWallPort !== true &&
          errorInfo.fireWallServer !== true))
    ) {
      setErrMsg(false);
      console.log(values, 'submitted line 355');
      setValues({
        ...values,
        connectorID: values.connectorID,
        connection: values.connection,
        settingsFormat: values.settingsFormat,
        server: values.server,
        port: values.port,
        dataBase: values.dataBase,
        user: values.user,
        password: values.password,
        characterset: values.characterset,
        integratedSecurity: values.integratedSecurity,
        maxRows: values.maxRows,
        other: values.other,
        queryPassthrough: values.queryPassthrough,
        readonly: values.readonly,
        sslServerCert: values.sslServerCert,
        timeOut: values.timeOut,
        usessl: values.usessl,
        fireWallPassword: values.fireWallPassword,
        fireWallPort: values.fireWallPort,
        fireWallServer: values.fireWallServer,
        fireWallType: values.fireWallType,
        zeroDatesToNull: values.zeroDatesToNull,
      });
      //submit functionalitye here
    } else if (
      values.connectorID == '' &&
      values.connection == '' &&
      values.server == '' &&
      values.port == '' &&
      values.dataBase == '' &&
      values.user == '' &&
      values.password == ''
    ) {
      setErrMsg(true);
    } else {
      setErrMsg(false);
      setSubmitErr(true);
    }
  };
  const handleReset = async () => {
    setErrMsg(false);
    setErrorInfo({
      connectorID: false,
      connection: false,
      settingsFormat: false,
      server: false,
      port: false,
      dataBase: false,
      user: false,
      password: false,
      integratedSecurity: false,
    });
    setValues({
      ...values,
      connectorID: '',
      connection: '',
      settingsFormat: 'Property List',
      server: '',
      port: '',
      dataBase: '',
      user: '',
      password: '',
      integratedSecurity: '',
      other: '',
      queryPassthrough: '',
      readonly: '',
      timeOut: '',
      zeroDatesToNull: '',
      usessl: '',
      fireWallPassword: '',
      fireWallPort: '',
      fireWallServer: '',
    });
    setSubmitErr(false);
  };
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides:
       
      {
        MuiInputLabel: {
          animated: {
            fontSize: 13,
          },
          outlined: {
            paddingTop: 3,
          },
        },
        MuiSelect: {
          select: {
            minWidth: '180px',
          },
        },
        MuiTypography:{body1:{
          fontSize:12,
          fontWeight:'bold'
          }},
      },
    });
  return (
    <div>
      <MuiThemeProvider theme={getMuiTheme()}>
        <div>
          {ErrMsg === true ? (
            <div style={{ textAlign: 'center' }} className="addfieldserror">
              Please fill the fields
            </div>
          ) : (
            ''
          )}
          <Grid item xs={12} sm={12}>
            <h2 className="Connectorsheader" style={{ color: '#0b153e',paddingLeft:'15px' }}>
              Connector Details
            </h2>
          </Grid>
          <form noValidate autoComplete="off">
            <Grid className="AddressconfigGrid">
              <Grid container spacing={1} className="ConnectorsFrom">
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <TextField
                    id="outlined-dense"
                    label="Connector ID"
                    className="partnertextField"
                    margin="dense"
                    variant="outlined"
                    //placeholder=""
                    onChange={handleChange('connectorID')}
                    value={values.connectorID}
                    name="connectorID"
                  />
                  {errorInfo.connectorID === true ? (
                    <div id="nameid" className="addfieldserror">
                      "Allowed Uppercase Alphabets & Numbers."
                    </div>
                  ) : (
                    ''
                  )}
                  {submitErr && !values.connectorID ? (
                    <div id="nameid" className="addfieldserror">
                      Please enter Connector ID
                    </div>
                  ) : (
                    ''
                  )}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <FormControl>
                    <InputLabel
                      style={{ color: '#0b153e', fontSize: 12 }}
                      id="demo-simple-select-label"
                    >
                      Connection
                    </InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={values.connection}
                      onChange={handleChange('connection')}
                      name="connection"
                    >
                      <MenuItem value={'MySQL'}>MySQL</MenuItem>
                    </Select>
                  </FormControl>
                  {submitErr && !values.connection ? (
                    <div id="nameid" className="addfieldserror">
                      Please select connection
                    </div>
                  ) : (
                    ''
                  )}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={6}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                 {/* <FormLabel className="Connectorsheader">Settings Format</FormLabel> */}
                  <Grid item xs={12} sm={6}>
            <Typography className="Connectorsheader" style={{ color: '#0b153e',padding:'1px'}}>
            Settings Format
            </Typography>
          </Grid>
                  <RadioGroup
                    aria-label="SettingsForma"
                    name="settingsFormat"
                    color="primary"
                    value={values.settingsFormat}
                    onChange={handleChange('settingsFormat')}
                    row
                  >
                    <Grid item xs={6} sm={6} className='Connectorstext'>
                      <FormControlLabel
                        value="Property List"
                        color="primary"
                        control={<Radio />}
                        label="Property List"
                      />
                    </Grid>
                    <Grid item xs={6} sm={6}>
                      <FormControlLabel
                        value="Connection String"
                        control={<Radio color="primary" />}
                        label="Connection String"
                      />
                    </Grid>
                  </RadioGroup>
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <TextField
                    id="outlined-dense"
                    label="Server"
                    className="partnertextField"
                    margin="dense"
                    variant="outlined"
                    onChange={handleChange('server')}
                    value={values.server}
                    name="server"
                  />
                  {errorInfo.server === true ? (
                    <div id="nameid" className="addfieldserror">
                      "Enter only alphabets"
                    </div>
                  ) : (
                    ''
                  )}
                  {submitErr && !values.server ? (
                    <div id="nameid" className="addfieldserror">
                      Please enter server
                    </div>
                  ) : (
                    ''
                  )}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <TextField
                    id="outlined-dense"
                    label="Port"
                    className="partnertextField"
                    margin="dense"
                    variant="outlined"
                    onChange={handleChange('port')}
                    value={values.port}
                    name="port"
                  />
                  {errorInfo.port === true ? (
                    <div id="nameid" className="addfieldserror">
                      "Allows numbers only between 0 and 65535"
                    </div>
                  ) : (
                    ''
                  )}
                  {submitErr && !values.port ? (
                    <div id="nameid" className="addfieldserror">
                      Please enter port
                    </div>
                  ) : (
                    ''
                  )}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <TextField
                    id="outlined-dense"
                    label="Data Base"
                    className="partnertextField"
                    margin="dense"
                    variant="outlined"
                    onChange={handleChange('dataBase')}
                    value={values.dataBase}
                    name="dataBase"
                  />
                  {errorInfo.dataBase === true ? (
                    <div id="nameid" className="addfieldserror">
                      "Enter only alphabets"
                    </div>
                  ) : (
                    ''
                  )}
                  {submitErr && !values.dataBase ? (
                    <div id="nameid" className="addfieldserror">
                      Please enter data base
                    </div>
                  ) : (
                    ''
                  )}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <TextField
                    id="outlined-dense"
                    label="User "
                    className="partnertextField"
                    margin="dense"
                    variant="outlined"
                    onChange={handleChange('user')}
                    value={values.user}
                    name="user"
                  />
                  {errorInfo.user === true ? (
                    <div id="nameid" className="addfieldserror">
                      "Enter only alphabets"
                    </div>
                  ) : (
                    ''
                  )}
                  {submitErr && !values.user ? (
                    <div id="nameid" className="addfieldserror">
                      Please enter user
                    </div>
                  ) : (
                    ''
                  )}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={3}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <TextField
                    id="outlined-dense"
                    label="Password "
                    type="password"
                    className="partnertextField"
                    margin="dense"
                    variant="outlined"
                    onChange={handleChange('password')}
                    value={values.password}
                    name="password"
                  />
                  {errorInfo.password === true ? (
                    <div id="nameid" className="addfieldserror">
                      "4 to 8 characters must have atleast
                      one[uppercase,lowercase,special character and number]"
                    </div>
                  ) : (
                    ''
                  )}
                  {submitErr && !values.password ? (
                    <div id="nameid" className="addfieldserror">
                      Please enter password
                    </div>
                  ) : (
                    ''
                  )}
                </Grid>
              </Grid>
              <Grid container spacing={1} className="ConnectorsFrom">
                <Grid
                  item
                  xs={12}
                  sm={6}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Button
                    // onClick={handleReset}
                    variant="contained"
                    fullWidth="true"
                    className="createpartnerbutton"
                  >
                    <SyncAltIcon style={{ paddingRight: '10px' }} /> Test
                    Connection
                  </Button>
                </Grid>
              </Grid>

              <Grid className="Connectorsheader">
              <ListItem button onClick={handleAdvanced}  className="Connectorsheader">
                <ListItemText
               //className="Connectorsheader"
                  style={{ color: '#0b153e' }}
                  primary="Advanced"
                />
                {advcData ? (
                  <ExpandLess style={{ marginRight: '1.5%' }} />
                ) : (
                  <ExpandMore style={{ marginRight: '1.5%' }} />
                )}
              </ListItem>
              </Grid>
              <Collapse in={advcData} unmountOnExit>
                <Grid container spacing={1} className="ConnectorsFrom">
                  <Grid
                    item
                    xs={12}
                    sm={3}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
                    <TextField
                      id="outlined-dense"
                      label="Characterset"
                      className="partnertextField"
                      margin="dense"
                      variant="outlined"
                      onChange={handleChange('characterset')}
                      value={values.characterset}
                      name="characterset"
                    />
                    {errorInfo.characterset === true ? (
                      <div id="nameid" className="addfieldserror">
                        "Enter only alphabets"
                      </div>
                    ) : (
                      ''
                    )}
                    {submitErr && !values.characterset ? (
                      <div id="nameid" className="addfieldserror">
                        Please enter characterset
                      </div>
                    ) : (
                      ''
                    )}
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={3}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
                    <FormControl>
                      <InputLabel
                        style={{ color: '#0b153e', fontSize: 12 }}
                        id="demo-simple-select-label"
                      >
                        Integrated Security
                      </InputLabel>
                      <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={values.integratedSecurity}
                        onChange={handleChange('integratedSecurity')}
                        name="integratedSecurity"
                      >
                        <MenuItem value={false}>False</MenuItem>
                        <MenuItem value={true}>True</MenuItem>
                      </Select>
                    </FormControl>
                    {submitErr && values.integratedSecurity.length == 0 ? (
                      <div id="nameid" className="addfieldserror">
                        Please select integrated Security
                      </div>
                    ) : (
                      ''
                    )}
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={3}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
                    <TextField
                      id="outlined-dense"
                      label="Max Rows"
                      className="partnertextField"
                      margin="dense"
                      variant="outlined"
                      onChange={handleChange('maxRows')}
                      value={values.maxRows}
                      name="maxRows"
                    />
                    {errorInfo.maxRows === true ? (
                      <div id="nameid" className="addfieldserror">
                        "Enter only numeric characters"
                      </div>
                    ) : (
                      ''
                    )}
                    {submitErr && !values.maxRows ? (
                      <div id="nameid" className="addfieldserror">
                        Please enter max rows
                      </div>
                    ) : (
                      ''
                    )}
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={3}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
                    <TextField
                      id="outlined-dense"
                      label="Other(optional)"
                      className="partnertextField"
                      margin="dense"
                      variant="outlined"
                      onChange={handleChange('other')}
                      value={values.other}
                      name="other"
                    />
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={3}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
                    <FormControl>
                      <InputLabel
                        style={{ color: '#0b153e', fontSize: 12 }}
                        id="demo-simple-select-label"
                      >
                        Query Passthrough
                      </InputLabel>
                      <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={values.queryPassthrough}
                        onChange={handleChange('queryPassthrough')}
                        name="queryPassthrough"
                      >
                        <MenuItem value={false}>False</MenuItem>
                        <MenuItem value={true}>True</MenuItem>
                      </Select>
                    </FormControl>
                    {submitErr && values.queryPassthrough.length == 0 ? (
                      <div id="nameid" className="addfieldserror">
                        Please select integrated Security
                      </div>
                    ) : (
                      ''
                    )}
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={3}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
                    <FormControl>
                      <InputLabel
                        style={{ color: '#0b153e', fontSize: 12 }}
                        id="demo-simple-select-label"
                      >
                        Readonly
                      </InputLabel>
                      <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={values.readonly}
                        onChange={handleChange('readonly')}
                        name="readonly"
                      >
                        <MenuItem value={false}>False</MenuItem>
                        <MenuItem value={true}>True</MenuItem>
                      </Select>
                    </FormControl>
                    {submitErr && values.readonly.length == 0 ? (
                      <div id="nameid" className="addfieldserror">
                        Please select integrated Security
                      </div>
                    ) : (
                      ''
                    )}
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={3}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
                    <TextField
                      id="outlined-dense"
                      label="Ssl Server Cert"
                      className="partnertextField"
                      margin="dense"
                      variant="outlined"
                      onChange={handleChange('sslServerCert')}
                      value={values.sslServerCert}
                      name="sslServerCert"
                    />
                    {errorInfo.sslServerCert === true ? (
                      <div id="nameid" className="addfieldserror">
                        "Enter only alphanumeric characters"
                      </div>
                    ) : (
                      ''
                    )}
                    {submitErr && !values.sslServerCert ? (
                      <div id="nameid" className="addfieldserror">
                        Please enter ssl server cert
                      </div>
                    ) : (
                      ''
                    )}
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={3}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
                    <TextField
                      id="outlined-dense"
                      label="Timeout"
                      className="partnertextField"
                      margin="dense"
                      variant="outlined"
                      onChange={handleChange('timeOut')}
                      value={values.timeOut}
                      name="timeOut"
                    />
                    {errorInfo.timeOut === true ? (
                      <div id="nameid" className="addfieldserror">
                        "Enter only numeric characters"
                      </div>
                    ) : (
                      ''
                    )}
                    {submitErr && !values.timeOut ? (
                      <div id="nameid" className="addfieldserror">
                        Please enter timeout
                      </div>
                    ) : (
                      ''
                    )}
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={3}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
                    <FormControl>
                      <InputLabel
                        style={{ color: '#0b153e', fontSize: 12 }}
                        id="demo-simple-select-label"
                      >
                        Zero Dates To Null
                      </InputLabel>
                      <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={values.zeroDatesToNull}
                        onChange={handleChange('zeroDatesToNull')}
                        name="zeroDatesToNull"
                      >
                        <MenuItem value={false}>False</MenuItem>
                        <MenuItem value={true}>True</MenuItem>
                      </Select>
                    </FormControl>
                    {submitErr && values.zeroDatesToNull.length == 0 ? (
                      <div id="nameid" className="addfieldserror">
                        Please select zero dates to null
                      </div>
                    ) : (
                      ''
                    )}
                  </Grid>
                </Grid>
              </Collapse>
              <ListItem button onClick={handleAuthentication}>
                <ListItemText
                  className="Connectorsheader"
                  style={{ color: '#0b153e' }}
                  primary="Authentication"
                />
                {authenticationData ? (
                  <ExpandLess style={{ marginRight: '1.5%' }} />
                ) : (
                  <ExpandMore style={{ marginRight: '1.5%' }} />
                )}
              </ListItem>
              <Collapse in={authenticationData} unmountOnExit>
                <Grid container spacing={1} className="ConnectorsFrom">
                  <Grid
                    item
                    xs={12}
                    sm={3}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
                    <FormControl>
                      <InputLabel
                        style={{ color: '#0b153e', fontSize: 12 }}
                        id="demo-simple-select-label"
                      >
                        Use SSL
                      </InputLabel>
                      <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={values.usessl}
                        onChange={handleChange('usessl')}
                        name="usessl"
                      >
                        <MenuItem value={false}>False</MenuItem>
                        <MenuItem value={true}>True</MenuItem>
                      </Select>
                    </FormControl>
                    {submitErr && values.usessl.length == 0 ? (
                      <div id="nameid" className="addfieldserror">
                        Please select use ssl
                      </div>
                    ) : (
                      ''
                    )}
                  </Grid>
                </Grid>
              </Collapse>
              <ListItem button onClick={handleFirewallData}>
                <ListItemText
                  className="Connectorsheader"
                  style={{ color: '#0b153e' }}
                  primary="Firewall"
                />
                {firewallData ? (
                  <ExpandLess style={{ marginRight: '1.5%' }} />
                ) : (
                  <ExpandMore style={{ marginRight: '1.5%' }} />
                )}
              </ListItem>
              <Collapse in={firewallData} unmountOnExit>
                <Grid container spacing={1} className="ConnectorsFrom">
                  <Grid
                    item
                    xs={12}
                    sm={3}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
                    <TextField
                      id="outlined-dense"
                      label="Firewall Password "
                      type="password"
                      className="partnertextField"
                      margin="dense"
                      variant="outlined"
                      onChange={handleChange('fireWallPassword')}
                      value={values.fireWallPassword}
                      name="fireWallPassword"
                    />
                    {errorInfo.fireWallPassword === true ? (
                      <div id="nameid" className="addfieldserror">
                        "4 to 8 characters must have atleast
                        one[uppercase,lowercase,special character and number]"
                      </div>
                    ) : (
                      ''
                    )}
                    {submitErr && !values.fireWallPassword ? (
                      <div id="nameid" className="addfieldserror">
                        Please enter firewall password
                      </div>
                    ) : (
                      ''
                    )}
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={3}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
                    <TextField
                      id="outlined-dense"
                      label="Firewall Port"
                      className="partnertextField"
                      margin="dense"
                      variant="outlined"
                      onChange={handleChange('fireWallPort')}
                      value={values.fireWallPort}
                      name="fireWallPort"
                    />
                    {errorInfo.fireWallPort === true ? (
                      <div id="nameid" className="addfieldserror">
                        "Allows numbers only between 0 and 65535"
                      </div>
                    ) : (
                      ''
                    )}
                    {submitErr && !values.fireWallPort ? (
                      <div id="nameid" className="addfieldserror">
                        Please enter firewall port
                      </div>
                    ) : (
                      ''
                    )}
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={3}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
                    <TextField
                      id="outlined-dense"
                      label="Firewall Server"
                      className="partnertextField"
                      margin="dense"
                      variant="outlined"
                      onChange={handleChange('fireWallServer')}
                      value={values.fireWallServer}
                      name="fireWallServer"
                    />
                    {errorInfo.fireWallServer === true ? (
                      <div id="nameid" className="addfieldserror">
                        "Enter only alphabets"
                      </div>
                    ) : (
                      ''
                    )}
                    {submitErr && !values.fireWallServer ? (
                      <div id="nameid" className="addfieldserror">
                        Please enter firewall Server
                      </div>
                    ) : (
                      ''
                    )}
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={3}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
                    <FormControl>
                      <InputLabel
                        style={{ color: '#0b153e', fontSize: 12 }}
                        id="demo-simple-select-label"
                      >
                        Firewall Type
                      </InputLabel>
                      <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={values.fireWallType}
                        onChange={handleChange('fireWallType')}
                        name="fireWallType"
                      >
                        <MenuItem value={'none'}>NONE</MenuItem>
                      </Select>
                    </FormControl>
                    {submitErr && values.fireWallType.length == 0 ? (
                      <div id="nameid" className="addfieldserror">
                        Please select use ssl
                      </div>
                    ) : (
                      ''
                    )}
                  </Grid>
                </Grid>
              </Collapse>
              <Grid container spacing={1} className="ConnectorsFrom ">
                <Grid
                  item
                  xs={12}
                  sm={6}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Button
                    style={{
                      color: '#000006',
                      backgroundColor: '#E5CCFF',
                    }}
                    onClick={handleReset}
                    variant="contained"
                    fullWidth="true"
                    className="createpartnerbutton"
                  >
                    RESET
                  </Button>
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={6}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <Button
                    onClick={handleSubmit}
                    vvariant="contained"
                    fullWidth="true"
                    className="createpartnerbutton"
                  >
                    SAVE
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </form>
        </div>
      </MuiThemeProvider>
    </div>
  );
}
