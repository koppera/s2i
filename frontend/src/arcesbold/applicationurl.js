import MaterialTable from 'material-table';
import { useMutation, useQuery } from 'graphql-hooks';
import { forwardRef, useEffect } from 'react';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import ReactDOM from 'react-dom';
import MTableToolbar from 'material-table';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import CloseIcon from '@material-ui/icons/Close';
import { withStyles } from '@material-ui/core/styles';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import { Grid, Typography } from '@material-ui/core';
import MuiDialogActions from '@material-ui/core/DialogActions';
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import Timestamp from '../timestamp';
import Error from '../components/emptyPage';
import Loader from '../components/loader';
import Theme from '../css/theme';
import Snackbar from '@material-ui/core/Snackbar';
import ApplicationUrlFormModel from '../components/tmsmodal';
import ApplicationUrlForm from './applicationURLForm';
import MySnackbarContentWrapper from '../components/Snackbar';
import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles,
} from '@material-ui/core/styles';
function iconStyles() {
  return {};
}
const DISPLAY_All_ApplicationURL = `{
  allApplicationURL {
    idApplicationURL
    applicationURLUUID
    publicDomain
    asynchronousMDNURL
    receivingURL
    publicURL
    publicCertificate
    createdOn
    updatedOn
  }
}


`;
const ADD_ApplicationURL = `mutation createApplicationURL($ApplicationURLInput:ApplicationURLInput){
  createApplicationURL(input:$ApplicationURLInput) {
    publicDomain
    asynchronousMDNURL
    receivingURL
    publicURL
    publicCertificate
  
  }
}
`;

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(2),
    },
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));
const styles = (theme) => ({
  root: {
    Width: 1200,
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: '#FFFFFF',
  },
});
export default function MaterialTableDemo() {
  //FETCH ALL APPLICATION URL
  const { loading, error, data, refetch } = useQuery(
    DISPLAY_All_ApplicationURL
  );
  //ADD APPLICATION URL
  const [addApplicationURL, addloading, adddata] = useMutation(
    ADD_ApplicationURL
  );
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [serverError, setServerError] = React.useState(false);
  const [message, setSnackbarMessage] = React.useState();
  const [variant, setSnackbarVariant] = React.useState('error');
  const [applicationURLData, setData] = React.useState([]);
  const [openApplicationModal, setOpenApplicationModal] = React.useState(false);
  const [applicationForm, setApplicationForm] = React.useState('update');
  const [rowData, setrowData] = React.useState();
  const columns = [
    { title: 'Public Domains', field: 'publicDomain' },
    { title: 'Asynchronous MDN URL', field: 'asynchronousMDNURL' },
    { title: 'Receiving URL', field: 'receivingURL' },
    { title: 'Public URL', field: 'publicURL' },
    { title: 'Public Certificate', field: 'publicCertificate' },
  ];
  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }

  const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => (
      <Clear color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline color="error" {...props} ref={ref} />
    )),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => (
      <Edit {...props} ref={ref} onClick={console.log('hello world')} />
    )),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };

  useEffect(() => {
    if (data !== undefined) {
      console.log(data.allApplicationURL);
      setData(data.allApplicationURL);
      setServerError(false);
    } else {
      console.log('server error');
      setServerError(true);
    }
  }, [data]);

  const handleRefetch = (value, message, modelresp) => {
    refetch({
      updateData(_, data) {
        return data;
      },
    });
    setOpenSnackbar(true);
    setSnackbarMessage(message);
    if (value) {
      setSnackbarVariant('error');
    } else {
      setSnackbarVariant('success');
    }
    if (modelresp) {
      handleCloseForm();
    }
  };

  const handleClickUpdateOpen = (e, rowData) => {
    console.log(e, rowData);
    setApplicationForm('Update');
    setrowData(rowData);
    setOpenApplicationModal(true);
  };
  const handleClickAddOpen = (e) => {
    setApplicationForm('Add');
    setOpenApplicationModal(true);
  };
  const handleCloseForm = (e) => {
    setOpenApplicationModal(false);
  };
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiDialogTitle: {
          root: {
            backgroundColor: '#0b153e',
            color: '#FFFFFF',
          },
        },

        MuiTypography: {
          h6: {
            fontSize: 14,
            fontFamily: 'Arial !important',
          },
        },
        MuiSelect: {
          icon: {
            color: '#0b153e',
          },
        },
      },
    });

  async function handleAddApplicationURL(newData) {
    return await new Promise(async (resolve) => {
      console.log(newData);
      resolve();
      await addApplicationURL({
        variables: {
          ApplicationURLInput: {
            publicDomain: newData.publicDomain,
            asynchronousMDNURL: newData.asynchronousMDNURL,
            receivingURL: newData.receivingURL,
            publicURL: newData.publicURL,
            publicCertificate: newData.publicCertificate,
          },
        },
      })
        .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
          console.log(data, error, graphQLErrors, networkError, cacheHit);
          if (data !== null && error == false) {
            handleRefetch(error, 'Saved successfully');
          } else if (error && graphQLErrors.length > 0) {
            let duperror = graphQLErrors[0].message;
            console.log(duperror);
            if (duperror !== null && duperror.indexOf('ER_DUP_ENTRY') !== -1) {
              handleRefetch(error, 'Name already existed');
            }
          }
        })
        .catch((e) => {
          // you can do something with the error here
          console.log(e);
        });
    });
  }

  const classes1 = useStyles();
  if (loading) return <Loader />;
  if (serverError)
    return <Error type={'Server connection lost.Please try again'} />;
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <MaterialTable
        icons={tableIcons}
        title="Application URLs"
        columns={columns}
        data={applicationURLData}
        editable={{
          onRowDelete: (oldData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                resolve();
              }, 600);
            }),
        }}
        options={{
          headerStyle: {
            textAlign: 'center',
            fontSize: 12,
            fontWeight: 'bold',
            fontFamily: 'Arial !important',
            backgroundColor: '#0b153e',
            color: '#ffffff',
            padding: '4px',
          },
          searchFieldStyle: {
            color: '#0b153e',
          },
          actionsColumnIndex: -1,
          pageSize: 10,
          pageSizeOptions: [10, 25, 50, 100],
          toolbar: true,
          paging: true,
        }}
        actions={[
          {
            icon: AddIcon,
            tooltip: 'Add',
            className: 'addIconColor',
            isFreeAction: true,
            onClick: (event) => handleClickAddOpen(event),
          },
          {
            icon: Edit,
            tooltip: 'Edit',
            className: 'addIconColor',
            onClick: (event, rowData) => handleClickUpdateOpen(event, rowData),
          },
        ]}
      />
      <ApplicationUrlFormModel
        handleCloseModal={handleCloseForm}
        isModalOpen={openApplicationModal}
      >
        <ApplicationUrlForm
          handleCloseModal={handleCloseForm}
          onSuccess={handleRefetch}
          isModalOpen={openApplicationModal}
          rowData={rowData}
          title={applicationForm}
        />
      </ApplicationUrlFormModel>
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        autoHideDuration={1500}
        open={openSnackbar}
        onClose={handleCloseSnackbar}
      >
        <MySnackbarContentWrapper
          onClose={handleCloseSnackbar}
          variant={variant}
          message={message}
        />
      </Snackbar>
    </MuiThemeProvider>
  );
}
