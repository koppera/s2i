import React, { useEffect, useContext, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useMutation, useQuery } from 'graphql-hooks';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import '../css/commonStyle.css';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import Theme from '../css/theme';
import Modal from '@material-ui/core/Modal';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
const ADD_CONNECTOR = `
mutation createConnectorsAPI($conectorInput:conectorInput){
  createConnectorsAPI(input:$conectorInput) {
    idConnectors
    connectorsUUID
    connectorID
    workspaceUUID
    ConnectorType
    ParentConnector
    createdOn
    updatedOn
  }
}
`;
const ALL_WorkSpace = `{
  fetchWorkSpace {
    idworkspace
    workspaceUUID
    workspaceName
    createdOn
    updatedOn
  }
}
`;
const Connectors_BY_workspace = `query fetchConnectorsByWorkspaceUUID($workspaceuuid:String){
  fetchConnectorsByWorkspaceUUID (workspaceuuid:$workspaceuuid) {
    idConnectors
    connectorsUUID
    connectorID
    workspaceUUID
    workspaceName
    ConnectorType
    ParentConnector
    createdOn
    updatedOn
  }
    
}

`;
const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

const getMuiTheme = () =>
  createMuiTheme(Theme, {
    overrides: {},
  });

export default function AddProjectForm(props) {
  //ADD CONNECTOR
  const [addConnector, addloading, adddata] = useMutation(ADD_CONNECTOR);
  const {
    loading: wloading,
    data: wdata,
    errors: werrors,
    networkError: wnetworkError,
  } = useQuery(ALL_WorkSpace);
  const {
    loading: cloading,
    data: cdata,
    errors: cerrors,
    networkError: cnetworkError,
  } = useQuery(Connectors_BY_workspace, {
    variables: {
      workspaceuuid: props.wsRowData.workspaceUUID,
    },
  });

  const classes = useStyles();
  const [values, setValues] = React.useState({
    ConnectorType: '',
    ConnectorID: '',
    parentConnector: null,
  });
  const [ErrMsgNameInfo, setErrMsgNameInfo] = React.useState(false);
  const [ErrMsg, setErrMsg] = React.useState(false);
  const [errorInfo, setErrorInfo] = React.useState({
    ConnectorID: false,
    parentConnector: false,
    ConnectorType: false,
  });
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [value, setValue] = React.useState('IS');
  const [variant, setSnackbarVariant] = React.useState('error');
  const [message, setSnackbarMessage] = React.useState();
  const [workspaceData, setWorkspace] = React.useState([]);
  const [parentConnectorData, setParentConnector] = React.useState([]);
  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }
  const handleCloseModal = () => {
    props.handleCloseModal(false);
  };
  const handleChange = (name) => (event) => {
    setErrMsg(false);

    setValues({ ...values, [name]: event.target.value });
    switch (name) {
      case 'ConnectorType':
        setValues({
          ...values,
          [event.target.name]: event.target.value,
        });
        break;
      case 'ConnectorID':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else {
          if (
            event.target.value.match('^[a-zA-Z0-9]*$') &&
            event.target.value.length <= 25
          ) {
            setErrorInfo({ ...errorInfo, [name]: false });
            setValues({ ...values, [name]: event.target.value });
          } else {
            setErrorInfo({ ...errorInfo, [name]: true });
          }
        }
        break;

      case 'workspace':
        setValues({
          ...values,
          [event.target.name]: event.target.value,
        });
        break;
      case 'parentConnector':
        setValues({
          ...values,
          [event.target.name]: event.target.value,
        });
        break;
      default:
        return false;
    }
  };
  useEffect(() => {
    if (wdata !== undefined) {
      console.log(wdata.fetchWorkSpace);
      setWorkspace(wdata.fetchWorkSpace);
    }
    if (cdata !== undefined) {
      console.log(cdata.fetchConnectorsByWorkspaceUUID);
      setParentConnector(cdata.fetchConnectorsByWorkspaceUUID);
    }
  }, [wdata, cdata]);
  function search(nameKey, myArray) {
    for (var i = 0; i < myArray.length; i++) {
      if (myArray[i].connectorID === nameKey) {
        return nameKey;
      }
    }
  }
  const handleSubmit = async () => {
    console.log(values);
    setErrMsg(false);

    if (
      values.ConnectorID === '' &&
      values.parentConnector === '' &&
      values.ConnectorType === ''
    ) {
      setErrMsg(true);
    } else {
      setErrMsg(false);
      //setValues({ ...values, ConnectorID: '', workspace: '' });
      console.log(values.ConnectorType);
      var resultObject = search(values.ConnectorID, parentConnectorData);
      console.log(resultObject);
      if (resultObject == values.ConnectorID) {
        setOpenSnackbar(true);
        setSnackbarVariant('error');
        setSnackbarMessage('ConnectorID already taken');
        // props.onSuccess(true, 'ConnectorID already taken', false);
      } else {
        await addConnector({
          variables: {
            conectorInput: {
              connectorID: values.ConnectorID,
              workspaceUUID: props.wsRowData.workspaceUUID,
              ParentConnector:
                values.parentConnector == '' ? null : values.parentConnector,
              ConnectorType: values.ConnectorType,
            },
          },
        })
          .then(
            ({
              data,
              error,
              graphQLErrors,
              networkError,
              cacheHit,
              loading,
            }) => {
              console.log(
                data,
                error,
                graphQLErrors,
                networkError,
                cacheHit,
                loading
              );
              if (data !== null && error == false) {
                console.log(' created successfully');
                setErrMsg(false);

                props.onSuccess(error, 'Saved successfully', true);
              } else {
                props.onSuccess(error, 'Graphql hooks error', false);
              }
            }
          )
          .catch((e) => {
            // you can do something with the error here
            console.log(e);
          });
      }
    }
  };

  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={props.isModalOpen}
        onClose={handleCloseModal}
        className="fabmodal"
      >
        <div className="modalpaper">
          <Grid container className="header">
            <Grid item xs={10} sm={10}>
              <h2 id="modal-title" className="h2">
                Connector
              </h2>
            </Grid>
            <Grid item xs={2} sm={2} className="close">
              <CloseIcon onClick={handleCloseModal} />
            </Grid>
          </Grid>
          <div>
            {ErrMsg === true ? (
              <div style={{ textAlign: 'center' }} className="addfieldserror">
                Please fill the fields
              </div>
            ) : (
              ''
            )}
            <form className="content" noValidate autoComplete="off">
              <Grid container spacing={3} className="userformcontent">
                <Grid item xs={12} sm={4}>
                  <FormControl>
                    <InputLabel
                      style={{ color: '#0b153e', fontSize: 12 }}
                      id="demo-simple-select-label"
                    >
                      ConnectorType
                    </InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      onChange={handleChange('ConnectorType')}
                      value={values.ConnectorType}
                      name="ConnectorType"
                    >
                      <MenuItem value={'REST'}>REST</MenuItem>
                      <MenuItem value={'JSON'}>JSON</MenuItem>
                      <MenuItem value={'Webhook'}>Webhook</MenuItem>
                      <MenuItem value={'XML Map'}>XML Map</MenuItem>
                      <MenuItem value={'X12'}>X12</MenuItem>
                    </Select>
                  </FormControl>
                  {errorInfo.ConnectorType === true ? (
                    <div className="addfieldserror">
                      "Allowed alphabets,no special characters and Numerics are
                      allowed.Maximum length is 25."
                    </div>
                  ) : (
                    ''
                  )}
                </Grid>
                <Grid item xs={12} sm={4}>
                  <TextField
                    id="outlined-dense"
                    label="Connector ID "
                    className="textField"
                    margin="dense"
                    variant="outlined"
                    onChange={handleChange('ConnectorID')}
                    value={values.ConnectorID}
                    name="ConnectorID"
                  />

                  {errorInfo.ConnectorID === true ? (
                    <div className="addfieldserror">
                      "Allowed alphabets,no special characters and Numerics are
                      allowed.Maximum length is 25."
                    </div>
                  ) : (
                    ''
                  )}
                </Grid>
                <Grid item xs={12} sm={4}>
                  {/*  <FormControl>
                    <InputLabel
                      style={{ color: '#0b153e', fontSize: 12 }}
                      id="demo-simple-select-label"
                    >
                      WorkSpace
                    </InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={values.workspace}
                      onChange={handleChange('workspace')}
                      name="workspace"
                    >
                      {workspaceData != undefined && workspaceData.length > 0
                        ? workspaceData.map((v, k) => {
                            return (
                              <MenuItem value={v.workspaceUUID}>
                                {v.workspaceName}
                              </MenuItem>
                            );
                          })
                        : null}
                    </Select>
                  </FormControl>

                  {errorInfo.workspace === true ? (
                    <div className="addfieldserror">
                      "Allowed alphabets,no special characters and Numerics are
                      allowed.Maximum length is 25."
                    </div>
                  ) : (
                    ''
                  )} */}
                  <TextField
                    id="outlined-dense"
                    label="Workspace "
                    className="textField"
                    margin="dense"
                    variant="outlined"
                    onChange={handleChange('workspace')}
                    value={props.wsRowData.workspaceName}
                    InputProps={{
                      readOnly: true,
                    }}
                    name="workspace"
                  />

                  {errorInfo.workspace === true ? (
                    <div className="addfieldserror">
                      "Allowed alphabets,no special characters and Numerics are
                      allowed.Maximum length is 25."
                    </div>
                  ) : (
                    ''
                  )}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={4}
                  gutterBottom
                  variant="body2"
                  color="textSecondary"
                  className="text"
                >
                  <FormControl>
                    <InputLabel
                      style={{ color: '#0b153e', fontSize: 12 }}
                      id="demo-simple-select-label"
                    >
                      Parent Connector
                    </InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={values.parentConnector}
                      onChange={handleChange('parentConnector')}
                      name="parentConnector"
                    >
                      <MenuItem value={null}>None</MenuItem>

                      {parentConnectorData != undefined &&
                      parentConnectorData.length > 0
                        ? parentConnectorData.map((v, k) => {
                            return (
                              <MenuItem value={v.connectorsUUID}>
                                {v.connectorID}
                              </MenuItem>
                            );
                          })
                        : null}
                    </Select>
                  </FormControl>
                </Grid>
                <Button
                  onClick={handleSubmit}
                  variant="contained"
                  fullWidth="true"
                  className="createpartnerbutton"
                >
                  Create
                </Button>
              </Grid>
            </form>

            <Snackbar
              anchorOrigin={{
                vertical: 'top',
                horizontal: 'center',
              }}
              autoHideDuration={5000}
              open={openSnackbar}
              onClose={handleCloseSnackbar}
            >
              <MySnackbarContentWrapper
                onClose={handleCloseSnackbar}
                variant={variant}
                message={message}
              />
            </Snackbar>
          </div>
        </div>
      </Modal>
    </MuiThemeProvider>
  );
}
