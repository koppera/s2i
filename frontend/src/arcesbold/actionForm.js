import React, { useEffect, useContext, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import '../css/commonStyle.css';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import Theme from '../css/theme';
import Modal from '@material-ui/core/Modal';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import {
  createMuiTheme,
  MuiThemeProvider,
} from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing(2)
  }
}));


const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
      
      }
    });

export default function AddProjectForm(props) {
  const classes = useStyles();
  const [values, setValues] = React.useState({ name: '', });
  const [ErrMsgNameInfo, setErrMsgNameInfo] = React.useState(false);
  const [ErrMsg, setErrMsg] = React.useState(false);
  const [errorInfo, setErrorInfo] = React.useState({
    name: false,
   
  });
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [value, setValue] = React.useState('IS');
  const [variant, setSnackbarVariant] = React.useState('error');
  const [message, setSnackbarMessage] = React.useState();

  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }

  const handleChange = name => event => {
    setErrMsg(false);
    
    setValues({ ...values, [name]: event.target.value });
    switch (name) {
      case 'name':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else {
          if (
            event.target.value.match() &&
            event.target.value.length <= 25
          ) {
            setErrorInfo({ ...errorInfo, [name]: false });
            setValues({ ...values, [name]: event.target.value });
          } else {
            setErrorInfo({ ...errorInfo, [name]: true });
          }
        }
        break;

     

      default:
        return false;
    }
  };

  const handleCloseModal = () => {
    props.handleCloseModal(false);
  };

  const handleSubmit = async () => {
    console.log(values);
    setErrMsg(false);
    
    if (values.name === '') {
      setErrMsg(true);
    } else {
      setErrMsg(false);
      setValues({ ...values, name: ''});
    }
  };

  return (
    <MuiThemeProvider theme={getMuiTheme()}>
         <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={props.isModalOpen}
          onClose={handleCloseModal}
          className="fabmodal"
        >
          <div className="modalpaper">
            <Grid container className="header">
              <Grid item xs={10} sm={10}>
                <h2 id="modal-title" className="h2">
                Action
                </h2>
              </Grid>
              <Grid item xs={2} sm={2} className="close">
                <CloseIcon onClick={handleCloseModal} />
              </Grid>
            </Grid>
    <div>
      {ErrMsg === true ? (
        <div style={{ textAlign: 'center' }} className="addfieldserror">
          Please fill the fields
        </div>
      ) : (
        ''
      )}
      <form className="content" noValidate autoComplete="off">
        <TextField
          id="outlined-dense"
          label="Action Name "
          className="textField"
          margin="dense"
          variant="outlined"
          onChange={handleChange('name')}
          value={values.name}
          name="name"
        />

        {errorInfo.name === true ? (
          <div className="addfieldserror">
            "Allowed alphabets,no special characters and Numerics are
            allowed.Maximum length is 25."
          </div>
        ) : (
          ''
        )}

      
        <Button
          onClick={handleSubmit}
          variant="contained"
          fullWidth="true"
          className="createpartnerbutton"
        >
          Create
        </Button>
      </form>
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center'
        }}
        autoHideDuration={5000}
        open={openSnackbar}
        onClose={handleCloseSnackbar}
      >
        <MySnackbarContentWrapper
          onClose={handleCloseSnackbar}
          variant={variant}
          message={message}
        />
      </Snackbar>
    </div>
    </div>
    </Modal>
    </MuiThemeProvider>
  );
}
