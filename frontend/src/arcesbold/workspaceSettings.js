import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Theme from '../css/theme';
import Connection from './connectorsForm';
import InputTable from './inputTable.js';
import OutputTable from './outputTable.js';
import ServerTable from './serverTable.js';
import Users from './userTable';
import ResourcesTable from './resourcesTable.js';
import ActionTable from './actionTable.js';
import {
  createMuiTheme,
  MuiThemeProvider,
  } from '@material-ui/core/styles';
function TabPanel(props) {
  const { children, value, index, ...other } = props;
 
  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={8}>{children}</Box>}
    </Typography>
    
  );
 
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};
 
function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}
 
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));
 
const getMuiTheme = () =>
createMuiTheme(Theme, {
  overrides: {
    MuiTab: {
      root: {
        textTransform: 'none !important',
        minWidth:'12% !important',
        
      },
    },
  },
});


export default function SimpleTabs() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
 
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
 
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
       <Typography>
       <h2 className="h2" style={{ color: '#0b153e',fontSize: 18,
        fontFamily: 'Arial !important', }}>Workspace Settings</h2>
  </Typography>
    <div className={classes.root}>
      <AppBar position="static">
        
        <Tabs value={value} onChange={handleChange} aria-label="simple tabs example">
          <Tab label="Connection" {...a11yProps(0)} />
          <Tab label="Resources" {...a11yProps(1)} />
          <Tab label="Actions" {...a11yProps(2)} />
          <Tab label="Users" {...a11yProps(3)} />
          <Tab label="Server" {...a11yProps(4)} />
          <Tab label="Input" {...a11yProps(5)} />
          <Tab label="Output" {...a11yProps(6)} />
          <Tab label="Events" {...a11yProps(7)} />
         
        </Tabs>
      </AppBar>
     
      <TabPanel value={value} index={0}>
      <Connection/>
      </TabPanel>
      <TabPanel value={value} index={1}>
      <ResourcesTable/>
      </TabPanel>
      <TabPanel value={value} index={2}>
      <ActionTable/>
      </TabPanel>
      <TabPanel value={value} index={3}>
      <Users/>
      </TabPanel>
      <TabPanel value={value} index={4}>
      <ServerTable/>
      </TabPanel>
      <TabPanel value={value} index={5}>
    <InputTable/>
      </TabPanel>
      <TabPanel value={value} index={6}>
   <OutputTable/>
      </TabPanel>
      <TabPanel value={value} index={7}>
      Events
      </TabPanel>
     
    </div>
    </MuiThemeProvider>
  );
}