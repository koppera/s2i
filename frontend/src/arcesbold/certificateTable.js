import MaterialTable from 'material-table';
import { useMutation, useQuery } from 'graphql-hooks';
import { forwardRef, useEffect } from 'react';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import ReactDOM from 'react-dom';
import MTableToolbar from 'material-table';
import React, { Component } from 'react';
import Timestamp from '../timestamp';
import Button from '@material-ui/core/Button';
import Error from '../components/emptyPage';
import AddIcon from '@material-ui/icons/Add';
import Loader from '../components/loader';
import Theme from '../css/theme';
import CertificateFormModel from '../components/tmsmodal';
import CertificateForm from './certificateModelForm';
import Snackbar from '@material-ui/core/Snackbar';
import MySnackbarContentWrapper from '../components/Snackbar';
import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles,
} from '@material-ui/core/styles';
const DISPLAY_All_CERTIFICATE = `
{
  allCertificate {
    idCertificate
    certificateUUID
    commonName
    organization
    fileName
    serialNumber
    cPassword
    validPeriod
    keySize
    publicKeyType
    signatureAlgorithm
    createdOn
    lastUpdatedOn
  }
}



`;
function iconStyles() {
  return {};
}

export default function MaterialTableDemo() {
  //FETCH ALL CERTIFAICATES
  const { loading, error, data, refetch } = useQuery(DISPLAY_All_CERTIFICATE);
  const [certificateData, setData] = React.useState([]);
  const [openCertificateModal,setOpenCertificateModal]= React.useState(false);
  const [certificateFormTitle,setCertificateFormTitle]= React.useState('update');
  const [rowData, setrowData] = React.useState();
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [serverError, setServerError] = React.useState(false);
  const [message, setSnackbarMessage] = React.useState();
  const [variant, setSnackbarVariant] = React.useState('error'); 
  const columns = [
    { title: 'Common Name', field: 'commonName' },
    { title: 'Organization', field: 'organization' },
    { title: 'File Name', field: 'fileName' },
    { title: 'Serial Number', field: 'serialNumber' },
    { title: 'Password', field: 'cPassword' },
    { title: 'Validity Period(Years)', field: 'validPeriod' },
    { title: 'KeySize', field: 'keySize' },
    { title: 'Public Key Type', field: 'publicKeyType' },
    { title: 'Signature Algorithm', field: 'signatureAlgorithm' },
  ];
  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }

  const tableIcons = {
    Add: forwardRef((props, ref) => (
      <AddBox color="primary" {...props} ref={ref} />
    )),
    Check: forwardRef((props, ref) => (
      <Check color="primary" {...props} ref={ref} />
    )),
    Clear: forwardRef((props, ref) => (
      <Clear color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline color="error" {...props} ref={ref} />
    )),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => (
      <Edit color="primary" {...props} ref={ref} />
    )),
    Export: forwardRef((props, ref) => (
      <SaveAlt color="primary" {...props} ref={ref} />
    )),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => (
      <FirstPage color="primary" {...props} ref={ref} />
    )),
    LastPage: forwardRef((props, ref) => (
      <LastPage color="primary" {...props} ref={ref} />
    )),
    NextPage: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft color="primary" {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => (
      <Clear color="primary" {...props} ref={ref} />
    )),
    Search: forwardRef((props, ref) => (
      <Search color="primary" {...props} ref={ref} />
    )),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };
  useEffect(() => {
    if (data !== undefined) {
      console.log(data.allCertificate);
      setData(data.allCertificate);
      setServerError(false);
    } else {
      console.log('server error');
      setServerError(true);
    }
  }, [data]);
  const handleRefetch = (value, message, modelresp) => {
    refetch({
      updateData(_, data) {
        return data;
      },
    });
    setOpenSnackbar(true);
    setSnackbarMessage(message);
    if (value) {
      setSnackbarVariant('error');
    } else {
      setSnackbarVariant('success');
    }
    if (modelresp) {
      // handleCloseForm();
    }
  };
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSelect: {
          icon: {
            color: '#0b153e',
          },
        },
      
        MuiTypography: {
          h6: {
            fontSize: 14,
            fontFamily: 'Arial !important',
          },
        },
      },
    });
  /* if (loading || addressFormLoader) return <Loader />;
  if (serverError)
    return <Error type={'Sever connection lost.Please try again'} />;*/
    const handleClickUpdateOpen = (e, rowData) => {
      console.log(e, rowData);
      setCertificateFormTitle('Update');
      setrowData(rowData);
      setOpenCertificateModal(true);
    };
    const handleClickAddOpen = (e) => {
      setCertificateFormTitle('Add');
      setOpenCertificateModal(true);
    };
    const handleCloseForm = (e) => {
      setOpenCertificateModal(false);
    };
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
    <MaterialTable
     
     icons={tableIcons}
         title="Certificate"
      columns={columns}
      data={certificateData}
      editable={{
        onRowAdd: newData =>console.log('add'),
        onRowUpdate: (newData, oldData) =>console.log('Update'),
        onRowDelete: oldData =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              //
            }, 600);
          }),
      }} options={{
        headerStyle: {
        
        textAlign: 'center',
        fontSize:12,
        fontWeight:'bold',
        fontFamily:'Arial !important',
        backgroundColor:"#0b153e",
        color:"#ffffff",
        padding:'4px',
       
        },
          searchFieldStyle: {
            color: '#0b153e',
          },
          actionsColumnIndex: -1,
          pageSize: 10,
          pageSizeOptions: [10, 25, 50, 100],
          toolbar: true,
          paging: true,
        }}
       /* actions={[
          {
            icon: AddIcon,
            tooltip: 'Add',
            className: 'addIconColor',
            isFreeAction: true,
            onClick: (event) => handleClickAddOpen(event),
          },
          {
            icon: Edit,
            tooltip: 'Edit',
            className: 'addIconColor',
            onClick: (event, rowData) => handleClickUpdateOpen(event, rowData),
          },
        ]}*/
      />
        </MuiThemeProvider>    
  );
}
