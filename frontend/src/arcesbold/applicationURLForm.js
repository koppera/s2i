import React, { useEffect, useContext, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import { useMutation, useQuery } from 'graphql-hooks';
import Snackbar from '@material-ui/core/Snackbar';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Switch from '@material-ui/core/Switch';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import Modal from '@material-ui/core/Modal';
import Theme from '../css/theme';
import Loader from '../components/loader';
import Checkbox from '@material-ui/core/Checkbox';
import Typography from '@material-ui/core/Typography';

import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
const ADD_ApplicationURL = `mutation createApplicationURL($ApplicationURLInput:ApplicationURLInput){
  createApplicationURL(input:$ApplicationURLInput) {
    publicDomain
    asynchronousMDNURL
    receivingURL
    publicURL
    publicCertificate
  
  }
}
`;
export default function ApplicationURLModel(props) {
  //ADD APPLICATION URL
  const [addApplicationURL, addloading, adddata] = useMutation(
    ADD_ApplicationURL
  );
  const [values, setValues] = React.useState({
    publicDomains: '',
    asynchronousMDNURL: '',
    receivingURL: '',
    publicURL: '',
    publicCertificate: '',
  });
  const [submitErr, setSubmitErr] = React.useState(false);
  const [ErrMsg, setErrMsg] = React.useState(false);
  const [errorInfo, setErrorInfo] = React.useState({
    publicDomains: false,
    asynchronousMDNURL: false,
    receivingURL: false,
    publicURL: false,
    publicCertificate: false,
  });
  const handleChange = (name) => (event) => {
    console.log(event.target.value, name);
    setErrMsg(false);
    setValues({ ...values, [name]: event.target.value });
    switch (name) {
      case 'publicDomains':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[a-zA-Z ]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'asynchronousMDNURL':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match('^(http(s)?://)?([a-z][a-zA-Z0-9./]*)?$')
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'receivingURL':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match('^(http(s)?://)?([a-z][a-zA-Z0-9./]*)?$')
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'publicURL':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match('^(http(s)?://)?([a-z][a-zA-Z0-9./]*)?$')
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'publicCertificate':
        setValues({
          ...values,
          [event.target.name]: event.target.value,
        });
        break;

      default:
        return false;
    }
  };
  const handleSubmit = async () => {
    if (
      values.publicDomains !== '' &&
      values.asynchronousMDNURL !== '' &&
      values.receivingURL !== '' &&
      values.publicURL !== '' &&
      values.publicCertificate !== '' &&
      errorInfo.publicDomains !== true &&
      errorInfo.asynchronousMDNURL !== true &&
      errorInfo.receivingURL !== true &&
      errorInfo.publicURL !== true
    ) {
      setErrMsg(false);
      setValues({
        ...values,
        publicDomains: values.publicDomains,
        asynchronousMDNURL: values.asynchronousMDNURL,
        receivingURL: values.receivingURL,
        publicURL: values.publicURL,
        publicCertificate: values.publicCertificate,
      });
      //submit functionalitye here
      await addApplicationURL({
        variables: {
          ApplicationURLInput: {
            publicDomain: values.publicDomains,
            asynchronousMDNURL: values.asynchronousMDNURL,
            receivingURL: values.receivingURL,
            publicURL: values.publicURL,
            publicCertificate: values.publicCertificate,
          },
        },
      })
        .then(
          ({ data, error, graphQLErrors, networkError, cacheHit, loading }) => {
            console.log(
              data,
              error,
              graphQLErrors,
              networkError,
              cacheHit,
              loading
            );
            if (data !== null && error == false) {
              console.log(' created successfully');
              setErrMsg(false);

              props.onSuccess(error, 'Saved successfully', true);
            } else {
              props.onSuccess(error, 'Graphql hooks error', false);
            }
          }
        )
        .catch((e) => {
          // you can do something with the error here
          console.log(e);
        });
    } else if (
      values.publicDomains == '' &&
      values.asynchronousMDNURL == '' &&
      values.receivingURL == '' &&
      values.publicURL == '' &&
      values.publicCertificate == ''
    ) {
      setErrMsg(true);
    } else {
      setErrMsg(false);
      setSubmitErr(true);
    }
  };
  const handleReset = async () => {
    setErrMsg(false);
    setErrorInfo({
      publicDomains: false,
      asynchronousMDNURL: false,
      receivingURL: false,
      publicURL: false,
      publicCertificate: false,
    });
    setValues({
      ...values,
      publicDomains: '',
      asynchronousMDNURL: '',
      receivingURL: '',
      publicURL: '',
      publicCertificate: '',
    });
    setSubmitErr(false);
  };
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiInputLabel: {
          animated: {
            fontSize: 13,
          },
          outlined: {
            paddingTop: 3,
          },
        },
        MuiSelect: {
          select: {
            minWidth: '180px',
          },
        },
      },
    });
  const handleCloseModal = () => {
    props.handleCloseModal(false);
  };
  return (
    <div>
      <MuiThemeProvider theme={getMuiTheme()}>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={props.isModalOpen}
          onClose={handleCloseModal}
          className="fabmodal"
        >
          <div className="modalpaper">
            <Grid container className="header">
              <Grid item xs={10} sm={10}>
                <h2 id="modal-title" className="h2">
                  Application URL
                </h2>
              </Grid>
              <Grid item xs={2} sm={2} className="close">
                <CloseIcon onClick={handleCloseModal} />
              </Grid>
            </Grid>
            <div>
              {ErrMsg === true ? (
                <div style={{ textAlign: 'center' }} className="addfieldserror">
                  Please fill the fields
                </div>
              ) : (
                ''
              )}
              <form noValidate autoComplete="off">
                <Grid className="AddressconfigGrid">
                  <Grid container spacing={3} className="userformcontent ">
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <TextField
                        id="outlined-dense"
                        label="Public Domain "
                        className="partnertextField"
                        margin="dense"
                        variant="outlined"
                        //placeholder=""
                        onChange={handleChange('publicDomains')}
                        value={values.publicDomains}
                        name="publicDomains"
                      />
                      {errorInfo.publicDomains === true ? (
                        <div id="nameid" className="addfieldserror">
                          "Enter only alphabets"
                        </div>
                      ) : (
                        ''
                      )}
                      {submitErr && !values.publicDomains ? (
                        <div id="nameid" className="addfieldserror">
                          Please enter public domains
                        </div>
                      ) : (
                        ''
                      )}
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <TextField
                        id="outlined-dense"
                        label="Asynchronous MDN URL"
                        className="partnertextField"
                        margin="dense"
                        variant="outlined"
                        onChange={handleChange('asynchronousMDNURL')}
                        value={values.asynchronousMDNURL}
                        name="asynchronousMDNURL"
                      />
                      {errorInfo.asynchronousMDNURL === true ? (
                        <div id="nameid" className="addfieldserror">
                          "Enter valid URL"
                        </div>
                      ) : (
                        ''
                      )}
                      {submitErr && !values.asynchronousMDNURL ? (
                        <div id="nameid" className="addfieldserror">
                          Please enter asynchronous MDN URL
                        </div>
                      ) : (
                        ''
                      )}
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <TextField
                        id="outlined-dense"
                        label="Receiving URL"
                        className="partnertextField"
                        margin="dense"
                        variant="outlined"
                        onChange={handleChange('receivingURL')}
                        value={values.receivingURL}
                        name="receivingURL"
                      />
                      {errorInfo.receivingURL === true ? (
                        <div id="nameid" className="addfieldserror">
                          "Enter valid URL"
                        </div>
                      ) : (
                        ''
                      )}
                      {submitErr && !values.receivingURL ? (
                        <div id="nameid" className="addfieldserror">
                          Please enter Receiving URL
                        </div>
                      ) : (
                        ''
                      )}
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <TextField
                        id="outlined-dense"
                        label="Public URL "
                        className="partnertextField"
                        margin="dense"
                        variant="outlined"
                        onChange={handleChange('publicURL')}
                        value={values.publicURL}
                        name="publicURL"
                      />
                      {errorInfo.publicURL === true ? (
                        <div id="nameid" className="addfieldserror">
                          "Enter valid URL"
                        </div>
                      ) : (
                        ''
                      )}
                      {submitErr && !values.publicURL ? (
                        <div id="nameid" className="addfieldserror">
                          Please enter public URL
                        </div>
                      ) : (
                        ''
                      )}
                    </Grid>

                    <Grid item xs={12} sm={12}>
                      <Typography>
                        <Checkbox
                          inputProps={{ 'aria-label': 'uncontrolled-checkbox' }}
                        />
                        Publish my AS2 Profile Settings at public rst
                      </Typography>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <FormControl>
                        <InputLabel
                          style={{ color: '#0b153e', fontSize: 12 }}
                          id="demo-simple-select-label"
                        >
                          Public Certificate
                        </InputLabel>
                        <Select
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          value={values.publicCertificate}
                          onChange={handleChange('publicCertificate')}
                          name="publicCertificate"
                        >
                          <MenuItem value={'testcer'}>testcer</MenuItem>
                        </Select>
                      </FormControl>
                      {submitErr && !values.publicCertificate ? (
                        <div id="nameid" className="addfieldserror">
                          Please enter public certificate
                        </div>
                      ) : (
                        ''
                      )}
                    </Grid>
                  </Grid>
                  <Grid container spacing={3} className="userformcontent ">
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <Button
                        style={{
                          color: '#000006',
                          backgroundColor: '#E5CCFF',
                        }}
                        onClick={handleReset}
                        variant="contained"
                        fullWidth="true"
                        className="createpartnerbutton"
                      >
                        RESET
                      </Button>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <Button
                        onClick={handleSubmit}
                        vvariant="contained"
                        fullWidth="true"
                        className="createpartnerbutton"
                      >
                        SAVE
                      </Button>
                    </Grid>
                  </Grid>
                </Grid>
              </form>
            </div>
          </div>
        </Modal>
      </MuiThemeProvider>
    </div>
  );
}
