import React from 'react';
import { useMutation } from 'graphql-hooks';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import Logout from '@material-ui/icons/ExitToApp';
import Theme from '../css/theme';
import {Typography, Paper } from '@material-ui/core';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import Checkbox from '@material-ui/core/Checkbox';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Collapse from '@material-ui/core/Collapse';




export default function ServerForm(props) {
  const [values, setValues] = React.useState({
    maxRequestPerHour: '',
    maxConcurrentRequest:  '',
    accessControlAllowOrigin:'',
    accessControlAllowCredentials:'',
    accessControlAllowMethods:'',
    accessControlAllowHeaders:'',
    accessControlMaxAge:'',
    serverSidePagingSide:'',
    defaultFormat:'',
    defaultVersion:'',
    dateTimeFormat:'',
    baseURL:'',
  });
  const [nameError, setNameError] = React.useState(false);

  const [errorInfo, setErrorInfo] = React.useState({
    maxRequestPerHour: false,
    maxConcurrentRequest: false,
    accessControlAllowOrigin:false,
    accessControlAllowCredentials:false,
    accessControlAllowMethods:false,
    accessControlAllowHeaders:false,
    accessControlMaxAge:false,
    serverSidePagingSide:false,
    defaultFormat:false,
    defaultVersion:false,
    dateTimeFormat:false,
    baseURL:false,

  });

  const [state, setState] = React.useState({
    checkedLogMsg: true,
    checkedSave: true,
    checkedAllowAuthtokenURL: false,
  });
  const handleOtherSettings = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };

  const [ErrMsg, setErrMsg] = React.useState(false);
  const [pwderror, setpwderror] = React.useState(false);

  const [openDefault, setOpenDefault] = React.useState(false);
  const [openCors, setOpenCors] = React.useState(false);
  const [openOData, setOpenOData] = React.useState(false);
  const [openOthers, setOpenOthers] = React.useState(false);

  const handleDefaultRateDropdown= () => {
    setOpenDefault(!openDefault);
  };

  const handleCorsDropdown= () => {
    setOpenCors(!openCors);
  };
  const handleODataDropdown= () => {
    setOpenOData(!openOData);
  };
  const handleOthersDropdown= () => {
    setOpenOthers(!openOthers);
  };


 const handlereset = () => {
    setErrMsg(false);
    setNameError(false);
    setErrorInfo({
        maxRequestPerHour: false,
        maxConcurrentRequest: false,
        accessControlAllowOrigin:false,
        accessControlAllowCredentials:false,
        accessControlAllowMethods:false,
        accessControlAllowHeaders:false,
        accessControlMaxAge:false,
        serverSidePagingSide:false,
        defaultFormat:false,
        defaultVersion:false,
        dateTimeFormat:false,
        baseURL:false,
    });
    setValues({
      ...values,
      maxRequestPerHour: '',
      maxConcurrentRequest:  '',
      accessControlAllowOrigin:'',
      accessControlAllowCredentials:'',
      accessControlAllowMethods:'',
      accessControlAllowHeaders:'',
      accessControlMaxAge:'',
      serverSidePagingSide:'',
      defaultFormat:'',
      defaultVersion:'',
      dateTimeFormat:'',
      baseURL:'',
    });
  };
  

const handleChange = (name) => (event) => {
    console.log(event.target.value, name);
    setErrMsg(false);
    setValues({ ...values, [name]: event.target.value });
    switch (name) {
      case 'maxRequestPerHour':
      case 'maxConcurrentRequest' :
      case 'serverSidePagingSide' :
      case 'accessControlMaxAge' :
         if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[0-9]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
        case 'accessControlAllowOrigin':
        case 'accessControlAllowMethods':
        case 'accessControlAllowHeaders':
        if (event.target.value.length === 0) {
            setValues({ ...values, [name]: '' });
            setErrorInfo({ ...errorInfo, [name]: false });
          } else if (event.target.value.match('^[a-zA-Z0-9]*$')) {
            setErrorInfo({ ...errorInfo, [name]: false });
            setValues({ ...values, [name]: event.target.value });
          } else {
            setErrorInfo({ ...errorInfo, [name]: true });
          }
          break;
          case 'baseURL':
            if (event.target.value.length === 0) {
              setValues({ ...values, [name]: '' });
              setErrorInfo({ ...errorInfo, [name]: false });
            } else if (
              event.target.value.match('^(http(s)?://)?([a-z][a-zA-Z0-9./]*)?$')
            ) {
              setErrorInfo({ ...errorInfo, [name]: false });
              setValues({ ...values, [name]: event.target.value });
            } else {
              setErrorInfo({ ...errorInfo, [name]: true });
            }
            break;
      default:
        return false;
    }
  };

  const handleSubmit = () => {
    setErrMsg(false);
    setNameError(false);
    if(
        values.maxRequestPerHour !== '' &&
        values.maxConcurrentRequest !==  '' &&
        values.accessControlAllowOrigin !== '' &&
        values.accessControlAllowCredentials !== '' &&
        values.accessControlAllowMethods !== '' &&
        values.accessControlAllowHeaders !=='' &&
        values.accessControlMaxAge !=='' &&
        values.serverSidePagingSide !==''&&
        values.defaultFormat !=='' &&
        values.defaultVersion !==''&&
        values.dateTimeFormat !=='' &&
        values.baseURL !== '' &&

        errorInfo.maxRequestPerHour !== true &&
        errorInfo.maxConcurrentRequest !== true &&
        errorInfo.accessControlAllowOrigin !== true &&
        errorInfo.accessControlAllowCredentials !== true &&
        errorInfo.accessControlAllowMethods !== true &&
        errorInfo.accessControlAllowHeaders !== true &&
        errorInfo.accessControlMaxAge !== true &&
        errorInfo.serverSidePagingSide !== true &&
        errorInfo.defaultFormat !== true &&
        errorInfo.defaultVersion !== true &&
        errorInfo.dateTimeFormat !== true &&
        errorInfo.baseURL !== true 

    ){
        setErrMsg(false);
        setNameError(false);
        setValues({
          ...values,
          maxRequestPerHour: values.maxRequestPerHour,
          maxConcurrentRequest:values.maxConcurrentRequest ,
          accessControlAllowOrigin:values.accessControlAllowOrigin ,
          accessControlAllowCredentials:values.accessControlAllowCredentials ,
          accessControlAllowMethods:values.accessControlAllowMethods,
          accessControlAllowHeaders:values.accessControlAllowHeaders,
          accessControlMaxAge:values.accessControlMaxAge,
          serverSidePagingSide:values.serverSidePagingSide,
          defaultFormat:values.defaultFormat ,
          defaultVersion:values.defaultVersion ,
          dateTimeFormat:values.dateTimeFormat ,
          baseURL:values.baseURL ,
 
        });
        //Submit functionality
    }
    else if(
        values.maxRequestPerHour === '' &&
        values.maxConcurrentRequest ===  '' &&
        values.accessControlAllowOrigin === '' &&
        values.accessControlAllowCredentials === '' &&
        values.accessControlAllowMethods === '' &&
        values.accessControlAllowHeaders ==='' &&
        values.accessControlMaxAge ==='' &&
        values.serverSidePagingSide ===''&&
        values.defaultFormat==='' &&
        values.defaultVersion ===''&&
        values.dateTimeFormat ==='' &&
        values.baseURL === ''
    ){
        setErrMsg(true);
    }
    else {
        setErrMsg(false);
        setNameError(true);
    }
 
  };
 
  return (
    <MuiThemeProvider theme={Theme}>
      <div>
      {ErrMsg === true ? (
                <div style={{ textAlign: 'center' }} className="addfieldserror">
                  Please fill the fields
                </div>
              ) : (
                ''
              )}

        <form noValidate autoComplete="off">
            
           {/*  <Grid item xs={12} sm={12}>
             <h2 className="h2" style={{ color: '#0b153e' }}>
             Default Rate Limits (per User)
            </h2>
              </Grid> */}
            <ListItem button 
             onClick={handleDefaultRateDropdown}
           >
            <ListItemText  className="h2" style={{ color: '#0b153e' }}primary="Default Rate Limits (per User)" />
           {openDefault ? <ExpandLess style={{ display:'flex'}} /> : <ExpandMore style={{ display:'flex'}}/>}
           </ListItem>
            <Collapse in={openDefault} unmountOnExit>  
            <Grid container spacing={3} className="userformcontent"> {/*Container1*/}
                        <Grid
                            item
                            xs={12}
                            sm={6}
                            gutterBottom
                            variant="body2"
                            color="textSecondary"
                            className="text"
                        >
                            <TextField
                            id="outlined-dense"
                            label="Max.Request per Hour"
                            className="partnertextField"
                            margin="dense"
                            variant="outlined"
                            //placeholder=""
                            onChange={handleChange('maxRequestPerHour')}
                            value={values.maxRequestPerHour}
                            name="maxRequestPerHour"
                            />
                            {errorInfo.maxRequestPerHour === true ? (
                            <div id="nameid" className="addfieldserror">
                                Enter only Numbers
                            </div>
                            ) : (
                            ''
                            )}
                            {nameError && !values.maxRequestPerHour ? (
                            <div id="nameid" className="addfieldserror">
                                Please enter Max Request Per Hour
                            </div>
                            ) : (
                            ''
                            )}
                        </Grid>
                        <Grid
                            item
                            xs={12}
                            sm={6}
                            gutterBottom
                            variant="body2"
                            color="textSecondary"
                            className="text"
                        >
                            <TextField
                            id="outlined-dense"
                            label="Max.Concurrent Request"
                            className="partnertextField"
                            margin="dense"
                            variant="outlined"
                            //placeholder=""
                            onChange={handleChange('maxConcurrentRequest')}
                            value={values.maxConcurrentRequest}
                            name="maxConcurrentRequest"
                            />
                            {errorInfo.maxConcurrentRequest === true ? (
                            <div id="nameid" className="addfieldserror">
                                Enter only Numbers
                            </div>
                            ) : (
                            ''
                            )}
                            {nameError && !values.maxConcurrentRequest ? (
                            <div id="nameid" className="addfieldserror">
                                Please enter Max Concurrent Request
                            </div>
                            ) : (
                            ''
                            )}
                        </Grid>
            </Grid> {/*COntainer1 end*/}
            </Collapse>

                                            
            {/* <Grid item xs={12} sm={12}>
             <h2 className="h2" style={{ color: '#0b153e' }}>
            Cross-Origin Resource Sharing (CORS)
            </h2>
            </Grid> */}
           <ListItem button 
             onClick={handleCorsDropdown}
           >
             <ListItemText  className="h2" style={{ color: '#0b153e' }}primary="Cross-Origin Resource Sharing (CORS)" />
           {openCors ? <ExpandLess style={{ display:'flex'}} /> : <ExpandMore style={{ display:'flex'}}/>}
           </ListItem>
            <Collapse in={openCors} unmountOnExit>  
            <Grid container spacing={3} className="userformcontent"> {/*CORS Container 2*/}
                        <Grid item xs={12} sm={6}>
                            <Typography>
                            <Checkbox
                                inputProps={{
                                'aria-label': 'uncontrolled-checkbox',
                                }}
                            />
                             Enable Cross-Origin Resource Sharing(CORS)
                            </Typography>
                        </Grid>
                      <Grid item xs={12} sm={6}>
                            <Typography>
                            <Checkbox
                                inputProps={{
                                'aria-label': 'uncontrolled-checkbox',
                                }}
                            />
                             Allow All domains Without "*"
                            </Typography>
                      </Grid>

                      <Grid
                            item
                            xs={12}
                            sm={3}
                            gutterBottom
                            variant="body2"
                            color="textSecondary"
                            className="text"
                        >
                            <TextField
                            id="outlined-dense"
                            label="Acces-Control-Allow-Origin"
                            className="partnertextField"
                            margin="dense"
                            variant="outlined"
                            //placeholder=""
                            onChange={handleChange('accessControlAllowOrigin')}
                            value={values.accessControlAllowOrigin}
                            name="accessControlAllowOrigin"
                            />
                            {errorInfo.accessControlAllowOrigin === true ? (
                            <div id="nameid" className="addfieldserror">
                                Enter only Alphabets and Numerics
                            </div>
                            ) : (
                            ''
                            )}
                            {nameError && !values.accessControlAllowOrigin ? (
                            <div id="nameid" className="addfieldserror">
                                Please enter Access-Control-Access-origin
                            </div>
                            ) : (
                            ''
                            )}
                        </Grid>

                        <Grid
                        item
                        xs={12}
                        sm={3}
                        gutterBottom
                        variant="body2"
                        color="textSecondary"
                        className="text"
                      >
                        <FormControl>
                          <InputLabel
                            style={{ color: '#0b153e', fontSize: 12 }}
                            id="demo-simple-select-label"
                          >
                            Access-Control-Allow-Credentials
                          </InputLabel>
                          <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={values.accessControlAllowCredentials}
                            onChange={handleChange('accessControlAllowCredentials')}
                            name="accessControlAllowCredentials"
                          >
                            <MenuItem value={'true'}>True</MenuItem>
                            <MenuItem value={'false'}>False</MenuItem>
                          </Select>
                        </FormControl>
                        {nameError && !values.accessControlAllowCredentials ? (
                          <div id="nameid" className="addfieldserror">
                            Please Select Access-Control-Allow-Credentials
                          </div>
                        ) : (
                          ''
                        )}
                      </Grid>

                      <Grid
                            item
                            xs={12}
                            sm={3}
                            gutterBottom
                            variant="body2"
                            color="textSecondary"
                            className="text"
                        >
                            <TextField
                            id="outlined-dense"
                            label="Acces-Control-Allow-Methods"
                            className="partnertextField"
                            margin="dense"
                            variant="outlined"
                            //placeholder=""
                            onChange={handleChange('accessControlAllowMethods')}
                            value={values.accessControlAllowMethods}
                            name="accessControlAllowMethods"
                            />
                            {errorInfo.accessControlAllowMethods === true ? (
                            <div id="nameid" className="addfieldserror">
                                Enter only Alphabets and Numerics
                            </div>
                            ) : (
                            ''
                            )}
                            {nameError && !values.accessControlAllowMethods ? (
                            <div id="nameid" className="addfieldserror">
                                Please enter Access-Control-Access-Methods
                            </div>
                            ) : (
                            ''
                            )}
                        </Grid>

                        
                      <Grid
                            item
                            xs={12}
                            sm={3}
                            gutterBottom
                            variant="body2"
                            color="textSecondary"
                            className="text"
                        >
                            <TextField
                            id="outlined-dense"
                            label="Acces-Control-Allow-Headers"
                            className="partnertextField"
                            margin="dense"
                            variant="outlined"
                            //placeholder=""
                            onChange={handleChange('accessControlAllowHeaders')}
                            value={values.accessControlAllowHeaders}
                            name="accessControlAllowHeaders"
                            />
                            {errorInfo.accessControlAllowHeaders === true ? (
                            <div id="nameid" className="addfieldserror">
                                Enter only Alphabets and Numerics
                            </div>
                            ) : (
                            ''
                            )}
                            {nameError && !values.accessControlAllowHeaders ? (
                            <div id="nameid" className="addfieldserror">
                                Please enter Access-Control-Access-Headers
                            </div>
                            ) : (
                            ''
                            )}
                        </Grid>

                        <Grid
                            item
                            xs={12}
                            sm={3}
                            gutterBottom
                            variant="body2"
                            color="textSecondary"
                            className="text"
                        >
                            <TextField
                            id="outlined-dense"
                            label="Acces-Control-Max-Age"
                            className="partnertextField"
                            margin="dense"
                            variant="outlined"
                            //placeholder=""
                            onChange={handleChange('accessControlMaxAge')}
                            value={values.accessControlMaxAge}
                            name="accessControlMaxAge"
                            />
                            {errorInfo.accessControlMaxAge === true ? (
                            <div id="nameid" className="addfieldserror">
                                Enter only Numbers
                            </div>
                            ) : (
                            ''
                            )}
                            {nameError && !values.accessControlMaxAge ? (
                            <div id="nameid" className="addfieldserror">
                                Please enter Access-Control-Max-Age
                            </div>
                            ) : (
                            ''
                            )}
                        </Grid>

             </Grid>{/*CORS Container 2 end */}
             </Collapse>

            {/* <Grid item xs={12} sm={12}>
             <h2 className="h2" style={{ color: '#0b153e' }}>
            OData
            </h2>
            </Grid> */}

            <ListItem button 
             onClick={handleODataDropdown}
           >
            <ListItemText  className="h2" style={{ color: '#0b153e' }}primary="OData" />
           {openOData ? <ExpandLess style={{ display:'flex'}} /> : <ExpandMore style={{ display:'flex'}}/>}
           </ListItem>
            <Collapse in={openOData} unmountOnExit> 

            <Grid container spacing={3} className="userformcontent"> {/*OData Container 3*/}

                                
                        <Grid
                            item
                            xs={12}
                            sm={3}
                            gutterBottom
                            variant="body2"
                            color="textSecondary"
                            className="text"
                        >
                            <TextField
                            id="outlined-dense"
                            label="Server side Paging side"
                            
                            className="partnertextField"
                            margin="dense"
                            variant="outlined"
                            //placeholder=""
                            onChange={handleChange('serverSidePagingSide')}
                            value={values.serverSidePagingSide}
                            name="serverSidePagingSide"
                            />
                            {errorInfo.serverSidePagingSide === true ? (
                            <div id="nameid" className="addfieldserror">
                                Enter only Numbers
                            </div>
                            ) : (
                            ''
                            )}
                            {nameError && !values.serverSidePagingSide ? (
                            <div id="nameid" className="addfieldserror">
                                Please enter Server side Paging side

                            </div>
                            ) : (
                            ''
                            )}
                        </Grid>

                        
                        <Grid
                        item
                        xs={12}
                        sm={3}
                        gutterBottom
                        variant="body2"
                        color="textSecondary"
                        className="text"
                      >
                        <FormControl>
                          <InputLabel
                            style={{ color: '#0b153e', fontSize: 12 }}
                            id="demo-simple-select-label"
                          >
                            Default Format
                          </InputLabel>
                          <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={values.defaultFormat}
                            onChange={handleChange('defaultFormat')}
                            name="defaultFormat"
                          >
                            <MenuItem value={'json'}>JSON</MenuItem>
                            <MenuItem value={'xml'}>XML(Atom)</MenuItem>
                          </Select>
                        </FormControl>
                        {nameError && !values.defaultFormat ? (
                          <div id="nameid" className="addfieldserror">
                            Please Select Default Format
                          </div>
                        ) : (
                          ''
                        )}
                      </Grid>

                         
                      <Grid
                        item
                        xs={12}
                        sm={3}
                        gutterBottom
                        variant="body2"
                        color="textSecondary"
                        className="text"
                      >
                        <FormControl>
                          <InputLabel
                            style={{ color: '#0b153e', fontSize: 12 }}
                            id="demo-simple-select-label"
                          >
                            Default Version
                          </InputLabel>
                          <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={values.defaultVersion}
                            onChange={handleChange('defaultVersion')}
                            name="defaultVersion"
                          >
                            <MenuItem value={'3.0'}>3.0</MenuItem>
                            <MenuItem value={'4.0'}>4.0</MenuItem>
                          </Select>
                        </FormControl>
                        {nameError && !values.defaultVersion ? (
                          <div id="nameid" className="addfieldserror">
                            Please Select Default Version
                          </div>
                        ) : (
                          ''
                        )}
                      </Grid>

                      <Grid
                        item
                        xs={12}
                        sm={3}
                        gutterBottom
                        variant="body2"
                        color="textSecondary"
                        className="text"
                      >
                        <FormControl>
                          <InputLabel
                            style={{ color: '#0b153e', fontSize: 12 }}
                            id="demo-simple-select-label"
                          >
                            Date Time Format
                          </InputLabel>
                          <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={values.dateTimeFormat}
                            onChange={handleChange('dateTimeFormat')}
                            name="dateTimeFormat"
                          >
                            <MenuItem value={'default'}>Default</MenuItem>
                          </Select>
                        </FormControl>
                        {nameError && !values.dateTimeFormat ? (
                          <div id="nameid" className="addfieldserror">
                            Please Select Date Time Format
                          </div>
                        ) : (
                          ''
                        )}
                      </Grid>

                      <Grid
                            item
                            xs={12}
                            sm={3}
                            gutterBottom
                            variant="body2"
                            color="textSecondary"
                            className="text"
                        >
                            <TextField
                            id="outlined-dense"
                            label="Base URL"
                            className="partnertextField"
                            margin="dense"
                            variant="outlined"
                            //placeholder=""
                            onChange={handleChange('baseURL')}
                            value={values.baseURL}
                            name="baseURL"
                            />
                            {errorInfo.baseURL === true ? (
                            <div id="nameid" className="addfieldserror">
                                Enter valid URL
                            </div>
                            ) : (
                            ''
                            )}
                            {nameError && !values.baseURL ? (
                            <div id="nameid" className="addfieldserror">
                                Please enter Base URL
                            </div>
                            ) : (
                            ''
                            )}
                        </Grid>

            </Grid> {/*OData Container 3 End*/}  
            </Collapse>

            
            <ListItem button 
             onClick={handleOthersDropdown}
           >
            <ListItemText  className="h2" style={{ color: '#0b153e' }}primary="Other Settings" />
           {openOthers ? <ExpandLess style={{ display:'flex'}} /> : <ExpandMore style={{display:'flex'}}/>}
           </ListItem>
            <Collapse in={openOthers} unmountOnExit> 

            <Grid container spacing={3} className="userformcontent"> {/*Other setting Container 4*/}

                         <Grid item xs={12} sm={4}>
                            <Typography>
                            <Checkbox
                                inputProps={{
                                'aria-label': 'uncontrolled-checkbox',
                                }}
                                checked={state.checkedLogMsg}
                                onChange={handleOtherSettings}
                                name="checkedLogMsg"
                                color="primary"
                            />
                             Log Messages
                            </Typography>
                        </Grid>

                        <Grid item xs={12} sm={4}>
                            <Typography>
                            <Checkbox
                                inputProps={{
                                'aria-label': 'uncontrolled-checkbox',
                                }}
                                checked={state.checkedSave}
                                onChange={handleOtherSettings}
                                name="checkedSave"
                                color="primary"
                            />
                             Save to Sent Folder
                            </Typography>
                        </Grid>

                        <Grid item xs={12} sm={4}>
                            <Typography>
                            <Checkbox
                                inputProps={{
                                'aria-label': 'uncontrolled-checkbox',
                                }}
                                checked={state.checkedAllowAuthtokenURL}
                                onChange={handleOtherSettings}
                                name="checkedAllowAuthtokenURL"
                                color="primary"
                            />
                             Allow Authtoken in URL
                            </Typography>
                        </Grid>


            </Grid>{/*Other setting Container 4 end*/}
            </Collapse>
            
            <Grid container spacing={3} className="userformcontent"> {/*Reset & save Container 5*/}
                      <Grid
                        item
                        xs={12}
                        sm={6}
                        gutterBottom
                        variant="body2"
                        color="textSecondary"
                        className="text"
                      >
                        <Button
                          style={{
                            color: '#000006',
                            backgroundColor: '#E5CCFF',
                          }}
                         onClick={handlereset}
                          variant="contained"
                          fullWidth="true"
                          className="createpartnerbutton"
                        >
                          RESET
                        </Button>
                      </Grid>
                      <Grid
                        item
                        xs={12}
                        sm={6}
                        gutterBottom
                        variant="body2"
                        color="textSecondary"
                        className="text"
                      >
                        <Button
                         onClick={handleSubmit}
                          vvariant="contained"
                          fullWidth="true"
                          className="createpartnerbutton"
                        >
                          SAVE
                        </Button>
                      </Grid>
                </Grid> {/*Reset & save Container 5 end*/}

        </form>

      </div>
    </MuiThemeProvider>
  );
}



