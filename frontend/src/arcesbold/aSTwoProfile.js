import MaterialTable from 'material-table';
import { useMutation, useQuery, useManualQuery } from 'graphql-hooks';
import { forwardRef, useEffect } from 'react';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import ReactDOM from 'react-dom';
import MTableToolbar from 'material-table';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import CloseIcon from '@material-ui/icons/Close';
import { withStyles } from '@material-ui/core/styles';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import { Grid, Typography } from '@material-ui/core';
import MuiDialogActions from '@material-ui/core/DialogActions';
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import Timestamp from '../timestamp';
import Error from '../components/emptyPage';
import Loader from '../components/loader';
import Theme from '../css/theme';
import Snackbar from '@material-ui/core/Snackbar';
import MySnackbarContentWrapper from '../components/Snackbar';
import AS2ProfileFormModel from '../components/tmsmodal';
import AS2ProfileForm from '../arcesb/as2ProfileModelForm';
import AS2ViewModel from '../components/tmsmodal';
import AS2View from '../arcesb/tradinginfo/as2ProfileView';
import CircularProgress from '@material-ui/core/CircularProgress';
import SendIcon from '@material-ui/icons/Send';
import ProgressLoader from '../components/AS2Loader';
import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles,
} from '@material-ui/core/styles';
const Post_As2Profile_API = `query postAs2ProfileAPI($AS2ProfileInput:AS2ProfileInput){
  postAs2ProfileAPI(input:$AS2ProfileInput) {
    statusAPI
    msg
  }
}

`;
const DISPLAY_All_AS2profile = `{
  allAS2Profile {
    idas2Profile
    as2ProfileUUID
    connectorID
    connectorDescription
    as2Identifier
    partnerURL
    sendMessageSecurity
    receiveMessageSecurity
    requestMDNReceipt
    security
    delivery
    encryptionCertificate
    streaming
    TLSServerCertificate
    as2Restart
    as2Reliability
    inputFolder
    outputFolder
    processedFolder
    localAs2Identifier
    profilePrivateCertificate
    profilecertificatePassword
    userProfileSettings
    TLSprivateCertificate
    TLScertificatePassword
    HTTPAuthenticationType
    HTTPAuthentication
    user
    password
    headersName
    headersValue
    maxWorkers
    maxFiles
    asyncMDNTimeout
    duplicateFileAction
    duplicateFileInterval
    encryptionAlgorithm
    extensionMap
    hTTPSubject
    logLevel
    logRequests
    messageId
    parentConnector
    parseFDAExtensions
    partnerSigningCertificate
    processingDelay
    signatureAlgorithm
    TLSenabledProtocols
    tempReceiveDirectory
    logMessages
    sendFilter
    savetoSentFolder
    workspaceUUID
    APIStatus
    createdOn
    updatedOn
}}

`;
const REMOVE_AS2Profile = `mutation removeAS2Profile($AS2ProfileInput:AS2ProfileInput){
  removeAS2Profile(input:$AS2ProfileInput) {
    idas2Profile
    as2ProfileUUID
  }
}

`;
const DISPLAY_Workspace = `{
  allWorkspaceByStatus {
    workspaceUUID
    workspaceName
    senderQualifier
    APIStatus
    createdOn
    updatedOn
  }
}`;
function iconStyles() {
  return {};
}
const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(2),
    },
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));
const styles = (theme) => ({
  root: {
    Width: 1200,
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: '#FFFFFF',
  },
});
export default function MaterialTableDemo() {
  const {
    loading: wloading,
    data: wdata,
    errors: werrors,
    networkError: wnetworkError,
  } = useQuery(DISPLAY_Workspace);
  //fetch api
  const [
    PostAPIAS2Profile,
    { loading: wploading, data: wpdata, error: wperrors },
  ] = useManualQuery(Post_As2Profile_API);
  //remove AS2Profile
  const [removeAS2Profile, removeloading, removedata] = useMutation(
    REMOVE_AS2Profile
  );
  //FETCH ALL AS2PROFILE
  const { loading, error, data, refetch } = useQuery(DISPLAY_All_AS2profile);
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [serverError, setServerError] = React.useState(false);
  const [message, setSnackbarMessage] = React.useState();
  const [variant, setSnackbarVariant] = React.useState('error');
  const [as2profileData, setData] = React.useState([]);
  const [openAS2View, setOpenAS2View] = React.useState(false);
  const [openAS2ProfileModal, setOpenAS2ProfileModal] = React.useState(false);
  const [as2profileForm, setAs2profileForm] = React.useState('update');
  const [workspaceData, setWorksapceData] = React.useState([]);
  const [rowData, setrowData] = React.useState();
  const [linearLoaderVal, setLinearLoader] = React.useState(false);
  const [responseMsg, setResponseMessage] = React.useState();
  const [startTime, setStartTime] = React.useState();
  const [msg, setMessage] = React.useState(null);
  const [endTime, setEndTime] = React.useState();
  const [load, setLoad] = React.useState(false);
  const [progressLoaderVal, setProgressLoader] = React.useState(false);

  const handleCloseLoader = () => {
    setLoad(false);
  };
  /* const handleProgressLoader = (val) => {
    console.log(val);
    if (val == 100) {
      setProgressLoader(false);
      setStartTime();
      handleRefetch(false, 'AS2 Connector synced Successfully');
    }
  };*/

  const columns = [
    {
      title: 'Workspace',
      field: 'workspaceUUID',
      lookup: workspaceData,
      editable: 'never',
    },
    { title: 'Connector ID', field: 'connectorID' },

    {
      title: 'Connector Description',
      field: 'connectorDescription',
    },
    { title: 'AS2 Identifier', field: 'as2Identifier' },
    {
      title: 'Partner URL',
      field: 'partnerURL',
    },
    {
      title: 'User',
      field: 'user',
    },
    {
      title: 'Name',
      field: 'headersName',
    },
    {
      title: 'Value',
      field: 'headersValue',
    },
    {
      title: 'Async MDN Timeout',
      field: 'asyncMDNTimeout',
    },
    {
      title: 'Duplicate File Interval',
      field: 'duplicateFileInterval',
    },
    {
      title: 'Extension Map',
      field: 'extensionMap',
    },
    {
      title: 'API Status',

      // field: 'Status'
      render: (rowData) => {
        //console.log(rowData, rowData.APIStatus);
        if (rowData === undefined) {
          return '';
        } else if (
          rowData.APIStatus == null ||
          rowData.APIStatus === 'Inactive'
        ) {
          return <div style={{ color: 'red' }}>Not Sync </div>;
        } else {
          return <div style={{ color: 'green' }}>Sync </div>;
        }
      },
    },
  ];
  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }
  const tableIcons = {
    Add: forwardRef((props, ref) => (
      <AddBox color="primary" {...props} ref={ref} />
    )),
    Check: forwardRef((props, ref) => (
      <Check color="primary" {...props} ref={ref} />
    )),
    Clear: forwardRef((props, ref) => (
      <Clear color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline color="error" {...props} ref={ref} />
    )),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => (
      <Edit color="primary" {...props} ref={ref} />
    )),
    Export: forwardRef((props, ref) => (
      <SaveAlt color="primary" {...props} ref={ref} />
    )),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => (
      <FirstPage color="primary" {...props} ref={ref} />
    )),
    LastPage: forwardRef((props, ref) => (
      <LastPage color="primary" {...props} ref={ref} />
    )),
    NextPage: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft color="primary" {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => (
      <Clear color="primary" {...props} ref={ref} />
    )),
    Search: forwardRef((props, ref) => (
      <Search color="primary" {...props} ref={ref} />
    )),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };

  useEffect(() => {
    if (data !== undefined) {
      console.log(data.allAS2Profile);
      setData(data.allAS2Profile);
      setServerError(false);
    } else {
      console.log('server error');
      setServerError(true);
    }
    if (wdata !== undefined) {
      console.log(wdata.allWorkspaceByStatus);
      var worksapceDD = wdata.allWorkspaceByStatus.reduce(
        (obj, item) => ((obj[item.workspaceUUID] = [item.workspaceName]), obj),
        {}
      );
      console.log('worksapceDD', worksapceDD);
      setWorksapceData(worksapceDD);
    }
  }, [data, wdata]);

  const handleRefetch = (value, message, modelClose) => {
    setStartTime();
    setEndTime();
    refetch({
      updateData(_, data) {
        return data;
      },
    });

    setProgressLoader(false);
    setOpenSnackbar(true);
    setSnackbarMessage(message);
    if (value) {
      setSnackbarVariant('error');
    } else {
      setSnackbarVariant('success');
    }
    if (modelClose) {
      handleCloseForm();
    }
  };
  const handleClickUpdateOpen = (e, rowData) => {
    console.log(e, rowData);
    setAs2profileForm('Update');
    setrowData(rowData);
    setOpenAS2ProfileModal(true);
  };
  const handleClickAddOpen = (e) => {
    setAs2profileForm('Add');
    setOpenAS2ProfileModal(true);
  };
  const handleCloseForm = (e) => {
    setOpenAS2ProfileModal(false);
  };
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiDialogTitle: {
          root: {
            backgroundColor: '#0b153e',
            color: '#FFFFFF',
          },
        },
        MuiTypography: {
          h6: {
            fontSize: 14,
            fontFamily: 'Arial !important',
          },
        },
        MuiSelect: {
          icon: {
            color: '#0b153e',
          },
        },
      },
    });
  async function handleRemoveAS2profile(oldData) {
    return await new Promise(async (resolve) => {
      resolve();
      await removeAS2Profile({
        variables: {
          AS2ProfileInput: {
            as2ProfileUUID: oldData.as2ProfileUUID,
          },
        },
      })
        .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
          console.log(data, error, graphQLErrors, networkError, cacheHit);
          if (data !== null && error == false) {
            handleRefetch(error, 'Deleted successfully');
          } else if (error && graphQLErrors.length > 0) {
            let duperror = graphQLErrors[0].message;
            console.log(duperror);
            if (
              duperror !== null &&
              duperror.indexOf('ER_ROW_IS_REFERENCED') !== -1
            ) {
              setOpenSnackbar(true);
              setSnackbarVariant('error');
              setSnackbarMessage('Selected row is referenced in other table');
            }
          }
        })
        .catch((e) => {
          // you can do something with the error here
          console.log(e);
        });
    });
  }
  const handleCloseView = (e) => {
    setOpenAS2View(false);
  };
  const handleOpenView = (e, rowData) => {
    setrowData(rowData);
    setOpenAS2View(true);
  };

  const handleAPISuccess = async (Status, aPIMsg) => {
    if (Status === 'failure' || Status === null) {
      handleRefetch(true, aPIMsg);
    } else {
      handleRefetch(false, aPIMsg);
    }
  };
  const handleCallAPI = async (event, rowData) => {
    console.log('rowData', rowData);
    setProgressLoader(true);
    setStartTime(1);

    await PostAPIAS2Profile({
      variables: {
        AS2ProfileInput: {
          as2ProfileUUID: rowData.as2ProfileUUID,
          workspaceUUID: rowData.workspaceUUID,
          connectorID: rowData.connectorID,
          as2Identifier: rowData.as2Identifier,
          partnerURL: rowData.partnerURL,
          delivery: rowData.delivery,
          encryptionAlgorithm: rowData.encryptionAlgorithm,
          signatureAlgorithm: rowData.signatureAlgorithm,
          asyncMDNTimeout: rowData.asyncMDNTimeout,
          HTTPAuthenticationType: rowData.HTTPAuthenticationType,
          streaming: rowData.streaming,
          as2Restart: rowData.as2Restart,
          as2Reliability: rowData.as2Reliability,
          logRequests: rowData.logRequests,
          parseFDAExtensions: rowData.parseFDAExtensions,
          TLSenabledProtocols: rowData.TLSenabledProtocols,
          logMessages: rowData.logMessages,
          savetoSentFolder: rowData.savetoSentFolder,
        },
      },
    }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
      console.log(data, error, graphQLErrors, networkError, cacheHit);

      if (data !== null && error == false) {
        setEndTime(100);
        setTimeout(() => {
          handleAPISuccess(
            data.postAs2ProfileAPI.statusAPI,
            data.postAs2ProfileAPI.msg
          );
        }, 5000);
      } else if (error && graphQLErrors.length > 0) {
        let duperror = graphQLErrors[0].message;
        console.log(duperror);
        if (duperror !== null) {
          setEndTime(90);
          setTimeout(() => {
            handleRefetch(error, 'AS2profile did not sync. Please try again!');
          }, 5000);
        }
      } else {
        setEndTime(90);
        setTimeout(() => {
          handleRefetch(error, 'graphql hooks error');
        }, 5000);
      }
    });
  };
  const classes1 = useStyles();
  if (loading && progressLoaderVal == false) return <Loader />;
  if (serverError)
    return <Error type={'Server connection lost.Please try again'} />;
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <MaterialTable
        icons={tableIcons}
        title="AS2 Profile"
        columns={columns}
        data={as2profileData}
        editable={{
          onRowDelete: (oldData) => handleRemoveAS2profile(oldData),
        }}
        actions={[
          {
            icon: AddIcon,
            tooltip: 'Add',
            className: 'addIconColor',
            isFreeAction: true,
            onClick: (event) => handleClickAddOpen(event),
          },
          {
            icon: Edit,
            tooltip: 'Edit',
            className: 'addIconColor',
            onClick: (event, rowData) => handleClickUpdateOpen(event, rowData),
          },
        ]}
        options={{
          headerStyle: {
            textAlign: 'center',
            fontSize: 12,
            fontWeight: 'bold',
            fontFamily: 'Arial !important',
            backgroundColor: '#0b153e',
            color: '#ffffff',
            padding: '4px',
          },
          searchFieldStyle: {
            color: '#0b153e',
          },
          actionsColumnIndex: -1,
          pageSize: 10,
          pageSizeOptions: [10, 25, 50, 100],
          toolbar: true,
          paging: true,
        }}
        actions={[
          (rowData) => {
            return rowData.APIStatus == null || rowData.APIStatus == 'Inactive'
              ? {
                  icon: SendIcon,
                  tooltip: 'Sync As2Profile',
                  disabled: false,
                  onClick: (event, rowData) => handleCallAPI(event, rowData),
                }
              : {
                  icon: SendIcon,
                  tooltip: 'Sync As2Profile',
                  disabled: true,
                };
          },
          {
            icon: AddIcon,
            tooltip: 'Add',
            className: 'addIconColor',
            isFreeAction: true,
            onClick: (event) => handleClickAddOpen(event),
          },
          (rowData) => {
            //  console.log(rowData, rowData.APIStatus);
            return rowData.APIStatus == null || rowData.APIStatus == 'Inactive'
              ? {
                  icon: Edit,
                  tooltip: 'Edit',
                  disabled: false,
                  className: 'addIconColor',
                  onClick: (event, rowData) =>
                    handleClickUpdateOpen(event, rowData),
                }
              : {
                  icon: Edit,
                  tooltip: 'Edit',
                  disabled: true,
                  //hidden: true
                };
          },
        ]}
        onRowClick={(event, rowData) => handleOpenView(event, rowData)}
      />
      <AS2ProfileFormModel
        handleCloseModal={handleCloseForm}
        isModalOpen={openAS2ProfileModal}
      >
        <AS2ProfileForm
          handleCloseModal={handleCloseForm}
          onSuccess={handleRefetch}
          isModalOpen={openAS2ProfileModal}
          rowData={rowData}
          title={as2profileForm}
        />
      </AS2ProfileFormModel>
      <AS2ViewModel
        handleCloseModal={handleCloseView}
        isModalOpen={openAS2View}
      >
        <AS2View
          handleCloseModal={handleCloseView}
          //onSuccess={handleRefetch}
          isModalOpen={openAS2View}
          rowData={rowData}
          title={as2profileForm}
        />
      </AS2ViewModel>

      <ProgressLoader
        isModalOpen={progressLoaderVal}
        maxTimeLoader={handleCloseLoader}
        value={startTime}
        time={endTime}
        msg={msg}
        labelText={responseMsg}
        // successProgressLoader={handleProgressLoader}
      />

      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        autoHideDuration={1500}
        open={openSnackbar}
        onClose={handleCloseSnackbar}
      >
        <MySnackbarContentWrapper
          onClose={handleCloseSnackbar}
          variant={variant}
          message={message}
        />
      </Snackbar>
    </MuiThemeProvider>
  );
}
