import MaterialTable from 'material-table';
import { forwardRef } from 'react';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import ReactDOM from 'react-dom';
import MTableToolbar  from 'material-table';
import React, { Component } from "react";
import Timestamp from '../timestamp';
import AddIcon from '@material-ui/icons/Add';
import Button from "@material-ui/core/Button";
import Error from '../components/emptyPage';
import Loader from '../components/loader';
import UserForm from './userFromModel';
import UserModel from '../components/tmsmodal';
import Theme from '../css/theme';
import CachedIcon from '@material-ui/icons/Cached';
import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles
} from '@material-ui/core/styles';

function iconStyles() {
  return {
 
  };
}


export default function UserTable() {
    const [userData, setData] = React.useState([]);
    const [openUserModal, setOpenUserModal] = React.useState(false);
    const [userTitle, setUserTitle] = React.useState('update');
    const [rowData, setrowData] = React.useState();
   const columns= [
      { title: 'User', 
      field: 'user' 
      },
      { title: 'Authtoken', 
       field: 'authtoken' 
      },
      { title: 'Max.Requests(hr)', 
      field: 'max.request(hr)' 
      },
   
      { title: 'Max.Concurrent', 
      field: 'max.concurrent' 
       },
       { title: 'Privileges', 
       field: 'privileges' 
         },
      ];
  const tableIcons = {
    Add: forwardRef((props, ref) => (
      <AddIcon color="primary" {...props} ref={ref} />
    )),

    Check: forwardRef((props, ref) => (
      <Check color="primary" {...props} ref={ref} />
    )),
    Clear: forwardRef((props, ref) => (
      <Clear color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline color="error" {...props} ref={ref} />
    )),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => (
      <Edit color="primary" {...props} ref={ref} />
    )),
    Export: forwardRef((props, ref) => (
      <SaveAlt color="primary" {...props} ref={ref} />
    )),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => (
      <FirstPage color="primary" {...props} ref={ref} />
    )),
    LastPage: forwardRef((props, ref) => (
      <LastPage color="primary" {...props} ref={ref} />
    )),
    NextPage: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft color="primary" {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => (
      <Clear color="primary" {...props} ref={ref} />
    )),
    Search: forwardRef((props, ref) => (
      <Search color="primary" {...props} ref={ref} />
    )),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSelect: {
          icon:{
            color:'#0b153e'
              }
        },
      
        MuiTypography:{h6:{
          fontSize:14,    
          fontFamily:"Arial !important"  
          }},
      }
      
    });
    const handleClickUpdateOpen = (e, rowData) => {
        console.log(e, rowData);
        setUserTitle('Update');
        setrowData(rowData);
        setOpenUserModal(true);
      };
      const handleClickAddOpen = (e) => {
        setUserTitle('Add');
        setOpenUserModal(true);
      };
      const handleCloseForm = (e) => {
        setOpenUserModal(false);
      };
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
    <MaterialTable
     
     icons={tableIcons}
         title="Users"
      columns={columns}
      data={userData}
      editable={{
        onRowDelete: oldData =>console.log('need to delete functionality')
         
      }} options={{
        headerStyle: {
        
        textAlign: 'center',
        fontSize:12,
        fontWeight:'bold',
        fontFamily:'Arial !important',
        backgroundColor:"#0b153e",
        color:"#ffffff",
        padding:'4px',
       
        },
          searchFieldStyle: {
            color: '#0b153e'
      
        },
        actionsColumnIndex: -1,
        pageSize:10,
        pageSizeOptions: [10, 25 ,50,100 ],
        toolbar: true,
        paging: true
       
        }}
        actions={[
          
            {
              icon: AddIcon,
              tooltip: 'Add',
              className: 'addIconColor',
              isFreeAction: true,
              onClick: (event) => handleClickAddOpen(event),
            },
            {
              icon: Edit,
              tooltip: 'Edit',
              className: 'addIconColor',
              onClick: (event, rowData) => handleClickUpdateOpen(event, rowData),
            },
            {
       
              icon: () =><CachedIcon style={{color:"#0b153e"}}/>,
              isFreeAction:true,
            },]}
      />
    <UserModel
        handleCloseModal={handleCloseForm}
        isModalOpen={openUserModal}
      >
        <UserForm
          handleCloseModal={handleCloseForm}
         // onSuccess={handleRefetch}
          isModalOpen={openUserModal}
          rowData={rowData}
          title={userTitle}
        />
      </UserModel>
        </MuiThemeProvider>    
  );
    }
