import React, { useEffect } from 'react';
import ConnectorTable from './connectorsTable';
import Grid from '@material-ui/core/Grid';
import Theme from '../css/theme';
import { makeStyles } from '@material-ui/core/styles';
import SendIcon from '@material-ui/icons/Send';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import TransactionFlow from './transactionFlow';
const useStyles = makeStyles((theme) => ({}));
const getMuiTheme = () => createMuiTheme(Theme, {});
export default function SimpleTabs(props) {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const [reloadValue, setReloadValue] = React.useState(false);
  console.log(props);
  useEffect(() => {
    console.log(reloadValue);
  }, [reloadValue]);
  const handleReload = (value) => {
    if (value) {
      setReloadValue(true);
    }
  };
  console.log(reloadValue);
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <div>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6} className="userformcontent">
            <ConnectorTable rowData={props.rowData} onSuccess={handleReload} />
            <br />
            <SendIcon style={{ marginLeft: '95%' }} />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TransactionFlow
              rowData={props.rowData}
              reloadValue={reloadValue}
            />
          </Grid>
        </Grid>
      </div>
    </MuiThemeProvider>
  );
}
