import React, { useEffect, useContext, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import { useMutation, useQuery } from 'graphql-hooks';
import Snackbar from '@material-ui/core/Snackbar';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Switch from '@material-ui/core/Switch';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
import Modal from '@material-ui/core/Modal';
import Theme from '../css/theme';
import Loader from '../components/loader';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { MenuList } from '@material-ui/core';
const ADD_Certificate = `
mutation createCertificate($CertificateInput:CertificateInput){
  createCertificate(input:$CertificateInput) {
    idCertificate
    certificateUUID
    commonName
    organization
    fileName
    serialNumber
    cPassword
    validPeriod
    keySize
    publicKeyType
    signatureAlgorithm
    createdOn
    lastUpdatedOn
  } 
}
`;
export default function CertificateModel(props) {
  //ADD CERTIFICATE
  const [addCertificate, addloading, adddata] = useMutation(ADD_Certificate);
  const [values, setValues] = React.useState({
    commonName: '',
    organization: '',
    fileName: '',
    serialNumber: '',
    password: '',
    validityPeriod: '',
    keySize: '',
    publicKeyType: '',
    signatureAlgorithm: '',
  });
  const [submitErr, setSubmitErr] = React.useState(false);
  const [ErrMsg, setErrMsg] = React.useState(false);
  const [errorInfo, setErrorInfo] = React.useState({
    commonName: false,
    organization: false,
    fileName: false,
    serialNumber: false,
    password: false,
    validityPeriod: false,
    keySize: false,
    publicKeyType: false,
    signatureAlgorithm: false,
  });
  const handleChange = (name) => (event) => {
    console.log(event.target.value, name);
    setErrMsg(false);
    setValues({ ...values, [name]: event.target.value });
    switch (name) {
      case 'commonName':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[a-zA-Z ]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'organization':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[a-zA-Z ]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'fileName':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match(/^[a-zA-Z]+(\.pfx)+$/)) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'serialNumber':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^[0-9]*$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'password':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match(
            '^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{4,8}$'
          )
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
        } else setErrorInfo({ ...errorInfo, [name]: true });
        break;
      case 'validityPeriod':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (event.target.value.match('^([0-9]{1,2})$')) {
          setErrorInfo({ ...errorInfo, [name]: false });
        } else setErrorInfo({ ...errorInfo, [name]: true });
        break;

      case 'keySize':
        setValues({
          ...values,
          [event.target.name]: event.target.value,
        });
        break;

      case 'publicKeyType':
        setValues({
          ...values,
          [event.target.name]: event.target.value,
        });
        break;
      case 'signatureAlgorithm':
        setValues({
          ...values,
          [event.target.name]: event.target.value,
        });
        break;

      default:
        return false;
    }
  };
  const handleSubmit = async () => {
    if (
      values.commonName !== '' &&
      values.organization !== '' &&
      values.fileName !== '' &&
      values.password !== '' &&
      values.serialNumber !== '' &&
      values.validityPeriod !== '' &&
      values.keySize !== '' &&
      values.publicKeyType !== '' &&
      values.signatureAlgorithm !== '' &&
      errorInfo.commonName !== true &&
      errorInfo.organization !== true &&
      errorInfo.fileName !== true &&
      errorInfo.password !== true &&
      errorInfo.serialNumber !== true &&
      errorInfo.validityPeriod !== true
    ) {
      setErrMsg(false);
      setValues({
        ...values,
        commonName: values.commonName,
        organization: values.organization,
        fileName: values.fileName,
        serialNumber: values.serialNumber,
        password: values.password,
        validityPeriod: values.validityPeriod,
        keySize: values.keySize,
        publicKeyType: values.publicKeyType,
        signatureAlgorithm: values.signatureAlgorithm,
      });
      //submit functionalitye here
      await addCertificate({
        variables: {
          CertificateInput: {
            commonName: values.commonName,
            organization: values.organization,
            fileName: values.fileName,
            serialNumber: values.serialNumber,
            cPassword: values.password,
            validPeriod: values.validityPeriod,
            keySize: values.keySize,
            publicKeyType: values.publicKeyType,
            signatureAlgorithm: values.signatureAlgorithm,
          },
        },
      })
        .then(
          ({ data, error, graphQLErrors, networkError, cacheHit, loading }) => {
            console.log(
              data,
              error,
              graphQLErrors,
              networkError,
              cacheHit,
              loading
            );
            if (data !== null && error == false) {
              console.log(' created successfully');
              setErrMsg(false);

              props.onSuccess(error, 'Saved successfully', true);
            } else {
              props.onSuccess(error, 'Graphql hooks error', false);
            }
          }
        )
        .catch((e) => {
          // you can do something with the error here
          console.log(e);
        });
    } else if (
      values.commonName == '' &&
      values.organization == '' &&
      values.fileName == '' &&
      values.password == '' &&
      values.serialNumber == '' &&
      values.validityPeriod == '' &&
      values.keySize == '' &&
      values.publicKeyType == '' &&
      values.signatureAlgorithm == ''
    ) {
      setErrMsg(true);
    } else {
      setErrMsg(false);
      setSubmitErr(true);
    }
  };
  const handleReset = async () => {
    setErrMsg(false);
    setErrorInfo({
      commonName: false,
      organization: false,
      fileName: false,
      serialNumber: false,
      password: false,
      validityPeriod: false,
      keySize: false,
      publicKeyType: false,
      signatureAlgorithm: false,
    });
    setValues({
      ...values,
      commonName: '',
      organization: '',
      fileName: '',
      serialNumber: '',
      password: '',
      validityPeriod: '',
      keySize: '',
      publicKeyType: '',
      signatureAlgorithm: '',
    });
    setSubmitErr(false);
  };
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSvgIcon: {
          root: {
            color: '#FFFFFF',
          },
        },

        MuiInputLabel: {
          animated: {
            fontSize: 13,
          },
          outlined: {
            paddingTop: 3,
          },
        },
        MuiSelect: {
          select: {
            minWidth: '180px',
          },
        },
      },
    });
  const handleCloseModal = () => {
    props.handleCloseModal(false);
  };
  return (
    <div>
      <MuiThemeProvider theme={getMuiTheme()}>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={props.isModalOpen}
          onClose={handleCloseModal}
          className="fabmodal"
        >
          <div className="modalpaper">
            <Grid container className="header">
              <Grid item xs={10} sm={10}>
                <h2 id="modal-title" className="h2">
                  Certificate
                </h2>
              </Grid>
              <Grid item xs={2} sm={2} className="close">
                <CloseIcon onClick={handleCloseModal} />
              </Grid>
            </Grid>
            <div>
              {ErrMsg === true ? (
                <div style={{ textAlign: 'center' }} className="addfieldserror">
                  Please fill the fields
                </div>
              ) : (
                ''
              )}
              <form noValidate autoComplete="off">
                <Grid className="AddressconfigGrid">
                  <Grid container spacing={3} className="userformcontent ">
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <TextField
                        id="outlined-dense"
                        label="Common Name "
                        className="partnertextField"
                        margin="dense"
                        variant="outlined"
                        //placeholder=""
                        onChange={handleChange('commonName')}
                        value={values.commonName}
                        name="commonName"
                      />
                      {errorInfo.commonName === true ? (
                        <div id="nameid" className="addfieldserror">
                          "Enter only alphabets"
                        </div>
                      ) : (
                        ''
                      )}
                      {submitErr && !values.commonName ? (
                        <div id="nameid" className="addfieldserror">
                          Please enter common name
                        </div>
                      ) : (
                        ''
                      )}
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <TextField
                        id="outlined-dense"
                        label="Organization"
                        className="partnertextField"
                        margin="dense"
                        variant="outlined"
                        onChange={handleChange('organization')}
                        value={values.organization}
                        name="organization"
                      />
                      {errorInfo.organization === true ? (
                        <div id="nameid" className="addfieldserror">
                          "Enter only alphabets"
                        </div>
                      ) : (
                        ''
                      )}
                      {submitErr && !values.organization ? (
                        <div id="nameid" className="addfieldserror">
                          Please enter organization
                        </div>
                      ) : (
                        ''
                      )}
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <TextField
                        id="outlined-dense"
                        label="File Name"
                        className="partnertextField"
                        margin="dense"
                        variant="outlined"
                        onChange={handleChange('fileName')}
                        value={values.fileName}
                        name="fileName"
                      />
                      {errorInfo.fileName === true ? (
                        <div id="nameid" className="addfieldserror">
                          "Enter only alphabets and file name must be end with
                          .pfx"
                        </div>
                      ) : (
                        ''
                      )}
                      {submitErr && !values.fileName ? (
                        <div id="nameid" className="addfieldserror">
                          Please enter fileName
                        </div>
                      ) : (
                        ''
                      )}
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <TextField
                        id="outlined-dense"
                        label="Serial Number "
                        className="partnertextField"
                        margin="dense"
                        variant="outlined"
                        onChange={handleChange('serialNumber')}
                        value={values.serialNumber}
                        name="serialNumber"
                      />
                      {errorInfo.serialNumber === true ? (
                        <div id="nameid" className="addfieldserror">
                          "Enter only numeric characters"
                        </div>
                      ) : (
                        ''
                      )}
                      {submitErr && !values.serialNumber ? (
                        <div id="nameid" className="addfieldserror">
                          Please enter serial number
                        </div>
                      ) : (
                        ''
                      )}
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <TextField
                        id="outlined-dense"
                        label="Password "
                        type="password"
                        className="partnertextField"
                        margin="dense"
                        variant="outlined"
                        onChange={handleChange('password')}
                        value={values.password}
                        name="password"
                      />
                      {errorInfo.password === true ? (
                        <div id="nameid" className="addfieldserror">
                          "4 to 8 characters must have atleast
                          one[uppercase,lowercase,special character and number]"
                        </div>
                      ) : (
                        ''
                      )}
                      {submitErr && !values.password ? (
                        <div id="nameid" className="addfieldserror">
                          Please enter password
                        </div>
                      ) : (
                        ''
                      )}
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <TextField
                        id="outlined-dense"
                        label="Validity Period (Years) "
                        className="partnertextField"
                        margin="dense"
                        variant="outlined"
                        onChange={handleChange('validityPeriod')}
                        value={values.validityPeriod}
                        name="validityPeriod"
                      />
                      {errorInfo.validityPeriod === true ? (
                        <div id="nameid" className="addfieldserror">
                          "Allowed only one (or) two numeric digits"
                        </div>
                      ) : (
                        ''
                      )}
                      {submitErr && !values.validityPeriod ? (
                        <div id="nameid" className="addfieldserror">
                          Please enter validity period
                        </div>
                      ) : (
                        ''
                      )}
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <FormControl>
                        <InputLabel
                          style={{ color: '#0b153e', fontSize: 12 }}
                          id="demo-simple-select-label"
                        >
                          Key Size
                        </InputLabel>
                        <Select
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          value={values.keySize}
                          onChange={handleChange('keySize')}
                          name="keySize"
                        >
                          <MenuItem value={'2048'}>2048</MenuItem>
                        </Select>
                      </FormControl>
                      {submitErr && !values.keySize ? (
                        <div id="nameid" className="addfieldserror">
                          Please enter key size
                        </div>
                      ) : (
                        ''
                      )}
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <FormControl>
                        <InputLabel
                          style={{ color: '#0b153e', fontSize: 12 }}
                          id="demo-simple-select-label"
                        >
                          Public Key Type
                        </InputLabel>
                        <Select
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          value={values.publicKeyType}
                          onChange={handleChange('publicKeyType')}
                          name="publicKeyType"
                        >
                          <MenuItem value={'X 509 Public Key'}>
                            X 509 Public Key
                          </MenuItem>
                        </Select>
                      </FormControl>
                      {submitErr && !values.publicKeyType ? (
                        <div id="nameid" className="addfieldserror">
                          Please enter public key type
                        </div>
                      ) : (
                        ''
                      )}
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <FormControl>
                        <InputLabel
                          style={{ color: '#0b153e', fontSize: 12 }}
                          id="demo-simple-select-label"
                        >
                          Signature Algorithm
                        </InputLabel>
                        <Select
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          value={values.signatureAlgorithm}
                          onChange={handleChange('signatureAlgorithm')}
                          name="signatureAlgorithm"
                        >
                          <MenuItem value={'SHA526'}>SHA526</MenuItem>
                        </Select>
                      </FormControl>
                      {submitErr && !values.signatureAlgorithm ? (
                        <div id="nameid" className="addfieldserror">
                          Please select signature algorithm
                        </div>
                      ) : (
                        ''
                      )}
                    </Grid>
                  </Grid>
                  <Grid container spacing={3} className="userformcontent ">
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <Button
                        style={{
                          color: '#000006',
                          backgroundColor: '#E5CCFF',
                        }}
                        onClick={handleReset}
                        variant="contained"
                        fullWidth="true"
                        className="createpartnerbutton"
                      >
                        RESET
                      </Button>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <Button
                        onClick={handleSubmit}
                        vvariant="contained"
                        fullWidth="true"
                        className="createpartnerbutton"
                      >
                        <LockOutlinedIcon style={{ paddingRight: '10px' }} />{' '}
                        Create certificate
                      </Button>
                    </Grid>
                  </Grid>
                </Grid>
              </form>
            </div>
          </div>
        </Modal>
      </MuiThemeProvider>
    </div>
  );
}
