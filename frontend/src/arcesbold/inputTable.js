import MaterialTable from 'material-table';
import { useMutation, useQuery, useManualQuery } from 'graphql-hooks';
import { forwardRef, useEffect } from 'react';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import ReactDOM from 'react-dom';
import MTableToolbar from 'material-table';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import MenuItem from '@material-ui/core/MenuItem';
import CloseIcon from '@material-ui/icons/Close';
import { withStyles } from '@material-ui/core/styles';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import { Grid, Typography } from '@material-ui/core';
import MuiDialogActions from '@material-ui/core/DialogActions';
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import Timestamp from '../timestamp';
import Theme from '../css/theme';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import Error from '../components/emptyPage';
import DeleteIcon from '@material-ui/icons/Delete';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import CachedIcon from '@material-ui/icons/Cached';
import SendIcon from '@material-ui/icons/Send';
import Snackbar from '@material-ui/core/Snackbar';
import MySnackbarContentWrapper from '../components/Snackbar';
import Loader from '../components/loader';
import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles,
} from '@material-ui/core/styles';
import { red } from '@material-ui/core/colors';

const Fetch_UploadCertificate_API = `query CertificateUploadAPI($CertificateUploadInput:CertificateUploadInput){
  CertificateUploadAPI (input:$CertificateUploadInput){
    Name
  }
}
`;
const POST_UploadCertificate_API = `query postCertificatedataAPI($CertificateUploadInput:CertificateUploadInput){
  postCertificatedataAPI (input:$CertificateUploadInput){
    Name
  }
}
`;
const All_UploadCertificate = `{
  allCertificateUpload {
    idCertificatesData
    certificatesDataUUID
    Name
    Data
    StoreType
    Subject
    Issuer
    IssuedTo
    IssuedBy
    EffectiveDate
    ExpirationDate
    ExpirationDays
    Serialnumber
    Thumbprint
    Keysize
    SignatureAlgorithm
    ConnectorId
    createdOn
    updatedOn
  }
}
`;
const ADD_UploadCertificate = `
mutation createCertificateUpload($CertificateUploadInput:CertificateUploadInput){
  createCertificateUpload(input:$CertificateUploadInput) {
    Name
  } 
}
`;
const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  input: {
    display: 'none',
  },
}));

export default function InputTable(props) {
  //FETCH connector
  const { loading, error, data, refetch } = useQuery(All_UploadCertificate);
  /*const [
    fetchAPIUploadCertificate,
    { loading: uloading, data: udata, error: uerrors },
  ] = useManualQuery(Fetch_UploadCertificate_API);*/
  const [
    POSTAPIUploadCertificate,
    { loading: uloading, data: udata, error: uerrors },
  ] = useManualQuery(POST_UploadCertificate_API);
  //ADD upload certificate
  const [addUploadCertificate, addloading, adddata] = useMutation(
    ADD_UploadCertificate
  );
  const [certificateUploadData, setData] = React.useState([]);
  const columns = [
    {
      title: 'idCertificatesData',
      field: 'idCertificatesData',
      hidden: true,
    },
    {
      title: 'certificatesDataUUID',
      field: 'certificatesDataUUID',
      hidden: true,
    },
    {
      title: 'File Name',
      field: 'Name',
    },
    {
      title: 'Upload File',
      field: 'uploadFile',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <input
              // accept="image/*"
              style={{
                display: 'none',
              }}
              id="contained-button-file"
              //multiple
              type="file"
              onChange={(v) => props.onChange(v.target.files)}
            />
            <label htmlFor="contained-button-file">
              <CloudUploadIcon style={{ color: '#0b153e' }} />
            </label>
          </div>
        );
      },
    },
    {
      title: 'Store Type',
      field: 'StoreType',
    },
    {
      title: 'Subject',
      field: 'Subject',
    },
    {
      title: 'Issuer',
      field: 'Issuer',
    },
    {
      title: 'Issued To',
      field: 'IssuedTo',
    },
    {
      title: 'Issued By',
      field: 'IssuedBy',
    },
    {
      title: 'Effective Date',
      field: 'EffectiveDate',
    },
    {
      title: 'Expiration Date',
      field: 'ExpirationDate',
    },
    {
      title: 'Expiration Days',
      field: 'ExpirationDays',
    },
    {
      title: 'Serial Number',
      field: 'Serialnumber',
    },
    {
      title: 'Thumbprint',
      field: 'Thumbprint',
    },
    {
      title: 'Key size',
      field: 'Keysize',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              //placeholder="Description"
              margin="normal"
              select
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            >
              <MenuItem value={'2048'}>2048</MenuItem>
            </TextField>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.Keysize}</div>;
      },
    },
    {
      title: 'Signature Algorithm',
      field: 'SignatureAlgorithm',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              //placeholder="Description"
              margin="normal"
              select
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            >
              <MenuItem value={'SHA526'}>SHA526</MenuItem>
            </TextField>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.SignatureAlgorithm}</div>;
      },
    },
    {
      title: 'Connector Id',
      field: 'ConnectorId',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              //placeholder="Description"
              margin="normal"
              select
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            >
              <MenuItem value={'connectorId 1'}> connectorId 1</MenuItem>
              <MenuItem value={'7447f710-bb90-11ea-8a6a-74e6e21b20e0'}>
                connector1
              </MenuItem>
            </TextField>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.ConnectorId}</div>;
      },
    },

    {
      title: 'Created On',
      field: 'createdOn',
      editable: 'never',
      render: (rowData) => {
        console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.createdOn);
        } else {
          return;
        }
      },
    },
    {
      title: 'Last Updated On',
      field: 'updatedOn',
      editable: 'never',
      render: (rowData) => {
        console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.updatedOn);
        } else {
          return;
        }
      },
    },
  ];

  const [open, setOpen] = React.useState(false);
  const [serverError, setServerError] = React.useState(false);
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [message, setSnackbarMessage] = React.useState();
  const [variant, setSnackbarVariant] = React.useState('error');
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const tableIcons = {
    Add: forwardRef((props, ref) => (
      <AddBox color="primary" {...props} ref={ref} />
    )),

    Check: forwardRef((props, ref) => (
      <Check color="white" {...props} ref={ref} />
    )),
    Clear: forwardRef((props, ref) => (
      <Clear color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline color="error" {...props} ref={ref} />
    )),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => (
      <Edit color="primary" {...props} ref={ref} />
    )),
    Export: forwardRef((props, ref) => (
      <SaveAlt color="primary" {...props} ref={ref} />
    )),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => (
      <FirstPage color="primary" {...props} ref={ref} />
    )),
    LastPage: forwardRef((props, ref) => (
      <LastPage color="primary" {...props} ref={ref} />
    )),
    NextPage: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft color="primary" {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => (
      <Clear color="primary" {...props} ref={ref} />
    )),
    Search: forwardRef((props, ref) => (
      <Search color="primary" {...props} ref={ref} />
    )),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };

  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSelect: {
          icon: {
            color: '#0b153e',
          },
        },

        MuiCheckbox: {
          colorSecondary: {
            color: 'grey',
          },
        },
        MuiIconButton: {
          root: {
            color: 'red !important',
          },
        },
        MuiCheckbox: {
          root: {
            color: 'grey',
          },
        },
        MuiIconButton: {
          colorInherit: {
            color: 'red !important',
          },
        },
        MuiIconButton: {
          colorSecondary: {
            color: 'inherit !important',
          },
        },
        MuiTypography: {
          h6: {
            fontSize: 14,
            fontFamily: 'Arial !important',
          },
        },
        MuiDialogTitle: {
          root: {
            backgroundColor: '#0b153e',
            color: '#FFFFFF',
          },
        },
        MuiGrid: {
          container: {
            flexWrap: 'nowrap',
          },
        },
      },
    });
  useEffect(() => {
    if (data !== undefined) {
      console.log(data.allCertificateUpload);
      setData(data.allCertificateUpload);
      setServerError(false);
    } else {
      console.log('server error');
      setServerError(true);
    }
  }, [data]);
  const handleRefetch = (value, message) => {
    refetch({
      updateData(_, data) {
        return data;
      },
    });
    setOpenSnackbar(true);
    setSnackbarMessage(message);
    if (value) {
      setSnackbarVariant('error');
    } else {
      setSnackbarVariant('success');
    }
  };
  async function handleAddUploadCertificate(newData) {
    return await new Promise(async (resolve) => {
      console.log(newData);
      resolve();
      await addUploadCertificate({
        variables: {
          CertificateUploadInput: {
            Data: newData.uploadFile[0],
            StoreType: newData.StoreType,
            Subject: newData.Subject,
            Issuer: newData.Issuer,
            IssuedTo: newData.IssuedTo,
            IssuedBy: newData.IssuedBy,
            EffectiveDate: newData.EffectiveDate,
            ExpirationDate: newData.ExpirationDate,
            ExpirationDays: newData.ExpirationDays,
            Serialnumber: newData.Serialnumber,
            Thumbprint: newData.Thumbprint,
            Keysize: newData.Keysize,
            SignatureAlgorithm: newData.SignatureAlgorithm,
            ConnectorId: newData.ConnectorId,
          },
        },
      })
        .then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
          console.log(data, error, graphQLErrors, networkError, cacheHit);
          if (data !== null && error == false) {
            // handleRefetch(error, 'Saved successfully');
          } else if (error && graphQLErrors.length > 0) {
            let duperror = graphQLErrors[0].message;
            console.log(duperror);
            if (duperror !== null && duperror.indexOf('ER_DUP_ENTRY') !== -1) {
              // handleRefetch(error, 'Name already existed');
            }
          }
        })
        .catch((e) => {
          // you can do something with the error here
          console.log(e);
        });
    });
  }
  const handleAPISuccess = async (Status, aPIMsg) => {
    if (Status === 'failure' || Status === null) {
      handleRefetch(true, aPIMsg);
    } else {
      handleRefetch(false, aPIMsg);
    }
  };
  const handleCallAPI = async (event, rowData) => {
    console.log('rowData', rowData);
    await POSTAPIUploadCertificate({
      variables: {
        CertificateUploadInput: {
          name: rowData.name,
          certificatesDataUUID: rowData.certificatesDataUUID,
        },
      },
    }).then(({ data, error, graphQLErrors, networkError, cacheHit }) => {
      console.log(data, error, graphQLErrors, networkError, cacheHit);
      if (data !== null && error == false) {
        handleAPISuccess(
          data.CertificateUploadAPI.statusAPI,
          data.CertificateUploadAPI.msg
        );
      } else {
        handleRefetch(error, 'graphql hooks error');
      }
    });
  };
  if (loading) return <Loader />;
  if (serverError)
    return <Error type={'Sever connection lost.Please try again'} />;
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <MaterialTable
        icons={tableIcons}
        title="Input"
        columns={columns}
        data={certificateUploadData}
        editable={{
          onRowAdd: (newData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                resolve();
                handleAddUploadCertificate(newData);
              }, 600);
            }),
          onRowUpdate: (newData, oldData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                resolve();
                if (oldData) {
                }
              }, 600);
            }),
          onRowDelete: (oldData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                resolve();
              }, 600);
            }),
        }}
        options={{
          color: '#ffffff',
          headerStyle: {
            textAlign: 'center',
            fontSize: 12,
            fontWeight: 'bold',
            fontFamily: 'Arial !important',
            backgroundColor: '#0b153e',
            color: '#ffffff',
            padding: '4px',
          },
          searchFieldStyle: {
            color: '#0b153e',
          },

          actionsColumnIndex: -1,
          pageSize: 5,
          pageSizeOptions: [5, 10, 20],
          toolbar: true,
          paging: true,
        }}
        actions={[
          {
            icon: () => <CachedIcon style={{ color: '#0b153e' }} />,
            isFreeAction: true,
          },
          {
            icon: SendIcon,
            tooltip: 'Send API',
            onClick: (event, rowData) => handleCallAPI(event, rowData),
          },
        ]}
      />
    </MuiThemeProvider>
  );
}
