import MaterialTable from 'material-table';
import { forwardRef } from 'react';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import ReactDOM from 'react-dom';
import MTableToolbar from 'material-table';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import CloseIcon from '@material-ui/icons/Close';
import { withStyles } from '@material-ui/core/styles';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import { Grid, Typography } from '@material-ui/core';
import MuiDialogActions from '@material-ui/core/DialogActions';
import React, { Component, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import Timestamp from '../timestamp';
import { useMutation, useQuery } from 'graphql-hooks';
import Theme from '../css/theme';
import Loader from '../components/loader';
import Error from '../components/emptyPage';
import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles,
} from '@material-ui/core/styles';
function iconStyles() {
  return {};
}
const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(2),
    },
  },

  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));
const styles = (theme) => ({
  root: {
    Width: 1200,
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: '#FFFFFF',
  },
});
const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const ADD_ORG_INTERFACE = `mutation createOrgInterface($InputCreate:OrgInterfaceInput){
          createOrgInterface(input:$InputCreate){
              interfaceUUID 
              projectUUID 
              sourceProfileUUID
              targetProfileUUID
              docTypeUUID
              serviceUUID
              organizationUUID
          }
        }`;

const DISPLAY_PARTNERS = `{
          allPartners1{
            PartnerProfileUUID
            CorporationName            
          }
        }`;

const DISPLAY_All_ORGANIZATION = `query fetchOrganization($searchInput:String){
    fetchOrganization(searchInput:$searchInput) {
     
      organizationUUID
      organizatonName
    
    } 
  }
        `;
const DISPLAY_All_INTERFACE = `query fetchInterface1($searchInput:String){
          fetchInterface1(searchInput:$searchInput) {           
            interfaceUUID
            name           
          }
        }
        `;

const DISPLAY_All_PROJECT = `query fetchProject1($searchInput:String){
          fetchProject1(searchInput:$searchInput) {
            ProjectUUID
            ProjectName           
          }
        }
        `;

const DISPLAY_All_DOCUMENTTYPE = `query fetchDocumentType1($searchInput:String){
          fetchDocumentType1(searchInput:$searchInput) {          
            documentTypeUUID
            name           
          }
        }
        `;
const DISPLAY_All_SERVICE = `query fetchSERVICE1($searchInput:String){
          fetchService1(searchInput:$searchInput) {
            serviceUUID
            name      
          } 
        }
        `;
const DISPLAY_All_ORGINTERFACE = `query organisationInterface($searchInput:String){
    organisationInterface(searchInput:$searchInput) {
      idOrgInterface
      orgInterfaceUUID
      interfaceUUID
      projectUUID
      sourceProfileUUID
      targetProfileUUID
      docTypeUUID
      serviceUUID
      organizationUUID
      createdOn
      lastUpdatedOn
    } }
        `;
const DISPLAY_All_ORGINTERFACEBYUUID = `query fetchOrgInterfaceByUUIDs($organizationUUID:String,$projectUUID:String,$interfaceUUID:String){
  fetchOrgInterfaceByUUIDs(organizationUUID:$organizationUUID,projectUUID:$projectUUID,interfaceUUID:$interfaceUUID) {
    idOrgInterface
    orgInterfaceUUID
    interfaceUUID
    projectUUID
    sourceProfileUUID
    targetProfileUUID
    docTypeUUID
    serviceUUID
    organizationUUID
    createdOn
    lastUpdatedOn
  }
}
              `;
export default function OrgInterface(props) {
  const [searchInput, setSearchValue] = React.useState('');
  const [orgData, setOrgData] = React.useState([]);
  const [orgInterface, setOrgInterface] = React.useState([]);
  const [partnerData, setPartnerData] = React.useState([]);
  const [interfaceData, setInterfaceData] = React.useState([]);
  const [projectData, setProjectData] = React.useState([]);
  const [documentTypeData, setDocumentTypeData] = React.useState([]);
  const [serviceData, setServiceData] = React.useState([]);
  const {
    loading: oloading,
    error: oerror,
    data: OrgData,
    refetch: orefetch,
  } = useQuery(DISPLAY_All_ORGANIZATION, {
    variables: {
      searchInput: searchInput,
    },
  });
  const {
    loading: ploading,
    error: perror,
    data: PartnerData,
    refetch: prefetch,
  } = useQuery(DISPLAY_PARTNERS);
  const {
    loading: iloading,
    error: ierror,
    data: InterfaceData,
    refetch: irefetch,
  } = useQuery(DISPLAY_All_INTERFACE, {
    variables: {
      searchInput: searchInput,
    },
  });

  const {
    loading: pjloading,
    error: pjerror,
    data: ProjectData,
    refetch: pjrefetch,
  } = useQuery(DISPLAY_All_PROJECT, {
    variables: {
      searchInput: searchInput,
    },
  });

  const {
    loading: dtloading,
    error: dterror,
    data: DocumentTypeData,
    refetch: dtrefetch,
  } = useQuery(DISPLAY_All_DOCUMENTTYPE, {
    variables: {
      searchInput: searchInput,
    },
  });
  const {
    loading: sloading,
    error: serror,
    data: ServiceData,
    refetch: srefetch,
  } = useQuery(DISPLAY_All_SERVICE, {
    variables: {
      searchInput: searchInput,
    },
  });

  /* const { loading: iodloading, error: ioderror, data, refetch } = useQuery(
    DISPLAY_All_ORGINTERFACE,
    {
      variables: {
        searchInput: searchInput,
      },
    }
  );*/
  const { loading: iodloading, error: ioderror, data, refetch } = useQuery(
    DISPLAY_All_ORGINTERFACEBYUUID,
    {
      variables: {
        organizationUUID: props.orguuid,
        projectUUID: props.projectuuid,
        interfaceUUID: props.interfaceuuid,
      },
    }
  );

  const [addOrgInterface, { loading: ioloading, error: ioerror }] = useMutation(
    ADD_ORG_INTERFACE
  );
  const columns = [
    {
      title: 'orginterfaceUUID',
      field: 'orginterfaceUUID',
      editable: 'never',
      hidden: true,
    },
    {
      title: 'Organization ',
      field: 'organizationUUID',
      lookup: orgData,
      hidden: true,
    },
    {
      title: 'Project',
      field: 'projectUUID',
      lookup: projectData,
      hidden: true,
    },
    {
      title: 'Interface ',
      field: 'interfaceUUID',
      lookup: interfaceData,
      hidden: true,
    },
    {
      title: 'Source Profile ',
      field: 'sourceProfileUUID',
      lookup: partnerData,
    },
    {
      title: 'Target Profile ',
      field: 'targetProfileUUID',
      lookup: partnerData,
    },
    { title: 'Document ', field: 'docTypeUUID', lookup: documentTypeData },
    { title: 'Service ', field: 'serviceUUID', lookup: serviceData },

    {
      title: 'Created',
      field: 'createdOn',
      editable: 'never',
      render: (rowData) => {
        console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.createdOn);
        } else {
          return;
        }
      },
    },
    {
      title: 'Updated',
      field: 'lastUpdatedOn',
      editable: 'never',
      render: (rowData) => {
        console.log(rowData);
        if (rowData != undefined) {
          return Timestamp(rowData.lastUpdatedOn);
        } else {
          return;
        }
      },
    },
  ];

  useEffect(() => {
    if (data !== undefined) {
      console.log(data.fetchOrgInterfaceByUUIDs[0]);
      setOrgInterface(data.fetchOrgInterfaceByUUIDs);

      /* console.log("data",data.organisationInterface[0].OrganizationUUID,
      orgData[data.organisationInterface[0].OrganizationUUID],partnerData[data.organisationInterface[0].sourceProfileUUID],
      interfaceData[data.organisationInterface[0].interfaceUUID],projectData[data.organisationInterface[0].projectUUID],
      documentTypeData[data.organisationInterface[0].docTypeUUID],partnerData[data.organisationInterface[0].targetProfileUUID],
      interfaceData[data.organisationInterface[0].interfaceUUID],projectData[data.organisationInterface[0].projectUUID],
    )*/
    }
    if (OrgData !== undefined) {
      console.log(OrgData);
      var OrgdropDown = OrgData.fetchOrganization.reduce(
        (obj, item) => (
          (obj[item.organizationUUID] = item.organizatonName), obj
        ),
        {}
      );
      console.log(OrgdropDown);
      setOrgData(OrgdropDown);
    }
    /*  if(OrgData!==undefined){
     var obj = OrgData.fetchOrganization1.reduce(function(acc, cur, i) {
        acc[cur.OrganizationUUID] = cur.OrganizationName;
        return acc;
        }, {});
        setOrgData(obj)
    }*/

    if (PartnerData !== undefined) {
      var PartnerDataDD = PartnerData.allPartners1.reduce(
        (obj, item) => (
          (obj[item.PartnerProfileUUID] = item.CorporationName), obj
        ),
        {}
      );
      console.log(PartnerDataDD);
      setPartnerData(PartnerDataDD);
    }
    if (InterfaceData !== undefined) {
      var InterfaceDataDD = InterfaceData.fetchInterface1.reduce(
        (obj, item) => ((obj[item.interfaceUUID] = item.name), obj),
        {}
      );
      console.log(InterfaceDataDD);
      setInterfaceData(InterfaceDataDD);
    }
    if (ProjectData !== undefined) {
      var ProjectDataDD = ProjectData.fetchProject1.reduce(
        (obj, item) => ((obj[item.ProjectUUID] = item.ProjectName), obj),
        {}
      );
      console.log(ProjectDataDD);
      setProjectData(ProjectDataDD);
    }
    if (DocumentTypeData !== undefined) {
      var DocumentTypeDataDD = DocumentTypeData.fetchDocumentType1.reduce(
        (obj, item) => ((obj[item.documentTypeUUID] = item.name), obj),
        {}
      );
      console.log(DocumentTypeDataDD);
      setDocumentTypeData(DocumentTypeDataDD);
    }
    if (ServiceData !== undefined) {
      var ServiceDataDD = ServiceData.fetchService1.reduce(
        (obj, item) => ((obj[item.serviceUUID] = item.name), obj),
        {}
      );
      console.log(ServiceDataDD);
      setServiceData(ServiceDataDD);
    }
  }, [
    OrgData,
    PartnerData,
    InterfaceData,
    ProjectData,
    DocumentTypeData,
    ServiceData,
    data,
  ]);
  const [state, setState] = React.useState({
    data: [],
  });
  const [open, setOpen] = React.useState(false);
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const classes = makeStyles(iconStyles)();
  const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => (
      <Clear color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline
        color="error"
        {...props}
        style={{ display: 'none' }}
        ref={ref}
      />
    )),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => (
      <Edit
        {...props}
        ref={ref}
        style={{ display: 'none' }}
        onClick={console.log('hello world')}
      />
    )),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSvgIcon: {
          root: {
            color: '#0b153e',
          },
        },
        MuiDialogTitle: {
          root: {
            backgroundColor: '#0b153e',
            color: '#FFFFFF',
          },
        },
        MuiGrid: {
          container: {
            flexWrap: 'nowrap',
          },
        },
      },
    });
  const classes1 = useStyles();
  function handleAddOrgInterface(newData) {
    return new Promise((resolve) => {
      console.log({
        projectUUID: props.projectuuid,
        sourceProfileUUID: newData.sourceProfileUUID,
        targetProfileUUID: newData.targetProfileUUID,
        docTypeUUID: newData.docTypeUUID,
        serviceUUID: newData.serviceUUID,
        interfaceUUID: props.interfaceuuid,
        organizationUUID: props.orguuid,
      });
      addOrgInterface({
        variables: {
          InputCreate: {
            projectUUID: props.projectuuid,
            sourceProfileUUID: newData.sourceProfileUUID,
            targetProfileUUID: newData.targetProfileUUID,
            docTypeUUID: newData.docTypeUUID,
            serviceUUID: newData.serviceUUID,
            interfaceUUID: props.interfaceuuid,
            organizationUUID: props.orguuid,
          },
        },
      });

      if (ioloading) {
        console.log('OrgInterface cannot created');
      } else {
        console.log('OrgInterface created successfully');
        handleRefetch();
      }
    });
  }
  const handleRefetch = () => {
    refetch({
      updateData(_, data) {
        return data;
      },
    });
  };

  const handleUpdateOrgInterface = (newData) => {};
  const handleDeleteOrgInterface = (newData) => {};

  if (ioloading || iodloading) return <Loader />;
  if (ioerror || ioderror) return <Error />;
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <MaterialTable
        icons={tableIcons}
        title="Organization Interface"
        columns={columns}
        data={orgInterface}
        editable={{
          onRowAdd: (newData) => handleAddOrgInterface(newData),
          onRowUpdate: (newData, oldData) => handleUpdateOrgInterface(newData),
          onRowDelete: (oldData) => handleDeleteOrgInterface(oldData),
        }}
        options={{
          headerStyle: {
            textAlign: 'center',
            fontSize: 12,
            fontWeight: 'bold',
            fontFamily: 'Arial !important',
            backgroundColor: '#0b153e',
            color: '#ffffff',
            padding: '4px',
          
          },

          searchFieldStyle: {
            color: '#0b153e',
          },
          actionsColumnIndex: -1
        }}
        onRowClick={handleClickOpen}
      />
    </MuiThemeProvider>
  );
}
