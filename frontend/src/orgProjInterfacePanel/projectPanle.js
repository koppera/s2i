import React from 'react';
import { useMutation, useQuery } from 'graphql-hooks';
import { forwardRef, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';

import MuiExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';

import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import InterFacePanel from './interfacePanel';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import ProjectModelForm from '../components/tmsmodal';
import ProjectForm from './projectForm';
import Paper from '@material-ui/core/Paper';
import AddBoxIcon from '@material-ui/icons/AddBox';
import Tooltip from '@material-ui/core/Tooltip';
import Theme from '../css/theme';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import MuiExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import { withStyles } from '@material-ui/core/styles';
import Loader from '../components/loader';
import InterfaceModelForm from '../components/tmsmodal';
import InterfaceForm from './interfaceForm';
import FormControlLabel from '@material-ui/core/FormControlLabel';
const DISPLAY_All_PROJECTS = `query fetchProjectByOrgUUID($organizationUUID:String){
  fetchProjectByOrgUUID(organizationUUID:$organizationUUID) {
    idOrgProject
    projectUUID
    projectName
    projectDescription
    organizationUUID
    createdOn
    updatedOn
  } 
  }
`;
const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: '12px',
    fontWeight: theme.typography.fontWeightRegular,
    flexBasis: '33.33%',
    flexShrink: 0,
    color: '#000000',
  },

  secondaryHeading: {
    fontSize: '12px',
    color: '#000000',
  },
  expansionPanelSummaryContent: {
    '& > :last-child': {
      paddingRight: 100,
    },
  },
  expansionPanelSummaryExpandIcon: {
    top: '80%',
  },
}));
const ExpansionPanelDetails = withStyles((theme) => ({
  root: {
    padding: theme.spacing(1),
  },
}))(MuiExpansionPanelDetails);
const ExpansionPanelSummary = withStyles({
  root: {
    //  backgroundColor: 'rgba(0, 0, 0, .03)',
    borderBottom: '1px solid rgba(0, 0, 0, .125)',
    marginBottom: -1,
    minHeight: 56,
    '&$expanded': {
      minHeight: 56,
    },
  },
  content: {
    '&$expanded': {
      margin: '12px 0',
    },
  },
  expanded: {},
})(MuiExpansionPanelSummary);

export default function ProjectPanel(props) {
  const classes = useStyles();
  //Fetch ALL organization
  const { loading, error, data, refetch } = useQuery(DISPLAY_All_PROJECTS, {
    variables: {
      organizationUUID: props.orgUUID,
    },
  });
  const [project, setProject] = React.useState([]);
  const [open, setOpen] = React.useState(false);
  const [projuuid, setProjUUID] = React.useState();
  const [expanded, setExpanded] = React.useState();
  const [panelNum, setPanel] = React.useState('');
  useEffect(() => {
    if (data !== undefined && data.fetchProjectByOrgUUID.length > 0) {
      setProject(data.fetchProjectByOrgUUID);
    }
  }, [data]);
  const handleOpen = (val) => {
    setProjUUID(val.projectUUID);
    setOpen(true);
  };
  let handleClose = () => {
    setOpen(false);
  };
  const handleRefetch = (value, message) => {
    refetch({
      updateData(_, data) {
        return data;
      },
    });
  };
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiTypography: {
          h6: {
            fontSize: 14,
            fontFamily: 'Arial !important',
          },
        },
      },
    });
  const handleChange = (panel) => (event, newExpanded) => {
    console.log(panel);
    console.log(newExpanded);
    console.log(expanded);
    setPanel(panel);
    setExpanded(newExpanded ? panel : false);
  };
  if (loading) return <Loader />;
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <div className={classes.root}>
        {/*  <Tooltip title="Add Project" aria-label="add">
          <AddBoxIcon
            style={{
              marginLeft: '95%',
              marginTop: '0px',
              color: '#0b153e',
              width: '16px',

              fontSize: '24px',
            }}
            onClick={handleOpen}
          />
        </Tooltip>*/}

        <div>
          {data !== undefined && data.fetchProjectByOrgUUID.length > 0 ? (
            project.map((v, k) => {
              return (
                <ExpansionPanel
                  key={k}
                  expanded={expanded === `panel${k}`}
                  onChange={handleChange(`panel${k}`)}
                >
                  <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon color="primary" />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                    style={{ height: '0', padding: '1%' }}
                  >
                    <Grid container spacing={12}>
                      <Grid
                        item
                        xs={6}
                        sm={4}
                        gutterBottom
                        variant="body2"
                        color="textSecondary"
                        className="text"
                      >
                        <Typography className={classes.heading}>
                          {v.projectName}
                        </Typography>
                      </Grid>
                      <Grid
                        item
                        xs={6}
                        sm={4}
                        variant="body2"
                        color="textSecondary"
                        className="text"
                      >
                        <Typography className={classes.secondaryHeading}>
                          {v.projectDescription}
                        </Typography>
                      </Grid>
                    </Grid>
                    <Tooltip title="Add Project" aria-label="add">
                      <FormControlLabel
                        aria-label="Acknowledge"
                        onClick={(event) => event.stopPropagation()}
                        onFocus={(event) => event.stopPropagation()}
                        control={
                          <AddBoxIcon
                            style={{
                              color: '#0b153e',
                              width: '16px',

                              fontSize: '24px',
                            }}
                            onClick={() => handleOpen(v)}
                          />
                        }
                      />
                    </Tooltip>
                    {/* <Tooltip title="Add Interface" aria-label="add">
                      <AddBoxIcon
                        style={{
                          color: '#0b153e',
                          width: '16px',

                          fontSize: '24px',
                        }}
                        onClick={() => handleOpen(v)}
                      />
                      </Tooltip>*/}
                  </ExpansionPanelSummary>
                  <ExpansionPanelDetails>
                    <InterFacePanel
                      projectuuid={v.projectUUID}
                      orguuid={props.orgUUID}
                    />
                  </ExpansionPanelDetails>
                </ExpansionPanel>
              );
            })
          ) : (
            <div className={classes.root}>
              <center>
                <Paper className="paperpage" elevation={3}>
                  No Projects...Please add Projects
                </Paper>
              </center>
            </div>
          )}
        </div>
        {/* <ProjectModelForm
          isModalOpen={open}
          handleCloseModal={handleClose}
          title={'Project'}
        >
          <ProjectForm
            handleCloseModal={handleClose}
            isModalOpen={open}
            orgUUID={props.orgUUID}
            onSuccess={handleRefetch}
          />
        </ProjectModelForm>*/}
        <InterfaceModelForm
          isModalOpen={open}
          handleCloseModal={handleClose}
          title={'Interface'}
        >
          <InterfaceForm
            handleCloseModal={handleClose}
            isModalOpen={open}
            projectuuid={projuuid}
            onSuccess={handleRefetch}
          />
        </InterfaceModelForm>
      </div>
    </MuiThemeProvider>
  );
}
