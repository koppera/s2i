import React from 'react';
import { useMutation, useQuery } from 'graphql-hooks';
import { forwardRef, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';

import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import ProjectPanel from './projectPanle';
import Grid from '@material-ui/core/Grid';
import OrganizationModelForm from '../components/tmsmodal';
import OrganizationalForm from './organizationForm';
import ModalFab from '../components/modalfabtms';
import Paper from '@material-ui/core/Paper';
import MuiExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import { withStyles } from '@material-ui/core/styles';
import MuiExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ProjectModelForm from '../components/tmsmodal';
import ProjectForm from './projectForm';
import AddBoxIcon from '@material-ui/icons/AddBox';
import Tooltip from '@material-ui/core/Tooltip';
import Loader from '../components/loader';
import FormControlLabel from '@material-ui/core/FormControlLabel';
const DISPLAY_All_ORGANIZATION = `{
    allOrganization {
      idOrganization
      organizationUUID
      organizatonName
      organizationDescription
      createdOn
      lastUpdatedOn
    } 
      
  }
`;

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: '12px',
    fontWeight: theme.typography.fontWeightRegular,
    flexBasis: '33.33%',
    flexShrink: 0,
    color: '#000000',
  },

  secondaryHeading: {
    fontSize: '12px',
    color: '#000000',
  },
}));

const ExpansionPanelDetails = withStyles((theme) => ({
  root: {
    padding: theme.spacing(1),
  },
}))(MuiExpansionPanelDetails);

const ExpansionPanelSummary = withStyles({
  root: {
    //  backgroundColor: 'rgba(0, 0, 0, .03)',
    borderBottom: '1px solid rgba(0, 0, 0, .125)',
    marginBottom: -1,
    minHeight: 56,
    '&$expanded': {
      minHeight: 56,
    },
  },
  content: {
    '&$expanded': {
      margin: '12px 0',
    },
  },
  expanded: {},
})(MuiExpansionPanelSummary);

export default function SimpleExpansionPanel() {
  const classes = useStyles();
  //Fetch ALL organization
  const { loading, error, data, refetch } = useQuery(DISPLAY_All_ORGANIZATION);
  const [organization, setOrganization] = React.useState([]);
  const [open, setOpen] = React.useState(false);
  const [projectOpen, setProjectOpen] = React.useState(false);
  const [orguuid, setOrgUUID] = React.useState();
  const [expanded, setExpanded] = React.useState();
  const [panelNum, setPanel] = React.useState('');
  useEffect(() => {
    if (data !== undefined && data.allOrganization.length > 0) {
      setOrganization(data.allOrganization);
    }
  }, [data]);
  const handleOpen = () => {
    setOpen(true);
  };
  const handleCloseProject = () => {
    setOrgUUID('');
    setProjectOpen(false);
  };
  const handleProjectOpen = (val) => {
    console.log(val);
    setOrgUUID(val.organizationUUID);
    setProjectOpen(true);
  };

  const handleRefetch = (value, message) => {
    refetch({
      updateData(_, data) {
        return data;
      },
    });
    console.log(panelNum);
    if (panelNum !== '') {
      setExpanded(panelNum);
    }
  };
  const handleChange = (panel) => (event, newExpanded) => {
    console.log(panel);
    console.log(newExpanded);
    console.log(expanded);
    setPanel(panel);
    setExpanded(newExpanded ? panel : false);
  };
  if (loading) return <Loader />;
  return (
    <div>
      {data !== undefined && data.allOrganization.length > 0 ? (
        organization.map((v, k) => {
          return (
            <ExpansionPanel
              key={k}
              expanded={expanded === `panel${k}`}
              onChange={handleChange(`panel${k}`)}
            >
              <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon color="primary" />}
                aria-controls="panel1a-content"
                id="panel1a-header"
                style={{
                  height: '0',
                  padding: '1%',
                  marginTop: '0px',
                  marginBottom: '0px',
                }}
              >
                <Grid container spacing={1}>
                  <Grid
                    item
                    xs={6}
                    sm={6}
                    gutterBottom
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
                    <Typography className={classes.heading}>
                      {v.organizatonName}
                    </Typography>
                  </Grid>
                  <Grid
                    item
                    xs={6}
                    sm={6}
                    variant="body2"
                    color="textSecondary"
                    className="text"
                  >
                    <Typography className={classes.secondaryHeading}>
                      {v.organizationDescription}
                    </Typography>
                  </Grid>
                </Grid>
                <Tooltip title="Add Project" aria-label="add">
                  <FormControlLabel
                    aria-label="Acknowledge"
                    onClick={(event) => event.stopPropagation()}
                    onFocus={(event) => event.stopPropagation()}
                    control={
                      <AddBoxIcon
                        style={{
                          color: '#0b153e',
                          width: '16px',
                          fontSize: '24px',
                        }}
                        onClick={() => handleProjectOpen(v)}
                      />
                    }
                  />
                </Tooltip>
                {/*  <Tooltip title="Add Project" aria-label="add">
                  <AddBoxIcon
                    style={{
                      color: '#0b153e',
                      width: '16px',
                      fontSize: '24px',
                    }}
                    onClick={() => handleProjectOpen(v)}
                  />
                  </Tooltip>*/}
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <ProjectPanel orgUUID={v.organizationUUID} />
              </ExpansionPanelDetails>
            </ExpansionPanel>
          );
        })
      ) : (
        <div className={classes.root}>
          <center>
            <Paper className="paperorgpage">
              No Organisation...Please add Organisation
            </Paper>
          </center>
        </div>
      )}
      <ModalFab title="Add Organization">
        <OrganizationalForm isModalOpen={open} onSuccess={handleRefetch} />
      </ModalFab>
      <ProjectModelForm
        handleCloseModal={handleCloseProject}
        isModalOpen={projectOpen}
        title={'Project'}
      >
        <ProjectForm
          handleCloseModal={handleCloseProject}
          isModalOpen={projectOpen}
          orgUUID={orguuid}
          onSuccess={handleRefetch}
        />
      </ProjectModelForm>
      {/*  <OrganizationModelForm
        isModalOpen={open}
        handleCloseModal={handleClose}
        title={'organization'}
      >
        <OrganizationalPanel
          handleCloseModal={handleClose}
          isModalOpen={open}
        />
    </OrganizationModelForm>*/}
    </div>
  );
}
