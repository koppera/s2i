import React from 'react';
import { useMutation, useQuery } from 'graphql-hooks';
import { forwardRef, useEffect } from 'react';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';

import MuiExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Grid from '@material-ui/core/Grid';
import Theme from '../css/theme';
import Paper from '@material-ui/core/Paper';
import Tooltip from '@material-ui/core/Tooltip';
import AddBoxIcon from '@material-ui/icons/AddBox';
import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles,
} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import InterfaceModelForm from '../components/tmsmodal';
import InterfaceForm from './interfaceForm';
import InterfacExpansion from './interfaceExpansion';
import MuiExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import { withStyles } from '@material-ui/core/styles';
const DISPLAY_All_INTERFACE = `query fetchInterfaceByprojectUUID($projectUUID:String){
  fetchInterfaceByprojectUUID(projectUUID:$projectUUID) {
    idInterface
    interfaceUUID
    name
    description
    projectUUID
    createdOn
    lastUpdatedOn
  } 
  }
`;
const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: '12px',
    fontWeight: theme.typography.fontWeightRegular,
    flexBasis: '33.33%',
    flexShrink: 0,
    color: '#000000',
  },

  secondaryHeading: {
    fontSize: '12px',
    color: '#000000',
  },
}));
const ExpansionPanelDetails = withStyles((theme) => ({
  root: {
    padding: theme.spacing(1),
  },
}))(MuiExpansionPanelDetails);
const ExpansionPanelSummary = withStyles({
  root: {
    //  backgroundColor: 'rgba(0, 0, 0, .03)',
    borderBottom: '1px solid rgba(0, 0, 0, .125)',
    marginBottom: -1,
    minHeight: 56,
    '&$expanded': {
      minHeight: 56,
    },
  },
  content: {
    '&$expanded': {
      margin: '12px 0',
    },
  },
  expanded: {},
})(MuiExpansionPanelSummary);

export default function InterfacePanel(props) {
  const classes = useStyles();
  //Fetch ALL organization
  const { loading, error, data, refetch } = useQuery(DISPLAY_All_INTERFACE, {
    variables: {
      projectUUID: props.projectuuid,
    },
  });

  const [InterfaceValue, setInterface] = React.useState([]);

  const [open, setOpen] = React.useState(false);
  useEffect(() => {
    if (data !== undefined && data.fetchInterfaceByprojectUUID.length > 0) {
      setInterface(data.fetchInterfaceByprojectUUID);
    }
  }, [data]);

  const handleOpen = () => {
    setOpen(true);
  };
  let handleClose = () => {
    setOpen(false);
  };
  const handleRefetch = (value, message) => {
    refetch({
      updateData(_, data) {
        return data;
      },
    });
  };
  return (
    <div className={classes.root}>
      {/*  <Tooltip title="Add Interface" aria-label="add">
        <AddBoxIcon
         style={{marginLeft:"95%" ,marginTop:"0px",color:"#0b153e",width: "16px",
    
         fontSize: "24px"}}
          onClick={handleOpen}
        />
      </Tooltip>*/}

      <div>
        {data !== undefined && data.fetchInterfaceByprojectUUID.length > 0 ? (
          InterfaceValue.map((v, k) => {
            return (
              <ExpansionPanel key={k}>
                <ExpansionPanelSummary
                  expandIcon={<ExpandMoreIcon color="primary" />}
                  aria-controls="panel1a-content"
                  id="panel1a-header"
                >
                  <Grid container spacing={12}>
                    <Grid
                      item
                      xs={6}
                      sm={4}
                      gutterBottom
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <Typography className={classes.heading}>
                        {' '}
                        {v.name}
                      </Typography>
                    </Grid>
                    <Grid
                      item
                      xs={6}
                      sm={4}
                      variant="body2"
                      color="textSecondary"
                      className="text"
                    >
                      <Typography className={classes.secondaryHeading}>
                        {v.description}
                      </Typography>
                    </Grid>
                  </Grid>
                  {/*<Fab
                      size="small"
                      color="secondary"
                      aria-label="add"
                      className={classes.margin}
                    >
                      <AddIcon />
                   </Fab>*/}
                </ExpansionPanelSummary>
                <ExpansionPanelDetails></ExpansionPanelDetails>
                <InterfacExpansion
                  projectuuid={props.projectuuid}
                  orguuid={props.orguuid}
                  interfaceuuid={v.interfaceUUID}
                />
              </ExpansionPanel>
            );
          })
        ) : (
          <div className={classes.root}>
            <center>
              <Paper className="paperpage" elevation={3}>
                No Interface...Please add Interface
              </Paper>
            </center>
          </div>
        )}
      </div>
      {/* <InterfaceModelForm
        isModalOpen={open}
        handleCloseModal={handleClose}
        title={'Interface'}
      >
        <InterfaceForm
          handleCloseModal={handleClose}
          isModalOpen={open}
          projectuuid={props.projectuuid}
          onSuccess={handleRefetch}
        />
     </InterfaceModelForm>*/}
    </div>
  );
}
