import React, { useContext, useCallback } from 'react';
import { useMutation, useQuery } from 'graphql-hooks';
import { forwardRef, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid';
import Theme from '../css/theme';

import {
  createMuiTheme,
  MuiThemeProvider,
} from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';
const ADD_Organization = `mutation createAddress($input:OrganizationInput){
  createOrganization(input:$input) {
    idOrganization
    organizationUUID
    organizatonName
    organizationDescription
    createdOn
    lastUpdatedOn
  }
}`;
const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));
const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
       
        MuiInputLabel:{
          outlined:{
            fontSize:13,
            paddingRight:2

        }
      }
      },
    });
export default function Organisation(props) {
  const classes = useStyles();
  //ADD organizaton
  const [addOrganization, loading, data] = useMutation(ADD_Organization);

  const [values, setValues] = React.useState({
    OrganizationName: '',
    Description: '',
  });
  const [ErrMsg, setErrMsg] = React.useState(false);
  const [errorInfo, setErrorInfo] = React.useState({
    OrganizationName: false,
    Description: false,
  });
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [value, setValue] = React.useState('IS');
  const [variant, setSnackbarVariant] = React.useState('error');
  const [message, setSnackbarMessage] = React.useState();

  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }
  const handleChange = (name) => (event) => {
    setErrMsg(false);
    setValues({ ...values, [name]: event.target.value });
    switch (name) {
      case 'OrganizationName':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match('^[a-zA-Z0-9,-.:/ ]*$') &&
          event.target.value.length <= 25
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'Description':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match('^[a-zA-Z0-9,-.:()@+/ ]*$')
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      default:
        return false;
    }
  };
  const handleSubmit = async () => {
    console.log(values);
    setErrMsg(false);
    if (values.OrganizationName === '' || values.Description === '') {
      setErrMsg(true);
    } else {
      await addOrganization({
        variables: {
          input: {
            organizatonName: values.OrganizationName,
            organizationDescription: values.Description,
          },
        },
      });
      console.log(loading, data);
      if (loading.loading == false) {
        setErrMsg(false);
        setValues({
          ...values,
          OrganizationName: '',
          Description: '',
        });
        setTimeout(() => {
          props.onSuccess(loading.loading, 'organization created successfully');
        }, 1000);
      } else {
        props.onSuccess(loading.loading, 'Graphql hooks error');
      }
    }
  };

  const handlereset = () => {
    setErrMsg(false);
    setErrorInfo({
      Name: false,
      Description: false,
    });
    setValues({
      ...values,
      OrganizationName: '',
      Description: '',
    });
  };
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
    <div>
      {ErrMsg === true ? (
        <div style={{ textAlign: 'center' }} className="addfieldserror">
          Please fill the fields
        </div>
      ) : (
        ''
      )}

      <form className="configcontainer" noValidate autoComplete="off">
        <Grid container spacing={2}>
          <Grid
            item
            xs={6}
            sm={6}
            gutterBottom
            variant="body2"
            color="textSecondary"
            className="text"
          >
            <TextField
              id="outlined-dense"
              label="Organization Name "
              className="textField"
              margin="dense"
              onChange={handleChange('OrganizationName')}
              value={values.OrganizationName}
              variant="outlined"
              name="OrganizationName"
            />
            {errorInfo.OrganizationName === true ? (
              <div id="nameid" className="addfieldserror">
                "Allowed alphabets and Numerics ,no special characters are
                  allowed.Maximum length is 25."
              </div>
            ) : (
              ''
            )}
          </Grid>
          <Grid
            item
            xs={6}
            sm={6}
            gutterBottom
            variant="body2"
            color="textSecondary"
            className="text"
          >
            <TextField
              id="outlined-dense"
              label="Description "
              className="textField"
              onChange={handleChange('Description')}
              value={values.Description}
              margin="dense"
              variant="outlined"
              name="Description"
            />
            {errorInfo.Description === true ? (
              <div id="nameid" className="addfieldserror">
               "Allowed alphabets and Numerics ,no special characters are
                  allowed."
              </div>
            ) : (
              ''
            )}
          </Grid>
        </Grid>
        <Grid container spacing={3}>
          <Grid item sm={6} xs={6}>
            <Button
            style={{
            color:'#000006',
            backgroundColor:'#E5CCFF'
                         }}
              variant="contained"
              //size="Large"
              className="createpartnerbutton"
              onClick={handlereset}
            >
              RESET
            </Button>
          </Grid>
          <Grid item sm={6} xs={6}>
            <Button
              variant="contained"
              // size="Large"
              className="createpartnerbutton"
              onClick={handleSubmit}
            >
              SAVE
            </Button>
          </Grid>
        </Grid>
      </form>
    </div>
    </MuiThemeProvider>
  );
}
