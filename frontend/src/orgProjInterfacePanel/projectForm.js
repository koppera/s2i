import React, { useEffect, useContext, useCallback } from 'react';
import { useMutation, useQuery } from 'graphql-hooks';
import { forwardRef } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';
const ADD_PROJECT = `mutation createAddress($input:ProjectInput){
    createProject(input:$input) {
      idOrgProject
      projectUUID
      projectName
      projectDescription
      organizationUUID
      createdOn
      updatedOn
    } 
  }`;
const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));
export default function Project(props) {
  const classes = useStyles();

  const [addProject, addloading, adddata] = useMutation(ADD_PROJECT);

  const [values, setValues] = React.useState({
    ProjectName: '',
    Description: '',
  });
  const [ErrMsg, setErrMsg] = React.useState(false);
  const [errorInfo, setErrorInfo] = React.useState({
    ProjectName: false,
    Description: false,
  });
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [value, setValue] = React.useState('IS');
  const [variant, setSnackbarVariant] = React.useState('error');
  const [message, setSnackbarMessage] = React.useState();

  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }
  const handleChange = (name) => (event) => {
    setErrMsg(false);
    setValues({ ...values, [name]: event.target.value });
    switch (name) {
      case 'ProjectName':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match('^[a-zA-Z0-9,-.:/ ]*$') &&
          event.target.value.length <= 25
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      case 'Description':
        if (event.target.value.length === 0) {
          setValues({ ...values, [name]: '' });
          setErrorInfo({ ...errorInfo, [name]: false });
        } else if (
          event.target.value.match('^[a-zA-Z0-9,-.:()@+/ ]*$') 
        ) {
          setErrorInfo({ ...errorInfo, [name]: false });
          setValues({ ...values, [name]: event.target.value });
        } else {
          setErrorInfo({ ...errorInfo, [name]: true });
        }
        break;
      default:
        return false;
    }
  };
  const handleSubmit = async () => {
    console.log(values);
    setErrMsg(false);
    if (values.ProjectName === '' || values.Description === '') {
      setErrMsg(true);
    } else {
      addProject({
        variables: {
          input: {
            projectName: values.ProjectName,
            projectDescription: values.Description,
            organizationUUID: props.orgUUID,
          },
        },
      });

      if (addloading.loading) {
        console.log('project not created');
      } else {
        setErrMsg(false);
        setValues({
          ...values,
          ProjectName: '',
          Description: '',
        });
        console.log('project created successfully');
        props.onSuccess();
        handleModalClose();
      }
    }
  };
  const handlereset = () => {
    setErrMsg(false);
    setErrorInfo({
      ProjectName: false,
      Description: false,
    });
    setValues({
      ...values,
      ProjectName: '',
      Description: '',
    });
  };
  const handleModalClose = () => {
    props.handleCloseModal(false);
  };
  return (
    <div>
      <Grid container className="header">
        <Grid item xs={10} sm={10}>
          <h2 className="h2">Project</h2>
        </Grid>
        <Grid item xs={2} sm={2} className="close">
          <CloseIcon onClick={handleModalClose} />
        </Grid>
      </Grid>
      <div>
        {ErrMsg === true ? (
          <div style={{ textAlign: 'center' }} className="addfieldserror">
            Please fill the fields
          </div>
        ) : (
          ''
        )}

        <form className="content" noValidate autoComplete="off">
          <Grid container spacing={2}>
            <Grid
              item
              xs={6}
              sm={6}
              gutterBottom
              variant="body2"
              color="textSecondary"
              className="text"
            >
              <TextField
                id="outlined-dense"
                label="ProjectName "
                className="textField"
                margin="dense"
                onChange={handleChange('ProjectName')}
                value={values.ProjectName}
                variant="outlined"
                name="ProjectName"
              />
              {errorInfo.ProjectName === true ? (
                <div id="nameid" className="addfieldserror">
                  "Allowed alphabets and Numerics ,no special characters are
                  allowed.Maximum length is 25."
                </div>
              ) : (
                ''
              )}
            </Grid>
            <Grid
              item
              xs={6}
              sm={6}
              gutterBottom
              variant="body2"
              color="textSecondary"
              className="text"
            >
              <TextField
                id="outlined-dense"
                label="Description "
                className="textField"
                onChange={handleChange('Description')}
                value={values.Description}
                margin="dense"
                variant="outlined"
                name="Description"
              />
              {errorInfo.Description === true ? (
                <div id="nameid" className="addfieldserror">
                  "Allowed alphabets and Numerics ,no special characters are
                  allowed."
                </div>
              ) : (
                ''
              )}
            </Grid>
          </Grid>
          <Grid container spacing={3}>
            <Grid item sm={6} xs={6}>
              <Button
                style={{
                  color: '#000006',
                  backgroundColor: '#E5CCFF',
                }}
                variant="contained"
                //size="Large"
                className="createpartnerbutton"
                onClick={handlereset}
              >
                RESET
              </Button>
            </Grid>
            <Grid item sm={6} xs={6}>
              <Button
                variant="contained"
                // size="Large"
                className="createpartnerbutton"
                onClick={handleSubmit}
              >
                SAVE
              </Button>
            </Grid>
          </Grid>
        </form>
      </div>
    </div>
  );
}
