import MaterialTable from 'material-table';
import { useMutation, useQuery } from 'graphql-hooks';
import { forwardRef, useEffect } from 'react';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import ReactDOM from 'react-dom';
import MTableToolbar from 'material-table';
import React, { Component } from 'react';
import Timestamp from '../timestamp';
import Button from '@material-ui/core/Button';
import Theme from '../css/theme';
import Error from '../components/emptyPage';
import MySnackbarContentWrapper from '../components/Snackbar';
import Snackbar from '@material-ui/core/Snackbar';
import TextField from'@material-ui/core/TextField';
import Loader from '../components/loader';
import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles
} from '@material-ui/core/styles';

const DISPLAY_All_USERTYPES = `{
  allUserTypes {
    idUserTypes
    userTypesUUID
    userTypesName
    userTypesDescription
    userTypesComments
    createdOn
    lastUpdatedOn
  }
}
`;
const ADD_USERTYPES = `mutation createUserTypes($inputCreate:UserTypesInput){
  createUserTypes(input:$inputCreate)
  {
    userTypesName
    userTypesDescription
    userTypesComments
   
  }
}
`;

function iconStyles() {
  return {
  
  };
}
export default function UserTypesTable() {
  //ADD USER TYPES
  const [addUserTypes, addloading, adddata] = useMutation(ADD_USERTYPES);

  //FETCH ALL USER TYPES
  const { loading, error, data, refetch } = useQuery(DISPLAY_All_USERTYPES);
  console.log(data);
  const [userTypedata, setData] = React.useState([]);
  const [serverError, setServerError] = React.useState(false);
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [message, setSnackbarMessage] = React.useState();
  const [variant, setSnackbarVariant] = React.useState('error');
  const [state, setState] = React.useState({
    columns: [
      { title: 'UUID', field: 'userTypesUUID',editable: 'never',hidden:true },
      { title: 'Name', field: 'userTypesName' ,
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              placeholder="Name"
              margin="normal"
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            />

              <div style={{ color: 'red' }}>
              {props.rowData.userTypesName === '' ||
              !/^[a-zA-Z ]+$/.test(props.rowData.userTypesName)
                ? 'Enter only alphabets'
                : ''}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.userTypesName}</div>;
      },
    }, 
      { title: 'Description', field: 'userTypesDescription',
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder="Description"
              margin="normal"
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            />
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.userTypesDescription}</div>;
      },
    },
      { title: 'Comments', field: 'userTypesComments' ,
      editComponent: (props) => {
        console.log('editComponent', props, props.value);
        return (
          <div>
            <TextField
              /*variant="Standard"*/
              /* label="addresstypeName"*/
              placeholder="Comments"
              margin="normal"
              fullWidth
              getOptionLabel={(option) => option.name}
              value={props.value}
              onChange={(v) => props.onChange(v.target.value)}
            />

             <div style={{ color: 'red' }}>
              {props.rowData.userTypesComments === '' ||
              !/^[a-zA-Z0-9.() ]+$/.test(props.rowData.userTypesComments)
                ? 'Special charecters are not allowed except(.)'
                : ''}
            </div>
          </div>
        );
      },
      render: (rowData) => {
        return <div>{rowData.userTypesComments}</div>;
      },
    },
      { title: 'Created', field: 'createdOn',editable: 'never' ,
      render : (rowData) => {
        console.log(rowData)
        if(rowData!= undefined){
          return Timestamp(rowData.createdOn)
        }else{
          return 
        }
      }  ,
    
    },
      { title: 'Last Modified', field: 'lastUpdatedOn',editable: 'never',
      render : (rowData) => {
        console.log(rowData)
        if(rowData!= undefined){
          return Timestamp(rowData.lastUpdatedOn)
        }else{
          return 
        }
      }  }, 
     
          
    ],
    data: userTypedata
  });
  const handleRefetch = (value, message) => {
    refetch({
      updateData(_, data) {
        return data;
      },
    });
    setOpenSnackbar(true);
    setSnackbarMessage(message);
    if (value) {
      setSnackbarVariant('error');
    } else {
      setSnackbarVariant('success');
    }
  };
  function handleCloseSnackbar(event, reason) {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  }
  function convertDateTIME(date) {
    var indiaTime = new Date(date).toLocaleString('en-US', {
      timeZone: 'Asia/Kolkata'
    });
    indiaTime = new Date(indiaTime);
    return indiaTime.toLocaleString();
  }
  useEffect(() => {
    if (data !== undefined) {
      console.log(data.allUserTypes);
      setData(data.allUserTypes);
      // setState({ data: data.allUserTypes });
      /*    setState(prevState => {
        const data1 = [...prevState.data];
        console.log(data1);
        data1.push(data.allUserTypes);
        return { ...prevState, data1 };
      });*/
      setServerError(false);
    }
    else {
      console.log('server error');
      setServerError(true);
    }
  }, [data]);
  const classes = makeStyles(iconStyles)();
  const tableIcons = {
    Add: forwardRef((props, ref) => (
      <AddBox color="primary" {...props} ref={ref} />
    )),
    Check: forwardRef((props, ref) => (
      <Check color="primary" {...props} ref={ref} />
    )),
    Clear: forwardRef((props, ref) => (
      <Clear color="error" {...props} ref={ref} />
    )),
    Delete: forwardRef((props, ref) => (
      <DeleteOutline color="error" {...props} ref={ref} />
    )),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => (
      <Edit
        {...props}
        ref={ref}
        color="primary"
        onClick={console.log('hello world')}
      />
    )),
    Export: forwardRef((props, ref) => (
      <SaveAlt color="primary" {...props} ref={ref} />
    )),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => (
      <FirstPage color="primary" {...props} ref={ref} />
    )),
    LastPage: forwardRef((props, ref) => (
      <LastPage color="primary" {...props} ref={ref} />
    )),
    NextPage: forwardRef((props, ref) => (
      <ChevronRight color="primary" {...props} ref={ref} />
    )),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeft color="primary" {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => (
      <Clear color="primary" {...props} ref={ref} />
    )),
    Search: forwardRef((props, ref) => (
      <Search color="primary" {...props} ref={ref} />
    )),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownward {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <Remove {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
  };
  const getMuiTheme = () =>
    createMuiTheme(Theme, {
      overrides: {
        MuiSelect: {
          icon:{
            color:'#0b153e'
              }
        },
      
        MuiTypography:{h6:{
          fontSize:14,   
          fontFamily:"Arial !important"   
          }},
      }
    });

    function handleAddUserType(newData){
      addUserTypes({
        variables: {
          inputCreate: {
            usertypesUUID: newData.usertypesUUID,
            usertypesName: newData.usertypesName,
            usertypesDescription: newData.usertypesDescription,
            usertypesComments: newData.usertypesComments
          }
        }
      });
      console.log(addloading, adddata);
      if (addloading.loading) {
        console.log('user type not created');
        handleRefetch(addloading.loading, 'Graphql hooks error');
      } else {
        console.log('user type added successfully');
        handleRefetch(addloading.loading, 'Saved successfully');
      }
    }
    if (loading) return <Loader />;
    if (serverError)
    return <Error type={'Server connection lost.Please try again'} />;
  return (
    <MuiThemeProvider theme={getMuiTheme()}>
    <MaterialTable
     
     icons={tableIcons}
         title="User Type"
      columns={state.columns}
      data={state.data}
      editable={{
        onRowAdd: newData=>handleAddUserType(newData),          
          onRowUpdate: (newData, oldData) =>
            new Promise(resolve => {
              setTimeout(() => {
                resolve();
                if (oldData) {
                  setState(prevState => {
                    const data = [...prevState.data];
                    data[data.indexOf(oldData)] = newData;
                    return { ...prevState, data };
                  });
                }
              }, 600);
            }),        
        onRowDelete: oldData =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              setState(prevState => {
                const data = [...prevState.data];
                data.splice(data.indexOf(oldData), 1);
                return { ...prevState, data };
              });
            }, 600);
          }),
      }} 
      options={{
        headerStyle: {
        
        textAlign: 'center',
        fontSize:12,
        fontWeight:'bold',
        fontFamily:'Arial !important',
        backgroundColor:"#0b153e",
        color:"#ffffff",
        padding:'4px',
       
        },
          searchFieldStyle: {
              color: '#0b153e'
            },
            actionsColumnIndex: -1,
            pageSize:10,
            pageSizeOptions: [10, 25 ,50,100 ],
            toolbar: true,
            paging: true
        }}
      />
       <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        autoHideDuration={1500}
        open={openSnackbar}
        onClose={handleCloseSnackbar}
      >
        <MySnackbarContentWrapper
          onClose={handleCloseSnackbar}
          variant={variant}
          message={message}
        />
      </Snackbar>
    </MuiThemeProvider>
  );
}
