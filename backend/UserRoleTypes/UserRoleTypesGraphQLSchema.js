var { makeExecutableSchema } = require('graphql-tools');
const config = require('../dbConfig');
const mysql = require('mysql');
const withAuthfn=require('../verifyMiddleware/withAuth');
var connection = require('../database');

/*var MySQLStore = require('connect-mysql'); // mysql session store
var options = {
      config: {
        host: config.db.host,
        user: config.db.user,
        password: config.db.password,
        database: config.db.database,
        port: config.db.port,
      },
      pool:true,
      keepalive:true

    };

    connection=MySQLStore(options)*/ /*= mysql.createPool({
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database,
  port: config.db.port,
});*/
/*/*function handleDisconnect() {
  connection = mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database,
    port: config.db.port,
  }); // Recreate the connection, since
  // the old one cannot be reused.

  connection.connect(function (err) {
    // The server is either down
    if (err) {
      // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    } // to avoid a hot loop, and to allow our node script to
  }); // process asynchronous requests in the meantime.
  // If you're also serving http, display a 503 error.
  connection.on('error', function (err) {
    console.log('db error', err);
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      // Connection to the MySQL server is usually
      handleDisconnect(); // lost due to either server restart, or a
    } else {
      // connnection idle timeout (the wait_timeout
      throw err; // server variable configures this)
    }
  });
}

handleDisconnect();*/

//connection.connect();

const typeDefs = `
scalar Date

type UserRoleTypes{
  idUserRoleType:String
  userRoleTypeUUID:String
  userRoleTypeName:String
  userRoleTypeDescription:String
  userRoleTypeComment:String
  createdOn:Date
  lastUpdatedOn:Date
}

input UserRoleTypesInput{
  userRoleTypeUUID:String
  userRoleTypeName:String
  userRoleTypeDescription:String
  userRoleTypeComment:String
}

type Query{
   allUserRoleTypes:[UserRoleTypes]
}

type Mutation{
   createUserRoleTypes(input:UserRoleTypesInput):UserRoleTypes
   updateUserRoleTypes(input:UserRoleTypesInput): UserRoleTypes
   removeUserRoleTypes(input:UserRoleTypesInput):UserRoleTypes
}

`;

const resolvers = {
  Query: {
    allUserRoleTypes: (root, args, context, info) => {
      //sql query for fetch all userTypes
      return new Promise((resolve, reject) => {
        connection.query(
          'SELECT * from userroletype ORDER BY  createdOn DESC ',
          function (err, post) {
            if (err) {
              reject(err);
            } else {
              resolve(post);
            }
          }
        );
      });
    },
  },
  Mutation: {
    async createUserRoleTypes(root, { input }) {
      //sql query for create userTypes
      console.log(
        input.userRoleTypeUUID,
        input.userRoleTypeName,
        input.userRoleTypeDescription,
        input.userRoleTypeComment,
        Date.now(),
        Date.now()
      );
      //sql query for create userTypes
      return new Promise((resolve, reject) => {
        var sql =
          'INSERT INTO userroletype (userRoleTypeUUID, userRoleTypeName, userRoleTypeDescription, userRoleTypeComment) VALUES(?,?,?,?)';

        var values = [
          input.userRoleTypeUUID,
          input.userRoleTypeName,
          input.userRoleTypeDescription,
          input.userRoleTypeComment,
        ];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async updateUserRoleTypes(root, { input }) {
      console.log(input);
      //sql query for update UserRoleTypes
      return new Promise((resolve, reject) => {
        var sql =
          'UPDATE userroletype SET userRoleTypeName = ?,userRoleTypeDescription=? ,userRoleTypeComment=?,lastUpdatedOn=? WHERE userRoleTypeUUID = ?';

        var values = [
          input.userRoleTypeName,
          input.userRoleTypeDescription,
          input.userRoleTypeComment,
          new Date(),
          input.userRoleTypeUUID,
        ];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async removeUserRoleTypes(root, { input }) {
      console.log(input);
      //sql query for create userRoleType
      return new Promise((resolve, reject) => {
        var sql = 'DELETE FROM `userroletype` WHERE `userRoleTypeUUID` = ?;';

        var values = [input.userRoleTypeUUID];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
};

const userRoleTypesSchema = makeExecutableSchema({
  typeDefs,
  resolvers,
  formatError: (err) => {
    // Don't give the specific errors to the client.
    if (err.message.startsWith('Database Error: ')) {
      return new Error('sql error');
    }

    // Otherwise return the original error.  The error can also
    // be manipulated in other ways, so long as it's returned.
    return err;
  },
});

module.exports = userRoleTypesSchema;
