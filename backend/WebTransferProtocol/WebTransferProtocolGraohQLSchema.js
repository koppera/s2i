var { makeExecutableSchema } = require('graphql-tools');
const config = require('../dbConfig');
const mysql = require('mysql');
const withAuthfn=require('../verifyMiddleware/withAuth');
var connection = require('../database');

/*var MySQLStore = require('connect-mysql'); // mysql session store
var options = {
      config: {
        host: config.db.host,
        user: config.db.user,
        password: config.db.password,
        database: config.db.database,
        port: config.db.port,
      },
      pool:true,
      keepalive:true

    };

    connection=MySQLStore(options)*/ /*= mysql.createPool({
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database,
  port: config.db.port,
});*/
/*/*function handleDisconnect() {
  connection = mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database,
    port: config.db.port,
  }); // Recreate the connection, since
  // the old one cannot be reused.

  connection.connect(function (err) {
    // The server is either down
    if (err) {
      // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    } // to avoid a hot loop, and to allow our node script to
  }); // process asynchronous requests in the meantime.
  // If you're also serving http, display a 503 error.
  connection.on('error', function (err) {
    console.log('db error', err);
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      // Connection to the MySQL server is usually
      handleDisconnect(); // lost due to either server restart, or a
    } else {
      // connnection idle timeout (the wait_timeout
      throw err; // server variable configures this)
    }
  });
}

handleDisconnect();*/

//connection.connect();
const typeDefs = `
scalar Date

type WebTransferProtocol{
  webTransferProtocolUUID:String
  webTransferProtocolPort:String
  webTransferProtocolHost:String
  webTransferProtocolUserName:String
  webTransferProtocolPassword:String
  protocolTypeUUID:String
  status:String
  webTransferProtocolLocation:String
  partnerProfileID:String
  certificateUUID:String
  createdOn:Date
  lastUpdatedOn:Date
}

input WebTransferProtocolInput{
  webTransferProtocolUUID:String
  webTransferProtocolPort:String
  webTransferProtocolHost:String
  webTransferProtocolUserName:String
  webTransferProtocolPassword:String
  protocolTypeUUID:String
  Status:String
  webTransferProtocolLocation:String
  partnerProfileID:String
  certificateUUID:String
}
type Query{
   allWebTransferProtocol:[WebTransferProtocol]
   fetchWebTransferProtocol(searchInput:String): [WebTransferProtocol]
   fetchWebTransferProtocolByPartnerProfile(partnerProfileID:String): [WebTransferProtocol]
}

type Mutation{
   createWebTransferProtocol(input:WebTransferProtocolInput): WebTransferProtocol
   updateWebTransferProtocol(input:WebTransferProtocolInput): WebTransferProtocol
   removeWebTransferProtocol(input:WebTransferProtocolInput):WebTransferProtocol
}

`;

const resolvers = {
  Query: {
    allWebTransferProtocol: (root, args, context, info) => {
      //sql query for fetch all WebTransferProtocol
      return new Promise((resolve, reject) => {
        connection.query(
          'SELECT * from webtransferprotocol ORDER BY  createdOn DESC;',
          function (err, post) {
            if (err) {
              reject(err);
            } else {
              resolve(post);
            }
          }
        );
      });
    },
    async fetchWebTransferProtocolByPartnerProfile(root, { partnerProfileID }) {
      //sql query for fetch all WebTransferProtocol by partnerProfileUUID
      return new Promise((resolve, reject) => {
        if (partnerProfileID == '') {
          sql = 'SELECT * from webtransferprotocol ORDER BY  createdOn DESC';
        } else {
          var sql =
            ' select * from webtransferprotocol  where partnerProfileID=? ORDER BY  createdOn DESC  ';
          values = [partnerProfileID];
        }
        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            resolve(result);
          }
        });
      });
    },
    async fetchWebTransferProtocol(root, { searchInput }) {
      //sql query for fetch WebTransferProtocol

      return new Promise((resolve, reject) => {
        var sql, values;
        if (searchInput == '') {
          sql = 'Select * from webtransferprotocol ORDER BY  createdOn DESC';
          console.log(sql);
        } else {
          var sql =
            ' Select * from webtransferprotocol where (webTransferProtocolHost = ' +
            mysql.escape(searchInput) +
            ' OR    webTransferProtocolPort = ' +
            mysql.escape(searchInput) +
            ' OR    webTransferProtocolUserName = ' +
            mysql.escape(searchInput) +
            ' OR    webTransferProtocolPassword = ' +
            mysql.escape(searchInput) +
            ' OR    protocolTypeUUID = ' +
            mysql.escape(searchInput) +
            ' OR   Status = ' +
            mysql.escape(searchInput) +
            ' OR   webTransferProtocolLocation = ' +
            mysql.escape(searchInput) +
            ' OR    partnerProfileID = ' +
            mysql.escape(searchInput) +
            ' OR    certificateUUID  = ' +
            mysql.escape(searchInput) +
            ')  ORDER BY  createdOn DESC ';
        }

        connection.query(sql, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
  Mutation: {
    async createWebTransferProtocol(root, { input }) {
      console.log(input);
      //sql query for create WebTransferProtocol
      return new Promise((resolve, reject) => {
        var sql =
          'INSERT INTO webtransferprotocol (`webTransferProtocolHost`, `webTransferProtocolPort`, `webTransferProtocolUserName`, `webTransferProtocolPassword`, `protocolTypeUUID`,`Status`,  `partnerProfileID`, `certificateUUID`,`webTransferProtocolLocation`) VALUES(?,?,?,?,?,?,?,?,?)';

        var values = [
          input.webTransferProtocolHost,
          input.webTransferProtocolPort,
          input.webTransferProtocolUserName,
          input.webTransferProtocolPassword,
          input.protocolTypeUUID,
          input.Status,
          input.partnerProfileID,
          input.certificateUUID,
          input.webTransferProtocolLocation,
        ];
        console.log(values);
        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async updateWebTransferProtocol(root, { input }) {
      console.log(input);
      //sql query for update WebTransferProtocol
      return new Promise((resolve, reject) => {
        var sql =
          'UPDATE webtransferprotocol SET `webTransferProtocolHost`=?,`webTransferProtocolPort`=?,`webTransferProtocolUserName`=?,`webTransferProtocolPassword`=?,`protocolTypeUUID`=?,`Status`=?,`webTransferProtocolLocation`=?,`lastUpdatedOn`=?,`protocolTypeUUID`=?,`partnerProfileID`=?,`certificateUUID`=?  WHERE `webTransferProtocolUUID` = ?';
        var values = [
          input.webTransferProtocolHost,
          input.webTransferProtocolPort,
          input.webTransferProtocolUserName,
          input.webTransferProtocolPassword,
          input.protocolTypeUUID,
          input.Status,
          input.webTransferProtocolLocation,
          new Date(),
          input.protocolTypeUUID,
          input.partnerProfileID,
          input.certificateUUID,
          input.webTransferProtocolUUID,
        ];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async removeWebTransferProtocol(root, { input }) {
      console.log(input);
      //sql query for create userTypes
      return new Promise((resolve, reject) => {
        var sql =
          'DELETE FROM `webtransferprotocol` WHERE webTransferProtocolUUID= ?';

        var values = [input.webTransferProtocolUUID];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
};

const WebTransferProtocolSchema = makeExecutableSchema({
  typeDefs,
  resolvers,
  formatError: (err) => {
    // Don't give the specific errors to the client.
    if (err.message.startsWith('Database Error: ')) {
      return new Error('sql error');
    }

    // Otherwise return the original error.  The error can also
    // be manipulated in other ways, so long as it's returned.
    return err;
  },
});

module.exports = WebTransferProtocolSchema;
