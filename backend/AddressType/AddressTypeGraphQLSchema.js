var { makeExecutableSchema } = require('graphql-tools');
const config = require('../dbConfig');
const mysql = require('mysql');
const withAuthfn=require('../verifyMiddleware/withAuth');
var connection = require('../database');

/*var MySQLStore = require('connect-mysql'); // mysql session store
var options = {
      config: {
        host: config.db.host,
        user: config.db.user,
        password: config.db.password,
        database: config.db.database,
        port: config.db.port,
      },
      pool:true,
      keepalive:true

    };

    connection=MySQLStore(options)*/ /*= mysql.createPool({
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database,
  port: config.db.port,
});*/
/*/*function handleDisconnect() {
  connection = mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database,
    port: config.db.port,
  }); // Recreate the connection, since
  // the old one cannot be reused.

  connection.connect(function (err) {
    // The server is either down
    if (err) {
      // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    } // to avoid a hot loop, and to allow our node script to
  }); // process asynchronous requests in the meantime.
  // If you're also serving http, display a 503 error.
  connection.on('error', function (err) {
    console.log('db error', err);
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      // Connection to the MySQL server is usually
      handleDisconnect(); // lost due to either server restart, or a
    } else {
      // connnection idle timeout (the wait_timeout
      throw err; // server variable configures this)
    }
  });
}

handleDisconnect();*/

//connection.connect();
const typeDefs = `
scalar Date

type AddressTypes{
   idAddressType:String
   addressTypeUUID:String
   addressTypeName:String
   addressTypeDescription:String
   createdOn:Date
   lastUpdatedOn:Date
}

input AddressTypesInput{

  idAddressType:String
  addressTypeUUID:String
  addressTypeName:String
  addressTypeDescription:String
}
type Query{
   allAddressTypes:[AddressTypes]
}

type Mutation{
   createAddressTypes(input:AddressTypesInput): AddressTypes
   updateAddressTypes(input:AddressTypesInput): AddressTypes
   removeAddressType(input:AddressTypesInput):AddressTypes
}

`;

const resolvers = {
  Query: {
    allAddressTypes: (root, args, context, info) => {
      //sql query for fetch all userTypes
      return new Promise((resolve, reject) => {
        connection.query(
          'SELECT * from addresstype ORDER BY  createdOn DESC  ',
          function (err, post) {
            if (err) {
              reject(err);
            } else {
              resolve(post);
            }
          }
        );
      });
    },
  },
  Mutation: {
    async createAddressTypes(root, { input }) {
      console.log(
        input.addressTypeName,
        input.addressTypeDescription,
        Date.now(),
        Date.now()
      );
      //sql query for create userTypes
      return new Promise((resolve, reject) => {
        var sql =
          'INSERT INTO addresstype (addressTypeName, addressTypeDescription) VALUES(?,?)';

        var values = [input.addressTypeName, input.addressTypeDescription];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async updateAddressTypes(root, { input }) {
      console.log(input.addressTypeUUID);
      //sql query for update userTypes
      return new Promise((resolve, reject) => {
        var sql =
          'UPDATE addresstype SET addressTypeName = ?,addressTypeDescription=?,lastUpdatedOn=? WHERE addressTypeUUID = ?';

        var values = [
          input.addressTypeName,
          input.addressTypeDescription,
          new Date(),
          input.addressTypeUUID,
        ];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async removeAddressType(root, { input }) {
      console.log(input.addressTypeUUID);
      //sql query for create userTypes
      return new Promise((resolve, reject) => {
        var sql = 'DELETE FROM `addresstype` WHERE `addressTypeUUID` = ?;';

        var values = [input.addressTypeUUID];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
};

const addressTypesSchema = makeExecutableSchema({
  typeDefs,
  resolvers,
  formatError: (err) => {
    // Don't give the specific errors to the client.
    if (err.message.startsWith('Database Error: ')) {
      return new Error('sql error');
    }

    // Otherwise return the original error.  The error can also
    // be manipulated in other ways, so long as it's returned.
    return err;
  },
});

module.exports = addressTypesSchema;
