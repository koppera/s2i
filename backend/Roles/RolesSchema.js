let Mongoose=require('mongoose');

let RolesSchema=new Mongoose.Schema({
    RoleName:String,
    RoleGroups:[{
        gid:String,
        groupName:String,
        CreatedDate:{type:Date,default:Date}
    }],
    Creator:String,
    CreatedDate:{type:Date,default:Date}
});

module.exports=Mongoose.model('RolesSchema',RolesSchema);