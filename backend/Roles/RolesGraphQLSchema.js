let { makeExecutableSchema } = require('graphql-tools');
let RolesSchema = require('./RolesSchema');
var UserSchema = require('../Users/UserSchema');
var ldap = require('ldapjs');
var client = ldap.createClient({ url: 'ldap:// 18.220.251.58:389' });
client.bind("cn=admin,dc=acme,dc=changemaker,dc=community", "password", function(err) {
  console.log('Binderror' + err);
});
const typeDefs = `
scalar Date
type UIDRoleGroup{
    _id:String
    rid:String
    RoleName:String
    Creator:String
    CreatedDate:Date
  }
type UIDRole{
    _id:String
    rid:String
    RoleName:String
    Creator:String
    CreatedDate:Date
  }
type RoleGroup{
        gid:String,
        groupName:String,
        CreateDate:Date 
    }

type Roles{
    _id:String
    RoleName:String
    Creator:String
    CreatedDate:Date
}

input RoleGroupInput{
    gid:String,
    groupName:String,
    CreateDate:Date 

}
input RoleInput{   
    RoleName:String
    member:String
    memberId:String
    Creator:String
}

input RolesInput{
    RoleName:String
    member:String
    memberId:String
    Creator:String
    CreatedDate:Date
}

type Query{
    viewRole:[Roles]
    fetchRolesById(Uid:String):[UIDRole]
    filterRolesByUser(userId:String):[UIDRoleGroup]
}

type Mutation{
     createRole(input:RolesInput):Roles
}

`;

const resolvers = {
  Query: {
    viewRole: (root, args, context, info) => {
      const roles = RolesSchema.find()
        .sort({ CreatedDate: -1 })
        .exec();
      if (!roles) {
        throw new Error('Error- No roles Exist.');
      }
      return roles;
    },
    async fetchRolesById(root, { Uid }) {
      console.log(Uid);
      if (Uid != '') {
        //uid based groups of user
        console.log(Uid);
        const userGroups = await UserSchema.find({ Uid: Uid });
        console.log(userGroups);
        if (userGroups !== undefined && userGroups.length > 0) {
          console.log(userGroups[0].UserRoles);
          return userGroups[0].UserRoles;
        }
      } else {
        const groups = RolesSchema.find({})
          .sort({ CreatedDate: -1 })
          .exec();
        if (!groups) {
          throw new Error('Cannot fetch groups');
        }
        return groups;
      }
    },
    async filterRolesByUser(root, { userId }) {
      console.log(userId);
      console.log('role information');

      const roles = await RolesSchema.find({})
        .sort({ CreatedDate: -1 })
        .exec();
      if (!roles) {
        throw new Error('Cannot fetch groups');
      }
      if (userId !== undefined) {
        const userGroups = await UserSchema.find({ _id: userId });
        if (userGroups !== undefined && userGroups[0].UserRoles.length > 0) {
          //  console.log(userGroups[0].UserGroups);
          let assignedRoles = userGroups[0].UserRoles;
          var result = roles.filter(function(o1) {
            // filter out (!) items in result2
            return !assignedRoles.some(function(o2) {
              return o1.id === o2.rid; // assumes unique id
            });
          });
          console.log(result);
          return result;
        } else {
          return roles;
        }
      } else {
        return roles;
      }
    }
  },
  Mutation: {
    /* async createRole(root, { input }) {
      const RoleModel = new RolesSchema(input);
      const newRole = await RoleModel.save();
      if (!newRole) throw new Error('Cannot add Roles ');
      return newRole;
    }
  },*/
    async createRole(root, { input }) {
      console.log(input);
      var entry = {
        cn: input.RoleName,
        member: 'cn=' + input.member + ',ou=Users,dc=acme,dc=changemaker,dc=community',
        objectclass: ['groupOfNames', 'top']
      };
      let dn = 'cn=' + input.RoleName + ',ou=Roles,dc=acme,dc=changemaker,dc=community'
      console.log('dn', dn);
      console.log(entry);
      client.add(dn, entry, async function(err) {
        console.log(err);
        if (err === null) {
          const RoleModel = new RolesSchema({
            RoleName: input.RoleName,
            Creator: input.Creator
          });
          const newRole = await RoleModel.save();
          if (!newRole) {
            throw new Error('Cannot create Group');
          } else {
            console.log(RoleModel);
            console.log(input.memberId);
            let roleInput = {
              rid: RoleModel._id,
              RoleName: RoleModel.RoleName
            };
            console.log(roleInput);
            /*const updateRole = await UserSchema.update(
              { _id: input.memberId },
              {
                $push: {
                  UserRoles: roleInput
                }
              }
            );*/
            const updateRole = UserSchema.update(
              { _id: input.memberId },
              {
                $set: {
                  Role: RoleModel.RoleName
                }
              }
            );
            console.log(updateRole);
            if (!updateRole) {
              console.log('error in updating');
              throw new Error('Error - User cannot be updated');
            }
            return updateRole;
          }
          return newRole;
        }
      });
    }
  }
};

var rolesSchema = makeExecutableSchema({
  typeDefs,
  resolvers
});

module.exports = rolesSchema;
