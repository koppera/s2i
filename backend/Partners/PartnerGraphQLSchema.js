var { makeExecutableSchema } = require('graphql-tools');
const config = require('../dbConfig');
var format = require('date-format');
const mysql = require('mysql');
const withAuthfn=require('../verifyMiddleware/withAuth');
const axios = require('axios');
const isTokenValid =require('../validate');
var connection = require('../database');

/*var MySQLStore = require('connect-mysql'); // mysql session store
var options = {
      config: {
        host: config.db.host,
        user: config.db.user,
        password: config.db.password,
        database: config.db.database,
        port: config.db.port,
      },
      pool:true,
      keepalive:true

    };

    connection=MySQLStore(options)*/ /*= mysql.createPool({
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database,
  port: config.db.port,
});*/
/*/*function handleDisconnect() {
  connection = mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database,
    port: config.db.port,
  }); // Recreate the connection, since
  // the old one cannot be reused.

  connection.connect(function (err) {
    // The server is either down
    if (err) {
      // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    } // to avoid a hot loop, and to allow our node script to
  }); // process asynchronous requests in the meantime.
  // If you're also serving http, display a 503 error.
  connection.on('error', function (err) {
    console.log('db error', err);
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      // Connection to the MySQL server is usually
      handleDisconnect(); // lost due to either server restart, or a
    } else {
      // connnection idle timeout (the wait_timeout
      throw err; // server variable configures this)
    }
  });
}

handleDisconnect();*/

//connection.connect();

const typeDefs = `
scalar Date

type Partner{
  partnerProfileUUID:String
  corporationName:String
  organizationUUID:String
  partnerProfileDescription:String
  organizationUnit:String
  status:String
  prefferedProtocolUUID:String
    createdOn:Date
    lastUpdatedOn:Date
    APIStatus:String
    APIStatusMsg:String
  APIPartnerID:String
}

type ProtocolType{
  protocolTypeUUID:String
  protocolTypeName:String
}

input PartnerInput{
  corporationName:String
  organizationUUID:String
  partnerProfileDescription:String
  organizationUnit:String
  status:String
  prefferedProtocolUUID:String
    partnerProfileUUID:String
}

input PartnerInputs{
  corporationName:String
    organizationUUID:String
  partnerProfileDescription:String
  organizationUnit:String
  status:String
    profileidtypesUUID:String
    profileIDValue:String
    firstName:String
    lastName:String
    email:String
    phone:String
    addressName:String
    addresslines1:String
    city:String
    county:String
    state:String
    country:String
    zip:String
}
type Success1{
  success:String
}
type APISuccess{
  statusAPI:String
  msg:String
  partnerID:String
}

type Query{
   allPartners(search:String):[Partner]
   allPartnersSearch(search:String):[Partner]
   allProtocolType:[ProtocolType]
   onePartners(search:String):[Partner]
   allPartnerDetailsAPI(search:String):APISuccess
   fetchPartnerProfileByOrganization(organizationUUID:String):[Partner]
  
}

type Mutation{
   createPartner(input:PartnerInput):[Partner]
   createPartnerdetails(input:PartnerInputs):Partner
  updatePartner(input:PartnerInput):Partner
  deletePartner(input:String):Partner
  fetchPartnerProfileBySearch(input:PartnerInput):[Partner]
}
`;

var lastUpdatedOn = new Date();
const resolvers = {
  Query: {
    allPartnerDetailsAPI: (root, { search }) => {
      var profile = {};
      var addressObject = [];
      var contactObject = [];
      var iDsObject = [];
      var wTPObject = {};
      var fTPObject = {};
      return new Promise((resolve, reject) => {
        /*console.log(
                      'Select `CorporationName`, `PartnerProfileDescription`, `OrganizationUnit`, `Status` from partnerprofile where PartnerProfileUUID=' +
                        mysql.escape(search),
                        'SELECT `addressUUID`,`addressNameID`, `addresslines1`, `addressLines2`, `addresslines3`, `city`, `state`, `country`, `zip` FROM `address` where partnerProfileUUID='+mysql.escape(search),
                        'SELECT `role`, `firstName`, `lastName`, `email`, `phone`, `fax`, `addressUUID`, `contactTypeUUID` FROM `contact` WHERE partnerProfileUUID='+mysql.escape(search),
                    );*/
        console.log(search);
        if (search == '' || search == null || search == undefined) {
          // sql = 'SELECT * from contact ORDER BY 	createdOn DESC';
          var err = 'Searched Partner does not exist';
          reject(err);
        } else {
          connection.query(
            'Select `organizationUUID`, `corporationName`,(SELECT  `protocolTypeName` FROM `protocoltype` WHERE  protocolTypeUUID =prefferedProtocolUUID) as `prefferedProtocol`  ,`partnerProfileDescription`, `organizationUnit`, `status` from partnerprofile where  partnerProfileUUID=' +
              mysql.escape(search),
            function (err, allPartnersSearch) {
              console.log(allPartnersSearch);
              if (err) {
                reject(err);
              } else {
                profile = {
                  organizationUUID: allPartnersSearch[0].organizationUUID,
                  profileName: allPartnersSearch[0].corporationName,
                  organizationUnit: allPartnersSearch[0].organizationUnit,
                  status: allPartnersSearch[0].status,
                  prefferedProtocol: '',
                };
                connection.query(
                  'Select `addressUUID`,`addressNameID`, `addressLines1`, `addressLines2`, `addressLines3`, `city`, `state`, `country`, `zip` FROM `address` where ( partnerProfileUUID=' +
                    mysql.escape(search) +
                    ')',
                  function (err, allPartnersAddress) {
                    console.log(allPartnersAddress);
                    if (err) {
                      reject(err);
                    } else {
                      allPartnersAddress.map((v, i) => {
                        JSON.stringify(v);
                        //console.log("v",JSON.stringify(v),"i",i,v[0])
                        addressObject.push({
                          addressLine1: v.addressLines1,
                          addressLine2: v.addressLines2,
                          addressLine3: v.addressLines3,
                          city: v.city,
                          state: v.state,
                          country: v.country,
                          zip: v.zip,
                        });
                      });
                      // allPartnersSearch= JSON.stringify(allPartnersSearch)
                      //console.log("allPartnersSearch",allPartnersSearch);
                      connection.query(
                        'SELECT `contact`.`role`,`contact`.`firstName`,`contact`.`lastName`,`contact`.`email`,`contact`.`phone`,`contact`.`fax` ,(`contacttype`.`name`) as `contactTypeName`,`address`.`addressNameID`,`address`.`addressLines1`,`address`.`addressLines2`,`address`.`addressLines3`,`address`.`city`,`address`.`state`,`address`.`country`,`address`.`zip`,`address`.`addressStatus`,`address`.`addressTypeUUID` , `contact`.`contactTypeUUID` FROM `contact` JOIN `address` ON `address`.`contactUUID`=`contact`.`contactUUID` JOIN `contacttype` ON `contacttype`.`contactTypeUUID`=`contact`.`contactTypeUUID` WHERE `contact`.`partnerProfileUUID`=' +
                          mysql.escape(search) +
                          ' UNION  SELECT `contact`.`role`,`contact`.`firstName`,`contact`.`lastName`,`contact`.`email`,`contact`.`phone`,`contact`.`fax` ,(`contacttype`.`name`) as `contactTypeName`,`address`.`addressNameID`,`address`.`addressLines1`,`address`.`addressLines2`,`address`.`addressLines3`,`address`.`city`,`address`.`state`,`address`.`country`,`address`.`zip`,`address`.`addressStatus`,`address`.`addressTypeUUID` , `contact`.`contactTypeUUID` FROM `contact` JOIN `address` ON `address`.`addressUUID`=`contact`.`addressUUID` JOIN `contacttype` ON `contacttype`.`contactTypeUUID`=`contact`.`contactTypeUUID` WHERE `contact`.`partnerProfileUUID`=' +
                          mysql.escape(search),
                        function (err, allPartnersContact) {
                          if (err) reject(err);
                          else {
                            console.log(
                              'allPartnersContact',
                              allPartnersContact
                            );
                            // var data={"allPartnersContact":allPartnersContact,"allPartners":allPartnersSearch,"allPartnersAddress":allPartnersAddress}
                            //  console.log("data",data,"addressObject",addressObject,"profile",profile)
                            // resolve(true);
                            allPartnersContact.map((v, i) => {
                              JSON.stringify(v);
                              console.log(
                                'allPartnersContact------>v',
                                JSON.stringify(v),
                                'i',
                                i,
                                v[0]
                              );
                              // console.log("v",JSON.stringify(v),"i",i,v[0])
                              contactObject.push({
                                role: v.role,
                                firstName: v.firstName,
                                lastName: v.lastName,
                                email: v.email,
                                phone: v.phone,
                                fax: v.fax,
                                contactType: v.contactTypeName,
                                address: {
                                  addressLine1: v.addressLines1,
                                  addressLine2: v.addressLines2,
                                  addressLine3: v.addressLines3,
                                  city: v.city,
                                  state: v.state,
                                  country: v.country,
                                  zip: v.zip,
                                },
                              });
                            });
                            console.log('contactObject', contactObject);
                            console.log(
                              'SELECT (profileidtypes.ProfileIDTypeName) as profileidtypesUUID, `profileIDValue` FROM `profileidvalues` JOIN profileidtypes ON `profileidvalues`.profileidtypesUUID= profileidtypes.ProfileIDTypesUUID WHERE `partnerProfileUUID`=' +
                                mysql.escape(search)
                            );
                            // console.log("contactObject",contactObject,"data",data,"addressObject",addressObject,"profile",profile)
                            connection.query(
                              'SELECT (profileidtypes.ProfileIDTypeName) as profileidtypesUUID, `profileIDValue` FROM `profileidvalues` JOIN profileidtypes ON `profileidvalues`.profileidtypesUUID= profileidtypes.ProfileIDTypesUUID WHERE `partnerProfileUUID`=' +
                                mysql.escape(search),
                              function (err, allPartnersIDs) {
                                if (err) reject(err);
                                else {
                                  //var data={"allPartnersIDs":allPartnersIDs,"allPartnersContact":allPartnersContact,"allPartners":allPartnersSearch,"allPartnersAddress":allPartnersAddress}
                                  // console.log("data",data,"addressObject",addressObject,"profile",profile)
                                  // resolve(true);
                                  allPartnersIDs.map((v, i) => {
                                    JSON.stringify(v);
                                    // console.log("v",JSON.stringify(v),"i",i,v[0])
                                    iDsObject.push({
                                      idType: v.profileidtypesUUID,
                                      value: v.profileIDValue,
                                    });
                                  });
                                  connection.query(
                                    'SELECT * FROM `webtransferprotocol` WHERE  partnerProfileID=' +
                                      mysql.escape(search),
                                    function (err, allWTP) {
                                      if (err) {
                                        reject(err);
                                      } else {
                                        console.log('allWTP', allWTP);
                                        JSON.stringify(allWTP);
                                        if (allWTP.length !== 0) {
                                          wTPObject = {
                                            host:
                                              allWTP[0].webTransferProtocolHost,
                                            port:
                                              allWTP[0].webTransferProtocolPort,
                                            userName:
                                              allWTP[0]
                                                .webTransferProtocolUserName,
                                            password:
                                              allWTP[0]
                                                .webTransferProtocolPassword,
                                            location:
                                              allWTP[0]
                                                .webTransferProtocolLocation,
                                            protocol: 'HTTP',
                                          };
                                        }
                                        /*allWTP.map((v,i)=>{
                                                                                                                JSON.stringify(v)
                                                                                                              // console.log("v",JSON.stringify(v),"i",i,v[0])
                                                                                                                  wTPObject.push({
                                                                                                                          "host" : v.webTransferProtocolHost,
                                                                                                                          "port" : v.webTransferProtocolPort,
                                                                                                                          "userName" : v.webTransferProtocolUserName,
                                                                                                                          "password" : v.webTransferProtocolPassword,
                                                                                                                          "location" : v.webTransferProtocolLocation,
                                                                                                                          "protocol" : "HTTP"                                                                 
                                                                                                                      })
                                                                                                            })*/
                                        console.log('wTPObject', wTPObject);
                                        //resolve(allProtocolType);
                                        connection.query(
                                          'SELECT * FROM `filetransferprotocol` WHERE  partnerProfileUUID=' +
                                            mysql.escape(search),
                                          function (err, allFTP) {
                                            if (err) reject(err);
                                            else {
                                              //resolve(allProtocolType);
                                              console.log('allFTP', allFTP);
                                              JSON.stringify(allFTP);
                                              if (allFTP.length !== 0) {
                                                fTPObject = {
                                                  host: allFTP[0].FTPHost,
                                                  port: allFTP[0].FTPPort,
                                                  userName:
                                                    allFTP[0].FTPuserName,
                                                  password:
                                                    allFTP[0].FTPPassword,
                                                  directory:
                                                    allFTP[0].Directory,
                                                  protocol: 'FTP',
                                                };
                                              }
                                              /* allFTP.map((v,i)=>{
                                                                                                                  JSON.stringify(v)                                                                        
                                                                                                                // console.log("v",JSON.stringify(v),"i",i,v[0])
                                                                                                                    fTPObject.push({
                                                                                                                            "host" : v.FTPHost,
                                                                                                                            "port" : v.FTPPort,
                                                                                                                            "userName" : v.FTPuserName,
                                                                                                                            "password" : v.FTPPassword,
                                                                                                                            "location" : v.Directory,
                                                                                                                            "protocol" : "FTP"                                                                 
                                                                                                                        })
                                                                                                                            })*/
                                              console.log(
                                                'fTPObject',
                                                fTPObject
                                              );
                                              var jsonData = {
                                                profile: {
                                                  profileName:
                                                    allPartnersSearch[0]
                                                      .corporationName,
                                                  organizationUnit:
                                                    allPartnersSearch[0]
                                                      .organizationUnit,
                                                  status:
                                                    allPartnersSearch[0].status,
                                                  prefferedProtocol:
                                                    allPartnersSearch[0]
                                                      .prefferedProtocol,
                                                  ids: iDsObject,
                                                  addresses: addressObject,
                                                  contact: contactObject,
                                                  deliveryMethods: {
                                                    webProtocol: wTPObject,
                                                    fileTransfer: fTPObject,
                                                  },
                                                },
                                              };
                                              console.log('jsonData', jsonData);
                                              axios({
                                                method: 'post',
                                                headers: {
                                                  organization:
                                                    allPartnersSearch[0]
                                                      .organizationUUID,
                                                  environment: config.db.partnerENV,
                                                  logType: 'B2B',
                                                  system: 'SoftwareAG',
                                                  projectID: '',
                                                },
                                                // url: config.db.BASE_URL+'rest/CG_EDI_TMS_SYNC/webservices/restws/provider/partnerProfile',
                                                //url:'https://is.tms.changemaker.community/restv2/partnerProfile',
                                                url:
                                                  config.db.BASE_URL +
                                                  'restv2/partnerProfile',
                                                data: jsonData,
                                              }).then(function (response) {
                                                console.log(
                                                  'response',
                                                  response
                                                );
                                                console.log(
                                                  'response Data',
                                                  response.data.response
                                                );
                                                var status, partnerId;
                                                if (
                                                  response.data.response
                                                    .status === 'success'
                                                ) {
                                                  status = 'Active';
                                                  partnerId =
                                                    response.data.response.data
                                                      .partnerID;
                                                } else {
                                                  status = 'Inactive';
                                                  partnerId = null;
                                                }
                                                console.log(  'UPDATE `partnerprofile` SET `APIStatus`=' +
                                                mysql.escape(status) +
                                                ',`APIStatusMsg`=' +
                                                mysql.escape(
                                                  response.data.response.msg
                                                ) +
                                                ',`APIPartnerID`=' +
                                                mysql.escape(partnerId) +
                                                ' WHERE `partnerProfileUUID`=' +
                                                mysql.escape(search))
                                                connection.query(
                                                  'UPDATE `partnerprofile` SET `APIStatus`=' +
                                                    mysql.escape(status) +
                                                    ',`APIStatusMsg`=' +
                                                    mysql.escape(
                                                      response.data.response.msg
                                                    ) +
                                                    ',`APIPartnerID`=' +
                                                    mysql.escape(partnerId) +
                                                    ' WHERE `partnerProfileUUID`=' +
                                                    mysql.escape(search),
                                                  function (
                                                    err,
                                                    updatePartnerStatus
                                                  ) {
                                                    if (err) reject(err);
                                                    else
                                                      resolve({
                                                        statusAPI:
                                                          response.data.response
                                                            .status,
                                                        msg:
                                                          response.data.response
                                                            .msg,
                                                      });
                                                  }
                                                );

                                                /* resolve({
                                                statusAPI:
                                                  response.data.response.status,
                                                msg: response.data.response.msg,
                                              });*/
                                                //resolve({success:response.data.response.msg})
                                              });
                                              // console.log("jsonData",jsonData)
                                              //console.log("data",data,"iDsObject",iDsObject,"contactObject",contactObject,"addressObject",addressObject,"profile",profile)
                                              //resolve(true)
                                            }
                                          }
                                        );
                                      }
                                    }
                                  );
                                }
                              }
                            );
                          }
                        }
                      );

                      //resolve(allPartnersAddress);
                    }
                  }
                );
              }
            }
          );
        }
      });
    },

    //resolve(allPartnersAddress);

    // );
    //resolve(allPartnersSearch);}
    //}
    //}
    //);
    //});

    allProtocolType: () => {
      return new Promise((resolve, reject) => {
        connection.query(
          'SELECT protocolTypeUUID,protocolTypeName FROM protocoltype',
          function (err, allProtocolType) {
            if (err) reject(err);
            else resolve(allProtocolType);
          }
        );
      });
    },
    allPartners: async () => {
      //withAuthfn();
      return await new Promise((resolve, reject) => {
        connection.query(
          'Select *  from partnerprofile ORDER BY 	CreatedOn DESC; ',
          function (err, allPartner) {
            if (err) reject(err);
            else resolve(allPartner);
          }
        );
      });
    },
    allPartnersSearch: (root, { search }) => {
      return new Promise((resolve, reject) => {
        console.log(
          'Select *  from partnerprofile where partnerProfileUUID=' +
            mysql.escape(search)
        );
        connection.query(
          'Select *  from partnerprofile where partnerProfileUUID=' +
            mysql.escape(search),
          function (err, allPartnersSearch) {
            console.log(allPartnersSearch);
            if (err) reject(err);
            else resolve(allPartnersSearch);
          }
        );
      });
    },
    onePartners: (root, { search }) => {
      return new Promise((resolve, reject) => {
        console.log(
          'Select *  from partnerprofile where partnerProfileUUID=' +
            mysql.escape(search)
        );
        connection.query(
          'Select *  from partnerprofile where partnerProfileUUID=' +
            mysql.escape(search),
          function (err, allPartnersSearch) {
            console.log(allPartnersSearch);
            if (err) reject(err);
            else resolve(allPartnersSearch);
          }
        );
      });
    },
    fetchPartnerProfileByOrganization: (root, { organizationUUID }, context) => {
      return new Promise(async(resolve, reject) => {
        const {  token } = await context();
   console.log("context----token------------",token)
   /* const { error } = await isTokenValid(token);
    console.log("error------------",error)
    if (error) {
      throw new Error(error);
    }*/
   
        console.log(
          'Select *  from partnerprofile where organizationUUID=' +
            mysql.escape(organizationUUID)
        );
        var noSql;
        if(organizationUUID===''||organizationUUID===null)
           noSql= 'Select *  from partnerprofile ORDER BY 	CreatedOn DESC'; 
        else
           noSql='Select *  from partnerprofile where organizationUUID=' +
           mysql.escape(organizationUUID)
      
        connection.query(
          noSql,
          function (err, allPartnersSearch) {
            //console.log(allPartnersSearch);
            if (err) reject(err);
            else resolve(allPartnersSearch);
          }
        );
      });
    },
  },
  Mutation: {
    async deletePartner(root, { input }) {
      console.log('input', input);
      return new Promise((resolve, reject) => {
        console.log(
          'DELETE FROM `partnerprofile` WHERE partnerProfileUUID=' +
            mysql.escape(input)
        );
        connection.query(
          'DELETE FROM `partnerprofile` WHERE partnerProfileUUID=' +
            mysql.escape(input),
          function (err, deleteResult) {
            if (err) reject(err);
            else resolve(deleteResult);
          }
        );
      });
    },
    async updatePartner(root, { input }) {
      return new Promise((resolve, reject) => {
        console.log(
          'UPDATE `partnerprofile` SET `corporationName`=' +
            mysql.escape(input.corporationName) +
            ',`partnerProfileDescription`=' +
            mysql.escape(input.partnerProfileDescription) +
            ',`organizationUnit`=' +
            mysql.escape(input.organizationUnit) +
            ',`status`=' +
            mysql.escape(input.status) +
            ',lastUpdatedOn`=' +
            mysql.escape(new Date()) +
            ',`prefferedProtocolUUID`=' +
            mysql.escape(input.prefferedProtocolUUID) +
            ' WHERE partnerProfileUUID=' +
            mysql.escape(input.partnerProfileUUID)
        );
        connection.query(
          'UPDATE `partnerprofile` SET `corporationName`=' +
            mysql.escape(input.corporationName) +
            ',`partnerProfileDescription`=' +
            mysql.escape(input.partnerProfileDescription) +
            ',`organizationUnit`=' +
            mysql.escape(input.organizationUnit) +
            ',`status`=' +
            mysql.escape(input.status) +
            ',`lastUpdatedOn`=' +
            mysql.escape(new Date()) +
            ',`prefferedProtocolUUID`=' +
            mysql.escape(input.prefferedProtocolUUID) +
            ' WHERE partnerProfileUUID=' +
            mysql.escape(input.partnerProfileUUID),
          function (err, updateResult) {
            if (err) reject(err);
            else resolve(updateResult);
          }
        );
      });
    },
    async createPartner(root, { input }) {
      return new Promise((resolve, reject) => {
        console.log(
          'INSERT INTO `partnerprofile`(`organizationUUID`, `corporationName`, `partnerProfileDescription`, `organizationUnit`,`status`,`prefferedProtocolUUID`) VALUES (' +
            mysql.escape(input.organizationUUID) +
            ',' +
            mysql.escape(input.corporationName) +
            ',' +
            mysql.escape(input.partnerProfileDescription) +
            ',' +
            mysql.escape(input.organizationUnit) +
            ',' +
            mysql.escape(input.status) +
            ',' +
            mysql.escape(input.prefferedProtocolUUID) +
            ')'
        );
        var fetchsql =
          'SELECT * FROM partnerprofile where corporationName=? and organizationUnit=?';
        var fetchValues = [input.corporationName, input.organizationUnit];
        console.log(fetchsql, fetchValues);
        connection.query(fetchsql, fetchValues, function (errs, results) {
          if (errs) {
            reject(errs);
          } else {
            console.log(results, results.length);
            if (results.length > 0) {
              reject('Combination already exist');
            } else {
              connection.query(
                'INSERT INTO `partnerprofile`(`organizationUUID`, `corporationName`, `partnerProfileDescription`, `organizationUnit`,`status`,`prefferedProtocolUUID`) VALUES (' +
                  mysql.escape(input.organizationUUID) +
                  ',' +
                  mysql.escape(input.corporationName) +
                  ',' +
                  mysql.escape(input.partnerProfileDescription) +
                  ',' +
                  mysql.escape(input.organizationUnit) +
                  ',' +
                  mysql.escape(input.status) +
                  ',' +
                  mysql.escape(input.prefferedProtocolUUID) +
                  ')',
                function (err, result) {
                  if (err) {
                    reject(err);
                  } else {
                    console.log('PARTNER results...........');
                    console.log(result, result.insertId);

                    if (result !== undefined && result.insertId !== undefined) {
                      connection.query(
                        `SELECT * from partnerprofile where idPartnerProfile=` +
                          mysql.escape(result.insertId),
                        function (adderr, results) {
                          if (adderr) {
                            reject(adderr);
                          } else {
                            console.log('partner find results...........');
                            console.log(results);
                            resolve(results);
                          }
                        }
                      );
                    } else {
                      resolve(result);
                    }
                  }
                }
              );
            }
          }
        });
      });
    },
    fetchPartnerProfileBySearch: (root, { input }) => {
      return new Promise((resolve, reject) => {
        var sql =
          'SELECT * FROM partnerprofile where ( corporationName=? AND  organizationUnit=?)';
        var values = [input.corporationName, input.organizationUnit];
        connection.query(sql, values, function (err, result) {
          console.log(result);
          if (err) reject(err);
          else resolve(result);
        });
      });
    },

    /*async createPartnerDetails(root, { input }) {
        return new Promise((resolve, reject) => {
          console.log(
            'INSERT INTO `partnerprofile`(  `CorporationName`, `PartnerProfileDescription`, `OrganizationUnit`) VALUES (' +
              mysql.escape(input.CorporationName) +
              ',' +
              mysql.escape(input.PartnerProfileDescription) +
              ',' +
              mysql.escape(input.OrganizationUnit) +
              ')'
          );
          connection.query(
            'INSERT INTO `partnerprofile`(  `CorporationName`, `PartnerProfileDescription`, `OrganizationUnit`) VALUES (' +
              mysql.escape(input.CorporationName) +
              ',' +
              mysql.escape(input.PartnerProfileDescription) +
              ',' +
              mysql.escape(input.OrganizationUnit) +
              ')',
            function(err, result) {
              if (err) reject(err);
              else {
                connection.query(
                    'INSERT INTO `profileidvalues`( `profileidtypesUUID`, `profileIDValue`, `profileidUUID`, `createdOn`, `lastUpdatedOn`) VALUES (' +
                      mysql.escape(input.CorporationName) +
                      ',' +
                      mysql.escape(input.PartnerProfileDescription) +
                      ',' +
                      mysql.escape(input.OrganizationUnit) +
                      ')',
                    function(err, result) {
                      if (err) reject(err);
                      else {
        
        
                        
                      };
                    }
                  );


              };
            }
          );
        });
      }*/
  },
};

let PartnerProfileSchema = makeExecutableSchema({
  typeDefs,
  resolvers,
});

module.exports = PartnerProfileSchema;
