var { makeExecutableSchema } = require('graphql-tools');
const config = require('../dbConfig');
const mysql = require('mysql');
const { GraphQLUpload } = require('graphql-upload');
//const { ApolloServer, gql } = require('apollo-server');

const axios = require('axios');
const fs = require('fs');
var connection = require('../database');

/*var MySQLStore = require('connect-mysql'); // mysql session store
var options = {
      config: {
        host: config.db.host,
        user: config.db.user,
        password: config.db.password,
        database: config.db.database,
        port: config.db.port,
      },
      pool:true,
      keepalive:true

    };

    connection=MySQLStore(options)*/
/*/*function handleDisconnect() {
  connection = mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database,
    port: config.db.port,
  }); // Recreate the connection, since
  // the old one cannot be reused.

  connection.connect(function (err) {
    // The server is either down
    if (err) {
      // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    } // to avoid a hot loop, and to allow our node script to
  }); // process asynchronous requests in the meantime.
  // If you're also serving http, display a 503 error.
  connection.on('error', function (err) {
    console.log('db error', err);
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      // Connection to the MySQL server is usually
      handleDisconnect(); // lost due to either server restart, or a
    } else {
      // connnection idle timeout (the wait_timeout
      throw err; // server variable configures this)
    }
  });
}

handleDisconnect();*/

//connection.connect();
const typeDefs = `
  scalar Date
  scalar Upload

  type  CertificateUpload {
    idCertificatesData:String,
    certificatesDataUUID:String, 
    Name:String,
    Data:String, 
    StoreType:String,
    Subject:String, 
    Issuer:String, 
    IssuedTo:String,
    IssuedBy:String,
    EffectiveDate:Date,
    ExpirationDate:Date, 
    ExpirationDays:String, 
    Serialnumber:String, 
    Thumbprint:String, 
    Keysize:String, 
    SignatureAlgorithm:String,
    ConnectorId:String, 
    createdOn:Date, 
    updatedOn:Date
  }

  input CertificateUploadInput {
    certificatesDataUUID:String, 
    Name:String,
    Data: Upload!, 
    StoreType:String,
    Subject:String, 
    Issuer:String, 
    IssuedTo:String,
    IssuedBy:String,
    EffectiveDate:Date,
    ExpirationDate:Date, 
    ExpirationDays:String, 
    Serialnumber:String, 
    Thumbprint:String, 
    Keysize:String, 
    SignatureAlgorithm:String,
    ConnectorId:String, 
  }
  
  type certificateUploadAPISuccess{
    statusAPI:String
    msg:String
  }
  type Query {
    CertificateUploadAPI( input: CertificateUploadInput):CertificateUpload
    allCertificateUpload: [CertificateUpload]
    postCertificatedataAPI( input: CertificateUploadInput):CertificateUpload
  }
  type Mutation {
    createCertificateUpload( input: CertificateUploadInput): CertificateUpload
   
  }
`;

const resolvers = {
  Upload: GraphQLUpload,
  Query: {
    async CertificateUploadAPI(root, { input }) {
      return new Promise((resolve, reject) => {
        var certificateURL =
        config.db.ARCESB_URL + 'certificates(' +
          input.name +
          ')';
        axios(certificateURL, {
          method: 'GET',
          headers: {
            'x-arcesb-authtoken': '6n3V6y1x0A4e6y5K4b7r',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
          },
        }).then(function (response) {
          // console.log(response);
          console.log(response.data);
        });
      });
    },
    async postCertificatedataAPI(root, { input }) {
      return new Promise((resolve, reject) => {
        var postdata = {
          '@odata.type': 'ArcESBAPI.Certificates',
          Name: input.Name,
          Data: input.Data,
        };

        axios(config.db.ARCESB_URL + 'certificates', {
          method: 'POST',
          processData: false,
          data: JSON.stringify(postdata),
          headers: {
            'x-arcesb-authtoken': config.db.arcesbHeader,
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
          },
        }).then(function (response) {
          console.log(response);
          console.log(response.data);
        });
      });
    },
    allCertificateUpload: (root, args, context, info) => {
      //sql query for CertificateUpload
      return new Promise((resolve, reject) => {
        connection.query(
          'SELECT * from certificatesdata_arcesb  ORDER BY  createdOn DESC ',
          function (err, post) {
            if (err) {
              reject(err);
            } else {
              resolve(post);
            }
          }
        );
      });
    },
  },
  Mutation: {
    async createCertificateUpload(root, { input }) {
      console.log('file name----------------------' + input.Data);
      console.log(input);
      //sql query for createupload certificate

      const { filename, mimetype, createReadStream } = await input.Data;

      console.log(createReadStream);
      const stream = createReadStream();

      console.log('----------createReadStream---------', createReadStream);
      const streamData = await readStream(stream);
      const buff = Buffer.from(streamData);
      return new Promise(async (resolve, reject) => {
        var sql =
          'INSERT INTO certificatesdata_arcesb ( Name, Data, StoreType, Subject, Issuer, IssuedTo, IssuedBy, EffectiveDate, ExpirationDate, ExpirationDays, Serialnumber, Thumbprint, Keysize, SignatureAlgorithm, ConnectorId) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
        var values = [
          filename,
          streamData,
          input.StoreType,
          input.Subject,
          input.Issuer,
          input.IssuedTo,
          input.IssuedBy,
          input.EffectiveDate,
          input.ExpirationDate,
          input.ExpirationDays,
          input.Serialnumber,
          input.Thumbprint,
          input.Keysize,
          input.SignatureAlgorithm,
          input.ConnectorId,
        ];
        console.log('sql', sql, values);
        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
};

const readStream = async (stream) => {
  stream.setEncoding('base64');

  return new Promise((resolve, reject) => {
    let data = '';

    // eslint-disable-next-line no-return-assign
    stream.on('data', (chunk) => (data += chunk));
    stream.on('end', () => {
      resolve(data);
      console.log(data);
    });
    stream.on('error', (error) => {
      reject(error);
      console.log(error.stack);
    });
  });
};

/*const profileidvaluesSchema1 = new ApolloServer({
  typeDefs,
  resolvers,
});*/
const profileidvaluesSchema = makeExecutableSchema({
  typeDefs,
  resolvers,
  formatError: (err) => {
    // Don't give the specific errors to the client.
    if (err.message.startsWith('Database Error: ')) {
      return new Error('sql error');
    }

    // Otherwise return the original error.  The error can also
    // be manipulated in other ways, so long as it's returned.
    return err;
  },
});

module.exports = profileidvaluesSchema;
