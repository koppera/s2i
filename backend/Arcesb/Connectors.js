var { makeExecutableSchema } = require('graphql-tools');
const config = require('../dbConfig');
const mysql = require('mysql');
const withAuthfn = require('../verifyMiddleware/withAuth');
const axios = require('axios');
var connection = require('../database');

/*var MySQLStore = require('connect-mysql'); // mysql session store
var options = {
      config: {
        host: config.db.host,
        user: config.db.user,
        password: config.db.password,
        database: config.db.database,
        port: config.db.port,
      },
      pool:true,
      keepalive:true

    };

    connection=MySQLStore(options)*/ /*= mysql.createPool({
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database,
  port: config.db.port,
});*/
/*function handleDisconnect() {
  connection = mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database,
    port: config.db.port,
  }); // Recreate the connection, since
  // the old one cannot be reused.

  connection.connect(function (err) {
    // The server is either down
    if (err) {
      // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    } // to avoid a hot loop, and to allow our node script to
  }); // process asynchronous requests in the meantime.
  // If you're also serving http, display a 503 error.
  connection.on('error', function (err) {
    console.log('db error', err);
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      // Connection to the MySQL server is usually
      handleDisconnect(); // lost due to either server restart, or a
    } else {
      // connnection idle timeout (the wait_timeout
      throw err; // server variable configures this)
    }
  });
}

handleDisconnect();*/
//connection.connect();
const typeDefs = `
scalar Date

type connectorValues{
  idConnectors:String
  connectorsUUID:String
  connectorID:String
  workspaceUUID:String
  workspaceName:String
  ConnectorType:String
  ParentConnector:String
  createdOn:Date
  updatedOn:Date
}

input conectorInput{
  connectorsUUID:String
  connectorID:String
  workspaceUUID:String
  ConnectorType:String
  ParentConnector:String
}

type Query{
    fetchConnectorsAPI(input:conectorInput):connectorValues
    allConnectors:[connectorValues]
    postConnectorsAPI(input:conectorInput):connectorValues
    fetchConnectorsByWorkspaceUUID(workspaceuuid:String):[connectorValues]
}

type Mutation{
  createConnectorsAPI(input:conectorInput) :connectorValues
}

`;

const resolvers = {
  Query: {
    async fetchConnectorsAPI(root, { input }) {
      return new Promise((resolve, reject) => {
        var connectorURL =
        config.db.ARCESB_URL +'connectors(' +
          input.connectorID +
          ')';
        axios(connectorURL, {
          method: 'GET',
          headers: {
            'x-arcesb-authtoken': config.db.arcesbHeader,
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
          },
        }).then(function (response) {
          console.log(response);
          console.log(response.data);
        });
      });
    },
    async postConnectorsAPI(root, { input }) {
      return new Promise((resolve, reject) => {
        var postdata = {
          '@odata.type': 'ArcESBAPI.Ports',
          ConnectorId: input.connectorID,
          Workspace: input.workspaceUUID,
          ConnectorType: input.ConnectorType,
        };

        axios(config.db.ARCESB_URL +'connectors', {
          method: 'POST',
          processData: false,
          data: JSON.stringify(postdata),
          headers: {
            'x-arcesb-authtoken':  config.db.arcesbHeader,
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
          },
        }).then(function (response) {
          console.log(response);
          console.log(response.data);
        });
      });
    },
    async fetchConnectorsByWorkspaceUUID(root, { workspaceuuid }) {
      return new Promise((resolve, reject) => {
        var sql, values;
        sql =
          ' select `idConnectors`, `connectorsUUID`,ConnectorType,connectorID,ParentConnector, workspaceUUID,(SELECT `workspaceName` FROM `workspace_arcesb` WHERE `workspaceUUID`=`connectors_arcesb`.`workspaceUUID`) as `workspaceName`, `createdOn`, `updatedOn` from `connectors_arcesb` where `workspaceUUID`=? ORDER BY `createdOn` DESC  ';
        values = [workspaceuuid];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    allConnectors: (root, args, context, info) => {
      //sql query for allConnectors
      return new Promise((resolve, reject) => {
        connection.query(
          'SELECT * from connectors_arcesb ORDER BY  createdOn DESC ',
          function (err, post) {
            if (err) {
              reject(err);
            } else {
              resolve(post);
            }
          }
        );
      });
    },
  },
  Mutation: {
    createConnectorsAPI(root, { input }) {
      return new Promise((resolve, reject) => {
        console.log(input);
        var sql =
          'INSERT INTO connectors_arcesb (connectorID,workspaceUUID,ConnectorType,ParentConnector) VALUES(?,?,?,?)';

        var values = [
          input.connectorID,
          input.workspaceUUID,
          input.ConnectorType,
          input.ParentConnector,
        ];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
};

const connectorsSchema = makeExecutableSchema({
  typeDefs,
  resolvers,
  formatError: (err) => {
    // Don't give the specific errors to the client.
    if (err.message.startsWith('Database Error: ')) {
      return new Error('sql error');
    }

    // Otherwise return the original error.  The error can also
    // be manipulated in other ways, so long as it's returned.
    return err;
  },
});

module.exports = connectorsSchema;
