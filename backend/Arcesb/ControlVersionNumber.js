var { makeExecutableSchema } = require('graphql-tools');
const config = require('../dbConfig');
const mysql = require('mysql');
const withAuthfn = require('../verifyMiddleware/withAuth');
const axios = require('axios');
var connection = require('../database');/*= mysql.createPool({
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database,
  port: config.db.port,
});*/
/*function handleDisconnect() {
  connection = mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database,
    port: config.db.port,
  }); // Recreate the connection, since
  // the old one cannot be reused.

  connection.connect(function (err) {
    // The server is either down
    if (err) {
      // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    } // to avoid a hot loop, and to allow our node script to
  }); // process asynchronous requests in the meantime.
  // If you're also serving http, display a 503 error.
  connection.on('error', function (err) {
    console.log('db error', err);
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      // Connection to the MySQL server is usually
      handleDisconnect(); // lost due to either server restart, or a
    } else {
      // connnection idle timeout (the wait_timeout
      throw err; // server variable configures this)
    }
  });
}

handleDisconnect();*/
//connection.connect();
const typeDefs = `
scalar Date

type controlVersionNumberValues{
    idControlVersionNumber:String
    controlVersionNumberUUID:String
    name:String
    createdOn:Date
    updatedOn:Date
}
input controlVersionNumberInput{
    controlVersionNumberUUID:String
    name:String
}

type Query{
 allControlVersionNumber:[controlVersionNumberValues]
}

type Mutation{
  createControlVersionNumber(input:controlVersionNumberInput): controlVersionNumberValues
  updateControlVersionNumber(input:controlVersionNumberInput): controlVersionNumberValues
  removeControlVersionNumber(input:controlVersionNumberInput): controlVersionNumberValues
}

`;

const resolvers = {
  Query: {
    allControlVersionNumber: (root, args, context, info) => {
      //sql query for fetch controlVersionNumber
      return new Promise((resolve, reject) => {
        connection.query(
          'SELECT * from controlVersionNumber_arcesb ORDER BY  createdOn DESC ',
          function (err, post) {
            if (err) {
              reject(err);
            } else {
              resolve(post);
            }
          }
        );
      });
    },
  },
  Mutation: {
    async createControlVersionNumber(root, { input }) {
      return new Promise((resolve, reject) => {
        var sql =
          'INSERT INTO `controlVersionNumber_arcesb`( `name`) VALUES (?)';

        var values = [input.name];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async updateControlVersionNumber(root, { input }) {
      console.log(input);
      //sql query for update controlVersionNumber
      return new Promise((resolve, reject) => {
        var sql =
          'UPDATE controlVersionNumber_arcesb SET name=?,updatedOn=? WHERE controlVersionNumberUUID = ?';

        var values = [input.name, new Date(), input.controlVersionNumberUUID];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async removeControlVersionNumber(root, { input }) {
      console.log(input);
      //sql query for remove controlVersionNumber
      return new Promise((resolve, reject) => {
        var sql =
          'DELETE FROM `controlVersionNumber_arcesb` WHERE `controlVersionNumberUUID` = ?;';

        var values = [input.controlVersionNumberUUID];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
};

const controlVersionNumberSchema = makeExecutableSchema({
  typeDefs,
  resolvers,
  formatError: (err) => {
    // Don't give the specific errors to the client.
    if (err.message.startsWith('Database Error: ')) {
      return new Error('sql error');
    }

    // Otherwise return the original error.  The error can also
    // be manipulated in other ways, so long as it's returned.
    return err;
  },
});

module.exports = controlVersionNumberSchema;
