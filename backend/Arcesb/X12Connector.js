var { makeExecutableSchema } = require('graphql-tools');
const config = require('../dbConfig');
const mysql = require('mysql');
const withAuthfn = require('../verifyMiddleware/withAuth');
const axios = require('axios');
const { GraphQLUpload } = require('graphql-upload');
var connection = require('../database'); /*= mysql.createPool({
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database,
  port: config.db.port,
});*/
/*function handleDisconnect() {
  connection = mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database,
    port: config.db.port,
  }); // Recreate the connection, since
  // the old one cannot be reused.

  connection.connect(function (err) {
    // The server is either down
    if (err) {
      // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    } // to avoid a hot loop, and to allow our node script to
  }); // process asynchronous requests in the meantime.
  // If you're also serving http, display a 503 error.
  connection.on('error', function (err) {
    console.log('db error', err);
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      // Connection to the MySQL server is usually
      handleDisconnect(); // lost due to either server restart, or a
    } else {
      // connnection idle timeout (the wait_timeout
      throw err; // server variable configures this)
    }
  });
}

handleDisconnect();*/

//connection.connect();
const typeDefs = `
scalar Date
scalar Upload
type X12Connector{
  idX12Connector:String, 
  X12ConnectorUUID:String, 
  connectorId:String, 
  connectorDescription:String, 
  transactionType:String, 
  profilesenderIdentifier:String, 
  senderIdQualifier:String, 
  receiverIdQualifier:String, 
  profilereceiverIdentifier:String, 
  controlVersionNumber:String, 
  usageIndicator:String,
  functionalGroup:String,
  ta1Acknowledgement:String,
  functionalACK:String, 
  functionalACKType:String, 
  send:String, 
  resendInterval:String, 
  resendIntervalMaximumAttachments:String, 
  dataElementSeparator:String, 
  componentElementSeparator:String, 
  segmentTerminator:String, 
  suffix:String, 
  authorizationQualifier:String,
  authorizationId:String, 
  securityQualifier:String,
  securityPassword:String, 
  controlStandardsIdentifier:String,
  senderIdentifier:String, 
  receiverIdentifier:String, 
  dateFormat:String,
  timeFormat:String,
  responsibleAgencyCode:String,
  identifierCode:String, 
  encoding:String,
  generateDescriptionAs:String,
  hippaSchemas:String,
  hippa278:String,
  nestLoops:String, 
  parentConnector:String,
  processingDelay:String,
  snipValidation:String, 
  strictSchemaValidation:String, 
  localFileScheme:String,
  logMessages:String, 
  sendFilter:String, 
  saveToSentFolder:String,
  createdOn:String, 
  updatedOn:String
  batchTransactions:String
  useReferenceId:String
  expandQualifierValues:String
  othersettingsFuntionalACKS:String   
  workspaceUUID:String
  APIStatus:String
  workspaceName:String
 
}
type x12ConnectorStatus{
  statusAPI:String
  msg:String
}
input X12ConnectorInput{
  X12ConnectorUUID:String, 
  connectorId:String, 
  connectorDescription:String, 
  transactionType:String, 
  profilesenderIdentifier:String, 
  senderIdQualifier:String, 
  receiverIdQualifier:String, 
  profilereceiverIdentifier:String, 
  controlVersionNumber:String, 
  usageIndicator:String,
  functionalGroup:String,
  ta1Acknowledgement:String,
  functionalACK:String, 
  functionalACKType:String, 
  send:String, 
  resendInterval:String, 
  resendIntervalMaximumAttachments:String, 
  dataElementSeparator:String, 
  componentElementSeparator:String, 
  segmentTerminator:String, 
  suffix:String, 
  authorizationQualifier:String,
  authorizationId:String, 
  securityQualifier:String,
  securityPassword:String, 
  controlStandardsIdentifier:String,
  senderIdentifier:String, 
  receiverIdentifier:String, 
  dateFormat:String,
  timeFormat:String,
  responsibleAgencyCode:String,
  identifierCode:String, 
  encoding:String,
  generateDescriptionAs:String,
  hippaSchemas:String,
  hippa278:String,
  nestLoops:String, 
  parentConnector:String,
  processingDelay:String,
  snipValidation:String, 
  strictSchemaValidation:String, 
  localFileScheme:String,
  logMessages:String, 
  sendFilter:String, 
  saveToSentFolder:String,
  updatedOn:String
  batchTransactions:String
  useReferenceId:String
  expandQualifierValues:String
  othersettingsFuntionalACKS:String
  workspaceUUID:String

  APIStatus:String

}
 input transactionFileInput{
  file: Upload!
  workSpaceName:String
  workFlowName:String
 }

type Query{
   allX12Connector:[X12Connector]
   postX12ConnectorAPI(input:X12ConnectorInput):x12ConnectorStatus
   fileuploadtestService(input:transactionFileInput):x12ConnectorStatus
}

type Mutation{
   createX12Connector(input:X12ConnectorInput): X12Connector
   updateX12Connector(input:X12ConnectorInput): X12Connector
   removeX12Connector(input:X12ConnectorInput):X12Connector
}

`;

const resolvers = {
  Upload: GraphQLUpload,
  Query: {
    allX12Connector: (root, args, context, info) => {
      //sql query for fetch all X12Connector
      return new Promise((resolve, reject) => {
        connection.query(
          'SELECT * ,(SELECT `workspaceName` FROM `workspace_arcesb` WHERE `workspaceUUID`=`x12connector_arcesb`.`workspaceUUID`) as `workspaceName` from x12connector_arcesb ORDER BY  createdOn DESC ',
          function (err, post) {
            if (err) {
              reject(err);
            } else {
              resolve(post);
            }
          }
        );
      });
    },
    fileuploadtestService(root, { input }) {
      return new Promise(async (resolve, reject) => {
        console.log('input', input);
        console.log(input.content);
        const { filename, mimetype, createReadStream } = await input.file;
        console.log(createReadStream);
        const stream = createReadStream();
        console.log('----------createReadStream---------', createReadStream);
        const streamData = await readStream(stream);
        //const encodedString = Buffer.from(streamData).toString('base64');
        /// console.log(encodedString);

        var postdata = {
          ConnectorId: input.workFlowName,
          Folder: 'Send',
          Filename: '850FromAmazone.edi',
          Content: streamData,
        };

        /*var postdata = {
                "Workspaceid": "ArcesbTest"
                
              };*/
        console.log(postdata);
        await axios({
          method: 'post',
          url: config.db.ARCESB_URL + 'files',
          data: postdata,
          headers: {
            'x-arcesb-authtoken': config.db.arcesbHeader,
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
          },
        })
          .then(function (response) {
            //console.log(response);
            console.log(response.data);

            if (response.data != null) {
              resolve({
                statusAPI: 'success',
                msg: 'Transaction Processed Successfully',
              });
            }
          })
          .catch((error) => {
            console.log(error);
            reject(error);
          });
      });
    },
    postX12ConnectorAPI(root, { input }) {
      console.log(input);

      return new Promise(async (resolve, reject) => {
        connection.query(
          'SELECT workspaceName,senderQualifier FROM workspace_arcesb where workspaceUUID=' +
            mysql.escape(input.workspaceUUID),
          async function (errS, resultS) {
            if (errS) {
              reject(errS);
            } else {
              let workspaceName = resultS[0].workspaceName;
              let senderQualifier = resultS[0].senderQualifier;
              let transactionTypeval = 'X124010850';
              console.log(workspaceName);
              var postdata = {
                workspace: workspaceName,

                PortType: 'X12',
                ConnectorId: input.connectorId,
                //  workspaceName + '_' + transactionTypeval + '_Inbound',
                ISAReceiverIdQualifier: input.receiverIdQualifier,
                ISASenderIdQualifier: input.senderIdQualifier,
                ISAReceiverIdentifier: input.receiverIdentifier,
                ISASenderIdentifier: input.senderIdentifier,
                TranslationType:
                  input.transactionType == 'X12 to XML'
                    ? 'X12ToXML'
                    : 'XMLToX12',
                SendToPort: input.connectorId + '_REST',
                ISASecurityQualifier: input.securityQualifier, //00,
                TrimWhitespace: 'False',
                ResendInterval: input.resendInterval,
                ISAAuthorizationQualifier: input.authorizationQualifier,
                GSResponsibleAgencyCode: input.responsibleAgencyCode,
                AutomationSend: input.send == 'Send' ? 'true' : 'false',
                ResendMaxAttempts: input.resendIntervalMaximumAttachments,
                ISAUsageIndicator: input.usageIndicator,
                CheckPendingInterval: 1,
                UseConnectionPool: 'false',
                GSDateFormat: input.dateFormat,
                XMLSuffix: 3, //input.suffix,
                AutomationMaxAttempts: 1,
                // ISAReceiverIdQualifier: 'ZZ',
                MaxWorkers: 1,
                //  SendToPort: 'Amazon_REST',
                // ISASenderIdQualifier: 'ZZ',
                ISAControlStandardsIdentifier: input.controlStandardsIdentifier,
                //ISAReceiverIdentifier: 'CLG',
                AllowSpaceCharAsComponentDelimiter: 'False',
                StrictSchemaValidation: input.strictSchemaValidation,
                // ISASenderIdentifier: 'AMZ',
                AllowShortISA: 'False',
                PortType: 'X12',
                ISA11IsRepetitionSeparator: 'True',
                Encoding: input.encoding, //'utf - 8',
                RecurseSubDirectories: 'False',
                EDISuffix: 0,
                OutputFormat: 'raw',
                LogLevel: 'Info',
                workspace: workspaceName,
                DataElementSeparator: input.dataElementSeparator, //'*',
                SegmentTerminator: input.segmentTerminator, //'~',
                SaveToSentFolder:
                  input.saveToSentFolder == 'Save to Sent folder'
                    ? 'true'
                    : 'false',
                // TranslationType: 'X12ToXML',
                RequestTA1ForFunctionalACK: 'False',
                MaskSensitive: 'true',
                BatchOutputSize: 0,
                ApplyIdentifiersToFunctionalGroup: 'true',
                GSGroupIdentifierCode: 004010,
                FunctionalAckExpected:
                  input.functionalACK == 'Functional ACK expected'
                    ? 'true'
                    : 'false',
                GSTimeFormat: input.timeFormat,
                ElementRefById: 'False',
                IncludeXMLComments: 'True',
                ComponentElementSeparator: input.componentElementSeparator, //':',
                LogMessages:
                  input.logMessages == 'Log messages' ? 'true' : 'false',
                ISAControlVersionNumber: input.controlVersionNumber, //00401,
                FunctionalAckType: input.functionalACKType, //997,
                TA1Ack: input.ta1Acknowledgement,
              };

              console.log('postdata', postdata);
              await axios({
                method: 'post',
                url: config.db.ARCESB_URL + 'connectors',
                data: postdata,
                headers: {
                  'x-arcesb-authtoken': config.db.arcesbHeader,
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*',
                },
              })
                .then(async function (response) {
                  //console.log(response);
                  console.log('RESULT', response.data);

                  if (response.status === 200) {
                    //REST CALL
                    var restPostData = {
                      workspace: workspaceName,
                      PortType: 'REST',
                      ConnectorId: input.connectorId + '_REST',
                      Method: 'POST',
                      Url:
                        'https://activemq.arcesb.changemaker.community/api/message/xmlActivemqArcesb?type=queue',
                      Authorization: 'Basic',
                      UserName: 'admin',
                      Password: 'admin',
                      BodyType: 'raw',
                      RawContentType: 'application/xml',
                    };
                    console.log(postdata);
                    await axios({
                      method: 'post',
                      url: config.db.ARCESB_URL + 'connectors',
                      data: restPostData,
                      headers: {
                        'x-arcesb-authtoken': config.db.arcesbHeader,
                        'Content-Type': 'application/json',
                        'Access-Control-Allow-Origin': '*',
                      },
                    })
                      .then(function (restResponse) {
                        console.log(restResponse);
                        var status;

                        if (restResponse.status === 200) {
                          status = 'Active';
                        } else {
                          status = 'Inactive';
                        }
                        var sql =
                          'UPDATE `x12connector_arcesb` SET `APIStatus`=' +
                          mysql.escape(status) +
                          ' WHERE X12ConnectorUUID=' +
                          mysql.escape(input.X12ConnectorUUID);
                        console.log(sql);
                        connection.query(sql, function (err, updatedStatus) {
                          if (err) reject(err);
                          else
                            resolve({
                              statusAPI: 'success',
                              msg: 'X12 Connector Sync Successfully',
                            });
                        });
                      })
                      .catch((error) => {
                        console.log(error);
                        reject(error);
                      });
                  }
                })
                .catch((error) => {
                  console.log(error);
                  reject(error);
                });
            }
          }
        );
      });
    },
  },
  Mutation: {
    async createX12Connector(root, { input }) {
      console.log(input);
      //sql query for create X12Connector
      return new Promise((resolve, reject) => {
        var sql =
          'INSERT INTO x12connector_arcesb ( connectorId, connectorDescription, transactionType, profilesenderIdentifier, senderIdQualifier, receiverIdQualifier, profilereceiverIdentifier, controlVersionNumber, usageIndicator, functionalGroup, ta1Acknowledgement, functionalACK, functionalACKType, send, resendInterval, resendIntervalMaximumAttachments, dataElementSeparator, componentElementSeparator, segmentTerminator, suffix, authorizationQualifier, authorizationId, securityQualifier, securityPassword, controlStandardsIdentifier, senderIdentifier, receiverIdentifier, dateFormat, timeFormat, responsibleAgencyCode, identifierCode, encoding, generateDescriptionAs, hippaSchemas, hippa278, nestLoops, parentConnector, processingDelay, snipValidation, strictSchemaValidation, localFileScheme, logMessages, sendFilter, saveToSentFolder,batchTransactions,useReferenceId,expandQualifierValues,othersettingsFuntionalACKS,workspaceUUID) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

        var values = [
          input.connectorId,
          input.connectorDescription,
          input.transactionType,
          input.profilesenderIdentifier,
          input.senderIdQualifier,
          input.receiverIdQualifier,
          input.profilereceiverIdentifier,
          input.controlVersionNumber,
          input.usageIndicator,
          input.functionalGroup,
          input.ta1Acknowledgement,
          input.functionalACK,
          input.functionalACKType,
          input.send,
          input.resendInterval,
          input.resendIntervalMaximumAttachments,
          input.dataElementSeparator,
          input.componentElementSeparator,
          input.segmentTerminator,
          input.suffix,
          input.authorizationQualifier,
          input.authorizationId,
          input.securityQualifier,
          input.securityPassword,
          input.controlStandardsIdentifier,
          input.senderIdentifier,
          input.receiverIdentifier,
          input.dateFormat,
          input.timeFormat,
          input.responsibleAgencyCode,
          input.identifierCode,
          input.encoding,
          input.generateDescriptionAs,
          input.hippaSchemas,
          input.hippa278,
          input.nestLoops,
          input.parentConnector,
          input.processingDelay,
          input.snipValidation,
          input.strictSchemaValidation,
          input.localFileScheme,
          input.logMessages,
          input.sendFilter,
          input.saveToSentFolder,
          input.batchTransactions,
          input.useReferenceId,
          input.expandQualifierValues,
          input.othersettingsFuntionalACKS,
          input.workspaceUUID,
        ];
        console.log(values);
        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
            console.log(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async updateX12Connector(root, { input }) {
      console.log(input);
      //sql query for update X12Connector
      return new Promise((resolve, reject) => {
        var sql =
          'UPDATE x12connector_arcesb SET  connectorId=?, connectorDescription=?, transactionType=?, profilesenderIdentifier=?, senderIdQualifier=?, receiverIdQualifier=?, profilereceiverIdentifier=?, controlVersionNumber=?, usageIndicator=?, functionalGroup=?, ta1Acknowledgement=?, functionalACK=?, functionalACKType=?, send=?, resendInterval=?, resendIntervalMaximumAttachments=?, dataElementSeparator=?, componentElementSeparator=?, segmentTerminator=?, suffix=?, authorizationQualifier=?, authorizationId=?, securityQualifier=?, securityPassword=?, controlStandardsIdentifier=?, senderIdentifier=?, receiverIdentifier=?, dateFormat=?, timeFormat=?, responsibleAgencyCode=?, identifierCode=?, encoding=?, generateDescriptionAs=?, hippaSchemas=?, hippa278=?, nestLoops=?, parentConnector=?, processingDelay=?, snipValidation=?, strictSchemaValidation=?, localFileScheme=?, logMessages=?, sendFilter=?, saveToSentFolder=?,batchTransactions=?, useReferenceId=?,expandQualifierValues=?,othersettingsFuntionalACKS=?,workspaceUUID=?, updatedOn=? WHERE X12ConnectorUUID = ?';

        var values = [
          input.connectorId,
          input.connectorDescription,
          input.transactionType,
          input.profilesenderIdentifier,
          input.senderIdQualifier,
          input.receiverIdQualifier,
          input.profilereceiverIdentifier,
          input.controlVersionNumber,
          input.usageIndicator,
          input.functionalGroup,
          input.ta1Acknowledgement,
          input.functionalACK,
          input.functionalACKType,
          input.send,
          input.resendInterval,
          input.resendIntervalMaximumAttachments,
          input.dataElementSeparator,
          input.componentElementSeparator,
          input.segmentTerminator,
          input.suffix,
          input.authorizationQualifier,
          input.authorizationId,
          input.securityQualifier,
          input.securityPassword,
          input.controlStandardsIdentifier,
          input.senderIdentifier,
          input.receiverIdentifier,
          input.dateFormat,
          input.timeFormat,
          input.responsibleAgencyCode,
          input.identifierCode,
          input.encoding,
          input.generateDescriptionAs,
          input.hippaSchemas,
          input.hippa278,
          input.nestLoops,
          input.parentConnector,
          input.processingDelay,
          input.snipValidation,
          input.strictSchemaValidation,
          input.localFileScheme,
          input.logMessages,
          input.sendFilter,
          input.saveToSentFolder,
          input.batchTransactions,
          input.useReferenceId,
          input.expandQualifierValues,
          input.othersettingsFuntionalACKS,
          input.workspaceUUID,

          new Date(),
          input.X12ConnectorUUID,
        ];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async removeX12Connector(root, { input }) {
      console.log(input);
      //sql query for remove X12Connector
      return new Promise((resolve, reject) => {
        var sql =
          'DELETE FROM `x12connector_arcesb` WHERE `X12ConnectorUUID` = ?;';

        var values = [input.X12ConnectorUUID];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
};
const readStream = async (stream) => {
  stream.setEncoding('base64');

  return new Promise((resolve, reject) => {
    let data = '';

    // eslint-disable-next-line no-return-assign
    stream.on('data', (chunk) => (data += chunk));
    stream.on('end', () => {
      resolve(data);
      console.log(data);
    });
    stream.on('error', (error) => {
      reject(error);
      console.log(error.stack);
    });
  });
};
const X12ConnectorSchemaArcesb = makeExecutableSchema({
  typeDefs,
  resolvers,
  formatError: (err) => {
    // Don't give the specific errors to the client.
    if (err.message.startsWith('Database Error: ')) {
      return new Error('sql error');
    }

    // Otherwise return the original error.  The error can also
    // be manipulated in other ways, so long as it's returned.
    return err;
  },
});

module.exports = X12ConnectorSchemaArcesb;
