var { makeExecutableSchema } = require('graphql-tools');
const config = require('../dbConfig');
const mysql = require('mysql');
const withAuthfn = require('../verifyMiddleware/withAuth');
const axios = require('axios');
var connection = require('../database'); /*= mysql.createPool({
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database,
  port: config.db.port,
});*/ /*function handleDisconnect() {
  connection = mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database,
    port: config.db.port,
  }); // Recreate the connection, since
  // the old one cannot be reused.

  connection.connect(function (err) {
    // The server is either down
    if (err) {
      // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    } // to avoid a hot loop, and to allow our node script to
  }); // process asynchronous requests in the meantime.
  // If you're also serving http, display a 503 error.
  connection.on('error', function (err) {
    console.log('db error', err);
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      // Connection to the MySQL server is usually
      handleDisconnect(); // lost due to either server restart, or a
    } else {
      // connnection idle timeout (the wait_timeout
      throw err; // server variable configures this)
    }
  });
}

handleDisconnect();*/

/*var MySQLStore = require('connect-mysql'); // mysql session store
var options = {
      config: {
        host: config.db.host,
        user: config.db.user,
        password: config.db.password,
        database: config.db.database,
        port: config.db.port,
      },
      pool:true,
      keepalive:true

    };

    connection=MySQLStore(options)*/ //connection.connect();
const typeDefs = `
scalar Date

type connectorValues{
  idConnectors:String
  connectorsUUID:String
  connectorID:String
  workspaceUUID:String
  workspaceName:String
  ConnectorType:String
  ParentConnector:String
  createdOn:Date
  updatedOn:Date
}

type transactionFlow{
    Status:String
}
type transactionStatus{
  statusAPI:String
  msg:String
}
type workFlowValues{
  workFlowName:String
  workspaceUUID:String
  workFlowUUID:String
  APIStatus:String
  ResendWorkflow:String
  createdOn:Date
  updatedOn:Date
}

input conectorInput{
  connectorsUUID:String
  connectorID:String
  workspaceUUID:String
  ConnectorType:String
  ParentConnector:String
}

input transactionInput{
  workFlowUUID:String
  workSpaceName:String
  transactionType:String
  senderIdentifier:String
  workFlowName:String
  workspaceUUID:String
  content:String
  APIStatus:String
  ResendWorkflow:String
}

input workflowInput{
  workFlowName:String
  workspaceUUID:String
}



type Query{
    fetchConnectorsAPI(input:conectorInput):connectorValues
    allConnectors:[connectorValues]
    postConnectorsAPI(input:conectorInput):connectorValues
    fetchConnectorsByWorkspaceUUID(workspaceuuid:String):[connectorValues]
    flowAPI825_1(input:transactionInput):transactionFlow
    flowAPI850_1(input:transactionInput):transactionFlow
    flowAPI825(input:transactionInput):transactionFlow
    flowAPI850(input:transactionInput):workFlowValues
    TestService(input:transactionInput):transactionStatus
    fetchWorkSpaceValues(workspaceuuid:String):workFlowValues
    fetchWorkFlowByWorkspace(workspaceUUID:String):[workFlowValues]
    flowAPI850_X12(input:transactionInput):transactionStatus
    flowAPI850_XMLMap(input:transactionInput):transactionStatus
    flowAPI850_JSON(input:transactionInput):transactionStatus
    flowAPI850_REST(input:transactionInput):transactionStatus
    flowAPI850_File(input:transactionInput):transactionStatus
    flowAPI850_Send(input:transactionInput):transactionStatus
}

type Mutation{
  createConnectorsAPI(input:conectorInput) :connectorValues
  createWorkFlow(input:workflowInput):workFlowValues
  removeWorkFlow(input:transactionInput):workFlowValues
}
`;

const resolvers = {
  Query: {
    async fetchWorkFlowByWorkspace(root, { workspaceUUID }) {
      return new Promise((resolve, reject) => {
        var sql, values;
        sql =
          ' select `idworkFlow`, `workFlowUUID`,workFlowName,APIStatus,ResendWorkflow,workFlowUUID,workspaceUUID,(SELECT `workspaceName` FROM `workspace_arcesb` WHERE `workspaceUUID`=`workflow_arcesb`.`workspaceUUID`) as `workspaceName`, `createdOn`, `updatedOn` from `workflow_arcesb` where `workspaceUUID`=? ORDER BY `createdOn` DESC  ';
        values = [workspaceUUID];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },

    async TestService(root, { input }) {
      return new Promise(async (resolve, reject) => {
        console.log('input', input);
        /* var transactionTypeval = input.transactionType.replace(' ', '');
        console.log(
          'input',
          input.workSpaceName + '_' + transactionTypeval + '_Inbound'
        );*/
        console.log(input.content);
        const encodedString = Buffer.from(input.content).toString('base64');
        console.log(encodedString);
        var postdata = {
          ConnectorId: input.workFlowName,
          Folder: 'Send',
          Filename: '850FromAmazone.edi',
          Content: encodedString,
        };

        /*var postdata = {
                "Workspaceid": "ArcesbTest"
                
              };*/
        console.log(postdata);
        await axios({
          method: 'post',
          url: config.db.ARCESB_URL + 'files',
          data: postdata,
          headers: {
            'x-arcesb-authtoken': config.db.arcesbHeader,
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
          },
        })
          .then(function (response) {
            //console.log(response);
            console.log(response.data);

            if (response.data != null) {
              resolve({
                statusAPI: 'success',
                msg: 'Transaction Processed Successfully',
              });
            }
          })
          .catch((error) => {
            console.log(error);
            reject(error);
          });
      });
    },
    async flowAPI825(root, { input }) {
      return new Promise(async (resolve, reject) => {
        console.log('input', input);
        var transactionTypeval = input.transactionType.replace(' ', '');
        console.log(
          'input',
          input.workSpaceName + '_' + transactionTypeval + '_Inbound',
          input.senderIdentifier
        );
        var postdata = {
          workspace: input.workSpaceName,
          PortType: 'X12',
          ConnectorId:
            input.workSpaceName + '_' + transactionTypeval + '_Inbound',
          ISAReceiverIdQualifier: 'ZZ',
          ISASenderIdQualifier: 'ZZ',
          ISAReceiverIdentifier: 'CLG',
          ISASenderIdentifier: input.senderIdentifier,
          TranslationType: 'X12ToXML',
          SendToPort: 'Map_Transaction_Log',
        };
        /*var postdata = {
        "Workspaceid": "ArcesbTest"
        
      };*/
        // console.log(postdata);
        await axios({
          method: 'post',
          url: config.db.ARCESB_URL + 'connectors',
          data: postdata,
          headers: {
            'x-arcesb-authtoken': config.db.arcesbHeader,
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
          },
        }).then(function (response) {
          //console.log(response);
          console.log(response.data);
        });
        var postdata1 = {
          workspace: input.workSpaceName,
          PortType: 'XMLMap',
          ConnectorId: 'Map_Transaction_Log',
          Source: '/opt/files/EDIdata850.xml',
          Destination: '/opt/files/transactionLogXML.xml',
          SendToPort: 'TransactionLogJSON',
        };
        /*var postdata = {
      "Workspaceid": "ArcesbTest"
      
    };*/
        // console.log(postdata1);
        await axios({
          method: 'post',
          url: config.db.ARCESB_URL + 'connectors',
          data: postdata1,
          headers: {
            'x-arcesb-authtoken': config.db.arcesbHeader,
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
          },
        }).then(function (response) {
          //console.log(response);
          console.log(response.data);
        });
        var postdata2 = {
          ConnectorId: 'TransactionLogJSON',
          PortType: 'JSON',
          workspace: input.workSpaceName,
          SendToPort: 'elasticsearchconnector',
        };
        /*var postdata = {
    "Workspaceid": "ArcesbTest"

    };*/
        // console.log(postdata2);
        await axios({
          method: 'post',
          url: config.db.ARCESB_URL + 'connectors',
          data: postdata2,
          headers: {
            'x-arcesb-authtoken': config.db.arcesbHeader,
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
          },
        }).then(function (response) {
          //console.log(response);
          console.log(response.data);
        });
        var postdata3 = {
          workspace: input.workSpaceName,
          PortType: 'REST',
          ConnectorId: 'elasticsearchconnector',
          Method: 'POST',
          Url: 'https://elastic.arcesb.changemaker.community/arcesb/test',
          Authorization: 'Basic',
          UserName: 'elastic',
          Password: 'elastic',
          BodyType: 'raw',
          RawContentType: 'application/json',
        };
        /*var postdata = {
        "Workspaceid": "ArcesbTest"
        
      };*/
        // console.log(postdata3);
        await axios({
          method: 'post',
          url: config.db.ARCESB_URL + 'connectors',
          data: postdata3,
          headers: {
            'x-arcesb-authtoken': config.db.arcesbHeader,
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
          },
        }).then(function (response) {
          //console.log(response);
          console.log(response.data);
        });
      });
    },

    async flowAPI850(root, { input }) {
      return new Promise(async (resolve, reject) => {
        console.log(input);
        connection.query(
          'INSERT INTO `workflow_arcesb`(  `workFlowName`, `workspaceUUID`) VALUES ( ' +
            mysql.escape(input.workFlowName) +
            ',' +
            mysql.escape(input.workspaceUUID) +
            ') ',
          function (err, post) {
            if (err) {
              reject(err);
            } else {
              resolve(post);
            }
          }
        );
      });
    },
    async flowAPI850_X12(root, { input }) {
      return new Promise(async (resolve, reject) => {
        var transactionTypeval = input.transactionType;
        console.log(input.transactionType);
        console.log(
          'input',
          input.workSpaceName + '_' + transactionTypeval + '_Inbound',
          input.senderIdentifier
        );

        var postdata = {
          workspace: input.workSpaceName,
          PortType: 'X12',
          ConnectorId:
            input.workSpaceName + '_' + transactionTypeval + '_Inbound',
          ISAReceiverIdQualifier: 'ZZ',
          ISASenderIdQualifier: 'ZZ',
          ISAReceiverIdentifier: 'CLG',
          ISASenderIdentifier: input.senderIdentifier,
          TranslationType: 'X12ToXML',
          SendToPort:
            'Map_' +
            input.workSpaceName +
            '_' +
            transactionTypeval +
            '_Transaction_Log',
        };
        await axios({
          method: 'post',
          url: config.db.ARCESB_URL + 'connectors',
          data: postdata,
          headers: {
            'x-arcesb-authtoken': config.db.arcesbHeader,
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
          },
        })
          .then(function (response) {
            console.log(response);
            console.log(response.data);
            if (response.status == 200 && response.data != null) {
              resolve({
                statusAPI: 'success',
                msg: 'Transaction Created successfully',
              });
            }
          })
          .catch((error) => {
            console.log('ERROR CATCHE ', error);
            //UPDATE Resend workflow value
            var sql =
              'UPDATE `workflow_arcesb` SET ResendWorkflow=' +
              mysql.escape(1) +
              ' WHERE workFlowName=' +
              mysql.escape(input.workFlowName);
            console.log(sql);

            connection.query(sql, function (err, updatedStatus) {
              console.log(updatedStatus);
              if (err) reject(err);
              else reject(error);
            });
          });
      });
    },
    async flowAPI850_XMLMap(root, { input }) {
      return new Promise(async (resolve, reject) => {
        var transactionTypeval = input.transactionType;
        console.log(
          'input',
          input.workSpaceName + '_' + transactionTypeval + '_Inbound',
          input.senderIdentifier
        );

        var postdata1 = {
          workspace: input.workSpaceName,
          PortType: 'XMLMap',
          ConnectorId:
            'Map_' +
            input.workSpaceName +
            '_' +
            transactionTypeval +
            '_Transaction_Log',
          Source: '/opt/files/EDIdata850.xml',
          Destination: '/opt/files/transactionLogXML.xml',
          SendToPort:
            'TransactionLogJSON_' +
            input.workSpaceName +
            '_' +
            transactionTypeval,
        };
        axios({
          method: 'post',
          url: config.db.ARCESB_URL + 'connectors',
          data: postdata1,
          headers: {
            'x-arcesb-authtoken': config.db.arcesbHeader,
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
          },
        })
          .then(function (response) {
            //console.log(response);
            console.log(response.data);
            if (response.data != null) {
              resolve({
                statusAPI: 'success',
                msg: 'Trransaction Created successfully',
              });
            }
          })
          .catch((error) => {
            console.log(error);
            var sql =
              'UPDATE `workflow_arcesb` SET ResendWorkflow=' +
              mysql.escape(2) +
              ' WHERE workFlowName=' +
              mysql.escape(input.workFlowName);
            console.log(sql);

            connection.query(sql, function (err, updatedStatus) {
              console.log(updatedStatus);
              if (err) reject(err);
              else reject(error);
            });
            //reject(error);
          });
      });
    },
    async flowAPI850_JSON(root, { input }) {
      return new Promise(async (resolve, reject) => {
        var transactionTypeval = input.transactionType;
        console.log(
          'input',
          input.workSpaceName + '_' + transactionTypeval + '_Inbound',
          input.senderIdentifier
        );
        var postdata2 = {
          ConnectorId:
            'TransactionLogJSON_' +
            input.workSpaceName +
            '_' +
            transactionTypeval,
          PortType: 'JSON',
          workspace: input.workSpaceName,
          SendToPort:
            'elasticsearchconnector_' +
            input.workSpaceName +
            '_' +
            transactionTypeval,
        };
        axios({
          method: 'post',
          url: config.db.ARCESB_URL + 'connectors',
          data: postdata2,
          headers: {
            'x-arcesb-authtoken': config.db.arcesbHeader,
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
          },
        })
          .then(function (response) {
            //console.log(response);
            console.log(response.data);
            if (response.data != null) {
              resolve({
                statusAPI: 'success',
                msg: 'Trransaction Created successfully',
              });
            }
          })
          .catch((error) => {
            // console.log(error);
            var sql =
              'UPDATE `workflow_arcesb` SET ResendWorkflow=' +
              mysql.escape(3) +
              ' WHERE workFlowName=' +
              mysql.escape(input.workFlowName);
            console.log(sql);

            connection.query(sql, function (err, updatedStatus) {
              console.log(updatedStatus);
              if (err) reject(err);
              else reject(error);
            });
            // reject(error);
          });
      });
    },
    async flowAPI850_REST(root, { input }) {
      return new Promise(async (resolve, reject) => {
        var transactionTypeval = input.transactionType;
        console.log(
          'input',
          input.workSpaceName + '_' + transactionTypeval + '_Inbound',
          input.senderIdentifier
        );
        var postdata3 = {
          workspace: input.workSpaceName,
          PortType: 'REST',
          ConnectorId:
            'elasticsearchconnector_' +
            input.workSpaceName +
            '_' +
            transactionTypeval,
          Method: 'POST',
          Url: 'https://elastic.arcesb.changemaker.community/arcesbtest/test',
          Authorization: 'Basic',
          UserName: 'elastic',
          Password: 'elastic',
          BodyType: 'raw',
          RawContentType: 'application/json',
        };
        axios({
          method: 'post',
          url: config.db.ARCESB_URL + 'connectors',
          data: postdata3,
          headers: {
            'x-arcesb-authtoken': config.db.arcesbHeader,
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
          },
        })
          .then(function (response) {
            //console.log(response);
            console.log(response.data);
            if (response.data != null) {
              resolve({
                statusAPI: 'success',
                msg: 'Trransaction Created successfully',
              });
            }
          })
          .catch((error) => {
            console.log(error);
            var sql =
              'UPDATE `workflow_arcesb` SET ResendWorkflow=' +
              mysql.escape(4) +
              ' WHERE workFlowName=' +
              mysql.escape(input.workFlowName);
            console.log(sql);

            connection.query(sql, function (err, updatedStatus) {
              console.log(updatedStatus);
              if (err) reject(err);
              else reject(error);
            });
            //reject(error);
          });
      });
    },
    async flowAPI850_File(root, { input }) {
      return new Promise(async (resolve, reject) => {
        var transactionTypeval = input.transactionType;
        console.log(
          'input',
          input.workSpaceName + '_' + transactionTypeval + '_Inbound',
          input.senderIdentifier
        );
        var connectorIDValue =
          input.workSpaceName + '_File_' + transactionTypeval;
        var path =
          '/opt/arcesb/workspaces/' +
          input.workSpaceName +
          '/' +
          'Map_' +
          input.workSpaceName +
          '_' +
          transactionTypeval +
          '_Transaction_Log';

        var postdata4 = {
          workspace: input.workSpaceName,
          ConnectorId: connectorIDValue,
          PortType: 'File',
          Path: path,
        };
        axios({
          method: 'post',
          url: config.db.ARCESB_URL + 'connectors',
          data: postdata4,
          headers: {
            'x-arcesb-authtoken': config.db.arcesbHeader,
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
          },
        })
          .then(function (response) {
            //console.log(response);
            console.log(response.data);
            if (response.data != null) {
              resolve({
                statusAPI: 'success',
                msg: 'Trransaction Created successfully',
              });
            }
          })
          .catch((error) => {
            console.log(error);
            var sql =
              'UPDATE `workflow_arcesb` SET ResendWorkflow=' +
              mysql.escape(5) +
              ' WHERE workFlowName=' +
              mysql.escape(input.workFlowName);
            console.log(sql);

            connection.query(sql, function (err, updatedStatus) {
              console.log(updatedStatus);
              if (err) reject(err);
              else reject(error);
            });
            //  reject(error);
          });
      });
    },
    async flowAPI850_Send(root, { input }) {
      return new Promise(async (resolve, reject) => {
        var transactionTypeval = input.transactionType;
        console.log(
          'input',
          input.workSpaceName + '_' + transactionTypeval + '_Inbound',
          input.senderIdentifier
        );
        var connectorIDValue =
          input.workSpaceName + '_File_' + transactionTypeval;
        var path =
          '/opt/arcesb/workspaces/' +
          input.workSpaceName +
          '/' +
          'Map_' +
          input.workSpaceName +
          '_' +
          transactionTypeval +
          '_Transaction_Log';
        var postdata5 = {
          ConnectorId: connectorIDValue,
          Folder: 'Send',
          Filename: 'Map.json',
          Content:
            'W3sibmFtZSI6InRyYW5zYWN0aW9uIiwiZm9yZWFjaCI6Ii9JbnRlcmNoYW5nZSIsImNoaWxkcmVuIjpbeyJuYW1lIjoiQXBwbGljYXRpb24iLCJjaGlsZHJlbiI6W3sibmFtZSI6IkVudmlyb25tZW50VHlwZSIsInZhbHVlIjoiREVWIn0seyJuYW1lIjoiTG9nVHlwZSIsInZhbHVlIjoiQjJCIn0seyJuYW1lIjoiT3JnYW5pc2F0aW9uIiwidmFsdWUiOiJDbG91ZEdlbiJ9LHsibmFtZSI6IlN5c3RlbSIsInZhbHVlIjoiQXJjRVNCIn1dfSx7Im5hbWUiOiJFcnJvckluZm8iLCJjaGlsZHJlbiI6W3sibmFtZSI6IkVycm9yRGV0YWlscyIsImNoaWxkcmVuIjpbeyJuYW1lIjoiZWxlbWVudCIsImNoaWxkcmVuIjpbeyJuYW1lIjoiRXJyb25Ob3RpZmljYXRpb24ifSx7Im5hbWUiOiJFcnJvckNvbW1lbnQifSx7Im5hbWUiOiJFcnJvckR1bXAifSx7Im5hbWUiOiJFcnJvcklEIn0seyJuYW1lIjoiRXJyb3JMb2NhdGlvbiJ9LHsibmFtZSI6IkVycm9yTWVzc2FnZSJ9LHsibmFtZSI6IkVycm9yU3RhdHVzIn0seyJuYW1lIjoiRXJyb3JUeXBlIn1dfV19XX0seyJuYW1lIjoiSW50ZXJmYWNlRGV0YWlscyIsImNoaWxkcmVuIjpbeyJuYW1lIjoiQnVzaW5lc3NLZXlQZXJmb3JtYW5jZVZhbHVlcyIsImNoaWxkcmVuIjpbeyJuYW1lIjoiZWxlbWVudCIsImNoaWxkcmVuIjpbeyJuYW1lIjoiS2V5UGVyZm9ybWFuY2VJbmRpY2F0b3IxIn0seyJuYW1lIjoiS2V5UGVyZm9ybWFuY2VJbmRpY2F0b3IyIn0seyJuYW1lIjoiS2V5UGVyZm9ybWFuY2VJbmRpY2F0b3IzIn0seyJuYW1lIjoiS2V5UGVyZm9ybWFuY2VJbmRpY2F0b3I0In0seyJuYW1lIjoiS2V5UGVyZm9ybWFuY2VJbmRpY2F0b3I1In0seyJuYW1lIjoiS2V5UGVyZm9ybWFuY2VLZXlOYW1lMSJ9LHsibmFtZSI6IktleVBlcmZvcm1hbmNlS2V5TmFtZTIifSx7Im5hbWUiOiJLZXlQZXJmb3JtYW5jZUtleU5hbWUzIn0seyJuYW1lIjoiS2V5UGVyZm9ybWFuY2VLZXlOYW1lNCJ9LHsibmFtZSI6IktleVBlcmZvcm1hbmNlS2V5TmFtZTUifV19XX0seyJuYW1lIjoiSW50ZXJmYWNlSUQiLCJ2YWx1ZSI6IlBPXzAwMSJ9LHsibmFtZSI6IkludGVyZmFjZU1vZHVsZSIsInZhbHVlIjoiUHVyY2hhc2VPcmRlciJ9LHsibmFtZSI6IkludGVyZmFjZVR5cGUiLCJ2YWx1ZSI6IkluYm91bmQifSx7Im5hbWUiOiJSZWdpb24ifSx7Im5hbWUiOiJTb3VyY2VTeXN0ZW1JRCIsInZhbHVlIjoiQW1hem9uIn0seyJuYW1lIjoiU291cmNlU3lzdGVtVHJhbklEIn0seyJuYW1lIjoiVGFyZ2V0U3lzdGVtSUQiLCJ2YWx1ZSI6IkNsb3VkR2VuIn0seyJuYW1lIjoiVGVjaG5pY2FsS2V5VmFsdWVzIiwiY2hpbGRyZW4iOlt7Im5hbWUiOiJlbGVtZW50IiwiY2hpbGRyZW4iOlt7Im5hbWUiOiJUcmFuc2FjdGlvbktleU5hbWUifSx7Im5hbWUiOiJUcmFuc2FjdGlvbktleVJlZmVyZW5jZSJ9LHsibmFtZSI6IlRyYW5zYWN0aW9uS2V5VmFsdWUifV19XX1dfSx7Im5hbWUiOiJUcmFuc2FjdGlvbkluZm8iLCJjaGlsZHJlbiI6W3sibmFtZSI6IlRyYW5zYXRpb25EZXRhaWxzIiwiY2hpbGRyZW4iOlt7Im5hbWUiOiJlbGVtZW50IiwiY2hpbGRyZW4iOlt7Im5hbWUiOiJQYXlsb2FkIiwiY2hpbGRyZW4iOlt7Im5hbWUiOiJQYXlsb2FkIiwidmFsdWUiOnsic2NyaXB0IjoiPGFyYzppbmZvIHRpdGxlPVwiQ3VzdG9tIFNjcmlwdFwiIGRlc2M9XCJUaGUgY3VzdG9tIHNjcmlwdC5cIj5cbiAgPGlucHV0IG5hbWU9XCJGaWxlUGF0aFwiICAgICAgICBkZXNjPVwiVGhlIHNlbmRpbmcgZmlsZSBwYXRoLlwiIC8+XG4gIDxpbnB1dCBuYW1lPVwiWFBhdGhcIiAgICAgICAgICAgZGVzYz1cIlRoZSBjdXJyZW50IHhwYXRoIGluIHRoZSBsb29wLlwiIC8+XG4gIDxpbnB1dCBuYW1lPVwiKlwiICAgICAgICAgICAgICAgZGVzYz1cIlRoZSBpbmZvcm1hdGlvbiBvZiB0aGUgY3VycmVudCBsb29wLlwiIC8+XG48L2FyYzppbmZvPlxuXG48YXJjOnNldCBhdHRyPVwiZmlsZVwiIHZhbHVlPVwiW0ZpbGVQYXRoXVwiPjwvYXJjOnNldD5cbjxhcmM6Y2FsbCBvcD1cImZpbGVSZWFkXCIgPlxuPGFyYzpzZXQgYXR0cj1cImZpbGUuZGF0YVwiIHZhbHVlPVwiW19vdXQxLmRhdGFdXCIgLz5cbjwvYXJjOmNhbGw+XG48YXJjOnNldCBhdHRyPVwicmVzdWx0LnRleHRcIiB2YWx1ZT1cIltmaWxlLmRhdGF8c3Vic3RyaW5nKFtmaWxlLmRhdGF8ZmluZCgmbHQ7KV0pXVwiPjwvYXJjOnNldD4ifX0seyJuYW1lIjoiU291cmNlVHlwZSJ9LHsibmFtZSI6IlNvdXJjZVR5cGVJRCJ9XX0seyJuYW1lIjoiUmVsYXRpb25UcmFuc2FjdGlvbklEIiwiY2hpbGRyZW4iOlt7Im5hbWUiOiJlbGVtZW50IiwiY2hpbGRyZW4iOlt7Im5hbWUiOiJDaGlsZElEIn0seyJuYW1lIjoiUGFyZW50VHJhbnNhY3Rpb25JRCJ9XX1dfV19XX1dfV19XQ==',
        };
        axios({
          method: 'post',
          url: config.db.ARCESB_URL + 'files',
          data: postdata5,
          headers: {
            'x-arcesb-authtoken': config.db.arcesbHeader,
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
          },
        })
          .then(function (response) {
            //console.log(response);
            console.log('mapJSON response........', response.data);
            if (response.data != null) {
              console.log(
                'input',
                input.workSpaceName + '_' + transactionTypeval + '_Inbound',
                input.senderIdentifier
              );
              axios({
                method: 'Delete',
                url:
                  config.db.ARCESB_URL +
                  'connectors(' +
                  input.workSpaceName +
                  '_File_' +
                  transactionTypeval +
                  ')',
                headers: {
                  'x-arcesb-authtoken': config.db.arcesbHeader,
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*',
                },
              }).then(function (response) {
                //console.log(response);
                console.log('delete connecctor..............', response.data);
                //update DB resend work flow Value to 0
                var sql =
                  'UPDATE `workflow_arcesb` SET ResendWorkflow=' +
                  mysql.escape(0) +
                  ',APIStatus=' +
                  mysql.escape('Active') +
                  ' WHERE workFlowName=' +
                  mysql.escape(input.workFlowName);
                console.log(sql);

                connection.query(sql, function (err, updatedStatus) {
                  console.log(updatedStatus);
                  if (err) reject(err);
                  else {
                    if (response.data != null) {
                      resolve({
                        statusAPI: 'success',
                        msg: 'Trransaction Created successfully',
                      });
                    }
                  }
                });
              });
              /*  resolve({
                statusAPI: 'success',
                msg: 'Trransaction Created successfully',
              });*/
            }
          })
          .catch((error) => {
            console.log(error);
            var sql =
              'UPDATE `workflow_arcesb` SET ResendWorkflow=' +
              mysql.escape(6) +
              ' WHERE workFlowName=' +
              mysql.escape(input.workFlowName);
            console.log(sql);

            connection.query(sql, function (err, updatedStatus) {
              console.log(updatedStatus);
              if (err) reject(err);
              else reject(error);
            });
            //reject(error);
          });
      });
    },
    async fetchConnectorsAPI(root, { input }) {
      return new Promise((resolve, reject) => {
        var connectorURL =
          config.db.ARCESB_URL + 'connectors(' + input.connectorID + ')';
        axios(connectorURL, {
          method: 'GET',
          headers: {
            'x-arcesb-authtoken': config.db.arcesbHeader,
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
          },
        }).then(function (response) {
          console.log(response);
          console.log(response.data);
        });
      });
    },
    async postConnectorsAPI(root, { input }) {
      return new Promise((resolve, reject) => {
        var postdata = {
          '@odata.type': 'ArcESBAPI.Ports',
          ConnectorId: input.connectorID,
          Workspace: input.workspaceUUID,
          ConnectorType: input.ConnectorType,
        };

        axios(config.db.ARCESB_URL + 'connectors', {
          method: 'POST',
          processData: false,
          data: JSON.stringify(postdata),
          headers: {
            'x-arcesb-authtoken': config.db.arcesbHeader,
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
          },
        }).then(function (response) {
          console.log(response);
          console.log(response.data);
        });
      });
    },
    async fetchConnectorsByWorkspaceUUID(root, { workspaceuuid }) {
      return new Promise((resolve, reject) => {
        var sql, values;
        sql =
          ' select `idConnectors`, `connectorsUUID`,ConnectorType,connectorID,ParentConnector, workspaceUUID,(SELECT `workspaceName` FROM `workspace_arcesb` WHERE `workspaceUUID`=`connectors_arcesb`.`workspaceUUID`) as `workspaceName`, `createdOn`, `updatedOn` from `connectors_arcesb` where `workspaceUUID`=? ORDER BY `createdOn` DESC  ';
        values = [workspaceuuid];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    allConnectors: (root, args, context, info) => {
      //sql query for allConnectors
      return new Promise((resolve, reject) => {
        connection.query(
          'SELECT * from connectors_arcesb ORDER BY  createdOn DESC ',
          function (err, post) {
            if (err) {
              reject(err);
            } else {
              resolve(post);
            }
          }
        );
      });
    },
  },
  Mutation: {
    createConnectorsAPI(root, { input }) {
      return new Promise((resolve, reject) => {
        console.log(input);
        var sql =
          'INSERT INTO connectors_arcesb (connectorID,workspaceUUID,ConnectorType,ParentConnector) VALUES(?,?,?,?)';

        var values = [
          input.connectorID,
          input.workspaceUUID,
          input.ConnectorType,
          input.ParentConnector,
        ];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async removeWorkFlow(root, { input }) {
      console.log(input);
      //sql query for create workspace
      return new Promise((resolve, reject) => {
        var sql = 'DELETE FROM `workflow_arcesb` WHERE `workFlowUUID` = ?;';

        var values = [input.workFlowUUID];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
};

const connectorsSchema = makeExecutableSchema({
  typeDefs,
  resolvers,
  formatError: (err) => {
    // Don't give the specific errors to the client.
    if (err.message.startsWith('Database Error: ')) {
      return new Error('sql error');
    }

    // Otherwise return the original error.  The error can also
    // be manipulated in other ways, so long as it's returned.
    return err;
  },
});

module.exports = connectorsSchema;
