var { makeExecutableSchema } = require('graphql-tools');
const config = require('../dbConfig');
const mysql = require('mysql');
const withAuthfn = require('../verifyMiddleware/withAuth');
const axios = require('axios');
var connection = require('../database');

/*var MySQLStore = require('connect-mysql'); // mysql session store
var options = {
      config: {
        host: config.db.host,
        user: config.db.user,
        password: config.db.password,
        database: config.db.database,
        port: config.db.port,
      },
      pool:true,
      keepalive:true

    };

    connection=MySQLStore(options)*/ /*= mysql.createPool({
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database,
  port: config.db.port,
});*/
/*/*function handleDisconnect() {
  connection = mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database,
    port: config.db.port,
  }); // Recreate the connection, since
  // the old one cannot be reused.

  connection.connect(function (err) {
    // The server is either down
    if (err) {
      // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    } // to avoid a hot loop, and to allow our node script to
  }); // process asynchronous requests in the meantime.
  // If you're also serving http, display a 503 error.
  connection.on('error', function (err) {
    console.log('db error', err);
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      // Connection to the MySQL server is usually
      handleDisconnect(); // lost due to either server restart, or a
    } else {
      // connnection idle timeout (the wait_timeout
      throw err; // server variable configures this)
    }
  });
}

handleDisconnect();*/

//connection.connect();
const typeDefs = `
scalar Date

type DocumentType{
  idDocumentType:String
  documentTypeUUID:String
  name:String
  docType:String
  description:String
  APIStatus:String
  createdOn:Date
  lastUpdatedOn:Date
}

input DocumentTypeInput{
    idDocumentType:String
    documentTypeUUID:String
    name:String
    docType:String
    APIStatus:String
    description:String
}
type docTypeAPISuccess{
  statusAPI:String
  msg:String
}

type Query{
  DocumnetTypeAPIArcesb(input:DocumentTypeInput):docTypeAPISuccess
   allDocumentTypeArcesb:[DocumentType]
   fetchDocumentTypeArcesb(searchInput:String): [DocumentType]
}

type Mutation{
   createDocumentTypeArcesb(input:DocumentTypeInput): DocumentType
   updateDocumentTypeArcesb(input:DocumentTypeInput): DocumentType
   removeDocumentTypeArcesb(input:DocumentTypeInput):DocumentType
   updateDocumentStatusByNameArcesb(input:DocumentTypeInput): DocumentType
}

`;

const resolvers = {
  Query: {
    async DocumnetTypeAPIArcesb(root, { input }) {
      console.log('search', input);
      return new Promise((resolve, reject) => {
        var jsonData = {
          documentType: {
            name: input.name,
            description: input.description,
            type: input.docType,
          },
        };
        console.log('jsonData', jsonData);
        axios({
          method: 'post',
          //url: config.db.BASE_URL+'rest/CG_EDI_TMS_SYNC/webservices/restws/provider/processingRule',
          url: config.db.BASE_URL + 'restv2/documentType',
          data: jsonData,
        }).then(function (response) {
          console.log('response', response);
          console.log('status', response.data.response);
          // resolve({success:response.data.response.msg})
          var status;
          if (response.data.response.status === 'success') {
            status = 'Active';
          } else {
            status = 'Inactive';
          }
          connection.query(
            'UPDATE `documenttype_arcesb` SET `APIStatus`=' +
              mysql.escape(status) +
              ' WHERE `documentTypeUUID`=' +
              mysql.escape(input.documentTypeUUID),
            function (err, updateDocumentStatus) {
              if (err) reject(err);
              else
                resolve({
                  statusAPI: response.data.response.status,
                  msg: response.data.response.msg,
                });
            }
          );
        });
        //resolve(true)
      });
    },
    allDocumentTypeArcesb: (root, args, context, info) => {
      //sql query for fetch all DocumentType
      return new Promise((resolve, reject) => {
        connection.query(
          'SELECT * from documenttype_arcesb ORDER BY  createdOn DESC ',
          function (err, post) {
            if (err) {
              reject(err);
            } else {
              resolve(post);
            }
          }
        );
      });
    },
    async fetchDocumentTypeArcesb(root, { searchInput }) {
      //sql query for fetch DocumentType
      /*if(withAuthfn()){console.log("true")}else{
        console.log("false")
      }*/

      return new Promise((resolve, reject) => {
        var sql, values;
        if (searchInput == '') {
          sql = 'Select * from documenttype_arcesb ORDER BY  createdOn DESC ';
          console.log(sql);
        } else {
          var sql =
            ' Select * from documenttype_arcesb where (name= ' +
            mysql.escape(searchInput) +
            'OR description=' +
            mysql.escape(searchInput) +
            ') ORDER BY  createdOn DESC ';
        }

        connection.query(sql, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
  Mutation: {
    async createDocumentTypeArcesb(root, { input }) {
      console.log(input);
      //sql query for create DocumentType
      return new Promise((resolve, reject) => {
        var sql =
          'INSERT INTO documenttype_arcesb (name,description,docType) VALUES(?,?,?)';

        var values = [input.name, input.description, input.docType];
        console.log(values);
        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async updateDocumentTypeArcesb(root, { input }) {
      console.log(input);
      //sql query for update DocumentType
      return new Promise((resolve, reject) => {
        var sql =
          'UPDATE documenttype_arcesb SET name = ?,description=?,docType=?,lastUpdatedOn=? WHERE documentTypeUUID = ?';

        var values = [
          input.name,
          input.description,
          input.docType,
          new Date(),
          input.documentTypeUUID,
        ];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async removeDocumentTypeArcesb(root, { input }) {
      console.log(input);
      //sql query for create userTypes
      return new Promise((resolve, reject) => {
        var sql =
          'DELETE FROM `documenttype_arcesb` WHERE `documentTypeUUID` = ?;';

        var values = [input.documentTypeUUID];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async updateDocumentStatusByNameArcesb(root, { input }) {
      console.log(input);
      //sql query for update DocumentType
      return new Promise((resolve, reject) => {
        var sql = 'UPDATE documenttype_arcesb SET APIStatus=? WHERE name = ?';

        var values = [input.APIStatus, input.name];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
};

const DocumentTypeSchemaArcesb = makeExecutableSchema({
  typeDefs,
  resolvers,
  formatError: (err) => {
    // Don't give the specific errors to the client.
    if (err.message.startsWith('Database Error: ')) {
      return new Error('sql error');
    }

    // Otherwise return the original error.  The error can also
    // be manipulated in other ways, so long as it's returned.
    return err;
  },
});

module.exports = DocumentTypeSchemaArcesb;
