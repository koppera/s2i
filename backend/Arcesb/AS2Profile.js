var { makeExecutableSchema } = require('graphql-tools');
const config = require('../dbConfig');
const mysql = require('mysql');
const withAuthfn = require('../verifyMiddleware/withAuth');
const axios = require('axios');
var connection = require('../database'); /*function handleDisconnect() {
  connection = mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database,
    port: config.db.port,
  }); // Recreate the connection, since
  // the old one cannot be reused.

  connection.connect(function (err) {
    // The server is either down
    if (err) {
      // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    } // to avoid a hot loop, and to allow our node script to
  }); // process asynchronous requests in the meantime.
  // If you're also serving http, display a 503 error.
  connection.on('error', function (err) {
    console.log('db error', err);
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      // Connection to the MySQL server is usually
      handleDisconnect(); // lost due to either server restart, or a
    } else {
      // connnection idle timeout (the wait_timeout
      throw err; // server variable configures this)
    }
  });
}

handleDisconnect();*/ /*= mysql.createPool({
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database,
  port: config.db.port,
});*/ //connection.connect();

/*var MySQLStore = require('connect-mysql'); // mysql session store
var options = {
      config: {
        host: config.db.host,
        user: config.db.user,
        password: config.db.password,
        database: config.db.database,
        port: config.db.port,
      },
      pool:true,
      keepalive:true

    };

    connection=MySQLStore(options)*/ const typeDefs = `
scalar Date

type AS2ProfileValues{
  idas2Profile:String, 
  as2ProfileUUID:String, 
  connectorID:String, 
  connectorDescription:String, 
  as2Identifier:String, 
  partnerURL:String, 
  sendMessageSecurity:String, 
  receiveMessageSecurity:String, 
  requestMDNReceipt:String, 
  security:String, 
  delivery:String,
  encryptionCertificate:String, 
  streaming:String, 
  TLSServerCertificate:String, 
  as2Restart:String, 
  as2Reliability:String, 
  inputFolder:String, 
  outputFolder:String, 
  processedFolder:String, 
  localAs2Identifier:String, 
  profilePrivateCertificate:String, 
  profilecertificatePassword:String, 
  userProfileSettings:String,
  TLSprivateCertificate:String, 
  TLScertificatePassword:String, 
  HTTPAuthenticationType:String, 
  HTTPAuthentication:String, 
  user:String, 
  password:String,  
  headersName:String, 
  headersValue:String, 
  maxWorkers:String, 
  maxFiles:String, 
  asyncMDNTimeout:String, 
  duplicateFileAction:String, 
  duplicateFileInterval:String, 
  encryptionAlgorithm:String, 
  extensionMap:String, 
  hTTPSubject:String, 
  logLevel:String, 
  logRequests:String, 
  messageId:String, 
  parentConnector:String, 
  parseFDAExtensions:String, 
  partnerSigningCertificate:String, 
  processingDelay:String, 
  signatureAlgorithm:String, 
  TLSenabledProtocols:String, 
  tempReceiveDirectory:String, 
  logMessages:String, 
  sendFilter:String, 
  savetoSentFolder:String, 
  workspaceUUID:String, 
  APIStatus:String,
  createdOn:Date,
  updatedOn:Date

}
type as2profileStatus{
  statusAPI:String
  msg:String
}
input AS2ProfileInput{   
  as2ProfileUUID:String, 
  connectorID:String, 
  connectorDescription:String, 
  as2Identifier:String, 
  partnerURL:String, 
  sendMessageSecurity:String, 
  receiveMessageSecurity:String, 
  requestMDNReceipt:String, 
  security:String, 
  delivery:String,
  encryptionCertificate:String, 
  streaming:String, 
  TLSServerCertificate:String, 
  as2Restart:String, 
  as2Reliability:String, 
  inputFolder:String, 
  outputFolder:String, 
  processedFolder:String, 
  localAs2Identifier:String, 
  profilePrivateCertificate:String, 
  profilecertificatePassword:String, 
  userProfileSettings:String,
  TLSprivateCertificate:String, 
  TLScertificatePassword:String, 
  HTTPAuthenticationType:String, 
  HTTPAuthentication:String, 
  user:String, 
  password:String,  
  headersName:String, 
  headersValue:String, 
  maxWorkers:String, 
  maxFiles:String, 
  asyncMDNTimeout:String, 
  duplicateFileAction:String, 
  duplicateFileInterval:String, 
  encryptionAlgorithm:String, 
  extensionMap:String, 
  hTTPSubject:String, 
  logLevel:String, 
  logRequests:String, 
  messageId:String, 
  parentConnector:String, 
  parseFDAExtensions:String, 
  partnerSigningCertificate:String, 
  processingDelay:String, 
  signatureAlgorithm:String, 
  TLSenabledProtocols:String, 
  tempReceiveDirectory:String, 
  logMessages:String, 
  sendFilter:String, 
  savetoSentFolder:String, 
  workspaceUUID:String, 
  APIStatus:String,
  updatedOn:Date
}
type Query{
   allAS2Profile:[AS2ProfileValues]
   fetchProfileAPI:[AS2ProfileValues]
   createAS2ProfileAPI:[AS2ProfileValues]
   postAs2ProfileAPI(input:AS2ProfileInput):as2profileStatus
}

type Mutation{
   createAS2Profile(input:AS2ProfileInput): AS2ProfileValues
   updateAS2Profile(input:AS2ProfileInput): AS2ProfileValues
   removeAS2Profile(input:AS2ProfileInput): AS2ProfileValues
}


`;

const resolvers = {
  Query: {
    fetchProfileAPI(root, { input }) {
      return new Promise((resolve, reject) => {
        axios(config.db.ARCESB_URL + 'profile', {
          method: 'GET',
          headers: {
            'x-arcesb-authtoken': config.db.arcesbHeader,
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
          },
        }).then(function (response) {
          console.log(response);
          console.log(response.data);
        });
      });
    },
    createAS2ProfileAPI(root, { input }) {
      return new Promise((resolve, reject) => {
        var putdata = {
          '@odata.type': 'ArcESBAPI.Profile',
          HeartbeatInterval: 1,
          NotifyEmail: 'NotifyEmail_1',
          NotifyEmailFrom: 'NotifyEmailFrom_1',
        };

        axios(
          config.db.ARCESB_URL + 'profile?@authtoken=' + config.db.arcesbHeader,
          {
            method: 'PUT',
            processData: false,
            data: JSON.stringify(putdata),
            headers: {
              'x-arcesb-authtoken': config.db.arcesbHeader,
              'Content-Type': 'application/json',
              'Access-Control-Allow-Origin': '*',
            },
          }
        ).then(function (response) {
          console.log(response);
          console.log(response.data);
        });
      });
    },
    allAS2Profile: (root, args, context, info) => {
      //sql query for fetch as2profile
      return new Promise((resolve, reject) => {
        connection.query(
          'SELECT * from as2profile_arcesb ORDER BY  createdOn DESC ',
          function (err, post) {
            if (err) {
              reject(err);
            } else {
              resolve(post);
            }
          }
        );
      });
    },
    postAs2ProfileAPI(root, { input }) {
      console.log(input);

      return new Promise(async (resolve, reject) => {
        connection.query(
          'SELECT workspaceName FROM workspace_arcesb where workspaceUUID=' +
            mysql.escape(input.workspaceUUID),
          async function (errS, resultS) {
            if (errS) {
              reject(errS);
            } else {
              let workspaceName = resultS[0].workspaceName;
              console.log(workspaceName);
              var postdata = {
                Workspace: workspaceName,
                ConnectorId: workspaceName + '_Inbound_' + input.connectorID,
                AS2Identifier: input.as2Identifier,
                URL: input.partnerURL,
                UseEncryption: 'true',
                UseSigning: 'true',
                UseCompression: 'false',
                ConnectionTimeout: '60',
                Certificate: 'tms_clientcert.p7b',
                SSLCertificate: '*',
                HTTPEnableChunkEncoding:
                  input.streaming != null ? 'true' : 'false',
                UseAS2Restart: input.as2Restart != null ? 'true' : 'false',
                EnableReliability:
                  input.as2Reliability != null ? 'true' : 'false',
                CertUseManagedSecurityAPI: 'false',
                ProxyType: 'None',
                MDNRequested: 'true',
                RequireEncryption: 'true',
                CheckPendingInterval: '1',
                UseConnectionPool: 'false',
                SSLEnabledProtocols: input.TLSenabledProtocols,
                DecodeQuotedPrintable: 'false',
                HTTPAuthenticationType: input.HTTPAuthenticationType,
                LogRequests: input.logRequests != null ? 'true' : 'false',
                InvalidFilenameMdnAction: 'Continue',
                PortType: 'AS2',
                MDNDeliveryType:
                  input.delivery == 'Synchronous'
                    ? 'synchronous'
                    : 'asynchronous',
                RecurseSubDirectories: 'False',
                OutputFormat: 'raw',
                RequireSignature: 'true',
                LogLevel: 'Info',
                UsePSS: 'false',
                AutomationRetryInterval: '60',
                SaveToSentFolder:
                  input.savetoSentFolder != null ? 'true' : 'false',
                ProxyUseGlobal: 'true',
                SSLClientUsePersonal: 'false',
                MDNSigned: 'true',
                SafeInterval: '0',
                InvalidFilenameMdnInterval: '1440',
                MaskSensitive: 'true',
                EncryptionAlgorithm: input.encryptionAlgorithm,
                ParseFDAExtensions: input.parseFDAExtensions ? 'true' : 'false',
                MoveFileAfterSend: 'true',
                LogMessages: input.logMessages != null ? 'true' : 'false',
                UseHTTPAuthentication: 'false',
                SendFilenameInContentType: 'false',
                SignatureAlgorithm: input.signatureAlgorithm,
                UseOAEP: 'false',
                ProxyAuthscheme: 'none',
                AsyncMDNTimeout: input.asyncMDNTimeout,
              };

              console.log(postdata);
              await axios({
                method: 'post',
                url: config.db.ARCESB_URL + 'connectors',
                data: postdata,
                headers: {
                  'x-arcesb-authtoken': config.db.arcesbHeader,
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*',
                },
              })
                .then(function (response) {
                  //console.log(response);
                  console.log('RESULT', response.data);
                  var status;

                  if (response.status === 200) {
                    status = 'Active';
                  } else {
                    status = 'Inactive';
                  }
                  console.log(input.workspaceUUID);
                  var sql =
                    'UPDATE `as2profile_arcesb` SET `APIStatus`=' +
                    mysql.escape(status) +
                    ' WHERE as2ProfileUUID=' +
                    mysql.escape(input.as2ProfileUUID);
                  console.log(sql);
                  connection.query(sql, function (err, updatedStatus) {
                    if (err) reject(err);
                    else
                      resolve({
                        statusAPI: 'success',
                        msg: 'AS2Profile Sync Successfully',
                      });
                  });
                })
                .catch((error) => {
                  console.log(error);
                  reject(error);
                });
            }
          }
        );
      });
    },
  },
  Mutation: {
    async createAS2Profile(root, { input }) {
      //sql query for create ApplicationURL

      return new Promise((resolve, reject) => {
        var sql =
          'INSERT INTO as2profile_arcesb (connectorID, connectorDescription, as2Identifier, partnerURL, sendMessageSecurity, receiveMessageSecurity, requestMDNReceipt, security, delivery, encryptionCertificate,TLSServerCertificate, streaming, as2Restart, as2Reliability,inputFolder, outputFolder, processedFolder, localAs2Identifier, profilePrivateCertificate, profilecertificatePassword, userProfileSettings, TLSprivateCertificate, TLScertificatePassword, HTTPAuthenticationType, user, password, headersName, headersValue, maxWorkers, maxFiles, asyncMDNTimeout, duplicateFileAction, duplicateFileInterval, encryptionAlgorithm, extensionMap, hTTPSubject, logLevel, logRequests, messageId, parentConnector, parseFDAExtensions, partnerSigningCertificate, processingDelay, signatureAlgorithm, TLSenabledProtocols, tempReceiveDirectory, logMessages, sendFilter, savetoSentFolder,HTTPAuthentication,workspaceUUID) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

        var values = [
          input.connectorID,
          input.connectorDescription,
          input.as2Identifier,
          input.partnerURL,
          input.sendMessageSecurity,
          input.receiveMessageSecurity,
          input.requestMDNReceipt,
          input.security,
          input.delivery,
          input.encryptionCertificate,
          input.TLSServerCertificate,
          input.streaming,
          input.as2Restart,
          input.as2Reliability,
          input.inputFolder,
          input.outputFolder,
          input.processedFolder,
          input.localAs2Identifier,
          input.profilePrivateCertificate,
          input.profilecertificatePassword,
          input.userProfileSettings,
          input.TLSprivateCertificate,
          input.TLScertificatePassword,
          input.HTTPAuthenticationType,
          input.user,
          input.password,
          input.headersName,
          input.headersValue,
          input.maxWorkers,
          input.maxFiles,
          input.asyncMDNTimeout,
          input.duplicateFileAction,
          input.duplicateFileInterval,
          input.encryptionAlgorithm,
          input.extensionMap,
          input.hTTPSubject,
          input.logLevel,
          input.logRequests,
          input.messageId,
          input.parentConnector,
          input.parseFDAExtensions,
          input.partnerSigningCertificate,
          input.processingDelay,
          input.signatureAlgorithm,
          input.TLSenabledProtocols,
          input.tempReceiveDirectory,
          input.logMessages,
          input.sendFilter,
          input.savetoSentFolder,
          input.HTTPAuthentication,
          input.workspaceUUID,
        ];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async updateAS2Profile(root, { input }) {
      console.log(input);
      //sql query for update DocumentType
      return new Promise((resolve, reject) => {
        var sql =
          'UPDATE as2profile_arcesb SET connectorID=?, connectorDescription=?, as2Identifier=?, partnerURL=?, sendMessageSecurity=?, receiveMessageSecurity=?, requestMDNReceipt=?, security=?, delivery=?, encryptionCertificate=?, TLSServerCertificate=?,streaming=?, as2Restart=?, as2Reliability=?, inputFolder=?, outputFolder=?, processedFolder=?, localAs2Identifier=?, profilePrivateCertificate=?, profilecertificatePassword=?,userProfileSettings=?,TLSprivateCertificate=?, TLScertificatePassword=?, HTTPAuthenticationType=?, user=?, password=?, headersName=?, headersValue=?, maxWorkers=?, maxFiles=?, asyncMDNTimeout=?, duplicateFileAction=?, duplicateFileInterval=?, encryptionAlgorithm=?, extensionMap=?, hTTPSubject=?, logLevel=?, logRequests=?, messageId=?, parentConnector=?, parseFDAExtensions=?, partnerSigningCertificate=?, processingDelay=?, signatureAlgorithm=?, TLSenabledProtocols=?, tempReceiveDirectory=?, logMessages=?, sendFilter=?, savetoSentFolder=? ,HTTPAuthentication=? ,workspaceUUID=?, updatedOn=? WHERE as2ProfileUUID=?';
        console.log(sql);
        var values = [
          input.connectorID,
          input.connectorDescription,
          input.as2Identifier,
          input.partnerURL,
          input.sendMessageSecurity,
          input.receiveMessageSecurity,
          input.requestMDNReceipt,
          input.security,
          input.delivery,
          input.encryptionCertificate,
          input.TLSServerCertificate,
          input.streaming,
          input.as2Restart,
          input.as2Reliability,
          input.inputFolder,
          input.outputFolder,
          input.processedFolder,
          input.localAs2Identifier,
          input.profilePrivateCertificate,
          input.profilecertificatePassword,
          input.userProfileSettings,
          input.TLSprivateCertificate,
          input.TLScertificatePassword,
          input.HTTPAuthenticationType,
          input.user,
          input.password,
          input.headersName,
          input.headersValue,
          input.maxWorkers,
          input.maxFiles,
          input.asyncMDNTimeout,
          input.duplicateFileAction,
          input.duplicateFileInterval,
          input.encryptionAlgorithm,
          input.extensionMap,
          input.hTTPSubject,
          input.logLevel,
          input.logRequests,
          input.messageId,
          input.parentConnector,
          input.parseFDAExtensions,
          input.partnerSigningCertificate,
          input.processingDelay,
          input.signatureAlgorithm,
          input.TLSenabledProtocols,
          input.tempReceiveDirectory,
          input.logMessages,
          input.sendFilter,
          input.savetoSentFolder,
          input.HTTPAuthentication,
          input.workspaceUUID,
          new Date(),
          input.as2ProfileUUID,
        ];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async removeAS2Profile(root, { input }) {
      console.log(input);
      //sql query for create userTypes
      return new Promise((resolve, reject) => {
        var sql = 'DELETE FROM `as2profile_arcesb` WHERE `as2ProfileUUID` = ?;';

        var values = [input.as2ProfileUUID];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
};

const applicationUrlSchema = makeExecutableSchema({
  typeDefs,
  resolvers,
  formatError: (err) => {
    // Don't give the specific errors to the client.
    if (err.message.startsWith('Database Error: ')) {
      return new Error('sql error');
    }

    // Otherwise return the original error.  The error can also
    // be manipulated in other ways, so long as it's returned.
    return err;
  },
});

module.exports = applicationUrlSchema;
