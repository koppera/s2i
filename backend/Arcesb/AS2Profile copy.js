var { makeExecutableSchema } = require('graphql-tools');
const config = require('../dbConfig');
const mysql = require('mysql');
const withAuthfn = require('../verifyMiddleware/withAuth');
const axios = require('axios');
var connection = require('../database');/*= mysql.createPool({
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database,
  port: config.db.port,
});*/
/*function handleDisconnect() {
  connection = mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database,
    port: config.db.port,
  }); // Recreate the connection, since
  // the old one cannot be reused.

  connection.connect(function (err) {
    // The server is either down
    if (err) {
      // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    } // to avoid a hot loop, and to allow our node script to
  }); // process asynchronous requests in the meantime.
  // If you're also serving http, display a 503 error.
  connection.on('error', function (err) {
    console.log('db error', err);
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      // Connection to the MySQL server is usually
      handleDisconnect(); // lost due to either server restart, or a
    } else {
      // connnection idle timeout (the wait_timeout
      throw err; // server variable configures this)
    }
  });
}

handleDisconnect();*/
//connection.connect();
const typeDefs = `
scalar Date

type AS2ProfileValues{
    idAs2profile:String
    as2profileUUID:String
    aS2Identifier:String
    rolloverCertificatePassword:String
    certificatePassword:String
    rolloverPrivatecertificate:String
    privateCertificate:String
    publicDomain:String
    asynchronousMDNURL:String
    receivingURL:String
    publicURL:String
    publicCertificate:String
    createdOn:Date
    updatedOn:Date
}

input AS2ProfileInput{

    idAs2profile:String
    as2profileUUID:String
    aS2Identifier:String
    rolloverCertificatePassword:String
    certificatePassword:String
    rolloverPrivatecertificate:String
    privateCertificate:String
    publicDomain:String
    asynchronousMDNURL:String
    receivingURL:String
    publicURL:String
    publicCertificate:String
   
}
type Query{
   allAS2Profile:[AS2ProfileValues]
   fetchProfileAPI:[AS2ProfileValues]
   createAS2ProfileAPI:[AS2ProfileValues]
}

type Mutation{
   createAS2Profile(input:AS2ProfileInput): AS2ProfileValues
   updateAS2Profile(input:AS2ProfileInput): AS2ProfileValues
   removeAS2Profile(input:AS2ProfileInput): AS2ProfileValues
}


`;

const resolvers = {
  Query: {
    fetchProfileAPI(root, { input }) {
      return new Promise((resolve, reject) => {
        axios(config.db.ARCESB_URL + 'profile', {
          method: 'GET',
          headers: {
            'x-arcesb-authtoken': config.db.arcesbHeader,
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
          },
        }).then(function (response) {
          console.log(response);
          console.log(response.data);
        });
      });
    },
    createAS2ProfileAPI(root, { input }) {
      return new Promise((resolve, reject) => {
        var putdata = {
          '@odata.type': 'ArcESBAPI.Profile',
          HeartbeatInterval: 1,
          NotifyEmail: 'NotifyEmail_1',
          NotifyEmailFrom: 'NotifyEmailFrom_1',
        };

        axios(
          config.db.ARCESB_URL + 'profile?@authtoken=' + config.db.arcesbHeader,
          {
            method: 'PUT',
            processData: false,
            data: JSON.stringify(putdata),
            headers: {
              'x-arcesb-authtoken': config.db.arcesbHeader,
              'Content-Type': 'application/json',
              'Access-Control-Allow-Origin': '*',
            },
          }
        ).then(function (response) {
          console.log(response);
          console.log(response.data);
        });
      });
    },
    allAS2Profile: (root, args, context, info) => {
      //sql query for fetch as2profile
      return new Promise((resolve, reject) => {
        connection.query(
          'SELECT * from as2profile_arcesb ORDER BY  createdOn DESC ',
          function (err, post) {
            if (err) {
              reject(err);
            } else {
              resolve(post);
            }
          }
        );
      });
    },
  },
  Mutation: {
    async createAS2Profile(root, { input }) {
      //sql query for create ApplicationURL

      return new Promise((resolve, reject) => {
        var sql =
          'INSERT INTO as2profile_arcesb (aS2Identifier,rolloverCertificatePassword,certificatePassword,rolloverPrivatecertificate,privateCertificate, publicDomain, asynchronousMDNURL,receivingURL,publicURL,publicCertificate) VALUES(?,?,?,?,?,?,?,?,?,?)';

        var values = [
          input.aS2Identifier,
          input.rolloverCertificatePassword,
          input.certificatePassword,
          input.rolloverPrivatecertificate,
          input.privateCertificate,
          input.publicDomain,
          input.asynchronousMDNURL,
          input.receivingURL,
          input.publicURL,
          input.publicCertificate,
        ];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async updateAS2Profile(root, { input }) {
      console.log(input);
      //sql query for update DocumentType
      return new Promise((resolve, reject) => {
        var sql =
          'UPDATE as2profile_arcesb SET aS2Identifier = ?,rolloverCertificatePassword=?,certificatePassword=?,rolloverPrivatecertificate=? ,privateCertificate=?,publicDomain=?,asynchronousMDNURL=? ,receivingURL=?,publicURL=? ,publicCertificate=?,updatedOn=? WHERE as2profileUUID = ?';

        var values = [
          input.aS2Identifier,
          input.rolloverCertificatePassword,
          input.certificatePassword,
          input.rolloverPrivatecertificate,
          input.privateCertificate,
          input.publicDomain,
          input.asynchronousMDNURL,
          input.receivingURL,
          input.publicURL,
          input.publicCertificate,
          new Date(),
          input.as2profileUUID,
        ];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async removeAS2Profile(root, { input }) {
      console.log(input);
      //sql query for create userTypes
      return new Promise((resolve, reject) => {
        var sql = 'DELETE FROM `as2profile_arcesb` WHERE `as2profileUUID` = ?;';

        var values = [input.as2profileUUID];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
};

const applicationUrlSchema = makeExecutableSchema({
  typeDefs,
  resolvers,
  formatError: (err) => {
    // Don't give the specific errors to the client.
    if (err.message.startsWith('Database Error: ')) {
      return new Error('sql error');
    }

    // Otherwise return the original error.  The error can also
    // be manipulated in other ways, so long as it's returned.
    return err;
  },
});

module.exports = applicationUrlSchema;
