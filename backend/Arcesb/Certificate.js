var { makeExecutableSchema } = require('graphql-tools');
const config = require('../dbConfig');
const mysql = require('mysql');
const withAuthfn = require('../verifyMiddleware/withAuth');
var connection = require('../database');

/*var MySQLStore = require('connect-mysql'); // mysql session store
var options = {
      config: {
        host: config.db.host,
        user: config.db.user,
        password: config.db.password,
        database: config.db.database,
        port: config.db.port,
      },
      pool:true,
      keepalive:true

    };

    connection=MySQLStore(options)*/ /*= mysql.createPool({
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database,
  port: config.db.port,
});*/
/*function handleDisconnect() {
  connection = mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database,
    port: config.db.port,
  }); // Recreate the connection, since
  // the old one cannot be reused.

  connection.connect(function (err) {
    // The server is either down
    if (err) {
      // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    } // to avoid a hot loop, and to allow our node script to
  }); // process asynchronous requests in the meantime.
  // If you're also serving http, display a 503 error.
  connection.on('error', function (err) {
    console.log('db error', err);
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      // Connection to the MySQL server is usually
      handleDisconnect(); // lost due to either server restart, or a
    } else {
      // connnection idle timeout (the wait_timeout
      throw err; // server variable configures this)
    }
  });
}

handleDisconnect();*/
//connection.connect();
const typeDefs = `
scalar Date

type CertificateValues{
    idCertificate:String
    certificateUUID:String
    commonName:String
    organization:String
    fileName:String
    serialNumber:String
    cPassword:String
    validPeriod:String
    keySize:String
    publicKeyType:String
    signatureAlgorithm:String
    createdOn:Date
    lastUpdatedOn:Date
}

input CertificateInput{

    idCertificate:String
    certificateUUID:String
    commonName:String
    organization:String
    fileName:String
    serialNumber:String
    cPassword:String
    validPeriod:String
    keySize:String
    publicKeyType:String
    signatureAlgorithm:String
   
   
}
type Query{
   allCertificate:[CertificateValues]

}

type Mutation{
   createCertificate(input:CertificateInput): CertificateValues
  
  
}


`;

const resolvers = {
  Query: {
    allCertificate: (root, args, context, info) => {
      //sql query for fetch all certificate
      return new Promise((resolve, reject) => {
        connection.query(
          'SELECT * from certificate_arcesb ORDER BY  createdOn DESC ',
          function (err, post) {
            if (err) {
              reject(err);
            } else {
              resolve(post);
            }
          }
        );
      });
    },
  },
  Mutation: {
    async createCertificate(root, { input }) {
      //sql query for create certificate

      return new Promise((resolve, reject) => {
        var sql =
          'INSERT INTO certificate_arcesb (commonName,organization,fileName,serialNumber,cPassword,validPeriod,keySize,publicKeyType,signatureAlgorithm) VALUES(?,?,?,?,?,?,?,?,?)';

        var values = [
          input.commonName,
          input.organization,
          input.fileName,
          input.serialNumber,
          input.cPassword,
          input.validPeriod,
          input.keySize,
          input.publicKeyType,
          input.signatureAlgorithm,
        ];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
};

const certificateSchema = makeExecutableSchema({
  typeDefs,
  resolvers,
  formatError: (err) => {
    // Don't give the specific errors to the client.
    if (err.message.startsWith('Database Error: ')) {
      return new Error('sql error');
    }

    // Otherwise return the original error.  The error can also
    // be manipulated in other ways, so long as it's returned.
    return err;
  },
});

module.exports = certificateSchema;
