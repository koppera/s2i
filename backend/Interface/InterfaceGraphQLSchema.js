var { makeExecutableSchema } = require('graphql-tools');
const config = require('../dbConfig');
const mysql = require('mysql');
const withAuthfn = require('../verifyMiddleware/withAuth');
var connection = require('../database');

/*var MySQLStore = require('connect-mysql'); // mysql session store
var options = {
      config: {
        host: config.db.host,
        user: config.db.user,
        password: config.db.password,
        database: config.db.database,
        port: config.db.port,
      },
      pool:true,
      keepalive:true

    };

    connection=MySQLStore(options)*/ /*= mysql.createPool({
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database,
  port: config.db.port,
});*/
/*/*function handleDisconnect() {
  connection = mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database,
    port: config.db.port,
  }); // Recreate the connection, since
  // the old one cannot be reused.

  connection.connect(function (err) {
    // The server is either down
    if (err) {
      // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    } // to avoid a hot loop, and to allow our node script to
  }); // process asynchronous requests in the meantime.
  // If you're also serving http, display a 503 error.
  connection.on('error', function (err) {
    console.log('db error', err);
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      // Connection to the MySQL server is usually
      handleDisconnect(); // lost due to either server restart, or a
    } else {
      // connnection idle timeout (the wait_timeout
      throw err; // server variable configures this)
    }
  });
}

handleDisconnect();*/

//connection.connect();
const typeDefs = `
scalar Date

type Interface{
    idInterface:String
    interfaceUUID:String
    name:String
    description:String
    projectUUID:String
    createdOn:Date
    lastUpdatedOn:Date
    projectName:String
}

input InterfaceInput{
    idInterface:String
    interfaceUUID:String
    name:String
    description:String
    projectUUID:String
}
type Query{
   allInterface:[Interface]
   fetchInterface(searchInput:String): [Interface]
   fetchInterfaceByprojectUUID(projectUUID:String):[Interface]
   fetchInterfaceByorgUUID(orgUUID:String):[Interface]
}

type Mutation{
   createInterface(input:InterfaceInput): Interface
   updateInterface(input:InterfaceInput): Interface
   removeInterface(input:InterfaceInput):Interface
}

`;

const resolvers = {
  Query: {
    allInterface: (root, args, context, info) => {
      //sql query for fetch all Interface
      return new Promise((resolve, reject) => {
        connection.query(
          'SELECT * from interface ORDER BY  createdOn DESC ',
          function (err, post) {
            if (err) {
              reject(err);
            } else {
              resolve(post);
            }
          }
        );
      });
    },
    async fetchInterface(root, { searchInput }) {
      //sql query for fetch Interface
      /* if(withAuthfn()){console.log("true")}else{
        console.log("false")
      }*/
      return new Promise((resolve, reject) => {
        var sql, values;

        if (searchInput == '') {
          sql =
            'SELECT `idInterface`, `interfaceUUID`, `name`, `description`, `createdOn`, `lastUpdatedOn`,(SELECT `projectName` FROM `orgproject` WHERE `projectUUID`=`interface`.`projectUUID`) as `projectName`,projectUUID  from interface ORDER BY  createdOn DESC ';
          /*sql =
            'SELECT `idInterface`, `interfaceUUID`, `name`, `description`, `createdOn`, `lastUpdatedOn`,`projectUUID` from interface ORDER BY  createdOn DESC ';*/
        } else {
          sql =
            ' SELECT `idInterface`, `interfaceUUID`, `name`, `description`, `createdOn`, `lastUpdatedOn`, (SELECT `projectName` FROM `orgproject` WHERE `projectUUID`=`interface`.`projectUUID`) as `projectName`,projectUUID  from interface where (name= ' +
            mysql.escape(searchInput) +
            'OR description=' +
            mysql.escape(searchInput) +
            'OR projectUUID=' +
            mysql.escape(searchInput) +
            ')ORDER BY  createdOn DESC ';
          /*  sql =
            ' SELECT `idInterface`, `interfaceUUID`, `name`, `description`, `createdOn`, `lastUpdatedOn`, `projectUUID`  from interface where (name= ' +
            mysql.escape(searchInput) +
            'OR description=' +
            mysql.escape(searchInput) +
            'OR projectUUID=' +
            mysql.escape(searchInput) +
            ')ORDER BY  createdOn DESC ';*/
        }

        connection.query(sql, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async fetchInterfaceByprojectUUID(root, { projectUUID }) {
      //sql query for fetch Project by projectuuid

      return new Promise((resolve, reject) => {
        var sql, values;

        if (projectUUID == '') {
          sql = 'Select * from interface ORDER BY  createdOn DESC ';
        } else {
          var sql =
            ' Select * from interface where projectUUID= ' +
            mysql.escape(projectUUID) +
            'ORDER BY  createdOn DESC ';
        }

        connection.query(sql, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async fetchInterfaceByorgUUID(root, { orgUUID }) {
      //sql query for fetch interface by orgUUID

      return new Promise((resolve, reject) => {
        var sql, values;

        if (orgUUID == '') {
          sql = 'Select * from interface ORDER BY  createdOn DESC ';
        } else {
          var sql =
            ` SELECT idInterface,interfaceUUID,name,description,t.createdOn,t.lastUpdatedOn,t.projectUUID
            FROM interface t
            JOIN orgproject k 
            ON t.projectUUID=K.projectUUID
            WHERE k.organizationUUID= ` +
            mysql.escape(orgUUID) +
            `ORDER BY  t.createdOn DESC `;
        }

        connection.query(sql, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
  Mutation: {
    async createInterface(root, { input }) {
      console.log(input);
      //sql query for create Interface
      return new Promise((resolve, reject) => {
        var sql =
          'INSERT INTO interface (name,description,projectUUID) VALUES(?,?,?)';

        var values = [input.name, input.description, input.projectUUID];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async updateInterface(root, { input }) {
      console.log(input);
      //sql query for update
      return new Promise((resolve, reject) => {
        var sql =
          'UPDATE interface SET name = ?,description=? ,projectUUID=?,lastUpdatedOn=? WHERE interfaceUUID = ?';

        var values = [
          input.name,
          input.description,
          input.projectUUID,
          new Date(),
          input.interfaceUUID,
        ];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async removeInterface(root, { input }) {
      console.log(input);
      //sql query for remove Interface
      return new Promise((resolve, reject) => {
        var sql = 'DELETE FROM `interface` WHERE `interfaceUUID` = ?;';

        var values = [input.interfaceUUID];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
};

const InterfaceSchema = makeExecutableSchema({
  typeDefs,
  resolvers,
  formatError: (err) => {
    // Don't give the specific errors to the client.
    if (err.message.startsWith('Database Error: ')) {
      return new Error('sql error');
    }

    // Otherwise return the original error.  The error can also
    // be manipulated in other ways, so long as it's returned.
    return err;
  },
});

module.exports = InterfaceSchema;
