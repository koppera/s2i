var { makeExecutableSchema } = require('graphql-tools');
const config = require('../dbConfig');
const mysql = require('mysql');
const withAuthfn=require('../verifyMiddleware/withAuth');
var connection = require('../database');

/*var MySQLStore = require('connect-mysql'); // mysql session store
var options = {
      config: {
        host: config.db.host,
        user: config.db.user,
        password: config.db.password,
        database: config.db.database,
        port: config.db.port,
      },
      pool:true,
      keepalive:true

    };

    connection=MySQLStore(options)*/ /*= mysql.createPool({
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database,
  port: config.db.port,
});*/
/*/*function handleDisconnect() {
  connection = mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database,
    port: config.db.port,
  }); // Recreate the connection, since
  // the old one cannot be reused.

  connection.connect(function (err) {
    // The server is either down
    if (err) {
      // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    } // to avoid a hot loop, and to allow our node script to
  }); // process asynchronous requests in the meantime.
  // If you're also serving http, display a 503 error.
  connection.on('error', function (err) {
    console.log('db error', err);
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      // Connection to the MySQL server is usually
      handleDisconnect(); // lost due to either server restart, or a
    } else {
      // connnection idle timeout (the wait_timeout
      throw err; // server variable configures this)
    }
  });
}

handleDisconnect();*/

//connection.connect();
const typeDefs = `
scalar Date

type ProfileIDValuesTypes{
  idProfileIDValues:String
  profileIDValuesUUID:String
  profileIDTypesUUID:String
  profileIDTypeName:String
  profileIDValue:String
    partnerProfileUUID:String
    createdOn:Date
    lastUpdatedOn:Date
}

input  ProfileIDValueInput{
  idProfileIDValues:String
  profileIDValuesUUID:String
  profileIDTypesUUID:String
  profileIDValue:String
    partnerProfileUUID:String
}
type Query{
   allProfileIDValues:[ProfileIDValuesTypes]
   fetchProfileIDValuesByPartnerProfile(partnerProfileUUID:String): [ProfileIDValuesTypes]
}

type Mutation{
   createProfileIDValue(input:ProfileIDValueInput): ProfileIDValuesTypes
   updateProfileIDValues(input:ProfileIDValueInput): ProfileIDValuesTypes
   removeProfileIDValues(input:ProfileIDValueInput):ProfileIDValuesTypes
}


`;

const resolvers = {
  Query: {
    allProfileIDValues: (root, args, context, info) => {
      //sql query for fetch all profileidvalues
      return new Promise((resolve, reject) => {
        connection.query(
          'SELECT * from profileidvalues ORDER BY  createdOn DESC ',
          function (err, post) {
            if (err) {
              reject(err);
            } else {
              resolve(post);
            }
          }
        );
      });
    },
    async fetchProfileIDValuesByPartnerProfile(root, { partnerProfileUUID }) {
      //sql query for fetch profileidvalues by partner profile id

      return new Promise((resolve, reject) => {
        var sql, values;

        var sql =
          ' select `idProfileIDValues`, `profileIDValuesUUID`, profileIDTypesUUID,(SELECT `profileIDTypeName` FROM `profileidtypes` WHERE `profileIDTypesUUID`=`profileidvalues`.`profileIDTypesUUID`) as `profileIDTypeName`, `profileIDValue`, `createdOn`, `lastUpdatedOn`, `partnerProfileUUID` from `profileidvalues` where `partnerProfileUUID`=? ORDER BY `createdOn` DESC  ';
        values = [partnerProfileUUID];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
  Mutation: {
    async createProfileIDValue(root, { input }) {
      console.log(input.profileIDValue, input.partnerProfileUUID);
      //sql query for create profileidvalues
      return new Promise((resolve, reject) => {
        var fetchProfilesql =
          'SELECT * FROM profileidvalues where profileIDValue=? AND profileIDTypesUUID=? AND partnerProfileUUID=?';
        var fetchProfileValues = [
          input.profileIDValue,
          input.profileIDTypesUUID,
          input.partnerProfileUUID,
        ];
        console.log(fetchProfilesql, fetchProfileValues);
        connection.query(fetchProfilesql, fetchProfileValues, function (
          errsProfile,
          resultsProfile
        ) {
          if (errsProfile) {
            reject(errsProfile);
          } else {
            console.log(resultsProfile, resultsProfile.length);
            if (resultsProfile.length > 0) {
              reject('Combination already exist');
            } else {
              var fetchsql =
                'SELECT * FROM profileidvalues where profileIDValue=? AND partnerProfileUUID!=?';
              var fetchValues = [
                input.profileIDValue,
                input.partnerProfileUUID,
              ];
              console.log(fetchsql, fetchValues);
              connection.query(fetchsql, fetchValues, function (errs, results) {
                if (errs) {
                  reject(errs);
                } else {
                  console.log(results, results.length);
                  if (results.length > 0) {
                    reject('Combination already exist');
                  } else {
                    var sql =
                      'INSERT INTO profileidvalues (profileIDTypesUUID, profileIDValue,partnerProfileUUID ) VALUES(?,?,?)';

                    var values = [
                      input.profileIDTypesUUID,
                      input.profileIDValue,
                      input.partnerProfileUUID,
                    ];
                    console.log('sql', sql);
                    connection.query(sql, values, function (err, result) {
                      if (err) {
                        reject(err);
                      } else {
                        console.log(result);
                        resolve(result);
                      }
                    });
                  }
                  //resolve(results);
                  /*  if (results) {
                  
                  }*/
                }
              });
            }
          }
        });
      });
    },
    async updateProfileIDValues(root, { input }) {
      console.log(input);
      //sql query for update userTypes
      return new Promise((resolve, reject) => {
        var fetchProfilesql =
          'SELECT * FROM profileidvalues where profileIDValue=? AND profileIDTypesUUID=? AND partnerProfileUUID=?';
        var fetchProfileValues = [
          input.profileIDValue,
          input.profileIDTypesUUID,
          input.partnerProfileUUID,
        ];
        console.log(fetchProfilesql, fetchProfileValues);
        connection.query(fetchProfilesql, fetchProfileValues, function (
          errsProfile,
          resultsProfile
        ) {
          if (errsProfile) {
            reject(errsProfile);
          } else {
            console.log(resultsProfile, resultsProfile.length);
            if (resultsProfile.length > 0) {
              reject('Combination already exist');
            } else {
              var fetchsql =
                'SELECT * FROM profileidvalues where profileIDValue=? AND partnerProfileUUID!=?';
              var fetchValues = [
                input.profileIDValue,
                input.partnerProfileUUID,
              ];
              console.log(fetchsql, fetchValues);
              connection.query(fetchsql, fetchValues, function (errs, results) {
                if (errs) {
                  reject(errs);
                } else {
                  console.log(results, results.length);
                  if (results.length > 0) {
                    reject('Profileid value already taken');
                  } else {
                    var sql =
                      'UPDATE profileidvalues SET profileIDTypesUUID = ?,profileIDValue=?,partnerProfileUUID=?,lastUpdatedOn=? WHERE profileIDValuesUUID = ?';

                    var values = [
                      input.profileIDTypesUUID,
                      input.profileIDValue,
                      input.partnerProfileUUID,
                      new Date(),
                      input.profileIDValuesUUID,
                    ];

                    connection.query(sql, values, function (err, result) {
                      if (err) {
                        reject(err);
                      } else {
                        console.log(result);
                        resolve(result);
                      }
                    });
                  }
                }
              });
            }
          }
        });
      });
    },
    async removeProfileIDValues(root, { input }) {
      console.log(input);
      //sql query for remove removeProfileIDValues
      return new Promise((resolve, reject) => {
        var sql =
          'DELETE FROM `profileidvalues` WHERE `profileIDValuesUUID` = ?;';

        var values = [input.profileIDValuesUUID];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
};

const profileidvaluesSchema = makeExecutableSchema({
  typeDefs,
  resolvers,
  formatError: (err) => {
    // Don't give the specific errors to the client.
    if (err.message.startsWith('Database Error: ')) {
      return new Error('sql error');
    }

    // Otherwise return the original error.  The error can also
    // be manipulated in other ways, so long as it's returned.
    return err;
  },
});

module.exports = profileidvaluesSchema;
