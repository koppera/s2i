var { makeExecutableSchema } = require('graphql-tools');
const config = require('../dbConfig');
const mysql = require('mysql');
const withAuthfn=require('../verifyMiddleware/withAuth');
var connection = require('../database');

/*var MySQLStore = require('connect-mysql'); // mysql session store
var options = {
      config: {
        host: config.db.host,
        user: config.db.user,
        password: config.db.password,
        database: config.db.database,
        port: config.db.port,
      },
      pool:true,
      keepalive:true

    };

    connection=MySQLStore(options)*/ /*= mysql.createPool({
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database,
  port: config.db.port,
});*/
/*/*function handleDisconnect() {
  connection = mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database,
    port: config.db.port,
  }); // Recreate the connection, since
  // the old one cannot be reused.

  connection.connect(function (err) {
    // The server is either down
    if (err) {
      // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    } // to avoid a hot loop, and to allow our node script to
  }); // process asynchronous requests in the meantime.
  // If you're also serving http, display a 503 error.
  connection.on('error', function (err) {
    console.log('db error', err);
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      // Connection to the MySQL server is usually
      handleDisconnect(); // lost due to either server restart, or a
    } else {
      // connnection idle timeout (the wait_timeout
      throw err; // server variable configures this)
    }
  });
}

handleDisconnect();*/

//connection.connect();
const typeDefs = `
scalar Date

type ContactTypes{
  idContact:String
  contactUUID:String
    role:String
    firstName:String
    lastName:String
    email:String
    phone:String
    fax:String
    addressUUID:String
    contactTypeUUID:String
    partnerProfileUUID:String
    createdOn:Date
    lastUpdatedOn:Date
    idAddress:String
  addressNameID:String
  addressLines1:String
  addressLines2:String
  addressLines3:String
  city:String
  state:String
  country:String
  zip:String
  addressStatus:String
  addressTypeUUID:String
  addressconatctUUID:String
  contactAddressUUID:String
  contactTypeName:String
    
}

input ContactInput{

  idContact:String
  contactUUID:String
    role:String
    firstName:String
    lastName:String
    email:String
    phone:String
    fax:String
    addressUUID:String
    partnerProfileUUID:String
    contactTypeUUID:String

}
type Query{
   allContacts:[ContactTypes]
   fetchContactByPartnerProfile(partnerProfileUUID:String): [ContactTypes]
   fetchContactByPartnerProfile1(partnerProfileUUID:String): [ContactTypes]
   
}
type Mutation{
   createContact(input:ContactInput): [ContactTypes]
   updateContact(input:ContactInput): ContactTypes
   removeContact(input:ContactInput): ContactTypes
   removeAddressandaddAddressUUID(input:ContactInput): ContactTypes
   UpdateaddressUUIDOnStatus(addressUUID:String): ContactTypes
}



`;

const resolvers = {
  Query: {
    allContacts: (root, args, context, info) => {
      //sql query for fetch all contact
      return new Promise((resolve, reject) => {
        connection.query(
          'SELECT * from contact ORDER BY 	createdOn DESC',
          function (err, post) {
            if (err) {
              reject(err);
            } else {
              resolve(post);
            }
          }
        );
      });
    },
    async fetchContactByPartnerProfile1(root, { partnerProfileUUID }) {
      //sql query for fetch contact by partner profile id

      return new Promise((resolve, reject) => {
        var sql, values;
        if (partnerProfileUUID == '') {
          sql = 'SELECT * from contact ORDER BY 	createdOn DESC';
        } else {
          var sql =
            ' select  contactUUID,role,firstName,lastName, email, phone,fax,addressUUID, partnerProfileUUID,contactTypeUUID,(SELECT `name` FROM `contacttype` WHERE `contactTypeUUID`=`contact`.`contactTypeUUID`) as `contactTypeName` ,createdOn,lastUpdatedOn from contact where partnerProfileUUID=? ORDER BY 	createdOn DESC';
          //'SELECT `contact`.`role`,`contact`.`firstName`,`contact`.`lastName`,`contact`.`email`,`contact`.`phone`,`contact`.`fax`,`contact`.`addressUUID`, (`contacttype`.`name`) as `contactTypeName`,`contact`.`createdOn`,`contact`.`lastUpdatedOn`,`address`.`addressNameID`,`address`.`addressLines1`,`address`.`addressLines2`,`address`.`addressLines3`,`address`.`city`,`address`.`state`,`address`.`country`,`address`.`zip`,`address`.`addressStatus`,`address`.`addressTypeUUID` , `contact`.`contactTypeUUID` FROM `contact` JOIN `address` ON `address`.`contactUUID`=`contact`.`contactUUID` JOIN `contacttype` ON `contacttype`.`contactTypeUUID`=`contact`.`contactTypeUUID` WHERE `contact`.`partnerProfileUUID`=? UNION  SELECT `contact`.`role`,`contact`.`firstName`,`contact`.`lastName`,`contact`.`email`,`contact`.`phone`,`contact`.`fax` ,`contact`.`addressUUID`, (`contacttype`.`name`) as `contactTypeName`,`contact`.`createdOn`,`contact`.`lastUpdatedOn`,`address`.`addressNameID`,`address`.`addressLines1`,`address`.`addressLines2`,`address`.`addressLines3`,`address`.`city`,`address`.`state`,`address`.`country`,`address`.`zip`,`address`.`addressStatus`,`address`.`addressTypeUUID` , `contact`.`contactTypeUUID` FROM `contact` JOIN `address` ON `address`.`addressUUID`=`contact`.`addressUUID` JOIN `contacttype` ON `contacttype`.`contactTypeUUID`=`contact`.`contactTypeUUID` WHERE `contact`.`partnerProfileUUID`=?'
          // 'SELECT * FROM contact INNER JOIN address ON contact.addressUUID = address.addressUUID  AND contact.partnerProfileUUID = ? ';
          values = [partnerProfileUUID];
        }

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            //   console.log(result);
            resolve(result);
          }
        });

        /*connection.query('SELECT `contact`.`role`,`contact`.`firstName`,`contact`.`lastName`,`contact`.`email`,`contact`.`phone`,`contact`.`fax` ,(`contacttype`.`name`) as `contactTypeName`,`contact`.`addressUUID`,`contact`.`createdOn`,`contact`.`lastUpdatedOn`,`address`.`addressNameID`,`address`.`addressLines1`,`address`.`addressLines2`,`address`.`addressLines3`,`address`.`city`,`address`.`state`,`address`.`country`,`address`.`zip`,`address`.`addressStatus`,`address`.`addressTypeUUID` , `contact`.`contactTypeUUID` FROM `contact` JOIN `address` ON `address`.`contactUUID`=`contact`.`contactUUID` JOIN `contacttype` ON `contacttype`.`contactTypeUUID`=`contact`.`contactTypeUUID` WHERE `contact`.`partnerProfileUUID`=' + mysql.escape(partnerProfileUUID) +
        ' UNION  SELECT `contact`.`role`,`contact`.`firstName`,`contact`.`lastName`,`contact`.`email`,`contact`.`phone`,`contact`.`fax` ,(`contacttype`.`name`) as `contactTypeName`,`contact`.`addressUUID`,`contact`.`createdOn`,`contact`.`lastUpdatedOn`,`address`.`addressNameID`,`address`.`addressLines1`,`address`.`addressLines2`,`address`.`addressLines3`,`address`.`city`,`address`.`state`,`address`.`country`,`address`.`zip`,`address`.`addressStatus`,`address`.`addressTypeUUID` , `contact`.`contactTypeUUID` FROM `contact` JOIN `address` ON `address`.`addressUUID`=`contact`.`addressUUID` JOIN `contacttype` ON `contacttype`.`contactTypeUUID`=`contact`.`contactTypeUUID` WHERE `contact`.`partnerProfileUUID`='+ mysql.escape(partnerProfileUUID) ,
         function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        })*/
      });
    },
    async fetchContactByPartnerProfile(root, { partnerProfileUUID }) {
      //sql query for fetch contact by partner profile id

      return new Promise((resolve, reject) => {
        var sql, values;

        /*var sql =
              ' c ORDER BY 	createdOn DESC';
              //'SELECT `contact`.`role`,`contact`.`firstName`,`contact`.`lastName`,`contact`.`email`,`contact`.`phone`,`contact`.`fax`,`contact`.`addressUUID`, (`contacttype`.`name`) as `contactTypeName`,`contact`.`createdOn`,`contact`.`lastUpdatedOn`,`address`.`addressNameID`,`address`.`addressLines1`,`address`.`addressLines2`,`address`.`addressLines3`,`address`.`city`,`address`.`state`,`address`.`country`,`address`.`zip`,`address`.`addressStatus`,`address`.`addressTypeUUID` , `contact`.`contactTypeUUID` FROM `contact` JOIN `address` ON `address`.`contactUUID`=`contact`.`contactUUID` JOIN `contacttype` ON `contacttype`.`contactTypeUUID`=`contact`.`contactTypeUUID` WHERE `contact`.`partnerProfileUUID`=? UNION  SELECT `contact`.`role`,`contact`.`firstName`,`contact`.`lastName`,`contact`.`email`,`contact`.`phone`,`contact`.`fax` ,`contact`.`addressUUID`, (`contacttype`.`name`) as `contactTypeName`,`contact`.`createdOn`,`contact`.`lastUpdatedOn`,`address`.`addressNameID`,`address`.`addressLines1`,`address`.`addressLines2`,`address`.`addressLines3`,`address`.`city`,`address`.`state`,`address`.`country`,`address`.`zip`,`address`.`addressStatus`,`address`.`addressTypeUUID` , `contact`.`contactTypeUUID` FROM `contact` JOIN `address` ON `address`.`addressUUID`=`contact`.`addressUUID` JOIN `contacttype` ON `contacttype`.`contactTypeUUID`=`contact`.`contactTypeUUID` WHERE `contact`.`partnerProfileUUID`=?'
           // 'SELECT * FROM contact INNER JOIN address ON contact.addressUUID = address.addressUUID  AND contact.partnerProfileUUID = ? ';
          values = [partnerProfileUUID];
        }

        connection.query(sql,values,
         function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        })*/
        /*         'SELECT `contact`.`role`,`contact`.`firstName`,`contact`.`lastName`,`contact`.`email`,`contact`.`phone`,`contact`.`fax` ,(`contacttype`.`name`) as `contactTypeName`,`contact`.`addressUUID`,`contact`.`createdOn`,`contact`.`lastUpdatedOn`,`address`.`addressNameID`,`address`.`addressLines1`,`address`.`addressLines2`,`address`.`addressLines3`,`address`.`city`,`address`.`state`,`address`.`country`,`address`.`zip`,`address`.`addressStatus`,`address`.`addressTypeUUID` , `address`.`addressUUID`,`address`.`partnerProfileUUID`,`contact`.`contactTypeUUID`,`contact`.`contactUUID` FROM `contact` JOIN `address` ON `address`.`contactUUID`=`contact`.`contactUUID` JOIN `contacttype` ON `contacttype`.`contactTypeUUID`=`contact`.`contactTypeUUID` WHERE `contact`.`partnerProfileUUID`=' +
            mysql.escape(partnerProfileUUID) +
            ' UNION  SELECT `contact`.`role`,`contact`.`firstName`,`contact`.`lastName`,`contact`.`email`,`contact`.`phone`,`contact`.`fax` ,(`contacttype`.`name`) as `contactTypeName`,`contact`.`addressUUID`,`contact`.`createdOn`,`contact`.`lastUpdatedOn`,`address`.`addressNameID`,`address`.`addressLines1`,`address`.`addressLines2`,`address`.`addressLines3`,`address`.`city`,`address`.`state`,`address`.`country`,`address`.`zip`,`address`.`addressStatus`,`address`.`addressTypeUUID` ,`address`.`addressUUID`,`address`.`partnerProfileUUID`, `contact`.`contactTypeUUID`,`contact`.`contactUUID` FROM `contact` JOIN `address` ON `address`.`addressUUID`=`contact`.`addressUUID` JOIN `contacttype` ON `contacttype`.`contactTypeUUID`=`contact`.`contactTypeUUID` WHERE `contact`.`partnerProfileUUID`=' +
            mysql.escape(partnerProfileUUID) +
            `ORDER BY 	createdOn DESC`,*/
        connection.query(
          'SELECT (`contact`.`addressUUID` )as `contactAddressUUID`,`contact`.`contactUUID`,`contact`.`role`,`contact`.`firstName`,`contact`.`lastName`,`contact`.`email`,`contact`.`phone`,`contact`.`fax` ,(`contacttype`.`name`) as `contactTypeName`,`contact`.`addressUUID`,`contact`.`createdOn`,`contact`.`lastUpdatedOn`,`address`.`addressNameID`,`address`.`addressLines1`,`address`.`addressLines2`,`address`.`addressLines3`,`address`.`city`,`address`.`state`,`address`.`country`,`address`.`zip`,`address`.`addressStatus`,`address`.`addressTypeUUID` , `address`.`addressUUID`,`address`.`partnerProfileUUID`,`contact`.`contactTypeUUID`,(`address`.`contactUUID`)as `addressconatctUUID` FROM `contact` JOIN `address` ON `address`.`contactUUID`=`contact`.`contactUUID` JOIN `contacttype` ON `contacttype`.`contactTypeUUID`=`contact`.`contactTypeUUID` WHERE `contact`.`partnerProfileUUID`=' +
            mysql.escape(partnerProfileUUID) +
            ' UNION  SELECT (`contact`.`addressUUID` )as `contactAddressUUID`,`contact`.`contactUUID`,`contact`.`role`,`contact`.`firstName`,`contact`.`lastName`,`contact`.`email`,`contact`.`phone`,`contact`.`fax` ,(`contacttype`.`name`) as `contactTypeName`,`contact`.`addressUUID`,`contact`.`createdOn`,`contact`.`lastUpdatedOn`,`address`.`addressNameID`,`address`.`addressLines1`,`address`.`addressLines2`,`address`.`addressLines3`,`address`.`city`,`address`.`state`,`address`.`country`,`address`.`zip`,`address`.`addressStatus`,`address`.`addressTypeUUID` ,`address`.`addressUUID`,`address`.`partnerProfileUUID`, `contact`.`contactTypeUUID`,(`address`.`contactUUID`)as `addressconatctUUID` FROM `contact` JOIN `address` ON `address`.`addressUUID`=`contact`.`addressUUID` JOIN `contacttype` ON `contacttype`.`contactTypeUUID`=`contact`.`contactTypeUUID` WHERE `contact`.`partnerProfileUUID`=' +
            mysql.escape(partnerProfileUUID) +
            `ORDER BY 	createdOn DESC`,
          function (err, result) {
            if (err) {
              reject(err);
            } else {
              // console.log(result);
              resolve(result);
            }
          }
        );
      });
    },
  },
  Mutation: {
    async createContact(root, { input }) {
      console.log(input.firstName, input);
      //sql query for create userTypes
      return new Promise((resolve, reject) => {
        var sql =
          'INSERT INTO contact (role, firstName,lastName ,email,phone,fax,addressUUID,contactTypeUUID,partnerProfileUUID) VALUES(?,?,?,?,?,?,?,?,?)';

        var values = [
          input.role,
          input.firstName,
          input.lastName,
          input.email,
          input.phone,
          input.fax,
          input.addressUUID,
          input.contactTypeUUID,
          input.partnerProfileUUID,
        ];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log('contact results...........');
            console.log(result, result.insertId);

            if (result !== undefined && result.insertId !== undefined) {
              connection.query(
                `SELECT * from contact where idContact=` +
                  mysql.escape(result.insertId),
                function (adderr, results) {
                  if (adderr) {
                    reject(adderr);
                  } else {
                    console.log('conatct find results...........');
                    console.log(results);
                    resolve(results);
                  }
                }
              );
            } else {
              resolve(result);
            }
            // resolve(result.insertId);
            /* if (result !== undefined && input.addressUUID == null) {
              /*  console.log(result.insertId);
              var sqladdress = `INSERT INTO address (partnerProfileUUID,contactUUID )VALUES (?
                , (select contactUUID from contact where idContact= ?));`;
              var SQLvalues = [input.partnerProfileUUID, result.insertId];
              connection.query(sqladdress, SQLvalues, function (
                adderr,
                results
              ) {
                if (adderr) {
                  reject(adderr);
                } else {
                  console.log('address results...........');
                  console.log(results);
                  resolve(results);
                }
              });
            } else {
              resolve(result);
            }*/
          }
        });
        /*  var sql =
          'INSERT INTO address (partnerProfileUUID,contactUUID) VALUES(?,?)';

        var values = [
         
          input.partnerProfileUUID,
          input.contactUUID,
        ];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });*/
      });
    },
    async updateContact(root, { input }) {
      console.log(input);
      //sql query for update Contact
      return new Promise((resolve, reject) => {
        var sql =
          'UPDATE contact SET role = ?,firstName=? ,lastName=?, email=?,phone=?,fax=?,addressUUID=?,contactTypeUUID=?,partnerProfileUUID=?,lastUpdatedOn=? WHERE contactUUID = ?';

        var values = [
          input.role,
          input.firstName,
          input.lastName,
          input.email,
          input.phone,
          input.fax,
          input.addressUUID,
          input.contactTypeUUID,
          input.partnerProfileUUID,
          new Date(),
          input.contactUUID,
        ];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async removeContact(root, { input }) {
      console.log(input);
      //sql query for create contact
      return new Promise((resolve, reject) => {
        //address deletion
        var sqladdress = 'DELETE FROM `address` WHERE `contactUUID` = ?;';
        var valuesaddress = [input.contactUUID];
        connection.query(sqladdress, valuesaddress, function (errs, results) {
          if (errs) {
            reject(errs);
          } else {
            console.log(results);
            //contact deletion
            var sql = 'DELETE FROM `contact` WHERE `contactUUID` = ?;';

            var values = [input.contactUUID];

            connection.query(sql, values, function (err, result) {
              if (err) {
                reject(err);
              } else {
                console.log(result);
                resolve(result);
              }
            });
          }
        });
      });
    },
    async removeAddressandaddAddressUUID(root, { input }) {
      return new Promise((resolve, reject) => {
        //remove the address table
        console.log(input);
        var sqladdress = 'DELETE FROM `address` WHERE `contactUUID` = ?;';
        var valuesaddress = [input.contactUUID];
        connection.query(sqladdress, valuesaddress, function (errs, results) {
          if (errs) {
            reject(errs);
          } else {
            console.log(results);
            //adding addressuuid in contact table
            var sql =
              'UPDATE contact SET addressUUID=?,lastUpdatedOn=? WHERE contactUUID = ?';

            var values = [input.addressUUID, new Date(), input.contactUUID];

            connection.query(sql, values, function (err, result) {
              if (err) {
                reject(err);
              } else {
                console.log(result);
                resolve(result);
              }
            });
          }
        });
      });
    },
    async UpdateaddressUUIDOnStatus(root, { addressUUID }) {
      return new Promise((resolve, reject) => {
        //remove the address table
        console.log(addressUUID);
        var sqladdress =
          'UPDATE contact SET addressUUID=?,lastUpdatedOn=? WHERE addressUUID = ?';
        var valuesaddress = [null, new Date(), addressUUID];
        connection.query(sqladdress, valuesaddress, function (errs, results) {
          if (errs) {
            reject(errs);
          } else {
            console.log(results);
            resolve(results);
          }
        });
      });
    },
  },
};

const contactSchema = makeExecutableSchema({
  typeDefs,
  resolvers,
  formatError: (err) => {
    // Don't give the specific errors to the client.
    if (err.message.startsWith('Database Error: ')) {
      return new Error('sql error');
    }

    // Otherwise return the original error.  The error can also
    // be manipulated in other ways, so long as it's returned.
    return err;
  },
});

module.exports = contactSchema;
