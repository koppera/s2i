let Mongoose = require('mongoose');

let GroupSchema = new Mongoose.Schema({
  CommonName: String,
  Creator: String,
  CreatedDate: { type: Date, default: Date }
});

module.exports = Mongoose.model('GroupSchema', GroupSchema);
