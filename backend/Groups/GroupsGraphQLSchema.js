var { makeExecutableSchema } = require('graphql-tools');
var GroupSchema = require('./GroupsSchema');
var UserSchema = require('../Users/UserSchema');
var ldap = require('ldapjs');
var client = ldap.createClient({ url: 'ldap:// 18.220.251.58:389' });
client.bind("cn=admin,dc=acme,dc=changemaker,dc=community", "password", function(err) {
  console.log('Binderror' + err);
});
const typeDefs = `
scalar Date
type UIDGroup{
  _id:String
  gid:String
  CommonName:String
  Creator:String
  CreatedDate:Date
}
type Group{   
     _id:String
    CommonName:String
    Creator:String
    CreatedDate:Date
}
input GroupInput{   
    CommonName:String
    member:String
    memberId:String
    Creator:String
}

type Query{
   fetchAllGroups:[Group]
   fetchGroupsById(Uid:String):[UIDGroup]
   filterGroupsByUser(userId:String):[UIDGroup]
}

type Mutation{
   createGroup(input:GroupInput):Group
}

`;

const resolvers = {
  Query: {
    fetchAllGroups: (root, args, context, info) => {
      const groups = GroupSchema.find({})
        .sort({ CreatedDate: -1 })
        .exec();
      if (!groups) {
        throw new Error('Cannot fetch groups');
      }
      return groups;
    },
    async fetchGroupsById(root, { Uid }) {
      console.log(Uid);
      if (Uid != '') {
        //uid based groups of user
        console.log(Uid);
        const userGroups = await UserSchema.find({ Uid: Uid });
        console.log(userGroups);
        if (userGroups !== undefined && userGroups.length > 0) {
          console.log(userGroups[0].UserGroups);
          return userGroups[0].UserGroups;
        }
      } else {
        const groups = GroupSchema.find({})
          .sort({ CreatedDate: -1 })
          .exec();
        if (!groups) {
          throw new Error('Cannot fetch groups');
        }
        return groups;
      }
    },
    async filterGroupsByUser(root, { userId }) {
      console.log(userId);
      console.log('group information');

      const groups = await GroupSchema.find({})
        .sort({ CreatedDate: -1 })
        .exec();
      if (!groups) {
        throw new Error('Cannot fetch groups');
      }
      if (userId !== undefined) {
        const userGroups = await UserSchema.find({ _id: userId });
        if (userGroups !== undefined && userGroups[0].UserGroups.length > 0) {
          //  console.log(userGroups[0].UserGroups);
          let assignedGroups = userGroups[0].UserGroups;
          var result = groups.filter(function(o1) {
            // filter out (!) items in result2
            return !assignedGroups.some(function(o2) {
              return o1.id === o2.gid; // assumes unique id
            });
          });
          console.log(result);
          return result;
        } else {
          return groups;
        }
      } else {
        return groups;
      }
    }
  },
  Mutation: {
    async createGroup(root, { input }) {
      console.log(input);
      var entry = {
        member: 'cn=' + input.member + ',ou=Users,dc=acme,dc=changemaker,dc=community',
        cn: input.CommonName,
        objectclass: ['groupOfNames', 'top']
      };
      let dn =
        'cn=' + input.CommonName + ',ou=Groups,dc=acme,dc=changemaker,dc=community';
      console.log('dn', dn);
      console.log(entry);
      client.add(dn, entry, async function(err) {
        console.log(err);
        if (err === null) {
          const GroupModel = new GroupSchema({
            CommonName: input.CommonName,
            Creator: input.Creator
          });
          const newGroup = await GroupModel.save();
          if (!newGroup) {
            throw new Error('Cannot create Group');
          } else {
            console.log(GroupModel);
            console.log(input.memberId);
            let groupInput = {
              gid: GroupModel._id,
              CommonName: GroupModel.CommonName
            };
            console.log(groupInput);
            const updateGroup = await UserSchema.update(
              { _id: input.memberId },
              {
                $push: {
                  UserGroups: groupInput
                }
              }
            );
            console.log(updateGroup);
            if (!updateGroup) {
              console.log('error in updating');
              throw new Error('Error - User cannot be updated');
            }
            return updateGroup;
          }
          return newGroup;
        }
      });
    }
  }
};

var groupSchema = makeExecutableSchema({
  typeDefs,
  resolvers
});

module.exports = groupSchema;
