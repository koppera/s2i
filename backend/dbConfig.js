module.exports.db = {
  host:process.env.HOST,
  user:process.env.DB_USER, 
  password:process.env.DB_PASSWORD,
  database:process.env.DB, 
  port:process.env.DB_PORT,
  BASE_URL:process.env.BASE_URL,
  Secret:process.env.LOGIN_SECRET,
  partnerENV:process.env.PARTNERENV,
  cluster_url:process.env.CLUSTER_URL,
  token:process.env.CLUSTER_TOKEN,
  namespace:process.env.CLUSTERNAMESPACE,
  arcesbHeader:process.env.ARCESB_URL_HEADER,
  ARCESB_URL:process.env.ARCESB_URL
};

/*module.exports.db = {
  host: '127.0.0.1',
  user: 'root',
  password: '',
  database: 'tms1',
  port: 3306,
  BASE_URL: 'https://is.tms.changemaker.community/',
  Secret: 'somesuperdupersecret',
  partnerENV: 'DEV',
  cluster_url: 'https://master.tms.changemaker.community:8443',
  token: 'OhysGFDsZa6EPcQOBjiVWeB99numWevSHs3YDCIrYbw',
  namespace: 'tms',
  arcesbHeader: '9u5S0y4m9W9m5m3V1f8c',
  ARCESB_URL: 'https://testarcesb.arcesb.changemaker.community/api.rsc/',
};*/
