var { makeExecutableSchema } = require('graphql-tools');
const config = require('../dbConfig');
const mysql = require('mysql');
const withAuthfn=require('../verifyMiddleware/withAuth');
var connection = require('../database');

/*var MySQLStore = require('connect-mysql'); // mysql session store
var options = {
      config: {
        host: config.db.host,
        user: config.db.user,
        password: config.db.password,
        database: config.db.database,
        port: config.db.port,
      },
      pool:true,
      keepalive:true

    };

    connection=MySQLStore(options)*/ /*= mysql.createPool({
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database,
  port: config.db.port,
});*/
/*/*function handleDisconnect() {
  connection = mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database,
    port: config.db.port,
  }); // Recreate the connection, since
  // the old one cannot be reused.

  connection.connect(function (err) {
    // The server is either down
    if (err) {
      // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    } // to avoid a hot loop, and to allow our node script to
  }); // process asynchronous requests in the meantime.
  // If you're also serving http, display a 503 error.
  connection.on('error', function (err) {
    console.log('db error', err);
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      // Connection to the MySQL server is usually
      handleDisconnect(); // lost due to either server restart, or a
    } else {
      // connnection idle timeout (the wait_timeout
      throw err; // server variable configures this)
    }
  });
}

handleDisconnect();*/

//connection.connect();
const typeDefs = `
scalar Date

type ContactTypeValues{
    idContactType:String
   contactTypeUUID:String
    name:String
    description:String
    createdOn:Date
    lastUpdatedOn:Date
}

input ContactTypeInput{

   idContactType:String
   contactTypeUUID:String
    name:String
    description:String
}
type Query{
   allContactsType:[ContactTypeValues]

}

type Mutation{
   createContactType(input:ContactTypeInput): ContactTypeValues
   updateContactType(input:ContactTypeInput): ContactTypeValues
   removeContactType(input:ContactTypeInput):ContactTypeValues
  
}


`;

const resolvers = {
  Query: {
    allContactsType: (root, args, context, info) => {
      //sql query for fetch all contacttype
      return new Promise((resolve, reject) => {
        connection.query(
          'SELECT * from contacttype ORDER BY  createdOn DESC ',
          function (err, post) {
            if (err) {
              reject(err);
            } else {
              resolve(post);
            }
          }
        );
      });
    },
  },
  Mutation: {
    async createContactType(root, { input }) {
      //sql query for create contacttype
      return new Promise((resolve, reject) => {
        name: String;
        description: String;
        var sql = 'INSERT INTO contacttype (name, description) VALUES(?,?)';

        var values = [input.name, input.description];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async updateContactType(root, { input }) {
      //sql query for update conatct type
      return new Promise((resolve, reject) => {
        var sql =
          'UPDATE contacttype SET name = ?,description=?,lastUpdatedOn=? WHERE contactTypeUUID = ?';

        var values = [
          input.name,
          input.description,
          new Date(),
          input.contactTypeUUID,
        ];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async removeContactType(root, { input }) {
      //sql query for create contact type
      return new Promise((resolve, reject) => {
        var sql = 'DELETE FROM `contacttype` WHERE `contactTypeUUID` = ?;';

        var values = [input.contactTypeUUID];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
};

const contacttypeSchema = makeExecutableSchema({
  typeDefs,
  resolvers,
  formatError: (err) => {
    // Don't give the specific errors to the client.
    if (err.message.startsWith('Database Error: ')) {
      return new Error('sql error');
    }

    // Otherwise return the original error.  The error can also
    // be manipulated in other ways, so long as it's returned.
    return err;
  },
});

module.exports = contacttypeSchema;
