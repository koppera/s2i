var { makeExecutableSchema } = require('graphql-tools');
//var UserSchema = require('./dbConfig');
var ldap = require('ldapjs');
var assert = require('assert');
var format = require('date-format');
const bcrypt = require('bcrypt')
const jsonwebtoken = require('jsonwebtoken')

const BCRYPT_SALT_ROUNDS = 12;
// eslint-disable-next-line prefer-destructuring

//console.log("Ldap",process.env)
/*var client = ldap.createClient({ url:'ldap:// 18.220.251.58:389' });
client.bind("cn=admin,dc=tms,dc=changemaker,dc=community", "password", function(err) {
  console.log('Binderror' + err);
});*/

const config = require('../dbConfig');


const mysql = require('mysql');
const withAuthfn=require('../verifyMiddleware/withAuth');
var connection = require('../database');
//var connection = require('../database');

/*var MySQLStore = require('connect-mysql'); // mysql session store
var options = {
      config: {
        host: config.db.host,
        user: config.db.user,
        password: config.db.password,
        database: config.db.database,
        port: config.db.port,
      },
      pool:true,
      keepalive:true

    };

    connection=MySQLStore(options)*/ /*= mysql.createPool({
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database,
  port: config.db.port,
});*/
/*/*function handleDisconnect() {
  connection = mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database,
    port: config.db.port,
  }); // Recreate the connection, since
  // the old one cannot be reused.

  connection.connect(function (err) {
    // The server is either down
    if (err) {
      // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    } // to avoid a hot loop, and to allow our node script to
  }); // process asynchronous requests in the meantime.
  // If you're also serving http, display a 503 error.
  connection.on('error', function (err) {
    console.log('db error', err);
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      // Connection to the MySQL server is usually
      handleDisconnect(); // lost due to either server restart, or a
    } else {
      // connnection idle timeout (the wait_timeout
      throw err; // server variable configures this)
    }
  });
}

handleDisconnect();*/

//connection.connect();

const typeDefs = `
 scalar Date


 type Users{
  idUser :String
  userUUID:String
  userID:String
  userPrivilage:String
  loginType:String
    createdOn:Date 
    lastUpdatedOn:Date
 }

input UserInput{
  idUser :String
  userUUID:String
  userID:String
  userPassword:String
  permission:String
  loginType:String
 }

 input UpdateUserInput{
  idUser :String
  userUUID:String
  userID:String
  permission:String
  loginType:String
 }

 type UserPaginated{
  edges:[Users]
  pageInfo:PageInfo
 }

 type PageInfo{
    endCursor:Date
 }

type Query{
    allUsers:[Users]
    removeUser:Users  
    fetchUsers(limit:Int,cursor:Int):UserPaginated
    loginUser1(userID:String,userPassword:String):[Users]
    checkToken(tokenVal:String):token
}

type token1{
  userUUID:String
  loginType:String
  tokenVal:String
}

type DecodeUser{
  userUUID:String
  loginType:String
  
}
type token{
  tokenVal:String
}

type Mutation{
    createUsers1 (userID: String!, userPassword: String!, loginType: String!): String
    createUsers(input:UserInput):token  
    updateUsers(input:UserInput): token
    removeUsers(input:UserInput):Users
    loginUser(userID:String,userPassword:String):token1
    verifyUser(tokenValverify:String!):DecodeUser
}
 `;

const resolvers = {
  Query: {
    async checkToken(root,{tokenVal}){
         return await new Promise((resolve,reject)=>{
            
         })
    },
    async fetchUsers(root, { limit, cursor }) {
      const cursorOptions = cursor
        ? {
            idUser: { $gt: cursor },
          }
        : {};
      return await new Promise((resolve, reject) => {
        console.log(
          cursor,
          '==>',
          cursorOptions,
          'SELECT count(idUser) from user where 1',
          'SELECT * from user where  LIMIT ' + limit + ' OFFSET ' + cursor,
          'SELECT * from user where idUser>=' +
            mysql.escape(cursor) +
            ' LIMIT ' +
            limit,
          'SELECT * from user where idUser=' +
            mysql.escape(cursor) +
            ' LIMIT ' +
            mysql.escape(limit),
          'SELECT * from user where idUser>=' +
            mysql.escape(cursor) +
            ' LIMIT ' +
            mysql.escape(limit)
        );
        connection.query('SELECT count(idUser) from user where 1', function (
          err,
          totalCount
        ) {
          if (err) {
            reject(err);
          } else {
            connection.query(
              'SELECT * from user  LIMIT ' + limit + ' OFFSET ' + cursor,
              function (err, users) {
                console.log(users);
                if (err) {
                  reject(err);
                } else {
                  const allUsers = {
                    totalCount: totalCount,
                    edges: users,
                    pageInfo: {
                      endCursor: cursor + 1,
                    },
                  };
                  resolve(allUsers);
                }
              }
            );
          }
        });
      });
    },
    allUsers: async (root, args, context, info) => {
      return await new Promise((resolve, reject) => {
        connection.query(
          'SELECT * from user ORDER BY  createdOn DESC ',
          function (err, post) {
            if (err) {
              reject(err);
            } else {
              resolve(post);
            }
          }
        );
      });
    },
    
    async loginUser1(root, { userID, userPassword }) {
      console.log(userID, userPassword);
      return new Promise((resolve, reject) => {
        var sql =
          'Select * from user where (userID= ' +
          mysql.escape(userID) +
          'AND userPassword=' +
          mysql.escape(userPassword) +
          ') ORDER BY  createdOn DESC ';
        console.log(sql);
        //var sql = 'SELECT *from user WHERE userID=? AND userPassword=?';
        // var values = [userID, userPassword];
        connection.query(sql, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
  Mutation: {
    async verifyUser(root,{tokenValverify}){
      console.log("verifyToken")
      return new Promise((resolve, reject) => {
      jwt.verify(tokenValverify, config.db.Secret, function(err, decoded) {
        console.log(decoded.foo) // bar
        resolve({userUUID:"ejrwjr",
          loginType:"dfef"})
      });
      reject("invalid token verify")
    });
    },
    async createUsers1(root, { userID, userPassword,loginType}) {
      return new Promise((resolve, reject) => {
        console.log("input",userID, userPassword,loginType)
        bcrypt.genSalt(10, (err, salt) => {
          bcrypt.hash(userPassword,  salt,(err,hashedPassword) => {
            console.log("salt",BCRYPT_SALT_ROUNDS,"hash",hashedPassword,"pwd",userPassword)
          connection.query(
            'INSERT INTO `user`( `userID`, `userPassword`, `loginType`) VALUES (' +
              mysql.escape(userID) +
              ',' +
              mysql.escape(hashedPassword) +
              ',' +
              mysql.escape(loginType) +
              ')',
            function (err, result) {
              if (err) {
                reject(err);
              } else {

                connection.query(
                  'SELECT * from user WHERE userID ='+mysql.escape(userID),
                  function (err, postData) {
                    if (err) {
                      reject(err);
                    } else {
                           // return json web token
                   // return json web token
                   console.log("res",postData.userUUID,"ip",userID)
                   var jwtresult= jsonwebtoken.sign({
                    idUser:userID,
                    uuidUser: postData.userUUID,
                     }, config.db.Secret, { expiresIn: '1y' })
              
                    console.log("jwtresult",jwtresult);
                  resolve(jwtresult);
                    }
                  }
                );
                   
                  
              }
            }
          );
         
        });
      });
        
      });
    },
    async createUsers(root, { input }) {
      return await new Promise((resolve, reject) => {
        console.log("input",input,"pwd",input.userPassword)
        //bcrypt.genSalt(10, (err, salt) => {
          bcrypt.hash(input.userPassword,  10,(err,hashedPassword) => {
            console.log("salt",BCRYPT_SALT_ROUNDS,"hash",hashedPassword,"pwd",input.userPassword)
          connection.query(
            'INSERT INTO `user`( `userID`, `userPassword`, `loginType`,`userPrivilage`) VALUES (' +
              mysql.escape(input.userID) +
              ',' +
              mysql.escape(hashedPassword) +
              ',' +
              mysql.escape(input.loginType) +
              ',' +
              mysql.escape(input.permission) +
              ')',
            function (err, result) {
              if (err) {
                reject(err);
              } else {
                        // return json web token
                   // return json web token
                 /*  console.log("res",result.userID,"ip",input.userID)
                   var jwtresult= jsonwebtoken.sign({
                    idUser:result.userID,
                   uuidUser: input.userID
                     }, config.db.Secret, { expiresIn: '1y' })
              
                  console.log(jwtresult);
              resolve({tokenVal:jwtresult});*/
              console.log(  'SELECT * from user WHERE userID ='+mysql.escape(input.userID))
              connection.query(
                'SELECT * from user WHERE userID ='+mysql.escape(input.userID),
                function (err, postData) {
                  if (err) {
                    reject(err);
                  } else {
                         // return json web token
                 // return json web token
                 console.log("res",postData)
                 var jwtresult= jsonwebtoken.sign({
                  idUser:input.userID,
                  uuidUser: postData.userUUID,
                   }, config.db.Secret, { expiresIn: '1y' })
            
                  console.log("jwtresult",jwtresult);
                  resolve({//userID:input.userID,
                            //loginType:'',
                            tokenVal:jwtresult});
                  }
                });                  
              }}
            );
          });
        }
        );
     // });
        
      },
    
    async updateUsers(root, { input }) {
      console.log(input);
      //sql query for update user
      return new Promise((resolve, reject) => {
        bcrypt.hash(input.userPassword,  10,(err,hashedPassword) => {
          console.log("salt",BCRYPT_SALT_ROUNDS,"hash",hashedPassword,"pwd",input.userPassword)
        var sql =
          'UPDATE user SET userID = ?,loginType=?,userPassword=?,lastUpdatedOn=?,userPrivilage=? WHERE userUUID = ?';

        var values = [
          input.userID,
          input.loginType,
          hashedPassword,
          new Date(),
          input.permission,
          input.userUUID,
        ];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            connection.query(
              'SELECT * from user WHERE userID ='+mysql.escape(input.userID),
              function (err, postData) {
                if (err) {
                  reject(err);
                } else {
                       // return json web token
               // return json web token
               console.log("res",postData)
               var jwtresult= jsonwebtoken.sign({
                idUser:input.userID,
                uuidUser: postData.userUUID,
                 }, config.db.Secret, { expiresIn: '1y' })
          
                console.log("jwtresult",jwtresult);
                resolve({//userID:input.userID,
                  //loginType:'',
                  tokenVal:jwtresult});
                }
              });
            //console.log(result);
            //resolve(result);
          }
        });
      });
      });
    },
    async removeUsers(root, { input }) {
      console.log(input);
      //sql query for create userTypes
      return new Promise((resolve, reject) => {
        var sql = 'DELETE FROM `user` WHERE `userUUID` = ?;';

        var values = [input.userUUID];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async loginUser(root, { userID, userPassword }) {
      console.log("userID, userPassword",userID, userPassword);
     
      return new Promise(async (resolve, reject) => {
       
       
          var sql = 'SELECT userID,userUUID,userPassword,loginType from user WHERE userID='+mysql.escape(userID);
          //var values = [userID, userPassword];
          var values = [mysql.escape(userID)];
                         console.log("values",values,sql)
          connection.query(sql,  async function (err, result) {
            console.log("err, result",err, JSON.stringify(result))
            if (err) {
              var send={msg: 'Authentication failed. User not found.',err:err}
              reject(send);
            } else {   /**if (!user) {
                            res.send({success: false, msg: 'Authentication failed. User not found.'});
                          } else {*/
                            //check if password matches
                            /*bcrypt.comparePassword(req.body.password, function (err, isMatch) {
                              if (isMatch && !err) {
                                // if user is found and password is right create a token
                                var token = jwt.encode(user, config.secret);
                                // return the information including token as JSON
                                res.json({success: true, token: 'JWT ' + token});
                              } else {
                                res.send({success: false, msg: 'Authentication failed. Wrong password.'});
                              }
                            });*/
                           // if(userPassword!==undefined)
                           if(result.length!==0)
                            {
                            console.log("Pwd",JSON.stringify(result))
                            var newPwd=result[0].userPassword;
                            console.log("newPwd",newPwd)
                            console.log("userPassword",userPassword)
                          

                          //const salt = await bcrypt.genSalt(10);
                          //const hash = await bcrypt.hash(userPassword, salt);                                                    
                          //console.log("hash",hash)
                          //const valid = await bcrypt.compare(userPassword, hash);
                          //const valid =await bcrypt.compareSync(userPassword, newPwd); // true
                            const valid = await bcrypt.compare(userPassword,newPwd);
                            console.log("vPwd",valid)
                            if (!valid) {
                                //throw new Error('Incorrect Username or password')
                                reject('Incorrect Username or password------');
                            }
                            console.log(":res  userUUID",result.userUUID,"res loginType",result.loginType);
                            // return json web token
                           var jwtresult= jsonwebtoken.sign({
                                // idUser:result[0].userID,
                                uuidUser: result[0].userUUID,
                                loginType:result[0].loginType,
                            }, config.db.Secret, { expiresIn: '1d' })
                           
                         console.log("jwtresult",jwtresult,":result.userUUID",result[0].userUUID,"result.loginType",result[0].loginType);
                         resolve({userUUID:result[0].userUUID,
                                  loginType:result[0].loginType,
                                  tokenVal:jwtresult});
                          }else reject('Incorrect Username or password after');
            }
          });
        
      });

    },
  },
};

const userSchema = makeExecutableSchema({
  typeDefs,
  resolvers,
  formatError: (err) => {
    // Don't give the specific errors to the client.
    if (err.message.startsWith('Database Error: ')) {
      return new Error('mongo db error');
    }

    // Otherwise return the original error.  The error can also
    // be manipulated in other ways, so long as it's returned.
    return err;
  },
});

module.exports = userSchema;

/**mutation loginUser($userID:String,$userPassword:String)
{loginUser(userID:$userID,userPassword:$userPassword){
  tokenVal
}}{
  "userID": "CDA123",
  "userPassword": "Cloudgen@123"
}
 */
/**
///admin
query: "mutation createUsers($inputCreate:UserInput){↵  createUsers(input:$inputCreate)↵  {↵   tokenVal↵  }↵}"
variables: {inputCreate: {userID: "CG003", loginType: "Admin", userPassword: "Cloudgen@123", permission: ""}}
inputCreate: {userID: "CG003", loginType: "Admin", userPassword: "Cloudgen@123", permission: ""}
loginType: "Admin"
permission: ""
userID: "CG003"
userPassword: "Cloudgen@123"*/