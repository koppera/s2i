var { makeExecutableSchema } = require('graphql-tools');
const config = require('../dbConfig');
const mysql = require('mysql');
const withAuthfn=require('../verifyMiddleware/withAuth');
var connection = require('../database'); /*= mysql.createPool({
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database,
  port: config.db.port,
});*/
/*function handleDisconnect() {
  connection =  mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database,
    port: config.db.port,
  });; // Recreate the connection, since
                                                  // the old one cannot be reused.

  connection.connect(function(err) {              // The server is either down
    if(err) {                                     // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    }                                     // to avoid a hot loop, and to allow our node script to
  });                                     // process asynchronous requests in the meantime.
                                          // If you're also serving http, display a 503 error.
  connection.on('error', function(err) {
    console.log('db error', err);
    if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
      handleDisconnect();                         // lost due to either server restart, or a
    } else {                                      // connnection idle timeout (the wait_timeout
      throw err;                                  // server variable configures this)
    }
  });
}

handleDisconnect();*/


//connection.connect();
const typeDefs = `
scalar Date

type profilecontactrelationTypes{
    idprofilecontactrelation:String
    profilecontactrelationUUID:String
    partnerProfileUUID:String
    contactUUID:String  
    createdon:Date
    lastupdatedon:Date
}

input profilecontactrelationInput{

    idprofilecontactrelation:String
    profilecontactrelationUUID:String
    partnerProfileUUID:String
    contactUUID:String  
}
type Query{
   allProfileContactRelation:[profilecontactrelationTypes]
}

type Mutation{
   createProfileContactRelation(input:profilecontactrelationInput): profilecontactrelationTypes
  
}


`;

const resolvers = {
  Query: {
    allProfileContactRelation: (root, args, context, info) => {
      //sql query for fetch all ProfileContactRelation
      return new Promise((resolve, reject) => {
        connection.query('SELECT * from profilecontactrelation', function(
          err,
          post
        ) {
          if (err) {
            reject(err);
          } else {
            resolve(post);
          }
        });
      });
    }
  },
  Mutation: {
    async createProfileContactRelation(root, { input }) {
      //sql query for create ProfileContactRelation

      return new Promise((resolve, reject) => {
        var sql =
          'INSERT INTO profilecontactrelation (partnerProfileUUID, contactUUID) VALUES(?,?)';

        var values = [input.partnerProfileUUID, input.contactUUID];

        connection.query(sql, values, function(err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    }
  }
};

const profilecontactrelationSchema = makeExecutableSchema({
  typeDefs,
  resolvers,
  formatError: err => {
    // Don't give the specific errors to the client.
    if (err.message.startsWith('Database Error: ')) {
      return new Error('sql error');
    }

    // Otherwise return the original error.  The error can also
    // be manipulated in other ways, so long as it's returned.
    return err;
  }
});

module.exports = profilecontactrelationSchema;
