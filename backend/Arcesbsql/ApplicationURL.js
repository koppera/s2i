var { makeExecutableSchema } = require('graphql-tools');
const config = require('../dbConfig');
const mysql = require('mysql');
const withAuthfn = require('../verifyMiddleware/withAuth');
var connection = require('../database');

/*var MySQLStore = require('connect-mysql'); // mysql session store
var options = {
      config: {
        host: config.db.host,
        user: config.db.user,
        password: config.db.password,
        database: config.db.database,
        port: config.db.port,
      },
      pool:true,
      keepalive:true

    };

    connection=MySQLStore(options)*/ /*= mysql.createPool({
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database,
  port: config.db.port,
});*/
/*function handleDisconnect() {
  connection = mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database,
    port: config.db.port,
  }); // Recreate the connection, since
  // the old one cannot be reused.

  connection.connect(function (err) {
    // The server is either down
    if (err) {
      // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    } // to avoid a hot loop, and to allow our node script to
  }); // process asynchronous requests in the meantime.
  // If you're also serving http, display a 503 error.
  connection.on('error', function (err) {
    console.log('db error', err);
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      // Connection to the MySQL server is usually
      handleDisconnect(); // lost due to either server restart, or a
    } else {
      // connnection idle timeout (the wait_timeout
      throw err; // server variable configures this)
    }
  });
}

handleDisconnect();*/
//connection.connect();
const typeDefs = `
scalar Date
type ApplicationURLValues{
    idApplicationURL:String
    applicationURLUUID:String
    publicDomain:String
    asynchronousMDNURL:String
    receivingURL:String
    publicURL:String
    publicCertificate:String
    createdOn:Date
    updatedOn:Date
}

input ApplicationURLInput{

   idApplicationURL:String
   applicationURLUUID:String
    publicDomain:String
    asynchronousMDNURL:String
    receivingURL:String
    publicURL:String
    publicCertificate:String
   
}
type Query{
   allApplicationURL:[ApplicationURLValues]

}

type Mutation{
   createApplicationURL(input:ApplicationURLInput): ApplicationURLValues
  
  
}


`;

const resolvers = {
  Query: {
    allApplicationURL: (root, args, context, info) => {
      //sql query for fetch allApplicationURL
      return new Promise((resolve, reject) => {
        connection.query(
          'SELECT * from applicationurl_arcesb ORDER BY  createdOn DESC ',
          function (err, post) {
            if (err) {
              reject(err);
            } else {
              resolve(post);
            }
          }
        );
      });
    },
  },
  Mutation: {
    async createApplicationURL(root, { input }) {
      //sql query for create ApplicationURL
      console.log(input);
      return new Promise((resolve, reject) => {
        var sql =
          'INSERT INTO applicationurl_arcesb (publicDomain,asynchronousMDNURL,receivingURL,publicURL,publicCertificate) VALUES(?,?,?,?,?)';

        var values = [
          input.publicDomain,
          input.asynchronousMDNURL,
          input.receivingURL,
          input.publicURL,
          input.publicCertificate,
        ];
        console.log(sql, values);
        connection.query(sql, values, function (err, result) {
          if (err) {
            console.log('error', err);
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
};

const ApplicationURLSchema = makeExecutableSchema({
  typeDefs,
  resolvers,
  formatError: (err) => {
    // Don't give the specific errors to the client.
    if (err.message.startsWith('Database Error: ')) {
      return new Error('sql error');
    }

    // Otherwise return the original error.  The error can also
    // be manipulated in other ways, so long as it's returned.
    return err;
  },
});

module.exports = ApplicationURLSchema;
