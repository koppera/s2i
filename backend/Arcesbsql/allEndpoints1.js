var { makeExecutableSchema } = require('graphql-tools');
const config = require('../dbConfig');
const mysql = require('mysql');
const withAuthfn = require('../verifyMiddleware/withAuth');
const axios = require('axios');
var connection = require('../database');

/*var MySQLStore = require('connect-mysql'); // mysql session store
var options = {
      config: {
        host: config.db.host,
        user: config.db.user,
        password: config.db.password,
        database: config.db.database,
        port: config.db.port,
      },
      pool:true,
      keepalive:true

    };

    connection=MySQLStore(options)*/ /*= mysql.createPool({
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database,
  port: config.db.port,
});*/
/*function handleDisconnect() {
  connection = mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database,
    port: config.db.port,
  }); // Recreate the connection, since
  // the old one cannot be reused.

  connection.connect(function (err) {
    // The server is either down
    if (err) {
      // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    } // to avoid a hot loop, and to allow our node script to
  }); // process asynchronous requests in the meantime.
  // If you're also serving http, display a 503 error.
  connection.on('error', function (err) {
    console.log('db error', err);
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      // Connection to the MySQL server is usually
      handleDisconnect(); // lost due to either server restart, or a
    } else {
      // connnection idle timeout (the wait_timeout
      throw err; // server variable configures this)
    }
  });
}

handleDisconnect();*/
//connection.connect();
const typeDefs = `
scalar Date

type connectorValues{
  idConnectors:String
  connectorsUUID:String
  connectorID:String
  workspaceUUID:String
  workspaceName:String
  ConnectorType:String
  ParentConnector:String
  createdOn:Date
  updatedOn:Date
}

type transactionFlow{
    Status:String
}

input conectorInput{
  connectorsUUID:String
  connectorID:String
  workspaceUUID:String
  ConnectorType:String
  ParentConnector:String
}

input transactionInput{
  workspace:String
}



type Query{
    fetchConnectorsAPI(input:conectorInput):connectorValues
    allConnectors:[connectorValues]
    postConnectorsAPI(input:conectorInput):connectorValues
    fetchConnectorsByWorkspaceUUID(workspaceuuid:String):[connectorValues]
    flowAPI825_1(input:transactionInput):transactionFlow
    flowAPI850_1(input:transactionInput):transactionFlow
    flowAPI825:transactionFlow
    flowAPI850:transactionFlow
    TestService:transactionFlow
}

type Mutation{
  createConnectorsAPI(input:conectorInput) :connectorValues
}

`;

const resolvers = {
  Query: {
    async TestService(root, { input }) {     
        return new Promise(async(resolve, reject) => { 

            var postdata = {
                "ConnectorId":"Amazon_X12_Inbound",
                "Folder":"Send",
                "Filename":"850FromAmazone.edi",
                "Content":"SVNBKjAwKiAgICAgICAgICAqMDAqICAgICAgICAgICowMSpBTVogICAgICAgICAgICAqMDEqQ0xHICAgICAgICAgICAgKjEwMTEyNyoxNzE5KlUqMDA0MDEqMDAwMDAzNDM4KjAqVCo+fg0KR1MqUE8qQU1aKkNMRyoyMDEwMTEyNyoxNzE5KjE0MjEqWCowMDQwMTB+DQpTVCo4NTAqMDAwMDAwMDEwfg0KQkVHKjAwKlNBKjg4NDU3OCoqMjAxMDExMjcqNjEwMzg1Mzg1fg0KUkVGKkRQKjAzOH4NClJFRipQUypSfg0KSVREKjE0KjMqMioqNDUqKjQ2fg0KRFRNKjAwMioyMDEwMTIxNH4NClBLRypGKjY4KioqUEFMTEVUSVpFIFNISVBNRU5Ufg0KUEtHKkYqNjYqKipSRUdVTEFSfg0KVEQ1KkEqOTIqUDMqKlNFRSBYWVogUkVUQUlMIFJPVVRJTkcgR1VJREV+DQpOMSpTVCpYWVogUkVUQUlMKjkqMDAwMzk0NzI2ODI5Mn4NCk4zKjMxODc1IFNPTE9OIFJEfg0KTjQqU09MT04qT0gqNDQxMzl+DQpQTzEqMSoxMjAqRUEqOS4yNSpURSpDQiowNjUzMjItMTE3KlBSKlJPKlZOKkFCMzU0Mn4NClBJRCpGKioqKlNNQUxMIFdJREdFVH4NClBPNCo0KjQqRUEqUExUOTQqKjMqTFIqMTUqQ1R+DQpQTzEqMioyMjAqRUEqMTMuNzkqVEUqQ0IqMDY2ODUwLTExNipQUipSTypWTipSRDUzMjJ+DQpQSUQqRioqKipNRURJVU0gV0lER0VUfg0KUE80KjIqMipFQX4NClBPMSozKjEyNipFQSoxMC45OSpURSpDQiowNjA3MzMtMTEwKlBSKlJPKlZOKlhZNTI2Nn4NClBJRCpGKioqKkxBUkdFIFdJREdFVH4NClBPNCo2KjEqRUEqUExUOTQqKjMqTFIqMTIqQ1R+DQpQTzEqNCo3NipFQSo0LjM1KlRFKkNCKjA2NTMwOC0xMTYqUFIqUk8qVk4qVlgyMzMyfg0KUElEKkYqKioqTkFOTyBXSURHRVR+DQpQTzQqNCo0KkVBKlBMVDk0Kio2KkxSKjE5KkNUfg0KUE8xKjUqNzIqRUEqNy41KlRFKkNCKjA2NTM3NC0xMTgqUFIqUk8qVk4qUlYwNTI0fg0KUElEKkYqKioqQkxVRSBXSURHRVR+DQpQTzQqNCo0KkVBfg0KUE8xKjYqNjk2KkVBKjkuNTUqVEUqQ0IqMDY3NTA0LTExOCpQUipSTypWTipEWDE4NzV+DQpQSUQqRioqKipPUkFOR0UgV0lER0VUfg0KUE80KjYqNipFQSpQTFQ5NCoqMypMUioxMCpDVH4NCkNUVCo2fg0KQU1UKjEqMTMwNDUuOTR+DQpTRSozMyowMDAwMDAwMTB+DQpHRSoxKjE0MjF+DQpJRUEqMSowMDAwMDM0Mzh+"
            };

            
              /*var postdata = {
                "Workspaceid": "ArcesbTest"
                
              };*/
             // console.log(postdata);
             await axios( {
                method: 'post',        
                url:config.db.ARCESB_URL+'files',
                data: postdata,
                headers: {
                  'x-arcesb-authtoken': config.db.arcesbHeader,
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*',
                },
              }).then(function (response) {
                //console.log(response);
                console.log(response.data);
              });

  })},
      async flowAPI825(root, { input }) {     
    return new Promise(async(resolve, reject) => { 
    var postdata = {
        "workspace":"ArcesbTest",
        "PortType":"X12",
        "ConnectorId":"Amazon_X12_Inbound",
        "ISAReceiverIdQualifier" : "ZZ",
        "ISASenderIdQualifier" : "ZZ",
        "ISAReceiverIdentifier" : "CLG",
        "ISASenderIdentifier" : "AMZ",
        "TranslationType" : "X12ToXML",
        "SendToPort" : "Map_Transaction_Log"
      };
      /*var postdata = {
        "Workspaceid": "ArcesbTest"
        
      };*/
        // console.log(postdata);
        await axios({
          method: 'post',
          url: config.db.ARCESB_URL + 'connectors',
          data: postdata,
          headers: {
            'x-arcesb-authtoken': config.db.arcesbHeader,
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
          },
        }).then(function (response) {
          //console.log(response);
          console.log(response.data);
        });
        var postdata1 = {
          workspace: 'ArcesbTest',
          PortType: 'XMLMap',
          ConnectorId: 'Map_Transaction_Log',
          Source:
            'C:/Users/Gopi CG/OneDrive - CLOUDGEN SYSTEMS PVT LTD/CG/ARCESB/TEMP/EDIdata850.xml',
          Destination:
            'C:/Users/Gopi CG/OneDrive - CLOUDGEN SYSTEMS PVT LTD/CG/ARCESB/TEMP/transactionLogXML.xml',
          SendToPort: 'TransactionLogJSON',
        };
        /*var postdata = {
      "Workspaceid": "ArcesbTest"
      
    };*/
        // console.log(postdata1);
        await axios({
          method: 'post',
          url: config.db.ARCESB_URL + 'connectors',
          data: postdata1,
          headers: {
            'x-arcesb-authtoken': config.db.arcesbHeader,
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
          },
        }).then(function (response) {
          //console.log(response);
          console.log(response.data);
        });
        var postdata2 = {
          ConnectorId: 'TransactionLogJSON',
          PortType: 'JSON',
          workspace: 'ArcesbTest',
          SendToPort: 'elasticsearchconnector',
        };
        /*var postdata = {
    "Workspaceid": "ArcesbTest"

    };*/
        // console.log(postdata2);
        await axios({
          method: 'post',
          url: config.db.ARCESB_URL + 'connectors',
          data: postdata2,
          headers: {
            'x-arcesb-authtoken': config.db.arcesbHeader,
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
          },
        }).then(function (response) {
          //console.log(response);
          console.log(response.data);
        });
        var postdata3 = {
          workspace: 'ArcesbTest',
          PortType: 'REST',
          ConnectorId: 'elasticsearchconnector',
          Method: 'POST',
          Url: 'https://elastic.arcesb.changemaker.community/arcesb/test',
          Authorization: 'Basic',
          UserName: 'elastic',
          Password: 'elastic',
          BodyType: 'raw',
          RawContentType: 'application/json',
        };
        /*var postdata = {
        "Workspaceid": "ArcesbTest"
        
      };*/
        // console.log(postdata3);
        await axios({
          method: 'post',
          url: config.db.ARCESB_URL + 'connectors',
          data: postdata3,
          headers: {
            'x-arcesb-authtoken': config.db.arcesbHeader,
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
          },
        }).then(function (response) {
          //console.log(response);
          console.log(response.data);
        });
      });
    },

    async flowAPI850(root, { input }) {
     
        return new Promise(async(resolve, reject) => { 
            console.log("850")
        var postdata = {
          workspace: 'ArcesbTest',
          PortType: 'X12',
          ConnectorId: 'Amazon_X12_Inbound',
          ISAReceiverIdQualifier: 'ZZ',
          ISASenderIdQualifier: 'ZZ',
          ISAReceiverIdentifier: 'CLG',
          ISASenderIdentifier: 'AMZ',
          TranslationType: 'X12ToXML',
          SendToPort: 'Map_Transaction_Log',
        };
        /*var postdata = {
            "Workspaceid": "ArcesbTest"
            
          };*/
          //console.log(postdata);
         await axios( {
            method: 'post',        
            url:config.db.ARCESB_URL+'connectors',
            data: postdata,
            headers: {
              'x-arcesb-authtoken': config.db.arcesbHeader,
              'Content-Type': 'application/json',
              'Access-Control-Allow-Origin': '*',
            },
          }).then(function (response) {
            //console.log(response);
            console.log("X12",response.data);
          });
         var postdata1 = {
            
              "workspace":"ArcesbTest",
              "PortType":"XMLMap",
              "ConnectorId":"Map_Transaction_Log",
              "Source":"/opt/files/EDIdata850.xml",
              "Destination":"/opt/files/transactionLogXML.xml",
              "SendToPort":"TransactionLogJSON"
              }
          /*var postdata = {
            "Workspaceid": "ArcesbTest"
            
          };*/
         // console.log(postdata1);
        await axios( {
            method: 'post',        
            url:config.db.ARCESB_URL+'connectors',
            data: postdata1,
            headers: {
              'x-arcesb-authtoken': config.db.arcesbHeader,
              'Content-Type': 'application/json',
              'Access-Control-Allow-Origin': '*',
            },
          }).then(function (response) {
            //console.log(response);
            console.log("Xml",response.data);
          });
          var postdata2 = {
            
            "ConnectorId":"TransactionLogJSON",
                    "PortType":"JSON",
                    "workspace":"ArcesbTest",
                    "SendToPort":"elasticsearchconnector"
            }
        /*var postdata = {
          "Workspaceid": "ArcesbTest"
          
        };*/
         // console.log(postdata2);
        await axios( {
            method: 'post',        
            url:config.db.ARCESB_URL+'connectors',
            data: postdata2,
            headers: {
              'x-arcesb-authtoken': config.db.arcesbHeader,
              'Content-Type': 'application/json',
              'Access-Control-Allow-Origin': '*',
            },
          }).then(function (response) {
            //console.log(response);
            console.log("json",response.data);
          });
          var postdata3 = {
            "workspace":"ArcesbTest",    
            "PortType":"REST",
            "ConnectorId":"elasticsearchconnector",
            "Method":"POST",
            "Url":"https://elastic.arcesb.changemaker.community/arcesb/test",
            "Authorization":"Basic",
            "UserName":"elastic",
            "Password":"elastic",
            "BodyType":"raw",
            "RawContentType":"application/json"
            }
          /*var postdata = {
            "Workspaceid": "ArcesbTest"
            
          };*/
         // console.log(postdata3);
         await axios( {
            method: 'post',        
            url:config.db.ARCESB_URL+'connectors',
            data: postdata3,
            headers: {
              'x-arcesb-authtoken': config.db.arcesbHeader,
              'Content-Type': 'application/json',
              'Access-Control-Allow-Origin': '*',
            },
          }).then(function (response) {
            //console.log(response);
            console.log("Rest",response.data);
          });
    
        }) 
      },
    async fetchConnectorsAPI(root, { input }) {
      return new Promise((resolve, reject) => {
        var connectorURL =
        config.db.ARCESB_URL +'connectors(' +
          input.connectorID +
          ')';
        axios(connectorURL, {
          method: 'GET',
          headers: {
            'x-arcesb-authtoken': config.db.arcesbHeader,
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
          },
        }).then(function (response) {
          console.log(response);
          console.log(response.data);
        });
      });
    },
    async postConnectorsAPI(root, { input }) {
      return new Promise((resolve, reject) => {
        var postdata = {
          '@odata.type': 'ArcESBAPI.Ports',
          ConnectorId: input.connectorID,
          Workspace: input.workspaceUUID,
          ConnectorType: input.ConnectorType,
        };

        axios(
          config.db.ARCESB_URL +'connectors',
          {
            method: 'POST',
            processData: false,
            data: JSON.stringify(postdata),
            headers: {
              'x-arcesb-authtoken': config.db.arcesbHeader,
              'Content-Type': 'application/json',
              'Access-Control-Allow-Origin': '*',
            },
          }
        ).then(function (response) {
          console.log(response);
          console.log(response.data);
        });
      });
    },
    async fetchConnectorsByWorkspaceUUID(root, { workspaceuuid }) {
      return new Promise((resolve, reject) => {
        var sql, values;
        sql =
          ' select `idConnectors`, `connectorsUUID`,ConnectorType,connectorID,ParentConnector, workspaceUUID,(SELECT `workspaceName` FROM `workspace_arcesb` WHERE `workspaceUUID`=`connectors_arcesb`.`workspaceUUID`) as `workspaceName`, `createdOn`, `updatedOn` from `connectors_arcesb` where `workspaceUUID`=? ORDER BY `createdOn` DESC  ';
        values = [workspaceuuid];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    allConnectors: (root, args, context, info) => {
      //sql query for allConnectors
      return new Promise((resolve, reject) => {
        connection.query(
          'SELECT * from connectors_arcesb ORDER BY  createdOn DESC ',
          function (err, post) {
            if (err) {
              reject(err);
            } else {
              resolve(post);
            }
          }
        );
      });
    },
  },
  Mutation: {
    createConnectorsAPI(root, { input }) {
      return new Promise((resolve, reject) => {
        console.log(input);
        var sql =
          'INSERT INTO connectors_arcesb (connectorID,workspaceUUID,ConnectorType,ParentConnector) VALUES(?,?,?,?)';

        var values = [
          input.connectorID,
          input.workspaceUUID,
          input.ConnectorType,
          input.ParentConnector,
        ];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
};

const connectorsSchema = makeExecutableSchema({
  typeDefs,
  resolvers,
  formatError: (err) => {
    // Don't give the specific errors to the client.
    if (err.message.startsWith('Database Error: ')) {
      return new Error('sql error');
    }

    // Otherwise return the original error.  The error can also
    // be manipulated in other ways, so long as it's returned.
    return err;
  },
});

module.exports = connectorsSchema;
