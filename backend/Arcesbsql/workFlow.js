var { makeExecutableSchema } = require('graphql-tools');
const config = require('../dbConfig');
const mysql = require('mysql');
const withAuthfn = require('../verifyMiddleware/withAuth');
const axios = require('axios');
var connection = require('../database');

/*var MySQLStore = require('connect-mysql'); // mysql session store
var options = {
      config: {
        host: config.db.host,
        user: config.db.user,
        password: config.db.password,
        database: config.db.database,
        port: config.db.port,
      },
      pool:true,
      keepalive:true

    };

    connection=MySQLStore(options)*/ /*= mysql.createPool({
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database,
  port: config.db.port,
});*/
/*function handleDisconnect() {
  connection = mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database,
    port: config.db.port,
  }); // Recreate the connection, since
  // the old one cannot be reused.

  connection.connect(function (err) {
    // The server is either down
    if (err) {
      // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    } // to avoid a hot loop, and to allow our node script to
  }); // process asynchronous requests in the meantime.
  // If you're also serving http, display a 503 error.
  connection.on('error', function (err) {
    console.log('db error', err);
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      // Connection to the MySQL server is usually
      handleDisconnect(); // lost due to either server restart, or a
    } else {
      // connnection idle timeout (the wait_timeout
      throw err; // server variable configures this)
    }
  });
}

handleDisconnect();*/
//connection.connect();
const typeDefs = `
scalar Date

type workFlowValues{
   
    workFlowUUID:String
    workFlowName:String
    senderQualifier:String
   
    createdOn:Date
    updatedOn:Date
  
}
input workFlowInput{
  senderQualifier:String
 
   workFlowName:String
}

type Query{
    fetchworkFlowAPI(input:workFlowInput):[workFlowValues]
    fetchworkFlow:[workFlowValues]
    postworkFlowAPI(input:workFlowInput):[workFlowValues]
}

type Mutation{
  createworkFlowAPI(input:workFlowInput): workFlowValues
}


`;

const resolvers = {
  Query: {
    fetchworkFlowAPI(root, { input }) {
      console.log(input);
      return new Promise((resolve, reject) => {
        var workFlowURL =
        config.db.ARCESB_URL +'workFlows(`' +
          input.workFlowName +
          '`)';
        console.log(workFlowURL);
        axios(workFlowURL, {
          method: 'GET',
          headers: {
            'x-arcesb-authtoken': config.db.arcesbHeader,
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
          },
        }).then(function (response) {
          console.log(response);
          console.log(response.data);
        });
      });
    },
    postworkFlowAPI(root, { input }) {
      console.log(input);
      return new Promise(async(resolve, reject) => {
        var postdata = {
          '@odata.type': 'ArcESBAPI.workFlows',
          workFlowid: input.workFlowName,
        };
        /*var postdata = {
          "workFlowid": "ArcesbTest"
          
        };*/
        console.log(postdata);
       await axios( {
            method: 'post',        
         url:config.db.ARCESB_URL+'workFlows',
          data: postdata,
          headers: {
            'x-arcesb-authtoken': config.db.arcesbHeader,
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
          },
        }).then(function (response) {
          //console.log(response);
          console.log(response.data);
        });
      });
    },
    fetchworkFlow: (root, args, context, info) => {
      //sql query for fetch workFlow_arcesb
      console.log("fetchworksapve")
      return new Promise((resolve, reject) => {
        connection.query(
          'SELECT * from workFlow_arcesb ORDER BY  createdOn DESC ',
          function (err, post) {
            if (err) {
              reject(err);
            } else {
              resolve(post);
            }
          }
        );
      });
    },
  },
  Mutation: {
    async createworkFlowAPI(root, { input }) {
      return new Promise((resolve, reject) => {
        var sql = 'INSERT INTO `workFlow_arcesb`( `workFlowName`, `senderQualifier`) VALUES (?,?)';
        var workFlowNameval=input.workFlowName.replace(/ /g,"");
        var values = [input.workFlowNameval,
        input.senderQualifier
      ];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
};

const workFlowSchema = makeExecutableSchema({
  typeDefs,
  resolvers,
  formatError: (err) => {
    // Don't give the specific errors to the client.
    if (err.message.startsWith('Database Error: ')) {
      return new Error('sql error');
    }

    // Otherwise return the original error.  The error can also
    // be manipulated in other ways, so long as it's returned.
    return err;
  },
});

module.exports = workFlowSchema;
