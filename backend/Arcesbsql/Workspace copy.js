var { makeExecutableSchema } = require('graphql-tools');
const config = require('../dbConfig');
const mysql = require('mysql');
const withAuthfn = require('../verifyMiddleware/withAuth');
const axios = require('axios');
var connection = require('../database');/*= mysql.createPool({
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database,
  port: config.db.port,
});*/
/*function handleDisconnect() {
  connection = mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database,
    port: config.db.port,
  }); // Recreate the connection, since
  // the old one cannot be reused.

  connection.connect(function (err) {
    // The server is either down
    if (err) {
      // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    } // to avoid a hot loop, and to allow our node script to
  }); // process asynchronous requests in the meantime.
  // If you're also serving http, display a 503 error.
  connection.on('error', function (err) {
    console.log('db error', err);
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      // Connection to the MySQL server is usually
      handleDisconnect(); // lost due to either server restart, or a
    } else {
      // connnection idle timeout (the wait_timeout
      throw err; // server variable configures this)
    }
  });
}

handleDisconnect();*/
//connection.connect();
const typeDefs = `
scalar Date

type workspaceValues{
    idworkspace:String
    workspaceUUID:String
    workspaceName:String
    createdOn:Date
    updatedOn:Date
}
input workspaceInput{
  workspaceUUID:String
   workspaceName:String
}

type Query{
    fetchWorkSpaceAPI(input:workspaceInput):[workspaceValues]
    fetchWorkSpace:[workspaceValues]
    postWorkSpaceAPI(input:workspaceInput):[workspaceValues]
}

type Mutation{
  createWorkSpaceAPI(input:workspaceInput): workspaceValues
}


`;

const resolvers = {
  Query: {
    fetchWorkSpaceAPI(root, { input }) {
      console.log(input);
      return new Promise((resolve, reject) => {
        var workspaceURL =
        config.db.ARCESB_URL +'workspaces(`' +
          input.workspaceName +
          '`)';
        console.log(workspaceURL);
        axios(workspaceURL, {
          method: 'GET',
          headers: {
            'x-arcesb-authtoken': config.db.arcesbHeader,
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
          },
        }).then(function (response) {
          console.log(response);
          console.log(response.data);
        });
      });
    },
    postWorkSpaceAPI(root, { input }) {
      console.log(input);
      return new Promise(async(resolve, reject) => {
        var postdata = {
          '@odata.type': 'ArcESBAPI.Workspaces',
          Workspaceid: input.workspaceName,
        };
        /*var postdata = {
          "Workspaceid": "ArcesbTest"
          
        };*/
        console.log(postdata);
       await axios( {
            method: 'post',        
         url:config.db.ARCESB_URL+'workspaces',
          data: postdata,
          headers: {
            'x-arcesb-authtoken': config.db.arcesbHeader,
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
          },
        }).then(function (response) {
          //console.log(response);
          console.log(response.data);
        });
      });
    },
    fetchWorkSpace: (root, args, context, info) => {
      //sql query for fetch workspace_arcesb
      return new Promise((resolve, reject) => {
        connection.query(
          'SELECT * from workspace_arcesb ORDER BY  createdOn DESC ',
          function (err, post) {
            if (err) {
              reject(err);
            } else {
              resolve(post);
            }
          }
        );
      });
    },
  },
  Mutation: {
    async createWorkSpaceAPI(root, { input }) {
      return new Promise((resolve, reject) => {
        var sql = 'INSERT INTO workspace_arcesb (workspaceName) VALUES(?)';

        var values = [input.workspaceName];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
};

const workspaceSchema = makeExecutableSchema({
  typeDefs,
  resolvers,
  formatError: (err) => {
    // Don't give the specific errors to the client.
    if (err.message.startsWith('Database Error: ')) {
      return new Error('sql error');
    }

    // Otherwise return the original error.  The error can also
    // be manipulated in other ways, so long as it's returned.
    return err;
  },
});

module.exports = workspaceSchema;
