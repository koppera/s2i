var { makeExecutableSchema } = require('graphql-tools');
const config = require('../dbConfig');
const mysql = require('mysql');
const withAuthfn = require('../verifyMiddleware/withAuth');
const axios = require('axios');
var connection = require('../database');

/*var MySQLStore = require('connect-mysql'); // mysql session store
var options = {
      config: {
        host: config.db.host,
        user: config.db.user,
        password: config.db.password,
        database: config.db.database,
        port: config.db.port,
      },
      pool:true,
      keepalive:true

    };

    connection=MySQLStore(options)*/ /*= mysql.createPool({
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database,
  port: config.db.port,
});*/
/*function handleDisconnect() {
  connection = mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database,
    port: config.db.port,
  }); // Recreate the connection, since
  // the old one cannot be reused.

  connection.connect(function (err) {
    // The server is either down
    if (err) {
      // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    } // to avoid a hot loop, and to allow our node script to
  }); // process asynchronous requests in the meantime.
  // If you're also serving http, display a 503 error.
  connection.on('error', function (err) {
    console.log('db error', err);
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      // Connection to the MySQL server is usually
      handleDisconnect(); // lost due to either server restart, or a
    } else {
      // connnection idle timeout (the wait_timeout
      throw err; // server variable configures this)
    }
  });
}

handleDisconnect();*/
//connection.connect();


const typeDefs = `
scalar Date

type workspaceValues{
   
    workspaceUUID:String
    workspaceName:String
    senderQualifier:String
    APIStatus:String
    createdOn:Date
    updatedOn:Date
  
}
type workspaceStatus{
  statusAPI:String
  msg:String
}
input workspaceInput{
  workspaceUUID:String
  senderQualifier:String
  APIStatus:String
   workspaceName:String
   updatedOn:Date
}

type Query{
    fetchWorkSpaceAPI(input:workspaceInput):[workspaceValues]
    fetchWorkSpace:[workspaceValues]
    postWorkSpaceAPI(input:workspaceInput):workspaceStatus
}

type Mutation{
  createWorkSpaceAPI(input:workspaceInput): workspaceValues
  updateWorkSpace(input:workspaceInput): workspaceValues
  removeWorkSpace(input:workspaceInput): workspaceValues
}


`;

const resolvers = {
  Query: {
    fetchWorkSpaceAPI(root, { input }) {
      console.log(input);
      return new Promise((resolve, reject) => {
        var workspaceURL =
          config.db.ARCESB_URL + 'workspaces(`' + input.workspaceName + '`)';
        console.log(workspaceURL);
        axios(workspaceURL, {
          method: 'GET',
          headers: {
            'x-arcesb-authtoken': config.db.arcesbHeader,
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
          },
        }).then(function (response) {
          console.log(response);
          console.log(response.data);
        });
      });
    },
    postWorkSpaceAPI(root, { input }) {
      console.log(input);
      return new Promise(async (resolve, reject) => {
        var postdata = {
          '@odata.type': 'ArcESBAPI.Workspaces',
          Workspaceid: input.workspaceName,
        };
        /*var postdata = {
          "Workspaceid": "ArcesbTest"
          
        };*/
        console.log(postdata);
        await axios({
          method: 'post',
          url: config.db.ARCESB_URL + 'workspaces',
          data: postdata,
          headers: {
            'x-arcesb-authtoken': config.db.arcesbHeader,
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
          },
        })
          .then(function (response) {
            //console.log(response);
            console.log('RESULT', response);
            var status;
            if (response.status === 200) {
              status = 'Active';
            } else {
              status = 'Inactive';
            }
            console.log(input.workspaceUUID);
            var sql =
              'UPDATE `workspace_arcesb` SET `APIStatus`=' +
              mysql.escape(status) +
              ' WHERE workspaceUUID=' +
              mysql.escape(input.workspaceUUID);
            console.log(sql);
            connection.query(sql, function (err, updatedStatus) {
              if (err) reject(err);
              else
                resolve({
                  statusAPI: 'success',
                  msg: 'Partner Sync Successfully',
                });
            });
          })
          .catch((error) => {
            console.log(error);
            reject(error);
          });
      });
    },
    fetchWorkSpace: (root, args, context, info) => {
      //sql query for fetch workspace_arcesb
      console.log('fetchworksapve');
      return new Promise((resolve, reject) => {
        connection.query(
          'SELECT * from workspace_arcesb ORDER BY  createdOn DESC ',
          function (err, post) {
            if (err) {
              reject(err);
            } else {
              resolve(post);
            }
          }
        );
      });
    },
  },
  Mutation: {
    async createWorkSpaceAPI(root, { input }) {
      return new Promise((resolve, reject) => {
        var sql =
          'INSERT INTO `workspace_arcesb`( `workspaceName`, `senderQualifier`) VALUES (?,?)';

        var values = [input.workspaceName, input.senderQualifier];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async updateWorkSpace(root, { input }) {
      console.log(input);
      //sql query for update workspace
      return new Promise((resolve, reject) => {
        var sql =
          'UPDATE workspace_arcesb SET workspaceName = ?,senderQualifier=?,updatedOn=? WHERE workspaceUUID = ?';

        var values = [
          input.workspaceName,
          input.senderQualifier,

          new Date(),
          input.workspaceUUID,
        ];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async removeWorkSpace(root, { input }) {
      console.log(input);
      //sql query for create workspace
      return new Promise((resolve, reject) => {
        var sql = 'DELETE FROM `workspace_arcesb` WHERE `workspaceUUID` = ?;';

        var values = [input.workspaceUUID];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
};

const workspaceSchema = makeExecutableSchema({
  typeDefs,
  resolvers,
  formatError: (err) => {
    // Don't give the specific errors to the client.
    if (err.message.startsWith('Database Error: ')) {
      return new Error('sql error');
    }

    // Otherwise return the original error.  The error can also
    // be manipulated in other ways, so long as it's returned.
    return err;
  },
});

module.exports = workspaceSchema;
