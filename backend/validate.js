const jwt = require("jsonwebtoken");
const config = require('./dbConfig');

async function isTokenValid(token) {
    if (token) {
      const bearerToken = token.split(" ");
  
      const result = new Promise((resolve, reject) => {
        jwt.verify(
          bearerToken[1],
          config.db.Secret,
          (error, decoded) => {
            if (error) {
              resolve({ error });
            }
            if (decoded) {
              console.log("decoded------------",decoded)
              resolve({ decoded });
            }
          }
        );
      });
      console.log("result------------",result)
      return result;
    }
  
    return { error: "No token provided" };
  }
  
  module.exports = isTokenValid;