var { makeExecutableSchema } = require('graphql-tools');
const config = require('../dbConfig');
const mysql = require('mysql');
const withAuthfn=require('../verifyMiddleware/withAuth');
var connection = require('../database');

/*var MySQLStore = require('connect-mysql'); // mysql session store
var options = {
      config: {
        host: config.db.host,
        user: config.db.user,
        password: config.db.password,
        database: config.db.database,
        port: config.db.port,
      },
      pool:true,
      keepalive:true

    };

    connection=MySQLStore(options)*/ /*= mysql.createPool({
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database,
  port: config.db.port,
});*/
/*/*function handleDisconnect() {
  connection = mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database,
    port: config.db.port,
  }); // Recreate the connection, since
  // the old one cannot be reused.

  connection.connect(function (err) {
    // The server is either down
    if (err) {
      // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    } // to avoid a hot loop, and to allow our node script to
  }); // process asynchronous requests in the meantime.
  // If you're also serving http, display a 503 error.
  connection.on('error', function (err) {
    console.log('db error', err);
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      // Connection to the MySQL server is usually
      handleDisconnect(); // lost due to either server restart, or a
    } else {
      // connnection idle timeout (the wait_timeout
      throw err; // server variable configures this)
    }
  });
}

handleDisconnect();*/

//connection.connect();
const typeDefs = `
scalar Date

type Address{
  idAddress:String
  addressUUID:String
  addressNameID:String
  addressLines1:String
  addressLines2:String
  addressLines3:String
  city:String
  state:String
  country:String
  zip:String
  addressStatus:String
  addressTypeUUID:String
  addressTypeName:String
  partnerProfileUUID : String
  createdOn:Date
  lastUpdatedOn:Date
  contactUUID:String
}

input AddressInput{
  idAddress:String
  addressUUID:String
  addressNameID:String
  addressLines1:String
  addressLines2:String
  addressLines3:String
  city:String
  state:String
 country:String
 zip:String
addressStatus:String
addressTypeUUID:String
partnerProfileUUID : String
contactUUID:String
}
type Query{
   allAddress:[Address]
   fetchAddressByUUID(addressUUID:String,contactUUID:String):[Address]
   fetchAddressByPartnerProfile(partnerProfileUUID:String,searchInput:String,contact:Int): [Address]
  }

type Mutation{
   createAddress(input:AddressInput): Address
   updateAddress(input:AddressInput): Address
   removeAddress(input:AddressInput):Address
}

`;

const resolvers = {
  Query: {
    allAddress: (root, args, context, info) => {
      //sql query for fetch all address
      return new Promise((resolve, reject) => {
        connection.query(
          'SELECT * from address ORDER BY Createdon DESC;',
          function (err, post) {
            if (err) {
              reject(err);
            } else {
              resolve(post);
            }
          }
        );
      });
    },
    async fetchAddressByUUID(root, { addressUUID, contactUUID }) {
      //sql query for fetch all address
      return new Promise((resolve, reject) => {
        var sql;
        if (addressUUID != null) {
          sql =
            'SELECT * from address where addressUUID=' +
            mysql.escape(addressUUID);
        } else {
          sql =
            'SELECT * from address where contactUUID=' +
            mysql.escape(contactUUID);
        }
        console.log(addressUUID, contactUUID, sql);
        connection.query(sql, function (err, post) {
          if (err) {
            reject(err);
          } else {
            resolve(post);
          }
        });
      });
    },
    async fetchAddressByPartnerProfile(
      root,
      { partnerProfileUUID, searchInput, contact }
    ) {
      //sql query for fetch address by partner profile id

      return new Promise((resolve, reject) => {
        var sql, values;
        console.log(
          partnerProfileUUID,
          'searchInput',
          searchInput,
          'contact',
          contact
        );

        if (searchInput === '') {
          var contactStmt = '';
          if (contact) {
            contactStmt = 'AND addressStatus="Active" AND contactUUID IS NULL';
          }
          console.log('no search');
          console.log(
            'partnerProfileUUID',
            partnerProfileUUID,
            'searchInput',
            searchInput
          );
          sql =
            'Select addressUUID,addressNameID,addressLines1, addressLines2,addressLines3,city,state,country, zip,addressStatus,addressTypeUUID,(SELECT `addressTypeName` FROM `addresstype` WHERE `addressTypeUUID`=`address`.`addressTypeUUID`) as `addressTypeName`, partnerProfileUUID, contactUUID,createdOn,lastUpdatedOn from address where ( partnerProfileUUID=' +
            mysql.escape(partnerProfileUUID) +
            contactStmt +
            ' ) ORDER BY createdOn DESC;';
          console.log(sql);
        } /*else {console.log("no search")
          var sql =
            ' Select * from address where (addressNameID= ' +
            mysql.escape(searchInput) +
            ' OR addressLines1=' +
            mysql.escape(searchInput) +
            ' OR addressLines2=' +
            mysql.escape(searchInput) +
            ' OR addressLines3=' +
            mysql.escape(searchInput) +
            ' OR city=' +
            mysql.escape(searchInput) +
            'OR state=' +
            mysql.escape(searchInput) +
            ' OR country=' +
            mysql.escape(searchInput) +
            ' OR zip=' +
            mysql.escape(searchInput) +
            ' OR addressStatus=' +
            mysql.escape(searchInput) +
            ') AND ( partnerProfileUUID=' +
            mysql.escape(partnerProfileUUID) +
            ') ORDER BY 	createdOn DESC;';
        }*/

        connection.query(sql, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log('result', result);
            resolve(result);
          }
        });
      });
    },
  },
  Mutation: {
    async createAddress(root, { input }) {
      console.log(input.addressUUID);
      //sql query for create userTypes
      return new Promise((resolve, reject) => {
        var sql =
          'INSERT INTO address (addressNameID,addressLines1,addressLines2,addressLines3,city,state,country,zip,addressStatus,addressTypeUUID,partnerProfileUUID,contactUUID) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)';

        var values = [
          input.addressNameID,
          input.addressLines1,
          input.addressLines2,
          input.addressLines3,
          input.city,
          input.state,
          input.country,
          input.zip,
          input.addressStatus,
          input.addressTypeUUID,
          input.partnerProfileUUID,
          input.contactUUID,
        ];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(input.addressUUID);
            console.log(result);
            if (input.addressUUID == null) {
              var sqlupdate =
                'UPDATE contact SET addressUUID=?,lastUpdatedOn=? WHERE contactUUID = ?';

              var valuesUpdate = [null, new Date(), input.contactUUID];
              connection.query(sqlupdate, valuesUpdate, function (
                errs,
                results
              ) {
                if (err) {
                  reject(errs);
                } else {
                  console.log(results);
                  resolve(results);
                }
              });
            } else {
              resolve(result);
            }
          }
        });
      });
    },
    async updateAddress(root, { input }) {
      console.log(input);
      //sql query for update userTypes
      return new Promise((resolve, reject) => {
        var sql =
          'UPDATE address SET addressNameID = ?,addressLines1=? ,addressLines2=?, addressLines3=?,city=?,state=?,country=?,zip=?,addressStatus=?,addressTypeUUID=?,partnerProfileUUID=?,lastUpdatedOn=? WHERE addressUUID = ?';

        var values = [
          input.addressNameID,
          input.addressLines1,
          input.addressLines2,
          input.addressLines3,
          input.city,
          input.state,
          input.country,
          input.zip,
          input.addressStatus,
          input.addressTypeUUID,
          input.partnerProfileUUID,
          new Date(),
          input.addressUUID,
        ];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);

            if (input.addressStatus == 'Inactive') {
              var sqladdress =
                'UPDATE contact SET addressUUID=?,lastUpdatedOn=? WHERE addressUUID = ?';
              var valuesaddress = [null, new Date(), input.addressUUID];
              connection.query(sqladdress, valuesaddress, function (
                errs,
                results
              ) {
                if (errs) {
                  reject(errs);
                } else {
                  console.log(results);
                  resolve(results);
                }
              });
            } else {
              resolve(result);
            }
          }
        });
      });
    },
    async removeAddress(root, { input }) {
      console.log(input);
      //sql query for create userTypes
      return new Promise((resolve, reject) => {
        var sql = 'DELETE FROM `address` WHERE `addressUUID` = ?;';

        var values = [input.addressUUID];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
};

const addressSchema = makeExecutableSchema({
  typeDefs,
  resolvers,
  formatError: (err) => {
    // Don't give the specific errors to the client.
    if (err.message.startsWith('Database Error: ')) {
      return new Error('sql error');
    }

    // Otherwise return the original error.  The error can also
    // be manipulated in other ways, so long as it's returned.
    return err;
  },
});

module.exports = addressSchema;
