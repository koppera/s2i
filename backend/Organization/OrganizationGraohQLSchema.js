var { makeExecutableSchema } = require('graphql-tools');
const config = require('../dbConfig');
const mysql = require('mysql');
const withAuthfn=require('../verifyMiddleware/withAuth');
var connection = require('../database'); /*= mysql.createPool({
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database,
  port: config.db.port,
});*/
/*function handleDisconnect() {
  connection =  mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database,
    port: config.db.port,
  });; // Recreate the connection, since
                                                  // the old one cannot be reused.

  connection.connect(function(err) {              // The server is either down
    if(err) {                                     // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    }                                     // to avoid a hot loop, and to allow our node script to
  });                                     // process asynchronous requests in the meantime.
                                          // If you're also serving http, display a 503 error.
  connection.on('error', function(err) {
    console.log('db error', err);
    if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
      handleDisconnect();                         // lost due to either server restart, or a
    } else {                                      // connnection idle timeout (the wait_timeout
      throw err;                                  // server variable configures this)
    }
  });
}

handleDisconnect();*/


//connection.connect();
const typeDefs = `
scalar Date

type Organization{
  idOrganization:String
  organizationUUID:String
  organizatonName:String
  organizationDescription:String
  createdOn:Date
  lastUpdatedOn:Date
}

input OrganizationInput{
    idOrganization:String
    organizationUUID:String
    organizatonName:String
    organizationDescription:String
}
type Query{
   allOrganization:[Organization]
   fetchOrganization(searchInput:String): [Organization]
}

type Mutation{
   createOrganization(input:OrganizationInput): Organization
   updateOrganization(input:OrganizationInput): Organization
   removeOrganization(input:OrganizationInput):Organization
}

`;

const resolvers = {
  Query: {
    allOrganization: (root, args, context, info) => {
      //sql query for fetch all Organization
      return new Promise((resolve, reject) => {
        connection.query(
          'SELECT * from organization ORDER BY  createdOn DESC;',
          function (err, post) {
            if (err) {
              reject(err);
            } else {
              resolve(post);
            }
          }
        );
      });
    },
    async fetchOrganization(root, { searchInput }) {
      //sql query for fetch Organization

      return new Promise((resolve, reject) => {
        var sql, values;
        if (searchInput == '') {
          sql = 'Select * from organization ORDER BY  createdOn DESC';
          console.log(sql);
        } else {
          var sql =
            ' Select * from organization where (organizatonName= ' +
            mysql.escape(searchInput) +
            'OR organizationDescription=' +
            mysql.escape(searchInput) +
            ')  ORDER BY  createdOn DESC ';
        }

        connection.query(sql, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
  Mutation: {
    async createOrganization(root, { input }) {
      console.log(input);
      //sql query for create Organization
      return await new Promise((resolve, reject) => {
        var sql =
          'INSERT INTO organization (organizatonName,organizationDescription) VALUES(?,?)';

        var values = [input.organizatonName, input.organizationDescription];
        console.log(values);
        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async updateOrganization(root, { input }) {
      console.log(input);
      //sql query for update Organization
      return new Promise((resolve, reject) => {
        var sql =
          'UPDATE organization SET organizatonName = ?,organizationDescription=? ,lastUpdatedOn=? WHERE organizationUUID = ?';

        var values = [
          input.organizatonName,
          input.organizationDescription,
          new Date(),
          input.organizationUUID,
        ];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async removeOrganization(root, { input }) {
      console.log(input);
      //sql query for create userTypes
      return new Promise((resolve, reject) => {
        var sql = 'DELETE FROM `organization` WHERE `organizationUUID` = ?;';

        var values = [input.organizationUUID];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
};

const OrganizationSchema = makeExecutableSchema({
  typeDefs,
  resolvers,
  formatError: (err) => {
    // Don't give the specific errors to the client.
    if (err.message.startsWith('Database Error: ')) {
      return new Error('sql error');
    }

    // Otherwise return the original error.  The error can also
    // be manipulated in other ways, so long as it's returned.
    return err;
  },
});

module.exports = OrganizationSchema;
