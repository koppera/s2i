var { makeExecutableSchema } = require('graphql-tools');
const config = require('../dbConfig');
const mysql = require('mysql');
const withAuthfn=require('../verifyMiddleware/withAuth');
var connection = require('../database'); /*= mysql.createPool({
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database,
  port: config.db.port,
});*/
/*function handleDisconnect() {
  connection =  mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database,
    port: config.db.port,
  });; // Recreate the connection, since
                                                  // the old one cannot be reused.

  connection.connect(function(err) {              // The server is either down
    if(err) {                                     // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    }                                     // to avoid a hot loop, and to allow our node script to
  });                                     // process asynchronous requests in the meantime.
                                          // If you're also serving http, display a 503 error.
  connection.on('error', function(err) {
    console.log('db error', err);
    if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
      handleDisconnect();                         // lost due to either server restart, or a
    } else {                                      // connnection idle timeout (the wait_timeout
      throw err;                                  // server variable configures this)
    }
  });
}

handleDisconnect();*/


//connection.connect();
const typeDefs = `
scalar Date

type Service{
  idService:String
  serviceUUID:String
  name:String
  description:String
  createdOn:Date
  lastUpdatedOn:Date
}

input ServiceInput{
    idService:String
    serviceUUID:String
    name:String
    description:String
}
type Query{
   allService:[Service]
   fetchService(searchInput:String): [Service]
}

type Mutation{
   createService(input:ServiceInput): Service
   updateService(input:ServiceInput): Service
   removeService(input:ServiceInput):Service
}

`;

const resolvers = {
  Query: {
    allService: (root, args, context, info) => {
      //sql query for fetch all Service
      return new Promise((resolve, reject) => {
        connection.query(
          'SELECT * from service ORDER BY  createdOn DESC ',
          function (err, post) {
            if (err) {
              reject(err);
            } else {
              resolve(post);
            }
          }
        );
      });
    },
    async fetchService(root, { searchInput }) {
      //sql query for fetch Service

      return new Promise((resolve, reject) => {
        var sql, values;
        if (searchInput == '') {
          sql = 'Select * from service ORDER BY  createdOn DESC ';
          console.log(sql);
        } else {
          var sql =
            ' Select * from service where (name= ' +
            mysql.escape(searchInput) +
            'OR description=' +
            mysql.escape(searchInput) +
            ')ORDER BY  createdOn DESC  ';
        }

        connection.query(sql, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
  Mutation: {
    async createService(root, { input }) {
      console.log(input);
      //sql query for create Service
      return new Promise((resolve, reject) => {
        var sql = 'INSERT INTO service (name,description) VALUES(?,?)';

        var values = [input.name, input.description];
        console.log(values);
        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async updateService(root, { input }) {
      console.log(input);
      //sql query for update Service
      return new Promise((resolve, reject) => {
        var sql =
          'UPDATE service SET name = ?,description=? ,lastUpdatedOn=? WHERE serviceUUID = ?';

        var values = [
          input.name,
          input.description,
          new Date(),
          input.serviceUUID,
        ];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async removeService(root, { input }) {
      console.log(input);
      //sql query for create userTypes
      return new Promise((resolve, reject) => {
        var sql = 'DELETE FROM `service` WHERE `serviceUUID` = ?;';

        var values = [input.serviceUUID];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
};

const ServiceSchema = makeExecutableSchema({
  typeDefs,
  resolvers,
  formatError: (err) => {
    // Don't give the specific errors to the client.
    if (err.message.startsWith('Database Error: ')) {
      return new Error('sql error');
    }

    // Otherwise return the original error.  The error can also
    // be manipulated in other ways, so long as it's returned.
    return err;
  },
});

module.exports = ServiceSchema;
