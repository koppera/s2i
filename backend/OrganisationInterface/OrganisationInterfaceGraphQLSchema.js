var { makeExecutableSchema } = require('graphql-tools');
const config = require('../dbConfig');
const mysql = require('mysql');
const withAuthfn = require('../verifyMiddleware/withAuth');
var connection = require('../database');

/*var MySQLStore = require('connect-mysql'); // mysql session store
var options = {
      config: {
        host: config.db.host,
        user: config.db.user,
        password: config.db.password,
        database: config.db.database,
        port: config.db.port,
      },
      pool:true,
      keepalive:true

    };

    connection=MySQLStore(options)*/ /*= mysql.createPool({
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database,
  port: config.db.port,
});*/
/*/*function handleDisconnect() {
  connection = mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database,
    port: config.db.port,
  }); // Recreate the connection, since
  // the old one cannot be reused.

  connection.connect(function (err) {
    // The server is either down
    if (err) {
      // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    } // to avoid a hot loop, and to allow our node script to
  }); // process asynchronous requests in the meantime.
  // If you're also serving http, display a 503 error.
  connection.on('error', function (err) {
    console.log('db error', err);
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      // Connection to the MySQL server is usually
      handleDisconnect(); // lost due to either server restart, or a
    } else {
      // connnection idle timeout (the wait_timeout
      throw err; // server variable configures this)
    }
  });
}

handleDisconnect();*/

const axios = require('axios');
//connection.connect();
const typeDefs = `
scalar Date

type Interface1{
  interfaceUUID:String
  name:String

}

type Project1{
  ProjectUUID:String
  ProjectName:String  
  organizationUUID:String  
}

type Partner1{
  PartnerProfileUUID:String
  CorporationName:String

  
}

type Organization1{
  organizationUUID:String
  organizatonName:String
}

type OrgInterface{
  idOrgInterface:String
  orgInterfaceUUID:String
  interfaceUUID :String
  projectUUID :String
  sourceProfileUUID:String
  targetProfileUUID:String
  sourceorganizationUnit:String
  targetorganizationUnit:String
  docTypeUUID:String
  serviceUUID:String
  organizationUUID: String
  APIStatus:String
  createdOn:Date
  lastUpdatedOn:Date

}

type Service1{ 
  serviceUUID:String
  name:String
}

type DocumentType1{
  documentTypeUUID:String
  name:String
}

input OrgInterfaceInput{
  orgInterfaceUUID:String
  interfaceUUID :String
  projectUUID :String
  sourceProfileUUID:String
  targetProfileUUID:String
  docTypeUUID:String
  serviceUUID:String
  organizationUUID: String
  targetorganizationUnit:String
  sourceorganizationUnit:String
  APIStatus:String
}
type Success{
  status:String
  msg:String
}
type orgIAPISuccess{
  statusAPI:String
  msg:String
}

type Success1{
  success:String
}
type Query{
  allOrgInterfaceAPI(input:OrgInterfaceInput):orgIAPISuccess
  organisationInterface(searchInput:String):[OrgInterface]
   fetchOrgInterface(partnerProfileUUID:String): [OrgInterface]
   allPartners1(search:String):[Partner1]  
    allPartnersByStatus(search:String):[Partner1]
    allPartnersByStatusandOrgUUID(organizationUUID:String):[Partner1]
    allPartnersByStatusandProjandorgUUID(projectUUID:String):[Partner1]
   fetchService1(searchInput:String): [Service1]
   fetchDocumentType1(searchInput:String): [DocumentType1]
   fetchOrganization1(searchInput:String): [Organization1]
   fetchInterface1(searchInput:String): [Interface1]
   fetchProject1(searchInput:String): [Project1]
   fetchProjectByUUID(projectUUID:String): [Project1]
   fetchOrgInterfaceByUUIDs(organizationUUID:String,projectUUID:String,interfaceUUID:String):[OrgInterface]
   organisationInterfaceByOrgUUID(organizationUUID:String):[OrgInterface]
   fetchorginterfaceByInteerfaceUUID(interfaceUUID:String):[OrgInterface]
  }

type Mutation{
   createOrgInterface(input:OrgInterfaceInput): OrgInterface
   updateOrgInterface(input:OrgInterfaceInput): OrgInterface
   removeOrgInterface(input:OrgInterfaceInput):OrgInterface
   

}

`;

const resolvers = {
  Query: {
    async allOrgInterfaceAPI(root, { input }) {
      console.log('search', input);
      return new Promise((resolve, reject) => {
        var jsonData = {
          rule: {
            ruleName: input.docTypeUUID.replace(/ /g, ''),
            ruleDescription: 'Processing Rule',
            disabled: 'false',
            uimode: 'true',
            sender: [
              {
                corpName: input.sourceProfileUUID,
                unitName: input.sourceorganizationUnit,
              },
            ],
            receiver: [
              {
                corpName: input.targetProfileUUID,
                unitName: input.targetorganizationUnit,
              },
            ],
            docTypeName: [input.docTypeUUID],
          },
        };
        console.log('jsonData', jsonData);
        axios({
          method: 'post',
          //url: config.db.BASE_URL+'rest/CG_EDI_TMS_SYNC/webservices/restws/provider/processingRule',
          url: config.db.BASE_URL + 'restv2/processingRule',
          data: jsonData,
        }).then(function (response) {
          console.log('response', response);
          console.log('status', response.data.response);
          // resolve({success:response.data.response.msg})
          var status;
          if (response.data.response.status === 'success') {
            status = 'Active';
          } else {
            status = 'Inactive';
          }
          connection.query(
            'UPDATE `orginterface` SET `APIStatus`=' +
              mysql.escape(status) +
              ' WHERE `orgInterfaceUUID`=' +
              mysql.escape(input.orgInterfaceUUID),
            function (err, updateOrgInterfaceStatus) {
              if (err) reject(err);
              else
                resolve({
                  statusAPI: response.data.response.status,
                  msg: response.data.response.msg,
                });
            }
          );
        });
        //resolve(true)
      });
      /* return new Promise((resolve, reject) => {

          connection.query( 'SELECT (organization.organizatonName) as organizationUUID,(documenttype.description)as docdescription,(orgproject.projectName )as projectUUID, (interface.name) as interfaceUUID,(interface.description) as interfaceDescription,(partnerprofile.corporationName) as sourceProfileUUID ,(partnerprofile.organizationUnit) as sourceorganizationUnit, (ppt.corporationName) as targetProfileUUID ,(ppt.organizationUnit) as targetorganizationUnit,(documenttype.name)as docTypeUUID, (service.name) as serviceUUID from orginterface JOIN organization ON organization.organizationUUID=orginterface.organizationUUID JOIN interface ON interface.interfaceUUID=orginterface.interfaceUUID JOIN orgproject ON orgproject.projectUUID=orginterface.projectUUID JOIN partnerprofile ON partnerprofile.partnerProfileUUID=orginterface.sourceProfileUUID JOIN partnerprofile AS ppt ON ppt.partnerProfileUUID=orginterface.targetProfileUUID JOIN documenttype ON documenttype.documentTypeUUID=orginterface.docTypeUUID JOIN service ON service.serviceUUID=orginterface.serviceUUID WHERE orginterface.orgInterfaceUUID='+mysql.escape(search), function (err, result) {
            if (err) {
              reject(err);
            } else {
              console.log(result);              
              var jsonData={
                "rule" : {
                  "ruleName" : result[0].docTypeUUID.replace(" ",""),
                  "ruleDescription" : result[0].docdescription,
                  "disabled" : false,
                  "uimode" : true,
                  "sender" : [ {
                    "corpName" : result[0].sourceProfileUUID ,
                    "unitName" : result[0].sourceorganizationUnit
                  } ],
                  "receiver" : [ {
                    "corpName" : result[0].targetProfileUUID ,
                    "unitName" : result[0].targetorganizationUnit
                  } ],
                  "docTypeName" : [ result[0].docTypeUUID ]
                }
              }
              axios({
                method: 'post',               
                url: config.db.BASE_URL+'rest/ CG_EDI_TMS_SYNC/webservices/restws/provider/processingRule',
                data: jsonData
            }).then(function (response) {
                    console.log("response",response)
                    resolve(true)
            });
              resolve(true);
            }
          });
       
    })*/
    },
    async organisationInterface(root, { searchInput }) {
      //sql query for fetch Interface

      return new Promise((resolve, reject) => {
        var sql, values;

        if (searchInput == '') {
          sql =
            'SELECT 	orginterface.orgInterfaceUUID,orginterface.createdOn,orginterface.lastUpdatedOn,orginterface.APIStatus,(organization.organizatonName) as organizationUUID,(orgproject.projectName )as projectUUID, (interface.name) as interfaceUUID,(partnerprofile.corporationName) as sourceProfileUUID ,(partnerprofile.organizationUnit) as sourceorganizationUnit , (ppt.corporationName) as targetProfileUUID,(ppt.organizationUnit) as targetorganizationUnit,(documenttype.name)as docTypeUUID, (service.name) as serviceUUID from orginterface JOIN organization ON organization.organizationUUID=orginterface.organizationUUID JOIN interface ON interface.interfaceUUID=orginterface.interfaceUUID JOIN orgproject ON orgproject.projectUUID=orginterface.projectUUID JOIN partnerprofile ON partnerprofile.partnerProfileUUID=orginterface.sourceProfileUUID JOIN partnerprofile AS ppt ON ppt.partnerProfileUUID=orginterface.targetProfileUUID JOIN documenttype ON documenttype.documentTypeUUID=orginterface.docTypeUUID JOIN service ON service.serviceUUID=orginterface.serviceUUID ORDER BY orginterface.createdOn DESC';
        } else {
          var sql =
            ' SELECT * from orginterface where (interfaceUUID= ' +
            mysql.escape(searchInput) +
            'OR docTypeUUID=' +
            mysql.escape(searchInput) +
            'OR projectUUID=' +
            mysql.escape(searchInput) +
            ')ORDER BY  createdOn DESC ';
        }

        connection.query(sql, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async fetchProject1(root, { searchInput }) {
      //sql query for fetch Project

      return new Promise((resolve, reject) => {
        var sql, values;

        if (searchInput == '') {
          sql =
            'Select ProjectUUID,ProjectName from orgproject ORDER BY  createdOn DESC ';
        } else {
          var sql =
            ' Select ProjectUUID,ProjectName from orgproject where (ProjectName= ' +
            mysql.escape(searchInput) +
            'OR ProjectDescription=' +
            mysql.escape(searchInput) +
            'OR OrganizationUUID=' +
            mysql.escape(searchInput) +
            ') ORDER BY  createdOn DESC ';
        }

        connection.query(sql, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async fetchInterface1(root, { searchInput }) {
      //sql query for fetch Interface

      return new Promise((resolve, reject) => {
        var sql, values;

        if (searchInput == '') {
          sql =
            'Select interfaceUUID,name from interface ORDER BY  createdOn DESC ';
        } else {
          var sql =
            ' Select interfaceUUID,name from interface where (name= ' +
            mysql.escape(searchInput) +
            'OR description=' +
            mysql.escape(searchInput) +
            'OR projectUUID=' +
            mysql.escape(searchInput) +
            ')ORDER BY  createdOn DESC ';
        }

        connection.query(sql, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async fetchOrganization1(root, { searchInput }) {
      //sql query for fetch Organization

      return new Promise((resolve, reject) => {
        var sql, values;
        if (searchInput == '') {
          sql =
            'Select organizationUUID,organizatonName from organization ORDER BY  createdOn DESC ';
          console.log(sql);
        } else {
          var sql =
            ' Select organizationUUID,organizatonName from organization where (organizatonName= ' +
            mysql.escape(searchInput) +
            'OR organizationDescription=' +
            mysql.escape(searchInput) +
            ') ORDER BY  createdOn DESC ';
        }

        connection.query(sql, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async fetchDocumentType1(root, { searchInput }) {
      //sql query for fetch DocumentType

      return new Promise((resolve, reject) => {
        var sql, values;
        if (searchInput == '') {
          sql =
            'Select documentTypeUUID,name from documenttype ORDER BY  createdOn DESC ';
          console.log(sql);
        } else {
          var sql =
            ' Select  documentTypeUUID,name from documenttype where (name= ' +
            mysql.escape(searchInput) +
            'OR description=' +
            mysql.escape(searchInput) +
            ') ORDER BY  createdOn DESC ';
        }

        connection.query(sql, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    allPartners1: (root, { input }) => {
      return new Promise((resolve, reject) => {
        connection.query(
          'Select  CorporationName,PartnerProfileUUID  from partnerprofile ORDER BY  createdOn DESC ',
          function (err, allPartner) {
            if (err) reject(err);
            else resolve(allPartner);
          }
        );
      });
    },
    allPartnersByStatus: (root, { input }) => {
      return new Promise((resolve, reject) => {
        connection.query(
          'Select  CorporationName,PartnerProfileUUID  from partnerprofile where  status="Active" and APIStatus="Active" ORDER BY  createdOn DESC ',
          function (err, allPartner) {
            if (err) reject(err);
            else resolve(allPartner);
          }
        );
      });
    },
    allPartnersByStatusandOrgUUID(root, { organizationUUID }) {
      return new Promise((resolve, reject) => {
        connection.query(
          'Select  CorporationName,PartnerProfileUUID  from partnerprofile where  status="Active" and APIStatus="Active" and organizationUUID=' +
            mysql.escape(organizationUUID) +
            ' ORDER BY  createdOn DESC',
          function (err, allPartner) {
            if (err) reject(err);
            else resolve(allPartner);
          }
        );
      });
    },
    allPartnersByStatusandProjandorgUUID: (root, { projectUUID }) => {
      return new Promise((resolve, reject) => {
        connection.query(
          ' Select  CorporationName,PartnerProfileUUID  from partnerprofile where  status="Active" and APIStatus="Active" and organizationUUID=(SELECT organizationUUID  FROM orgproject where projectUUID=' +
            mysql.escape(projectUUID) +
            ')',
          function (err, allPartner) {
            if (err) {
              reject(err);
            } else {
              console.log(
                allPartner,
                allPartner.length,
                allPartner[0].organizationUUID
              );
              resolve(allPartner);
            }
          }
        );
      });
    },
    async fetchService1(root, { searchInput }) {
      //sql query for fetch Service

      return new Promise((resolve, reject) => {
        var sql, values;
        if (searchInput == '') {
          sql =
            'Select serviceUUID,name from service ORDER BY  createdOn DESC ';
          console.log(sql);
        } else {
          var sql =
            ' Select serviceUUID,name from service where (name= ' +
            mysql.escape(searchInput) +
            'OR description=' +
            mysql.escape(searchInput) +
            ') ORDER BY  createdOn DESC  ';
        }

        connection.query(sql, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async fetchOrgInterface(root, { search }) {
      //sql query for fetch OrgInterface by partner profile id

      return new Promise((resolve, reject) => {
        var sql, values;
        if (search === '') {
          sql = 'SELECT * from orginterface ORDER BY  createdOn DESC ';
        } else {
          /*  interfaceUUID :String
            projectUUID :String
            sourceProfileUUID:String
            targetProfileUUID:String
            docTypeUUID:String
            serviceUUID:String
            organizationUUID: String
            SELECT 	organizationUUID,	OrganizatonName FROM `organization`
            SELECT projectUUID,ProjectName FROM `orgproject` where OrgUUID=''
            SELECT interfaceUUID,name FROM `interface` where projectUUID=''

          select * from orginterface where*/

          var sql = ' select * from orginterface ORDER BY  createdOn DESC ';
          values = '';
        }

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async fetchOrgInterfaceByUUIDs(
      root,
      { organizationUUID, projectUUID, interfaceUUID }
    ) {
      //sql query for fetch OrgInterface by organizationUUID, projectUUID, interfaceUUID

      return new Promise((resolve, reject) => {
        var sql, values;
        if (
          organizationUUID === '' ||
          projectUUID === '' ||
          interfaceUUID === ''
        ) {
          sql = 'SELECT * from orginterface ORDER BY  createdOn DESC ';
        } else {
          var sql =
            ' SELECT * from orginterface WHERE (organizationUUID=' +
            mysql.escape(organizationUUID) +
            ' AND projectUUID=' +
            mysql.escape(projectUUID) +
            ' AND interfaceUUID=' +
            mysql.escape(interfaceUUID) +
            ')';
        }

        connection.query(sql, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async organisationInterfaceByOrgUUID(root, { organizationUUID }) {
      //sql query for fetch OrgInterface by organizationUUID

      return new Promise((resolve, reject) => {
        var sql, values;
        if (organizationUUID === '') {
          sql = 'SELECT * from orginterface ORDER BY  createdOn DESC ';
        } else {
          var sql =
            ' SELECT * from orginterface WHERE organizationUUID=' +
            mysql.escape(organizationUUID);
        }

        connection.query(sql, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async fetchorginterfaceByInteerfaceUUID(root, { interfaceUUID }) {
      //sql query for fetch OrgInterface by interfaceUUID

      return new Promise((resolve, reject) => {
        var sql, values;
        if (interfaceUUID === '') {
          sql = 'SELECT * from orginterface ORDER BY  createdOn DESC ';
        } else {
          /*   var sql =
            ' SELECT * from orginterface WHERE interfaceUUID=' +
            mysql.escape(interfaceUUID);*/
          sql =
            `SELECT  orginterface.orgInterfaceUUID,orginterface.createdOn,orginterface.lastUpdatedOn,orginterface.APIStatus,(organization.organizatonName) as organizationUUID,(orgproject.projectName )as projectUUID, (interface.name) as 
            interfaceUUID,(partnerprofile.corporationName) as sourceProfileUUID ,
            (partnerprofile.organizationUnit) as sourceorganizationUnit ,
             (ppt.corporationName) as targetProfileUUID,(ppt.organizationUnit)
              as targetorganizationUnit,(documenttype.name)as docTypeUUID, 
              (service.name) as serviceUUID from orginterface JOIN 
              organization ON organization.organizationUUID=orginterface.organizationUUID JOIN
               interface ON interface.interfaceUUID=orginterface.interfaceUUID JOIN 
               orgproject ON orgproject.projectUUID=orginterface.projectUUID JOIN
                partnerprofile ON partnerprofile.partnerProfileUUID=orginterface.sourceProfileUUID
                 JOIN partnerprofile AS ppt ON ppt.partnerProfileUUID=orginterface.targetProfileUUID
                  JOIN documenttype ON documenttype.documentTypeUUID=orginterface.docTypeUUID JOIN 
                  service ON service.serviceUUID=orginterface.serviceUUID 
                  WHERE orginterface.interfaceUUID=` +
            mysql.escape(interfaceUUID) +
            ` ORDER BY orginterface.createdOn DESC`;
        }

        connection.query(sql, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async fetchProjectByUUID(root, { projectUUID }) {
      return new Promise((resolve, reject) => {
        var sql, values;
        if (projectUUID === '') {
          sql = 'SELECT * from orgproject ORDER BY  createdOn DESC ';
        } else {
          var sql =
            ' SELECT * from orgproject WHERE projectUUID=' +
            mysql.escape(projectUUID);
        }

        connection.query(sql, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },

  Mutation: {
    async createOrgInterface(root, { input }) {
      console.log(input);
      //sql query for create userTypes
      return new Promise((resolve, reject) => {
        var sql =
          'INSERT INTO `orginterface`( `interfaceUUID`, `projectUUID`, `sourceProfileUUID`, `targetProfileUUID`, `docTypeUUID`, `serviceUUID`, `organizationUUID`) VALUES (?,?,?,?,?,?,?)';

        var values = [
          input.interfaceUUID,
          input.projectUUID,
          input.sourceProfileUUID,
          input.targetProfileUUID,
          input.docTypeUUID,
          input.serviceUUID,
          input.organizationUUID,
        ];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async updateOrgInterface(root, { input }) {
      console.log(input);
      //sql query for update userTypes
      return new Promise((resolve, reject) => {
        var sql =
          'UPDATE orginterface SET addressNameID = ?,addresslines1=? ,addressLines2=?, addresslines3=?,city=?,state=?,country=?,zip=?,addressstatus=?,addressTypeUUID=?,partnerProfileUUID=?,lastUpdatedOn=? WHERE addressUUID = ?';

        var values = [
          input.interfaceUUID,
          input.projectUUID,
          input.sourceProfileUUID,
          input.targetProfileUUID,
          input.docTypeUUID,
          input.serviceUUID,
          input.organizationUUID,
        ];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async removeOrgInterface(root, { input }) {
      console.log(input);
      //sql query for create userTypes
      return new Promise((resolve, reject) => {
        var sql = 'DELETE FROM `OrgInterface` WHERE `orgInterfaceUUID` = ?;';

        var values = [input.orgInterfaceUUID];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
};

const OrgInterfaceSchema = makeExecutableSchema({
  typeDefs,
  resolvers,
  formatError: (err) => {
    // Don't give the specific errors to the client.
    if (err.message.startsWith('Database Error: ')) {
      return new Error('sql error');
    }

    // Otherwise return the original error.  The error can also
    // be manipulated in other ways, so long as it's returned.
    return err;
  },
});

module.exports = OrgInterfaceSchema;
