const Express = require('express');
const app = Express();
const cors = require('cors');

const mysql = require('mysql');
const config = require('./dbConfig');
var pool = require('./database');
const isTokenValid = require('./validate');
const passport = require('passport');
const bodyParser = require('body-parser');
var { mergeSchemas } = require('graphql-tools');
const ExpressGraphQLHTTP = require('express-graphql');
const Organisation = require('./Organization/OrganizationGraohQLSchema');
const Project = require('./Project/ProjectGraphQLSchema');
const Inteface = require('./Interface/InterfaceGraphQLSchema');
const DocumentType = require('./DocumentType/DocumentTypeGraphQLSchema');
const Service = require('./Service/ServiceGraphQLSchema');
const Users = require('./Users/UserGraphQLSchema');
const Login = require('./LoginUsers/LoginUserGraphQLSchema');
const Partners = require('./Partners/PartnerGraphQLSchema');
const UsersRoleAssignment = require('./UserRoleAssignment/UserRoleAssignmentGraphQLSchema');
const UsersRoleType = require('./UserRoleTypes/UserRoleTypesGraphQLSchema');
const UserTypes = require('./UserTypes/UserTypesGraphQLSchema');
const AddressTypes = require('./AddressType/AddressTypeGraphQLSchema');
const Address = require('./Address/AddressGraphQLSchema');
const Contact = require('./Contact/ContactGraphQLSchema');
const ContactType = require('./ContactType/ContactTypeGraphQLSchema');
const ProfileIDTypes = require('./ProfileIDTypes/ProfileIDTypesGraphQLSchema');
const ProfileIDValues = require('./ProfileIDValues/ProfileIDValuesGraphQLSchema');
const ProfileAddressRelation = require('./ProfileAddressRelation/ProfileAddressRelationGraphQLSchema');
const ProfileContactRelation = require('./ProfileContactRelation/ProfileContactRelationGraphQLSchema');
const OrganisationInterface = require('./OrganisationInterface/OrganisationInterfaceGraphQLSchema');
const FTPProtocolSchema = require('./FTPProtocol/FTPProtocolGraohQLSchema');
const WebTransferProtocolSchema = require('./WebTransferProtocol/WebTransferProtocolGraohQLSchema');
const ProtocolType = require('./ProtocolType/ProtocolTypeGraphQLSchema');
const { login, okd } = require('okd-api');
const ProtocolCertificate = require('./ProtocolCertificate/ProtocolCertificateGraphQLSchema');
//ARCESB
const ApplicationURL_arcesb = require('./Arcesb/ApplicationURL');
const AS2Profile_arcesb = require('./Arcesb/AS2Profile');
const Certificate_arcesb = require('./Arcesb/Certificate');
const Connectors_arcesb = require('./Arcesb/Connectors');
const Workspace_arcesb = require('./Arcesb/Workspace');
const DocumentType_arcesb = require('./Arcesb/DocumentTypeGraphQLSchema');
const Certificate_Upload_arcesb = require('./Arcesb/CertificateUpload');
const AllEndPoints_arcesb = require('./Arcesb/allEndpoints');
const X12Connector_arcesb = require('./Arcesb/X12Connector');
const Qualifier_arcesb = require('./Arcesb/Qualifier');
const ControlVersionNumber_arcesb = require('./Arcesb/ControlVersionNumber');
const { graphqlUploadExpress } = require('graphql-upload');
//const BlogSchema=require('./elasticSearch/blogGraphQLSchema');
const cookieParser = require('cookie-parser');
const path = require('path');
const jwt = require('express-jwt');
const jwtverify = require('jsonwebtoken');
const uiapp = Express();
uiapp.use(Express.static(path.join(__dirname, '/../frontend/build')));
uiapp.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname, '/../frontend/build', 'index.html'));
});
uiapp.listen(5005);
//file upload
/*const debug = require('debug')('myapp:server');
const multer = require('multer');
const logger = require('morgan');
//const serveIndex = require('serve-index');
var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './public/uploads');
  },
  filename: (req, file, cb) => {
    cb(
      null,
      file.fieldname + '-' + Date.now() + path.extname(file.originalname)
    );
  },
});
//will be using this for uplading
app.use(Express.static('public'));
const upload = multer({ storage: storage });
app.post('/testUpload', upload.single('file'), function (req, res) {
  console.log(req.files);

  debug(req.file);
  console.log('storage location is ', req.hostname + '/' + req.path);
  return res.send(req.file);
});*/

const aws_app = Express();
aws_app.use(bodyParser.urlencoded({ extended: true }));
aws_app.use(bodyParser.json());
aws_app.set('port', 5006);
aws_app.use('*', cors());

aws_app.get('/runningPods', async function (req, res) {
  await getRunningPods()
    .then(async (result) => {
      return await result.running((events) => {
        return events;
      });
    })
    .then((responseNew) => {
      res.send(responseNew);
    })
    .catch((e) => {
      console.log(e);
    });
});

async function getRunningPods() {
  //try {

  return await new Promise(async (resolve, reject) => {
    //const cluster ='https://api.cm.changemaker.community:6443';
    //const token = 'RcuPgaqgm55l8FjfUlZHpPVhvZRGEIyREzxsGe9WkW8';
    const cluster = config.db.cluster_url;
    const token = config.db.token;

    const imagestream = await okd(cluster, token).namespace(config.db.namespace)
      .pod;
    console.log('imagestream', imagestream);

    if (imagestream) {
      resolve(imagestream);
    } else {
      reject('Error no pods');
    }
  });
  /*} catch (err) {
    console.log(err);
  }*/
}
var awslistener = aws_app.listen(aws_app.get('port'), function () {
  console.log('Node listening on port', awslistener.address().port);
});

app.set('port', 5004);
app.use('*', cors());

app.set('secret', config.db.Secret);
app.use(cookieParser());

var connection;
/*function handleDisconnect() {
  connection = mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database,
    port: config.db.port,
  }); // Recreate the connection, since
  // the old one cannot be reused.

  connection.connect(function (err) {
    // The server is either down
    if (err) {
      // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    } // to avoid a hot loop, and to allow our node script to
  }); // process asynchronous requests in the meantime.
  // If you're also serving http, display a 503 error.
  connection.on('error', function (err) {
    console.log('db error', err);
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      // Connection to the MySQL server is usually
      handleDisconnect(); // lost due to either server restart, or a
    } else {
      // connnection idle timeout (the wait_timeout
      throw err; // server variable configures this)
    }
  });
}

handleDisconnect();*/

/*var MySQLStore = require('connect-mysql'); // mysql session store
var options = {
      config: {
        host: config.db.host,
        user: config.db.user,
        password: config.db.password,
        database: config.db.database,
        port: config.db.port,
      },
      pool:true,
      keepalive:true

    };

    connection=MySQLStore(options)*/

/*const Schema=mergeSchemas({
    schemas:[
        Users,
        UserRole,
        ApplicationUsers,
        Organisation,
        OrgProject,
        UserRoleAssignment,
        UserTypes,
        UserRoleTypes

    ]
});*/

const Schema = mergeSchemas({
  schemas: [
    /*
    Project,
    Inteface,
    DocumentType,
    Service,
    
    Partners,
    AddressTypes,
    Address,
    Contact,
    ContactType,
    ProfileIDValues,
    ProfileIDTypes,

    ProfileAddressRelation,
    ProfileContactRelation,
    OrganisationInterface,
    FTPProtocolSchema,
    WebTransferProtocolSchema,
    ProtocolType,
    ProtocolCertificate,*/
    Organisation,
    Users,
    UsersRoleAssignment,
    UsersRoleType,
    UserTypes,
    ApplicationURL_arcesb,
    AS2Profile_arcesb,
    Certificate_arcesb,
    DocumentType_arcesb,
    Connectors_arcesb,
    Workspace_arcesb,
    Certificate_Upload_arcesb,
    AllEndPoints_arcesb,
    X12Connector_arcesb,
    Qualifier_arcesb,
    ControlVersionNumber_arcesb,
    // BlogSchema
  ],
});

let custumFormatErrorFn = (error) => ({
  message: error.message,
  location: error.location,
  stack: error.stack ? error.stack.split('\n') : [],
  path: error.path,
});
app.use('*', cors());

// authentication middleware
const authMiddleware = jwt({
  secret: app.get('secret'),
  credentialsRequired: false,
  //requestProperty: 'auth',
  getToken: function fromHeaderOrQuerystring(req) {
    console.log(
      "req.headers.referer.includes('/login')",
      req.headers.authorization
    );
    if (
      req.headers.authorization &&
      req.headers.authorization.split(' ')[0] === 'Bearer'
    ) {
      return req.headers.authorization.split(' ')[1];
    } else if (req.query && req.query.token) {
      return req.query.token;
    }
    console.log('authentication middleware');
    //return res.sendStatus(401);
    // return res.sendStatus(401).send('Unauthorized: No token provided');

    return null;
  },
});

app.use(authMiddleware);
/* app.use(function (err, req, res, next) {

    console.log("check this")
    if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
      const token = req.headers.authorization.split(' ')[1];
      console.log("token----",token)
      if (!token) {
        console.log("218token----",token)
        res.status(401).send('Unauthorized: No token provided');
      } else {console.log("220token----",token)
        jwtverify.verify(token, config.db.Secret, function(err, decoded) {
      if (err) {console.log("err222----",err)
        //res.status(401).send('Unauthorized: Invalid token');
        if (err.name === 'UnauthorizedError') {
          res.status(401).send('invalid token...');
          return;
        }
      } else {console.log("228token----",decoded)
        console.log("req",req,"decoded",decoded)
       // req.email = decoded.email;
        next();
      }
    });
  }}
   
    next();
  });*/

/* const withAuthfn = function(req, res, next) { console.log("req",req,"res",res,"req.headers.referer.includes('/login')",req.headers.referer.includes('/login'),req.headers.referer)
  if(!req.headers.referer.includes('/login')){
    if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
        const token = req.headers.authorization.split(' ')[1];
        console.log("token",token)
        if (!token) {
          res.status(401).send('Unauthorized: No token provided');
        } else {
          jwt.verify(token, app.get('secret'), function(err, decoded) {
            if (err) {
              res.status(401).send('Unauthorized: Invalid token');
            } else {
              console.log("req",req,"decoded",decoded)
             // req.email = decoded.email;
              next();
            }
          });
        }
      }
    }else{
      next();
    }
   
  }

  app.use(withAuthfn);*/

// my custom route middleware to verify a token
/*app.use(function(req, res, next) {
console.log("------------------------------------");
console.log("route middleware to verify a token");
console.log("");
// check header or url parameters or post parameters for token
if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
var token = req.headers.authorization.split(' ')[1] ;
console.log("req.cookies.access_token:", req.headers.authorization);

// decode token
if (token) {

  // verifies secret and checks exp
  jwt.verify(token, app.get('secret'), function(err, decoded) {
    if (err) {
      console.log("jwt.verify ERROR")
      return res.json({ success: false, message: 'Failed to authenticate token.', err:err });
    } else {
      console.log("jwt.verify OK")
      // if everything is good, save to request for use in other routes
      req.decoded = decoded;
      next();
    }
  });

} }else {

  // if there is no token
  // return an error
  return res.status(403).send({
      success: false,
      message: 'No token provided.'
  });

}
});*/

// Create a context for holding contextual data
const context = async (req) => {
  const { authorization: token } = req.headers;
  console.log('auth----req---------', token);
  return { token };
};

app.use(
  '/graphql',
  bodyParser.json(),
  authMiddleware,
  graphqlUploadExpress({ maxFileSize: 10000000, maxFiles: 10 }),
  ExpressGraphQLHTTP(async (req) => ({
    schema: Schema,
    rootValue: global,
    graphiql: true,
    customFormatErrorFn: custumFormatErrorFn,
    /* context: {
      user: req.user,
    },*/
    context: () => context(req),
    uploads: false,
    subscriptions: {
      path: '/subscriptions',
      onConnect: async (connectionParams, webSocket) => {
        console.log(
          `Subscription client connected using Apollo server's built-in SubscriptionServer.`
        );
      },
    },
  }))
);

app.listen(app.get('port'), () => {
  console.log('listening to port', app.get('port'));
});
