var {makeExecutableSchema}=require('graphql-tools');
var elasticsearch = require('elasticsearch');
const config = require('../dbConfig');
var client = new elasticsearch.Client({
  host: 'elasticsearch.tms.changemaker.community',
  log: 'trace',
  apiVersion: '7.2', // use the same version of your Elasticsearch instance
});
var search="TMS"
var search="Elasticsearch"
var mysql= require('mysql');
var connection = mysql.createPool({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database,
    port: config.db.port
  });
  
  //connection.connect();

  const typeDefs=`
  scalar Date
  type Elastic{
      PostName:String
      PostType:String
      PostBody:String
  }
  type Query {
    dummy: String
  }


   type Mutation{
     elasticSearch(search:String):[Elastic]
   }

  `;

  const resolvers={
     
      Mutation:{
      async elasticSearch(root,{search}){
          console.log("search",search)
        async function result(){
            try {
                return await client.search({
                    index: 'blog',
                    body: {
                        query: {
                            match: {                       
                                "PostBody":search,                       
                            }
                        }
                    }
                });
                //console.log(response.hits.hits)
              } catch (error) {
                console.trace(error.message)
              }
        }
        
        async function result2(){
            try {
               return await client.search({
                    index: 'blog',
                    body: {
                        query: {
                            match: {                       
                                "PostName": search,                       
                            }
                        }
                    }
                });
                //console.log(response.hits.hits)
              } catch (error) {
                console.trace(error.message)
              }
        }
        
        async function result3(){
            try {
               return await client.search({
                    index: 'blog',
                    body: {
                        query: {
                            match: {                       
                                "Name":search,                       
                            }
                        }
                    }
                });
                //console.log(response.hits.hits)
              } catch (error) {
                console.trace(error.message)
              }
        }
        
        async function alldata(){
            let data=[];
            let data1=await result();
          // data={...data,"data1":data1}
            let data2=await result2();
          // data={...data,"data2":data2}
           let data3=await result3();
          // data={...data,"data3":data3}
           console.log("data3",data3.hits.hits,"data2",data2.hits.hits,"data1",data1.hits.hits)
          data1.hits.hits.map((v,i)=>{
              data.push(v._source);
              // console.log("v---data2",v._source,"i-----data2",i)
           })
           data2.hits.hits.map((v,i)=>{
            data.push(v._source);
            // console.log("v---data2",v._source,"i-----data2",i)
         })
         data3.hits.hits.map((v,i)=>{
            data.push(v._source);
            // console.log("v---data2",v._source,"i-----data2",i)
         })
             return data
        }
        
       return new Promise((resolve,reject)=>{
            const searchRes=alldata();
            if(!searchRes)reject(searchRes)
            else resolve(searchRes)
        })
        
        //console.log("data",alldata())


      }


      }
  };

 const blogSchema=makeExecutableSchema({
            typeDefs,
            resolvers
  });

  module.exports=blogSchema;