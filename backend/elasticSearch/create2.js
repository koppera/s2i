var elasticsearch = require('elasticsearch');
var client = new elasticsearch.Client({
  host: 'elasticsearch.tms.changemaker.community',
  log: 'trace',
  apiVersion: '7.2', // use the same version of your Elasticsearch instance
});

client.search({
  index: 'ipdata',
  type:"text",
  body: {
    query: {
      match: {
        body: 'elasticsearch'
      }
    }
  }
}).then(function(resp) {
  console.log(resp);
  
}, function(err) {
  console.trace(err.message);
});

/*client.ping({
  // ping usually has a 3000ms timeout
  requestTimeout: 80000
}, function (error,data) {
  if (error) {
    console.trace('elasticsearch cluster is down!',error);
  } else {
    console.log('All is well',data);
  }
});

  client.index({
    index: 'blog',
    id: '2',
    type: 'posts',
    body: {
        "PostName": " Elasticsearch ",
        "PostType": "App",
        "PostBody": "This is the text  about using Elasticsearch .",
    }
}, function(err, resp, status) {
    console.log(resp);
});
*/

/*
client.ping({
    // ping usually has a 3000ms timeout
    requestTimeout: 80000
  }, function (error,data) {
    if (error) {
      console.trace('elasticsearch cluster is down!',error);
    } else {
      console.log('All is well',data);
    }
  });

client.search({
  index: 'blog',
  type: 'posts',
  q: 'PostName:Node.js'
}).then(function(resp) {
  console.log(resp);
}, function(err) {
  console.trace(err.message);
});

client.search({
  index: 'blog',
  type: 'posts',
  body: {
      query: {
          match: {
              "PostName": 'Node.js'
          }
      }
  }
}).then(function(resp) {
  console.log(resp);
}, function(err) {
  console.trace(err.message);
});*/