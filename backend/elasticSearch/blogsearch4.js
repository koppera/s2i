var elasticsearch = require('elasticsearch');
var client = new elasticsearch.Client({
  host: 'elasticsearch.tms.changemaker.community',
  log: 'trace',
  apiVersion: '7.2', // use the same version of your Elasticsearch instance
});




client.search({
    index: 'blog',
}).then(function(resp) {
    console.log(resp);
}, function(err) {
    console.trace(err.message);
});