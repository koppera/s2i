var elasticsearch = require('elasticsearch');
const express = require('express');
const app = express();
const cors = require('cors');
app.set('port', 5005);
var client = new elasticsearch.Client({
  host: 'elasticsearch.tms.changemaker.community',
  log: 'trace',
  apiVersion: '7.2', // use the same version of your Elasticsearch instance
});
var search="TMS"
var search="Elasticsearch"

async function result(){
    try {
        return await client.search({
            index: 'blog',
            body: {
                query: {
                    match: {                       
                        "PostBody":search,                       
                    }
                }
            }
        });
        //console.log(response.hits.hits)
      } catch (error) {
        console.trace(error.message)
      }
}

async function result2(){
    try {
       return await client.search({
            index: 'blog',
            body: {
                query: {
                    match: {                       
                        "PostName": search,                       
                    }
                }
            }
        });
        //console.log(response.hits.hits)
      } catch (error) {
        console.trace(error.message)
      }
}

async function result3(){
    try {
       return await client.search({
            index: 'blog',
            body: {
                query: {
                    match: {                       
                        "Name": search,                       
                    }
                }
            }
        });
        //console.log(response.hits.hits)
      } catch (error) {
        console.trace(error.message)
      }
}

async function alldata1(){
    let data={}
    let data1=await result();
   data={...data,"data1":data1}
    let data2=await result2();
   data={...data,"data2":data2}
   let data3=await result3();
   data={...data,"data3":data3}
   console.log("data3",data.data3.hits.hits,"data2",data.data2.hits.hits,"data1",data.data1.hits.hits)
   data.data2.hits.hits.map((v,i)=>{
       console.log("v---data2",v._source,"i-----data2",i)
   })
     return data
}

async function alldata(){
    let data=[];
    let data1=await result();
  // data={...data,"data1":data1}
    let data2=await result2();
  // data={...data,"data2":data2}
   let data3=await result3();
  // data={...data,"data3":data3}
   //console.log("data3",data3.hits.hits,"data2",data2.hits.hits,"data1",data1.hits.hits)
  data1.hits.hits.map((v,i)=>{
      data.push(v._source);
      // console.log("v---data2",v._source,"i-----data2",i)
   })
   data2.hits.hits.map((v,i)=>{
    data.push(v._source);
    // console.log("v---data2",v._source,"i-----data2",i)
 })
 data3.hits.hits.map((v,i)=>{
    data.push(v._source);
    // console.log("v---data2",v._source,"i-----data2",i)
 })
     return data
}
app.use('/elastic',
async function(response,request){
   await alldata().then(res=>{console.log("res",res)
   response.send("res",res)
})
    .catch(err=>{console.log("err",err)})
}
)

app.listen(app.get('port'), () => {
    console.log('listening to port', app.get('port'));
  });
  
//console.log("data",alldata())

/*
client.search({
   
    index: 'blog',

     
    body: {
        query: {
            match: {
               
                "PostBody":search,
               
            }
        }
    }
  
}).then(function(resp) {
    console.log(resp);
}, function(err) {
    console.trace(err.message);
});

client.search({
   
    index: 'blog',

     
    body: {
        query: {
            match: {
                "PostName": search,
               
               
            }
        }
    }
  
}).then(function(resp) {
    console.log(resp);
}, function(err) {
    console.trace(err.message);
});

client.search({
   
    index: 'blog',

     
    body: {
        query: {
            match: {
                "Name": search,
               
               
            }
        }
    }
  
}).then(function(resp) {
    console.log(resp);
}, function(err) {
    console.trace(err.message);
});


/*
///working///////
client.search({
    index: 'blog',
    
    body: {
        query: {
            match: {
                "Name": "TMS connect"
            }
        }
    }
}).then(function(resp) {
    console.log(resp);
}, function(err) {
    console.trace(err.message);
});
///working///////
/*
client.index({
    index: 'blog',
    id: '3',
    type: 'posts',
    body: {
        "Name": "TMS connect",
        "Type": "Transaction",
        "Body": "EDI app",
    }
}, function(err, resp, status) {
    console.log(resp);
});

client.search({
    index: 'blog',
    type:"_doc",
   
    
}).then(function(resp) {
    console.log(resp);
}, function(err) {
    console.trace(err.message);
})

/*
client.search({
    index: 'blog',
    type: 'posts',
    body: {
        query: {
            match: {
                "PostName": 'Node.js'
            }
        }
    }
}).then(function(resp) {
    console.log(resp);
}, function(err) {
    console.trace(err.message);
});








/*

client.index({
    index: 'blog',
    id: '1',
    type: 'posts',
    body: {
        "PostName": "Integrating Elasticsearch Into Your Node.js Application",
        "PostType": "Tutorial",
        "PostBody": "This is the text of our tutorial about using Elasticsearch in your Node.js application.",
    }
}, function(err, resp, status) {
    console.log(resp);
});


client.indices.create({
    index: 'blog'
}, function(err, resp, status) {
    if (err) {
        console.log(err);
    } else {
        console.log("create", resp);
    }
});
/*
client.index({
    index: 'blognew',
    id: '2',
    type: 'posts2',
    body: {
        "PostName": "Integrating Elasticsearch ",
        "PostType": "Tutorial",
        "PostBody": " Elasticsearch in Node.js application.",
    }
}, function(err, resp, status) {
    console.log(resp);
});



client.search({
    index: 'blog',
    type: 'posts',
    body: {
        query: {
            match: {
                "PostName": 'Node.js'
            }
        }
    }
}).then(function(resp) {
    console.log(resp);
}, function(err) {
    console.trace(err.message);
});*/