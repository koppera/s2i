var { makeExecutableSchema } = require('graphql-tools');
const mysql = require('mysql');
const withAuthfn=require('../verifyMiddleware/withAuth');

var connection = mysql.createPool({
  host: '127.0.0.1',
  user: 'root',
  password: 'password',
  database: 'tms',
  port: 3306,
});

const typeDefs = `
scalar Date

type Profiles{

}

input ProfileInput{

}

type Query{
    allProfiles:[Profiles]
}

type Mutation{
    createProfile(input:ProfileInput):Profiles
}
`;

const resolvers = {
  Query: {
    allProfiles: () => {
      return new Promise((resolve, reject) => {
        connection.query(
          'SELECT * FROM `profileidtypes` ORDER BY  createdOn DESC ',
          function (err, allProfiles) {
            if (err) reject(err);
            else resolve(allProfiles);
          }
        );
      });
    },
  },
  Mutation: {},
};

let ProfileSchema = makeExecutableSchema({
  typeDefs,
  resolvers,
});

module.exports = ProfileSchema;
