let Mongoose = require('mongoose');

let UserSchema = new Mongoose.Schema({
  id: String,
  UserPassword: String,
  Mail: { type: String, required: true, unique: true },
  CommonName: { type: String, required: true, unique: true },
  Surname: String,
  Uid: { type: String, required: true, unique: true },
  Mobile: String,
  EmployeeNumber: String,
  Role: { type: Number, default: 0 },
  UserGroups: [
    {
      gid: String,
      CommonName: String,
      CreatedDate: { type: Date, default: Date }
    }
  ],
  UserRoles: [
    {
      rid: String,
      RoleName: String,
      CreatedDate: { type: Date, default: Date }
    }
  ],
  Creator: String,
  CreatedDate: { type: Date, default: Date }
});

module.exports = Mongoose.model('UserSchema', UserSchema);
