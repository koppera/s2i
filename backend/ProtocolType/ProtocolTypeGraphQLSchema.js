var { makeExecutableSchema } = require('graphql-tools');
const config = require('../dbConfig');
const mysql = require('mysql');
const withAuthfn=require('../verifyMiddleware/withAuth');
var connection = mysql.createConnection({
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database,
  port: config.db.port,
});

//connection.connect();

const typeDefs = `
scalar Date

type ProtocolTypes{
    idFileProtocolType:String
    protocolTypeUUID:String
    protocolTypeName:String
    protocolTypeDescription:String
    createdOn:Date
    lastUpdatedOn:Date
}

input ProtocolTypesInput{

  idFileProtocolType:String
  protocolTypeUUID:String
  protocolTypeName:String
  protocolTypeDescription:String
}
type Query{
   allProtocolTypes:[ProtocolTypes]
}

type Mutation{
   createProtocolTypes(input:ProtocolTypesInput): ProtocolTypes
   updateProtocolTypes(input:ProtocolTypesInput): ProtocolTypes
   removeProtocolTypes(input:ProtocolTypesInput):ProtocolTypes
}

`;

const resolvers = {
  Query: {
    allProtocolTypes: (root, args, context, info) => {
      //sql query for fetch all userTypes
      return new Promise((resolve, reject) => {
        connection.query(
          'SELECT * from protocoltype ORDER BY  createdOn DESC  ',
          function (err, post) {
            if (err) {
              reject(err);
            } else {
              resolve(post);
            }
          }
        );
      });
    },
  },
  Mutation: {
    async createProtocolTypes(root, { input }) {
      console.log(
        input.protocolTypeName,
        input.protocolTypeDescription,
        Date.now(),
        Date.now()
      );
      //sql query for create userTypes
      return new Promise((resolve, reject) => {
        var sql =
          'INSERT INTO protocoltype (protocolTypeName, protocolTypeDescription) VALUES(?,?)';

        var values = [input.protocolTypeName, input.protocolTypeDescription];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async updateProtocolTypes(root, { input }) {
      console.log(input.protocolTypeUUID);
      //sql query for update userTypes
      return new Promise((resolve, reject) => {
        var sql =
          'UPDATE protocoltype SET protocolTypeName = ?,protocolTypeDescription=?,lastUpdatedOn=? WHERE protocolTypeUUID = ?';

        var values = [
          input.protocolTypeName,
          input.protocolTypeDescription,
          new Date(),
          input.protocolTypeUUID,
        ];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async removeProtocolTypes(root, { input }) {
      console.log(input.protocolTypeUUID);
      //sql query for create userTypes
      return new Promise((resolve, reject) => {
        var sql = 'DELETE FROM `protocoltype` WHERE `protocolTypeUUID` = ?;';

        var values = [input.protocolTypeUUID];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
};

const ProtocolTypesSchema = makeExecutableSchema({
  typeDefs,
  resolvers,
  formatError: (err) => {
    // Don't give the specific errors to the client.
    if (err.message.startsWith('Database Error: ')) {
      return new Error('sql error');
    }

    // Otherwise return the original error.  The error can also
    // be manipulated in other ways, so long as it's returned.
    return err;
  },
});

module.exports = ProtocolTypesSchema;
