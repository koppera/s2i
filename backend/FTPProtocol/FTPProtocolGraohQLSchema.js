var { makeExecutableSchema } = require('graphql-tools');
const config = require('../dbConfig');
const mysql = require('mysql');
const withAuthfn=require('../verifyMiddleware/withAuth');
var connection = require('../database');

/*var MySQLStore = require('connect-mysql'); // mysql session store
var options = {
      config: {
        host: config.db.host,
        user: config.db.user,
        password: config.db.password,
        database: config.db.database,
        port: config.db.port,
      },
      pool:true,
      keepalive:true

    };

    connection=MySQLStore(options)*/ /*= mysql.createPool({
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database,
  port: config.db.port,
});*/
/*/*function handleDisconnect() {
  connection = mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database,
    port: config.db.port,
  }); // Recreate the connection, since
  // the old one cannot be reused.

  connection.connect(function (err) {
    // The server is either down
    if (err) {
      // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    } // to avoid a hot loop, and to allow our node script to
  }); // process asynchronous requests in the meantime.
  // If you're also serving http, display a 503 error.
  connection.on('error', function (err) {
    console.log('db error', err);
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      // Connection to the MySQL server is usually
      handleDisconnect(); // lost due to either server restart, or a
    } else {
      // connnection idle timeout (the wait_timeout
      throw err; // server variable configures this)
    }
  });
}

handleDisconnect();*/

//connection.connect();
const typeDefs = `
scalar Date

type FTPProtocol{
  FTPUUID:String
  FTPPort:String
  FTPHost:String
  FTPuserName:String
  FTPPassword:String
  FTPTYPEUUID:String
  Status:String
  Directory:String
  partnerProfileUUID:String
  certificateUUID:String
  createdOn:Date
  lastUpdatedOn:Date
}

input FTPProtocolInput{
  FTPUUID:String
  FTPPort:String
  FTPHost:String
  FTPuserName:String
  FTPPassword:String
  FTPTYPEUUID:String
  Status:String
  Directory:String
  partnerProfileUUID:String
  certificateUUID:String
}
type allProtocol{
  UUID:String
  Port:String
  Host:String
  UserName,:String
  Password:String
  TypeUUID,:String
  Status,:String
  Directory:String
  partnerProfileUUID:String
  certificateUUID:String
  createdOn:Date
  lastUpdatedOn:Date
}
type Query{
   allFTPProtocol:[FTPProtocol]
   fetchFTPProtocol(searchInput:String): [FTPProtocol]
   fetchFTPProtocolByPartnerProfile(partnerProfileUUID:String): [FTPProtocol]
   fetchAllProtocol(partnerProfileUUID:String): [allProtocol]
}

type Mutation{
   createFTPProtocol(input:FTPProtocolInput): FTPProtocol
   updateFTPProtocol(input:FTPProtocolInput): FTPProtocol
   removeFTPProtocol(input:FTPProtocolInput):FTPProtocol
}

`;

const resolvers = {
  Query: {
    allFTPProtocol: (root, args, context, info) => {
      //sql query for fetch all FTPProtocol
      return new Promise((resolve, reject) => {
        connection.query(
          'SELECT * from filetransferprotocol ORDER BY  createdOn DESC;',
          function (err, post) {
            if (err) {
              reject(err);
            } else {
              resolve(post);
            }
          }
        );
      });
    },
    async fetchFTPProtocolByPartnerProfile(root, { partnerProfileUUID }) {
      //sql query for fetch all FTPProtocol by partnerProfileUUID
      return new Promise((resolve, reject) => {
        if (partnerProfileUUID == '') {
          sql = 'SELECT * from filetransferprotocol ORDER BY  createdOn DESC';
        } else {
          var sql =
            ' select * from filetransferprotocol  where partnerProfileUUID=? ORDER BY  createdOn DESC  ';
          values = [partnerProfileUUID];
        }
        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            resolve(result);
          }
        });
      });
    },
    async fetchFTPProtocol(root, { searchInput }) {
      //sql query for fetch FTPProtocol

      return new Promise((resolve, reject) => {
        var sql, values;
        if (searchInput == '') {
          sql = 'Select * from filetransferprotocol ORDER BY  createdOn DESC';
          console.log(sql);
        } else {
          var sql =
            ' Select * from filetransferprotocol where (FTPHost = ' +
            mysql.escape(searchInput) +
            ' OR    FTPPort = ' +
            mysql.escape(searchInput) +
            ' OR    FTPuserName = ' +
            mysql.escape(searchInput) +
            ' OR    FTPPassword = ' +
            mysql.escape(searchInput) +
            ' OR    FTPTYPEUUID = ' +
            mysql.escape(searchInput) +
            ' OR   Status = ' +
            mysql.escape(searchInput) +
            ' OR    Directory = ' +
            mysql.escape(searchInput) +
            ' OR    partnerProfileUUID = ' +
            mysql.escape(searchInput) +
            ' OR    certificateUUID  = ' +
            mysql.escape(searchInput) +
            ')  ORDER BY  createdOn DESC ';
        }

        connection.query(sql, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async fetchAllProtocol(root, { partnerProfileUUID }) {
      //sql query for fetch FTPProtocol
      var sql, values;
      return new Promise((resolve, reject) => {
        if (partnerProfileUUID == '') {
          sql = 'SELECT * from filetransferprotocol ORDER BY  createdOn DESC';
        } else {
          sql = `SELECT FTPUUID AS UUID, 
FTPHost AS Host,
FTPPort AS Port,
FTPuserName AS UserName,
FTPPassword AS Password,
FTPTYPEUUID AS TypeUUID,
Status AS Status,
createdOn,
lastUpdatedOn,
partnerProfileUUID,
certificateUUID,
Directory
FROM filetransferprotocol WHERE partnerProfileUUID=? UNION ALL 
SELECT webTransferProtocolUUID AS UUID ,
webTransferProtocolHost As Host,
webTransferProtocolPort AS Port,
webTransferProtocolUserName AS UserName,
webTransferProtocolPassword AS Password,
protocolTypeUUID AS TypeUUID,
Status,
createdOn,
lastUpdatedOn,
partnerProfileID AS partnerProfileUUID,
certificateUUID,
null as Directory
 FROM webtransferprotocol WHERE partnerProfileID=?;`;
          values = [partnerProfileUUID, partnerProfileUUID];
        }
        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            resolve(result);
          }
        });
      });
    },
  },
  Mutation: {
    async createFTPProtocol(root, { input }) {
      console.log(input);
      //sql query for create FTPProtocol
      return new Promise((resolve, reject) => {
        var sql =
          'INSERT INTO filetransferprotocol (`FTPHost`, `FTPPort`, `FTPuserName`, `FTPPassword`, `FTPTYPEUUID`,`Status`, `Directory`, `partnerProfileUUID`, `certificateUUID`) VALUES(?,?,?,?,?,?,?,?,?)';

        var values = [
          input.FTPHost,
          input.FTPPort,
          input.FTPuserName,
          input.FTPPassword,
          input.FTPTYPEUUID,
          input.Status,
          input.Directory,
          input.partnerProfileUUID,
          input.certificateUUID,
        ];
        console.log(values);
        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async updateFTPProtocol(root, { input }) {
      console.log(input);
      //sql query for update FTPProtocol
      return new Promise((resolve, reject) => {
        var sql =
          'UPDATE filetransferprotocol SET `FTPHost`=?,`FTPPort`=?,`FTPuserName`=?,`FTPPassword`=?,`FTPTYPEUUID`=?,`Status`=?,`lastUpdatedOn`=?,`Directory`=?,`partnerProfileUUID`=?,`certificateUUID`=?  WHERE `FTPUUID` = ?';

        var values = [
          input.FTPHost,
          input.FTPPort,
          input.FTPuserName,
          input.FTPPassword,
          input.FTPTYPEUUID,
          input.Status,
          new Date(),
          input.Directory,
          input.partnerProfileUUID,
          input.certificateUUID,
          input.FTPUUID,
        ];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async removeFTPProtocol(root, { input }) {
      console.log(input);
      //sql query for create userTypes
      return new Promise((resolve, reject) => {
        var sql = 'DELETE FROM `filetransferprotocol` WHERE FTPUUID = ?';

        var values = [input.FTPUUID];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
};

const FTPProtocolSchema = makeExecutableSchema({
  typeDefs,
  resolvers,
  formatError: (err) => {
    // Don't give the specific errors to the client.
    if (err.message.startsWith('Database Error: ')) {
      return new Error('sql error');
    }

    // Otherwise return the original error.  The error can also
    // be manipulated in other ways, so long as it's returned.
    return err;
  },
});

module.exports = FTPProtocolSchema;
