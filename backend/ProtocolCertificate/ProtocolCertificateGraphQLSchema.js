var { makeExecutableSchema } = require('graphql-tools');
const config = require('../dbConfig');
const mysql = require('mysql');
const { GraphQLUpload } = require('graphql-upload');
//const { ApolloServer, gql } = require('apollo-server');

const axios = require('axios');
const fs = require('fs');
var connection = require('../database');

/*var MySQLStore = require('connect-mysql'); // mysql session store
var options = {
      config: {
        host: config.db.host,
        user: config.db.user,
        password: config.db.password,
        database: config.db.database,
        port: config.db.port,
      },
      pool:true,
      keepalive:true

    };

    connection=MySQLStore(options)*/
/*/*function handleDisconnect() {
  connection = mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database,
    port: config.db.port,
  }); // Recreate the connection, since
  // the old one cannot be reused.

  connection.connect(function (err) {
    // The server is either down
    if (err) {
      // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    } // to avoid a hot loop, and to allow our node script to
  }); // process asynchronous requests in the meantime.
  // If you're also serving http, display a 503 error.
  connection.on('error', function (err) {
    console.log('db error', err);
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      // Connection to the MySQL server is usually
      handleDisconnect(); // lost due to either server restart, or a
    } else {
      // connnection idle timeout (the wait_timeout
      throw err; // server variable configures this)
    }
  });
}

handleDisconnect();*/

//connection.connect();
const typeDefs = `
  scalar Date
  scalar Upload
  type File1 {
    Name: String
    imageData: String
    Length: String
    Type: String
  }
  type ProtocolCertificate1 {
   
    publicCertificate: Upload
   
  }
  type ProtocolCertificate {
    partnerProfileUUID: String
    idProtocolCertificate: String
    protocolCertificateUUID: String
    protocolCertificateName: String
    publicCertificate: File1
    privateCertificate: String
    usageValue: String
    protocolCertificateComments: String
    APIStatus: String
    createdOn: Date
    lastUpdatedOn: Date
  }
  input FileInput {
    Name: String
    imageData: String
    Length: String
    Type: String
  }
  input ProtocolCertificateInput {
    partnerProfileUUID: String
    idProtocolCertificate: String
    protocolCertificateUUID: String
    protocolCertificateName: String
    publicCertificate: Upload!
    privateCertificate: String
    usageValue: String
    APIStatus: String
    protocolCertificateComments: String
    createdOn: Date
    lastUpdatedOn: Date
  }
  
  type certificateAPISuccess{
    statusAPI:String
    msg:String
  }
  type Query {
    ProtocolCertificateAPI(protocolCertificateUUID: String,partnerProfileUUId: String):certificateAPISuccess
    fetchProtocolCertificateByPartnerProfile(
      partnerProfileUUID: String
    ): [ProtocolCertificate]
  }

  type Mutation {
    createProtocolCertificate1(file: Upload!): ProtocolCertificate
    createProtocolCertificate( input: ProtocolCertificateInput): ProtocolCertificate
    updateProtocolCertificate(
      input: ProtocolCertificateInput
    ): ProtocolCertificate
    removeProtocolCertificate(
      protocolCertificateUUID: String
    ): ProtocolCertificate
  }
`;

const resolvers = {
  Upload: GraphQLUpload,
  Query: {
    async ProtocolCertificateAPI(
      root,
      { protocolCertificateUUID, partnerProfileUUId }
    ) {
      console.log(
        'search',
        protocolCertificateUUID,
        partnerProfileUUId,
        `SELECT publicCertificate FROM protocolcertificate where protocolCertificateUUID=` +
          partnerProfileUUId
      );
      return new Promise((resolve, reject) => {
        connection.query(
          'SELECT publicCertificate, usageValue FROM protocolcertificate where protocolCertificateUUID=' +
            mysql.escape(protocolCertificateUUID),
          function (errS, resultS) {
            if (errS) {
              reject(errS);
            } else {
              console.log(resultS);
              connection.query(
                'SELECT corporationName,organizationUnit FROM partnerprofile where partnerProfileUUID=' +
                  mysql.escape(partnerProfileUUId),
                function (errP, resultP) {
                  if (errP) {
                    reject(errP);
                  } else {
                    console.log(resultP);
                    var jsonData = {
                      certificate: {
                        corpName: resultP[0].corporationName,
                        unitName: resultP[0].organizationUnit,
                        usage: resultS[0].usageValue,
                        certificateData: resultS[0].publicCertificate,
                      },
                    };

                    console.log(
                      'jsonData',
                      jsonData,
                      'url',
                      config.db.BASE_URL + 'restv2/partnerCertificate'
                    );
                    axios({
                      method: 'post',
                      //url: config.db.BASE_URL+'rest/CG_EDI_TMS_SYNC/webservices/restws/provider/processingRule',
                      url: config.db.BASE_URL + 'restv2/partnerCertificate',
                      data: jsonData,
                      headers: { 'Content-Type': 'application/json' },
                    }).then(function (response) {
                      console.log('response', response);
                      console.log('status', response.data.response);
                      // resolve({success:response.data.response.msg})
                      var status;
                      if (response.data.response.status === 'success') {
                        status = 'Active';
                      } else {
                        status = 'Inactive';
                      }
                      connection.query(
                        'UPDATE `protocolcertificate` SET `APIStatus`=' +
                          mysql.escape(status) +
                          ' WHERE `protocolCertificateUUID`=' +
                          mysql.escape(protocolCertificateUUID),
                        function (err, updateProtocolStatus) {
                          if (err) reject(err);
                          else
                            resolve({
                              statusAPI: response.data.response.status,
                              msg: response.data.response.msg,
                            });
                        }
                      );
                    });
                    // resolve(resultS);
                  }
                }
              );
            }
          }
        );
      });
    },
    async fetchProtocolCertificateByPartnerProfile(
      root,
      { partnerProfileUUID }
    ) {
      //sql query for fetch ProtocolCertificate by partner profile id

      return new Promise((resolve, reject) => {
        var sql, values;

        var sql =
          ' select * FROM `protocolcertificate` where `partnerProfileUUID`=? ORDER BY `createdOn` DESC  ';
        values = [partnerProfileUUID];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            resolve(result);
          }
        });
      });
    },
  },
  Mutation: {
    async createProtocolCertificate(root, { input }) {
      console.log('file name----------------------' + input.publicCertificate);
      console.log(input);
      //sql query for createProtocolCertificate
      return new Promise(async (resolve, reject) => {
        /*   var fileByteArray = [];
        function processFile(theFile) {
          return function (e) {
            var theBytes = e.target.result; //.split('base64,')[1]; // use with uploadFile2
            fileByteArray.push(theBytes);
          };
        }
        function uploadFile3(files) {
          // var files = myInput.files[0];
          var reader = new FileReader();
          reader.onload = processFile(files);
          reader.readAsArrayBuffer(files);
        }
        uploadFile3(input.publicCertificate);*/
        const {
          filename,
          mimetype,
          createReadStream,
        } = await input.publicCertificate;

        console.log(createReadStream);
        const stream = createReadStream();

        console.log('----------createReadStream---------', createReadStream);
        const streamData = await readStream(stream);
        const buff = Buffer.from(streamData);
        // console.log(stream, streamData, buff);
        // console.log("stream",stream,"streamData", streamData,"buff", buff);

        var sql =
          'INSERT INTO protocolcertificate (protocolCertificateName,publicCertificate,privateCertificate,usageValue,protocolCertificateComments,partnerProfileUUID) VALUES(?,?,?,?,?,?)';
        // var sql =
        //'INSERT INTO protocolcertificate (publicCertificate,usageValue) VALUES(?,?)';

        var values = [
          filename,
          streamData,
          null,
          input.usageValue,
          input.protocolCertificateComments,
          input.partnerProfileUUID,
        ];
        console.log('sql', sql, values);
        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async createProtocolCertificate1(root, { file }) {
      console.log('file name' + file);
      console.log(file);
      //sql query for createProtocolCertificate
      return new Promise(async (resolve, reject) => {
        const { filename, mimetype, createReadStream } = await file;
        console.log(filename, mimetype, createReadStream);
        const stream = createReadStream();
        const streamData = await readStream(stream);
        const buff = Buffer.from(streamData);
        console.log(stream, streamData, buff);
        var publicCertificate = {
          Name: filename,
          imageData: buff,
          Length: stream.bytesRead,
          Type: mimetype,
        };
        /* function readImageFile(file) {
          // read binary data from a file:
          const bitmap = fs.readFileSync(file);
          const buf = new Buffer(bitmap);
          return buf;
        }
        const data = await readImageFile(file);
        console.log(data);*/
        // var sql =
        // 'INSERT INTO protocolcertificate (protocolCertificateName,publicCertificate,privateCertificate,usageValue,protocolCertificateComments,partnerProfileUUID) VALUES(?,?,?,?,?,?)';
        var sql =
          'INSERT INTO protocolcertificate (publicCertificate,usageValue) VALUES(?,?)';

        var values = [buff, 'bbbkk'];
        console.log('sql', sql, values);
        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async updateProtocolCertificate(root, { input }) {
      console.log(input);
      //sql query for update ProtocolCertificate
      return new Promise((resolve, reject) => {
        var sql =
          'UPDATE protocolcertificate SET profileIDTypesUUID = ?,profileIDValue=?,partnerProfileUUID=?,lastUpdatedOn=? WHERE profileIDValuesUUID = ?';

        var values = [
          input.profileIDTypesUUID,
          input.profileIDValue,
          input.partnerProfileUUID,
          new Date(),
          input.profileIDValuesUUID,
        ];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async removeProtocolCertificate(root, { protocolCertificateUUID }) {
      // console.log(input);
      //sql query for remove ProtocolCertificate
      return new Promise((resolve, reject) => {
        var sql =
          'DELETE FROM `protocolcertificate` WHERE `protocolCertificateUUID` = ?;';

        var values = [protocolCertificateUUID];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
};

const readStream = async (stream) => {
  stream.setEncoding('base64');

  return new Promise((resolve, reject) => {
    let data = '';

    // eslint-disable-next-line no-return-assign
    stream.on('data', (chunk) => (data += chunk));
    stream.on('end', () => {
      resolve(data);
      console.log(data);
    });
    stream.on('error', (error) => {
      reject(error);
      console.log(error.stack);
    });
  });
};

/*const profileidvaluesSchema1 = new ApolloServer({
  typeDefs,
  resolvers,
});*/
const profileidvaluesSchema = makeExecutableSchema({
  typeDefs,
  resolvers,
  formatError: (err) => {
    // Don't give the specific errors to the client.
    if (err.message.startsWith('Database Error: ')) {
      return new Error('sql error');
    }

    // Otherwise return the original error.  The error can also
    // be manipulated in other ways, so long as it's returned.
    return err;
  },
});

module.exports = profileidvaluesSchema;
