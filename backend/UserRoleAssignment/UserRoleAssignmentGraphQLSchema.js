var { makeExecutableSchema } = require('graphql-tools');
const config = require('../dbConfig');
const mysql = require('mysql');
const withAuthfn=require('../verifyMiddleware/withAuth');
var connection = require('../database');

/*var MySQLStore = require('connect-mysql'); // mysql session store
var options = {
      config: {
        host: config.db.host,
        user: config.db.user,
        password: config.db.password,
        database: config.db.database,
        port: config.db.port,
      },
      pool:true,
      keepalive:true

    };

    connection=MySQLStore(options)*/ /*= mysql.createPool({
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database,
  port: config.db.port,
});*/
/*/*function handleDisconnect() {
  connection = mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database,
    port: config.db.port,
  }); // Recreate the connection, since
  // the old one cannot be reused.

  connection.connect(function (err) {
    // The server is either down
    if (err) {
      // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    } // to avoid a hot loop, and to allow our node script to
  }); // process asynchronous requests in the meantime.
  // If you're also serving http, display a 503 error.
  connection.on('error', function (err) {
    console.log('db error', err);
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      // Connection to the MySQL server is usually
      handleDisconnect(); // lost due to either server restart, or a
    } else {
      // connnection idle timeout (the wait_timeout
      throw err; // server variable configures this)
    }
  });
}

handleDisconnect();*/

//connection.connect();

const typeDefs = `
scalar Date

type UserRoleAssignment{
  idUserRoleAssignment:String
  userRoleAssignmentUUID:String
  userUUID:String
  userRoleUUID:String
  userOrgUUID:String
  userTypeUUID:String
  createdOn:Date
  lastUpdatedOn:Date
  organizatonName:String
}

input UserRoleAssignmentInput{
  userUUID:String
  userRoleUUID:String
  userOrgUUID:String
  userTypeUUID:String
  userRoleAssignmentUUID:String
} 

type Query{
   allUserRoleAssignment:[UserRoleAssignment]
   fetchOrganizationFromRoleAssignment(userUUID:String):[UserRoleAssignment]
}

type Mutation{
   createUserRoleAssignment(input:UserRoleAssignmentInput):UserRoleAssignment
   updateUserRoleAssignment(input:UserRoleAssignmentInput): UserRoleAssignment
   removeUserRoleAssignment(input:UserRoleAssignmentInput):UserRoleAssignment
}

`;

const resolvers = {
  Query: {
    allUserRoleAssignment: (root, args, context, info) => {
      //sql query for fetch all userAssignment
      /**SELECT userroleassignment.userRoleAssignmentUUID, userroleassignment.createdOn,userroleassignment.lastUpdatedOn, (user.userID)as userUUID, (userroletype.userRoleTypeName ) as userRoleUUID, (organization.organizatonName) as userOrgUUID, userroleassignment.userTypeUUID from userroleassignment JOIN organization ON organization.organizationUUID=userroleassignment.userOrgUUID JOIN user ON user.userUUID=userroleassignment.userUUID JOIN userroletype ON userroletype.userRoleTypeUUID=userroleassignment.userRoleUUID ORDER BY userroleassignment.createdOn DESC */
      return new Promise((resolve, reject) => {
        connection.query(
          'SELECT * from userroleassignment ORDER BY  createdOn DESC ',
          function (err, post) {
            if (err) {
              reject(err);
            } else {
              resolve(post);
            }
          }
        );
      });
    },
    async fetchOrganizationFromRoleAssignment(root, { userUUID }) {
      console.log(userUUID);
      return new Promise((resolve, reject) => {
        var sql =
          'SELECT organizatonName,idUserRoleAssignment,userRoleAssignmentUUID, userUUID, userRoleUUID, userOrgUUID,userTypeUUID FROM userroleassignment Inner join organization on organization.organizationUUID =userroleassignment.userOrgUUID where userroleassignment.userUUID=' +
          mysql.escape(userUUID);

        console.log(sql);
        connection.query(sql, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
  Mutation: {
    async createUserRoleAssignment(root, { input }) {
      //sql query for create userAssignment
      console.log(
        input.userUUID,
        input.userRoleUUID,
        input.userOrgUUID,
        input.userTypeUUID,
        Date.now(),
        Date.now()
      );
      //sql query for create userAssignment
      return new Promise((resolve, reject) => {
        var sql =
          'INSERT INTO userroleassignment (`userUUID`, `userRoleUUID`, `userOrgUUID`, `userTypeUUID`) VALUES(?,?,?,?)';

        var values = [
          input.userUUID,
          input.userRoleUUID,
          input.userOrgUUID,
          input.userTypeUUID,
        ];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async updateUserRoleAssignment(root, { input }) {
      console.log(input);
      //sql query for update userroleassignment
      return new Promise((resolve, reject) => {
        var sql =
          'UPDATE userroleassignment SET userUUID = ?,userRoleUUID=? ,userOrgUUID=?,userTypeUUID=?,lastUpdatedOn=? WHERE userRoleAssignmentUUID = ?';

        var values = [
          input.userUUID,
          input.userRoleUUID,
          input.userOrgUUID,
          input.userTypeUUID,
          new Date(),
          input.userRoleAssignmentUUID,
        ];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async removeUserRoleAssignment(root, { input }) {
      console.log(input);
      //sql query for remove userroleassignment
      return new Promise((resolve, reject) => {
        var sql =
          'DELETE FROM `userroleassignment` WHERE `userRoleAssignmentUUID` = ?;';

        var values = [input.userRoleAssignmentUUID];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
};

const userRoleAssignmentSchema = makeExecutableSchema({
  typeDefs,
  resolvers,
  formatError: (err) => {
    // Don't give the specific errors to the client.
    if (err.message.startsWith('Database Error: ')) {
      return new Error('sql error');
    }

    // Otherwise return the original error.  The error can also
    // be manipulated in other ways, so long as it's returned.
    return err;
  },
});

module.exports = userRoleAssignmentSchema;
