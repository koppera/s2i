const jwt = require('jsonwebtoken');
const config = require('../dbConfig');

const withAuthfn = function(req, res, next) { console.log("req",req,"res",res,"req.headers.referer.includes('/login')",req.headers)
//if(!req.headers.referer.includes('/login')){
  if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
      const token = req.headers.authorization.split(' ')[1];
      console.log("token",token)
      if (!token) {
        res.status(401).send('Unauthorized: No token provided');
      } else {
        jwt.verify(token, config.db.Secret, function(err, decoded) {
          if (err) {
            res.status(401).send('Unauthorized: Invalid token');
          } else {
            console.log("req",req,"decoded",decoded)
           // req.email = decoded.email;
            next();
          }
        });
      }
    }
  /*}else{
    next();
  }*/
 
}

//app.use(withAuthfn);
module.exports = withAuthfn;