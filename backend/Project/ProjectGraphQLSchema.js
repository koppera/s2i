var { makeExecutableSchema } = require('graphql-tools');
const config = require('../dbConfig');
const mysql = require('mysql');
var connection = require('../database'); /*= mysql.createPool({
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database,
  port: config.db.port,
});*/
/*function handleDisconnect() {
  connection =  mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database,
    port: config.db.port,
  });; // Recreate the connection, since
                                                  // the old one cannot be reused.

  connection.connect(function(err) {              // The server is either down
    if(err) {                                     // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    }                                     // to avoid a hot loop, and to allow our node script to
  });                                     // process asynchronous requests in the meantime.
                                          // If you're also serving http, display a 503 error.
  connection.on('error', function(err) {
    console.log('db error', err);
    if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
      handleDisconnect();                         // lost due to either server restart, or a
    } else {                                      // connnection idle timeout (the wait_timeout
      throw err;                                  // server variable configures this)
    }
  });
}

handleDisconnect();*/


//connection.connect();
const typeDefs = `
scalar Date

type Project{
    idOrgProject:String
    projectUUID:String
    projectName:String
    projectDescription:String
    organizationUUID:String
    createdOn:Date
    updatedOn:Date
}

input ProjectInput{
    idOrgProject:String
    projectUUID:String
    projectName:String
    projectDescription:String
    organizationUUID:String
}
type Query{
   allProject:[Project]
   fetchProject(searchInput:String): [Project]
   fetchProjectByOrgUUID(organizationUUID:String):[Project]
}

type Mutation{
   createProject(input:ProjectInput): Project
   updateProject(input:ProjectInput): Project
   removeProject(input:ProjectInput):Project
}

`;

const resolvers = {
  Query: {
    allProject: (root, args, context, info) => {
      //sql query for fetch all Project
      return new Promise((resolve, reject) => {
        connection.query(
          'SELECT * from orgproject  ORDER BY  createdOn DESC;',
          function (err, post) {
            if (err) {
              reject(err);
            } else {
              resolve(post);
            }
          }
        );
      });
    },
    async fetchProject(root, { searchInput }) {
      //sql query for fetch Project

      return new Promise((resolve, reject) => {
        var sql, values;

        if (searchInput == '') {
          sql = 'Select `idOrgProject`, `projectUUID`, `projectName`, `projectDescription`, `createdOn`, `updatedOn`, (SELECT `organizatonName` FROM `organization` WHERE `organizationUUID`=orgproject.organizationUUID)as `organizationUUID` from orgproject  ORDER BY  createdOn DESC;';
        } else {
          var sql =
            ' Select `idOrgProject`, `projectUUID`, `projectName`, `projectDescription`, `createdOn`, `updatedOn`, (SELECT `organizatonName` FROM `organization` WHERE `organizationUUID`=orgproject.organizationUUID)as `organizationUUID` from orgproject where (projectName= ' +
            mysql.escape(searchInput) +
            'OR projectDescription=' +
            mysql.escape(searchInput) +
            'OR organizationUUID=' +
            mysql.escape(searchInput) +
            ') ORDER BY  createdOn DESC;';
        }

        connection.query(sql, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async fetchProjectByOrgUUID(root, { organizationUUID }) {
      //sql query for fetch Project by orguuid

      return new Promise((resolve, reject) => {
        var sql, values;

        if (organizationUUID == '') {
          sql = 'Select * from orgproject  ORDER BY  createdOn DESC;';
        } else {
          var sql =
            'Select `idOrgProject`, `projectUUID`, `projectName`, `projectDescription`, `createdOn`, `updatedOn`, (SELECT `organizatonName` FROM `organization` WHERE `organizationUUID`=orgproject.organizationUUID)as `organizationUUID` from orgproject where organizationUUID= ' +
            mysql.escape(organizationUUID) +
            ' ORDER BY  createdOn DESC;';
        }

        connection.query(sql, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
  Mutation: {
    async createProject(root, { input }) {
      console.log(input);
      //sql query for create Project
      return new Promise((resolve, reject) => {
        var sql =
          'INSERT INTO orgproject (projectName,projectDescription,organizationUUID) VALUES(?,?,?) ;';

        var values = [
          input.projectName,
          input.projectDescription,
          input.organizationUUID,
        ];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async updateProject(root, { input }) {
      console.log(input);
      //sql query for update
      return new Promise((resolve, reject) => {
        var sql =
          'UPDATE orgproject SET projectName = ?,projectDescription=? ,organizationUUID=?,updatedOn=? WHERE projectUUID = ?';

        var values = [
          input.projectName,
          input.projectDescription,
          input.organizationUUID,
          new Date(),
          input.projectUUID,
        ];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
    async removeProject(root, { input }) {
      console.log(input);
      //sql query for create PROJECT
      return new Promise((resolve, reject) => {
        var sql = 'DELETE FROM `orgproject` WHERE `projectUUID` = ?;';

        var values = [input.projectUUID];

        connection.query(sql, values, function (err, result) {
          if (err) {
            reject(err);
          } else {
            console.log(result);
            resolve(result);
          }
        });
      });
    },
  },
};

const ProjectSchema = makeExecutableSchema({
  typeDefs,
  resolvers,
  formatError: (err) => {
    // Don't give the specific errors to the client.
    if (err.message.startsWith('Database Error: ')) {
      return new Error('sql error');
    }

    // Otherwise return the original error.  The error can also
    // be manipulated in other ways, so long as it's returned.
    return err;
  },
});

module.exports = ProjectSchema;
